//
//  MyPostTableViewCell.m
//  Mabo
//
//  Created by vishnuprasathg on 11/8/17.
//  Copyright © 2017 DCI. All rights reserved.
//

#import "FevouritesTableViewCell.h"

@implementation FevouritesTableViewCell

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

- (IBAction)textPostProfilePicBtnAction:(id)sender {
}

- (IBAction)textPostDeleteBtnAction:(id)sender {
}

- (IBAction)textPostLocationBtnAction:(id)sender {
}

- (IBAction)textPostFevoriteBtnAction:(id)sender {
}

- (IBAction)textPostCommentsBtnAction:(id)sender {
}

- (IBAction)eventPostProfilePicBtnAction:(id)sender {
}

- (IBAction)eventPostEditBtnAction:(id)sender {
}

- (IBAction)eventPostDeleteBtnAction:(id)sender {
}

- (IBAction)eventPostJoiningBtnAction:(id)sender {
}

- (IBAction)eventPostMaybeBtnAction:(id)sender {
}

- (IBAction)eventPostPriceBtnAction:(id)sender {
}

- (IBAction)eventPostLocationBtnAction:(id)sender {
}

- (IBAction)eventPostFevoritesBtnAction:(id)sender {
}

- (IBAction)eventPostCommentsBtnAction:(id)sender {
}

@end
