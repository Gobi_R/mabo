  //
//  ChatRecentsViewController.m
//  Mabo
//
//  Created by vishnuprasathg on 11/23/17.
//  Copyright © 2017 DCI. All rights reserved.
//

#import "ChatRecentsViewController.h"
#import "ChatFavoritesViewController.h"
#import "SearchChatUserViewController.h"
#import "Client.h"
#import "UtilityGraphic.h"
#import "ActivityView.h"
#import <Firebase/Firebase.h>
#import "NSDate+TimeAgo.h"
#import "PrefixHeader.pch"
#import "Utilities.h"
#import "ChatViewController.h"
#import "SharedVariables.h"
#import "CallLogsViewController.h"
#import "SinchClient.h"
#import "UserProfileViewController.h"

@interface ChatRecentsViewController ()<UITableViewDelegate,UITableViewDataSource,UINavigationControllerDelegate>

@property (nonatomic, strong) NSMutableArray *chatList;

@property (nonatomic, strong) ActivityView *activity;

@property (nonatomic, strong) Firebase *connectRef;
@property (nonatomic, strong) Firebase *lastOnlineRef;
@property (nonatomic, strong) Firebase *connectRefObserv;
@property (nonatomic, strong) NSDictionary *dictionary;
@end

@implementation ChatRecentsViewController

{
    NSMutableArray * recentArray;
}

- (void)viewDidLoad {
    [super viewDidLoad];
    
    //[[SinchClient sharedClient] activeClient];

    self.segmentOutlet.selectedSegmentIndex = 0;
    recentArray = [[NSMutableArray alloc] init];
    
    self.tableViewOutlet.delegate = self;
    self.tableViewOutlet.dataSource = self;
    
    
    // Do any additional setup after loading the view.
}
- (void)navigationController:(UINavigationController *)navigationController
      willShowViewController:(UIViewController *)viewController animated:(BOOL)animated
{
    [self viewWillAppear:animated];
}

-(void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    self.segmentOutlet.selectedSegmentIndex = 0;
}
-(void)viewDidAppear:(BOOL)animated
{
   NSLog(@"chatlist:%@",self.chatList);
    
    _segmentOutlet.selectedSegmentIndex = 0;
    
    [self controlIncoming];
    
    [[NSNotificationCenter defaultCenter] removeObserver:self name:@"cacheUpload" object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(receiveCacheUpload:) name:@"cacheUpload" object:nil];
    
    [self.tableViewOutlet reloadData];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

-(NSMutableArray *)sortArray:(NSMutableArray *)array{
    NSDateFormatter *formatter = [[NSDateFormatter alloc] init];
    [formatter setDateFormat:@"yyyy-MM-dd HH:mm:ss Z"];
    
    NSArray *arrayResult = [array sortedArrayUsingComparator:^NSComparisonResult(NSDictionary *obj1, NSDictionary *obj2) {
        
        NSString *data1=[NSString stringWithFormat:@"%@",[obj1 objectForKey:@"lastUpdate"]];
        NSString *data2=[NSString stringWithFormat:@"%@",[obj2 objectForKey:@"lastUpdate"]];
        
        NSDate *d1 = [formatter dateFromString:data1];
        NSDate *d2 = [formatter dateFromString:data2];
        return [d2 compare:d1];
    }];
    
    return  [arrayResult mutableCopy];
}

#pragma mark - TABLEVIEW DELEGATE METHODS


-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    
        return self.chatList.count;
    
}

-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    
    UITableViewCell *cellReal = [tableView dequeueReusableCellWithIdentifier:@"Cell"];
    
    if (self.chatList.count > 0) {
        
        //NSDictionary* recentDict = [self.chatList objectAtIndex:indexPath.row];
        
//        UIImageView* profileImageView = [cellReal viewWithTag:1];
//        UILabel * nameLbl = [cellReal viewWithTag:2];
//        UILabel* lastSeen = [cellReal viewWithTag:3];
//
//        NSString* name = [[recentDict objectForKey:@"user"] objectForKey:@"extended_name"];
//        NSString* picture = [[recentDict objectForKey:@"user"] objectForKey:@"picture_path"];
//        NSString* lastseen = [[recentDict objectForKey:@"user"] objectForKey:@"last_seen"];
        
        [self populateCell:[self.chatList objectAtIndex:indexPath.row] tableCell:cellReal];
        int newMessage = [self findNotReaded:[self.chatList objectAtIndex:indexPath.row]];
        [self updateBadge:newMessage tableCell:cellReal];
        
        
    }
    
    return cellReal;
}

- (BOOL)tableView:(UITableView *)tableView shouldHighlightRowAtIndexPath:(NSIndexPath *)indexPath{
    return (self.chatList.count>0);
}


-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
//    if([[Client sharedClient].reachAbility isReachable]){
//        //        ProfileViewController *profile = [[ProfileViewController alloc] initWithUser:[[self.visibleUserList objectAtIndex:indexPath.row] objectForKey:@"username"] andName:[[self.visibleUserList objectAtIndex:indexPath.row] objectForKey:@"extended_name"]];
//        //        [self.navigationController pushViewController:profile animated:YES];
//    }
    
    
    NSString *email = [[self.chatList objectAtIndex:indexPath.row] objectForKey:@"username"];
    
    
    int indexProfileFoto= [[[self.chatList objectAtIndex:indexPath.row] objectForKey:@"picture_index_default"] intValue];
    NSArray *arrayPictures = [[self.chatList objectAtIndex:indexPath.row] objectForKey:@"pictures"];
    NSString *pathPicture=@"";
    for(unsigned i=0;i<arrayPictures.count;i++){
        NSDictionary *dic = [arrayPictures objectAtIndex:i];
        if([[dic objectForKey:@"pic_index"] intValue]==indexProfileFoto){
            pathPicture = [dic objectForKey:@"web_path"];
            break;
        }
    }
    
    if([[self.chatList objectAtIndex:indexPath.row] objectForKey:@"picture_path"]){
        pathPicture=[[self.chatList objectAtIndex:indexPath.row] objectForKey:@"picture_path"];
    }
    
    NSString *extended_name = [[self.chatList objectAtIndex:indexPath.row] objectForKey:@"extended_name"];
    
    if (extended_name == nil) {
        
        extended_name = @"";
    }
    
    NSDictionary *dictionaryUser = @{@"email":email,@"picture":pathPicture,@"extended_name":extended_name};
    
    ChatViewController * chatVC = [self.storyboard instantiateViewControllerWithIdentifier:@"ChatViewController"];
    chatVC.userDictionaryToChat = dictionaryUser;
    [self.navigationController pushViewController:chatVC animated:YES];
    
}

- (void)tableView:(UITableView *)tableView commitEditingStyle:(UITableViewCellEditingStyle)editingStyle forRowAtIndexPath:(NSIndexPath *)indexPath {
    if (editingStyle == UITableViewCellEditingStyleDelete) {
        NSString *email = [[self.chatList objectAtIndex:indexPath.row] objectForKey:@"username"];
        [[Client sharedClient] deleteUserInCache:email];
        self.chatList = [self sortArray:[[[Client sharedClient] getMessagesList] mutableCopy]];
        [self.tableViewOutlet deleteRowsAtIndexPaths:@[indexPath] withRowAnimation:UITableViewRowAnimationRight];
    }
}


- (IBAction)segmentValueChanged:(id)sender {
    
    
    if(self.segmentOutlet.selectedSegmentIndex==0){
        
    }
    else if(self.segmentOutlet.selectedSegmentIndex==1){
        
        ChatFavoritesViewController * chatFavVC = [self.storyboard instantiateViewControllerWithIdentifier:@"ChatFavoritesViewController"];
        [self.navigationController pushViewController:chatFavVC animated:NO];
        
    }

}
- (IBAction)plusBtnAction:(id)sender {
    
    SearchChatUserViewController * chatSearchVC = [self.storyboard instantiateViewControllerWithIdentifier:@"SearchChatUserViewController"];
    [self.navigationController pushViewController:chatSearchVC animated:YES];
}
- (IBAction)profileBtnAction:(id)sender {
    
    UIView* baseView = [sender superview];
    UIView* cell = [baseView superview];

    NSIndexPath *indexPath = [self.tableViewOutlet indexPathForCell:cell];
    
    NSDictionary * dictionaryPostSelected = self.chatList[indexPath.row];
    
    NSLog(@"dict:%@",dictionaryPostSelected);
    
    UserProfileViewController * userProfileVC = [self.storyboard instantiateViewControllerWithIdentifier:@"UserProfileViewController"];
    userProfileVC.userDict = dictionaryPostSelected;
    //[self.navigationController pushViewController:userProfileVC animated:YES];
    [self.navigationController presentViewController:userProfileVC animated:YES completion:nil];
    
}

-(void)updateBadge:(int)number tableCell:(UITableViewCell *)cell{
    
    UIView * badge = [cell viewWithTag:4];
    UILabel* labelBadge = [cell viewWithTag:5];
    
    [badge setBackgroundColor:[UIColor colorWithRed:122/255.0f green:222/255.0f blue:225/255.0f alpha:1.0f]];
    
    badge.layer.cornerRadius = badge.frame.size.height / 2;
    badge.layer.masksToBounds = YES;
    
    if(number!=0){
        [badge setHidden:NO];
        [labelBadge setText:[NSString stringWithFormat:@"%i",number]];
        //[self.labelName setFont:[UIFont fontWithName:FONT_MB_BOLDITALIC size:16]];
    }else{
        [labelBadge setText:@""];
        [badge setHidden:YES];
        //[self.labelName setFont:[UIFont fontWithName:FONT_MB_REGULAR size:16]];
    }
}

-(void)populateCell:(NSDictionary *)dictionary tableCell:(UITableViewCell *)cell{
    self.dictionary = dictionary;
    [self.activity setHidden:YES];
    /*
     if(self.delegate){
     [self.cancel setHidden:![self.delegate isInEdit]];
     }
     */
    
    UIImageView* picture = [cell viewWithTag:1];
    UILabel * labelName = [cell viewWithTag:2];
    
    picture.layer.cornerRadius = picture.frame.size.height / 2;
    picture.layer.masksToBounds = YES;
    
    
    [labelName setText:[dictionary objectForKey:@"extended_name"]];
    UIImage *defaultPicture = [UIImage imageNamed:@"avatar2.png"];
    defaultPicture = [UtilityGraphic imageWithImage:defaultPicture scaledToSize:CGSizeMake(picture.frame.size.width,picture.frame.size.height)];
    [picture setImage:defaultPicture];
    [picture setClipsToBounds:YES];
    
    if([dictionary objectForKey:@"pictures"] && ![[dictionary objectForKey:@"pictures"] isEqual:[NSNull null]] && [[dictionary objectForKey:@"pictures"] count]>0){
        int indexProfileFoto=[[dictionary objectForKey:@"picture_index_default"] intValue];
        NSArray *arrayPictures = [dictionary objectForKey:@"pictures"];
        NSString *pathPicture=@"";
        for(unsigned i=0;i<arrayPictures.count;i++){
            NSDictionary *dic = [arrayPictures objectAtIndex:i];
            if([[dic objectForKey:@"pic_index"] intValue]==indexProfileFoto){
                pathPicture = [dic objectForKey:@"web_path"];
                break;
            }
        }
        if(![pathPicture isEqualToString:@""] || ![pathPicture isEqual:[NSNull null]]){
            [self.activity setHidden:NO];
            [[Client sharedClient] getPicture:pathPicture from:pathPicture withCompletition:^(UIImage *image, BOOL isInCache, id sender) {
                if(sender==pathPicture){
                    [self.activity setHidden:YES];
                    image = [UtilityGraphic imageWithImage:image scaledToSize:CGSizeMake(picture.frame.size.width,picture.frame.size.height)];
                    [picture setImage:image];
                }
            }];
        }
    }
    
    
    if([dictionary objectForKey:@"picture_path"] && ![[dictionary objectForKey:@"picture_path"] isEqual:[NSNull null]]){
        if(![[dictionary objectForKey:@"picture_path"] isEqualToString:@"null"] && ![[dictionary objectForKey:@"picture_path"] isEqualToString:@""]){
            [self.activity setHidden:NO];
            [[Client sharedClient] getPicture:[dictionary objectForKey:@"picture_path"] from:[dictionary objectForKey:@"picture_path"] withCompletition:^(UIImage *image, BOOL isInCache, id sender) {
                if(sender==[dictionary objectForKey:@"picture_path"]){
                    [self.activity setHidden:YES];
                    image = [UtilityGraphic imageWithImage:image scaledToSize:CGSizeMake(picture.frame.size.width,picture.frame.size.height)];
                    [picture setImage:image];
                }
            }];
        }
    }
    
    [self lunchObserv:cell];
}

-(void)lunchObserv :(UITableViewCell *)cell{
    
    UILabel* labelConnect = [cell viewWithTag:3];
    
    [self.connectRef removeAllObservers];
    [self.connectRefObserv removeAllObservers];
    [self.lastOnlineRef removeAllObservers];
    self.connectRef=nil;
    self.connectRefObserv=nil;
    self.lastOnlineRef=nil;
    
    [labelConnect setText:@"Offline"];
    labelConnect.textColor = [UIColor redColor];
    
    self.connectRef = [[Firebase alloc] initWithUrl:FIREBASE_URL];
    self.connectRef = [self.connectRef childByAppendingPath:[NSString stringWithFormat:@"users/%@/connections",[UtilityGraphic URLEncodedString:[self.dictionary objectForKey:@"username"]]]];
    NSLog(@"userRecent:%@",[self.dictionary objectForKey:@"username"]);
    [self.connectRef observeEventType:FEventTypeValue withBlock:^(FDataSnapshot *snapshot) {
        if(![snapshot.value isEqual:[NSNull null]]){
            [labelConnect setText:@"Online"];
            [labelConnect setTextColor:[UIColor greenColor]];
        }else{
            [labelConnect setText:@"Offline"];
            [labelConnect setTextColor:COLOR_MB_LIGHTGRAY];
            self.lastOnlineRef = [[Firebase alloc] initWithUrl:FIREBASE_URL];
            self.lastOnlineRef = [self.lastOnlineRef  childByAppendingPath:[NSString stringWithFormat:@"users/%@/lastOnLine",[UtilityGraphic URLEncodedString:[self.dictionary objectForKey:@"username"]]]];
            [self.lastOnlineRef observeEventType:FEventTypeValue withBlock:^(FDataSnapshot *snapshot) {
                if(![snapshot.value isEqual:[NSNull null]]){
                    long long value = [snapshot.value longLongValue];
                    NSDate *date = [[NSDate alloc] initWithTimeIntervalSince1970:value/1000];
//                    NSString *ago = [date timeAgo];
//                    [labelConnect setText:@""];
//                    [labelConnect setText:[NSString stringWithFormat:@"%@: %@",NSLocalizedString(@"Last seen",nil),ago]];
                    
                    NSDateFormatter *fmt = [[NSDateFormatter alloc] init];
                    [fmt setDateFormat:@"dd MMMM yyyy"] ;
                    fmt.timeZone = [NSTimeZone systemTimeZone];
                    NSString *local = [fmt stringFromDate:date];
                    NSLog(@"%@", local);
                    
                    NSMutableAttributedString * value1 = [[Utilities sharedInstance]loadTimeDifferent:date];
                    NSString * validateString = [NSString stringWithFormat:@"last seen %@",value1];
                    
                    validateString = [validateString substringToIndex:[validateString length] - 3];
                    [labelConnect setText:validateString];
                    
                }
            }];
            
        }
    }];
    
    self.connectRefObserv = [[Firebase alloc] initWithUrl:FIREBASE_URL];
    self.connectRefObserv = [self.connectRefObserv childByAppendingPath:[NSString stringWithFormat:@"users/%@/connections",[UtilityGraphic URLEncodedString:[self.dictionary objectForKey:@"username"]]]];
    [self.connectRef observeEventType:FEventTypeChildChanged withBlock:^(FDataSnapshot *snapshot) {
        if(![snapshot.value isEqual:[NSNull null]]){
            [labelConnect setText:@"Online"];
            [labelConnect setTextColor:[UIColor greenColor]];
        }else{
            [labelConnect setText:@"Offline"];
            [labelConnect setTextColor:COLOR_MB_LIGHTGRAY];
            self.lastOnlineRef = [[Firebase alloc] initWithUrl:FIREBASE_URL];
            self.lastOnlineRef = [self.lastOnlineRef  childByAppendingPath:[NSString stringWithFormat:@"users/%@/lastOnLine",[UtilityGraphic URLEncodedString:[self.dictionary objectForKey:@"username"]]]];
            [self.lastOnlineRef observeEventType:FEventTypeValue withBlock:^(FDataSnapshot *snapshot) {
                if(![snapshot.value isEqual:[NSNull null]]){
                    long long value = [snapshot.value longLongValue];
                    NSDate *date = [[NSDate alloc] initWithTimeIntervalSince1970:value/1000];
//                    NSString *ago = [date timeAgo];
//                    [labelConnect setText:@""];
//                    [labelConnect setText:[NSString stringWithFormat:@"%@: %@",NSLocalizedString(@"Last seen",nil),ago]];
                    
                    NSDateFormatter *fmt = [[NSDateFormatter alloc] init];
                    [fmt setDateFormat:@"dd MMMM yyyy"] ;
                    fmt.timeZone = [NSTimeZone systemTimeZone];
                    NSString *local = [fmt stringFromDate:date];
                    NSLog(@"%@", local);
                    
                    NSMutableAttributedString * value1 = [[Utilities sharedInstance]loadTimeDifferent:date];
                    NSString * validateString = [NSString stringWithFormat:@"last seen %@",value1];
                    
                    validateString = [validateString substringToIndex:[validateString length] - 3];
                    [labelConnect setText:validateString];
                }
                else
                {
                    [labelConnect setText:@"Offline"];
                    labelConnect.textColor = [UIColor redColor];
                    NSLog(@"Offline");
                }
            }];
            
        }
        
    }];
    
}

-(int)findNotReaded:(NSDictionary *)dictionary{
    int result=0;
    
    if([dictionary objectForKey:@"list"]){
        NSPredicate *predicate = [NSPredicate predicateWithFormat:@"(readed==%i)",0];
        NSArray *arrayResult = [[dictionary objectForKey:@"list"] filteredArrayUsingPredicate:predicate];
        result = (int)arrayResult.count;
    }else{
        NSString *username = [dictionary objectForKey:@"username"];
        NSPredicate *predicate = [NSPredicate predicateWithFormat:@"(username==%@)",username];
        NSArray *arrayUserResult = [[[Client sharedClient] getMessagesList] filteredArrayUsingPredicate:predicate];
        NSArray *arrayListMessageUser = [[arrayUserResult lastObject] objectForKey:@"list"];
        
        NSPredicate *predicateReaded = [NSPredicate predicateWithFormat:@"(readed==%i)",0];
        NSArray *arrayResultReaded = [arrayListMessageUser filteredArrayUsingPredicate:predicateReaded];
        
        result = (int)arrayResultReaded.count;
        
    }
    return result;
}

-(NSDictionary *)findUserFromEmail:(NSString *)email{
    NSPredicate *predicate = [NSPredicate predicateWithFormat:@"(username==%@)",email];
    NSArray *arrayResult = [self.chatList filteredArrayUsingPredicate:predicate];
    return (arrayResult.count>0)?[arrayResult lastObject]:nil;
}

-(void)controlIncoming{
    [self reloadContent];
    
    if([Client sharedClient].incomingUser){
        NSDictionary *findedUserDictionary = [self findUserFromEmail:[Client sharedClient].incomingUser];
        if(findedUserDictionary){
            NSString *email = [findedUserDictionary objectForKey:@"username"];
            int indexProfileFoto=[[findedUserDictionary objectForKey:@"picture_index_default"] intValue];
            NSArray *arrayPictures = [findedUserDictionary objectForKey:@"pictures"];
            NSString *pathPicture=@"";
            for(unsigned i=0;i<arrayPictures.count;i++){
                NSDictionary *dic = [arrayPictures objectAtIndex:i];
                if([[dic objectForKey:@"pic_index"] intValue]==indexProfileFoto){
                    pathPicture = [dic objectForKey:@"web_path"];
                    break;
                }
            }
            [Client sharedClient].incomingUser=nil;
            NSString *extended_name = [findedUserDictionary objectForKey:@"extended_name"];
            NSDictionary *dictionaryUser = @{@"email":email,@"picture":pathPicture,@"extended_name":extended_name};
            
            ChatViewController * chatVC = [self.storyboard instantiateViewControllerWithIdentifier:@"ChatViewController"];
            chatVC.userDictionaryToChat = dictionaryUser;
            [self.navigationController pushViewController:chatVC animated:YES];
            
//            ChatViewController *chat = [[ChatViewController alloc] initWithUserDictionary:dictionaryUser];
//            [self.navigationController pushViewController:chat animated:YES];
        }
    }
}

-(void)reloadContent{
    
    self.chatList = [self sortArray:[[[Client sharedClient] getMessagesList] mutableCopy]];
    
    if(!self.chatList){
        self.chatList = [NSMutableArray arrayWithCapacity:0];
    }
    
    NSArray * friendsArray = [Client sharedClient].user.friends;
    
    NSMutableArray * indexArray = [[NSMutableArray alloc]init];
    NSMutableIndexSet *indexes = [NSMutableIndexSet new];
    
    for (NSDictionary*friendDict in friendsArray) {
        
        NSString *status = [NSString stringWithFormat:@"%@",[friendDict objectForKey:@"status"]];
        
        if ([status isEqualToString:@"1"]) {
            
            NSString *friendEmail = [NSString stringWithFormat:@"%@",[friendDict objectForKey:@"username"]];
            //int index = 0;
            
            for(int i=0;i<self.chatList.count;i++){
                //for (NSDictionary*chatDict in self.chatList) {
                NSDictionary*chatDict = self.chatList[i];
                NSString *chatEmail = [NSString stringWithFormat:@"%@",[chatDict objectForKey:@"username"]];
                
                if ([friendEmail isEqualToString:chatEmail]) {
                    [indexes addIndex:i];
                }
                    //index = index + 1;
            }
        }
    }
    
    [self.chatList removeObjectsAtIndexes:indexes];
    
    //[self.table setHidden:(self.chatList.count==0)];
    [self.tableViewOutlet reloadSections:[NSIndexSet indexSetWithIndex:0] withRowAnimation:UITableViewRowAnimationFade];
    [self changeTitleController];
    
}

-(void)changeTitleController{
    int counter = 0;
    for(unsigned i=0;i<self.chatList.count;i++){
        counter += [self findNotReaded:[self.chatList objectAtIndex:i]];
    }
    if(counter==0){
        //[self setTitle:NSLocalizedString(@"Chat", nil)];
        [[self.tabBarController.tabBar.items objectAtIndex:3] setBadgeValue:nil];
    }else{
       // [self setTitle:[NSString stringWithFormat:@"%@ %i",NSLocalizedString(@"Chat", nil),counter]];
        
        [[self.tabBarController.tabBar.items objectAtIndex:3] setBadgeValue:[NSString stringWithFormat:@"%d",counter]];
    }
    
    [[UIApplication sharedApplication] setApplicationIconBadgeNumber:counter];
    //self.title=@"";
    //[self.delegate updateTitle];
}

-(void)receiveCacheUpload:(NSNotification *)notification{
    [self controlIncoming];
}

- (IBAction)logBtnAction:(id)sender {
    
    CallLogsViewController * chatSearchVC = [self.storyboard instantiateViewControllerWithIdentifier:@"CallLogsViewController"];
    [self.navigationController pushViewController:chatSearchVC animated:YES];
    
}



@end
