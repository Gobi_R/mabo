     //
//  ProfileViewController.m
//  Mobo
//
//  Created by vishnuprasathg on 10/16/17.
//  Copyright © 2017 DCI. All rights reserved.
//

#import "ProfileViewController.h"
#import "UtilityGraphic.h"
#import "PrefixHeader.pch"
#import "Client.h"
#import "SharedVariables.h"
#import "MyPostViewController.h"
#import "Utilities.h"
#import "SettingsViewController.h"
//#import "UIImageView+AFNetworking.h"
#import <MapKit/MapKit.h>
#import <CoreLocation/CoreLocation.h>
#import "TabBarViewController.h"
#import "NotificationViewController.h"
#import "ChatRecentsViewController.h"
#import "SinchClient.h"
#import "UIImageView+WebCache.h"

@interface ProfileViewController ()<MKMapViewDelegate,CLLocationManagerDelegate>

@property (nonatomic, strong) NSMutableArray *arrayListTab;
@property (nonatomic, strong) NSDictionary *dictionaryUser;
@property (nonatomic, assign) BOOL isVisibility;
@property (nonatomic, assign) BOOL isFavorites;
@property (nonatomic, assign) BOOL isScrumble;

//@property (nonatomic, assign) BOOL iseditClicked;
@property (nonatomic, strong) NSDictionary *dictionaryMultiTextDelete;

@property BOOL isNotMe;
@property CLLocationCoordinate2D cordinateViewMap;
@property (nonatomic, strong) MKMapView *map;

@property (nonatomic, strong) NSMutableArray *chatList;



@end

@implementation ProfileViewController

{
    
//    BOOL isVisibility;
//    BOOL isFavorites;
//    BOOL isScrumble;
    
    BOOL iseditClicked;
    
    CLLocationManager *objLocationManager;
    double latitude_UserLocation, longitude_UserLocation;
    
    BOOL isGenderClicked;
    BOOL isDOBClicked;
    BOOL isIntrestedClicked;
    NSArray * genderArray ;
    NSArray * intrestedInArray ;
    
    NSMutableArray * intrestArray ;
    NSMutableArray * occupationsArray ;
    NSMutableArray * schoolsArray ;
    UITextField * activeField;
    NSIndexPath* indexPath;
    
    BOOL isInterested;
    BOOL isOccupation;
    BOOL isSchool;
}


- (void)viewDidLoad {
    [super viewDidLoad];
    
    //[[SinchClient sharedClient] activeClient];
    
    // Do any additional setup after loading the view.
    NSLog(@"%@",[SharedVariables sharedInstance].fcmToken);
    //[self sendUpdate:@{@"push_device_token":[SharedVariables sharedInstance].fcmToken} inView:_userNameTxtFieldOutlet];
    
    [SharedVariables sharedInstance].tabBarController = self.tabBarController;
    
    self.chatList = [self sortArray:[[[Client sharedClient] getMessagesList] mutableCopy]];
    
    [self changeTitleController];
    
    [[Utilities sharedInstance] showCustomPopUpForProfileScreen:self title:@"Mabo" message:@"Completing your profile will increase you being searched by others." actionResponse:@"profilePopUp"];
    
    _isVisibility = NO;
    _isFavorites = NO;
    _isScrumble = NO;
    
    self.userImageLblOutlet.layer.cornerRadius = self.userImageLblOutlet.frame.size.height / 2;
    self.userImageLblOutlet.clipsToBounds = YES;
    //self.userImageLblOutlet.layer.masksToBounds = YES;
    
//    yourImageView.layer.cornerRadius = yourRadius;
//    yourImageView.clipsToBounds = YES;
    
    self.map = [[MKMapView alloc] init];
    [self.map setDelegate:self];
    //
    
    NSLog(@"lat:%f",self.map.centerCoordinate.latitude);
    NSLog(@"lng:%f",self.map.centerCoordinate.longitude);
    
    
    objLocationManager = [[CLLocationManager alloc] init];
    objLocationManager.delegate = self;
    objLocationManager.distanceFilter = kCLDistanceFilterNone;
    objLocationManager.desiredAccuracy = kCLLocationAccuracyHundredMeters;
    if ([objLocationManager respondsToSelector:@selector(requestWhenInUseAuthorization)]) {
        [objLocationManager requestWhenInUseAuthorization];
    }
    [objLocationManager startUpdatingLocation];
    
    self.baseScrollView.delegate = self;
    intrestArray = [[NSMutableArray alloc] init];
    occupationsArray = [[NSMutableArray alloc] init];
    schoolsArray = [[NSMutableArray alloc] init];
    
    self.genderTableView.delegate = self;
    self.genderTableView.dataSource = self;
    
    self.interstedInTableView.delegate = self;
    self.interstedInTableView.dataSource = self;
    
    self.interestsTableView.delegate = self;
    self.interestsTableView.dataSource = self;
    
    self.occupationsTableView.delegate = self;
    self.occupationsTableView.dataSource = self;
    
    self.schoolsTableView.delegate = self;
    self.schoolsTableView.dataSource = self;
    
    genderArray = [[NSArray alloc] initWithObjects:@"Male",@"Female",@"Other",@"None", nil];
    intrestedInArray = [[NSArray alloc] initWithObjects:@"New friend",@"Nice date",@"Crazy night out",@"Business contact",@"Fun events",@"Whatever happens",@"None", nil];
    
    
     isGenderClicked = NO;
     isDOBClicked = NO;
     isIntrestedClicked = NO;
    
    [self authendicate];

//    self.dictionaryUser = [Client sharedClient].user.getUserInDictionary;
//
//    NSLog(@"UserDict:%@",self.dictionaryUser);
    
//    if([[Client sharedClient].reachAbility isReachable]){
//        [[Client sharedClient] getUserInfo:[Client sharedClient].user.email completition:^(NSDictionary *dictionary, NSError *error) {
//            self.dictionaryUser=dictionary;
//            NSLog(@"userDict:%@",self.dictionaryUser);
    
//            [[Client sharedClient] updateCacheInfoUserWithDictionary:self.dictionaryUser andUser:self.userNameTxtFieldOutlet.text];
//            //[self setNavigationControllerApparence];
//        }];
//    }
    
    self.genderTableViewHeightConstraint.constant = 0;
    self.interestedInTableViewHeightConstraint.constant = 0;
    self.datePickerHeightConstraint.constant = 0;
    
    self.dobTopConstraint.constant = 0;
    self.interestedInTopConstraint.constant = 0;
    self.incomeTopConstraint.constant = 0;
    
    self.customeDatePicker.datePickerMode = UIDatePickerModeDate;
    [self.customeDatePicker addTarget:self action:@selector(updateTextField:)
         forControlEvents:UIControlEventValueChanged];

    self.automaticallyAdjustsScrollViewInsets = NO;
    
    
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(keyboardWasShown:) name:UIKeyboardDidShowNotification object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(keyboardWillBeHidden:) name:UIKeyboardWillHideNotification object:nil];
    
}

-(void)authendicate
{
    
    [[Client sharedClient] getUserInfo:[Client sharedClient].user.email completition:^(NSDictionary *dictionary, NSError *error) {
        if(dictionary){
            [[Client sharedClient].user initWithDictionary:dictionary];
            [[Client sharedClient].user getUserInDictionary];
            
            self.dictionaryUser = dictionary;
            
            [self loadData];
            
            
            
            NSString *userName = [dictionary valueForKey:@"extended_name"];
            
            if (userName.length <= 0) {
                
                NSLog(@"SignUpDict:%@",[SharedVariables sharedInstance].signUpResponseDict);
                
                self.userNameTxtFieldOutlet.text = [[[[SharedVariables sharedInstance].signUpResponseDict valueForKey:@"data"] objectForKey:@"user"] objectForKey:@"username"];
            }
            else
            {
                self.userNameTxtFieldOutlet.text = [dictionary valueForKey:@"extended_name"];
            }
            
            NSLog(@"UserEmail:%@",[Client sharedClient].user.email);
        }
        
    }];

}


-(void)loadData
{
    self.userNameTxtFieldOutlet.userInteractionEnabled = NO;
    
    NSLog(@"Extended_name:%@",[self.dictionaryUser valueForKey:@"extended_name"]);
    
    NSString * location_visibility = [self.dictionaryUser valueForKey:@"location_visibility"];
    
    if ([[NSString stringWithFormat:@"%@", location_visibility]  isEqual: @"1"]) {
        
        self.userVisibleLblOutlet.text = @"public";
        
    } else if ([[NSString stringWithFormat:@"%@", location_visibility]  isEqual: @"2"]) {
        
        self.userVisibleLblOutlet.text = @"only favorites";
        
    } else if ([[NSString stringWithFormat:@"%@", location_visibility]  isEqual: @"0"]) {
        
        self.userVisibleLblOutlet.text = @"no one";
        
    }
    
    if ([[NSString stringWithFormat:@"%@", location_visibility]  isEqual: @"1"]) {
        
        [self.visibilityBtnOutlet setImage:[UIImage imageNamed:@"toggle_on.png"] forState:UIControlStateNormal];
        [self.onlyWithFavouritesBtnOutlet setImage:[UIImage imageNamed:@"toggle_off.png"] forState:UIControlStateNormal];
        [self.scrumbleLocationBtnOutlet setImage:[UIImage imageNamed:@"toggle_off.png"] forState:UIControlStateNormal];
        
        _isVisibility = YES;
        _isFavorites = NO;
        _isScrumble = NO;
    }
    else if ([[NSString stringWithFormat:@"%@", location_visibility]  isEqual: @"2"]) {
        
        [self.visibilityBtnOutlet setImage:[UIImage imageNamed:@"toggle_on.png"] forState:UIControlStateNormal];
        [self.onlyWithFavouritesBtnOutlet setImage:[UIImage imageNamed:@"toggle_on.png"] forState:UIControlStateNormal];
        
        _isVisibility = YES;
        _isFavorites = YES;
        
    } else if ([[NSString stringWithFormat:@"%@", location_visibility]  isEqual: @"0"]) {
        
        [self.visibilityBtnOutlet setImage:[UIImage imageNamed:@"toggle_off.png"] forState:UIControlStateNormal];
        [self.onlyWithFavouritesBtnOutlet setImage:[UIImage imageNamed:@"toggle_off.png"] forState:UIControlStateNormal];
        
        self.visibilityBtnOutlet.enabled = YES;
        self.onlyWithFavouritesBtnOutlet.enabled = NO;
        self.scrumbleLocationBtnOutlet.enabled = NO;
        
        _isVisibility = NO;
        _isFavorites = NO;
        
    }
    
    NSString * is_scrumble = [self.dictionaryUser valueForKey:@"is_scrumble"];
    
    if ([[NSString stringWithFormat:@"%@", is_scrumble]  isEqual: @"1"]) {
        
        [self.scrumbleLocationBtnOutlet setImage:[UIImage imageNamed:@"toggle_on.png"] forState:UIControlStateNormal];
        
        _isScrumble = YES;
    }
    else
    {
        
        [self.scrumbleLocationBtnOutlet setImage:[UIImage imageNamed:@"toggle_off.png"] forState:UIControlStateNormal];
        
        _isScrumble = NO;
    }
    
    NSString *resultLocationStatusText=(_isFavorites == YES)?NSLocalizedString(@"Status: only favorites", nil):NSLocalizedString(@"Status: public", nil);
    
    
    if(!_isVisibility == YES){
        //self.isHiddenOnMap=YES;
        resultLocationStatusText=NSLocalizedString(@"Hidden", nil);
    }
    
    BOOL resultScrumbleStatusText = (_isScrumble == YES)?YES:NO;
    
    [self.userVisibleLblOutlet setText:[NSString stringWithFormat:@"%@",resultLocationStatusText]];
    if(resultScrumbleStatusText && _isVisibility == YES && _isScrumble == YES){
        [self.userVisibleLblOutlet setText:[NSString stringWithFormat:@"%@ %@",resultLocationStatusText,NSLocalizedString(@"(scrumble)", nil)]];
    }
    
    
    NSString * sex = [self.dictionaryUser valueForKey:@"sex"];
    
    if ([[NSString stringWithFormat:@"%@", sex]  isEqual: @"-1"]) {
        
        self.genderSelectedLblOutlet.text = @"None";
    }
    else
    {
        
        NSInteger sex_Int = [sex integerValue];
        
        self.genderSelectedLblOutlet.text = [genderArray objectAtIndex:sex_Int];
        
    }
    NSString * income = [self.dictionaryUser valueForKey:@"income"];
    
    
    NSString * interested_in = [self.dictionaryUser valueForKey:@"interested_in"];
    
    if ([[NSString stringWithFormat:@"%@", interested_in]  isEqual: @"-1"]) {
        
        self.intrestedInLblOutlet.text = @"None";
    }
    else
    {
        
        NSInteger interested_in_Int = [interested_in integerValue];
        
        self.intrestedInLblOutlet.text = [intrestedInArray objectAtIndex:interested_in_Int];
        
    }
    
    NSArray * interests = [self.dictionaryUser valueForKey:@"interests"];
    intrestArray = interests.mutableCopy;
    [self.interestsTableView reloadData];
    
    if (intrestArray.count >0) {
        
        self.interestsTableViewHeightConstraint.constant = self.interestsTableView.contentSize.height;
        self.interstsBaseViewHeightConstraint.constant = 56 + self.interestsTableView.contentSize.height;
        self.interetTxtFieldOtlet.hidden = YES;
        
    }
    else
    {
        self.interestsTableViewHeightConstraint.constant = 0;
        self.interstsBaseViewHeightConstraint.constant = 56;
        self.interetTxtFieldOtlet.hidden = YES;
        
        
        
    }
    
    NSArray * occupations = [self.dictionaryUser valueForKey:@"occupations"];
    
    occupationsArray = occupations.mutableCopy;
    [self.occupationsTableView reloadData];
    
    if (occupationsArray.count >0) {
        
        self.occupationsTableViewHeightConstraint.constant = self.occupationsTableView.contentSize.height;
        self.occupationsBaseViewHeightConstraint.constant = 56 + self.occupationsTableView.contentSize.height;
        self.occupationsTxtFieldOutlet.hidden = YES;
    }
    else
    {
        self.occupationsTableViewHeightConstraint.constant = 0;
        self.occupationsBaseViewHeightConstraint.constant = 56;
        self.occupationsTxtFieldOutlet.hidden = YES;
        
        
    }
    
    NSArray * schools = [self.dictionaryUser valueForKey:@"schools"];
    
    schoolsArray = schools.mutableCopy;
    [self.schoolsTableView reloadData];
    
    if (schoolsArray.count >0) {
        
        self.schoolsTableViewHeightConstraint.constant = self.schoolsTableView.contentSize.height;
        self.schoolsBaseViewHeightConstraint.constant = 56 + self.schoolsTableView.contentSize.height;
        self.schoolsTxtFieldOutlet.hidden = YES;
    }
    else
    {
        self.schoolsTableViewHeightConstraint.constant = 0;
        self.schoolsBaseViewHeightConstraint.constant = 56;
        self.schoolsTxtFieldOutlet.hidden = YES;
    }
    
    
    
    NSArray * profilePic = [self.dictionaryUser valueForKey:@"pictures"];
    
    if (profilePic.count >0) {
        
        
        dispatch_queue_t queue = dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0ul);
        dispatch_async(queue, ^{
            
            NSDictionary * picFirstDict = profilePic.firstObject;
            
            NSString * profilePicPath = [picFirstDict valueForKey:@"web_path"];
            
            NSString * urlstring = [NSString stringWithFormat:@"http://34.230.105.103:8002/%@", profilePicPath];
            NSURL *imageURL = [NSURL URLWithString:urlstring];
            NSData *imageData = [NSData dataWithContentsOfURL:imageURL];
            UIImage *image = [UIImage imageWithData:imageData];
            
            dispatch_sync(dispatch_get_main_queue(), ^{
                
                [self.userImageLblOutlet setImageWithURL:imageURL placeholderImage:[UIImage imageNamed:@"avatar2.png"]];
                
            });
        });
        
        
    }
    else
    {
        self.userImageLblOutlet.image = [UIImage imageNamed:@"avatar2.png"];
    }
    
    self.userImageLblOutlet.userInteractionEnabled = YES;
    
    NSLog(@"imageFrame:%@",self.userImageLblOutlet);
    
    //        self.userImageLblOutlet.layer.cornerRadius = 20;
    //        self.userImageLblOutlet.layer.masksToBounds = YES;
    
    UITapGestureRecognizer *tapGesture = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(handleTap:)];
    tapGesture.delegate = self;
    tapGesture.numberOfTapsRequired = 1;
    [self.userImageLblOutlet addGestureRecognizer:tapGesture];
    
    
    NSCalendar *calendar = [[NSCalendar alloc] initWithCalendarIdentifier:NSGregorianCalendar];
    NSDate *currentDate = [NSDate date];
    NSDateComponents *comps = [[NSDateComponents alloc] init];
    [comps setYear:-18];
    NSDate *maxDate = [calendar dateByAddingComponents:comps toDate:currentDate options:0];
    //[comps setYear:18];
    //NSDate *minDate = [calendar dateByAddingComponents:comps toDate:currentDate options:0];
    
    [self.customeDatePicker setMaximumDate:maxDate];
    //[self.customeDatePicker setMinimumDate:minDate];
    
    
    NSString * agrString = [NSString stringWithFormat:@"%@",[self.dictionaryUser valueForKey:@"age"]];
    
    if ([agrString isEqualToString:@"-1"])
    {
        
        self.dobLblOutlet.text = @"None";
    }
    else
    {
        
        NSString * timeStampString = [self.dictionaryUser valueForKey:@"age"];
        NSTimeInterval _interval=[timeStampString doubleValue];
        NSDate *date = [NSDate dateWithTimeIntervalSince1970:_interval];
        NSDateFormatter *_formatter=[[NSDateFormatter alloc]init];
        [_formatter setDateFormat:@"dd MMMM yyyy"];
        NSString *_date=[_formatter stringFromDate:date];
        
        self.dobLblOutlet.text = _date;
        
    }
}

-(void)viewWillAppear:(BOOL)animated
{
    iseditClicked = NO;
    self.myProfileSegmentControlOutlet.selectedSegmentIndex = 0;
    
     isInterested = NO;
     isOccupation = NO;
     isSchool = NO;
    
//    ChatRecentsViewController * chatRecent = [[ChatRecentsViewController alloc] init];
//    [chatRecent receiveCacheUpload:nil];
    
//    [self sendUpdate:@{@"lat":[NSString stringWithFormat:@"1.2345"]} inView:_userNameTxtFieldOutlet];
//    
//    [self sendUpdate:@{@"lon":[NSString stringWithFormat:@"67.8912"]} inView:_userNameTxtFieldOutlet];
}
-(void)viewDidAppear:(BOOL)animated
{
    self.myProfileSegmentControlOutlet.selectedSegmentIndex = 0;
}
//-(void)viewWillLayoutSubviews
//{
//    _iseditClicked = NO;
//}


- (void)keyboardWasShown:(NSNotification *)aNotification

{// scroll to the text view
    NSDictionary* info = [aNotification userInfo];
    CGSize kbSize = [[info objectForKey:UIKeyboardFrameBeginUserInfoKey] CGRectValue].size;
    
    NSDictionary *info1  = aNotification.userInfo;
    NSValue      *value = info1[UIKeyboardFrameEndUserInfoKey];
    
    CGRect rawFrame      = [value CGRectValue];
    CGRect keyboardFrame = [self.view convertRect:rawFrame fromView:nil];
    
    NSLog(@"keyboardFrame: %@", NSStringFromCGRect(keyboardFrame));
    
    UIEdgeInsets contentInsets = UIEdgeInsetsMake(0.0, 0.0, kbSize.height, 0.0);
    self.baseScrollView.contentInset = contentInsets;
    self.baseScrollView.scrollIndicatorInsets = contentInsets;
    
    // If active text field is hidden by keyboard, scroll it so it's visible.
    // Your app might not need or want this behavior.
    CGRect aRect = self.view.frame;
    if (kbSize.height <= 0) {
        
        kbSize.height = keyboardFrame.size.height;
    }
    
    aRect.size.height -= kbSize.height;
    self.baseScrollView.scrollEnabled = YES;
    if (!CGRectContainsPoint(aRect, activeField.frame.origin) ) {
        [self.baseScrollView scrollRectToVisible:activeField.frame animated:YES];
    }
}

- (void)keyboardWillBeHidden:(NSNotification *)aNotification
{
    // scroll back..
    UIEdgeInsets contentInsets = UIEdgeInsetsZero;
    self.baseScrollView.contentInset = contentInsets;
    self.baseScrollView.scrollIndicatorInsets = contentInsets;
    //self.baseScrollViewOutlet.scrollEnabled = NO;
}


- (void)locationManager:(CLLocationManager *)manager
     didUpdateLocations:(NSArray *)locations __OSX_AVAILABLE_STARTING(__MAC_NA,__IPHONE_6_0)
{
    CLLocation *newLocation = [locations objectAtIndex:0];
    NSLog(@"location:%@",manager.location);
    [SharedVariables sharedInstance].myClLocation = manager.location;
    
    NSLog(@"location:%@",[SharedVariables sharedInstance].myClLocation);
    latitude_UserLocation = newLocation.coordinate.latitude;
    longitude_UserLocation = newLocation.coordinate.longitude;
    [objLocationManager stopUpdatingLocation];
    
    NSLog(@"lat:%f",latitude_UserLocation);
    NSLog(@"lng:%f",longitude_UserLocation);
    
    [SharedVariables sharedInstance].selectedLat = [NSString stringWithFormat:@"%f",latitude_UserLocation];
    [SharedVariables sharedInstance].selectedLon = [NSString stringWithFormat:@"%f",longitude_UserLocation];
    
    
//    [self sendUpdate:@{@"lat":[NSString stringWithFormat:@"%f",latitude_UserLocation]} inView:_userNameTxtFieldOutlet];
//    [self sendUpdate:@{@"lon":[NSString stringWithFormat:@"%f",longitude_UserLocation]} inView:_userNameTxtFieldOutlet];
    
    [self sendUpdate:@{@"lat":[NSString stringWithFormat:@"%f",latitude_UserLocation]} inView:_userNameTxtFieldOutlet];
    [self sendUpdate:@{@"lon":[NSString stringWithFormat:@"%f",longitude_UserLocation]} inView:_userNameTxtFieldOutlet];
    
    NSLog(@"UPDATElocation");
    
    [SharedVariables sharedInstance].lat = &(latitude_UserLocation);
    [SharedVariables sharedInstance].lon = &(longitude_UserLocation);
    
    NSLog(@"%f", *[SharedVariables sharedInstance].lat);
    NSLog(@"%f", *[SharedVariables sharedInstance].lon);
    
    [self getAddressFromLatLon:newLocation.coordinate.latitude withLongitude:newLocation.coordinate.longitude];
   
}

-(void)getAddressFromLatLon:(double)pdblLatitude withLongitude:(double)pdblLongitude
{
    CLGeocoder *ceo = [[CLGeocoder alloc]init];
    CLLocation *loc = [[CLLocation alloc]initWithLatitude:pdblLatitude longitude:pdblLongitude]; //insert your coordinates
    
    [ceo reverseGeocodeLocation:loc
              completionHandler:^(NSArray *placemarks, NSError *error) {
                  CLPlacemark *placemark = [placemarks objectAtIndex:0];
                  if (placemark) {
                      
                      
                      NSLog(@"placemark %@",placemark);
                      //String to hold address
                      NSString *locatedAt = [[placemark.addressDictionary valueForKey:@"FormattedAddressLines"] componentsJoinedByString:@", "];
                      NSLog(@"addressDictionary %@", placemark.addressDictionary);
                      
                      NSLog(@"placemark %@",placemark.region);
                      NSLog(@"placemark %@",placemark.country);  // Give Country Name
                      NSLog(@"placemark %@",placemark.locality); // Extract the city name
                      NSLog(@"location %@",placemark.name);
                      NSLog(@"location %@",placemark.ocean);
                      NSLog(@"location %@",placemark.postalCode);
                      NSLog(@"location %@",placemark.subLocality);
                      
                      NSLog(@"location %@",placemark.location);
                      //Print the location to console
                      NSLog(@"I am currently at %@",locatedAt);
                      
                      [SharedVariables sharedInstance].myCurrentAddress = locatedAt;
                  }
                  else {
                      NSLog(@"Could not locate");
                  }
              }
     ];
}


- (void)locationManager:(CLLocationManager *)manager
       didFailWithError:(NSError *)error
{
    [objLocationManager stopUpdatingLocation];
}

-(void)updateTextField:(UIDatePicker *)sender
{
    NSDateFormatter *formatter = [[NSDateFormatter alloc] init];
    [formatter setDateFormat:@"dd MMMM yyyy"];
    self.dobLblOutlet.text = [formatter stringFromDate:sender.date];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/
-(void)addArrowIconinView:(UIView *)view{
    CGSize sizeIcon = CGSizeMake(view.frame.size.height/2, view.frame.size.height/2);
    UIImageView *iconView = [[UIImageView alloc] initWithFrame:CGRectMake(view.frame.size.width-sizeIcon.width-10, (view.frame.size.height-sizeIcon.height)/2, sizeIcon.width, sizeIcon.height)];
    [iconView setBackgroundColor:[UIColor darkGrayColor]];
    [iconView.layer setCornerRadius:view.frame.size.height/2/4];
    [iconView.layer setBorderColor:[UIColor yellowColor].CGColor];
    [iconView.layer setBorderWidth:1];
    [iconView setClipsToBounds:YES];
    
//    UIImage *arrow = [UIImage imageNamed:@"arrowRight"];
//    arrow = [UtilityGraphic rotateUIImage:arrow clockwise:YES];
//    CGSize newSize = CGSizeMake(arrow.size.width*(iconView.frame.size.width/4)/arrow.size.height, iconView.frame.size.width/4);
//    arrow = [UtilityGraphic imageWithImage:arrow scaledToSize:newSize];
//    arrow = [arrow imageWithRenderingMode:UIImageRenderingModeAlwaysTemplate];
//    [iconView setContentMode:UIViewContentModeCenter];
//    [iconView setImage:arrow];
//    [iconView setTintColor:[UIColor yellowColor]];
//    [view addSubview:iconView];
}

- (IBAction)editBtnAction:(id)sender {
    
    //NSLog(@"edit:%@",iseditClicked);
    
    if (iseditClicked == NO) {
        
        [self.editBtnOutlet setImage:[UIImage imageNamed:@"ic_done.png"] forState:UIControlStateNormal];
        
        self.userNameTxtFieldOutlet.userInteractionEnabled =YES;
        
       // [[Utilities sharedInstance]showCustomAlertView:self.view title:@"Mabo" message:@"This will let you change your username."];
        
        [[Utilities sharedInstance] showCustomAlertView:self.view title:@"Mabo" message:@"This will let you change your username." controller:self reason:@"profileViewController"];
        

        iseditClicked = YES;
    }
    else
    {
        
        [self.editBtnOutlet setImage:[UIImage imageNamed:@"ic_edit.png"] forState:UIControlStateNormal];
        
        self.userNameTxtFieldOutlet.userInteractionEnabled =NO;

        iseditClicked = NO;
        
        [self sendUpdate:@{@"extended_name":_userNameTxtFieldOutlet.text} inView:_userNameTxtFieldOutlet];
    }

}

-(void)handleTap: (UITapGestureRecognizer *) recognizer{
    
    [[Utilities sharedInstance] showCustomCameraAlertView:self];
    
//    UIActionSheet *actionSheet = [[UIActionSheet alloc] initWithTitle:@"Please Select Your Option"delegate:self cancelButtonTitle:@"Cancel" destructiveButtonTitle:nil otherButtonTitles:@"Camera", @"Gallery",nil];
//    
//    [actionSheet showInView:self.view];

}

-(void)actionSheet:(UIActionSheet *)actionSheet didDismissWithButtonIndex:(NSInteger)buttonIndex{
    
    
    NSLog(@"From didDismissWithButtonIndex - Selected Option: %@", [actionSheet buttonTitleAtIndex:buttonIndex]);
    
    NSString*image=[actionSheet buttonTitleAtIndex:buttonIndex];
    
    if ([[actionSheet buttonTitleAtIndex:buttonIndex] isEqualToString:@"Camera"]) {
        
        if (![UIImagePickerController isSourceTypeAvailable:UIImagePickerControllerSourceTypeCamera])
        {
            UIAlertView* alert = [[UIAlertView alloc] initWithTitle:nil message:@"Device Camera Is Not Working" delegate:nil cancelButtonTitle:nil otherButtonTitles:@"OK", nil];
            [alert show];
            return;
        }
        else{
            
            UIImagePickerController *picker = [[UIImagePickerController alloc] init];
            picker.delegate = self;
            picker.allowsEditing = YES;
            picker.sourceType = UIImagePickerControllerSourceTypeCamera;
            
            [self presentViewController:picker animated:YES completion:NULL];
            
        }
    }
    
    else if ([[actionSheet buttonTitleAtIndex:buttonIndex] isEqualToString:@"Gallery"]){
        
        
        UIImagePickerController *pickerView = [[UIImagePickerController alloc] init];
        pickerView.allowsEditing = YES;
        pickerView.delegate = self;
        [pickerView setSourceType:UIImagePickerControllerSourceTypeSavedPhotosAlbum];
        [self presentViewController:pickerView animated:YES completion:nil];
        
    }
    
}

- (void)imagePickerController:(UIImagePickerController *)picker didFinishPickingMediaWithInfo:(NSDictionary *)info{
    UIImage* orginalImage = [info objectForKey:UIImagePickerControllerOriginalImage];
    
    self.userImageLblOutlet.image = orginalImage;
    
    int index = 0;
    
    NSString *file_name = [info objectForKey:UIImagePickerControllerMediaType];
    
    NSLog(@"name:%@",file_name);
    
    UIImage* currentImage = [UIImage imageNamed:file_name];
    
    [[Client sharedClient] updatePicture:orginalImage withIndex:index completition:^(NSDictionary *dictionaryPicture, NSError *error) {
        if(dictionaryPicture){
            [[Client sharedClient] getUserInfo:[Client sharedClient].user.email completition:^(NSDictionary *dictionary, NSError *error) {
                if(dictionary){
                    [[Client sharedClient].user initWithDictionary:dictionary];
//                    self.dictionaryUser=[[Client sharedClient].user getUserInDictionary];
//                    self.tempImageProfile=nil;
//                    [self refreshImages];
                }
            }];
        }else{
            UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"OOPS!" message:NSLocalizedString(@"Error to send image", nil) delegate:self cancelButtonTitle:@"OK" otherButtonTitles:nil];
            [alert show];
        }
        
    }];
    
    [self dismissViewControllerAnimated:YES completion:nil];
    
    
}

- (void)imagePickerControllerDidCancel:(UIImagePickerController *)picker {
    
    [picker dismissViewControllerAnimated:YES completion:NULL];
    
}

- (IBAction)myProfileSegmentControlAction:(id)sender {
    
    if(self.myProfileSegmentControlOutlet.selectedSegmentIndex==0){
        
    }
    else if(self.myProfileSegmentControlOutlet.selectedSegmentIndex==1){
        
        MyPostViewController * myPostVC = [self.storyboard instantiateViewControllerWithIdentifier:@"MyPostViewController"];
        [self.navigationController pushViewController:myPostVC animated:NO];
        
    }
}

- (IBAction)scrumbleInfoBtnAction:(id)sender {
    
   // [[Utilities sharedInstance]showCustomAlertView:self.view title:@"Mabo" message:@"Scrumble will NOT show your exact location, but with a 500m radius."];
    
    [[Utilities sharedInstance] showCustomAlertView:self.view title:@"Mabo" message:@"Scrumble will NOT show your exact location, but with a 500m radius." controller:self reason:@"profileViewController"];
}

- (IBAction)visibilityOnOffBtnAction:(id)sender {
    
    
    int resultLocationStatus = [[Client sharedClient].user.location_visibility intValue];
    int resultScrumbleStatus = (_isScrumble == YES)?1:0;
    
    
    if (_isVisibility == NO) {
        
        [self.visibilityBtnOutlet setImage:[UIImage imageNamed:@"toggle_on.png"] forState:UIControlStateNormal];
        
        _isVisibility = YES;
        
        [[Client sharedClient] activeLocation];
        [self.onlyWithFavouritesBtnOutlet setAlpha:1];
        [self.scrumbleLocationBtnOutlet setAlpha:1];
        self.onlyWithFavouritesBtnOutlet.enabled = YES;
        self.scrumbleLocationBtnOutlet.enabled = YES;
        [self.onlyWithFavouritesBtnOutlet setUserInteractionEnabled:YES];
        [self.scrumbleLocationBtnOutlet setUserInteractionEnabled:YES];
        resultLocationStatus=(_isFavorites == YES)?2:1;
    }
    else
    {
        
        [self.visibilityBtnOutlet setImage:[UIImage imageNamed:@"toggle_off.png"] forState:UIControlStateNormal];
        [self.onlyWithFavouritesBtnOutlet setImage:[UIImage imageNamed:@"toggle_off.png"] forState:UIControlStateNormal];
        [self.scrumbleLocationBtnOutlet setImage:[UIImage imageNamed:@"toggle_off.png"] forState:UIControlStateNormal];
        
        _isVisibility = NO;
        _isFavorites = NO;
        _isScrumble = NO;
        
        
        [self.onlyWithFavouritesBtnOutlet setAlpha:.5];
        [self.scrumbleLocationBtnOutlet setAlpha:.5];
        [self.onlyWithFavouritesBtnOutlet setUserInteractionEnabled:NO];
        [self.scrumbleLocationBtnOutlet setUserInteractionEnabled:NO];
        resultLocationStatus=0;
        resultScrumbleStatus=0;
    }
    
    
    NSString *resultLocationStatusText=(_isFavorites == YES)?NSLocalizedString(@"Status: only favorites", nil):NSLocalizedString(@"Status: public", nil);
    
    
    if(!_isVisibility == YES){
        //self.isHiddenOnMap=YES;
        resultLocationStatusText=NSLocalizedString(@"Status: no one", nil);
    }
    
    BOOL resultScrumbleStatusText = (_isScrumble == YES)?YES:NO;
    
    [self.userVisibleLblOutlet setText:[NSString stringWithFormat:@"%@",resultLocationStatusText]];
    if(resultScrumbleStatusText && _isVisibility == YES){
        [self.userVisibleLblOutlet setText:[NSString stringWithFormat:@"%@ %@",resultLocationStatusText,NSLocalizedString(@"with Scrumble", nil)]];
    }
    
    
    [self sendUpdate:@{@"location_visibility":[NSNumber numberWithInt:resultLocationStatus],
                       @"is_scrumble":[NSNumber numberWithInt:resultScrumbleStatus]} inView:sender];


    }

-(void)sendUpdate:(NSDictionary *)dictionary inView:(UIView *)updateView{
    NSLog(@"update");
    NSLog(@"updateDict:%@",dictionary);
    if(!self.isNotMe){
        [[Client sharedClient].user initWithDictionary:dictionary];
        
//        CGPoint center = CGPointMake(-20, updateView.frame.size.height/2);
//        ActivityView *activity = [[ActivityView alloc] initWithFrame:CGRectMake(0, 0, 20, 20)];
//        [activity setColorBase:COLOR_MB_GREEN];
//        [activity setColorCircles:COLOR_MB_LIGHTGRAY];
//        [activity setLineWidthCircles:1];
//        [updateView addSubview:activity];
//        [activity setCenterActivity:center];
//        [activity show];
        
        NSLog(@"userDict:%@",[Client sharedClient].user.getUserInDictionary);
        
        [[Client sharedClient] updateUserInfoWithCompletition:^(BOOL success, NSError *error) {
           // [activity hide];
            if(success){
                NSLog(@"update ok");
                [[Client sharedClient] getUserInfo:[Client sharedClient].user.email completition:^(NSDictionary *dictionary, NSError *error) {
                    if(dictionary){
                        [[Client sharedClient].user initWithDictionary:dictionary];
                        self.dictionaryUser=[[Client sharedClient].user getUserInDictionary];
                        //[self loadData];
//                        self.tempImageProfile=nil;
//                        [self refreshImages];
                    }
                }];
            }else if(error){
                NSLog(@"error update: %@",error.description);
                UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"OOPS!" message:NSLocalizedString(@"Error to send update", nil) delegate:self cancelButtonTitle:@"OK" otherButtonTitles:nil];
                [alert show];
               // [self populateContent];
            }
        }];
    }
}


- (IBAction)onlyWithFavoritesOnOffBtnAction:(id)sender {
    
    int resultLocationStatus = [[Client sharedClient].user.location_visibility intValue];
    int resultScrumbleStatus = (_isScrumble == YES)?1:0;
    
    if (_isFavorites == NO) {
        
        
        [self.onlyWithFavouritesBtnOutlet setImage:[UIImage imageNamed:@"toggle_on.png"] forState:UIControlStateNormal];
        
        _isFavorites = YES;
        
        resultLocationStatus=2;
    }
    else
    {
        
        [self.onlyWithFavouritesBtnOutlet setImage:[UIImage imageNamed:@"toggle_off.png"] forState:UIControlStateNormal];
        
        _isFavorites = NO;
        
        resultLocationStatus=1;
    }
    
    NSString *resultLocationStatusText=(_isFavorites == YES)?NSLocalizedString(@"Status: only favorites", nil):NSLocalizedString(@"Status: public", nil);
    
    
    if(!_isVisibility == YES){
        //self.isHiddenOnMap=YES;
        resultLocationStatusText=NSLocalizedString(@"Status: Hidden", nil);
    }
    
    BOOL resultScrumbleStatusText = (_isScrumble == YES)?YES:NO;
    
    [self.userVisibleLblOutlet setText:[NSString stringWithFormat:@"%@",resultLocationStatusText]];
    if(resultScrumbleStatusText && _isVisibility == YES){
        [self.userVisibleLblOutlet setText:[NSString stringWithFormat:@"%@ %@",resultLocationStatusText,NSLocalizedString(@"with Scrumble", nil)]];
    }
    
    
    [self sendUpdate:@{@"location_visibility":[NSNumber numberWithInt:resultLocationStatus],
                       @"is_scrumble":[NSNumber numberWithInt:resultScrumbleStatus]} inView:sender];

    

}

- (IBAction)scrumbleOnOffBtnAction:(id)sender {
    
    int resultLocationStatus = [[Client sharedClient].user.location_visibility intValue];
    int resultScrumbleStatus = (_isScrumble == YES)?1:0;

    
    if (_isScrumble == NO) {
        
        [self.scrumbleLocationBtnOutlet setImage:[UIImage imageNamed:@"toggle_on.png"] forState:UIControlStateNormal];
        
        _isScrumble = YES;
        
        
        resultScrumbleStatus = 1;
    }
    else
    {
       
        [self.scrumbleLocationBtnOutlet setImage:[UIImage imageNamed:@"toggle_off.png"] forState:UIControlStateNormal];
        
        _isScrumble = NO;
        
        resultScrumbleStatus = 0;
    }
    
    NSString *resultLocationStatusText=(_isFavorites == YES)?NSLocalizedString(@"Status: only favorites", nil):NSLocalizedString(@"Status: public", nil);
    
    
    if(!_isVisibility == YES){
        //self.isHiddenOnMap=YES;
        resultLocationStatusText=NSLocalizedString(@"Status: Hidden", nil);
    }
    
    BOOL resultScrumbleStatusText = (_isScrumble == YES)?YES:NO;
    
    [self.userVisibleLblOutlet setText:[NSString stringWithFormat:@"%@",resultLocationStatusText]];
    if(resultScrumbleStatusText && _isVisibility == YES && _isScrumble == YES){
        [self.userVisibleLblOutlet setText:[NSString stringWithFormat:@"%@ %@",resultLocationStatusText,NSLocalizedString(@"(scrumble)", nil)]];
    }
    
    
    [self sendUpdate:@{@"location_visibility":[NSNumber numberWithInt:resultLocationStatus],
                       @"is_scrumble":[NSNumber numberWithInt:resultScrumbleStatus]} inView:sender];
    
}

- (IBAction)genderDropdownBtnAction:(id)sender {
    
    if (isGenderClicked == NO) {
        
        [self.genderTableView reloadData];
        
        [UIView animateWithDuration:3.5f delay:0.0
                            options:UIKeyboardAnimationDurationUserInfoKey.floatValue+2.25f
                         animations:^{
                             
                             self.interestedInTableViewHeightConstraint.constant = 0;
                             self.datePickerHeightConstraint.constant = 0;
                             
                             self.genderTableViewHeightConstraint.constant = self.genderTableView.contentSize.height;
                             
                             self.dobTopConstraint.constant = self.genderTableView.contentSize.height;
                             self.interestedInTopConstraint.constant = 0;
                             self.incomeTopConstraint.constant = 0;
                             
                             
                         } completion:^(BOOL finished) {
                             
                             isGenderClicked = YES;
                             
                         }];

    }
    else
    {
        
        [UIView animateWithDuration:3.5f delay:0.0
                            options:UIKeyboardAnimationDurationUserInfoKey.floatValue+2.25f
                         animations:^{
                             
                             self.genderTableViewHeightConstraint.constant = 0;
                             
                             
                             self.dobTopConstraint.constant = 0;
                             self.interestedInTopConstraint.constant = 0;
                             self.incomeTopConstraint.constant = 0;
                             
                         } completion:^(BOOL finished) {
                             
                             isGenderClicked = NO;
                             
                         }];
    }
    
    
}

- (IBAction)dateOfbirthDropdownBtnAction:(id)sender {
    
    if (isDOBClicked == NO) {
        
        [UIView animateWithDuration:1.0f delay:0.0
                            options:UIKeyboardAnimationDurationUserInfoKey.floatValue+3.25f
                         animations:^{
                             
                             self.interestedInTableViewHeightConstraint.constant = 0;
                             self.genderTableViewHeightConstraint.constant = 0;
                             
                             self.datePickerHeightConstraint.constant = 140;
                             
                             self.dobTopConstraint.constant = 0;
                             self.interestedInTopConstraint.constant = 140;
                             self.incomeTopConstraint.constant = 0;
                             
                             
                         } completion:^(BOOL finished) {
                             
                             isDOBClicked = YES;
                             
                         }];
        
    }
    else
    {
        
        [UIView animateWithDuration:1.0f delay:0.0
                            options:UIKeyboardAnimationDurationUserInfoKey.floatValue+3.25f
                         animations:^{
                             
                             self.datePickerHeightConstraint.constant = 0;
                             
                             self.dobTopConstraint.constant = 0;
                             self.interestedInTopConstraint.constant = 0;
                             self.incomeTopConstraint.constant = 0;
                             
                             
                         } completion:^(BOOL finished) {
                             
                             isDOBClicked = NO;
                             
                             NSTimeInterval t = [_customeDatePicker.date timeIntervalSince1970];
                             
                             int value = (int)t / 1000;
                             
                             NSDateFormatter * dateFormatter = [[NSDateFormatter alloc] init] ;
                             [dateFormatter setDateFormat:@"yyyy-MM-dd"] ;
                             NSDate *date = _customeDatePicker.date;
                             NSLog(@"date=%@",date) ;
                             NSTimeInterval interval  = [date timeIntervalSince1970] ;
                             NSLog(@"interval=%f",interval) ;
                             NSDate *methodStart = [NSDate dateWithTimeIntervalSince1970:interval] ;
                             [dateFormatter setDateFormat:@"yyyy/MM/dd "] ;
                             NSLog(@"result: %@", [dateFormatter stringFromDate:methodStart]) ;
                             
//                             time_t unixTime = (time_t) [self.customeDatePicker.date timeIntervalSince1970];
//
//                             NSLog(@"el timestamp:: %lo", unixTime);
                             
                            // NSString *dateStr = @"Tue, 25 May 2010 12:53:58 +0000";
                             
                             // Convert string to date object
//                             NSDateFormatter *dateFormat = [[NSDateFormatter alloc] init];
//                             [dateFormat setDateFormat:@"LLL d, yyyy - HH:mm:ss zzz"];
//                             NSDate *date = [dateFormat dateFromString:dateStr];
                             
                            // double timeStamp = interval;
                             
                             [self sendUpdate:@{@"age":[NSString stringWithFormat:@"%f",interval]} inView:_customeDatePicker];
                             
                         }];
    }
    
}

- (IBAction)interestedInDropdownBtnAction:(id)sender {
    
    if (isIntrestedClicked == NO) {
        
        [self.interstedInTableView reloadData];
        
        [UIView animateWithDuration:0.5f delay:0.0
                            options:UIKeyboardAnimationDurationUserInfoKey.floatValue+1.25f
                         animations:^{
                             
                             self.datePickerHeightConstraint.constant = 0;
                             self.genderTableViewHeightConstraint.constant = 0;
                             
                             self.interestedInTableViewHeightConstraint.constant = self.interstedInTableView.contentSize.height;
                             
                             self.dobTopConstraint.constant = 0;
                             self.interestedInTopConstraint.constant = 0;
                             self.incomeTopConstraint.constant = self.interstedInTableView.contentSize.height;
                             
                             
                         } completion:^(BOOL finished) {
                             
                             isIntrestedClicked = YES;
                             
                         }];
        
    }
    else
    {
        
        [UIView animateWithDuration:0.5f delay:0.0
                            options:UIKeyboardAnimationDurationUserInfoKey.floatValue+1.25f
                         animations:^{
                             
                             self.interestedInTableViewHeightConstraint.constant = 0;
                             
                             self.dobTopConstraint.constant = 0;
                             self.interestedInTopConstraint.constant = 0;
                             self.incomeTopConstraint.constant = 0;
                             
                         } completion:^(BOOL finished) {
                             
                             isIntrestedClicked = NO;
                             
                         }];
    }
    
    
}

- (IBAction)interestsPlusBtnAction:(id)sender {
    
    self.interetTxtFieldOtlet.hidden = NO;
    UIView* tableViewBase = [sender superview];
    UITableViewCell* cell = (UITableViewCell*)[tableViewBase superview];
    
    if (isInterested == NO) {
        
        isInterested = YES;
        UIButton* plusBtn = [cell viewWithTag:10];
        
        UIImage *btnImage = [UIImage imageNamed:@"ic_done.png"];
        [plusBtn setImage:btnImage forState:UIControlStateNormal];
    }
    else
    {
        isInterested = NO;
        UIButton* plusBtn = [cell viewWithTag:10];
    
        UIImage *btnImage = [UIImage imageNamed:@"plus_ico.png"];
        [plusBtn setImage:btnImage forState:UIControlStateNormal];
        
        if (![self.interetTxtFieldOtlet.text isEqualToString:@""]) {
            
            [self updateMultiTextWithType:@"interests" withTitle:self.interetTxtFieldOtlet.text];
            
            self.interetTxtFieldOtlet.hidden = YES;
            
            
        }
        
    }

}

- (IBAction)occupationsPlusBtnAction:(id)sender {
    
    self.occupationsTxtFieldOutlet.hidden = NO;
    
    UIView* tableViewBase = [sender superview];
    UITableViewCell* cell = (UITableViewCell*)[tableViewBase superview];
    
    if (isOccupation == NO) {
        
        isOccupation = YES;
        UIButton* plusBtn = [cell viewWithTag:11];
        
        UIImage *btnImage = [UIImage imageNamed:@"ic_done.png"];
        [plusBtn setImage:btnImage forState:UIControlStateNormal];
    }
    else
    {
        isOccupation = NO;
        UIButton* plusBtn = [cell viewWithTag:11];
        
        
        
        UIImage *btnImage = [UIImage imageNamed:@"plus_ico.png"];
        [plusBtn setImage:btnImage forState:UIControlStateNormal];
        
        if (![self.occupationsTxtFieldOutlet.text isEqualToString:@""]) {
            
            [self updateMultiTextWithType:@"occupations" withTitle:self.occupationsTxtFieldOutlet.text];
            
            self.occupationsTxtFieldOutlet.hidden = YES;
            

            
        }
        
    }
}

- (IBAction)schoolsPlusBtnAction:(id)sender {
    
    self.schoolsTxtFieldOutlet.hidden = NO;
    
    UIView* tableViewBase = [sender superview];
    UITableViewCell* cell = (UITableViewCell*)[tableViewBase superview];
    
    if (isSchool == NO) {
        
        isSchool = YES;
        UIButton* plusBtn = [cell viewWithTag:12];
        
        UIImage *btnImage = [UIImage imageNamed:@"ic_done.png"];
        [plusBtn setImage:btnImage forState:UIControlStateNormal];
        
    }
    else
    {
        isSchool = NO;
        UIButton* plusBtn = [cell viewWithTag:12];
        
        
        
        UIImage *btnImage = [UIImage imageNamed:@"plus_ico.png"];
        [plusBtn setImage:btnImage forState:UIControlStateNormal];
        
        if (![self.schoolsTxtFieldOutlet.text isEqualToString:@""]) {
            
            [self updateMultiTextWithType:@"schools" withTitle:self.schoolsTxtFieldOutlet.text];
            
            self.schoolsTxtFieldOutlet.hidden = YES;
            
        }
    }
}

-(void)updateUi:(NSString *)type
{
    if ([type isEqualToString:@"interests"]) {
        
        
        if (self.interetTxtFieldOtlet.text.length >= 3) {
            
            [intrestArray addObject:self.interetTxtFieldOtlet.text];
            [self.interestsTableView reloadData];
            
            self.interetTxtFieldOtlet.text = @"";
            
            self.interestsTableViewHeightConstraint.constant = self.interestsTableView.contentSize.height;
            self.interstsBaseViewHeightConstraint.constant = 56 + self.interestsTableView.contentSize.height;
        }
        
    }
    else if ([type isEqualToString:@"occupations"]) {
        
        if (self.occupationsTxtFieldOutlet.text.length >= 3) {
            
            [occupationsArray addObject:self.occupationsTxtFieldOutlet.text];
            [self.occupationsTableView reloadData];
            
            self.occupationsTxtFieldOutlet.text = @"";
            
            self.occupationsTableViewHeightConstraint.constant = self.occupationsTableView.contentSize.height;
            self.occupationsBaseViewHeightConstraint.constant = 56 + self.occupationsTableView.contentSize.height;
        }
        
    }
    else if ([type isEqualToString:@"schools"]) {
        
        if (self.schoolsTxtFieldOutlet.text.length >= 3) {
            
            [schoolsArray addObject:self.schoolsTxtFieldOutlet.text];
            [self.schoolsTableView reloadData];
            
            self.schoolsTxtFieldOutlet.text = @"";
            
            self.schoolsTableViewHeightConstraint.constant = self.schoolsTableView.contentSize.height;
            self.schoolsBaseViewHeightConstraint.constant = 56 + self.schoolsTableView.contentSize.height;
        }
        
    }
}

- (IBAction)interestsCloseBtnAction:(id)sender {
    
    UIView* tableViewBase = [sender superview];
    UITableViewCell* cell = (UITableViewCell*)[tableViewBase superview];
    indexPath = [self.interestsTableView indexPathForCell:cell];
    
    NSString * value = [intrestArray objectAtIndex:indexPath.row];
    
    [self deleteMultiTextWithType:@"interests" withTitle:value];
    
}

- (IBAction)occupationsCloseBtnAction:(id)sender {
    
    UIView* tableViewBase = [sender superview];
    UITableViewCell* cell = (UITableViewCell*)[tableViewBase superview];
    indexPath = [self.occupationsTableView indexPathForCell:cell];
    
    NSString * value = [occupationsArray objectAtIndex:indexPath.row];
    
    [self deleteMultiTextWithType:@"occupations" withTitle:value];
    
    
}

- (IBAction)schoolsCloseBtnAction:(id)sender {
    
    UIView* tableViewBase = [sender superview];
    UITableViewCell* cell = (UITableViewCell*)[tableViewBase superview];
    indexPath = [self.schoolsTableView indexPathForCell:cell];
    
    NSString * value = [schoolsArray objectAtIndex:indexPath.row];
    
    [self deleteMultiTextWithType:@"schools" withTitle:value];
    
    

}

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 1;    //count of section
}


- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    
    if (tableView == self.genderTableView) {
        
        return genderArray.count;
    }
    else if (tableView == self.interstedInTableView)
    {
      return intrestedInArray.count;
    }
    else if (tableView == self.interestsTableView)
    {
        return intrestArray.count;
    }
    else if (tableView == self.occupationsTableView)
    {
        return occupationsArray.count;
    }
    else if (tableView == self.schoolsTableView)
    {
        return schoolsArray.count;
    }
    else
    {
        return 1;
    }
    
        //count number of row from counting array hear cataGorry is An Array
}



- (UITableViewCell *)tableView:(UITableView *)tableView
         cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    UITableViewCell *cell = [[UITableViewCell alloc] init];
    if (tableView == self.genderTableView) {
        
        cell = (UITableViewCell *) [tableView dequeueReusableCellWithIdentifier:@"CellGender"];
        
        UILabel *lbldescription = (UILabel *)[cell viewWithTag:1];
        lbldescription.text = [genderArray objectAtIndex:indexPath.row];
    }
    else if (tableView == self.interstedInTableView)
    {
        cell = (UITableViewCell *) [tableView dequeueReusableCellWithIdentifier:@"CellIntrested"];
        UILabel *lbldescription = (UILabel *)[cell viewWithTag:1];
        lbldescription.text = [intrestedInArray objectAtIndex:indexPath.row];
        
        
    }
    
    else if (tableView == self.interestsTableView)
    {
        cell = (UITableViewCell *) [tableView dequeueReusableCellWithIdentifier:@"CellInterest"];
        UILabel *lbldescription = (UILabel *)[cell viewWithTag:1];
        
        if (intrestArray.count >0) {
            
            lbldescription.text = [intrestArray objectAtIndex:indexPath.row];
        }
    
    }
    else if (tableView == self.occupationsTableView)
    {
        cell = (UITableViewCell *) [tableView dequeueReusableCellWithIdentifier:@"CellOccupations"];
        UILabel *lbldescription = (UILabel *)[cell viewWithTag:1];
        
        if (occupationsArray.count >0) {
            
            lbldescription.text = [occupationsArray objectAtIndex:indexPath.row];
        }
        
    }
    else if (tableView == self.schoolsTableView)
    {
        cell = (UITableViewCell *) [tableView dequeueReusableCellWithIdentifier:@"CellSchools"];
        UILabel *lbldescription = (UILabel *)[cell viewWithTag:1];
        
        if (schoolsArray.count >0) {
            
            lbldescription.text = [schoolsArray objectAtIndex:indexPath.row];
        }
    }
    
    return cell;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    
    if (tableView == self.genderTableView) {
        
        self.genderSelectedLblOutlet.text = [genderArray objectAtIndex:indexPath.row];
        
        [UIView animateWithDuration:3.5f delay:0.0
                            options:UIKeyboardAnimationDurationUserInfoKey.floatValue+2.25f
                         animations:^{
                             
                             self.genderTableViewHeightConstraint.constant = 0;
                             
                             
                             self.dobTopConstraint.constant = 0;
                             self.interestedInTopConstraint.constant = 0;
                             self.incomeTopConstraint.constant = 0;
                             
                         } completion:^(BOOL finished) {
                             
                             isGenderClicked = NO;
                             
                           // NSNumber *value = [[NSNumber alloc] initWithInteger:indexPath.row];
                             
                             int value = indexPath.row;
                             
                             NSString* stingValue = [genderArray objectAtIndex:indexPath.row];
                             
                             if ([stingValue isEqualToString:@"None"]) {
                                 
                                 [self sendUpdate:@{@"sex":[NSNumber numberWithInt:-1]} inView:self.genderBtnOutlet];
                             }
                             else{
                             
                             [self sendUpdate:@{@"sex":[NSNumber numberWithInt:value]} inView:self.genderBtnOutlet];
                             
                             }
                         }];
    }
    else if (tableView == self.interstedInTableView)
    {
        
        self.intrestedInLblOutlet.text = [intrestedInArray objectAtIndex:indexPath.row];
        
        [UIView animateWithDuration:0.5f delay:0.0
                            options:UIKeyboardAnimationDurationUserInfoKey.floatValue+1.25f
                         animations:^{
                             
                             self.interestedInTableViewHeightConstraint.constant = 0;
                             
                             self.dobTopConstraint.constant = 0;
                             self.interestedInTopConstraint.constant = 0;
                             self.incomeTopConstraint.constant = 0;
                             
                         } completion:^(BOOL finished) {
                             
                             isIntrestedClicked = NO;
                             
                             
                             int value = indexPath.row;
                             
                             NSString* stingValue = [intrestedInArray objectAtIndex:indexPath.row];
                             if ([stingValue isEqualToString:@"None"]) {
                                 
                                 [self sendUpdate:@{@"interested_in":[NSNumber numberWithInt:-1]} inView:self.interestedInBtnOutlet];
                             }
                             else{
                                 
                                 [self sendUpdate:@{@"interested_in":[NSNumber numberWithInt:value]} inView:self.interestedInBtnOutlet];
                                 
                             }

                            
                             
                             
                             
                         }];

    }
}

- (void)textFieldDidBeginEditing:(UITextField *)textField {
    
    activeField = textField;
    
  
}
- (void)textFieldDidEndEditing:(UITextField *)textField {
    activeField = nil;

}

-(BOOL) textFieldShouldReturn: (UITextField *) textField
{
    [textField resignFirstResponder];
    
    if (textField == self.interetTxtFieldOtlet) {
        
//        if (![textField.text isEqualToString:@""]) {
//
//            [self updateMultiTextWithType:@"interests" withTitle:textField.text];
//
//            self.interetTxtFieldOtlet.hidden = YES;
//
//            if (textField.text.length >= 3) {
//
//                [intrestArray addObject:textField.text];
//                [self.interestsTableView reloadData];
//
//                self.interestsTableViewHeightConstraint.constant = self.interestsTableView.contentSize.height;
//                self.interstsBaseViewHeightConstraint.constant = 56 + self.interestsTableView.contentSize.height;
//            }
//
//
//        }
    
    }
    else if (textField == self.occupationsTxtFieldOutlet) {
        
//        if (![textField.text isEqualToString:@""]) {
//
//            [self updateMultiTextWithType:@"occupations" withTitle:textField.text];
//
//            self.occupationsTxtFieldOutlet.hidden = YES;
//
//            if (textField.text.length >= 3) {
//
//            [occupationsArray addObject:textField.text];
//            [self.occupationsTableView reloadData];
//
//            self.occupationsTableViewHeightConstraint.constant = self.occupationsTableView.contentSize.height;
//            self.occupationsBaseViewHeightConstraint.constant = 56 + self.occupationsTableView.contentSize.height;
//
//            }
//        }
        
    }
    
    else if (textField == self.schoolsTxtFieldOutlet) {
        
//        if (![textField.text isEqualToString:@""]) {
//
//            [self updateMultiTextWithType:@"schools" withTitle:textField.text];
//
//            self.schoolsTxtFieldOutlet.hidden = YES;
//
//            if (textField.text.length >= 3) {
//
//            [schoolsArray addObject:textField.text];
//            [self.schoolsTableView reloadData];
//
//            self.schoolsTableViewHeightConstraint.constant = self.schoolsTableView.contentSize.height;
//            self.schoolsBaseViewHeightConstraint.constant = 56 + self.schoolsTableView.contentSize.height;
//
//            }
//        }

    }
    
//    if (textField == self.userNameTxtFieldOutlet) {
//
//    }
//    else
//    {
//       textField.text = @"";
//    }
    
    return YES;
}
//- (void)scrollViewDidScroll:(UIScrollView *)scrollView
//{
//
//    [self.userNameTxtFieldOutlet resignFirstResponder];
//    [self.interetTxtFieldOtlet resignFirstResponder];
//    [self.occupationsTxtFieldOutlet resignFirstResponder];
//    [self.schoolsTxtFieldOutlet resignFirstResponder];
//    
//}
-(void)deletingTitleMultiText{
    NSString *title = [self.dictionaryMultiTextDelete objectForKey:@"title"];
    NSString *type = [self.dictionaryMultiTextDelete objectForKey:@"type"];
    if(![title isEqualToString:@""] && title){
        NSMutableArray *arrayCopy = [[Client sharedClient].user.interests mutableCopy];
        [arrayCopy removeObject:title];
        NSDictionary *dictionary = @{type:arrayCopy};
        [[Client sharedClient].user initWithDictionary:dictionary];
        
        /*
         CGPoint center = CGPointMake(-20, updateView.frame.size.height/2);
         HKActivityView *activity = [[HKActivityView alloc] initWithFrame:CGRectMake(0, 0, 10, 10)];
         [activity setColorBase:HK_GREEN];
         [activity setTintBar:[UIColor whiteColor]];
         [activity setLineWidthCircle:1];
         [updateView addSubview:activity];
         [activity setCenter:center];
         [activity show];
         */
        
        [[Client sharedClient] deleteMultiTextWithType:type withTitle:title completition:^(NSDictionary *dictionary, NSError *error) {
            if(dictionary){
                BOOL success = [[dictionary objectForKey:@"success"] boolValue];
                if(success){
                    [[Client sharedClient] getUserInfo:[Client sharedClient].user.email completition:^(NSDictionary *dictionary, NSError *error) {
                        if(dictionary){
                            [[Client sharedClient].user initWithDictionary:dictionary];
                            self.dictionaryUser=[[Client sharedClient].user getUserInDictionary];
                           // [self refreshMultyText];
                            
                            if ([type isEqualToString:@"interests"]) {
                                
                                [intrestArray removeObjectAtIndex:indexPath.row];
                                
                                [self.interestsTableView reloadData];
                                
                                if (intrestArray.count <= 0) {
                                    
                                    self.interestsTableViewHeightConstraint.constant = 0;
                                    self.interstsBaseViewHeightConstraint.constant = 56;
                                }
                                else
                                {
                                    self.interestsTableViewHeightConstraint.constant = self.interestsTableView.contentSize.height;
                                    self.interstsBaseViewHeightConstraint.constant = 56 + self.interestsTableView.contentSize.height;
                                    
                                }
                            }
                            else if ([type isEqualToString:@"occupations"]) {
                                
                                [occupationsArray removeObjectAtIndex:indexPath.row];
                                
                                [self.occupationsTableView reloadData];
                                
                                if (occupationsArray.count <= 0) {
                                    
                                    self.occupationsTableViewHeightConstraint.constant = 0;
                                    self.occupationsBaseViewHeightConstraint.constant = 56;
                                }
                                else
                                {
                                    
                                    self.occupationsTableViewHeightConstraint.constant = self.occupationsTableView.contentSize.height;
                                    self.occupationsBaseViewHeightConstraint.constant = 56 + self.occupationsTableView.contentSize.height;
                                    
                                }
                            }
                            else if ([type isEqualToString:@"schools"]) {
                                
                                [schoolsArray removeObjectAtIndex:indexPath.row];
                                
                                [self.schoolsTableView reloadData];
                                
                                if (schoolsArray.count <= 0) {
                                    
                                    self.schoolsTableViewHeightConstraint.constant = 0;
                                    self.schoolsBaseViewHeightConstraint.constant = 56;
                                }
                                else
                                {
                                    self.schoolsTableViewHeightConstraint.constant = self.schoolsTableView.contentSize.height;
                                    self.schoolsBaseViewHeightConstraint.constant = 56 + self.schoolsTableView.contentSize.height;
                                    
                                }
                            }
                        }
                    }];
                }else{
                    UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"OOPS!" message:[dictionary objectForKey:@"errorString"] delegate:self cancelButtonTitle:@"OK" otherButtonTitles:nil];
                    [alert show];
                    //[self populateContent];
                }
            }else{
                UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"OOPS!" message:NSLocalizedString(@"Error to send update", nil) delegate:self cancelButtonTitle:@"OK" otherButtonTitles:nil];
                [alert show];
               // [self populateContent];
            }
        }];
    }
    
}

-(void)updateMultiTextWithType:(NSString *)type withTitle:(NSString *)title{
    if(![title isEqualToString:@""]){
        
        NSMutableArray *arrayCopy = [[Client sharedClient].user.interests mutableCopy];
        [arrayCopy addObject:title];
        NSDictionary *dictionary = @{type:arrayCopy};
        [[Client sharedClient].user initWithDictionary:dictionary];
        
        /*
         CGPoint center = CGPointMake(-20, updateView.frame.size.height/2);
         HKActivityView *activity = [[HKActivityView alloc] initWithFrame:CGRectMake(0, 0, 10, 10)];
         [activity setColorBase:HK_GREEN];
         [activity setTintBar:[UIColor whiteColor]];
         [activity setLineWidthCircle:1];
         [updateView addSubview:activity];
         [activity setCenter:center];
         [activity show];
         */
        
        [[Client sharedClient] updateMultiTextWithType:type withTitle:title completition:^(NSDictionary *dictionary, NSError *error) {
            if(dictionary){
                BOOL success = [[dictionary objectForKey:@"success"] boolValue];
                if(success){
                    [[Client sharedClient] getUserInfo:[Client sharedClient].user.email completition:^(NSDictionary *dictionary, NSError *error) {
                        if(dictionary){
                            
                            [self updateUi:type];
                            
                            [[Client sharedClient].user initWithDictionary:dictionary];
                            self.dictionaryUser=[[Client sharedClient].user getUserInDictionary];
                          //  [self refreshMultyText];
                        }
                    }];
                }else{
                    UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Warning!" message:[dictionary objectForKey:@"errorString"] delegate:self cancelButtonTitle:@"OK" otherButtonTitles:nil];
                    [alert show];
                   // [self populateContent];
                }
            }else{
                UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Warning!" message:NSLocalizedString(@"Error to send update", nil) delegate:self cancelButtonTitle:@"OK" otherButtonTitles:nil];
                [alert show];
               // [self populateContent];
            }
        }];
    }
}

-(void)deleteMultiTextWithType:(NSString *)type withTitle:(NSString *)title{
    
    self.dictionaryMultiTextDelete=@{@"type":type,@"title":title};
    UIActionSheet *actionSheet = [[UIActionSheet alloc]
                                  initWithTitle:NSLocalizedString(@"Are you sure you want to delete this title?", nil)
                                  delegate:self
                                  cancelButtonTitle:NSLocalizedString(@"Cancel",nil)
                                  destructiveButtonTitle:nil
                                  otherButtonTitles:@"Delete Title", nil];
    [actionSheet setTag:101];
    [actionSheet showInView:self.view];
}

-(void)actionSheet:(UIActionSheet *)actionSheet clickedButtonAtIndex:(NSInteger)buttonIndex{
    
    switch (actionSheet.tag) {
        
        case 101:
            switch(buttonIndex){
                case 0:
                    [self deletingTitleMultiText];
                    break;
            }
            break;
    }
}

-(void)openCamera
{
    UIImagePickerController *picker = [[UIImagePickerController alloc] init];
    picker.delegate = self;
    picker.allowsEditing = YES;
    picker.sourceType = UIImagePickerControllerSourceTypeCamera;
    
    [self presentViewController:picker animated:YES completion:NULL];
}
-(void)openGallery
{
    UIImagePickerController *pickerView = [[UIImagePickerController alloc] init];
    pickerView.allowsEditing = YES;
    pickerView.delegate = self;
    [pickerView setSourceType:UIImagePickerControllerSourceTypeSavedPhotosAlbum];
    [self presentViewController:pickerView animated:YES completion:nil];
}


- (IBAction)settingBtnAction:(id)sender {
    
    SettingsViewController * settingVC = [self.storyboard instantiateViewControllerWithIdentifier:@"SettingsViewController"];
    [self.navigationController pushViewController:settingVC animated:YES];
    
}

- (IBAction)notificationBtnAction:(id)sender {
    
    NotificationViewController * notificationVC = [self.storyboard instantiateViewControllerWithIdentifier:@"NotificationViewController"];
    [self.navigationController pushViewController:notificationVC animated:YES];
}

-(void)later
{
    
    TabBarViewController * tabVC = self.parentViewController;
    NSLog(@"%@",self.parentViewController);
    
    [tabVC setSelectedIndex:0];
}
-(void)afterEditButtonPressed
{
    [self.userNameTxtFieldOutlet becomeFirstResponder];
}

-(void)changeTitleController{
    int counter = 0;
    for(unsigned i=0;i<self.chatList.count;i++){
        counter += [self findNotReaded:[self.chatList objectAtIndex:i]];
    }
    if(counter==0){
        //[self setTitle:NSLocalizedString(@"Chat", nil)];
        [[self.tabBarController.tabBar.items objectAtIndex:3] setBadgeValue:nil];
    }else{
        // [self setTitle:[NSString stringWithFormat:@"%@ %i",NSLocalizedString(@"Chat", nil),counter]];
        
        [[self.tabBarController.tabBar.items objectAtIndex:3] setBadgeValue:[NSString stringWithFormat:@"%d",counter]];
    }
    
    [[UIApplication sharedApplication] setApplicationIconBadgeNumber:counter];
    //self.title=@"";
    //[self.delegate updateTitle];
}

-(NSMutableArray *)sortArray:(NSMutableArray *)array{
    NSDateFormatter *formatter = [[NSDateFormatter alloc] init];
    [formatter setDateFormat:@"yyyy-MM-dd HH:mm:ss Z"];
    
    NSArray *arrayResult = [array sortedArrayUsingComparator:^NSComparisonResult(NSDictionary *obj1, NSDictionary *obj2) {
        
        NSString *data1=[NSString stringWithFormat:@"%@",[obj1 objectForKey:@"lastUpdate"]];
        NSString *data2=[NSString stringWithFormat:@"%@",[obj2 objectForKey:@"lastUpdate"]];
        
        NSDate *d1 = [formatter dateFromString:data1];
        NSDate *d2 = [formatter dateFromString:data2];
        return [d2 compare:d1];
    }];
    
    return  [arrayResult mutableCopy];
}

-(int)findNotReaded:(NSDictionary *)dictionary{
    int result=0;
    
    if([dictionary objectForKey:@"list"]){
        NSPredicate *predicate = [NSPredicate predicateWithFormat:@"(readed==%i)",0];
        NSArray *arrayResult = [[dictionary objectForKey:@"list"] filteredArrayUsingPredicate:predicate];
        result = (int)arrayResult.count;
    }else{
        NSString *username = [dictionary objectForKey:@"username"];
        NSPredicate *predicate = [NSPredicate predicateWithFormat:@"(username==%@)",username];
        NSArray *arrayUserResult = [[[Client sharedClient] getMessagesList] filteredArrayUsingPredicate:predicate];
        NSArray *arrayListMessageUser = [[arrayUserResult lastObject] objectForKey:@"list"];
        
        NSPredicate *predicateReaded = [NSPredicate predicateWithFormat:@"(readed==%i)",0];
        NSArray *arrayResultReaded = [arrayListMessageUser filteredArrayUsingPredicate:predicateReaded];
        
        result = (int)arrayResultReaded.count;
        
    }
    return result;
}


@end
