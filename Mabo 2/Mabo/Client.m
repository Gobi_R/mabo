 //
//  Client.m
//  Mabo
//
//  Created by Steewe MacBook Pro on 30/03/15.
//  Copyright (c) 2015 Steewe. All rights reserved.
//

#import "Client.h"
#import <CommonCrypto/CommonDigest.h>
#import "CocoaWSSE.h"
#import "AppDelegate.h"
//#import "NavigationController.h"
#import "SignInViewController.h"
#import "SinchClient.h"
#import "SharedVariables.h"
#import <FBSDKLoginKit/FBSDKLoginKit.h>
#import <FBSDKCoreKit/FBSDKCoreKit.h>
#import "SignInViewController.h"
#import "Utilities.h"
#import "UtilityGraphic.h"

@interface Client()

@property BOOL isFirstCreate;

@end

@implementation Client

//@synthesize arr;
static Client* _sharedClient = nil;

#define PATH_USER_CACHE @"user.plist"
#define PATH_CHAT_CACHE @"chat.plist"
#define SERVER @"http://34.230.105.103:8002/"
#define SERVER @"http://34.230.105.103:8002/"
//http://34.230.105.103:8002/api/users/{user_id}
#define URL_SALT [NSString stringWithFormat:@"%@api/",SERVER]
#define URL [NSString stringWithFormat:@"%@api/users/",SERVER]
#define URL_MYPOST [NSString stringWithFormat:@"%@api/wallmessages/",SERVER]
#define GET_SALT(email) [NSString stringWithFormat:@"users/%@/salt",email]
#define GET_USER_DETAIL(email) [NSString stringWithFormat:@"users/%@",email]
#define GET_LOGIN @"user/login"
#define GET_USER(email) [NSString stringWithFormat:@"users/%@",email]
#define CREATE_ACCOUNT @"signups"
#define PATCH_USER [NSString stringWithFormat:@"users/%@",self.user.email]
#define POST_PICTURE(index) [NSString stringWithFormat:@"users/%@/pictures/%@",self.user.email,index]
#define DELETE_PICTURE(index) [NSString stringWithFormat:@"users/%@/pictures/%@",self.user.email,index]
#define PATCH_LOCATION [NSString stringWithFormat:@"users/%@/location",self.user.email]
#define GET_AROUNDS(radius,lat,lon) [NSString stringWithFormat:@"users/%@/arounds/%@?lat=%f&lng=%f",self.user.email,radius,lat,lon]
#define PUT_FRIEND(user)[NSString stringWithFormat:@"users/%@/friends/%@",self.user.email,user]
#define GET_SEARCH [NSString stringWithFormat:@"users/%@/search",self.user.email]
#define PUT_WAKEUP(recipient)[NSString stringWithFormat:@"users/%@/wakeups/%@",self.user.email,recipient]
#define GET_PICTURE(email) [NSString stringWithFormat:@"users/%@/pictures/-1",email]

#define PUT_MULTI_TEXT(type,title) [NSString stringWithFormat:@"users/%@/%@/%@",self.user.email,type,title]
#define DELETE_ACCOUNT [NSString stringWithFormat:@"users/%@",self.user.email]

#define POST_WALLMESSAGE [NSString stringWithFormat:@"wallmessages/%@",self.user.email]
#define POST_EDIT_WALLMESSAGE [NSString stringWithFormat:@"wallmessages/"]
#define POST_PICTURE_WALLMESSAGE(wallMessageId) [NSString stringWithFormat:@"wallmessages/%@/pictures",wallMessageId]
#define POST_VIDEO_WALLMESSAGE(wallMessageId) [NSString stringWithFormat:@"wallmessages/%@/videos",wallMessageId]
#define GET_POSTS_AROUNDS(radius,lat,lon) [NSString stringWithFormat:@"wallmessages/%@/arounds/%@?lat=%f&lng=%f",self.user.email,radius,lat,lon]
#define GET_POSTS_AROUNDS_WITHOUT_RADIOUS(radius,lat,lon) [NSString stringWithFormat:@"wallmessages/%@/arounds/%@?lat=%d&lng=%d",self.user.email,radius,lat,lon]
#define DELETE_WALL(wall_message_id) [NSString stringWithFormat:@"wallmessages/%@",wall_message_id]
#define PATCH_WALL(wall_message_id) [NSString stringWithFormat:@"wallmessages/%@",wall_message_id]
#define PUT_PARTECIPATE(wall_message_id) [NSString stringWithFormat:@"wallmessages/%@/join",wall_message_id]
#define PUT_LIKE(wall_message_id) [NSString stringWithFormat:@"wallmessages/%@/like",wall_message_id]
#define PUT_COMMENT(wall_message_id) [NSString stringWithFormat:@"wallmessages/%@/comment",wall_message_id]
#define GET_WALLMESSAGE(wallMessageId) [NSString stringWithFormat:@"wallmessages/%@",wallMessageId]


+ (Client*)sharedClient{
    @synchronized([Client class]){
        if (!_sharedClient)
            _sharedClient = [[self alloc] init];
        return _sharedClient;
    }
    
    return nil;
}

+ (id)alloc{
    @synchronized([Client class]){
        NSAssert(_sharedClient == nil, @"Attempted to allocate a second instance of a singleton.");
        _sharedClient = [super alloc];
        return _sharedClient;
    }
    return nil;
}

#pragma mark - LOADER CONTENT

-(void)createOrLoadCacheChat{
    NSString *applicationDocumentsDir = [NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES) objectAtIndex:0];
    NSString *pathChatCache = [applicationDocumentsDir stringByAppendingPathComponent:PATH_CHAT_CACHE];
    NSDictionary *dictionaryCacheChat = [NSDictionary dictionaryWithContentsOfFile:pathChatCache];
    if(!dictionaryCacheChat){
        NSDictionary *newDictionaryCacheChat = [NSDictionary dictionary];
        [newDictionaryCacheChat writeToFile:pathChatCache atomically:NO];
    }
}

-(void)createOrLoadCacheUser{
    self.isFirstCreate=NO;
    self.sendingPhoto=[NSMutableArray arrayWithCapacity:0];
    //self.pictureInDownload=[NSMutableArray arrayWithCapacity:0];
    self.user = [[User alloc] init];
    NSString *applicationDocumentsDir = [NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES) objectAtIndex:0];
    NSString *pathUserCache = [applicationDocumentsDir stringByAppendingPathComponent:PATH_USER_CACHE];
    
    NSData *myData = [NSData dataWithContentsOfFile:pathUserCache];
    NSDictionary *dictionaryCacheUser = (NSDictionary*) [NSKeyedUnarchiver unarchiveObjectWithData:myData];
    
    NSMutableDictionary *dictionaryFilter = [NSMutableDictionary dictionaryWithDictionary:@{@"sex":@"",@"age":@"",@"interested_in":@"",@"income":@"",@"text":@""}];
    if(!dictionaryCacheUser){
        NSDictionary *newDictionaryCacheUser = [NSDictionary dictionary];
        NSData *dataFromDictionary = [NSKeyedArchiver archivedDataWithRootObject:newDictionaryCacheUser];
        [dataFromDictionary writeToFile:pathUserCache atomically:NO];
        //[newDictionaryCacheUser writeToFile:pathUserCache atomically:NO];
        [self.user initWithDictionary:@{@"email":@"",@"password":@"",@"pictures":@[],@"friends":@[],@"last_update":[NSNumber numberWithInt:0],@"platform":[NSNumber numberWithInt:1],@"picture_index_default":[NSNumber numberWithInt:0],@"filterMap":dictionaryFilter}];
    }else{
        
        if(![dictionaryCacheUser objectForKey:@"filterMap"] || [[dictionaryCacheUser objectForKey:@"filterMap"] isEqual:[NSNull null]] || ![[dictionaryCacheUser objectForKey:@"filterMap"] isKindOfClass:[NSMutableDictionary class]]){
            NSMutableDictionary *newDictionaryForMap = [NSMutableDictionary dictionaryWithDictionary:dictionaryCacheUser];
            [newDictionaryForMap setObject:dictionaryFilter forKey:@"filterMap"];
            dictionaryCacheUser = [NSDictionary dictionaryWithDictionary:newDictionaryForMap];
        }
        [self.user initWithDictionary:dictionaryCacheUser];
    }
}

#pragma mark - ACTIVE LOCATION

-(void)activeLocation{
    NSLog(@"lunch active location");
    self.locationManager = [[CLLocationManager alloc] init];
    [self.locationManager setDelegate:self];
    if([self.locationManager respondsToSelector:@selector(requestWhenInUseAuthorization)]){
        [self.locationManager requestWhenInUseAuthorization];
        [self.locationManager requestAlwaysAuthorization];
    }
    
    self.locationManager.desiredAccuracy = kCLLocationAccuracyBest;
    //[self.locationManager setActivityType:CLActivityTypeAutomotiveNavigation];
    self.locationManager.distanceFilter = 100.0f;
    [self.locationManager startMonitoringSignificantLocationChanges];
    [self.locationManager startUpdatingLocation];
}

#pragma mark - CHECK AND LOGIN

-(void)checkAuthenticateWithCompletition:(void (^)(BOOL success))result{
    NSLog(@"lat:%@",self.user.lat);
    NSLog(@"lon:%@",self.user.lon);
    
    if(![self.user.email isEqualToString:@""] && ![self.user.password isEqualToString:@""]){
        [self loginWithEmail:self.user.email withPassword:self.user.password passwordPlain:NO completition:^(BOOL success, NSError *error) {
            self.authenticate=success;
            result(self.authenticate);
        }];
        
    }else{
        self.authenticate=NO;
        result(self.authenticate);
    }
}

-(void)deleteAccountWithCompletition:(void (^)(BOOL success))result{
    NSString *callURL = [NSString stringWithFormat:@"%@%@",URL,DELETE_ACCOUNT];
    
    NSMutableURLRequest *request = [[NSMutableURLRequest alloc] initWithURL:[NSURL URLWithString:callURL]];
    [request setHTTPMethod:@"DELETE"];
    [request setValue:@"application/json" forHTTPHeaderField:@"Content-Type"];
    [request setValue:@"application/json" forHTTPHeaderField:@"Accept"];
    
    [NSURLConnection sendAsynchronousRequest:request queue:[NSOperationQueue currentQueue] completionHandler:^(NSURLResponse *response, NSData *data, NSError *error) {
        if(data){
            NSDictionary *json = [NSJSONSerialization JSONObjectWithData:data options:kNilOptions error:NULL];
            BOOL success = [[json objectForKey:@"success"] boolValue];
            result(success);
        }
        if(error){
            result(NO);
        }
    }];

}

-(void)logOut{
    
    NSString *applicationDocumentsDir = [NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES) objectAtIndex:0];
    NSString *pathUserCache = [applicationDocumentsDir stringByAppendingPathComponent:PATH_USER_CACHE];
    
    NSMutableDictionary *dictionaryFilter = [NSMutableDictionary dictionaryWithDictionary:@{@"sex":@"",@"age":@"",@"interested_in":@"",@"income":@"",@"text":@""}];
    NSDictionary *newDictionaryCacheUser = [NSDictionary dictionary];
    [newDictionaryCacheUser writeToFile:pathUserCache atomically:NO];
    
    [self.user initWithDictionary:@{@"email":@"",@"password":@"",@"pictures":@[],@"friends":@[],@"last_update":[NSNumber numberWithInt:0],@"platform":[NSNumber numberWithInt:1],@"picture_index_default":[NSNumber numberWithInt:0],@"filterMap":dictionaryFilter}];
    
    [newDictionaryCacheUser writeToFile:pathUserCache atomically:NO];
    
    [self.user resetUser];
    [self.user initWithDictionary:@{@"email":@"",@"password":@"",@"pictures":@[],@"friends":@[],@"last_update":[NSNumber numberWithInt:0],@"platform":[NSNumber numberWithInt:1],@"picture_index_default":[NSNumber numberWithInt:0],@"filterMap":dictionaryFilter}];
    
    [self.loginManager logOut];
    
//    AppDelegate *ad = [UIApplication sharedApplication].delegate;
//    [ad switchLogOutController];
}

-(void)loginWithEmail:(NSString *)email withPassword:(NSString *)password passwordPlain:(BOOL)isPlain completition:(void (^)(BOOL success, NSError *error))result{
    if(isPlain){
        [self getSalt:email completition:^(NSString *salt, NSError *error) {
            if(salt){
                [SharedVariables sharedInstance].signInSaltString = salt;
                
                NSDictionary *dictionary=@{@"email":email,
                                           @"salt":salt,
                                           @"password":[self getCryptWithPlainPassword:password andSalt:salt],
                                           @"last_update":[NSNumber numberWithInt:0]
                                           };
                
                NSLog(@"dict:%@",dictionary);
        
                [self.user initWithDictionary:dictionary];
                [self getLoginWithCompletition:^(BOOL success, NSError *error) {
                    result(success,error);
                }];
            }else{
                result(NO, error);
            }
        }];
    }else{
        [self getLoginWithCompletition:^(BOOL success, NSError *error) {
            result(success,error);
        }];
    }
}

-(void)createAccountWithDictionary:(NSDictionary *)dictionary completition:(void (^)(BOOL success, NSError *error))result{
    
    NSLog(@"dictionary: %@",dictionary);
    self.isFirstCreate=YES;
    [self.user resetUser];
    NSString *callURL = [NSString stringWithFormat:@"%@%@/manual",URL_SALT,CREATE_ACCOUNT];
    NSMutableURLRequest *request = [[NSMutableURLRequest alloc] initWithURL:[NSURL URLWithString:callURL]];
    [request setHTTPMethod:@"POST"];
    [request setValue:@"application/json" forHTTPHeaderField:@"Content-Type"];
    [request setValue:@"application/json" forHTTPHeaderField:@"Accept"];
    NSData* jsonData = [NSJSONSerialization dataWithJSONObject:dictionary options:0 error:nil];
    [request setHTTPBody:jsonData];
    NSLog(@"ciamo: %@",callURL);
    [NSURLConnection sendAsynchronousRequest:request queue:[NSOperationQueue currentQueue] completionHandler:^(NSURLResponse *response, NSData *data, NSError *error) {
        NSLog(@"respose: %@",response);
        if(data){
            NSDictionary *json = [NSJSONSerialization JSONObjectWithData:data options:kNilOptions error:NULL];
            NSLog(@"json: %@",json);
            [SharedVariables sharedInstance].signUpResponseDict = json;
            if(json){
                BOOL success = [[json objectForKey:@"success"] boolValue];
                NSError *MBError = [[json objectForKey:@"data"] objectForKey:@"errors"];
                if(success){
                    NSString *email = [[[json objectForKey:@"data"] objectForKey:@"user"] objectForKey:@"email"];
                    NSString *salt=[[[json objectForKey:@"data"] objectForKey:@"user"] objectForKey:@"salt"];
                    
                    NSString * password = [NSString stringWithFormat:@"%@",[dictionary objectForKey:@"plainPassword"]];
                        
                        NSString * pwd = [NSString stringWithFormat:@"%@",[[dictionary objectForKey:@"signup_form"]objectForKey:@"plainPassword"]];
                        password = [self getCryptWithPlainPassword:pwd andSalt:salt];

                    NSString *idUser = [[[json objectForKey:@"data"] objectForKey:@"user"] objectForKey:@"id"];
                    NSString *username = [[[json objectForKey:@"data"] objectForKey:@"user"] objectForKey:@"username"];
                    NSString *extendedName = [[[json objectForKey:@"data"] objectForKey:@"user"] objectForKey:@"extended_name"];
                    
                    NSDictionary *dictionary = @{@"email":email,
                                                 @"password":password,
                                                 @"username":username,
                                                 @"idUser":idUser,
                                                 @"salt":salt,
                                                 @"extended_name":extendedName};
                    [self.user initWithDictionary:dictionary];
                }
                else
                {
                    
                }
                result(success,MBError);
            }else{
                result(NO,error);
            }
            
        }
        if(error){
            NSLog(@"error: %@",error);
            result(NO,error);
        }
    }];
}

-(void)createAccountFbWithDictionary:(NSDictionary *)dictionary completition:(void (^)(BOOL success, NSError *error))result{
    self.isFirstCreate=YES;
    [self.user resetUser];
    NSString *callURL = [NSString stringWithFormat:@"%@%@/social",URL_SALT,CREATE_ACCOUNT];
    NSMutableURLRequest *request = [[NSMutableURLRequest alloc] initWithURL:[NSURL URLWithString:callURL]];
    [request setHTTPMethod:@"POST"];
    [request setValue:@"application/json" forHTTPHeaderField:@"Content-Type"];
    [request setValue:@"application/json" forHTTPHeaderField:@"Accept"];
    NSData* jsonData = [NSJSONSerialization dataWithJSONObject:dictionary options:0 error:nil];
    [request setHTTPBody:jsonData];
    NSLog(@"ciamo: %@",callURL);
    [NSURLConnection sendAsynchronousRequest:request queue:[NSOperationQueue currentQueue] completionHandler:^(NSURLResponse *response, NSData *data, NSError *error) {
        NSLog(@"respose: %@",response);
        if(data){
            NSDictionary *json = [NSJSONSerialization JSONObjectWithData:data options:kNilOptions error:NULL];
            NSLog(@"json: %@",json);
            if(json){
                BOOL success = [[json objectForKey:@"success"] boolValue];
                NSError *MBError = [[json objectForKey:@"data"] objectForKey:@"errors"];
                if(success){
                    NSString *email = [[[json objectForKey:@"data"] objectForKey:@"user"] objectForKey:@"email"];
                    NSString *salt=[[[json objectForKey:@"data"] objectForKey:@"user"] objectForKey:@"salt"];
                    
                    NSString * password = [NSString stringWithFormat:@"%@",[dictionary objectForKey:@"plainPassword"]];
                    
                        NSString * pwd = [NSString stringWithFormat:@"%@",[[dictionary objectForKey:@"signup_form"]objectForKey:@"plainPassword"]];
                        
                        password = [self getCryptWithPlainPassword:pwd andSalt:salt];
                    
                    NSString *idUser = [[[json objectForKey:@"data"] objectForKey:@"user"] objectForKey:@"id"];
                    NSString *username = [[[json objectForKey:@"data"] objectForKey:@"user"] objectForKey:@"username"];
                    NSString *extendedName = [[[json objectForKey:@"data"] objectForKey:@"user"] objectForKey:@"extended_name"];
                    
                    NSDictionary *dictionary = @{@"email":email,
                                                 @"password":password,
                                                 @"username":username,
                                                 @"idUser":idUser,
                                                 @"salt":salt,
                                                 @"extended_name":extendedName};
                    [self.user initWithDictionary:dictionary];
                }

                result(success,MBError);
                
            }else{
                result(NO,error);
            }
            
        }
        if(error){
            NSLog(@"error: %@",error);
            result(NO,error);
        }
    }];
}



-(void)getLoginWithCompletition:(void (^)(BOOL success, NSError *error))result{
    NSString *callURL = [NSString stringWithFormat:@"%@%@",URL,self.user.email];
    
    
   // NSString *str = @"http://34.230.105.103:8002/api/users/dcitest2017@gmail.com";
//    str = [str stringByReplacingOccurrencesOfString:@"@"
//                                         withString:@"%40"];
//    
//    NSString *callURL = [NSString stringWithFormat:@"%@",str];
    
    NSMutableURLRequest *request = [[NSMutableURLRequest alloc] initWithURL:[NSURL URLWithString:callURL]];
    
    [request setHTTPMethod:@"GET"];
    NSLog(@"URL:%@",callURL);
    NSLog(@"Header:%@",[self getHeader]);
    [request setValue:[self getHeader] forHTTPHeaderField:@"X-WSSE"];

    [NSURLConnection sendAsynchronousRequest:request queue:[NSOperationQueue currentQueue] completionHandler:^(NSURLResponse *response, NSData *data, NSError *error) {
        
        if(data){
            NSDictionary *json = [NSJSONSerialization JSONObjectWithData:data options:kNilOptions error:NULL];
            
            NSLog(@"JSON:%@",json);
            if(json){
                
                [SharedVariables sharedInstance].signInResponseDict = json;
                
                BOOL success = [[json objectForKey:@"success"] boolValue];
                NSError *MBError = [json objectForKey:@"errorString"];
               
                double lastUpdateFromLogin = [[[json objectForKey:@"data"] objectForKey:@"last_update"] doubleValue];
                int lastUpdateFromUser = [self.user.last_update intValue];
                if((int)lastUpdateFromUser<(int)lastUpdateFromLogin || lastUpdateFromUser==0){
                    NSLog(@"update profile");
                    [self getUserInfo:self.user.email completition:^(NSDictionary *dictionary, NSError *error) {
                        if(dictionary){
                            NSMutableDictionary *dictionaryForUpdate = [NSMutableDictionary dictionaryWithDictionary:dictionary];
                            [dictionaryForUpdate setObject:[NSNumber numberWithInt:(int)lastUpdateFromLogin] forKeyedSubscript:@"last_update"];
                        
                            [self.user initWithDictionary:dictionaryForUpdate];
                            [[NSNotificationCenter defaultCenter] postNotificationName:@"updateProfile" object:self];
                        }
                    }];
                }
                
                [self enablePresence];
                result(success,MBError);
            }else{
                result(NO,error);
                
            }
        }
//        else
//        {
//            result(nil,error);
//        }
    }];
    
}

#pragma mark - CACHE METHODS

-(void)saveUser:(NSDictionary *)dictionary{
    NSString *applicationDocumentsDir = [NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES) objectAtIndex:0];
    NSString *pathUserCahce = [applicationDocumentsDir stringByAppendingPathComponent:PATH_USER_CACHE];
    NSData *dataFromDictionary = [NSKeyedArchiver archivedDataWithRootObject:dictionary];
    [dataFromDictionary writeToFile:pathUserCahce atomically:NO];
    //[dictionary writeToFile:pathUserCahce atomically:NO];
}

-(void)updateUserCahceWithcompletition:(void (^)(BOOL success, NSError *error))result{
    [self getUserInfo:self.user.email completition:^(NSDictionary *dictionary, NSError *error) {
        if(dictionary){
            [self.user initWithDictionary:dictionary];
            result(YES,error);
        }else{
            result(NO,error);
        }
    }];
}

-(NSArray *)getMessagesList{
    NSString *applicationDocumentsDir = [NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES) objectAtIndex:0];
    NSString *pathChatCache = [applicationDocumentsDir stringByAppendingPathComponent:PATH_CHAT_CACHE];
    NSDictionary *dictionaryCacheChat = [NSDictionary dictionaryWithContentsOfFile:pathChatCache];
    NSArray *arrayCacheCurrentUser = [dictionaryCacheChat objectForKey:self.user.email];
    return arrayCacheCurrentUser;
}

-(void)updateCacheChatWithMessage:(NSDictionary *)messageDictionary{
    NSString *applicationDocumentsDir = [NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES) objectAtIndex:0];
    NSString *pathChatCache = [applicationDocumentsDir stringByAppendingPathComponent:PATH_CHAT_CACHE];
    NSMutableDictionary *dictionaryCacheChat = [[NSDictionary dictionaryWithContentsOfFile:pathChatCache] mutableCopy];
        
    NSString *usernameToChat=[messageDictionary objectForKey:@"usernameToChat"];
    NSString *sender=[messageDictionary objectForKey:@"sender"];
    NSString *recipient=[messageDictionary objectForKey:@"recipient"];
    NSString *text=[messageDictionary objectForKey:@"text"];
    NSString *timeStamp=[messageDictionary objectForKey:@"timestamp"];
    NSString *idMessage=[messageDictionary objectForKey:@"messageId"];
    int sentStatus=[[messageDictionary objectForKey:@"sent"] intValue];
        
    NSMutableArray *arrayChatsCurrentUser = [[dictionaryCacheChat objectForKey:self.user.email] mutableCopy];
    NSMutableArray *resultArrayChatsCurrentUser = [NSMutableArray arrayWithCapacity:0];
        
    if(arrayChatsCurrentUser){
        resultArrayChatsCurrentUser=arrayChatsCurrentUser;
    }
    [dictionaryCacheChat setObject:resultArrayChatsCurrentUser forKey:self.user.email];
    
        
    NSPredicate *predicate = [NSPredicate predicateWithFormat:@"(username==%@)",usernameToChat];
    NSArray *arrayCacheUserToChat = [resultArrayChatsCurrentUser filteredArrayUsingPredicate:predicate];
    NSMutableDictionary *dictionaryCacheUserToChat=[arrayCacheUserToChat lastObject];
    if(dictionaryCacheUserToChat){
        NSMutableArray *arrayListMessage = [dictionaryCacheUserToChat objectForKey:@"list"];
        [dictionaryCacheUserToChat setObject:timeStamp forKey:@"lastUpdate"];
        BOOL valueReaded = [sender isEqualToString:self.user.email];
        if(arrayListMessage.count==0){
            arrayListMessage = [NSMutableArray arrayWithCapacity:0];
            [dictionaryCacheUserToChat setObject:arrayListMessage forKey:@"list"];
        }
        [arrayListMessage addObject:@{@"sender":sender,@"recipient":recipient,@"text":text,@"timestamp":timeStamp,@"messageId":idMessage,@"readed":[NSNumber numberWithBool:valueReaded],@"sent":[NSNumber numberWithInt:sentStatus]}];
        [dictionaryCacheChat writeToFile:pathChatCache atomically:NO];
        [[NSNotificationCenter defaultCenter] postNotificationName:@"cacheUpload" object:self];
    }else{
        NSMutableDictionary *newDictionaryCacheUserToChat = [NSMutableDictionary dictionaryWithCapacity:0];
        [newDictionaryCacheUserToChat setObject:usernameToChat forKey:@"username"];
        [newDictionaryCacheUserToChat setObject:[NSMutableArray arrayWithCapacity:0] forKey:@"list"];
        NSMutableArray *newArrayListMessage = [NSMutableArray arrayWithCapacity:0];
        BOOL valueReaded = [sender isEqualToString:self.user.email];
        [newArrayListMessage addObject:@{@"sender":sender,@"recipient":recipient,@"text":text,@"timestamp":timeStamp,@"messageId":idMessage,@"readed":[NSNumber numberWithBool:valueReaded],@"sent":[NSNumber numberWithInt:sentStatus]}];
        
        [newDictionaryCacheUserToChat setObject:newArrayListMessage forKey:@"list"];
        [newDictionaryCacheUserToChat setObject:timeStamp forKey:@"lastUpdate"];
        [resultArrayChatsCurrentUser addObject:newDictionaryCacheUserToChat];
        [dictionaryCacheChat writeToFile:pathChatCache atomically:NO];
        [[NSNotificationCenter defaultCenter] postNotificationName:@"cacheUpload" object:self];
        
        [self getUserInfo:usernameToChat completition:^(NSDictionary *dictionary, NSError *error) {
            if(dictionary && ![dictionary isEqual:[NSNull null]]){
                /*
                NSString *resultPathImage = ([dictionary objectForKey:@"pictures"])?[dictionary objectForKey:@"pictures"]:@"";
                [newDictionaryCacheUserToChat setObject:resultPathImage forKey:@"pictures"];
                [newDictionaryCacheUserToChat setObject:[dictionary objectForKey:@"extended_name"] forKey:@"extended_name"];
                [newDictionaryCacheUserToChat setObject:[NSNumber numberWithInt:[[dictionary objectForKey:@"picture_index_default"] intValue]] forKey:@"picture_index_default"];
                NSMutableArray *newArrayListMessage = [NSMutableArray arrayWithCapacity:0];
                BOOL valueReaded = [sender isEqualToString:self.user.email];
                [newArrayListMessage addObject:@{@"sender":sender,@"recipient":recipient,@"text":text,@"timestamp":timeStamp,@"messageId":idMessage,@"readed":[NSNumber numberWithBool:valueReaded],@"sent":[NSNumber numberWithInt:sentStatus]}];
                [newDictionaryCacheUserToChat setObject:newArrayListMessage forKey:@"list"];
                [newDictionaryCacheUserToChat setObject:timeStamp forKey:@"lastUpdate"];
                [resultArrayChatsCurrentUser addObject:newDictionaryCacheUserToChat];
                [dictionaryCacheChat writeToFile:pathChatCache atomically:NO];
                 */
                [self updateCacheInfoUserWithDictionary:dictionary andUser:usernameToChat];
            }
                
            [[NSNotificationCenter defaultCenter] postNotificationName:@"cacheUpload" object:self];
        }];
    }
}

-(void)deleteUserInCache:(NSString *)username{
    NSString *applicationDocumentsDir = [NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES) objectAtIndex:0];
    NSString *pathChatCache = [applicationDocumentsDir stringByAppendingPathComponent:PATH_CHAT_CACHE];
    NSMutableDictionary *dictionaryCacheChat = [[NSDictionary dictionaryWithContentsOfFile:pathChatCache] mutableCopy];
    NSMutableArray *arrayChatsCurrentUser = [dictionaryCacheChat objectForKey:self.user.email];
    
    NSPredicate *predicate = [NSPredicate predicateWithFormat:@"(username==%@)",username];
    NSArray *arrayCacheUserToChat = [arrayChatsCurrentUser filteredArrayUsingPredicate:predicate];
    NSMutableDictionary *dictionaryCacheUserToChat=[arrayCacheUserToChat lastObject];
    
    [arrayChatsCurrentUser removeObject:dictionaryCacheUserToChat];
    [dictionaryCacheChat writeToFile:pathChatCache atomically:NO];
}

-(void)updateCacheInfoUserWithDictionary:(NSDictionary *)dictionary andUser:(NSString *)username{
    NSString *applicationDocumentsDir = [NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES) objectAtIndex:0];
    NSString *pathChatCache = [applicationDocumentsDir stringByAppendingPathComponent:PATH_CHAT_CACHE];
    NSMutableDictionary *dictionaryCacheChat = [[NSDictionary dictionaryWithContentsOfFile:pathChatCache] mutableCopy];
    
    NSArray *pictures = [dictionary objectForKey:@"pictures"];
    int picture_index_default = [[dictionary objectForKey:@"picture_index_default"] intValue];
    NSString *extended_name = [dictionary objectForKey:@"extended_name"];
    
    NSString *picture_path=@"";
    
    for(unsigned i=0;i<pictures.count;i++){
        NSDictionary *dic = [pictures objectAtIndex:i];
        if([[dic objectForKey:@"pic_index"] intValue]==picture_index_default){
            picture_path = [dic objectForKey:@"web_path"];
            break;
        }
    }
    
    NSMutableArray *arrayChatsCurrentUser = [dictionaryCacheChat objectForKey:self.user.email];
    
    if(arrayChatsCurrentUser){
        NSPredicate *predicate = [NSPredicate predicateWithFormat:@"(username==%@)",username];
        NSArray *arrayCacheUserToChat = [arrayChatsCurrentUser filteredArrayUsingPredicate:predicate];
        NSMutableDictionary *dictionaryCacheUserToChat=[arrayCacheUserToChat lastObject];
        if(dictionaryCacheUserToChat){
            [dictionaryCacheUserToChat setObject:extended_name forKey:@"extended_name"];
            [dictionaryCacheUserToChat setObject:[NSNumber numberWithInt:picture_index_default] forKey:@"picture_index_default"];
            [dictionaryCacheUserToChat setObject:pictures forKey:@"pictures"];
            [dictionaryCacheChat writeToFile:pathChatCache atomically:NO];
            [[NSNotificationCenter defaultCenter] postNotificationName:@"cacheUpload" object:self];
        }
    }
    
    //UPDATE DEI FAVORITI
    if(![self.user.friends isEqual:[NSNull null]]){
        NSMutableArray *arrayFavorite = [self.user.friends mutableCopy];
        NSPredicate *predicate = [NSPredicate predicateWithFormat:@"(username==%@)",username];
        NSArray *arrayResultFavoriteToUpdate = [arrayFavorite filteredArrayUsingPredicate:predicate];
        NSMutableDictionary *dictionaryResultFavoriteToUpdate=[[arrayResultFavoriteToUpdate lastObject] mutableCopy];
        if(dictionaryResultFavoriteToUpdate){
            [dictionaryResultFavoriteToUpdate setObject:extended_name forKey:@"extended_name"];
            [dictionaryResultFavoriteToUpdate setObject:picture_path forKey:@"picture_path"];
            
            [arrayFavorite removeObject:[arrayResultFavoriteToUpdate lastObject]];
            [arrayFavorite addObject:dictionaryResultFavoriteToUpdate];
            [self.user initWithDictionary:@{@"friends":arrayFavorite}];
            [[NSNotificationCenter defaultCenter] postNotificationName:@"updateProfile" object:self];
        }
    }
}

-(void) updateCacheChatMessageReadedWithMessageId:(NSDictionary *)messageDictionary{
    NSString *applicationDocumentsDir = [NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES) objectAtIndex:0];
    NSString *pathChatCache = [applicationDocumentsDir stringByAppendingPathComponent:PATH_CHAT_CACHE];
    NSMutableDictionary *dictionaryCacheChat = [[NSDictionary dictionaryWithContentsOfFile:pathChatCache] mutableCopy];
    NSMutableArray *arrayChatsCurrentUser = [dictionaryCacheChat objectForKey:self.user.email];
    
    NSPredicate *predicate = [NSPredicate predicateWithFormat:@"(username==%@)",[messageDictionary objectForKey:@"sender"]];
    NSArray *arrayCacheUserToChat = [arrayChatsCurrentUser filteredArrayUsingPredicate:predicate];
    NSMutableDictionary *dictionaryCacheUserToChat=[arrayCacheUserToChat lastObject];
    
    NSMutableArray *arrayListMessage = [dictionaryCacheUserToChat objectForKey:@"list"];
   
    if([messageDictionary objectForKey:@"messageId"]){
        NSPredicate *predicateId = [NSPredicate predicateWithFormat:@"(messageId==%@)",[messageDictionary objectForKey:@"messageId"]];
        
        NSArray *arrayResultMessage = [arrayListMessage filteredArrayUsingPredicate:predicateId];
        NSMutableDictionary *dictionaryMessage=[arrayResultMessage lastObject];
        
        [dictionaryMessage setObject:[NSNumber numberWithBool:YES] forKey:@"readed"];
        
    }else{
        for(unsigned i=0;i<arrayListMessage.count;i++){
            NSMutableDictionary *dictionaryMessage=[arrayListMessage objectAtIndex:i];
            [dictionaryMessage setObject:[NSNumber numberWithBool:YES] forKey:@"readed"];
        }
    }
    
    [dictionaryCacheChat writeToFile:pathChatCache atomically:NO];
}

-(void)updateMessageStatusFromMessageId:(NSString *)messageId withRecipient:(NSString *)recipient withTimeStamp:(NSDate *)timeStamp withStatus:(int)status{
    NSString *applicationDocumentsDir = [NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES) objectAtIndex:0];
    NSString *pathChatCache = [applicationDocumentsDir stringByAppendingPathComponent:PATH_CHAT_CACHE];
    NSMutableDictionary *dictionaryCacheChat = [[NSDictionary dictionaryWithContentsOfFile:pathChatCache] mutableCopy];
    NSMutableArray *arrayChatsCurrentUser = [dictionaryCacheChat objectForKey:self.user.email];
    
    NSPredicate *predicate = [NSPredicate predicateWithFormat:@"(username==%@)",recipient];
    NSArray *arrayCacheUserToChat = [arrayChatsCurrentUser filteredArrayUsingPredicate:predicate];
    NSMutableDictionary *dictionaryCacheUserToChat=[arrayCacheUserToChat lastObject];
    
    NSMutableArray *arrayListMessage = [dictionaryCacheUserToChat objectForKey:@"list"];
    
    NSPredicate *predicateId = [NSPredicate predicateWithFormat:@"(messageId==%@)",messageId];
    NSArray *arrayResultMessage = [arrayListMessage filteredArrayUsingPredicate:predicateId];
    NSMutableDictionary *dictionaryMessage=[arrayResultMessage lastObject];
    
    [dictionaryMessage setObject:[NSNumber numberWithInt:status] forKey:@"sent"];
    [dictionaryMessage setObject:timeStamp forKey:@"timestamp"];
    
    [dictionaryCacheChat writeToFile:pathChatCache atomically:NO];
    
    if(status==0){
        [[NSNotificationCenter defaultCenter] postNotificationName:@"sentMessageStatus" object:self];
    }
}

-(NSMutableArray *)getMessagesCacheFromUser:(NSString *)user numberMessage:(int)number{
    NSMutableArray *array = [NSMutableArray arrayWithCapacity:0];
    
    NSString *applicationDocumentsDir = [NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES) objectAtIndex:0];
    NSString *pathChatCache = [applicationDocumentsDir stringByAppendingPathComponent:PATH_CHAT_CACHE];
    NSDictionary *dictionaryCahceChat = [NSDictionary dictionaryWithContentsOfFile:pathChatCache];
    NSArray *arrayCacheCurrentUser = [dictionaryCahceChat objectForKey:self.user.email];
    
    NSPredicate *predicate = [NSPredicate predicateWithFormat:@"(username==%@)",user];
    NSArray *arrayCacheUserToChat = [arrayCacheCurrentUser filteredArrayUsingPredicate:predicate];
    NSMutableDictionary *dictionaryCacheUserToChat=[arrayCacheUserToChat lastObject];
    
    array = [dictionaryCacheUserToChat objectForKey:@"list"];
    
    int howMany = number;
    int indexStart = (int)array.count-howMany;
    if(array.count<number){
        howMany=(int)array.count;
        indexStart=0;
    }
    array =  [[array subarrayWithRange:NSMakeRange(indexStart, howMany)] mutableCopy];
    
    return array;
}

-(int)getMessageCahceFromUserCount:(NSString *)user{
    NSMutableArray *array = [NSMutableArray arrayWithCapacity:0];
    
    NSString *applicationDocumentsDir = [NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES) objectAtIndex:0];
    NSString *pathChatCache = [applicationDocumentsDir stringByAppendingPathComponent:PATH_CHAT_CACHE];
    NSDictionary *dictionaryCahceChat = [NSDictionary dictionaryWithContentsOfFile:pathChatCache];
    NSArray *arrayCacheCurrentUser = [dictionaryCahceChat objectForKey:self.user.email];
    
    NSPredicate *predicate = [NSPredicate predicateWithFormat:@"(username==%@)",user];
    NSArray *arrayCacheUserToChat = [arrayCacheCurrentUser filteredArrayUsingPredicate:predicate];
    NSMutableDictionary *dictionaryCacheUserToChat=[arrayCacheUserToChat lastObject];
    
    array = [dictionaryCacheUserToChat objectForKey:@"list"];
    
    return (int)array.count;
}

-(void)deleteMessageInCahceWithMessageId:(NSString *)messageId withRecipient:(NSString *)recipient{
    NSString *applicationDocumentsDir = [NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES) objectAtIndex:0];
    NSString *pathChatCache = [applicationDocumentsDir stringByAppendingPathComponent:PATH_CHAT_CACHE];
    NSMutableDictionary *dictionaryCacheChat = [[NSDictionary dictionaryWithContentsOfFile:pathChatCache] mutableCopy];
    NSMutableArray *arrayChatsCurrentUser = [dictionaryCacheChat objectForKey:self.user.email];
    
    NSPredicate *predicate = [NSPredicate predicateWithFormat:@"(username==%@)",recipient];
    NSArray *arrayCacheUserToChat = [arrayChatsCurrentUser filteredArrayUsingPredicate:predicate];
    NSMutableDictionary *dictionaryCacheUserToChat=[arrayCacheUserToChat lastObject];
    
    NSMutableArray *arrayListMessage = [dictionaryCacheUserToChat objectForKey:@"list"];
    
    NSPredicate *predicateId = [NSPredicate predicateWithFormat:@"(messageId==%@)",messageId];
    NSArray *arrayResultMessage = [arrayListMessage filteredArrayUsingPredicate:predicateId];
    NSMutableDictionary *dictionaryMessage=[arrayResultMessage lastObject];
    
    [arrayListMessage removeObject:dictionaryMessage];
    [dictionaryCacheChat writeToFile:pathChatCache atomically:NO];
}

-(int)getMessageNotReadedCount{
    int count = 0;
    
    NSString *applicationDocumentsDir = [NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES) objectAtIndex:0];
    NSString *pathChatCache = [applicationDocumentsDir stringByAppendingPathComponent:PATH_CHAT_CACHE];
    NSDictionary *dictionaryCacheChat = [NSDictionary dictionaryWithContentsOfFile:pathChatCache];
    NSArray *arrayCacheCurrentUser = [dictionaryCacheChat objectForKey:self.user.email];
    
    for(unsigned i=0;i<arrayCacheCurrentUser.count;i++){
        NSArray *list = [[arrayCacheCurrentUser objectAtIndex:i] objectForKey:@"list"];
        
        for(unsigned j=0;j<list.count;j++){
            if([[[list objectAtIndex:j] objectForKey:@"readed"] intValue]==0){
                count++;
            }
        }
        /*
        NSPredicate *predicate = [NSPredicate predicateWithFormat:@"(readed == %i)",0];
        NSArray *arrayResultList = [list filteredArrayUsingPredicate:predicate];
        count+=(int)arrayResultList.count;
         */
    }
    
    return count;
}



#pragma mark - CLIENT API

-(void)deleteMultiTextWithType:(NSString *)type withTitle:(NSString *)title completition:(void (^)(NSDictionary *dictionary, NSError *error))result{
    
    NSString *titleEncode = [title stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding];
    NSString *callURL = [NSString stringWithFormat:@"%@%@",URL_SALT,PUT_MULTI_TEXT(type,titleEncode)];
    
    NSMutableURLRequest *request = [[NSMutableURLRequest alloc] initWithURL:[NSURL URLWithString:callURL]];
    [request setHTTPMethod:@"DELETE"];
    [request setValue:@"application/json" forHTTPHeaderField:@"Content-Type"];
    [request setValue:@"application/json" forHTTPHeaderField:@"Accept"];
    
    [NSURLConnection sendAsynchronousRequest:request queue:[NSOperationQueue currentQueue] completionHandler:^(NSURLResponse *response, NSData *data, NSError *error) {
        if(data){
            NSDictionary *json = [NSJSONSerialization JSONObjectWithData:data options:kNilOptions error:NULL];
            result(json,nil);
        }
        if(error){
            result(nil,error);
        }
    }];
}


-(void)updateMultiTextWithType:(NSString *)type withTitle:(NSString *)title completition:(void (^)(NSDictionary *dictionary, NSError *error))result{
    
    NSString *titleEncode = [title stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding];
    NSString *callURL = [NSString stringWithFormat:@"%@%@",URL_SALT,PUT_MULTI_TEXT(type,titleEncode)];
    
    NSMutableURLRequest *request = [[NSMutableURLRequest alloc] initWithURL:[NSURL URLWithString:callURL]];
    [request setHTTPMethod:@"PUT"];
    [request setValue:@"application/json" forHTTPHeaderField:@"Content-Type"];
    [request setValue:@"application/json" forHTTPHeaderField:@"Accept"];
    
    [NSURLConnection sendAsynchronousRequest:request queue:[NSOperationQueue currentQueue] completionHandler:^(NSURLResponse *response, NSData *data, NSError *error) {
        if(data){
            NSDictionary *json = [NSJSONSerialization JSONObjectWithData:data options:kNilOptions error:NULL];
            result(json,nil);
        }
        if(error){
            result(nil,error);
        }
    }];
}

-(void)sendWakeUpWithDictionary:(NSDictionary *)dictionary completition:(void (^)(BOOL success,NSError *error))result{

    NSString *preview=[dictionary objectForKey:@"text"];
        
    NSDictionary *dictionaryMessageData=@{@"SIN":[dictionary objectForKey:@"payLoad"],
                                          @"sender":[dictionary objectForKey:@"sender"],
                                          @"recipient":[dictionary objectForKey:@"recipient"],
                                          @"text":[dictionary objectForKey:@"text"],
                                          @"sound":@"default",
                                          @"badge":[NSNumber numberWithInt:1]
                                          };

    
    NSData *jsonDataMessage = [NSJSONSerialization dataWithJSONObject:dictionaryMessageData options:NSJSONWritingPrettyPrinted error:nil];
    
    NSDateFormatter *formatter = [[NSDateFormatter alloc] init];
    formatter.dateFormat = @"yyyy-MM-dd HH:mm:ss";
    NSString *datString = [formatter stringFromDate:[NSDate date]];
    
    NSDictionary *dictionaryWakeUp=@{@"wakeup_form":@{@"device_token":[dictionary objectForKey:@"token"],
                                                      @"message_data":[[NSString alloc] initWithData:jsonDataMessage encoding:NSUTF8StringEncoding],@"message_text":preview,@"message_date":datString,@"message_type":@"message"
                                                      }
                                     };
    NSString *recipientID = [dictionary objectForKey:@"recipient"];
    NSString *callURL = [NSString stringWithFormat:@"%@%@",URL_SALT,PUT_WAKEUP(recipientID)];
    NSMutableURLRequest *request = [[NSMutableURLRequest alloc] initWithURL:[NSURL URLWithString:callURL]];
    [request setHTTPMethod:@"PUT"];
    [request setValue:@"application/json" forHTTPHeaderField:@"Content-Type"];
    [request setValue:@"application/json" forHTTPHeaderField:@"Accept"];
    NSData* jsonData = [NSJSONSerialization dataWithJSONObject:dictionaryWakeUp options:0 error:nil];
    [request setHTTPBody:jsonData];
    [NSURLConnection sendAsynchronousRequest:request queue:[NSOperationQueue currentQueue] completionHandler:^(NSURLResponse *response, NSData *data, NSError *error) {
        
        if(data){
            NSDictionary *json = [NSJSONSerialization JSONObjectWithData:data options:kNilOptions error:NULL];
            NSLog(@"result push: %@",json);
            if(json){
                BOOL success = [[json objectForKey:@"success"] boolValue];
                NSError *HKError = [json objectForKey:@"errorString"];
                
                result(success, HKError);
            }else{
                result(NO,error);
            }
            
        }
        if(error){
            NSLog(@"PUT WAKEUP error: %@",error);
            result(NO,error);
        }
    }];
}


-(void)getUserInfo:(NSString *)email completition:(void (^)(NSDictionary *dictionary, NSError *error))result{
    
    NSString *callURL = [NSString stringWithFormat:@"%@%@",URL_SALT,GET_USER(email)];
    NSMutableURLRequest *request = [[NSMutableURLRequest alloc] initWithURL:[NSURL URLWithString:callURL]];
    
    [request setHTTPMethod:@"GET"];
    [request setValue:[self getHeader] forHTTPHeaderField:@"X-WSSE"];
    
    [NSURLConnection sendAsynchronousRequest:request queue:[NSOperationQueue currentQueue] completionHandler:^(NSURLResponse *response, NSData *data, NSError *error) {
        
        if(data){
            NSDictionary *json = [NSJSONSerialization JSONObjectWithData:data options:kNilOptions error:NULL];
            if(json){
                BOOL success = [[json objectForKey:@"success"] boolValue];
                NSError *MBError = [json objectForKey:@"errorString"];
                if(success){
                    NSDictionary *dictionaryResult = [[json objectForKey:@"data"] objectForKey:@"user"];
                    result(dictionaryResult,MBError);
                }else{
                    result(nil,MBError);
                }
            }else{
                result(nil,error);
            }
        }
        if(error){
            result(nil,error);
        }
    }];
}


//-(void)getSearchWithDictionary:(NSDictionary *)dicSearch completition:(void (^)(NSDictionary *dictionary, NSError *error))result{
//    NSString *stringSearchType=@"?";
//    for(NSString *key in dicSearch){
//        stringSearchType = [stringSearchType stringByAppendingFormat:@"%@=%@&",key,[dicSearch objectForKey:key]];
//    }
//    stringSearchType = [stringSearchType substringToIndex:stringSearchType.length-1];
//    
//    NSString *callURL = [NSString stringWithFormat:@"%@%@%@",URL,GET_SEARCH,stringSearchType];
//    NSMutableURLRequest *request = [[NSMutableURLRequest alloc] initWithURL:[NSURL URLWithString:callURL]];
//    
//    [request setHTTPMethod:@"GET"];
//    [request setValue:[self getHeader] forHTTPHeaderField:@"X-WSSE"];
//    
//    [NSURLConnection sendAsynchronousRequest:request queue:[NSOperationQueue currentQueue] completionHandler:^(NSURLResponse *response, NSData *data, NSError *error) {
//        
//        if(data){
//            NSDictionary *json = [NSJSONSerialization JSONObjectWithData:data options:kNilOptions error:NULL];
//            if(json){
//                BOOL success = [[json objectForKey:@"success"] boolValue];
//                NSError *HKError = [json objectForKey:@"errorString"];
//                if(success){
//                    NSDictionary *dictionaryResult = [json objectForKey:@"data"] ;
//                    result(dictionaryResult,HKError);
//                }else{
//                    result(nil,HKError);
//                }
//            }else{
//                
//                result(nil,error);
//            }
//        }
//        if(error){
//            result(nil,error);
//        }
//    }];
//    
//}


-(void)getUsersAroundWithRadius:(float)radius andCoordinate:(CLLocationCoordinate2D)coordinate completition:(void (^)(NSArray *usersList, NSError *error))result{
//    double lat = [[Client sharedClient].user.lat doubleValue];
//    double lon = [[Client sharedClient].user.lon doubleValue];
    
    double lat = coordinate.latitude;
    double lon = coordinate.longitude;
    
    NSString *radiusString = [NSString stringWithFormat:@"%f",(float)radius];
    NSString *callURL = [NSString stringWithFormat:@"%@%@",URL_SALT,GET_AROUNDS(radiusString,lat,lon)];
    
    if([self.user.filterMap isKindOfClass:[NSMutableDictionary class]] && (int)[self.user.filterMap allKeys].count>0){
        for(NSString *key in [self.user.filterMap allKeys]){
            NSString *stringValue = [self.user.filterMap objectForKey:key];
            if(![stringValue isEqualToString:@""]){
                callURL = [NSString stringWithFormat:@"%@&%@=%@",callURL,key,stringValue];
            }
        }
    }
    
    NSMutableURLRequest *request = [[NSMutableURLRequest alloc] initWithURL:[NSURL URLWithString:callURL]];
    
    NSLog(@"Url:%@",callURL);
    NSLog(@"Header:%@",[self getHeader]);
    
    [request setHTTPMethod:@"GET"];
    [request setValue:[self getHeader] forHTTPHeaderField:@"X-WSSE"];
    
    [NSURLConnection sendAsynchronousRequest:request queue:[NSOperationQueue currentQueue] completionHandler:^(NSURLResponse *response, NSData *data, NSError *error) {
        
        if(data){
            NSDictionary *json = [NSJSONSerialization JSONObjectWithData:data options:kNilOptions error:NULL];
            if(json){
                BOOL success = [[json objectForKey:@"success"] boolValue];
                NSError *MBError = [json objectForKey:@"errorString"];
                NSArray *arrayResult = [NSArray array];
                if(success){
                    arrayResult = [NSArray array];
                    if([[json objectForKey:@"data"] count]>0){
                        arrayResult = [[json objectForKey:@"data"] objectForKey:@"users"];
                    }
                    result(arrayResult,MBError);
                }else{
                    result(arrayResult,MBError);
                }
            }else{
                NSLog(@"error Arounds: %@",error);
                result(nil,error);
            }
        }
        if(error){
            NSLog(@"error Arounds: %@",error);
            result(nil,error);
        }
    }];
}


-(void)getPicture:(NSString *)path from:(id)sender withCompletition:(void (^)(UIImage *image, BOOL isInCache, id sender))result{
    NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
    NSString *documentsDirectoryPrimary = [paths objectAtIndex:0];
    NSString *documentsDirectory = [documentsDirectoryPrimary stringByAppendingPathComponent:@"Cache"];
    NSURL *urlDirectoryCache = [NSURL fileURLWithPath:documentsDirectory];
    
    if (![[NSFileManager defaultManager] fileExistsAtPath:documentsDirectory]){
        NSError* error;
        if([[NSFileManager defaultManager] createDirectoryAtPath:documentsDirectory withIntermediateDirectories:NO attributes:nil error:&error]){
            NSLog(@"Directory created");
        }else{
            NSLog(@"error Directory created");
        }
    }
    
    [urlDirectoryCache setResourceValue:[NSNumber numberWithBool: YES] forKey:NSURLIsExcludedFromBackupKey error:nil];
    
    if([path isEqual:[NSNull null]] || [path isEqualToString:@""]){
        result(nil,NO,sender);
        return;
    }
    
    NSString *nameImage = [[path componentsSeparatedByString:@"/"] lastObject];
    NSString *fullPath = [documentsDirectory stringByAppendingPathComponent:nameImage];
    UIImage *image = [UIImage imageWithContentsOfFile:fullPath];
    if(image){
        result(image,YES,sender);
    }else{
        dispatch_queue_t queue = dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_HIGH, 0ul);
        dispatch_async(queue, ^(void){
            NSString *completeURL = [NSString stringWithFormat:@"%@%@",SERVER,path];
            NSData *imageData = [NSData dataWithContentsOfURL:[NSURL URLWithString:completeURL]];
            dispatch_async(dispatch_get_main_queue(), ^{
                UIImage *imageLoaded = [UIImage imageWithData:imageData];
                NSFileManager *fileManager = [NSFileManager defaultManager];
                [fileManager createFileAtPath:fullPath contents:imageData attributes:nil];
                result(imageLoaded,NO,sender);
            });
        });
    }
}

-(void)getPictureFromEmail:(NSString *)email completition:(void (^)(NSString *pathImage, NSError *error))result{
    NSString *callURL = [NSString stringWithFormat:@"%@%@",URL,GET_PICTURE(email)];
    NSMutableURLRequest *request = [[NSMutableURLRequest alloc] initWithURL:[NSURL URLWithString:callURL]];
    
    [request setHTTPMethod:@"GET"];
    [NSURLConnection sendAsynchronousRequest:request queue:[NSOperationQueue currentQueue] completionHandler:^(NSURLResponse *response, NSData *data, NSError *error) {
        if(data){
            NSDictionary *json = [NSJSONSerialization JSONObjectWithData:data options:kNilOptions error:NULL];
            BOOL success = [[json objectForKey:@"success"] boolValue];
            NSError *HKError = [json objectForKey:@"errorString"];
            if(success){
                result([[json objectForKey:@"data"] objectForKey:@"path"],HKError);
            }else{
                result(nil,HKError);
            }
        }else{
            result(nil,error);
        }
    }];
}


//-(void)deletePictureAtIndex:(int)index completition:(void (^)(BOOL success, NSError *error))result{
//    NSString *indexString = [NSString stringWithFormat:@"%i",index];
//    NSString *callURL = [NSString stringWithFormat:@"%@%@",URL,DELETE_PICTURE(indexString)];
//    NSMutableURLRequest *request = [[NSMutableURLRequest alloc] initWithURL:[NSURL URLWithString:callURL]];
//    [request setHTTPMethod:@"DELETE"];
//    [request setValue:[self getHeader] forHTTPHeaderField:@"X-WSSE"];
//    [request setValue:@"application/json" forHTTPHeaderField:@"Content-Type"];
//    [request setValue:@"application/json" forHTTPHeaderField:@"Accept"];
//    
//    [NSURLConnection sendAsynchronousRequest:request queue:[NSOperationQueue currentQueue] completionHandler:^(NSURLResponse *response, NSData *data, NSError *error) {
//        if(data){
//            NSDictionary *json = [NSJSONSerialization JSONObjectWithData:data options:kNilOptions error:NULL];
//            BOOL success = [[json objectForKey:@"success"] boolValue];
//            result(success,error);
//        }
//        if(error){
//            result(NO, error);
//        }
//    }];
//}
//
//
-(void)updatePicture:(UIImage *)image withIndex:(int)index completition:(void (^)(NSDictionary *dictionary, NSError *error))result{
    
    int realIndex = index;
    BOOL isChat=NO;
    if(index==-2){
        isChat=YES;
        realIndex=-1;
    }
    
    NSString *indexString = [NSString stringWithFormat:@"%i",realIndex];
    NSString *callURL = [NSString stringWithFormat:@"%@%@",URL_SALT,POST_PICTURE(indexString)];
    NSMutableURLRequest *request = [[NSMutableURLRequest alloc] initWithURL:[NSURL URLWithString:callURL]];
    
    NSString *chatValue=(isChat)?@"true":@"false";

    
    NSData *dataImage = UIImageJPEGRepresentation(image, 1);
    NSString *base64string=[dataImage base64EncodedStringWithOptions:kNilOptions];
    NSDictionary *dictionary = @{@"picture_form":@{@"title":[NSString stringWithFormat:@"%@_picture",@"my"],
                                                   @"file":base64string,
                                                   @"is_chat":chatValue
                                                   }
                                 };
    
    [request setHTTPMethod:@"POST"];
    [request setValue:[self getHeader] forHTTPHeaderField:@"X-WSSE"];
    [request setValue:@"application/json" forHTTPHeaderField:@"Content-Type"];
    [request setValue:@"application/json" forHTTPHeaderField:@"Accept"];
    
    NSData* jsonData = [NSJSONSerialization dataWithJSONObject:dictionary options:0 error:nil];
    
    [request setHTTPBody:jsonData];
    [NSURLConnection sendAsynchronousRequest:request queue:[NSOperationQueue currentQueue] completionHandler:^(NSURLResponse *response, NSData *data, NSError *error) {
        if(data){
            NSDictionary *json = [NSJSONSerialization JSONObjectWithData:data options:kNilOptions error:NULL];
            NSLog(@"json: %@",json);
            if(!isChat){
                NSString *nameImage = [[[[json objectForKey:@"data"] objectForKey:@"path"] componentsSeparatedByString:@"/"] lastObject];
                NSFileManager *fileManager = [NSFileManager defaultManager];
                NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
                NSString *documentsDirectory = [paths objectAtIndex:0];
                NSString *fullPath = [documentsDirectory stringByAppendingPathComponent:nameImage];
                [fileManager createFileAtPath:fullPath contents:dataImage attributes:nil];
            }
            result(json,nil);
        }
        if(error){
            result(nil, error);
        }
    }];
}

//-(void)updateLocation{
//    NSNumber *latitute = [NSNumber numberWithFloat:self.locationManager.location.coordinate.latitude];
//    NSNumber *longitude = [NSNumber numberWithFloat:self.locationManager.location.coordinate.longitude];
//    
//    [self.user setLat:[NSNumber numberWithDouble:self.locationManager.location.coordinate.latitude]];
//    [self.user setLon:[NSNumber numberWithDouble:self.locationManager.location.coordinate.longitude]];
//    [self.user createDictionaryUser];
//    
//    NSString *callURL = [NSString stringWithFormat:@"%@%@",URL,PATCH_LOCATION];
//    NSMutableURLRequest *request = [[NSMutableURLRequest alloc] initWithURL:[NSURL URLWithString:callURL]];
//    
//    [request setHTTPMethod:@"PATCH"];
//    [request setValue:[self getHeader] forHTTPHeaderField:@"X-WSSE"];
//    [request setValue:@"application/json" forHTTPHeaderField:@"Content-Type"];
//    [request setValue:@"application/json" forHTTPHeaderField:@"Accept"];
//    NSDictionary *dictionary = @{@"user_location_form":@{
//                                         @"lat":latitute,
//                                         @"lon":longitude,
//                                         @"address":@""
//                                         }
//                                 };
//    NSData* jsonData = [NSJSONSerialization dataWithJSONObject:dictionary options:0 error:nil];
//    [request setHTTPBody:jsonData];
//    
//    [NSURLConnection sendAsynchronousRequest:request queue:[NSOperationQueue currentQueue] completionHandler:^(NSURLResponse *response, NSData *data, NSError *error) {
//        
//        if(data){
//            NSDictionary *json = [NSJSONSerialization JSONObjectWithData:data options:kNilOptions error:NULL];
//            [self.user setLat:[[[json objectForKey:@"data"] objectForKey:@"user_location"] objectForKey:@"lat"]];
//            [self.user setLon:[[[json objectForKey:@"data"] objectForKey:@"user_location"] objectForKey:@"lon"]];
//        }
//        if(error){
//            NSLog(@"error patch location: %@",error);
//        }
//    }];
//}

-(void) updateFriend:(NSString *)user withStatus:(NSString *)status completition:(void (^)(BOOL success,NSError *error))result{
    NSDictionary *dictionaryUser=@{@"favourite_friend":@{@"status":status}};
    
    NSString *callURL = [NSString stringWithFormat:@"%@%@",URL_SALT,PUT_FRIEND(user)];
    NSMutableURLRequest *request = [[NSMutableURLRequest alloc] initWithURL:[NSURL URLWithString:callURL]];
    [request setHTTPMethod:@"PUT"];
    [request setValue:@"application/json" forHTTPHeaderField:@"Content-Type"];
    [request setValue:@"application/json" forHTTPHeaderField:@"Accept"];
    NSData* jsonData = [NSJSONSerialization dataWithJSONObject:dictionaryUser options:0 error:nil];
    [request setHTTPBody:jsonData];
    [NSURLConnection sendAsynchronousRequest:request queue:[NSOperationQueue currentQueue] completionHandler:^(NSURLResponse *response, NSData *data, NSError *error) {
        if(data){
            NSDictionary *json = [NSJSONSerialization JSONObjectWithData:data options:kNilOptions error:NULL];
            if(json){
                BOOL success = [[json objectForKey:@"success"] boolValue];
                NSError *MBError = [json objectForKey:@"errorString"];
                result(success,MBError);
            }else{
                result(NO,error);
            }
            
        }
        if(error){
            NSLog(@"updateFriends error: %@",error);
            result(NO,error);
        }
    }];
}

-(void)updateUserInfoWithCompletition:(void (^)(BOOL success,NSError *error))result{
    
    NSLog(@"userdict:%@",[Client sharedClient].user.getUserInDictionary);
    NSDictionary *dictionary = [Client sharedClient].user.getUserInDictionary;
    //[self.user getUserInDictionary];
    NSMutableDictionary *dictionaryUser = [NSMutableDictionary dictionary];
    NSMutableDictionary *dictionaryForm = [NSMutableDictionary dictionary];
    
    for(NSString *key in dictionary){
        if(![[dictionary objectForKey:key] isKindOfClass:[NSArray class]] && [self controlKey:key]){
            BOOL validate =YES;
            if([[dictionary objectForKey:key] isKindOfClass:[NSNumber class]]){
                validate=[dictionary objectForKey:key]!=[NSNull null];
            }else{
                validate=![[dictionary objectForKey:key] isEqualToString:@""];
            }
            if(validate){
                id value;
                if([[dictionary objectForKey:key] isKindOfClass:[NSNumber class]]){
                    if([key isEqualToString:@"is_scrumble"]){
                        value=[NSNumber numberWithBool:[[dictionary objectForKey:key] boolValue]];
                    }else{
                        value=[dictionary objectForKey:key];
                    }
                }else{
                    value=[dictionary objectForKey:key];
                }
                
                [dictionaryForm setObject:value forKey:key];
            }
        }
        
    }
    
    [dictionaryUser setObject:dictionaryForm forKey:@"user_form"];
        
    NSString *callURL = [NSString stringWithFormat:@"%@%@",URL_SALT,PATCH_USER];
    NSMutableURLRequest *request = [[NSMutableURLRequest alloc] initWithURL:[NSURL URLWithString:callURL]];
    [request setHTTPMethod:@"PATCH"];
    [request setValue:@"application/json" forHTTPHeaderField:@"Content-Type"];
    [request setValue:@"application/json" forHTTPHeaderField:@"Accept"];
    NSData* jsonData = [NSJSONSerialization dataWithJSONObject:dictionaryUser options:0 error:nil];
    [request setHTTPBody:jsonData];
    [NSURLConnection sendAsynchronousRequest:request queue:[NSOperationQueue currentQueue] completionHandler:^(NSURLResponse *response, NSData *data, NSError *error) {
        if(data){
            NSDictionary *json = [NSJSONSerialization JSONObjectWithData:data options:kNilOptions error:NULL];
            if(json){
                
                NSMutableDictionary *dictionaryUser = [NSMutableDictionary dictionary];
                NSMutableDictionary *dictionaryForm = [NSMutableDictionary dictionary];
                [dictionaryForm setObject:[SharedVariables sharedInstance].fcmToken forKey:@"push_device_token"];
                
                [dictionaryUser setObject:dictionaryForm forKey:@"user_form"];
                
                NSString *callURL = [NSString stringWithFormat:@"%@%@",URL_SALT,PATCH_USER];
                NSMutableURLRequest *request = [[NSMutableURLRequest alloc] initWithURL:[NSURL URLWithString:callURL]];
                [request setHTTPMethod:@"PATCH"];
                [request setValue:@"application/json" forHTTPHeaderField:@"Content-Type"];
                [request setValue:@"application/json" forHTTPHeaderField:@"Accept"];
                NSData* jsonData = [NSJSONSerialization dataWithJSONObject:dictionaryUser options:0 error:nil];
                [request setHTTPBody:jsonData];
                [NSURLConnection sendAsynchronousRequest:request queue:[NSOperationQueue currentQueue] completionHandler:^(NSURLResponse *response, NSData *data, NSError *error) {
                    if(data){
                        NSDictionary *json = [NSJSONSerialization JSONObjectWithData:data options:kNilOptions error:NULL];
                        if(json){
                            [SharedVariables sharedInstance].isCreated = YES;
                            BOOL success = [[json objectForKey:@"success"] boolValue];
                            NSError *HKError = [json objectForKey:@"errorString"];
                            result(success, HKError);
                        }else{
                            result(NO,error);
                        }
                        
                    }
                    if(error){
                        result(NO,error);
                    }
                }];

                BOOL success = [[json objectForKey:@"success"] boolValue];
                NSError *HKError = [json objectForKey:@"errorString"];
                result(success, HKError);
            }else{
                result(NO,error);
            }
            
        }
        if(error){
            result(NO,error);
        }
    }];
    
}

-(BOOL)controlKey:(NSString *)value{
    BOOL result = YES;
    NSArray *arrayKey=@[@"email",@"password",@"salt",@"username",@"friends",@"pictures",@"idUser",@"last_update",@"filterMap",@"wall_messages"];
    if([arrayKey containsObject:value]){
        result = NO;
    }
    return result;
}


-(void)getSalt:(NSString *)email completition:(void (^)(NSString *salt, NSError *error))result{
    NSString *callURL = [NSString stringWithFormat:@"%@%@",URL_SALT,GET_SALT(email)];
    NSLog(@"SALT_URL: %@",callURL);
    NSMutableURLRequest *request = [[NSMutableURLRequest alloc] initWithURL:[NSURL URLWithString:callURL]];
    
    [request setHTTPMethod:@"GET"];
    

    [NSURLConnection sendAsynchronousRequest:request queue:[NSOperationQueue currentQueue] completionHandler:^(NSURLResponse *response, NSData *data, NSError *error) {
        if(data){
            NSDictionary *json = [NSJSONSerialization JSONObjectWithData:data options:kNilOptions error:NULL];
            BOOL success = [[json objectForKey:@"success"] boolValue];
            NSError *HKError = [json objectForKey:@"errorString"];
            if(success){
                NSLog(@"JSON:%@",json);
                result([[json objectForKey:@"data"] objectForKey:@"salt"],HKError);
            }else{
                result(nil,HKError);
            }
        }else{
            result(nil,error);
        }
    }];
    
}

-(void)getUserProfileDetail:(NSString *)email completition:(void (^)(NSDictionary *profileDict, NSError *error))result{
    NSString *callURL = [NSString stringWithFormat:@"%@%@",URL_SALT,GET_USER_DETAIL(email)];
    NSLog(@"UserProfile: %@",callURL);
    NSMutableURLRequest *request = [[NSMutableURLRequest alloc] initWithURL:[NSURL URLWithString:callURL]];
    
    [request setHTTPMethod:@"GET"];
    
    
    [NSURLConnection sendAsynchronousRequest:request queue:[NSOperationQueue currentQueue] completionHandler:^(NSURLResponse *response, NSData *data, NSError *error) {
        if(data){
            NSDictionary *json = [NSJSONSerialization JSONObjectWithData:data options:kNilOptions error:NULL];
            BOOL success = [[json objectForKey:@"success"] boolValue];
            NSError *HKError = [json objectForKey:@"errorString"];
            if(success){
                NSLog(@"JSON:%@",json);
                result([json objectForKey:@"data"],HKError);
            }else{
                result(nil,HKError);
            }
        }else{
            result(nil,error);
        }
    }];
    
}
-(void)lastSeen:(NSString *)email completition:(void (^)(NSDictionary *responceDict, NSError *error))result{
    NSString *callURL = [NSString stringWithFormat:@"%@%@/lastseen",URL_SALT,GET_USER_DETAIL(email)];
    NSLog(@"blockedlist: %@",callURL);
    NSMutableURLRequest *request = [[NSMutableURLRequest alloc] initWithURL:[NSURL URLWithString:callURL]];
    
    [request setHTTPMethod:@"GET"];
    
    [NSURLConnection sendAsynchronousRequest:request queue:[NSOperationQueue currentQueue] completionHandler:^(NSURLResponse *response, NSData *data, NSError *error) {
        if(data){
            NSDictionary *json = [NSJSONSerialization JSONObjectWithData:data options:kNilOptions error:NULL];
            BOOL success = [[json objectForKey:@"success"] boolValue];
            NSError *HKError = [json objectForKey:@"errorString"];
            if(success){
                NSLog(@"JSON:%@",json);
                result([json objectForKey:@"data"],HKError);
            }else{
                result(nil,HKError);
            }
        }else{
            result(nil,error);
        }
    }];
}

-(void)getUserBlockList:(NSString *)email completition:(void (^)(NSDictionary *responceDict, NSError *error))result{
    NSString *callURL = [NSString stringWithFormat:@"%@%@/blockedlist",URL_SALT,GET_USER_DETAIL(email)];
    NSLog(@"blockedlist: %@",callURL);
    NSMutableURLRequest *request = [[NSMutableURLRequest alloc] initWithURL:[NSURL URLWithString:callURL]];
    
    [request setHTTPMethod:@"GET"];

    [NSURLConnection sendAsynchronousRequest:request queue:[NSOperationQueue currentQueue] completionHandler:^(NSURLResponse *response, NSData *data, NSError *error) {
        if(data){
            NSDictionary *json = [NSJSONSerialization JSONObjectWithData:data options:kNilOptions error:NULL];
            BOOL success = [[json objectForKey:@"success"] boolValue];
            NSError *HKError = [json objectForKey:@"errorString"];
            if(success){
                NSLog(@"JSON:%@",json);
                result([json objectForKey:@"data"],HKError);
            }else{
                result(nil,HKError);
            }
        }else{
            result(nil,error);
        }
    }];
    
}

-(void)deleteAccount:(NSString *)email completition:(void (^)(NSDictionary *responseDict, NSError *error))result{
    NSString *callURL = [NSString stringWithFormat:@"%@%@",URL_SALT,GET_USER_DETAIL(email)];
    NSLog(@"UserProfile: %@",callURL);
    NSMutableURLRequest *request = [[NSMutableURLRequest alloc] initWithURL:[NSURL URLWithString:callURL]];
    
    [request setHTTPMethod:@"DELETE"];
    
    
    [NSURLConnection sendAsynchronousRequest:request queue:[NSOperationQueue currentQueue] completionHandler:^(NSURLResponse *response, NSData *data, NSError *error) {
        if(data){
            NSDictionary *json = [NSJSONSerialization JSONObjectWithData:data options:kNilOptions error:NULL];
            BOOL success = [[json objectForKey:@"success"] boolValue];
            NSError *HKError = [json objectForKey:@"errorString"];
            if(success){
                NSLog(@"JSON:%@",json);
                result([json objectForKey:@"data"],HKError);
            }else{
                result(nil,HKError);
            }
        }else{
            result(nil,error);
        }
    }];
}
-(void)feedBackSubmit:(NSString *)value completition:(void (^)(NSDictionary *responseDict, NSError *error))result{
    NSString *callURL = [NSString stringWithFormat:@"%@%@/feedbacks/%@",URL_SALT,GET_USER_DETAIL(self.user.email),value];
    callURL = [callURL stringByReplacingOccurrencesOfString:@" "
                                         withString:@"%20"];
    NSLog(@"UserProfile: %@",callURL);
    const char *pswChar = [callURL cStringUsingEncoding:NSUTF8StringEncoding];
    callURL = [NSString stringWithFormat:@"%s",pswChar];
    NSLog(@"UserProfile: %@",callURL);
    NSMutableURLRequest *request = [[NSMutableURLRequest alloc] initWithURL:[NSURL URLWithString:callURL]];
    
    [request setHTTPMethod:@"PUT"];
    
    
    [NSURLConnection sendAsynchronousRequest:request queue:[NSOperationQueue currentQueue] completionHandler:^(NSURLResponse *response, NSData *data, NSError *error) {
        if(data){
            NSDictionary *json = [NSJSONSerialization JSONObjectWithData:data options:kNilOptions error:NULL];
            BOOL success = [[json objectForKey:@"success"] boolValue];
            NSError *HKError = [json objectForKey:@"errorString"];
            if(success){
                NSLog(@"JSON:%@",json);
                result([json objectForKey:@"data"],HKError);
            }else{
                result(nil,HKError);
            }
        }else{
            result(nil,error);
        }
    }];
    
}

-(NSString *)getHeader{
    
    
   // return [CocoaWSSE headerWithUsername:[SharedVariables sharedInstance].signInEmailString password:[self getCryptWithPlainPassword:[SharedVariables sharedInstance].signInPasswordString andSalt:[SharedVariables sharedInstance].signInSaltString]];
    
    return [CocoaWSSE headerWithUsername:self.user.email password:self.user.password];
}

-(NSString *)getCryptWithPlainPassword:(NSString *)plainPassword andSalt:(NSString*)salt{
    const char *pswChar = [plainPassword cStringUsingEncoding:NSUTF8StringEncoding];
    const char *saltChar = [salt cStringUsingEncoding:NSUTF8StringEncoding];
    uint8_t digest[CC_SHA512_DIGEST_LENGTH];
    char salted[strlen(pswChar) + strlen(saltChar) + 3];
    snprintf(salted, sizeof salted, "%s{%s}", pswChar, saltChar);
    CC_SHA512(salted, (CC_LONG) strlen(salted), digest);
    for (int i = 0; i < 4999; i++) {
        CC_SHA512_CTX context;
        CC_SHA512_Init(&context);
        CC_SHA512_Update(&context, digest, (CC_LONG) CC_SHA512_DIGEST_LENGTH);
        CC_SHA512_Update(&context, salted, (CC_LONG)strlen(salted));
        CC_SHA512_Final(digest, &context);
    }
    return [[NSData dataWithBytes:digest length:CC_SHA512_DIGEST_LENGTH] base64EncodedStringWithOptions:0];
}

-(void)postWallMessageFromDictionary:(NSDictionary *)dictionary completition:(void (^)(NSDictionary *dictionary, NSError *error))result{
    NSString *callURL = [NSString stringWithFormat:@"%@%@",URL_SALT,POST_WALLMESSAGE];
    NSMutableURLRequest *request = [[NSMutableURLRequest alloc] initWithURL:[NSURL URLWithString:callURL]];
    
    NSMutableDictionary *dictionaryForm = [NSMutableDictionary dictionaryWithCapacity:0];
    NSMutableDictionary *dictionaryInternalForm = [NSMutableDictionary dictionaryWithCapacity:0];
    [dictionaryForm setObject:dictionaryInternalForm forKey:@"wall_message_form"];
    
    for(NSString *key in dictionary.allKeys){
        if(![key isEqualToString:@"photos"] && ![key isEqualToString:@"videos"]){
            [dictionaryInternalForm setObject:[dictionary objectForKey:key] forKey:key];
        }
        
    }
    
//    for(NSString *key in dictionary.allKeys){
//        if(![key isEqualToString:@"videos"]){
//            [dictionaryInternalForm setObject:[dictionary objectForKey:key] forKey:key];
//        }
//    }
    
    [request setHTTPMethod:@"POST"];
    [request setValue:[self getHeader] forHTTPHeaderField:@"X-WSSE"];
    [request setValue:@"application/json" forHTTPHeaderField:@"Content-Type"];
    [request setValue:@"application/json" forHTTPHeaderField:@"Accept"];
    
    NSData* jsonData = [NSJSONSerialization dataWithJSONObject:dictionaryForm options:0 error:nil];
    
    [request setHTTPBody:jsonData];
    [NSURLConnection sendAsynchronousRequest:request queue:[NSOperationQueue currentQueue] completionHandler:^(NSURLResponse *response, NSData *data, NSError *error) {
        if(data){
            NSDictionary *json = [NSJSONSerialization JSONObjectWithData:data options:kNilOptions error:NULL];
            result(json,nil);
        }
        if(error){
            result(nil, error);
        }
    }];

}


-(void)postImage:(UIImage *)image toWallMessageWithId:(NSString *)idMessage completition:(void (^)(BOOL success,NSError *error))result{
    NSString *callURL = [NSString stringWithFormat:@"%@%@",URL_SALT,POST_PICTURE_WALLMESSAGE(idMessage)];
    NSMutableURLRequest *request = [[NSMutableURLRequest alloc] initWithURL:[NSURL URLWithString:callURL]];
    
    NSData *dataImage = UIImageJPEGRepresentation(image, 1);
    NSString *base64string=[dataImage base64EncodedStringWithOptions:kNilOptions];
    NSDictionary *dictionary = @{@"picture_form":@{@"file":base64string}};
    
    [request setHTTPMethod:@"POST"];
    [request setValue:[self getHeader] forHTTPHeaderField:@"X-WSSE"];
    [request setValue:@"application/json" forHTTPHeaderField:@"Content-Type"];
    [request setValue:@"application/json" forHTTPHeaderField:@"Accept"];
    
    NSData* jsonData = [NSJSONSerialization dataWithJSONObject:dictionary options:0 error:nil];
    
    [request setHTTPBody:jsonData];
    [NSURLConnection sendAsynchronousRequest:request queue:[NSOperationQueue currentQueue] completionHandler:^(NSURLResponse *response, NSData *data, NSError *error) {
        if(data){
            NSDictionary *json = [NSJSONSerialization JSONObjectWithData:data options:kNilOptions error:NULL];
            BOOL success = [[json objectForKey:@"success"] boolValue];
            result(success,nil);
        }
        if(error){
            result(NO, error);
        }
    }];

}

-(void)postVideo:(NSString *)file toWallMessageWithId:(NSString *)idMessage completition:(void (^)(BOOL success,NSError *error))result{
    NSString *callURL = [NSString stringWithFormat:@"%@%@",URL_SALT,POST_VIDEO_WALLMESSAGE(idMessage)];
    NSMutableURLRequest *request = [[NSMutableURLRequest alloc] initWithURL:[NSURL URLWithString:callURL]];
    
    NSString * stringPath  = [NSString stringWithFormat:@"%@",file];
    NSLog(@"stringpath %@",stringPath);
//    NSURL *fileURL = [NSURL URLWithString:stringPath];
//    NSString *myString = [fileURL path];
    
   // NSData *data = [NSData dataWithContentsOfFile:myString];
    
    //NSData *data = [[NSData dataWithContentsOfFile:myString]base64EncodedDataWithOptions:NSDataBase64Encoding64CharacterLineLength];

//    NSData *plainData = [myString dataUsingEncoding:NSUTF8StringEncoding];
      NSString *base64String;
//    if ([plainData respondsToSelector:@selector(base64EncodedStringWithOptions:)]) {
//        base64String = [plainData base64EncodedStringWithOptions:kNilOptions];  // iOS 7+
//    } else {
//        base64String = [plainData base64Encoding];                              // pre iOS7
//    }
    //base64String = [NSString stringWithFormat:@"%@",data];
    
    NSDictionary *dictionary = @{@"video_form":@{@"file":@"",@"path":stringPath}};
    
    [request setHTTPMethod:@"POST"];
    [request setValue:[self getHeader] forHTTPHeaderField:@"X-WSSE"];
    [request setValue:@"application/json" forHTTPHeaderField:@"Content-Type"];
    [request setValue:@"application/json" forHTTPHeaderField:@"Accept"];
    
    NSData* jsonData = [NSJSONSerialization dataWithJSONObject:dictionary options:0 error:nil];
    
    [request setHTTPBody:jsonData];
    [NSURLConnection sendAsynchronousRequest:request queue:[NSOperationQueue currentQueue] completionHandler:^(NSURLResponse *response, NSData *data, NSError *error) {
        if(data){
            NSDictionary *json = [NSJSONSerialization JSONObjectWithData:data options:kNilOptions error:NULL];
            BOOL success = [[json objectForKey:@"success"] boolValue];
            result(success,nil);
        }
        if(error){
            result(NO, error);
        }
    }];
    
}

-(void)changePassword:(NSMutableDictionary *)file toWallMessageWithId:(NSString *)idMessage completition:(void (^)(BOOL success,NSError *error))result{
    NSString *callURL = [NSString stringWithFormat:@"%@%@/changepasswords",URL,self.user.email];
    NSMutableURLRequest *request = [[NSMutableURLRequest alloc] initWithURL:[NSURL URLWithString:callURL]];
    
//    NSData *dataImage = [NSData dataWithContentsOfFile:file];
//    NSString *base64string=[dataImage base64EncodedStringWithOptions:kNilOptions];
    NSDictionary *dictionary = @{@"change_password_form":file};
    
    [request setHTTPMethod:@"POST"];
    [request setValue:[self getHeader] forHTTPHeaderField:@"X-WSSE"];
    [request setValue:@"application/json" forHTTPHeaderField:@"Content-Type"];
    [request setValue:@"application/json" forHTTPHeaderField:@"Accept"];
    
    NSData* jsonData = [NSJSONSerialization dataWithJSONObject:dictionary options:0 error:nil];
    
    [request setHTTPBody:jsonData];
    [NSURLConnection sendAsynchronousRequest:request queue:[NSOperationQueue currentQueue] completionHandler:^(NSURLResponse *response, NSData *data, NSError *error) {
        if(data){
            NSDictionary *json = [NSJSONSerialization JSONObjectWithData:data options:kNilOptions error:NULL];
            BOOL success = [[json objectForKey:@"success"] boolValue];
            result(success,nil);
        }
        if(error){
            result(NO, error);
        }
    }];
    
}

-(void)forgotPassword:(NSMutableDictionary *)file toWallMessageWithId:(NSString *)emailId completition:(void (^)(BOOL success,NSError *error))result{
    NSString *callURL = [NSString stringWithFormat:@"%@%@/forgotpasswords",URL,emailId];
    NSMutableURLRequest *request = [[NSMutableURLRequest alloc] initWithURL:[NSURL URLWithString:callURL]];
    
//    NSData *dataImage = [NSData dataWithContentsOfFile:file];
//    NSString *base64string=[dataImage base64EncodedStringWithOptions:kNilOptions];
    NSDictionary *dictionary = @{@"fos_user_resetting_form":file};
    
    [request setHTTPMethod:@"POST"];
    [request setValue:[self getHeader] forHTTPHeaderField:@"X-WSSE"];
    [request setValue:@"application/json" forHTTPHeaderField:@"Content-Type"];
    [request setValue:@"application/json" forHTTPHeaderField:@"Accept"];
    
    NSData* jsonData = [NSJSONSerialization dataWithJSONObject:dictionary options:0 error:nil];
    
    [request setHTTPBody:jsonData];
    [NSURLConnection sendAsynchronousRequest:request queue:[NSOperationQueue currentQueue] completionHandler:^(NSURLResponse *response, NSData *data, NSError *error) {
        if(data){
            NSDictionary *json = [NSJSONSerialization JSONObjectWithData:data options:kNilOptions error:NULL];
            BOOL success = [[json objectForKey:@"success"] boolValue];
            result(success,nil);
        }
        if(error){
            result(NO, error);
        }
    }];
    
}

-(void)getPostsAroundWithRadius:(float)radius andCoordinate:(CLLocationCoordinate2D)coordinate completition:(void (^)(NSArray *postList, NSError *error))result{
    float lat = coordinate.latitude;
    float lon = coordinate.longitude;
    
    NSString *radiusString = @"500";
    [NSString stringWithFormat:@"%f",(float)radius];
    NSString *callURL = [NSString stringWithFormat:@"%@%@",URL_SALT,GET_POSTS_AROUNDS(radiusString,lat,lon)];
    
    NSMutableURLRequest *request = [[NSMutableURLRequest alloc] initWithURL:[NSURL URLWithString:callURL]];
    
    [request setHTTPMethod:@"GET"];
    [request setValue:[self getHeader] forHTTPHeaderField:@"X-WSSE"];
    
    [NSURLConnection sendAsynchronousRequest:request queue:[NSOperationQueue currentQueue] completionHandler:^(NSURLResponse *response, NSData *data, NSError *error) {
        
        if(data){
            NSDictionary *json = [NSJSONSerialization JSONObjectWithData:data options:kNilOptions error:NULL];
            if(json){
                BOOL success = [[json objectForKey:@"success"] boolValue];
                NSError *MBError = [json objectForKey:@"errorString"];
                NSArray *arrayResult = [NSArray array];
                if(success){
                    arrayResult = [NSArray array];
                    if([[json objectForKey:@"data"] count]>0){
                        arrayResult = [[json objectForKey:@"data"] objectForKey:@"wall_messages"];
                    }
                    result(arrayResult,MBError);
                }else{
                    result(arrayResult,MBError);
                }
            }else{
                NSLog(@"error Arounds Posts: %@",error);
                result(nil,error);
            }
        }
        if(error){
            NSLog(@"error Arounds Posts: %@",error);
            result(nil,error);
        }
    }];
}

-(void)getPostsAroundWithOutRadius:(float)radius andCoordinate:(CLLocationCoordinate2D)coordinate completition:(void (^)(NSArray *postList, NSError *error))result{
    int lat = 100;
    int lon = 100;
    NSString* radiusString = @"noradius";
    
    NSString *callURL = [NSString stringWithFormat:@"%@%@",URL_SALT,GET_POSTS_AROUNDS_WITHOUT_RADIOUS(radiusString,lat,lon)];
    
    NSMutableURLRequest *request = [[NSMutableURLRequest alloc] initWithURL:[NSURL URLWithString:callURL]];
    
    [request setHTTPMethod:@"GET"];
    [request setValue:[self getHeader] forHTTPHeaderField:@"X-WSSE"];
    
    [NSURLConnection sendAsynchronousRequest:request queue:[NSOperationQueue currentQueue] completionHandler:^(NSURLResponse *response, NSData *data, NSError *error) {
        
        if(data){
            NSDictionary *json = [NSJSONSerialization JSONObjectWithData:data options:kNilOptions error:NULL];
            if(json){
                BOOL success = [[json objectForKey:@"success"] boolValue];
                NSError *MBError = [json objectForKey:@"errorString"];
                NSArray *arrayResult = [NSArray array];
                if(success){
                    arrayResult = [NSArray array];
                    if([[json objectForKey:@"data"] count]>0){
                        arrayResult = [[json objectForKey:@"data"] objectForKey:@"wall_messages"];
                    }
                    result(arrayResult,MBError);
                }else{
                    result(arrayResult,MBError);
                }
            }else{
                NSLog(@"error Arounds Posts: %@",error);
                result(nil,error);
            }
        }
        if(error){
            NSLog(@"error Arounds Posts: %@",error);
            result(nil,error);
        }
    }];
}

-(void)deleteWallMessageFromId:(NSString *)idMessage completition:(void (^)(BOOL success, NSError *error))result{
    NSString *callURL = [NSString stringWithFormat:@"%@%@",URL_SALT,DELETE_WALL(idMessage)];
    
    NSMutableURLRequest *request = [[NSMutableURLRequest alloc] initWithURL:[NSURL URLWithString:callURL]];
    [request setHTTPMethod:@"DELETE"];
    [request setValue:@"application/json" forHTTPHeaderField:@"Content-Type"];
    [request setValue:@"application/json" forHTTPHeaderField:@"Accept"];
    
    [NSURLConnection sendAsynchronousRequest:request queue:[NSOperationQueue currentQueue] completionHandler:^(NSURLResponse *response, NSData *data, NSError *error) {
        if(data){
            NSDictionary *json = [NSJSONSerialization JSONObjectWithData:data options:kNilOptions error:NULL];
            BOOL success = [[json objectForKey:@"success"] boolValue];
            result(success,error);
        }
        if(error){
            result(NO,error);
        }
    }];
}

-(void)updateWallMessageFromId:(NSString *)idMessage withDictionary:(NSDictionary *)dictionary completition:(void (^)(BOOL success, NSError *error))result{
    NSString *callURL = [NSString stringWithFormat:@"%@%@",URL_SALT,PATCH_WALL(idMessage)];
    NSMutableURLRequest *request = [[NSMutableURLRequest alloc] initWithURL:[NSURL URLWithString:callURL]];
    
    NSMutableDictionary *dictionaryForm = [NSMutableDictionary dictionaryWithCapacity:0];
    NSMutableDictionary *dictionaryInternalForm = [NSMutableDictionary dictionaryWithCapacity:0];
    [dictionaryForm setObject:dictionaryInternalForm forKey:@"wall_message_form"];
   
    [dictionaryInternalForm setObject:[dictionary objectForKey:@"message"] forKey:@"message"];
    [dictionaryInternalForm setObject:[dictionary objectForKey:@"poi"] forKey:@"poi"];
    [dictionaryInternalForm setObject:[dictionary objectForKey:@"lat"] forKey:@"lat"];
    [dictionaryInternalForm setObject:[dictionary objectForKey:@"lon"] forKey:@"lon"];
    [dictionaryInternalForm setObject:[dictionary objectForKey:@"event_date"] forKey:@"event_date"];
    [dictionaryInternalForm setObject:[dictionary objectForKey:@"event_location_info"] forKey:@"event_location_info"];
    [dictionaryInternalForm setObject:[dictionary objectForKey:@"event_price"] forKey:@"event_price"];
    
    [request setHTTPMethod:@"PATCH"];
    [request setValue:[self getHeader] forHTTPHeaderField:@"X-WSSE"];
    [request setValue:@"application/json" forHTTPHeaderField:@"Content-Type"];
    [request setValue:@"application/json" forHTTPHeaderField:@"Accept"];
    
    NSData* jsonData = [NSJSONSerialization dataWithJSONObject:dictionaryForm options:0 error:nil];
    
    [request setHTTPBody:jsonData];
    [NSURLConnection sendAsynchronousRequest:request queue:[NSOperationQueue currentQueue] completionHandler:^(NSURLResponse *response, NSData *data, NSError *error) {
        NSLog(@"error: %@",error);
        if(data){
            NSDictionary *json = [NSJSONSerialization JSONObjectWithData:data options:kNilOptions error:NULL];
            NSLog(@"json: %@",json);
            BOOL success = [[json objectForKey:@"success"] boolValue];
            result(success,nil);
        }
        if(error){
            result(NO, error);
        }
    }];
}

-(void)updatePartecipateWallMessageFromId:(NSString *)idMessage withStatus:(int)status completition:(void (^)(BOOL success,NSError *error))result{
    
    NSString *method;
    
    if (status == 1 || status == 2) {
        
       method = @"PUT";
    }
    else
    {
       method = @"DELETE";
    }

   // NSString *method = (status<0)?@"DELETE":@"PUT";
    
    NSString *callURL = [NSString stringWithFormat:@"%@%@",URL_SALT,PUT_PARTECIPATE(idMessage)];
    NSMutableURLRequest *request = [[NSMutableURLRequest alloc] initWithURL:[NSURL URLWithString:callURL]];
    [request setHTTPMethod:method];
    [request setValue:@"application/json" forHTTPHeaderField:@"Content-Type"];
    [request setValue:@"application/json" forHTTPHeaderField:@"Accept"];
    
    NSDictionary *dictionary=@{@"wall_message_join_form":@{@"status":[NSNumber numberWithInt:status]}};
    NSData* jsonData = [NSJSONSerialization dataWithJSONObject:dictionary options:0 error:nil];
    if(status>=0){[request setHTTPBody:jsonData];}
    
    [NSURLConnection sendAsynchronousRequest:request queue:[NSOperationQueue currentQueue] completionHandler:^(NSURLResponse *response, NSData *data, NSError *error) {
        
        if(data){
            NSDictionary *json = [NSJSONSerialization JSONObjectWithData:data options:kNilOptions error:NULL];
            BOOL success = [[json objectForKey:@"success"] boolValue];
            result(success, error);
        }
        if(error){
            result(NO,error);
        }
    }];
}

-(void)updateLikeWallMessageFromId:(NSString *)idMessage withStatus:(int)status completition:(void (^)(BOOL success,NSError *error))result{
    
    NSString *method = (status<0)?@"DELETE":@"PUT";
    
    NSString *callURL = [NSString stringWithFormat:@"%@%@",URL_SALT,PUT_LIKE(idMessage)];
    NSMutableURLRequest *request = [[NSMutableURLRequest alloc] initWithURL:[NSURL URLWithString:callURL]];
    [request setHTTPMethod:method];
    [request setValue:@"application/json" forHTTPHeaderField:@"Content-Type"];
    [request setValue:@"application/json" forHTTPHeaderField:@"Accept"];
    
    [NSURLConnection sendAsynchronousRequest:request queue:[NSOperationQueue currentQueue] completionHandler:^(NSURLResponse *response, NSData *data, NSError *error) {
        
        if(data){
            NSDictionary *json = [NSJSONSerialization JSONObjectWithData:data options:kNilOptions error:NULL];
            BOOL success = [[json objectForKey:@"success"] boolValue];
            result(success, error);
        }
        if(error){
            result(NO,error);
        }
    }];
}

-(void)sendCommentWallMessageFromId:(NSString *)idMessage withComment:(NSString *)comment completition:(void (^)(NSDictionary *dictionary,NSError *error))result{
    
    NSDictionary *dictionaryComment=@{@"wall_message_comment_form":@{@"content":comment}};
    
    NSString *callURL = [NSString stringWithFormat:@"%@%@",URL_SALT,PUT_COMMENT(idMessage)];
    NSMutableURLRequest *request = [[NSMutableURLRequest alloc] initWithURL:[NSURL URLWithString:callURL]];
    [request setHTTPMethod:@"PUT"];
    [request setValue:@"application/json" forHTTPHeaderField:@"Content-Type"];
    [request setValue:@"application/json" forHTTPHeaderField:@"Accept"];
    NSData* jsonData = [NSJSONSerialization dataWithJSONObject:dictionaryComment options:0 error:nil];
    [request setHTTPBody:jsonData];
    
    [NSURLConnection sendAsynchronousRequest:request queue:[NSOperationQueue currentQueue] completionHandler:^(NSURLResponse *response, NSData *data, NSError *error) {
        if(data){
            NSDictionary *json = [NSJSONSerialization JSONObjectWithData:data options:kNilOptions error:NULL];
            if(json){
                result (json,error);
            }else{
                result(nil,error);
            }
        }
        if(error){
            NSLog(@"send comment error: %@",error);
            result(nil,error);
        }
    }];
}

-(void)getWallMessageFromId:(NSString *)idWall completition:(void (^)(NSArray *dictionary,NSError *error))result{
    NSString *callURL = [NSString stringWithFormat:@"%@%@",URL_SALT,GET_WALLMESSAGE(idWall)];
    NSMutableURLRequest *request = [[NSMutableURLRequest alloc] initWithURL:[NSURL URLWithString:callURL]];
    
    [request setHTTPMethod:@"GET"];
    [request setValue:[self getHeader] forHTTPHeaderField:@"X-WSSE"];
    
    [NSURLConnection sendAsynchronousRequest:request queue:[NSOperationQueue currentQueue] completionHandler:^(NSURLResponse *response, NSData *data, NSError *error) {
        
        if(data){
            NSDictionary *json = [NSJSONSerialization JSONObjectWithData:data options:kNilOptions error:NULL];
            if(json){
                BOOL success = [[json objectForKey:@"success"] boolValue];
                NSError *MBError = [json objectForKey:@"errorString"];
                if(success){
                    NSArray *arrayResult = [[json objectForKey:@"data"] objectForKey:@"wall_messages"];
                    result(arrayResult,MBError);
                }else{
                    result(nil,MBError);
                }
            }else{
                result(nil,error);
            }
        }
        if(error){
            result(nil,error);
        }
    }];
}

-(void)getMyPost:(void (^)(BOOL success, NSError *error))result{
    NSString *callURL = [NSString stringWithFormat:@"%@%@",URL_MYPOST,self.user.email];
    
    
    // NSString *str = @"http://34.230.105.103:8002/api/users/dcitest2017@gmail.com";
    //    str = [str stringByReplacingOccurrencesOfString:@"@"
    //                                         withString:@"%40"];
    //
    //    NSString *callURL = [NSString stringWithFormat:@"%@",str];
    
    NSMutableURLRequest *request = [[NSMutableURLRequest alloc] initWithURL:[NSURL URLWithString:callURL]];
    
    [request setHTTPMethod:@"GET"];
    NSLog(@"URL:%@",callURL);
    NSLog(@"Header:%@",[self getHeader]);
    [request setValue:[self getHeader] forHTTPHeaderField:@"X-WSSE"];
    
    [NSURLConnection sendAsynchronousRequest:request queue:[NSOperationQueue currentQueue] completionHandler:^(NSURLResponse *response, NSData *data, NSError *error) {
        
        if(data){
            NSDictionary *json = [NSJSONSerialization JSONObjectWithData:data options:kNilOptions error:NULL];
            
            NSLog(@"JSON:%@",json);
            if(json){
                
                [SharedVariables sharedInstance].myPostResponseDict = json;
                
                BOOL success = [[json objectForKey:@"success"] boolValue];
                NSError *MBError = [json objectForKey:@"errorString"];
                            
                [self enablePresence];
                result(success,MBError);
            }else{
                result(NO,error);
                
            }
        }
    }];
    
}

-(void)getCallLogList:(NSString *)page completition:(void (^)(BOOL success, NSError *error))result{
    
    NSString *callURL = [NSString stringWithFormat:@"%@%@/calllist?current_page=%@",URL,self.user.email,page];
    
    //http://34.230.105.103:8002/api/users/chatmotog1@mailinator.com/calllist?current_page=1

    // NSString *str = @"http://34.230.105.103:8002/api/users/dcitest2017@gmail.com";
    //    str = [str stringByReplacingOccurrencesOfString:@"@"
    //                                         withString:@"%40"];
    //
    //    NSString *callURL = [NSString stringWithFormat:@"%@",str];
    
    NSMutableURLRequest *request = [[NSMutableURLRequest alloc] initWithURL:[NSURL URLWithString:callURL]];
    
    [request setHTTPMethod:@"GET"];
    NSLog(@"URL:%@",callURL);
    NSLog(@"Header:%@",[self getHeader]);
    [request setValue:[self getHeader] forHTTPHeaderField:@"X-WSSE"];
    
    [NSURLConnection sendAsynchronousRequest:request queue:[NSOperationQueue currentQueue] completionHandler:^(NSURLResponse *response, NSData *data, NSError *error) {
        
        if(data){
            NSDictionary *json = [NSJSONSerialization JSONObjectWithData:data options:kNilOptions error:NULL];
            
            NSLog(@"JSON:%@",json);
            if(json){
                
                
                NSDictionary * resposeDict = json;
                
                [SharedVariables sharedInstance].callLogListResponseDict = [resposeDict objectForKey:@"data"];
                [SharedVariables sharedInstance].morePageCallLogListResponseDict = [resposeDict objectForKey:@"data"];
                
                BOOL success = [[json objectForKey:@"success"] boolValue];
                NSError *MBError = [json objectForKey:@"errorString"];
                
                [self enablePresence];
                result(success,MBError);
            }else{
                result(NO,error);
                
            }
        }
    }];
}

-(void)saveCallLog:(NSString *)email to_user:(NSString *)to_user type:(NSString *)type duration:(NSString *)duration callstartdatetime:(NSString *)callstartdatetime callenddatetime:(NSString *)callenddatetime from_user:(NSString *)from_user completition:(void (^)(BOOL success, NSError *error))result{
    
    
   // http://34.230.105.103:8002/api/users/chatmotog1@mailinator.com/callsave?to_user=iyyapparajdci@gmail.com&type=missed_call&duration=4:30&callstartdatetime=2014%2013:40:40&callenddatetime=2018%2014:40:40&from_user=chatmotog1@mailinator.com
    
   // NSString *callURL = [NSString stringWithFormat:@"%@%@",URL_MYPOST,self.user.email];
    
    NSString *callURL = [NSString stringWithFormat:@"%@%@/callsave?to_user=%@&type=%@&duration=%@&callstartdatetime=%@&callenddatetime=%@&from_user=%@",URL,self.user.email,to_user,type,duration,callstartdatetime,callenddatetime,from_user];
    
    callURL = [callURL stringByReplacingOccurrencesOfString:@" "
                                                 withString:@"%20"];
    
    
    NSMutableURLRequest *request = [[NSMutableURLRequest alloc] initWithURL:[NSURL URLWithString:callURL]];
    
    [request setHTTPMethod:@"GET"];
    [request setValue:[self getHeader] forHTTPHeaderField:@"X-WSSE"];
    
    [NSURLConnection sendAsynchronousRequest:request queue:[NSOperationQueue currentQueue] completionHandler:^(NSURLResponse *response, NSData *data, NSError *error) {
        
        if(data){
            NSDictionary *json = [NSJSONSerialization JSONObjectWithData:data options:kNilOptions error:NULL];
            if(json){
                BOOL success = [[json objectForKey:@"success"] boolValue];
                NSError *MBError = [json objectForKey:@"errorString"];
                if(success){
                    NSDictionary *dictionaryResult = [[json objectForKey:@"data"] objectForKey:@"user"];
                    result(dictionaryResult,MBError);
                }else{
                    result(nil,MBError);
                }
            }else{
                result(nil,error);
            }
        }
        if(error){
            result(nil,error);
        }
    }];
}

-(void)getMyLikes:(NSString *)idWall completition:(void (^)(BOOL success, NSError *error))result{
    NSString *callURL = [NSString stringWithFormat:@"%@%@/like",URL_MYPOST,idWall];
    
    
    // NSString *str = @"http://34.230.105.103:8002/api/users/dcitest2017@gmail.com";
    //    str = [str stringByReplacingOccurrencesOfString:@"@"
    //                                         withString:@"%40"];
    //
    //    NSString *callURL = [NSString stringWithFormat:@"%@",str];
    
    NSMutableURLRequest *request = [[NSMutableURLRequest alloc] initWithURL:[NSURL URLWithString:callURL]];
    
    [request setHTTPMethod:@"GET"];
    NSLog(@"URL:%@",callURL);
    NSLog(@"Header:%@",[self getHeader]);
    [request setValue:[self getHeader] forHTTPHeaderField:@"X-WSSE"];
    
    [NSURLConnection sendAsynchronousRequest:request queue:[NSOperationQueue currentQueue] completionHandler:^(NSURLResponse *response, NSData *data, NSError *error) {
        
        if(data){
            NSDictionary *json = [NSJSONSerialization JSONObjectWithData:data options:kNilOptions error:NULL];
            
            NSLog(@"JSON:%@",json);
            if(json){
                
                [SharedVariables sharedInstance].likesDict = json;
                
                BOOL success = [[json objectForKey:@"success"] boolValue];
                NSError *MBError = [json objectForKey:@"errorString"];
                
                [self enablePresence];
                result(success,MBError);
            }else{
                result(NO,error);
                
            }
        }
    }];
    
}

-(void)getMyComments:(NSString *)idWall completition:(void (^)(BOOL success, NSError *error))result{
    NSString *callURL = [NSString stringWithFormat:@"%@%@/comment",URL_MYPOST,idWall];
    
    
    // NSString *str = @"http://34.230.105.103:8002/api/users/dcitest2017@gmail.com";
    //    str = [str stringByReplacingOccurrencesOfString:@"@"
    //                                         withString:@"%40"];
    //
    //    NSString *callURL = [NSString stringWithFormat:@"%@",str];
    
    // NSDictionary *dictionary = @{@"wall_message_comment_form":@{@"content":base64string}};
    
    NSMutableURLRequest *request = [[NSMutableURLRequest alloc] initWithURL:[NSURL URLWithString:callURL]];
    
    [request setHTTPMethod:@"GET"];
    NSLog(@"URL:%@",callURL);
    NSLog(@"Header:%@",[self getHeader]);
    [request setValue:[self getHeader] forHTTPHeaderField:@"X-WSSE"];
    
    [NSURLConnection sendAsynchronousRequest:request queue:[NSOperationQueue currentQueue] completionHandler:^(NSURLResponse *response, NSData *data, NSError *error) {
        
        if(data){
            NSDictionary *json = [NSJSONSerialization JSONObjectWithData:data options:kNilOptions error:NULL];
            
            NSLog(@"JSON:%@",json);
            if(json){
                
                [SharedVariables sharedInstance].CommentsDict = json;
                
                BOOL success = [[json objectForKey:@"success"] boolValue];
                NSError *MBError = [json objectForKey:@"errorString"];
                
                [self enablePresence];
                result(success,MBError);
            }else{
                result(NO,error);
                
            }
        }
    }];
    
}

-(void)getMyJoins:(NSString *)idWall completition:(void (^)(BOOL success, NSError *error))result{
    NSString *callURL = [NSString stringWithFormat:@"%@%@/join",URL_MYPOST,idWall];
    
    
    // NSString *str = @"http://34.230.105.103:8002/api/users/dcitest2017@gmail.com";
    //    str = [str stringByReplacingOccurrencesOfString:@"@"
    //                                         withString:@"%40"];
    //
    //    NSString *callURL = [NSString stringWithFormat:@"%@",str];
    
    NSMutableURLRequest *request = [[NSMutableURLRequest alloc] initWithURL:[NSURL URLWithString:callURL]];
    
    [request setHTTPMethod:@"GET"];
    NSLog(@"URL:%@",callURL);
    NSLog(@"Header:%@",[self getHeader]);
    [request setValue:[self getHeader] forHTTPHeaderField:@"X-WSSE"];
    
    [NSURLConnection sendAsynchronousRequest:request queue:[NSOperationQueue currentQueue] completionHandler:^(NSURLResponse *response, NSData *data, NSError *error) {
        
        if(data){
            NSDictionary *json = [NSJSONSerialization JSONObjectWithData:data options:kNilOptions error:NULL];
            
            NSLog(@"JSON:%@",json);
            if(json){
                
                [SharedVariables sharedInstance].joinsDict = json;
                
                BOOL success = [[json objectForKey:@"success"] boolValue];
                NSError *MBError = [json objectForKey:@"errorString"];
                
                [self enablePresence];
                result(success,MBError);
            }else{
                result(NO,error);
                
            }
        }
    }];
    
}

-(void)getChatSearchUser:(NSString *)email searchText:(NSString *)searchText completition:(void (^)(BOOL success, NSError *error))result{
    NSString *callURL = [NSString stringWithFormat:@"%@%@/search?text=%@",URL,email,searchText];
    
    //http://34.230.105.103:8002/api/users/priyachatwhite%40mailinator.com/search?text=priya
    
    // NSString *str = @"http://34.230.105.103:8002/api/users/dcitest2017@gmail.com";
        callURL = [callURL stringByReplacingOccurrencesOfString:@" "
                                             withString:@"%20"];
    
        callURL = [NSString stringWithFormat:@"%@",callURL];
    
    NSMutableURLRequest *request = [[NSMutableURLRequest alloc] initWithURL:[NSURL URLWithString:callURL]];
    
    [request setHTTPMethod:@"GET"];
    NSLog(@"URL:%@",callURL);
    NSLog(@"Header:%@",[self getHeader]);
    [request setValue:[self getHeader] forHTTPHeaderField:@"X-WSSE"];
    
    [NSURLConnection sendAsynchronousRequest:request queue:[NSOperationQueue currentQueue] completionHandler:^(NSURLResponse *response, NSData *data, NSError *error) {
        
        if(data){
            NSDictionary *json = [NSJSONSerialization JSONObjectWithData:data options:kNilOptions error:NULL];
            
            NSLog(@"JSON:%@",json);
            if(json){
                
                [SharedVariables sharedInstance].chatSearchDict = json;
                
                BOOL success = [[json objectForKey:@"success"] boolValue];
                NSError *MBError = [json objectForKey:@"errorString"];
                
                [self enablePresence];
                result(success,MBError);
            }else{
                result(NO,error);
                
            }
        }
    }];
    
}



#pragma mark - FB METHODS

-(void)FBLogin{
    self.loginManager = [[FBSDKLoginManager alloc] init];
    [self.loginManager logOut];
    [self.loginManager logInWithReadPermissions:@[@"email",@"public_profile"] handler:^(FBSDKLoginManagerLoginResult *result, NSError *error) {
        
        if (error) {
            NSLog(@"FBLOGIN process Error");
            
            if ([[SharedVariables sharedInstance].fbLogInViewController isKindOfClass:SignInViewController.class]) {
                
                
                
                SignInViewController * signInVC = [SharedVariables sharedInstance].fbLogInViewController;
                
                [signInVC afterFbLoginCancel];
                
            }
            
//            AppDelegate *ad = (AppDelegate*)[UIApplication sharedApplication].delegate;
//            NavigationController *nav = (NavigationController*)ad.window.rootViewController;
//            LoginViewController *login = (LoginViewController *)nav.visibleViewController;
//            [login hideLoading];
            
        } else if (result.isCancelled) {
            
            if ([[SharedVariables sharedInstance].fbLogInViewController isKindOfClass:SignInViewController.class]) {
                
                
                
                SignInViewController * signInVC = [SharedVariables sharedInstance].fbLogInViewController;
                
                [signInVC afterFbLoginCancel];
                
            }
            
//            NSLog(@"FBLOGIN cencelled");
//            AppDelegate *ad = (AppDelegate*)[UIApplication sharedApplication].delegate;
//            NavigationController *nav = (NavigationController*)ad.window.rootViewController;
//            LoginViewController *login = (LoginViewController *)nav.visibleViewController;
//            [login hideLoading];
        }
        else {
            
            //if([FBSDKAccessToken currentAccessToken]){
        
        FBSDKGraphRequest *postRequest = nil;
        NSDictionary *parameters = [NSDictionary dictionaryWithObject:@"id,email,first_name,last_name,name,picture{url}" forKey:@"fields"];
        
        postRequest = [[FBSDKGraphRequest alloc] initWithGraphPath:@"me" parameters:parameters HTTPMethod:@"GET"];
        [postRequest startWithCompletionHandler:^(FBSDKGraphRequestConnection *connection, id result, NSError *error)
         {
             
             NSMutableDictionary * resultDict = [[NSMutableDictionary alloc] init];
             
             if ([SharedVariables sharedInstance].isFbGetEmail == YES) {
                 
                 NSString * emailStr =[SharedVariables sharedInstance].fbLogInUserInputEmail;
                 resultDict = [NSMutableDictionary dictionaryWithObjectsAndKeys:
                                                           [result objectForKey:@"first_name"],@"first_name",
                                                           [result objectForKey:@"id"],@"id",
                                                           [result objectForKey:@"last_name"],@"last_name",
                                                           [result objectForKey:@"name"],@"name",
                                                           [result objectForKey:@"picture"],@"picture",
                                                           nil];
                 [resultDict setObject:emailStr forKey:@"email"];
                 [SharedVariables sharedInstance].isFbGetEmail = NO;
                 
             }
             else
             {
                 resultDict = result;
             }
             
             if (![resultDict objectForKey:@"email"]) {
                 
                 if ([[SharedVariables sharedInstance].fbLogInViewController isKindOfClass:SignInViewController.class]) {
                     
                     SignInViewController * signInVC = [SharedVariables sharedInstance].fbLogInViewController;
                     
                     [SharedVariables sharedInstance].isFbGetEmail = YES;
                     
                     //[[Utilities sharedInstance] showFbloginEmailAlertView:signInVC.view title:@"" message:@""];
                     [[Utilities sharedInstance] showFbloginEmailAlertView:[SharedVariables sharedInstance].fbLogInViewController.view title:@"" message:@"" controller:[SharedVariables sharedInstance].fbLogInViewController];
                     
                 }
             }
             else if (!error) {
                 NSLog(@"responseDict:%@",resultDict);
                 [self createFromFB:resultDict];
                 
             }
             else{
                 NSLog(@"errore FB LOGIN:%@",error.localizedDescription);
             }
         }];

        }
    }];
}

-(void)createFromFB:(id)user{
    NSString *sexResult = ([[user objectForKey:@"gender"] isEqualToString:@"male"])?@"0":@"1";
    
    NSMutableDictionary *dictionaryForUser = [NSMutableDictionary dictionaryWithObjectsAndKeys:
                                              [user objectForKey:@"email"],@"email",
                                              [user objectForKey:@"email"],@"username",
                                              [user objectForKey:@"name"],@"extended_name",
                                              [NSNumber numberWithInt:0],@"picture_index_default",
                                              sexResult,@"sex",nil];
    
    NSDictionary *dictionary = [NSDictionary dictionaryWithObjectsAndKeys:
                                [user objectForKey:@"email"],@"email",
                                [user objectForKey:@"email"],@"username",
                                [user objectForKey:@"id"],@"plainPassword",nil];
    
    
    NSDictionary *formDict = [NSDictionary dictionaryWithObjectsAndKeys:
                              dictionary,@"signup_form",nil];
    
    [self createAccountFbWithDictionary:formDict completition:^(BOOL success, NSError *error) {
        if(success){
            [[Client sharedClient] checkAuthenticateWithCompletition:^(BOOL success) {
                if(success){
                    [self getFBPicture:user];
                    
                    [dictionaryForUser setObject:[NSNumber numberWithInt:1] forKey:@"location_visibility"];
                    [dictionaryForUser setObject:[NSNumber numberWithInt:0] forKey:@"is_scrumble"];
                    
                    [self.user initWithDictionary:dictionaryForUser];
                    [self updateUserInfoWithCompletition:^(BOOL success, NSError *error) {
                        [[NSNotificationCenter defaultCenter] postNotificationName:@"updateProfile" object:nil];
                    }];
                    
//                    AppDelegate *ad = [[UIApplication sharedApplication] delegate];
//                    if([ad.navController.visibleViewController isKindOfClass:[LoginViewController class]]){
//                        //[ad switchRootController];
//                        [((LoginViewController*)ad.navController.visibleViewController) goToTutorial];
//                    }
                    
                    [self loginWithEmail:[user objectForKey:@"email"]  withPassword:[user objectForKey:@"id"] passwordPlain:YES completition:^(BOOL success, NSError *error) {
                        /*
                         AppDelegate *ad = [UIApplication sharedApplication].delegate;
                         NavigationController *nav = (NavigationController *)ad.window.rootViewController;
                         LoginViewController *login = (LoginViewController *)nav.visibleViewController;
                         [login deleteLoading];
                         */
                        if(success){
                            //NSLog(@"OK Login");
                            [dictionaryForUser setObject:[NSNumber numberWithInt:1] forKey:@"location_visibility"];
                            [dictionaryForUser setObject:[NSNumber numberWithInt:0] forKey:@"is_scrumble"];
                            [dictionaryForUser removeObjectForKey:@"picture_index_default"];
                            [dictionaryForUser removeObjectForKey:@"extended_name"];
                            
                            [self.user initWithDictionary:dictionaryForUser];
                            [self updateUserInfoWithCompletition:^(BOOL success, NSError *error) {
                                NSLog(@"ok update user");
                                
                                if ([[SharedVariables sharedInstance].fbLogInViewController isKindOfClass:SignInViewController.class]) {
                                    
                                    
                                    
                                    SignInViewController * signInVC = [SharedVariables sharedInstance].fbLogInViewController;
                                    
                                    [signInVC afterFbLoginSuccess];
                                    
                                }
                            }];
                            //                        AppDelegate *ad = [[UIApplication sharedApplication] delegate];
                            //                        if([ad.navController.visibleViewController isKindOfClass:[LoginViewController class]]){
                            //                            //[ad switchRootController];
                            //                            [((LoginViewController*)ad.navController.visibleViewController) goToTutorial];
                            //                        }
                        }
                        if(error){
                            // NSLog(@"Login FB error: %@",error);
                        }
                    }];

              
                    
                }
            }];
        }else{
            //NSLog(@"error FB: %@",error);
            NSLog(@"invalid:%@",error.description);
            
            NSString *errorStr = [NSString stringWithFormat:@"%@",error.description];
            
            if ([errorStr rangeOfString:@"already used"].location == NSNotFound) {
                NSLog(@"string does not contain already used");
            } else {
                
                NSLog(@"string contain already used");
                [self loginWithEmail:[user objectForKey:@"email"]  withPassword:[user objectForKey:@"id"] passwordPlain:YES completition:^(BOOL success, NSError *error) {
                    /*
                     AppDelegate *ad = [UIApplication sharedApplication].delegate;
                     NavigationController *nav = (NavigationController *)ad.window.rootViewController;
                     LoginViewController *login = (LoginViewController *)nav.visibleViewController;
                     [login deleteLoading];
                     */
                    if(success){
                        //NSLog(@"OK Login");
                        [dictionaryForUser setObject:[NSNumber numberWithInt:1] forKey:@"location_visibility"];
                        [dictionaryForUser setObject:[NSNumber numberWithInt:0] forKey:@"is_scrumble"];
                        //[dictionaryForUser removeObjectForKey:@"picture_index_default"];
                        //[dictionaryForUser removeObjectForKey:@"extended_name"];
                        
                        [self.user initWithDictionary:dictionaryForUser];
                        [self updateUserInfoWithCompletition:^(BOOL success, NSError *error) {
                            NSLog(@"ok update user");
                            
                            if ([[SharedVariables sharedInstance].fbLogInViewController isKindOfClass:SignInViewController.class]) {
                                
                                
                                
                                SignInViewController * signInVC = [SharedVariables sharedInstance].fbLogInViewController;
                                
                                [signInVC afterFbLoginSuccess];
                                
                            }
                        }];
                        //                        AppDelegate *ad = [[UIApplication sharedApplication] delegate];
                        //                        if([ad.navController.visibleViewController isKindOfClass:[LoginViewController class]]){
                        //                            //[ad switchRootController];
                        //                            [((LoginViewController*)ad.navController.visibleViewController) goToTutorial];
                        //                        }
                    }
                    else
                    {
                        if ([[SharedVariables sharedInstance].fbLogInViewController isKindOfClass:SignInViewController.class]) {
                            
                            
                            
                            SignInViewController * signInVC = [SharedVariables sharedInstance].fbLogInViewController;
                            
                            [signInVC afterFbLoginCancel];
                            
                        }
                    }
                    NSString*erroStr = [NSString stringWithFormat:@"%@",error];
                    
                    if(![erroStr isEqualToString:@"<null>"] && [erroStr isEqualToString:@"(null)"] ){
                         NSLog(@"Login FB error: %@",error);
                        //[[Utilities sharedInstance] showCustomAlertView:[SharedVariables sharedInstance].fbLogInViewController title:@"Mabo" message:erroStr];
                        [[Utilities sharedInstance] showCustomAlertView:[SharedVariables sharedInstance].fbLogInViewController.view title:@"Mabo" message:erroStr controller:[SharedVariables sharedInstance].fbLogInViewController reason:@"creatFb"];
                    }
                }];
            }
        }
    }];
}
/*
- (void)sessionStateChanged:(FBSession *)session state:(FBSessionState) state error:(NSError *)error {
    if (!error && state == FBSessionStateOpen){
        [[FBRequest requestForMe] startWithCompletionHandler: ^(FBRequestConnection *connection, NSDictionary<FBGraphUser> *user, NSError *error) {
            if (!error) {
                
                NSString *sexResult = ([[user objectForKey:@"gender"] isEqualToString:@"male"])?@"0":@"1";
                
                NSMutableDictionary *dictionaryForUser = [NSMutableDictionary dictionaryWithObjectsAndKeys:
                                                          [user objectForKey:@"email"],@"email",
                                                          [user objectForKey:@"email"],@"username",
                                                          [user objectForKey:@"name"],@"extended_name",
                                                          [NSNumber numberWithInt:0],@"picture_index_default",
                                                          sexResult,@"sex",nil];
                
                NSDictionary *dictionary = [NSDictionary dictionaryWithObjectsAndKeys:
                                            [user objectForKey:@"email"],@"email",
                                            [user objectForKey:@"email"],@"username",
                                            [user objectForKey:@"id"],@"plainPassword",nil];
                
                [self createAccountWithDictionary:dictionary completition:^(BOOL success, NSError *error) {
                    if(success){
                        [[Client sharedClient] checkAuthenticateWithCompletition:^(BOOL success) {
                            
                            
                            if(success){
                                [self getFBPicture];
                                
                                [dictionaryForUser setObject:[NSNumber numberWithInt:1] forKey:@"location_visibility"];
                                [dictionaryForUser setObject:[NSNumber numberWithInt:0] forKey:@"is_scrumble"];
                                
                                [self.user initWithDictionary:dictionaryForUser];
                                [self updateUserInfoWithCompletition:^(BOOL success, NSError *error) {
                                    [[NSNotificationCenter defaultCenter] postNotificationName:@"updateProfile" object:nil];
                                }];
                                
                                AppDelegate *ad = [[UIApplication sharedApplication] delegate];
                                if([ad.navController.visibleViewController isKindOfClass:[LoginViewController class]]){
                                    //[ad switchRootController];
                                    [((LoginViewController*)ad.navController.visibleViewController) goToTutorial];
                                }
                            }
                        }];
                    }else{
                        //NSLog(@"error FB: %@",error);
                        NSLog(@"invalid");
                        if([error.description isEqualToString:@"Invalid form"]){
                            [self loginWithEmail:[user objectForKey:@"email"]  withPassword:[user objectForKey:@"id"] passwordPlain:YES completition:^(BOOL success, NSError *error) {
 
                                if(success){
                                    //NSLog(@"OK Login");
                                    [dictionaryForUser setObject:[NSNumber numberWithInt:1] forKey:@"location_visibility"];
                                    [dictionaryForUser setObject:[NSNumber numberWithInt:0] forKey:@"is_scrumble"];
                                    [dictionaryForUser removeObjectForKey:@"picture_index_default"];
                                    [dictionaryForUser removeObjectForKey:@"extended_name"];

                                    [self.user initWithDictionary:dictionaryForUser];
                                    [self updateUserInfoWithCompletition:^(BOOL success, NSError *error) {
                                        NSLog(@"ok update user");
                                    }];
                                    AppDelegate *ad = [[UIApplication sharedApplication] delegate];
                                    if([ad.navController.visibleViewController isKindOfClass:[LoginViewController class]]){
                                        //[ad switchRootController];
                                        [((LoginViewController*)ad.navController.visibleViewController) goToTutorial];
                                    }
                                }
                                if(error){
                                    // NSLog(@"Login FB error: %@",error);
                                }
                            }];
                        }
                        
                    }
                }];
            }else{
                // NSLog(@"FB request user error: %@",error);
            }//ppppp
        }];
        return;
    }
    if (state == FBSessionStateClosed || state == FBSessionStateClosedLoginFailed){
 
    }
    
    if (error){
 
    }
}
*/

-(void)getFBPicture:(id)result{
    NSString *urlPicture = [NSString stringWithFormat:@"http://graph.facebook.com/%@/picture?type=large", [result objectForKey:@"id"]];
    dispatch_async(dispatch_queue_create("MBimageQueue", NULL), ^{
        NSData *imageData = [NSData dataWithContentsOfURL:[NSURL URLWithString:urlPicture]];
        dispatch_async(dispatch_get_main_queue(), ^{
            UIImage *FBPicture  = [UIImage imageWithData:imageData];
            [self updatePicture:FBPicture withIndex:0 completition:^(NSDictionary *dictionaryPicture, NSError *error) {
                NSLog(@"picture FB uploaded");
                [self updateUserInfoWithCompletition:^(BOOL success, NSError *error) {
                    NSLog(@"ok update user");
                }];
            }];
            
        });
    });
}

#pragma mark - LOCATION DELEGATE METHODS

//-(void)locationManager:(CLLocationManager *)manager didUpdateLocations:(NSArray *)locations{
//    if(self.user.location_visibility!=0){
//        NSLog(@"UPDATE LOCATION");
//        [self updateLocation];
//    }
//    if(self.isFirstCreate){
//        self.isFirstCreate=NO;
//        [self.user initWithDictionary:@{@"location_visibility":[NSNumber numberWithInt:1],
//                                        @"is_scrumble":[NSNumber numberWithInt:0]}];
//        [self updateUserInfoWithCompletition:^(BOOL success, NSError *error) {
//            if(success){
//                NSLog(@"update user from location");
//                [self updateLocation];
//                [[NSNotificationCenter defaultCenter] postNotificationName:@"updateProfile" object:nil];
//            }else{
//                NSLog(@"error Update user: %@",error);
//            }
//        }];
//    }
//}

- (void)locationManager:(CLLocationManager *)manager didFailWithError:(NSError *)error{
    NSLog(@"didFailWithError: %@",error.localizedDescription);
    [self.locationManager stopUpdatingLocation];
    if([CLLocationManager authorizationStatus]!=kCLAuthorizationStatusAuthorizedWhenInUse && [CLLocationManager authorizationStatus]!=kCLAuthorizationStatusAuthorizedAlways){
        
        NSDictionary *dictionaryRestInfo = @{@"location_visibility":[NSNumber numberWithInt:0],@"is_scrumble":[NSNumber numberWithInt:0]};
        
        [[Client sharedClient].user initWithDictionary:dictionaryRestInfo];
        [[Client sharedClient] updateUserInfoWithCompletition:^(BOOL success, NSError *error) {
            NSLog(@"ok update Location");
            [[NSNotificationCenter defaultCenter] postNotificationName:@"updateProfile" object:nil];
        }];
        
        
        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"OOPS!" message:NSLocalizedString(@"You need to turn on location services for this app in your general settings",nil) delegate:self cancelButtonTitle:@"OK" otherButtonTitles:nil];
        [alert show];
    }else{
        //NSLog(@"error: %@",error.localizedDescription);
        //NSLog(@"loc: %@",[Singleton sharedSingleton].locMan.location);
        [self.locationManager stopUpdatingLocation];
    }
}

#pragma mark - REACHABILITY

-(void)startReachAbility{
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(reachabilityChanged:)
                                                 name:kReachabilityChangedNotification
                                               object:nil];
    
    self.reachAbility = [Reachability reachabilityForInternetConnection];
    [self.reachAbility startNotifier];
}

-(void)reachabilityChanged:(NSNotification*)note{

    [[NSNotificationCenter defaultCenter] postNotificationName:@"connectionChange" object:self];
    
    if(![[Client sharedClient].user.email isEqualToString:@""]){
        if(!self.authenticate && self.reachAbility.isReachable){
            [self checkAuthenticateWithCompletition:^(BOOL success) {
                if(success){
                    //self.authenticate=YES;
                    NSLog(@"autenticato");
                    //[[SinchClient sharedClient] activeClient];
                }else{
                    //self.authenticate=NO;
                }
            }];
        }
        
        if(!self.reachAbility.isReachable){
            //self.authenticate=NO;
        }
    }else{
        if(self.reachAbility.isReachable){
//            AppDelegate *ad = [UIApplication sharedApplication].delegate;
//            [ad switchLogOutController];
        }
    }
}


#pragma mark - FIREBASE

-(void)enablePresence{
    Firebase *myConnectionsRef = [[Firebase alloc] initWithUrl:@"https://mabo-7.firebaseio.com"];
    myConnectionsRef = [myConnectionsRef childByAppendingPath:[NSString stringWithFormat:@"users/%@/connections",[UtilityGraphic URLEncodedString:self.user.email]]];
    
    Firebase *lastOnlineRef = [[Firebase alloc] initWithUrl:@"https://mabo-7.firebaseio.com"];
    lastOnlineRef = [lastOnlineRef childByAppendingPath:[NSString stringWithFormat:@"users/%@/lastOnLine",[UtilityGraphic URLEncodedString:self.user.email]]];
    
    self.firebaseConnectedRef = [[Firebase alloc] initWithUrl:[NSString stringWithFormat:@"%@/.info/connected",@"https://mabo-7.firebaseio.com"]];
    
    [self.firebaseConnectedRef observeEventType:FEventTypeValue withBlock:^(FDataSnapshot *snapshot) {
        if([snapshot.value boolValue]) {
            Firebase *con = [myConnectionsRef childByAutoId];
            [con setValue:[NSNumber numberWithBool:YES]];
            [con onDisconnectRemoveValue];
            [lastOnlineRef onDisconnectSetValue:kFirebaseServerValueTimestamp];
        }
    }];
   
}

@end
