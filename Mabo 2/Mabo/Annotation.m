//
//  HKAnnotation.m
//  honk
//
//  Created by Steewe MacBook Pro on 26/01/15.
//  Copyright (c) 2015 Steewe. All rights reserved.
//

#import "Annotation.h"

@implementation Annotation

@synthesize coordinate = _coordinate;
@synthesize title = _title;
@synthesize subtitle = _subtitle;
@synthesize picture = _picture;
@synthesize idMessage = _idMessage;
@synthesize isEvent = _isEvent;
@synthesize email = _email;
@synthesize userDictionary = _userDictionary;

- (id)initWithDictionary:(NSDictionary *)dictionary andCoordinate:(CLLocationCoordinate2D)coordinate andUserDic:(NSDictionary *)userDictionary{
    self = [super init];
    
    if (self != nil) {

        _userDictionary=userDictionary;
        _coordinate=coordinate;
        _title = [dictionary objectForKey:@"title"];
        _subtitle = [dictionary objectForKey:@"subtitle"];
        _picture = ([dictionary objectForKey:@"picture_path"])?[dictionary objectForKey:@"picture_path"]:@"";
        _idMessage=([dictionary objectForKey:@"id"])?[dictionary objectForKey:@"id"]:@"";
        _isEvent=([dictionary objectForKey:@"is_event"])?[dictionary objectForKey:@"is_event"]:@"0";
        _email=([dictionary objectForKey:@"email"])?[dictionary objectForKey:@"email"]:@"0";
        if(![dictionary objectForKey:@"email"]){
            _email=[dictionary objectForKey:@"username"];
        }
        self.isScrumble = [[dictionary objectForKey:@"scrumble"] boolValue];
        self.index = [[dictionary objectForKey:@"index"] intValue];
        
    }
    
    return self;
}

- (id)initWithDictionaryObject:(NSDictionary *)dictionary andCoordinate:(CLLocationCoordinate2D)coordinate andUserDic:(NSDictionary *)userDictionary{
    self = [super init];
    
    if (self != nil) {
        
        _userDictionary=userDictionary;
        _coordinate=coordinate;
        _title = [dictionary objectForKey:@"title"];
        _subtitle = [dictionary objectForKey:@"subtitle"];
        _picture = ([dictionary objectForKey:@"picture_path"])?[dictionary objectForKey:@"picture_path"]:@"";
        _idMessage=([dictionary objectForKey:@"id"])?[dictionary objectForKey:@"id"]:@"";
        _isEvent=([dictionary objectForKey:@"is_event"])?[dictionary objectForKey:@"is_event"]:@"0";
        _email=([dictionary objectForKey:@"email"])?[dictionary objectForKey:@"email"]:@"0";
        if(![dictionary objectForKey:@"email"]){
            _email=[dictionary objectForKey:@"username"];
        }
        self.isScrumble = [[dictionary objectForKey:@"scrumble"] boolValue];
        self.index = [[dictionary objectForKey:@"index"] intValue];
        
    }
    
    return self;
}

- (NSDictionary *)userDictionary {
    return _userDictionary;
}

- (NSString *)title {
    return _title;
}

- (NSString *)subtitle {
    return _subtitle;
}

- (NSString *)picture {
    return _picture;
}

- (NSString *)idMessage {
    return _idMessage;
}

- (NSString *)isEvent {
    return _isEvent;
}

- (NSString *)email {
    return _email;
}

- (CLLocationCoordinate2D)coordinate {
    return _coordinate;
}

@end
