//
//  ChatViewController.h
//  Mobo
//
//  Created by vishnuprasathg on 10/17/17.
//  Copyright © 2017 DCI. All rights reserved.
//

#import <UIKit/UIKit.h>

@protocol ChatViewControllerDelegate <NSObject>

-(void)reloadCell:(id)cell;
-(void)destroy:(NSString *)path;
-(NSString *)whoIs;
-(void)shareImage:(UIImage *)image fromCell:(id)cell;
-(void)resendOrDeleteMessage:(NSDictionary *)dictionary withCell:(id)cell;

@end

@interface ChatViewController : UIViewController<ChatViewControllerDelegate>

@property (nonatomic, weak) id <ChatViewControllerDelegate> delegate;
@property (weak, nonatomic) IBOutlet UILabel *statusLblOutlet;

- (IBAction)backBtnAction:(id)sender;
- (IBAction)videoCallBtnAction:(id)sender;
- (IBAction)cameraBtnAction:(id)sender;
- (IBAction)locationBtnaction:(id)sender;
- (IBAction)sentBtnAction:(id)sender;
@property (weak, nonatomic) IBOutlet UIView *sentMsgBaseViewOutlet;
@property (weak, nonatomic) IBOutlet UITableView *tableViewOutlet;
@property (weak, nonatomic) IBOutlet UITextField *chatTxtFldOutlet;
@property (weak, nonatomic) IBOutlet UILabel *titleLblOutlet;
@property (weak, nonatomic) IBOutlet UITextView *msgTextViewOutlet;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *sentMsgBaseViewHeightConstraint;
@property (weak, nonatomic) IBOutlet UIScrollView *baseScrollViewOutlet;
@property (weak, nonatomic) IBOutlet UIView *tableViewBaseViewOutlet;

@property (nonatomic, retain) NSDictionary * userDictionaryToChat;
@property (nonatomic, strong) NSString *type;
@property (nonatomic, strong) NSString *viewIdendify;

-(NSString *)getUserIncoming;
-(void)updateWithMessage:(NSDictionary *)dictionaryMessage;
-(void)cloudUploadFailuer:(NSString *)error;

-(void)openCamera;

-(void)openGallery;

-(void)openVideoRoll;

-(void)openVideoCamera;

@end
