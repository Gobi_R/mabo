//
//  AddCommentsView.h
//  Mabo
//
//  Created by vishnuprasathg on 11/13/17.
//  Copyright © 2017 DCI. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface AddCommentsView : UIView
@property (weak, nonatomic) IBOutlet UITextField *commentTxtFieldOutlet;

@property (strong, nonatomic) UIViewController * alertParentViewController;

- (IBAction)cancelbtnAction:(id)sender;
- (IBAction)okBtnAction:(id)sender;

@end
