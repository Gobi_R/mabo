//
//  NotificationViewController.m
//  Mabo
//
//  Created by Aravind Kumar on 27/12/17.
//  Copyright © 2017 DCI. All rights reserved.
//

#import "NotificationViewController.h"
#import "Client.h"
#import "NSDate+TimeAgo.h"
#import "UtilityGraphic.h"
#import "Utilities.h"
#import "UserProfileViewController.h"
#import "SinglePostViewController.h"


@interface NotificationViewController ()

@property (nonatomic, strong) NSArray *notificationList;
@property (nonatomic, strong) NSDictionary *dictionary;
@property BOOL isNotification;
@property BOOL isUser;

@end

@implementation NotificationViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
}
-(void)viewWillAppear:(BOOL)animated
{
    self.tableViewOutlet.delegate = self;
    self.tableViewOutlet.dataSource = self;
    
    [self updateProfile:nil];
    
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(updateProfile:) name:@"updateProfile" object:nil];
    //[[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(receiveConnectionChange:) name:@"connectionChange" object:nil];
}

-(void)viewWillDisappear:(BOOL)animated{
    [super viewWillDisappear:animated];
    [[NSNotificationCenter defaultCenter] removeObserver:self name:@"updateProfile" object:nil];
    //[[NSNotificationCenter defaultCenter] removeObserver:self name:@"connectionChange" object:nil];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

-(void)updateProfile:(NSNotificationCenter *)notification{
    self.notificationList = [Client sharedClient].user.notifications;
    
    NSMutableArray *array = [NSMutableArray arrayWithCapacity:[self.notificationList count]];
    NSEnumerator *enumerator = [self.notificationList reverseObjectEnumerator];
    for (id element in enumerator) {
        [array addObject:element];
    }
    self.notificationList = [NSArray arrayWithArray:array];
    [self.tableViewOutlet reloadSections:[NSIndexSet indexSetWithIndex:0] withRowAnimation:UITableViewRowAnimationFade];
}

#pragma mark - TABLEVIEW DELEGATE METHODS


-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    
        return self.notificationList.count;
   
}

-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    
    UITableViewCell *cellReal = [tableView dequeueReusableCellWithIdentifier:@"Cell"];
    
    NSDictionary *dict = [self.notificationList objectAtIndex:indexPath.row];
    
    if (self.notificationList.count > 0)
    {
        [self populateCell:dict Cell:cellReal];
    }
    
    return cellReal;
}

- (BOOL)tableView:(UITableView *)tableView shouldHighlightRowAtIndexPath:(NSIndexPath *)indexPath{
    return (self.notificationList.count>0);
}


-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{

    if([[Client sharedClient].reachAbility isReachable]){
        NSDictionary *dictionary = [self.notificationList objectAtIndex:indexPath.row];
        NSURL *deepLink = [NSURL URLWithString:[dictionary objectForKey:@"deep_link"]];
        NSArray *arrayLink = [deepLink pathComponents];
        if([[arrayLink objectAtIndex:1] isEqualToString:@"users"]){
            NSString *username = [arrayLink objectAtIndex:2];
            NSString *name = [dictionary objectForKey:@"title"];
            
            
//            UserProfileViewController *pvc = [[ProfileViewController alloc] initWithUser:username andName:name];
//            [self.navigationController pushViewController:pvc animated:YES];
            
            UserProfileViewController * userProfileVC = [self.storyboard instantiateViewControllerWithIdentifier:@"UserProfileViewController"];
            userProfileVC.userDict = dictionary;
            userProfileVC.viewIdendify = @"notification";
            userProfileVC.userName = username;
            //[self.navigationController pushViewController:userProfileVC animated:YES];
            [self.navigationController presentViewController:userProfileVC animated:YES completion:nil];
            
            
        }else{
            NSString *idWall = [arrayLink objectAtIndex:2];
            
            SinglePostViewController * singlePostVC = [self.storyboard instantiateViewControllerWithIdentifier:@"SinglePostViewController"];
            singlePostVC.idWall = idWall;
            
            [self.navigationController pushViewController:singlePostVC animated:YES];
            
//            DetailWallMessageViewController *dwmvc = [[DetailWallMessageViewController alloc] initWithWallId:idWall];
//            [self.navigationController pushViewController:dwmvc animated:YES];
            
        }
        
    }
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    
    UITableViewCell *cellReal = [tableView dequeueReusableCellWithIdentifier:@"Cell"];
    
    return 90;
}

-(void)populateCell:(NSDictionary *)dictionary Cell:(UITableViewCell *)cell{
    self.dictionary=dictionary;
//    [self.labelTimeStamp setHidden:YES];
    self.isUser=NO;
    
   // [self.activity setHidden:YES];
    
    UIImageView* profileImageView = [cell viewWithTag:1];
    UILabel * nameLbl = [cell viewWithTag:2];
    UILabel* contectLbl = [cell viewWithTag:3];
    UILabel* timeLbl = [cell viewWithTag:4];
    
    UIImage* profilePlaceHolder = [UIImage imageNamed:@"avatar2.png"];
    
    profileImageView.image = profilePlaceHolder;
    
    profileImageView.layer.cornerRadius = profileImageView.frame.size.height / 2;
    profileImageView.layer.masksToBounds = YES;
    
    self.isNotification=([dictionary objectForKey:@"deep_link"]!=nil);
    if(self.isNotification){
        NSURL *deepLink = [NSURL URLWithString:[dictionary objectForKey:@"deep_link"]];
        NSArray *arrayLink = [deepLink pathComponents];
        if([[arrayLink objectAtIndex:1] isEqualToString:@"users"]){
            self.isUser=YES;
//            self.ghostUserName = [arrayLink objectAtIndex:2];
        }
        //int valueTimeStamp = [[dictionary objectForKey:@"timestamp"] intValue];
        NSString * timeStampString = [dictionary valueForKey:@"timestamp"];
        NSTimeInterval _interval=[timeStampString doubleValue];
        NSDate *date = [NSDate dateWithTimeIntervalSince1970:_interval];
        NSDateFormatter *_formatter=[[NSDateFormatter alloc]init];
        [_formatter setDateFormat:@"dd MMMM yyyy"];
        NSString *_date=[_formatter stringFromDate:date];
        
//        timeLbl.text = _date;
//        [date timeAgo];
        
        NSMutableAttributedString*tmeString = [[Utilities sharedInstance]loadTimeDifferent:date];
        
        timeLbl.attributedText = tmeString;
        
        //[timeLbl setText:ago];
        //[self.labelTimeStamp setHidden:NO];
    }
    
    [nameLbl setText:(self.isNotification)?[dictionary objectForKey:@"title"]:[dictionary objectForKey:@"extended_name"]];
    //[self.labelDistance setText:@""];
    [profileImageView setClipsToBounds:YES];
    UIImage *defaultPicture;
    
    if(self.isUser || !self.isNotification){
       // defaultPicture= DEFAULT_PICTURE;
        defaultPicture = [UtilityGraphic imageWithImage:defaultPicture scaledToSize:CGSizeMake(profileImageView.frame.size.width,profileImageView.frame.size.height)];
        [profileImageView setImage:defaultPicture];
    }
    if(self.isNotification && !self.isUser){
       // defaultPicture= DEFAULT_PICTURE_NOTIFICATION;
        defaultPicture = [UtilityGraphic imageWithImage:defaultPicture scaledToSize:CGSizeMake(profileImageView.frame.size.width,profileImageView.frame.size.height)];
        [profileImageView setImage:defaultPicture];
    }
    
    if(!self.isNotification){
        if([dictionary objectForKey:@"lat"] && [dictionary objectForKey:@"lon"]){
            float lat = [[dictionary objectForKey:@"lat"] doubleValue];
            float lon = [[dictionary objectForKey:@"lon"] doubleValue];
            
            CLLocation *location = [[CLLocation alloc] initWithLatitude:lat longitude:lon];
            CLLocationDistance distance = [location distanceFromLocation:[Client sharedClient].locationManager.location];
            
            int meters = (int)distance;
            int kms=roundf(distance/1000.0f);
            
//            int mls = KMS_IN_MLS(kms);
//            int feet = MTS_IN_FTS(meters);
//            
//            if(mls<1){
//                [self.labelDistance setText:[NSString stringWithFormat:@"%i feet",feet]];
//            }else{
//                [self.labelDistance setText:[NSString stringWithFormat:@"%i miles",mls]];
//            }
//            
//            if(feet<0){[self.labelDistance setText:@""];}
//            
//            [self.labelName setFrame:CGRectMake(MARGIN*2+self.picture.frame.size.width, MARGIN*1.5, [UIScreen mainScreen].bounds.size.width-(MARGIN*4+self.picture.frame.size.width), 20)];
        }else{
            if([dictionary objectForKey:@"status"]){
//                [self.labelDistance setText:([[dictionary objectForKey:@"status"] intValue]==0)?@"Joining":@"Maybe"];
//                [self.labelName setFrame:CGRectMake(MARGIN*2+self.picture.frame.size.width, MARGIN*1.5, [UIScreen mainScreen].bounds.size.width-(MARGIN*4+self.picture.frame.size.width), 20)];
            }else{
//                [self.labelName setFrame:CGRectMake(MARGIN*2+self.picture.frame.size.width, MARGIN*1.5, [UIScreen mainScreen].bounds.size.width-(MARGIN*4+self.picture.frame.size.width), 20)];
            }
        }
    }else{
        [contectLbl setText:[dictionary objectForKey:@"sub_title"]];
    }
    
    if([dictionary objectForKey:@"picture_path"] && ![[dictionary objectForKey:@"picture_path"] isEqual:[NSNull null]]){
        if(![[dictionary objectForKey:@"picture_path"] isEqualToString:@"null"] && ![[dictionary objectForKey:@"picture_path"] isEqualToString:@""]){
            //[self.activity setHidden:NO];
            [[Client sharedClient] getPicture:[dictionary objectForKey:@"picture_path"] from:[dictionary objectForKey:@"picture_path"] withCompletition:^(UIImage *image, BOOL isInCache, id sender) {
                if(sender==[dictionary objectForKey:@"picture_path"]){
                    //[self.activity setHidden:YES];
                    image = [UtilityGraphic imageWithImage:image scaledToSize:CGSizeMake(profileImageView.frame.size.width,profileImageView.frame.size.height)];
                    [profileImageView setImage:image];
                }
            }];
        }
    }else{
        [profileImageView setImage:defaultPicture];
    }
    if(self.isNotification && ![[dictionary objectForKey:@"picture"] isEqualToString:@""]){
        if(self.isUser){
           // [self.activity setHidden:NO];
            [[Client sharedClient] getPicture:[dictionary objectForKey:@"picture"] from:[dictionary objectForKey:@"id"] withCompletition:^(UIImage *image, BOOL isInCache, id sender) {
                if(sender==[dictionary objectForKey:@"id"] && image){
                   // [self.activity setHidden:YES];
                    image = [UtilityGraphic imageWithImage:image scaledToSize:CGSizeMake(profileImageView.frame.size.width,profileImageView.frame.size.height)];
                    [profileImageView setImage:image];
                }else{
                   // [self.activity setHidden:YES];
                }
            }];
        }
    }
    else
    {
        // avatar2.png
        [profileImageView setImage:[UIImage imageNamed:@"avatar2"]];
    }
    
    if(!self.isNotification){
//        if([[dictionary objectForKey:@"username"] isEqualToString:[Client sharedClient].user.email]){
//            [self.arrow setHidden:YES];
//            self.labelName.text=@"Me";
//            [self setBackgroundColor:[COLOR_MB_GREEN colorWithAlphaComponent:.5]];
//            [self setUserInteractionEnabled:NO];
//        }else{
//            [self.arrow setHidden:NO];
//            [self setBackgroundColor:COLOR_MB_GRAYALPHA];
//            [self setUserInteractionEnabled:YES];
//        }
       // self.ghostUserName = [dictionary objectForKey:@"username"];
       // [self lunchObserv];
    }else{
        if(self.isUser){
           // [self lunchObserv];
            //[self.labelConnect setHidden:NO];
        }else{
//            [self.connectRef removeAllObservers];
//            [self.connectRefObserv removeAllObservers];
//            [self.lastOnlineRef removeAllObservers];
//            self.connectRef=nil;
//            self.connectRefObserv=nil;
//            self.lastOnlineRef=nil;
//            [self.labelConnect setHidden:YES];
        }
    }
}


/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

- (IBAction)backBtnAction:(id)sender {
    
    [self.navigationController popViewControllerAnimated:YES];
}
@end
