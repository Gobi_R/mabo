   //
//  HelpsViewController.m
//  Mabo
//
//  Created by vishnuprasathg on 11/22/17.
//  Copyright © 2017 DCI. All rights reserved.
//

#import "HelpsViewController.h"
#import "Client.h"
#import "LoadingView.h"
#import "Utilities.h"

#define SERVER @"http://34.230.105.103/"


@interface HelpsViewController ()<UITextViewDelegate>

@property (nonatomic, strong) LoadingView *loading;

@end

@implementation HelpsViewController

{
    NSString* methodType;
}

- (void)viewDidLoad {
    [super viewDidLoad];
    
//http://34.230.105.103/mhelp.php
//http://34.230.105.103/mfaq.php
//http://34.230.105.103/mabout.php
//http://34.230.105.103/mprivacy.php
//http://34.230.105.103/mterms.php
//http://34.230.105.103/contact.php
    
//case 0:
//    binding.webview.loadUrl(Constants.URL_BASE+"privacy/mprivacy");
//    break;
//case 1:
//    binding.webview.loadUrl(Constants.URL_BASE+"terms/mterms");
//    break;
//case 2:
//    binding.webview.loadUrl(Constants.URL_BASE+"help/mhelp");
//    break;
//case 3:
//    binding.webview.loadUrl(Constants.URL_BASE+"about/mabout");
//    break;
//case 4:
//    binding.webview.loadUrl(Constants.URL_BASE+"faq/mfaq");
//    break;
//case 5:
//    binding.webview.loadUrl(Constants.URL_BASE+"contact");
//    break;

    
    if ([self.viewIdendify isEqualToString:@"termsOfUse"]) {
        
        self.titleLblOutlet.text = @"Terms of use";
        methodType = @"terms/mterms";
        self.feedBackBaseView.hidden = YES;
        self.webViewOutlet.hidden = NO;
    }
    else if ([self.viewIdendify isEqualToString:@"help"]) {
        
        self.titleLblOutlet.text = @"Help";
        methodType = @"help/mhelp";
        self.feedBackBaseView.hidden = YES;
        self.webViewOutlet.hidden = NO;
    }
    else if ([self.viewIdendify isEqualToString:@"about"]) {
        
        self.titleLblOutlet.text = @"About";
        methodType = @"about/mabout";
        self.feedBackBaseView.hidden = YES;
        self.webViewOutlet.hidden = NO;
    }
    else if ([self.viewIdendify isEqualToString:@"faq"]) {
        
        self.titleLblOutlet.text = @"FAQ";
        methodType = @"faq/mfaq";
        self.feedBackBaseView.hidden = YES;
        self.webViewOutlet.hidden = NO;
    }
    else if ([self.viewIdendify isEqualToString:@"contact"]) {
        
        self.titleLblOutlet.text = @"Contact";
        methodType = @"contact";
        self.feedBackBaseView.hidden = YES;
        self.webViewOutlet.hidden = NO;
    }
    else if ([self.viewIdendify isEqualToString:@"privacy"]) {
        
        self.titleLblOutlet.text = @"Privacy policy";
        methodType = @"privacy/mprivacy";
        self.feedBackBaseView.hidden = YES;
        self.webViewOutlet.hidden = NO;
    }
    
    else if ([self.viewIdendify isEqualToString:@"feedBack"]) {
        
        self.titleLblOutlet.text = @"FeedBack";
        self.feedBackBaseView.hidden = NO;
        self.webViewOutlet.hidden = YES;
        self.textViewOutlet.delegate = self;
        
        self.textViewOutlet.text = @"Enter the feedback message here";
        self.textViewOutlet.textColor = [UIColor lightGrayColor];
        self.textViewOutlet.delegate = self;
        
        self.textViewOutlet.layer.cornerRadius = 10;
        self.textViewOutlet.layer.masksToBounds = YES;
        
        self.submitBtnOutlet.layer.cornerRadius = 10;
        self.submitBtnOutlet.layer.masksToBounds = YES;
        
        [self.textViewOutlet.layer setBorderColor:[UIColor colorWithRed:122/255.0f green:222/255.0f blue:225/255.0f alpha:1.0f].CGColor];
        [self.textViewOutlet.layer setBorderWidth:2.0];

    }
    
    NSLog(@"%@",methodType);
    
    NSString *callURL = [NSString stringWithFormat:@"http://www.mabo-app.com/%@",methodType];
    NSLog(@"URL:%@",callURL);
    NSURL *url = [NSURL URLWithString:callURL];
    NSURLRequest *urlRequest = [NSURLRequest requestWithURL:url];
    [self.webViewOutlet loadRequest:urlRequest];
    
    // Do any additional setup after loading the view.
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (BOOL) textViewShouldBeginEditing:(UITextView *)textView
{
    if (textView == self.textViewOutlet) {
        
        if (self.textViewOutlet.text.length > 0 && [self.textViewOutlet.text  isEqual: @"Enter the feedback message here"]) {
            
            self.textViewOutlet.text = @"";
            self.textViewOutlet.textColor = [UIColor blackColor];
        }
        
    }
    
    return YES;
}

-(void) textViewDidChange:(UITextView *)textView
{
    if (textView == self.textViewOutlet) {
        
        if(self.textViewOutlet.text.length == 0){
            self.textViewOutlet.textColor = [UIColor lightGrayColor];
            self.textViewOutlet.text = @"Enter the feedback message here";
            [self.textViewOutlet resignFirstResponder];
            
        }
        
    }
}

- (BOOL)textView:(UITextView *)textView shouldChangeTextInRange:(NSRange)range replacementText:(NSString *)text {
    
    if([text isEqualToString:@"\n"]) {
        
        if(self.textViewOutlet.text.length == 0){
            self.textViewOutlet.textColor = [UIColor lightGrayColor];
            self.textViewOutlet.text = @"Enter the feedback message here";
            [self.textViewOutlet resignFirstResponder];
        }
        [textView resignFirstResponder];
        return NO;
    }
    return YES;
}


- (IBAction)backBtnAction:(id)sender {
    
    [self.navigationController popViewControllerAnimated:YES];
}
- (IBAction)submitBtnAction:(id)sender {
    
    NSString* feedBack = self.textViewOutlet.text;
    
    
    if (feedBack.length >= 10 && ![feedBack isEqualToString:@"Enter the feedback message here"]) {
        
        self.loading = [[LoadingView alloc] initWithFrame:[UIScreen mainScreen].bounds];
        [self.view addSubview:self.loading];
        [self.loading animate:YES];
        
        [[Client sharedClient] feedBackSubmit:feedBack completition:^(NSDictionary *responceDict, NSError *error) {
            [self.loading animate:NO];
            if(responceDict){
                
                NSLog(@"dict:%@",responceDict);
                
                NSString* responce = [responceDict objectForKey:@"feedback"];
                
                self.textViewOutlet.text = @"Enter the feedback message here";
                self.textViewOutlet.textColor = [UIColor lightGrayColor];
                
                [[Utilities sharedInstance] showCustomAlertView:self.view title:@"Mabo" message:responce controller:self reason:@"feedback"];
                
            }else{
                NSLog(@"error:%@",error);
            }
        }];
        
    }
    else
    {
        [[Utilities sharedInstance] showCustomAlertView:self.view title:@"Mabo" message:@"Write some feedback" controller:self reason:@"feedBack"];
        return;
    }
    
}
@end
