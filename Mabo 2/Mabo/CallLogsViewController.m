//
//  CallLogsViewController.m
//  Mabo
//
//  Created by Aravind Kumar on 05/01/18.
//  Copyright © 2018 DCI. All rights reserved.


#import "CallLogsViewController.h"
#import "Client.h"
#import "LoadingView.h"
#import "SharedVariables.h"
//#import "UIImageView+AFNetworking.h"
#import "Utilities.h"
#import "UIImageView+WebCache.h"

@interface CallLogsViewController ()<UITableViewDelegate,UITableViewDataSource>

@property (nonatomic, strong) NSMutableArray *logListArray;
@property (nonatomic, strong) LoadingView *loading;
@property (nonatomic, strong) NSString *totalPage;

@end

@implementation CallLogsViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    self.loading = [[LoadingView alloc] initWithFrame:[UIScreen mainScreen].bounds];
    [self.view addSubview:self.loading];
    [self.loading animate:YES];
    
    
   // [[Client sharedClient]]
    [[Client sharedClient] getCallLogList:@"1" completition:^(BOOL success, NSError *error) {
        
        if (success) {
            [self.loading animate:NO];
            
//            NSLog(@"%@",[SharedVariables sharedInstance].myPostResponseDict);
//            self.dictionaryUser = [[SharedVariables sharedInstance].myPostResponseDict objectForKey:@"data"];
//            
            self.logListArray=[[[SharedVariables sharedInstance].callLogListResponseDict objectForKey:@"loglist"] mutableCopy];
            NSLog(@"WallMessages:%@",self.logListArray);
            
            self.totalPage = [[SharedVariables sharedInstance].callLogListResponseDict objectForKey:@"totalpages"];
            
            [SharedVariables sharedInstance].callListPage = 1;
            
            self.tableViewOutlet.reloadData;
        
        }
        else
        {
            [self.loading animate:NO];
        }
    }];
    
}
    // Do any additional setup after loading the view.

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - TABLEVIEW DELEGATE METHODS


-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    
    return self.logListArray.count;
    
}

-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    
    UITableViewCell *cellReal = [tableView dequeueReusableCellWithIdentifier:@"Cell"];
    
    if (self.logListArray.count > 0) {
        
        NSDictionary* logDict = [self.logListArray objectAtIndex:indexPath.row];
        
        NSLog(@"LogDict:%@",logDict);
        
                UIImageView* profileImageView = [cellReal viewWithTag:1];
                UILabel * nameLbl = [cellReal viewWithTag:2];
                UILabel* lastSeen = [cellReal viewWithTag:3];
                UIImageView* profileImageV = [cellReal viewWithTag:4];
                UILabel * durationLbl = [cellReal viewWithTag:5];
        
        NSString* type = [logDict objectForKey:@"type"];
        
        if ([type isEqualToString:@"missed"]) {
            
            NSString* fromUser = [logDict objectForKey:@"fromuser"];
            NSString* startDataTime = [logDict objectForKey:@"startdatetime"];
            NSString* endDateTime = [logDict objectForKey:@"enddatetime"];
            NSString* duration = [logDict objectForKey:@"duration"];
            NSString* fromImage = [logDict objectForKey:@"fromimage"];
            
            if (fromUser == nil || fromUser == (id)[NSNull null]) {
                // nil branch
            } else {
                nameLbl.text = fromUser;
            }
            
            if (duration == nil || duration == (id)[NSNull null]) {
                // nil branch
            } else {
                durationLbl.text = duration;
            }
            
            UIImage* image = [UIImage imageNamed:@"ic_callmissed"];
            profileImageV.image = image;
            
            if (![startDataTime isEqualToString:@"<null>"]) {
                
                NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
                [dateFormatter setDateFormat:@"yyyy-MM-dd HH:mm:ss"];
                NSDate *startDate = [dateFormatter dateFromString:startDataTime];
                NSLog(@"%@", startDate);
                
                NSTimeInterval interval = [startDate timeIntervalSince1970];
                NSLog(@"interval=%f",interval) ;
                startDate = [NSDate dateWithTimeIntervalSince1970:interval] ;
                [dateFormatter setDateFormat:@"yyyy/MM/dd "] ;
                NSLog(@"result: %@", [dateFormatter stringFromDate:startDate]) ;
                
                NSMutableAttributedString*tmeString = [[Utilities sharedInstance]loadTimeLocalDifferent:startDate];
                lastSeen.attributedText = tmeString;
            }
            
            
            if (!fromImage)
            {
                
                NSString * urlstring = [NSString stringWithFormat:@"http://34.230.105.103:8002/%@", fromImage];
                NSURL *imageURL = [NSURL URLWithString:urlstring];
                //            NSData *imageData = [NSData dataWithContentsOfURL:imageURL];
                //            UIImage *image = [UIImage imageWithData:imageData];
                //            profileImageView.image = image;
                [profileImageView setImageWithURL:imageURL placeholderImage:[UIImage imageNamed:@"avatar3.png"]];
                
                profileImageView.layer.cornerRadius = profileImageView.frame.size.height / 2;
                profileImageView.layer.masksToBounds = YES;
                 
            }
            else
            {
                
                profileImageView.image = [UIImage imageNamed:@"avatar3.png"];
            }
        
        }
        else if ([type isEqualToString:@"incoming"])
        {
            
            NSString* fromUser = [logDict objectForKey:@"fromuser"];
            NSString* startDataTime = [logDict objectForKey:@"startdatetime"];
            NSString* endDateTime = [logDict objectForKey:@"enddatetime"];
            NSString* duration = [logDict objectForKey:@"duration"];
            NSString* fromImage = [logDict objectForKey:@"fromimage"];
            
            if (fromUser == nil || fromUser == (id)[NSNull null]) {
                // nil branch
            } else {
                nameLbl.text = fromUser;
            }
            
            if (duration == nil || duration == (id)[NSNull null]) {
                // nil branch
            } else {
                durationLbl.text = duration;
            }
            
            UIImage* image = [UIImage imageNamed:@"ic_callincome"];
            profileImageV.image = image;
            
            if (![startDataTime isEqualToString:@"<null>"]) {
                
                NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
                [dateFormatter setDateFormat:@"yyyy-MM-dd HH:mm:ss"];
                NSDate *startDate = [dateFormatter dateFromString:startDataTime];
                NSLog(@"%@", startDate);
                
                NSTimeInterval interval = [startDate timeIntervalSince1970];
                NSLog(@"interval=%f",interval) ;
                startDate = [NSDate dateWithTimeIntervalSince1970:interval] ;
                [dateFormatter setDateFormat:@"yyyy/MM/dd "] ;
                NSLog(@"result: %@", [dateFormatter stringFromDate:startDate]) ;
                
                NSMutableAttributedString*tmeString = [[Utilities sharedInstance]loadTimeLocalDifferent:startDate];
                lastSeen.attributedText = tmeString;
            }
            
            
            if (!fromImage)
            {
                
                NSString * urlstring = [NSString stringWithFormat:@"http://34.230.105.103:8002/%@", fromImage];
                NSURL *imageURL = [NSURL URLWithString:urlstring];
                //            NSData *imageData = [NSData dataWithContentsOfURL:imageURL];
                //            UIImage *image = [UIImage imageWithData:imageData];
                //            profileImageView.image = image;
                [profileImageView setImageWithURL:imageURL placeholderImage:[UIImage imageNamed:@"avatar3.png"]];
                
                profileImageView.layer.cornerRadius = profileImageView.frame.size.height / 2;
                profileImageView.layer.masksToBounds = YES;
                
                
            }
            else
            {
                
                profileImageView.image = [UIImage imageNamed:@"avatar3.png"];
            }
            
        }
        else if ([type isEqualToString:@"outgoing"])
        {
            
            NSString* toUser = [logDict objectForKey:@"touser"];
            NSString* startDataTime = [logDict objectForKey:@"startdatetime"];
            NSString* endDateTime = [logDict objectForKey:@"enddatetime"];
            NSString* duration = [logDict objectForKey:@"duration"];
            NSString* toImage = [logDict objectForKey:@"toimage"];
            
            if (toUser == nil || toUser  == (id)[NSNull null]) {
                // nil branch
            } else {
                nameLbl.text = toUser;
            }
            
            if (duration == nil || duration == (id)[NSNull null]) {
                // nil branch
            } else {
                durationLbl.text = duration;
            }
            
            UIImage* image = [UIImage imageNamed:@"ic_calloutgoing"];
            profileImageV.image = image;
            
            if (![startDataTime isEqualToString:@"<null>"]) {
                
                NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
                [dateFormatter setDateFormat:@"yyyy-MM-dd HH:mm:ss"];
                NSDate *startDate = [dateFormatter dateFromString:startDataTime];
                NSLog(@"%@", startDate);
                
                NSTimeInterval interval = [startDate timeIntervalSince1970];
                NSLog(@"interval=%f",interval) ;
                startDate = [NSDate dateWithTimeIntervalSince1970:interval] ;
                [dateFormatter setDateFormat:@"yyyy/MM/dd "] ;
                NSLog(@"result: %@", [dateFormatter stringFromDate:startDate]) ;
                
                NSMutableAttributedString*tmeString = [[Utilities sharedInstance]loadTimeLocalDifferent:startDate];
                lastSeen.attributedText = tmeString;
            }
            
            if (!toImage)
            {
                
                NSString * urlstring = [NSString stringWithFormat:@"http://34.230.105.103:8002/%@", toImage];
                NSURL *imageURL = [NSURL URLWithString:urlstring];
                //            NSData *imageData = [NSData dataWithContentsOfURL:imageURL];
                //            UIImage *image = [UIImage imageWithData:imageData];
                //            profileImageView.image = image;
                [profileImageView setImageWithURL:imageURL placeholderImage:[UIImage imageNamed:@"avatar3.png"]];
                
                profileImageView.layer.cornerRadius = profileImageView.frame.size.height / 2;
                profileImageView.layer.masksToBounds = YES;
                
                
            }
            else
            {
              profileImageView.image = [UIImage imageNamed:@"avatar3.png"];
            }
            
        }
        NSLog(@"%@",cellReal);
    }
    return cellReal;
}

- (BOOL)tableView:(UITableView *)tableView shouldHighlightRowAtIndexPath:(NSIndexPath *)indexPath{
    return (self.logListArray.count>0);
}

-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    //    if([[Client sharedClient].reachAbility isReachable]){
    //        //        ProfileViewController *profile = [[ProfileViewController alloc] initWithUser:[[self.visibleUserList objectAtIndex:indexPath.row] objectForKey:@"username"] andName:[[self.visibleUserList objectAtIndex:indexPath.row] objectForKey:@"extended_name"]];
    //        //        [self.navigationController pushViewController:profile animated:YES];
    //    }

//    NSString *email = [[self.chatList objectAtIndex:indexPath.row] objectForKey:@"username"];
//
//
//    int indexProfileFoto= [[[self.chatList objectAtIndex:indexPath.row] objectForKey:@"picture_index_default"] intValue];
//    NSArray *arrayPictures = [[self.chatList objectAtIndex:indexPath.row] objectForKey:@"pictures"];
//    NSString *pathPicture=@"";
//    for(unsigned i=0;i<arrayPictures.count;i++){
//        NSDictionary *dic = [arrayPictures objectAtIndex:i];
//        if([[dic objectForKey:@"pic_index"] intValue]==indexProfileFoto){
//            pathPicture = [dic objectForKey:@"web_path"];
//            break;
//        }
//    }
//
//    if([[self.chatList objectAtIndex:indexPath.row] objectForKey:@"picture_path"]){
//        pathPicture=[[self.chatList objectAtIndex:indexPath.row] objectForKey:@"picture_path"];
//    }
//
//    NSString *extended_name = [[self.chatList objectAtIndex:indexPath.row] objectForKey:@"extended_name"];
//
//    if (extended_name == nil) {
//
//        extended_name = @"";
//    }
//
//    NSDictionary *dictionaryUser = @{@"email":email,@"picture":pathPicture,@"extended_name":extended_name};
//
//    ChatViewController * chatVC = [self.storyboard instantiateViewControllerWithIdentifier:@"ChatViewController"];
//    chatVC.userDictionaryToChat = dictionaryUser;
//    [self.navigationController pushViewController:chatVC animated:YES];
    
}

#pragma mark - UIScrollDelegate

- (void)scrollViewWillEndDragging:(UIScrollView*)aScrollView withVelocity:(CGPoint)velocity targetContentOffset:(inout CGPoint )targetContentOffset
{
    CGPoint offset = aScrollView.contentOffset;
    CGRect bounds = aScrollView.bounds;
    CGSize size = aScrollView.contentSize;
    UIEdgeInsets inset = aScrollView.contentInset;
    float y = offset.y + bounds.size.height - inset.bottom;
    float h = size.height;
    
    float reload_distance = 10;
    if(y > h + reload_distance)
    {
        [self loadMorePage];
    }
}

-(void)loadMorePage
{
    NSInteger currentPage = [self.totalPage integerValue];
    
    if ([SharedVariables sharedInstance].callListPage <= currentPage) {
        
    }
    else
    {
        [SharedVariables sharedInstance].callListPage = [SharedVariables sharedInstance].callListPage + 1;
        NSLog(@"page:%d",[SharedVariables sharedInstance].callListPage);
        
        self.loading = [[LoadingView alloc] initWithFrame:[UIScreen mainScreen].bounds];
        [self.view addSubview:self.loading];
        [self.loading animate:YES];
        
        [[Client sharedClient] getCallLogList:[NSString stringWithFormat:@"%d",[SharedVariables sharedInstance].callListPage] completition:^(BOOL success, NSError *error) {
            
            if (success) {
                [self.loading animate:NO];
                
                //            NSLog(@"%@",[SharedVariables sharedInstance].myPostResponseDict);
                //            self.dictionaryUser = [[SharedVariables sharedInstance].myPostResponseDict objectForKey:@"data"];
                //
                NSArray * callListArray = [[[SharedVariables sharedInstance].morePageCallLogListResponseDict objectForKey:@"loglist"] mutableCopy];
                self.logListArray = [self.logListArray arrayByAddingObjectsFromArray:callListArray];
                
                NSLog(@"WallMessages:%@",self.logListArray);
                
                //self.totalPage = [[SharedVariables sharedInstance].callLogListResponseDict objectForKey:@"totalpages"];
                
                self.tableViewOutlet.reloadData;
                
            }
            else
            {
                [self.loading animate:NO];
            }
        }];
    }
    
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

- (IBAction)backBtnAction:(id)sender {
    
    [self.navigationController popViewControllerAnimated:YES];
}
@end
