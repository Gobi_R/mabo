//
//  UserProfileViewController.h
//  Mabo
//
//  Created by vishnuprasathg on 11/14/17.
//  Copyright © 2017 DCI. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface UserProfileViewController : UIViewController<UITableViewDelegate,UITableViewDataSource>
@property (strong, nonatomic) UIWindow *window;
@property (weak, nonatomic) IBOutlet UILabel *titleLblOutlet;
@property (weak, nonatomic) IBOutlet UILabel *userNameLblOutlet;

@property (weak, nonatomic) IBOutlet UIImageView *profileImageViewOutlet;
@property (weak, nonatomic) IBOutlet UILabel *visibleLblOutlet;
@property (weak, nonatomic) IBOutlet UILabel *genderLblOutlet;
@property (weak, nonatomic) IBOutlet UILabel *dateOfBirthLblOutlet;
@property (weak, nonatomic) IBOutlet UILabel *interestedInLblOutlet;
@property (weak, nonatomic) IBOutlet UIButton *blockThisUserBtnOutlet;

@property (weak, nonatomic) IBOutlet UITableView *hobbiesTableView;
@property (weak, nonatomic) IBOutlet UITableView *occupationsTableView;
@property (weak, nonatomic) IBOutlet UITableView *schoolsTableView;

@property (weak, nonatomic) IBOutlet UIView *hobbiesBaseViewOutlet;
@property (weak, nonatomic) IBOutlet UIView *occupationsBaseViewOutlet;
@property (weak, nonatomic) IBOutlet UIView *schoolsBaseViewOutlet;

@property (weak, nonatomic) IBOutlet NSLayoutConstraint *schoolsBaseViewHeightConstraint;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *occupationsBaseViewHeightConstraint;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *hobbiesBaseViewHeightConstraint;


@property (weak, nonatomic) IBOutlet NSLayoutConstraint *schoolsTableViewHeightConstraint;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *occupationsTableViewHeightConstraint;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *hobbiesTableViewHeightConstraint;

@property (weak, nonatomic) IBOutlet UIButton *favoriteBtnOutlet;
@property (weak, nonatomic) IBOutlet UIButton *locationBtnOutlet;
@property (weak, nonatomic) IBOutlet UIButton *chatBtnOutlet;

- (IBAction)favoriteBtnAction:(id)sender;
- (IBAction)locationBtnAction:(id)sender;
- (IBAction)chatBtnAction:(id)sender;

- (IBAction)blockThisUserBtnAction:(id)sender;
- (IBAction)backBtnAction:(id)sender;

@property (nonatomic, retain) NSDictionary * userDict;
@property (nonatomic, retain) NSString * viewIdendify;
@property (nonatomic, retain) NSString * userName;

-(void)blockedSuccessfully;

@end
