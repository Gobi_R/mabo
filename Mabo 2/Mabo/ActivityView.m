//
//  ActivityView.m
//  Mabo
//
//  Created by Steewe MacBook Pro on 28/04/15.
//  Copyright (c) 2015 Steewe. All rights reserved.
//

#import "ActivityView.h"
#import "PrefixHeader.pch"

@interface ActivityView ()

    @property BOOL isShow;
    @property int radiusIntern;

@end

@implementation ActivityView


-(instancetype)initWithFrame:(CGRect)frame{
    self=[super initWithFrame:frame];
    if(self){
        [self setBackgroundColor:[UIColor clearColor]];
        [self setHidden:YES];
        self.colorBase = COLOR_MB_GREEN;
        self.colorCircles= COLOR_MB_LIGHTGRAY;
        self.lineWidthCircles=2;
        self.radiusIntern=self.frame.size.width/4;
        //[[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(startAnimation) name:@"appResignActive" object:nil];
    }
    
    return self;
}

-(void)willMoveToWindow:(UIWindow *)newWindow{
    [super willMoveToWindow:newWindow];
    if(!self.isShow){
        [self startAnimation];
    }
}

-(void)willRemoveSubview:(UIView *)subview{
    [super willRemoveSubview:subview];
    //[[NSNotificationCenter defaultCenter] removeObserver:self name:@"appResignActive" object:nil];
    self.isShow=NO;
}


-(void)setCenterActivity:(CGPoint)centerPoint{
    self.centerPoint=centerPoint;
    self.frame = CGRectMake(centerPoint.x-self.frame.size.width/2, centerPoint.y-self.frame.size.height/2, self.frame.size.width, self.frame.size.height);
    [self setNeedsDisplay];
}

- (void)drawRect:(CGRect)rect {
    [super drawRect:rect];
    CGContextRef currentContext = UIGraphicsGetCurrentContext();
    CGContextSetFillColorWithColor(currentContext, self.colorBase.CGColor);
    UIBezierPath *path = [UIBezierPath bezierPathWithRoundedRect:self.bounds cornerRadius:self.frame.size.width/2];
    CGContextAddPath(currentContext, path.CGPath);
    CGContextFillPath(currentContext);
}

-(void)show{
    [self createCircle];
    [self setHidden:NO];
}

-(void)hide{
    [self setHidden:YES];
    [self removeFromSuperview];
}

-(void)startAnimation{
    self.isShow=YES;
    
    for(CAShapeLayer *circle in self.circles.sublayers){
        
        int randomTime = arc4random_uniform(2)+1;
        
        CABasicAnimation *rotateAnimation = [CABasicAnimation animationWithKeyPath:@"transform.rotation.z"];
        rotateAnimation.byValue = [NSNumber numberWithDouble:M_PI];
        rotateAnimation.duration = randomTime;
        rotateAnimation.repeatCount=INFINITY;
        
        [circle addAnimation:rotateAnimation forKey:@"show"];
    }
    
}

-(void)createCircle{
    if(self.circles){[self.circles removeFromSuperlayer];}
    self.circles = [CAShapeLayer layer];
    [self.layer addSublayer:self.circles];
    float valueAlpha = 1;
    for(unsigned i=0;i<3;i++){
        int molt = (i+1);
        CGSize sizeCircle = CGSizeMake(self.radiusIntern*molt,self.radiusIntern*molt);
        CGRect rectCircle = CGRectMake((self.frame.size.width-sizeCircle.width)/2, (self.frame.size.height-sizeCircle.height)/2, sizeCircle.width, sizeCircle.height);
        
        CAShapeLayer *circle = [CAShapeLayer layer];
        [circle setFrame:rectCircle];
        UIBezierPath *path = [UIBezierPath bezierPathWithOvalInRect:CGRectMake(0, 0, sizeCircle.width, sizeCircle.height)];
        [circle setFillColor:[UIColor clearColor].CGColor];
        [circle setStrokeColor:self.colorCircles.CGColor];
        [circle setStrokeEnd:self.lineWidthCircles];
        [circle setPath:path.CGPath];
        
        [self createMoon:circle];
        
        [self.circles addSublayer:circle];
        valueAlpha-=.18;
        int randomAngle = arc4random_uniform(360);
        //int randomTime = arc4random_uniform(12)+6;
        [circle setTransform:CATransform3DMakeRotation(DEGREES_TO_RADIANS(randomAngle), 0, 0, 1)];
        /*
         
        CABasicAnimation *rotateAnimation = [CABasicAnimation animationWithKeyPath:@"transform.rotation.z"];
        rotateAnimation.byValue = [NSNumber numberWithDouble:M_PI];
        rotateAnimation.duration = randomTime;
        rotateAnimation.repeatCount=INFINITY;
        
        [circle.layer addAnimation:rotateAnimation forKey:@"show"];
         */
    }

    [self startAnimation];
}
-(void)createMoon:(CAShapeLayer *)layerCircle{
    
    float radius=(self.radiusIntern/5);
    
    CGRect rectLeft = CGRectMake(-radius, layerCircle.frame.size.height/2-radius, radius*2, radius*2);
    CGRect rectRight = CGRectMake(layerCircle.frame.size.width-radius, layerCircle.frame.size.height/2-radius, radius*2, radius*2);

    UIBezierPath *path = [UIBezierPath bezierPathWithOvalInRect:rectLeft];

    CAShapeLayer *moonLeft = [CAShapeLayer layer];
    [moonLeft setFillColor:[UIColor lightGrayColor].CGColor];
    [moonLeft setPath:path.CGPath];
    [layerCircle addSublayer:moonLeft];

    path = [UIBezierPath bezierPathWithOvalInRect:rectRight];

    CAShapeLayer *moonRight = [CAShapeLayer layer];
    [moonRight setFillColor:[UIColor lightGrayColor].CGColor];
    [moonRight setPath:path.CGPath];
    [layerCircle addSublayer:moonRight];
}

@end
