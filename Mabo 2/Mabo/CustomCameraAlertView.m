//
//  CustomCameraAlertView.m
//  Mobo
//
//  Created by vishnuprasathg on 10/24/17.
//  Copyright © 2017 DCI. All rights reserved.
//

#import "CustomCameraAlertView.h"
#import "Utilities.h"
#import "ProfileViewController.h"
#import "CreatPostViewController.h"

@implementation CustomCameraAlertView

/*
// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect {
    // Drawing code
}
*/

- (IBAction)cameraBtnAction:(id)sender {
    
    if ([_alertParentViewController isKindOfClass: ProfileViewController.class]) {
        
        [[Utilities sharedInstance] hideCustomAlertView:self.superview];
        
        ProfileViewController * profileVC = _alertParentViewController;
        
        [profileVC openCamera];
        
        
    }
    else if ([_alertParentViewController isKindOfClass: CreatPostViewController.class]) {
        
        [[Utilities sharedInstance] hideCustomAlertView:self.superview];
        
        CreatPostViewController * creatPostVC = _alertParentViewController;
        
        [creatPostVC openCamera];
        
        
    }
}

- (IBAction)galleryBtnAction:(id)sender {
    
    if ([_alertParentViewController isKindOfClass: ProfileViewController.class]) {
        
            [[Utilities sharedInstance] hideCustomAlertView:self.superview];
            
            ProfileViewController * profileVC = _alertParentViewController;
            
            [profileVC openGallery];

        
    }
    else if ([_alertParentViewController isKindOfClass: CreatPostViewController.class]) {
        
        [[Utilities sharedInstance] hideCustomAlertView:self.superview];
        
        CreatPostViewController * creatPostVC = _alertParentViewController;
        
        [creatPostVC openGallery];
        
        
    }
}

- (IBAction)cancelBtnAction:(id)sender{
    
    [[Utilities sharedInstance] hideCustomAlertView:self.superview];
}

@end
