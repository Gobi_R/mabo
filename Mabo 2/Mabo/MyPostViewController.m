//
//  MyPostViewController.m
//  Mobo
//
//  Created by vishnuprasathg on 10/23/17.
//  Copyright © 2017 DCI. All rights reserved.
//

#import "MyPostViewController.h"
#import "ProfileViewController.h"
#import "CreatPostViewController.h"
#import "MyPostTableViewCell.h"
#import "Client.h"
//#import "UIImageView+AFNetworking.h"
#import "SharedVariables.h"
#import "LoadingView.h"
#import "ShowPhotoVideoViewController.h"
#import "Utilities.h"
//#import "UIImageView+WebCache.h"
#import "MapViewController.h"
#import "LikesViewController.h"
#import "CommentsViewController.h"
#import "JoinsViewController.h"
#import "CreatPostViewController.h"
#import "MyPostTableViewCell.h"
#import "UserProfileViewController.h"
#import <SDWebImage/UIImageView+WebCache.h>

@interface MyPostViewController ()
@property (nonatomic, strong) NSDictionary *dictionaryUser;
@property (nonatomic, strong) NSMutableArray *arrayPosts;
@property (nonatomic, strong) LoadingView *loading;
@property(nonatomic,assign) BOOL isJoin;
@property int joinStatus;


@end

@implementation MyPostViewController

{
    NSMutableArray* postArray;
    UIRefreshControl*refreshControl;
    NSDictionary*dictionaryPostSelected;
    BOOL isLike;
}

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    
    refreshControl = [[UIRefreshControl alloc]init];
    refreshControl.tintColor = [UIColor colorWithRed:122/255.0f green:222/255.0f blue:225/255.0f alpha:1.0f];
    [self.myPostTableViewOutlet addSubview:refreshControl];
    [refreshControl addTarget:self action:@selector(refreshTable) forControlEvents:UIControlEventValueChanged];
    
    self.dictionaryUser = [[NSDictionary alloc] init];
    self.arrayPosts = [[NSMutableArray alloc] init];
    
    postArray = [[NSMutableArray alloc] initWithObjects:@"1",@"2",@"3",@"4",@"5", nil];
    
     postArray = [[NSMutableArray alloc] initWithObjects:@"1",@"2",@"3", nil];
    
    self.myPostTableViewOutlet.delegate = self;
    self.myPostTableViewOutlet.dataSource = self;
    
//    self.loading = [[LoadingView alloc] initWithFrame:[UIScreen mainScreen].bounds];
//    [self.view addSubview:self.loading];
//    [self.loading animate:YES];
//    
//    [[Client sharedClient] getMyPost:^(BOOL success, NSError *error) {
//        
//        if (success) {
//            [self.loading animate:NO];
//            NSLog(@"%@",[SharedVariables sharedInstance].myPostResponseDict);
//            
//            self.dictionaryUser = [[SharedVariables sharedInstance].myPostResponseDict objectForKey:@"data"];
//            
//            self.arrayPosts=[[self.dictionaryUser objectForKey:@"wall_messages"] mutableCopy];
//            NSLog(@"WallMessages:%@",self.arrayPosts);
//            [self populateContent];
//        }
//        else
//        {
//            [self.loading animate:NO];
//        }
//        
//    }];
}

- (void)refreshTable {
    //TODO: refresh your data
    [refreshControl endRefreshing];
    self.loading = [[LoadingView alloc] initWithFrame:[UIScreen mainScreen].bounds];
    [self.view addSubview:self.loading];
    [self.loading animate:YES];
    
    [[Client sharedClient] getMyPost:^(BOOL success, NSError *error) {
        
        if (success) {
            [self.loading animate:NO];
            NSLog(@"%@",[SharedVariables sharedInstance].myPostResponseDict);
            
            self.dictionaryUser = [[SharedVariables sharedInstance].myPostResponseDict objectForKey:@"data"];
            
            self.arrayPosts=[[self.dictionaryUser objectForKey:@"wall_messages"] mutableCopy];
            NSLog(@"WallMessages:%@",self.arrayPosts);
            [self populateContent];
        }
        else
        {
            [self.loading animate:NO];
        }
        
    }];

}
-(void)viewWillAppear:(BOOL)animated
{

    
}

-(void)viewDidAppear:(BOOL)animated
{
    [self refreshTable];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

-(void)populateContent{

        self.arrayPosts=[[self.dictionaryUser objectForKey:@"wall_messages"] mutableCopy];
    
    NSArray * array = [[self.arrayPosts reverseObjectEnumerator] allObjects];
    self.arrayPosts= [array mutableCopy];
    
    if (self.arrayPosts.count > 0) {
        
        self.myPostTableViewOutlet.hidden = NO;
        self.emptyPostLblOutlet.hidden = YES;
        [self.myPostTableViewOutlet reloadData];
        
    }
    else
    {
       self.myPostTableViewOutlet.hidden = YES;
        self.emptyPostLblOutlet.hidden = NO;
    }
    
    
}




/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

- (IBAction)MyPostSegmentControlAction:(id)sender {
    
    if(self.myPostSegmentControlOutlet.selectedSegmentIndex==0){
        
        [self.navigationController popViewControllerAnimated:NO];
        
//        ProfileViewController * profileVC = [self.storyboard instantiateViewControllerWithIdentifier:@"ProfileViewController"];
//        [self.navigationController pushViewController:profileVC animated:NO];
        
    }
    else if(self.myPostSegmentControlOutlet.selectedSegmentIndex==1){
        
        
        
    }
}

- (IBAction)newBtnAction:(id)sender {
    
            CreatPostViewController * creatPostVC = [self.storyboard instantiateViewControllerWithIdentifier:@"CreatPostViewController"];
            [self.navigationController pushViewController:creatPostVC animated:YES];
    
}

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 1;    //count of section
}


- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    
    return self.arrayPosts.count;
    //count number of row from counting array hear cataGorry is An Array
}

- (UITableViewCell *)tableView:(UITableView *)tableView
         cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    
    MyPostTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"Cell"];
    
    cell.eventPostProfileImageView.image = [UIImage imageNamed:@"avatar2.png"];
    cell.eventPostImageViewOutlet.image = [UIImage imageNamed:@"avatar2.png"];
    cell.textPostImageViewOutlet.image = [UIImage imageNamed:@"avatar2.png"];
    cell.textPostProfileImageView.image = [UIImage imageNamed:@"avatar2.png"];
    
    
    if (nil == cell) {
        cell = [[MyPostTableViewCell alloc] initWithStyle:UITableViewCellStyleSubtitle reuseIdentifier:@"Cell"];
    }
    NSDictionary* wallMessagesDict = [self.arrayPosts objectAtIndex:indexPath.row];
    
    
    NSString * isEvent = [NSString stringWithFormat:@"%@",[wallMessagesDict objectForKey:@"is_event"]];
    
    if ([isEvent isEqualToString:@"0"]) {
        
        cell.textPostBaseView.hidden = NO;
        cell.eventPostBaseView.hidden = YES;
        
        
        cell.textPostTitleNamelblOutlet.text = [NSString stringWithFormat:@"%@",[wallMessagesDict objectForKey:@"title"]];
        
        cell.textPostWillBeThereLblOutlet.text = [NSString stringWithFormat:@"%@",[wallMessagesDict objectForKey:@"joinCount"]];
        cell.textPostLikedThisLblOutlet.text = [NSString stringWithFormat:@"%@",[wallMessagesDict objectForKey:@"likesCount"]];
        cell.textPostCommentsLblOutlet.text = [NSString stringWithFormat:@"%@",[wallMessagesDict objectForKey:@"commentCount"]];
        
        cell.textPostTextViewOutlet.text = [NSString stringWithFormat:@"%@",[wallMessagesDict objectForKey:@"message"]];
        
        NSString* lat = [wallMessagesDict objectForKey:@"lat"];
        NSString* lng = [wallMessagesDict objectForKey:@"lon"];
        
        CLLocationCoordinate2D  ctrpoint;
        ctrpoint.latitude = [lat doubleValue];
        ctrpoint.longitude = [lng doubleValue];
        
        NSString* currentLat = [NSString stringWithFormat:@"%f",[SharedVariables sharedInstance].myClLocation.coordinate.latitude];
        NSString* currentLng = [NSString stringWithFormat:@"%f",[SharedVariables sharedInstance].myClLocation.coordinate.longitude];
        
        CLLocation *loc1 = [[CLLocation alloc]initWithLatitude:[currentLat doubleValue] longitude:[currentLng doubleValue]];
        
        CLLocation *loc2 = [[CLLocation alloc]initWithLatitude:ctrpoint.latitude longitude:ctrpoint.longitude];
        
        CLLocationDistance distance = [loc1 distanceFromLocation:loc2];
        
        NSArray * arr = [[NSString stringWithFormat:@"%f",distance] componentsSeparatedByString:@"."];
        NSString* dist = [NSString stringWithFormat:@"%@",arr.firstObject];
        
        cell.textPostDistanceLblOutlet.text = [NSString stringWithFormat:@"%@ M",dist];
        
        
        NSArray*likesArry = [wallMessagesDict objectForKey:@"likes"];
        
        if (likesArry.count == 0) {
            
            cell.textPostLikeImageViewOutlet.image = [UIImage imageNamed:@"ic_liked.png"];
        }
        else
        {
            
            for (NSDictionary*likesDict in likesArry) {
                
                NSString*likeuser = [likesDict objectForKey:@"username"];
                
                if ([likeuser isEqualToString:[Client sharedClient].user.email]) {
                    
                    cell.textPostLikeImageViewOutlet.image = [UIImage imageNamed:@"ic_like.png"];
                    break;
                }
                else
                {
                    cell.textPostLikeImageViewOutlet.image = [UIImage imageNamed:@"ic_liked.png"];
                }
            }
        }
        
        
        NSString * timeStampString = [NSString stringWithFormat:@"%@",[wallMessagesDict objectForKey:@"created"]];
        
        if (![timeStampString isEqualToString:@"<null>"]) {
            
            NSTimeInterval _interval=[timeStampString doubleValue];
            NSDate *date = [NSDate dateWithTimeIntervalSince1970:_interval];
            NSLog(@"%@", date);
            
            
            NSDateFormatter *formatter = [[NSDateFormatter alloc] init];
            [formatter setDateFormat:@"dd MMMM yyyy"];
            NSString *result = [formatter stringFromDate:date];
            
            NSMutableAttributedString*tmeString = [[Utilities sharedInstance]loadTimeDifferent:date];
            
            cell.textPostMomentLblOutlet.attributedText = tmeString;
        }
        else
        {
            cell.textPostMomentLblOutlet.text = [NSString stringWithFormat:@"Not Available"];
        }
        
        
        //cell.textPostMomentLblOutlet.text = [NSString stringWithFormat:@"%@",[wallMessagesDict objectForKey:@"created"]];
        
        
        NSDictionary*userDict = [wallMessagesDict objectForKey:@"user"];
        
        cell.textPostSubTitleLblOutlet.text = [NSString stringWithFormat:@"by %@",[userDict objectForKey:@"extended_name"]];
        
        
        dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_BACKGROUND, 0), ^{
            // retrive image on global queue
            
            NSString* profilePicPath = [NSString stringWithFormat:@"%@",[userDict objectForKey:@"pic_path"]];
            
            NSString * urlstring = [NSString stringWithFormat:@"http://34.230.105.103:8002/%@", profilePicPath];
            NSURL *imageURL = [NSURL URLWithString:urlstring];
            UIImage * img = [UIImage imageWithData:[NSData dataWithContentsOfURL:[NSURL URLWithString:urlstring]]];
            
            dispatch_async(dispatch_get_main_queue(), ^{
                
                cell.textPostProfileImageView.layer.cornerRadius = cell.textPostProfileImageView.frame.size.height / 2;
                cell.textPostProfileImageView.layer.masksToBounds = YES;
                
                // assign cell image on main thread
                
                [cell.textPostProfileImageView sd_setImageWithURL:imageURL
                                                 placeholderImage:[UIImage imageNamed:@"avatar2.png"]];
                //[cell.textPostProfileImageView sd_setImageWithURL:imageURL placeholderImage:[UIImage imageNamed:@"avatar2.png"]];
                //[cell.textPostProfileImageView setImageWithURL:imageURL placeholderImage:[UIImage imageNamed:@"avatar2.png"]];
            });
        });
        
        NSArray * picArray = [wallMessagesDict objectForKey:@"pictures"];
        NSArray * videoArray = [wallMessagesDict objectForKey:@"videos"];
        
        
        if (picArray.count > 0 || videoArray.count>0) {
            
            if(videoArray.count > 0) {
                
                dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_BACKGROUND, 0), ^{
                    // retrive image on global queue
                    
                    NSDictionary * videoDict = picArray.firstObject;
                    
                    NSString*sourceURLString =[NSString stringWithFormat:@"http://34.230.105.103:8002/%@",[videoDict objectForKey:@"pic_path"]];
                    
                    NSURL *imageURL = [NSURL URLWithString:sourceURLString];
                    
                    
                    
                    dispatch_async(dispatch_get_main_queue(), ^{
                        
                        [cell.textPostImageViewOutlet sd_setImageWithURL:imageURL
                                                        placeholderImage:[UIImage imageNamed:@"avatar2.png"]];
                        //[cell.textPostImageViewOutlet sd_setImageWithURL:imageURL placeholderImage:[UIImage imageNamed:@"placeHolderImage.png"]];
                        //cell.textPostImageViewOutlet.image = orginalImage;
                    });
                });
                
                
                cell.textPostPlayImageView.hidden = NO;
                
                cell.testPostImagesCountLblOutlet.hidden = YES;
                
                
                // });
            }
            else if (picArray.count > 0) {
                
                NSDictionary * imageDict = picArray.firstObject;
                
                dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), ^{
                    // retrive image on global queue
                    
                    NSString* profilePicPath = [NSString stringWithFormat:@"%@",[imageDict objectForKey:@"pic_path"]];
                    
                    NSString * urlstring = [NSString stringWithFormat:@"http://34.230.105.103:8002/%@", profilePicPath];
                    NSURL *imageURL = [NSURL URLWithString:urlstring];
                    UIImage * img = [UIImage imageWithData:[NSData dataWithContentsOfURL:[NSURL URLWithString:urlstring]]];
                    
                    dispatch_async(dispatch_get_main_queue(), ^{
                        
                        cell.textPostImageViewOutlet.layer.cornerRadius = 5;
                        cell.textPostImageViewOutlet.layer.masksToBounds = YES;
                        [cell.textPostImageViewOutlet sd_setImageWithURL:imageURL
                                                        placeholderImage:[UIImage imageNamed:@"avatar2.png"]];
                        //[cell.textPostImageViewOutlet sd_setImageWithURL:imageURL placeholderImage:[UIImage imageNamed:@"placeHolderImage.png"]];
                        // [cell.textPostImageViewOutlet setImageWithURL:imageURL placeholderImage:[UIImage imageNamed:@"placeHolderImage.png"]];
                    });
                });
                
                cell.textPostPlayImageView.hidden = YES;
                
                if (picArray.count > 1) {
                    
                    cell.testPostImagesCountLblOutlet.hidden = NO;
                    
                    cell.testPostImagesCountLblOutlet.text = [NSString stringWithFormat:@"+%lu",(unsigned long)picArray.count -1];
                }
                else
                {
                    cell.testPostImagesCountLblOutlet.hidden = YES;
                }
                
            }
            
            cell.textPostImageViewHeightConstraint.constant = 159;
            cell.textPostBaseViewHeightConstraint.constant = 443;
            cell.textPostImageViewOutlet.hidden = NO;
            
            
            cell.textPostImageViewOutlet.userInteractionEnabled = YES;
            cell.textPostImageViewOutlet.tag = indexPath.row;
            
            UITapGestureRecognizer *tapped = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(textPostImageTapped:)];
            tapped.numberOfTapsRequired = 1;
            [cell.textPostImageViewOutlet addGestureRecognizer:tapped];
            
            
        }
        else
        {
            cell.textPostPlayImageView.hidden = YES;
            cell.textPostImageViewOutlet.hidden = YES;
            cell.textPostImageViewHeightConstraint.constant = 0;
            cell.textPostBaseViewHeightConstraint.constant = 443-159;
        }
        
    }
    else if ([isEvent isEqualToString:@"1"]) {
        
        cell.textPostBaseView.hidden = YES;
        cell.eventPostBaseView.hidden = NO;
        
        
        NSArray*joinArray = [wallMessagesDict objectForKey:@"joins"];
        
        NSPredicate *predicate = [NSPredicate predicateWithFormat:@"(username==%@)",[Client sharedClient].user.email];
        NSArray *arrayUserResult = [joinArray filteredArrayUsingPredicate:predicate];
        if(arrayUserResult.count>0){
            int status = [[[arrayUserResult lastObject] objectForKey:@"status"] intValue];
            //            self.joinStatus = status;
            //            self.isJoin=YES;
            if(status==0){
                
                
            }else if(status==1){
                
                cell.eventPostJoiningBaseView.backgroundColor = [UIColor colorWithRed:122/255.0f green:222/255.0f blue:225/255.0f alpha:1.0f];
                cell.eventPostJoiningBtnOutlet.backgroundColor = [UIColor clearColor];
                [cell.eventPostJoiningBtnOutlet setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
                
                cell.eventPostMaybeBaseView.backgroundColor = [UIColor whiteColor];
                cell.eventPostMaybeBtnOutlet.backgroundColor = [UIColor clearColor];
                [cell.eventPostMaybeBtnOutlet setTitleColor:[UIColor colorWithRed:122/255.0f green:222/255.0f blue:225/255.0f alpha:1.0f] forState:UIControlStateNormal];
                
            }
            else if(status==2){
                
                cell.eventPostMaybeBaseView.backgroundColor = [UIColor colorWithRed:122/255.0f green:222/255.0f blue:225/255.0f alpha:1.0f];
                cell.eventPostMaybeBtnOutlet.backgroundColor = [UIColor clearColor];
                [cell.eventPostMaybeBtnOutlet setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
                
                cell.eventPostJoiningBaseView.backgroundColor = [UIColor whiteColor];
                cell.eventPostJoiningBtnOutlet.backgroundColor = [UIColor clearColor];
                [cell.eventPostJoiningBtnOutlet setTitleColor:[UIColor colorWithRed:122/255.0f green:222/255.0f blue:225/255.0f alpha:1.0f] forState:UIControlStateNormal];
            }
        }
        else{
            //self.isJoin=NO;
            
            cell.eventPostJoiningBaseView.backgroundColor = [UIColor whiteColor];
            cell.eventPostJoiningBtnOutlet.backgroundColor = [UIColor clearColor];
            [cell.eventPostJoiningBtnOutlet setTitleColor:[UIColor colorWithRed:122/255.0f green:222/255.0f blue:225/255.0f alpha:1.0f] forState:UIControlStateNormal];
            
            cell.eventPostMaybeBaseView.backgroundColor = [UIColor whiteColor];
            cell.eventPostMaybeBtnOutlet.backgroundColor = [UIColor clearColor];
            [cell.eventPostMaybeBtnOutlet setTitleColor:[UIColor colorWithRed:122/255.0f green:222/255.0f blue:225/255.0f alpha:1.0f] forState:UIControlStateNormal];
            
        }
        //else{
        //            self.isJoin=NO;
        //
        //        }
        
        cell.eventPostTitleNameLblOutlet.text = [NSString stringWithFormat:@"%@",[wallMessagesDict objectForKey:@"title"]];
        
        cell.eventPostWillBeThereLblOutlet.text = [NSString stringWithFormat:@"%@",[wallMessagesDict objectForKey:@"joinCount"]];
        cell.eventPostLikedThisLblOutlet.text = [NSString stringWithFormat:@"%@",[wallMessagesDict objectForKey:@"likesCount"]];
        cell.eventPostCommentsLblOutlet.text = [NSString stringWithFormat:@"%@",[wallMessagesDict objectForKey:@"commentCount"]];
        
        cell.eventPostTextViewOutlet.text = [NSString stringWithFormat:@"%@",[wallMessagesDict objectForKey:@"message"]];
        
        
        NSString* lat = [wallMessagesDict objectForKey:@"lat"];
        NSString* lng = [wallMessagesDict objectForKey:@"lon"];
        
        CLLocationCoordinate2D  ctrpoint;
        ctrpoint.latitude = [lat doubleValue];
        ctrpoint.longitude = [lng doubleValue];
        
        NSString* currentLat = [NSString stringWithFormat:@"%f",[SharedVariables sharedInstance].myClLocation.coordinate.latitude];
        NSString* currentLng = [NSString stringWithFormat:@"%f",[SharedVariables sharedInstance].myClLocation.coordinate.longitude];
        
        CLLocation *loc1 = [[CLLocation alloc]initWithLatitude:[currentLat doubleValue] longitude:[currentLng doubleValue]];
        
        CLLocation *loc2 = [[CLLocation alloc]initWithLatitude:ctrpoint.latitude longitude:ctrpoint.longitude];
        
        CLLocationDistance distance = [loc1 distanceFromLocation:loc2];
        
        NSArray * arr = [[NSString stringWithFormat:@"%f",distance] componentsSeparatedByString:@"."];
        NSString* dist = [NSString stringWithFormat:@"%@",arr.firstObject];
        
        
        cell.eventPostDistanceLblOutlet.text = [NSString stringWithFormat:@"%@ M",dist];
        
        NSArray*likesArry = [wallMessagesDict objectForKey:@"likes"];
        
        if (likesArry.count == 0) {
            
            cell.eventPostLikeImageViewOutlet.image = [UIImage imageNamed:@"ic_liked.png"];
        }
        else
        {
            
            for (NSDictionary*likesDict in likesArry) {
                
                NSString*likeuser = [likesDict objectForKey:@"username"];
                
                if ([likeuser isEqualToString:[Client sharedClient].user.email]) {
                    
                    cell.eventPostLikeImageViewOutlet.image = [UIImage imageNamed:@"ic_like.png"];
                    break;
                }
                else
                {
                    cell.eventPostLikeImageViewOutlet.image = [UIImage imageNamed:@"ic_liked.png"];
                }
            }
        }
        
        NSString * timeStampString = [NSString stringWithFormat:@"%@",[wallMessagesDict objectForKey:@"created"]];
        
        if (![timeStampString isEqualToString:@"<null>"]) {
            
            NSTimeInterval _interval=[timeStampString doubleValue];
            NSDate *date = [NSDate dateWithTimeIntervalSince1970:_interval];
            NSLog(@"%@", date);
            
            
            NSDateFormatter *formatter = [[NSDateFormatter alloc] init];
            [formatter setDateFormat:@"dd MMMM yyyy"];
            NSString *result = [formatter stringFromDate:date];
            
            NSMutableAttributedString*tmeString = [[Utilities sharedInstance]loadTimeDifferent:date];
            
            cell.eventPostMomentsLblOutlet.attributedText = tmeString;
        }
        else
        {
            cell.eventPostMomentsLblOutlet.text = [NSString stringWithFormat:@"Not Available"];
        }
        
        // cell.eventPostMomentsLblOutlet.text = [NSString stringWithFormat:@"%@",[wallMessagesDict objectForKey:@"created"]];
        if ([[NSString stringWithFormat:@"%@",[wallMessagesDict objectForKey:@"event_location_info"]] isEqualToString:@"<null>"]) {
            
            cell.eventPostQuickNotLblOutlet.text = @"";
        }
        else
        {
            
            cell.eventPostQuickNotLblOutlet.text = [NSString stringWithFormat:@"%@",[wallMessagesDict objectForKey:@"event_location_info"]];
        }
        
        NSString * timeStampString1 = [NSString stringWithFormat:@"%@",[wallMessagesDict objectForKey:@"event_date"]];
        
        //NSString * timeStampString1 = @"1513232169";
        
        if (![timeStampString1 isEqualToString:@"<null>"]) {
            
            NSTimeInterval _interval=[timeStampString1 doubleValue];
            NSDate *date = [NSDate dateWithTimeIntervalSince1970:_interval];
            NSLog(@"%@", date);
            
            
            NSDateFormatter *formatter = [[NSDateFormatter alloc] init];
            [formatter setDateFormat:@"dd MMMM yyyy"];
            NSString *result = [formatter stringFromDate:date];
            
            // NSMutableAttributedString*tmeString = [[Utilities sharedInstance]loadTimeDifferent:date];
            
            // cell.eventPostDateLblOutlet.attributedText = tmeString;
            
            NSString * str = [NSString stringWithFormat:@"Date: %@",result];
            // NSString *truncatedString = [str substringToIndex:[str length]-3];
            
            cell.eventPostDateLblOutlet.text = str;
        }
        else
        {
            cell.eventPostDateLblOutlet.text = [NSString stringWithFormat:@"Not Available"];
        }
        
        // cell.eventPostDateLblOutlet.text = [NSString stringWithFormat:@"%@",[wallMessagesDict objectForKey:@"event_date"]];
        
        NSString* price = [NSString stringWithFormat:@"%@",[wallMessagesDict objectForKey:@"event_price"]];
        
        if ([price isEqualToString:@"0"]) {
            
            [cell.eventPostPriceBtnOutlet setTitle:@"Free" forState:UIControlStateNormal];
        }
        else
        {
            
            [cell.eventPostPriceBtnOutlet setTitle:[NSString stringWithFormat:@"$ %@",[wallMessagesDict objectForKey:@"event_price"]] forState:UIControlStateNormal];
        }
        
        
        NSDictionary*userDict = [wallMessagesDict objectForKey:@"user"];
        
        cell.eventPostSubtitleLblOutlet.text = [NSString stringWithFormat:@"by %@",[userDict objectForKey:@"extended_name"]];
        
        
        dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_BACKGROUND, 0), ^{
            // retrive image on global queue
            
            NSString* profilePicPath = [NSString stringWithFormat:@"%@",[userDict objectForKey:@"pic_path"]];
            
            NSString * urlstring = [NSString stringWithFormat:@"http://34.230.105.103:8002/%@", profilePicPath];
            NSURL *imageURL = [NSURL URLWithString:urlstring];
            UIImage * img = [UIImage imageWithData:[NSData dataWithContentsOfURL:[NSURL URLWithString:urlstring]]];
            
            dispatch_async(dispatch_get_main_queue(), ^{
                
                cell.eventPostProfileImageView.layer.cornerRadius = cell.textPostProfileImageView.frame.size.height / 2;
                cell.eventPostProfileImageView.layer.masksToBounds = YES;
                [cell.eventPostProfileImageView sd_setImageWithURL:imageURL
                                                  placeholderImage:[UIImage imageNamed:@"avatar2.png"]];
                //[cell.eventPostProfileImageView sd_setImageWithURL:imageURL placeholderImage:[UIImage imageNamed:@"avatar2.png"]];
                //[cell.eventPostProfileImageView setImageWithURL:imageURL placeholderImage:[UIImage imageNamed:@"avatar2.png"]];
            });
        });
        NSArray * picArray = [wallMessagesDict objectForKey:@"pictures"];
        NSArray * videoArray = [wallMessagesDict objectForKey:@"videos"];
        
        
        if (picArray.count > 0 || videoArray.count>0) {
            
            if(videoArray.count > 0) {
                
                dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_BACKGROUND, 0), ^{
                    // retrive image on global queue
                    
                    NSDictionary * videoDict = picArray.firstObject;
                    
                    NSString*sourceURLString =[NSString stringWithFormat:@"http://34.230.105.103:8002/%@",[videoDict objectForKey:@"pic_path"]];
                    NSURL *imageURL = [NSURL URLWithString:sourceURLString];
                    
                    dispatch_async(dispatch_get_main_queue(), ^{
                        
                        [cell.eventPostImageViewOutlet sd_setImageWithURL:imageURL
                                                         placeholderImage:[UIImage imageNamed:@"avatar2.png"]];
                        //[cell.eventPostImageViewOutlet sd_setImageWithURL:imageURL placeholderImage:[UIImage imageNamed:@"placeHolderImage.png"]];
                    });
                });
                
                cell.eventPostPlayImageView.hidden = NO;
                
                cell.eventPostImagesCountLblOutlet.hidden = YES;
                
                // });
            }
            else if (picArray.count > 0) {
                
                NSDictionary * imageDict = picArray.firstObject;
                
                dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_BACKGROUND, 0), ^{
                    // retrive image on global queue
                    
                    NSString* profilePicPath = [NSString stringWithFormat:@"%@",[imageDict objectForKey:@"pic_path"]];
                    
                    NSString * urlstring = [NSString stringWithFormat:@"http://34.230.105.103:8002/%@", profilePicPath];
                    NSURL *imageURL = [NSURL URLWithString:urlstring];
                    UIImage * img = [UIImage imageWithData:[NSData dataWithContentsOfURL:[NSURL URLWithString:urlstring]]];
                    
                    dispatch_async(dispatch_get_main_queue(), ^{
                        
                        cell.eventPostImageViewOutlet.layer.cornerRadius = 5;
                        cell.eventPostImageViewOutlet.layer.masksToBounds = YES;
                        [cell.eventPostImageViewOutlet sd_setImageWithURL:imageURL
                                                         placeholderImage:[UIImage imageNamed:@"placeHolderImage.png"]];
                        //[cell.eventPostImageViewOutlet sd_setImageWithURL:imageURL placeholderImage:[UIImage imageNamed:@"placeHolderImage.png"]];
                        //[cell.eventPostImageViewOutlet setImageWithURL:imageURL placeholderImage:[UIImage imageNamed:@"placeHolderImage.png"]];
                    });
                });
                
                
                cell.eventPostPlayImageView.hidden = YES;
                
                if (picArray.count > 1) {
                    
                    cell.eventPostImagesCountLblOutlet.hidden = NO;
                    
                    cell.eventPostImagesCountLblOutlet.text = [NSString stringWithFormat:@"+%lu",(unsigned long)picArray.count -1];
                }
                else
                {
                    cell.eventPostImagesCountLblOutlet.hidden = YES;
                }
                
            }
            
            cell.eventPostImageViewHeightConstraint.constant = 159;
            cell.eventPostBaseViewHeightConstraint.constant = 500;
            cell.eventPostImageViewOutlet.hidden = NO;
            
            
            cell.eventPostImageViewOutlet.userInteractionEnabled = YES;
            cell.eventPostImageViewOutlet.tag = indexPath.row;
            
            UITapGestureRecognizer *tapped = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(eventPostImageTapped:)];
            tapped.numberOfTapsRequired = 1;
            [cell.eventPostImageViewOutlet addGestureRecognizer:tapped];
            
            
        }
        else
        {
            cell.eventPostPlayImageView.hidden = YES;
            cell.eventPostImageViewOutlet.hidden = YES;
            cell.eventPostImageViewHeightConstraint.constant = 0;
            cell.eventPostBaseViewHeightConstraint.constant = 500-159;
        }
    }
    
   // MyPostTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"Cell"]
    return cell;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    
}

-(CGFloat)tableView:(UITableView *)tableView estimatedHeightForRowAtIndexPath:(NSIndexPath *)indexPath{
    
    return UITableViewAutomaticDimension;
}

-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    
    
    MyPostTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"Cell"];
    
    int height;
    
    NSDictionary* wallMessagesDict = [self.arrayPosts objectAtIndex:indexPath.row];
    
    NSString * isEvent = [NSString stringWithFormat:@"%@",[wallMessagesDict objectForKey:@"is_event"]];
    
    if ([isEvent isEqualToString:@"0"]) {
        
        
        NSArray * picArray = [wallMessagesDict objectForKey:@"pictures"];
        NSArray * videoArray = [wallMessagesDict objectForKey:@"videos"];
        
        
        if (picArray.count > 0 || videoArray.count>0) {
            
//            cell.textPostImageViewHeightConstraint.constant = 159;
//            cell.textPostBaseViewHeightConstraint.constant = 443;
            
            height = 443;
            
        }
        else
        {
//            cell.textPostImageViewOutlet.hidden = YES;
//            cell.textPostImageViewHeightConstraint.constant = 0;
//            cell.textPostBaseViewHeightConstraint.constant = 443-159;
            
            height = 443-159;
        }
    }
    else {
        
        NSArray * picArray = [wallMessagesDict objectForKey:@"pictures"];
        NSArray * videoArray = [wallMessagesDict objectForKey:@"videos"];
        
        
        if (picArray.count > 0 || videoArray.count>0) {
            
//            cell.eventPostImageViewHeightConstraint.constant = 159;
//            cell.eventPostBaseViewHeightConstraint.constant = 500;
//            cell.eventPostImageViewOutlet.hidden = NO;
//            cell.eventPostImageViewOutlet.image = [UIImage imageNamed:@"sunder pichai.jpg"];
            
            height = 500;
            
        }
        else
        {
//            cell.eventPostImageViewOutlet.hidden = YES;
//            cell.eventPostImageViewHeightConstraint.constant = 0;
//            cell.eventPostBaseViewHeightConstraint.constant = 500-159;
            
            height = 500-159;
        }
    }
    return height + 10;
}

-(UIImage *)generateThumbImage : (NSString *)filepath
{
    NSURL *url = [NSURL fileURLWithPath:filepath];
    UIImage* thumbnail;
    //AVAssetImageGenerator *imageGenerator;
    AVAsset *asset = [AVAsset assetWithURL:url];
    AVAssetImageGenerator *imageGenerator = [[AVAssetImageGenerator alloc]initWithAsset:asset];
    imageGenerator.appliesPreferredTrackTransform = YES;
    CMTime time = [asset duration];
    time.value = 0;
    float duration = CMTimeGetSeconds([asset duration]);
    for(float i = 0.0; i<duration; i=i+0.1)
    {
        CGImageRef imgRef = [imageGenerator copyCGImageAtTime:CMTimeMake(i, duration) actualTime:NULL error:nil];
        thumbnail = [[UIImage alloc] initWithCGImage:imgRef scale:UIViewContentModeScaleAspectFit orientation:UIImageOrientationUp];
        //[thumbnailImages addObject:thumbnail];
    }
    return thumbnail;
}

- (IBAction)backbtnAction:(id)sender {
    
    [self.navigationController popViewControllerAnimated:NO];
}

- (IBAction)delectPostAction:(id)sender {
    
    UIView* baseView = [sender superview];
    UIView* cellContectView = [baseView superview];
    UIView* cell = [cellContectView superview];
    
    NSIndexPath *indexPath = [self.myPostTableViewOutlet indexPathForCell:cell];
    
    dictionaryPostSelected = self.arrayPosts[indexPath.row];
    
    NSString * isEvent = [NSString stringWithFormat:@"%@",[dictionaryPostSelected objectForKey:@"is_event"]];
    
    if ([isEvent isEqualToString:@"0"]) {
    
    [[Utilities sharedInstance] showCustomAlertViewWithMultiple:self title:@"Delete Post" message:@"Are you sure would like to delete this post?" actionResponse:@"deletePost"];
        
    }else if ([isEvent isEqualToString:@"1"]) {
        
        [[Utilities sharedInstance] showCustomAlertViewWithMultiple:self title:@"Delete Event" message:@"Are you sure would like to delete this event?" actionResponse:@"deletePost"];
        
    }
    
}

-(void)postDelete
{
    self.loading = [[LoadingView alloc] initWithFrame:[UIScreen mainScreen].bounds];
    [self.view addSubview:self.loading];
    [self.loading animate:YES];
    
    NSLog(@"dict:%@",dictionaryPostSelected);
    [self deletePostEffet];
}
- (IBAction)locationBtnAction:(id)sender {
    
    UIView* baseView = [sender superview];
    UIView* baseView1 = [baseView superview];
    UIView* baseView2 = [baseView1 superview];
    UIView* cellContectView = [baseView2 superview];
    UIView* cell = [cellContectView superview];
    
    NSIndexPath *indexPath = [self.myPostTableViewOutlet indexPathForCell:cell];
    
    dictionaryPostSelected = self.arrayPosts[indexPath.row];
    
    NSLog(@"dict:%@",dictionaryPostSelected);
    
    NSString* lat = [dictionaryPostSelected objectForKey:@"lat"];
    NSString* lng = [dictionaryPostSelected objectForKey:@"lon"];
    
    
    MapViewController * mapVC = [self.storyboard instantiateViewControllerWithIdentifier:@"MapViewController"];
    mapVC.viewIdendity = @"myPostLocation";
    mapVC.latti = [lat doubleValue];
    mapVC.longi = [lng doubleValue];
    [self.navigationController pushViewController:mapVC animated:YES];
    
}

- (IBAction)PostLikedThisBtnaction:(id)sender {
    
    UIView* baseView = [sender superview];
    UIView* baseView1 = [baseView superview];
    UIView* baseView2 = [baseView1 superview];
    UIView* cell = [baseView2 superview];

    
    NSIndexPath *indexPath = [self.myPostTableViewOutlet indexPathForCell:cell];
    
    dictionaryPostSelected = self.arrayPosts[indexPath.row];
    
    NSLog(@"dict:%@",dictionaryPostSelected);
    
    
    NSString*likesCount = [NSString stringWithFormat:@"%@",[dictionaryPostSelected objectForKey:@"likesCount"]];
    
    if (![likesCount isEqualToString:@"0"]) {
        
        LikesViewController * likesVC = [self.storyboard instantiateViewControllerWithIdentifier:@"LikesViewController"];
        
        likesVC.wallIdString = [dictionaryPostSelected objectForKey:@"id"];
        [self.navigationController pushViewController:likesVC animated:YES];
    }
}

-(IBAction)PostCommentsBtnAction:(id)sender
{
 
    UIView* baseView = [sender superview];
    UIView* baseView1 = [baseView superview];
    UIView* baseView2 = [baseView1 superview];
    UIView* cell = [baseView2 superview];
    
    NSIndexPath *indexPath = [self.myPostTableViewOutlet indexPathForCell:cell];
    
    dictionaryPostSelected = self.arrayPosts[indexPath.row];
    
    NSLog(@"dict:%@",dictionaryPostSelected);
    
     NSString* commentsCount = [NSString stringWithFormat:@"%@",[dictionaryPostSelected objectForKey:@"commentCount"]];
    
    if (![commentsCount isEqualToString:@"0"]) {
        
        CommentsViewController * commentsVC = [self.storyboard instantiateViewControllerWithIdentifier:@"CommentsViewController"];
        commentsVC.wallIdString = [dictionaryPostSelected objectForKey:@"id"];
        [self.navigationController pushViewController:commentsVC animated:YES];
    }
}

- (IBAction)likeBtnAction:(id)sender {
    
    UIView* baseView = [sender superview];
    UIView* baseView1 = [baseView superview];
    UIView* baseView2 = [baseView1 superview];
    UIView* cellContectView = [baseView2 superview];
    UIView* cell = [cellContectView superview];
    
    NSIndexPath *indexPath = [self.myPostTableViewOutlet indexPathForCell:cell];
    
    dictionaryPostSelected = self.arrayPosts[indexPath.row];
    
    NSLog(@"dict:%@",dictionaryPostSelected);
    
    [self updateActionFooter:[dictionaryPostSelected objectForKey:@"likes"]];
    
}

-(void)updateActionFooter:(NSArray *)array{
    NSPredicate *predicate = [NSPredicate predicateWithFormat:@"(username==%@)",[Client sharedClient].user.email];
    NSArray *arrayUserResult = [array filteredArrayUsingPredicate:predicate];
    
    if(arrayUserResult.count>0){
        isLike=YES;
        [self updateLike:dictionaryPostSelected status:-1];
    }else{
        isLike=NO;
       [self updateLike:dictionaryPostSelected status:0];
    }
}

- (IBAction)commentsBtnAction:(id)sender {
    
    UIView* baseView = [sender superview];
    UIView* baseView1 = [baseView superview];
    UIView* baseView2 = [baseView1 superview];
    UIView* cellContectView = [baseView2 superview];
    UIView* cell = [cellContectView superview];
    
    NSIndexPath *indexPath = [self.myPostTableViewOutlet indexPathForCell:cell];
    
    dictionaryPostSelected = self.arrayPosts[indexPath.row];
    
    NSLog(@"dict:%@",dictionaryPostSelected);
    
    [[Utilities sharedInstance] showCustomAddCommentView:self];
}

- (IBAction)editPostBtnAction:(id)sender {
    
    UIView* baseView = [sender superview];
    UIView* cellContectView = [baseView superview];
    UIView* cell = [cellContectView superview];
    
    NSIndexPath *indexPath = [self.myPostTableViewOutlet indexPathForCell:cell];
    
    dictionaryPostSelected = self.arrayPosts[indexPath.row];
    
    
    CreatPostViewController * creatVC = [self.storyboard instantiateViewControllerWithIdentifier:@"CreatPostViewController"];
    creatVC.editDateDict = dictionaryPostSelected;
    creatVC.viewIdendity = @"editPost";
    [self.navigationController pushViewController:creatVC animated:YES];
    
}

- (IBAction)joiningBtnAction:(id)sender {
    
    UIView* baseView = [sender superview];
    UIView* baseView1 = [baseView superview];
    UIView* baseView2 = [baseView1 superview];
    UIView* cellContectView = [baseView2 superview];
    UIView* cell = [cellContectView superview];
    
    NSIndexPath *indexPath = [self.myPostTableViewOutlet indexPathForCell:cell];
    
    dictionaryPostSelected = self.arrayPosts[indexPath.row];
    
    NSArray*joinArray = [dictionaryPostSelected objectForKey:@"joins"];
    
    NSPredicate *predicate = [NSPredicate predicateWithFormat:@"(username==%@)",[Client sharedClient].user.email];
    NSArray *arrayUserResult = [joinArray filteredArrayUsingPredicate:predicate];
    if(arrayUserResult.count>0){
        int status = [[[arrayUserResult lastObject] objectForKey:@"status"] intValue];
        self.joinStatus = status;
        self.isJoin=YES;
        if(status==0){
            
            self.isJoin=NO;
            
        }else if(status==1){
            
            self.isJoin=YES;
        }
        else if(status==2){
            
            self.isJoin=NO;
        }
    }else{
        self.isJoin=NO;
        
    }
    
    int result = 0;
    if(self.isJoin)
    {
        
    }
    else
    {
       result = 1;
    }

    [self updatePartecipate:dictionaryPostSelected status:result];
    
}

- (IBAction)mayBeBtnAction:(id)sender {
    
    
    UIView* baseView = [sender superview];
    UIView* baseView1 = [baseView superview];
    UIView* baseView2 = [baseView1 superview];
    UIView* cellContectView = [baseView2 superview];
    UIView* cell = [cellContectView superview];
    
    NSIndexPath *indexPath = [self.myPostTableViewOutlet indexPathForCell:cell];
    
    dictionaryPostSelected = self.arrayPosts[indexPath.row];
    
    NSArray*joinArray = [dictionaryPostSelected objectForKey:@"joins"];
    
    NSPredicate *predicate = [NSPredicate predicateWithFormat:@"(username==%@)",[Client sharedClient].user.email];
    NSArray *arrayUserResult = [joinArray filteredArrayUsingPredicate:predicate];
    if(arrayUserResult.count>0){
        int status = [[[arrayUserResult lastObject] objectForKey:@"status"] intValue];
        self.joinStatus = status;
        self.isJoin=YES;
        if(status==0){
            
            self.isJoin=NO;
            
        }else if(status==1){
            
            self.isJoin=NO;
        }
        else if(status==2){
            
            self.isJoin=YES;
        }
    }else{
        self.isJoin=NO;
        
    }

    
    int result = 0;
    if(self.isJoin)
    {
        
    }
    else
    {
      result = 2;
    }
    
    [self updatePartecipate:dictionaryPostSelected status:result];
    
}

- (IBAction)willBeThereAction:(id)sender {
    
    
    UIView* baseView = [sender superview];
    UIView* baseView1 = [baseView superview];
    UIView* baseView2 = [baseView1 superview];
    UIView* cell = [baseView2 superview];
    
    NSIndexPath *indexPath = [self.myPostTableViewOutlet indexPathForCell:cell];
    
    dictionaryPostSelected = self.arrayPosts[indexPath.row];
    
    NSLog(@"dict:%@",dictionaryPostSelected);
    
    
    NSString * joinCount = [NSString stringWithFormat:@"%@",[dictionaryPostSelected objectForKey:@"joinCount"]];
    
    if (![joinCount isEqualToString:@"0"]) {
        
        JoinsViewController * joinsVC = [self.storyboard instantiateViewControllerWithIdentifier:@"JoinsViewController"];
        joinsVC.wallIdString = [dictionaryPostSelected objectForKey:@"id"];
        [self.navigationController pushViewController:joinsVC animated:YES];
    }

}

- (IBAction)shareBtnAction:(id)sender {
    
    UIView* baseView = [sender superview];
    UIView* baseView1 = [baseView superview];
    UIView* baseView2 = [baseView1 superview];
    UIView* cell = [baseView2 superview];
    
    NSIndexPath *indexPath = [self.myPostTableViewOutlet indexPathForCell:cell];
    
    dictionaryPostSelected = self.arrayPosts[indexPath.row];
    
    NSString* idString = [NSString stringWithFormat:@"%@",[dictionaryPostSelected objectForKey:@"id"]];
    
    NSString * shareStr = [NSString stringWithFormat:@"mabo.pushapp://mabo.pushapp.me/wallmessages/%@",idString];
    
    NSArray *activityItems = @[shareStr];
    
    UIActivityViewController *activityViewControntroller = [[UIActivityViewController alloc] initWithActivityItems:activityItems applicationActivities:nil];
    activityViewControntroller.excludedActivityTypes = @[];
    if (UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPad) {
        activityViewControntroller.popoverPresentationController.sourceView = self.view;
        activityViewControntroller.popoverPresentationController.sourceRect = CGRectMake(self.view.bounds.size.width/2, self.view.bounds.size.height/4, 0, 0);
    }
    [self presentViewController:activityViewControntroller animated:true completion:nil];
}

- (IBAction)myPostProfilePicAction:(id)sender {
    
    UIView* baseView = [sender superview];
    UIView* baseView1 = [baseView superview];
    UIView* baseView2 = [baseView1 superview];
    UIView* cell = [baseView2 superview];
    
    NSIndexPath *indexPath = [self.myPostTableViewOutlet indexPathForCell:cell];
    
    dictionaryPostSelected = self.arrayPosts[indexPath.row];
    
    NSLog(@"dict:%@",dictionaryPostSelected);
    
    UserProfileViewController * userProfileVC = [self.storyboard instantiateViewControllerWithIdentifier:@"UserProfileViewController"];
    userProfileVC.userDict = dictionaryPostSelected;
   // [self.navigationController pushViewController:userProfileVC animated:YES];
    [self.navigationController presentViewController:userProfileVC animated:YES completion:nil];
}

-(void)commentOk
{
    self.loading = [[LoadingView alloc] initWithFrame:[UIScreen mainScreen].bounds];
    [self.view addSubview:self.loading];
    [self.loading animate:YES];
    
    [[Client sharedClient] sendCommentWallMessageFromId:[[dictionaryPostSelected objectForKey:@"id"] stringValue] withComment:self.postComment completition:^(NSDictionary *dictionary, NSError *error) {
        
        [self.loading animate:NO];
        
        if(dictionary){
            if([[dictionary objectForKey:@"success"] boolValue])
            {
                [[Client sharedClient] getUserInfo:[Client sharedClient].user.email completition:^(NSDictionary *dictionary, NSError *error) {
                    //self.cellSelected=nil;
                    if(dictionary){
                        [[Client sharedClient].user initWithDictionary:dictionary];
                        self.dictionaryUser=[[Client sharedClient].user getUserInDictionary];
                        [self updateProfile:nil];
                    }
                    
                }];
            }
//            {
//                
//                [[Client sharedClient] getUserInfo:[Client sharedClient].user.email completition:^(NSDictionary *dictionary, NSError *error) {
//                    if(dictionary){[[Client sharedClient].user initWithDictionary:dictionary];}
//                }];
//                
//            }
            else{
                
                [[Utilities sharedInstance] showCustomAlertView:self.view title:@"Mabo" message:@"Error to send comment" controller:self reason:@"commentPost"];
                
//                UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"OOPS!" message:NSLocalizedString(@"Error to send comment", nil) delegate:self cancelButtonTitle:@"OK" otherButtonTitles:nil];
//                [alert show];
            }
        }else{
            
            [[Utilities sharedInstance] showCustomAlertView:self.view title:@"Mabo" message:@"Error to send comment" controller:self reason:@"commentPost"];
//            UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"OOPS!" message:NSLocalizedString(@"Error to send comment", nil) delegate:self cancelButtonTitle:@"OK" otherButtonTitles:nil];
//            [alert show];
        }
    }];
}



-(void)textPostImageTapped :(id) sender
{
    UITapGestureRecognizer *gesture = (UITapGestureRecognizer *) sender;
    NSLog(@"Tag = %ld", gesture.view.tag);
    
    NSDictionary* wallMessagesDict = [self.arrayPosts objectAtIndex:gesture.view.tag];
    
    
    NSArray * picArray = [wallMessagesDict objectForKey:@"pictures"];
    NSArray * videoArray = [wallMessagesDict objectForKey:@"videos"];
    
    if (picArray.count > 0 || videoArray.count>0) {
        
        ShowPhotoVideoViewController * showPhotoVideoVC = [self.storyboard instantiateViewControllerWithIdentifier:@"ShowPhotoVideoViewController"];
        
         if(videoArray.count > 0) {
            
            showPhotoVideoVC.titleString = @"video";
            showPhotoVideoVC.imagesArray = videoArray;
            [self.navigationController pushViewController:showPhotoVideoVC animated:YES];
        }
        else if (picArray.count > 0) {
            
            showPhotoVideoVC.titleString = @"Photos";
            showPhotoVideoVC.imagesArray = picArray;
            [self.navigationController pushViewController:showPhotoVideoVC animated:YES];
        }
        
    }
    
}

-(void)eventPostImageTapped :(id) sender
{
    UITapGestureRecognizer *gesture = (UITapGestureRecognizer *) sender;
    NSLog(@"Tag = %ld", gesture.view.tag);
    
    NSDictionary* wallMessagesDict = [self.arrayPosts objectAtIndex:gesture.view.tag];
    
    
    NSArray * picArray = [wallMessagesDict objectForKey:@"pictures"];
    NSArray * videoArray = [wallMessagesDict objectForKey:@"videos"];
    
    if (picArray.count > 0 || videoArray.count>0) {
        
        ShowPhotoVideoViewController * showPhotoVideoVC = [self.storyboard instantiateViewControllerWithIdentifier:@"ShowPhotoVideoViewController"];
        
         if(videoArray.count > 0) {
            
            showPhotoVideoVC.titleString = @"video";
            showPhotoVideoVC.imagesArray = videoArray;
            [self.navigationController pushViewController:showPhotoVideoVC animated:YES];
        }
        
       else if (picArray.count > 0) {
            
            showPhotoVideoVC.titleString = @"Photos";
            showPhotoVideoVC.imagesArray = picArray;
            [self.navigationController pushViewController:showPhotoVideoVC animated:YES];
        }
    }
}

-(void)deletePostEffet{
    [[Client sharedClient] deleteWallMessageFromId:[[dictionaryPostSelected objectForKey:@"id"] stringValue] completition:^(BOOL success, NSError *error) {
        
        [self.loading animate:NO];
        if(success){
            [self.arrayPosts removeObject:dictionaryPostSelected];
            [[Client sharedClient] getUserInfo:[Client sharedClient].user.email completition:^(NSDictionary *dictionary, NSError *error) {
                dictionaryPostSelected=nil;
                //self.cellSelected=nil;
                if(dictionary){
                    [[Client sharedClient].user initWithDictionary:dictionary];
                    self.dictionaryUser=[[Client sharedClient].user getUserInDictionary];
                    [self updateProfile:nil];
                }
            }];
            
        }else{

            [[Utilities sharedInstance] showCustomAlertView:self.view title:@"Mabo" message:@"Error to delete this post" controller:self reason:@"deletePost"];
            
        }
    }];
}

-(void)updateProfile:(NSNotification *)notification{
//    if(!self.isNotMe){
//        self.dictionaryUser = [[Client sharedClient].user getUserInDictionary];
        [self populateContent];
//    }else{
//        if([[Client sharedClient].reachAbility isReachable]){
//            [[Client sharedClient] getUserInfo:self.usernameProfile completition:^(NSDictionary *dictionary, NSError *error) {
//                //self.dictionaryUser=dictionary;
//                [self populateContent];
//                [[Client sharedClient] updateCacheInfoUserWithDictionary:dictionary andUser:self.usernameProfile];
//                //[self setNavigationControllerApparence];
//                
//            }];
//        }
   // }
}

-(void)updateLike:(NSDictionary *)dictionary status:(int)valueStatus{
    
    self.loading = [[LoadingView alloc] initWithFrame:[UIScreen mainScreen].bounds];
    [self.view addSubview:self.loading];
    [self.loading animate:YES];
    
    [[Client sharedClient] updateLikeWallMessageFromId:[[dictionary objectForKey:@"id"] stringValue] withStatus:valueStatus completition:^(BOOL success, NSError *error) {
        [self.loading animate:NO];
        if(success){
            [[Client sharedClient] getUserInfo:[Client sharedClient].user.email completition:^(NSDictionary *dictionary, NSError *error) {
                //self.cellSelected=nil;
                if(dictionary){
                    [[Client sharedClient].user initWithDictionary:dictionary];
                    self.dictionaryUser=[[Client sharedClient].user getUserInDictionary];
                    [self updateProfile:nil];
                    //[self.myPostTableViewOutlet reloadData];
                }
                
            }];
        }else{
            NSString *message = (valueStatus<0)?NSLocalizedString(@"Error to send your Like change", nil):NSLocalizedString(@"Error to send your Like", nil);
            
            [[Utilities sharedInstance] showCustomAlertView:self.view title:@"Mabo" message:message controller:self reason:@"commentPost"];
//            UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"OOPS!" message:message delegate:self cancelButtonTitle:@"OK" otherButtonTitles:nil];
//            [alert show];
        }
    }];
}

-(void)updatePartecipate:(NSDictionary *)dictionary status:(int)valueStatus{
    
    self.loading = [[LoadingView alloc] initWithFrame:[UIScreen mainScreen].bounds];
    [self.view addSubview:self.loading];
    [self.loading animate:YES];
    
    [[Client sharedClient] updatePartecipateWallMessageFromId:[[dictionary objectForKey:@"id"] stringValue] withStatus:valueStatus completition:^(BOOL success, NSError *error) {
        
        [self.loading animate:NO];
        if(success){
        
            [[Client sharedClient] getUserInfo:[Client sharedClient].user.email completition:^(NSDictionary *dictionary, NSError *error) {
                //self.cellSelected=nil;
                if(dictionary){
                    [[Client sharedClient].user initWithDictionary:dictionary];
                    self.dictionaryUser=[[Client sharedClient].user getUserInDictionary];
                    [self updateProfile:nil];
                }
                
            }];
        }else{
            NSString *message = (valueStatus<0)?NSLocalizedString(@"Error to send your participate change", nil):NSLocalizedString(@"Error to send your participate", nil);
            [[Utilities sharedInstance] showCustomAlertView:self.view title:@"Mabo" message:message controller:self reason:@"joining"];
        }
    }];
}

-(BOOL) textFieldShouldReturn: (UITextField *) textField
{
    [textField resignFirstResponder];
    
    return YES;
}

@end
