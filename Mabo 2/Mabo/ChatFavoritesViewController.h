//
//  ChatFavoritesViewController.h
//  Mabo
//
//  Created by vishnuprasathg on 11/23/17.
//  Copyright © 2017 DCI. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface ChatFavoritesViewController : UIViewController
- (IBAction)segmentValueChanged:(id)sender;
@property (weak, nonatomic) IBOutlet UISegmentedControl *segmentOutlet;
- (IBAction)plusBtnAction:(id)sender;
- (IBAction)backBtnAction:(id)sender;
@property (weak, nonatomic) IBOutlet UITableView *tableViewOutlet;
- (IBAction)profileBtnAction:(id)sender;

@end
