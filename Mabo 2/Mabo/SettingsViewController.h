//
//  SettingsViewController.h
//  Mobo
//
//  Created by vishnuprasathg on 10/23/17.
//  Copyright © 2017 DCI. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface SettingsViewController : UIViewController
- (IBAction)backBtnAction:(id)sender;
@property (weak, nonatomic) IBOutlet UIButton *shareManoBtnoutlet;
@property (weak, nonatomic) IBOutlet UIButton *logOutBtnOutlet;
@property (weak, nonatomic) IBOutlet UIButton *deleteAccountBtnOutlet;
@property (weak, nonatomic) IBOutlet UIButton *changePasswordBtnOutlet;
@property (weak, nonatomic) IBOutlet UIButton *pushNotificationBtnOutlet;
@property (weak, nonatomic) IBOutlet UIButton *blockedUserBtnOutlet;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *blockedUserLeftConstraint;

- (IBAction)pushNotificationBtnAction:(id)sender;
- (IBAction)logOutBtnAction:(id)sender;
- (IBAction)deleteBtnAction:(id)sender;
- (IBAction)shareMaboBtnAction:(id)sender;
- (IBAction)changePasswordBtnAction:(id)sender;
- (IBAction)blockedUserBtnAction:(id)sender;
@property (weak, nonatomic) IBOutlet UIButton *blockedUserBtn1Outlet;

- (IBAction)termsOfUseBtnAction:(id)sender;
- (IBAction)helpBtnAction:(id)sender;
- (IBAction)aboutBtnAction:(id)sender;
- (IBAction)faqBtnAction:(id)sender;
- (IBAction)contactBtnAction:(id)sender;
- (IBAction)rateTheAppBtnAction:(id)sender;
- (IBAction)feedBackBtnAction:(id)sender;
- (IBAction)privacyBtnAction:(id)sender;

- (void)socialShare;
- (void)logOut;
-(void)deleteAccount;

@end
