//
//  TabBarViewController.h
//  Mobo
//
//  Created by vishnuprasathg on 10/17/17.
//  Copyright © 2017 DCI. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface TabBarViewController : UITabBarController

-(void)updateBadgeItem;

@end
