//
//  CustomAlertView.h
//  Mobo
//
//  Created by vishnuprasathg on 10/16/17.
//  Copyright © 2017 DCI. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface CustomAlertView : UIView
@property (weak, nonatomic) IBOutlet UILabel *titleLblOutlet;
@property (weak, nonatomic) IBOutlet UILabel *messageLblOutlet;
@property (weak, nonatomic) IBOutlet UIButton *okBtnOutlet;


@property (strong, nonatomic) NSString * yesActionResponse;
@property (strong, nonatomic) UIViewController * alertParentViewController;

@end
