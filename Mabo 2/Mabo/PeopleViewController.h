//
//  PeopleViewController.h
//  Mobo
//
//  Created by vishnuprasathg on 10/17/17.
//  Copyright © 2017 DCI. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <Firebase/Firebase.h>

@interface PeopleViewController : UIViewController
@property (weak, nonatomic) IBOutlet UIView *mapBaseView;
@property (weak, nonatomic) IBOutlet UIButton *mapBtnOutlet;
@property (weak, nonatomic) IBOutlet UIButton *filterBtnOutlet;
@property (weak, nonatomic) IBOutlet UIButton *plusBtnOutlet;
@property (weak, nonatomic) IBOutlet UIButton *peopleBtnOutlet;
@property (weak, nonatomic) IBOutlet UITableView *tableViewOutlet;

- (IBAction)plusBtnAction:(id)sender;
- (IBAction)filterBtnAction:(id)sender;
- (IBAction)mapBtnAction:(id)sender;
- (IBAction)peopleBtnAction:(id)sender;

- (IBAction)profilePicBtnAction:(id)sender;

@end
