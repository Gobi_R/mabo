//
//  AddCommentsView.m
//  Mabo
//
//  Created by vishnuprasathg on 11/13/17.
//  Copyright © 2017 DCI. All rights reserved.
//

#import "AddCommentsView.h"
#import "Utilities.h"
#import "MyPostViewController.h"
#import "PostsViewController.h"
#import "FevouritesViewController.h"
#import "Client.h"

@implementation AddCommentsView

/*
// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect {
    // Drawing code
}
*/

- (IBAction)cancelbtnAction:(id)sender {
    
    [[Utilities sharedInstance] hideCustomAlertView:self.superview];
}

- (IBAction)okBtnAction:(id)sender {
    
    [[Utilities sharedInstance] hideCustomAlertView:self.superview];
    
    
    
    if ([_alertParentViewController isKindOfClass:MyPostViewController.class]) {
        
        
        MyPostViewController * myPostVC = _alertParentViewController;
        
        myPostVC.postComment = self.commentTxtFieldOutlet.text;
        
        [myPostVC commentOk];
    }
    
    else if ([_alertParentViewController isKindOfClass:PostsViewController.class]) {
        
        
        PostsViewController * PostVC = _alertParentViewController;
        PostVC.postComment = self.commentTxtFieldOutlet.text;
        
        [PostVC commentOk];
        
    }
    else if ([_alertParentViewController isKindOfClass:FevouritesViewController.class]) {
        
        
        FevouritesViewController * fevVC = _alertParentViewController;
        fevVC.postComment = self.commentTxtFieldOutlet.text;
        
        [fevVC commentOk];
        
    }


}
@end
