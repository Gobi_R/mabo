//
//  ChatTableViewCell.h
//  Mabo
//
//  Created by vishnuprasathg on 11/27/17.
//  Copyright © 2017 DCI. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface ChatTableViewCell : UITableViewCell
@property (weak, nonatomic) IBOutlet UIView *textMsgBaseView;
@property (weak, nonatomic) IBOutlet UITextView *textMsgTextViewOutlet;
@property (weak, nonatomic) IBOutlet UILabel *rightTimeLblOutlet;
@property (weak, nonatomic) IBOutlet UILabel *leftTimeLblOutlet;
@property (weak, nonatomic) IBOutlet UIView *mediaMsgBaseView;
@property (weak, nonatomic) IBOutlet UIImageView *leftImageViewOutlet;
@property (weak, nonatomic) IBOutlet UIImageView *rightImageViewOutlet;
@property (weak, nonatomic) IBOutlet UILabel *rightMediaTimeLblOutlet;
@property (weak, nonatomic) IBOutlet UILabel *leftMediaTimeLblOutlet;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *textViewHeightConstraint;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *textViewWidthConstraint;
@property (weak, nonatomic) IBOutlet UITextView *lextTxtMsgTextViewOutlet;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *leftTextViewHeightConstraint;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *leftTextViewWidthConstraint;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *textMsgBaseViewHeightConstraint;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *mediaMsgBaseViewHeightConstraint;
@property (weak, nonatomic) IBOutlet UIView *rightMapViewBaseView;
@property (weak, nonatomic) IBOutlet UIView *leftMapViewBaseView;
@property (weak, nonatomic) IBOutlet UIImageView *rightPlayImageViewOutlet;
@property (weak, nonatomic) IBOutlet UIImageView *leftPlayImageViewOutlet;

@end
