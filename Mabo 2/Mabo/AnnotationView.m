//
//  HKAnnotationView.m
//  honk
//
//  Created by Steewe MacBook Pro on 26/01/15.
//  Copyright (c) 2015 Steewe. All rights reserved.
//

#import "AnnotationView.h"
#import "Client.h"
#import "ActivityView.h"
#import "PrefixHeader.pch"

@interface AnnotationView()

@property (nonatomic, strong) UIImageView *picture;
@property (nonatomic, strong) UIImageView *pinView;
@property (nonatomic, strong) UIButton *arrow;
@property (nonatomic, strong) ActivityView *activity;

@end

@implementation AnnotationView

-(instancetype)initWithAnnotation:(id<MKAnnotation>)annotation reuseIdentifier:(NSString *)reuseIdentifier{
    self=[super initWithAnnotation: annotation reuseIdentifier: reuseIdentifier];
    if(self){
        self.pinView = [[UIImageView alloc] initWithFrame:self.frame];
        [self addSubview:self.pinView];
        
        self.picture = [[UIImageView alloc] initWithFrame:self.frame];
        [self addSubview:self.picture];
        [self.picture setContentMode:UIViewContentModeScaleAspectFill];
        
        //[self setLeftCalloutAccessoryView:self.picture];
        
        UIImage *imageArrow = [UIImage imageNamed:@"forward_arrow"];
        CGSize sizeArrow = CGSizeMake(imageArrow.size.width*(22)/imageArrow.size.height, 22);
        self.arrow = [UIButton buttonWithType:UIButtonTypeCustom];
        [self.arrow setFrame:CGRectMake(0, 0, sizeArrow.width, sizeArrow.height)];
        
        imageArrow = [UtilityGraphic imageWithImage:imageArrow scaledToSize:CGSizeMake(sizeArrow.width,sizeArrow.height)];
        imageArrow = [imageArrow imageWithRenderingMode:UIImageRenderingModeAlwaysTemplate];
        [self.arrow.imageView setTintColor:COLOR_MB_GREEN];
        [self.arrow setImage:imageArrow forState:UIControlStateNormal];
        
        [self setRightCalloutAccessoryView:self.arrow];
        
        self.activity = [[ActivityView alloc] initWithFrame:CGRectMake(0, 0, 20, 20)];
        [self.activity setColorBase:COLOR_MB_GREEN];
        [self.activity setColorCircles:COLOR_MB_LIGHTGRAY];
        [self.activity setCenterActivity:CGPointMake(self.picture.frame.size.width/2, self.picture.frame.size.height/2)];
        [self.activity setLineWidthCircles:1];
        [self.picture addSubview:self.activity];
        //[self.activity show];
        [self.activity setHidden:YES];
        
    }
    
    return self;
}


-(void)refreshWithAnnotation:(Annotation *)annotation{
    self.canShowCallout=(![annotation.idMessage isEqualToString:@""]);
    [self.activity setHidden:YES];
    BOOL isWall = self.canShowCallout;
    BOOL isEvent = [annotation.isEvent boolValue];
    self.idMessage=annotation.idMessage;
    self.emailUser=annotation.subtitle;
    self.nameUser=annotation.title;
    UIImage *pinImage;
    //float offsetImageX = 0;
    float offsetImageY = 3.5;
    float scaledImage = 1;
    [self.picture setHidden:YES];
    
    if(annotation.isScrumble){
        
        if ([self isFavourite:annotation.subtitle] == YES) {
            
            pinImage = [UIImage imageNamed:@"pinScrumbleFavorite"];
        }
        else
        {
            pinImage = [UIImage imageNamed:@"pinScrumble2"];
        }
        //pinImage = [UIImage imageNamed:([self isFavourite:annotation.subtitle])?@"pinScrumbleFavorite":@"pinScrumble"];
        [self.pinView setImage:pinImage];
        scaledImage = 0.75f;
        CGSize sizePicture = CGSizeMake(pinImage.size.width*scaledImage, pinImage.size.width*scaledImage);
        
       // [self.pinView setFrame:CGRectMake(0, 0, pinImage.size.width-20, pinImage.size.height-30)];
        [self.pinView setFrame:CGRectMake(0, 0, sizePicture.width-10, sizePicture.width-10)];
        self.centerOffset = CGPointMake(0, 0);
        
        [self.picture setFrame:CGRectMake(5, 5, sizePicture.width-20, sizePicture.height-20)];
        self.picture.layer.cornerRadius=self.picture.frame.size.width/2;
        [self.picture setHidden:NO];
    }else if(isWall){
        [self.arrow.imageView setTintColor:(isEvent)?COLOR_MB_YELLOW:COLOR_MB_GREEN];
        
        if (isEvent == YES) {
            
            pinImage = [UIImage imageNamed:@"pinEvent"];
        }
        else
        {
            pinImage = [UIImage imageNamed:@"pinPost"];
        }
        
       // pinImage = [UIImage imageNamed:(isEvent)?@"pinEvent":@"pinPost"];
        [self.pinView setImage:pinImage];
        
        scaledImage = 0.78f;
        CGSize sizePicture = CGSizeMake(pinImage.size.width*scaledImage, pinImage.size.width*scaledImage);
        
        [self.pinView setFrame:CGRectMake(0, 0, pinImage.size.width-20, pinImage.size.height-30)];
        self.centerOffset = CGPointMake(0, -self.pinView.frame.size.height/2);
        
        [self.picture setFrame:CGRectMake(7, 6, sizePicture.width-20, sizePicture.height-20)];
        self.picture.layer.cornerRadius=self.picture.frame.size.width/2;
    }else{
        
        if ([self isFavourite:annotation.subtitle] == YES) {
            
            pinImage = [UIImage imageNamed:@"pinFavorite"];
        }
        else
        {
            pinImage = [UIImage imageNamed:@"pin2"];
        }
       // pinImage= [UIImage imageNamed:([self isFavourite:annotation.subtitle])?@"pinFavorite":@"pin"];
        [self.pinView setImage:pinImage];
        
        
        scaledImage = 0.78f;
        CGSize sizePicture = CGSizeMake(pinImage.size.width*scaledImage, pinImage.size.width*scaledImage);
        
        [self.pinView setFrame:CGRectMake(0, 0, pinImage.size.width-20, pinImage.size.height-30)];
        self.centerOffset = CGPointMake(0, -self.pinView.frame.size.height/2);
        
        [self.picture setFrame:CGRectMake(7, 6, sizePicture.width-20, sizePicture.height-20)];
        self.picture.layer.cornerRadius=self.picture.frame.size.width/2;
        [self.picture setHidden:NO];
    }
    
    self.frame=self.pinView.frame;
    [self.picture setClipsToBounds:YES];
    
    UIImage *imageDefault = DEFAULT_PICTURE;
    imageDefault = [UtilityGraphic imageWithImage:imageDefault scaledToSize:CGSizeMake(self.picture.frame.size.width,self.picture.frame.size.height)];
    [self.picture setImage:imageDefault];
    
    
    if(!isWall){
        if(![annotation.picture isEqual:[NSNull null]]){
            if(![annotation.picture isEqualToString:@""]){
                [self.activity setFrame:CGRectMake(0, 0, self.picture.frame.size.width, self.picture.frame.size.height)];
                [self.activity setCenterActivity:CGPointMake(self.picture.frame.size.width/2, self.picture.frame.size.height/2)];
                [self.activity show];
                [self.activity setHidden:NO];
                [[Client sharedClient] getPicture:annotation.picture from:self.picture withCompletition:^(UIImage *image, BOOL isInCache, id sender) {
                    if(sender==self.picture){
                        image = [UtilityGraphic imageWithImage:image scaledToSize:CGSizeMake(self.picture.frame.size.width,self.picture.frame.size.height)];
                        
                        [self.picture setImage:image];
                        [self.activity setHidden:YES];
                    }
                }];
            }
        }
    }
}

-(BOOL)isFavourite:(NSString *)email{
    NSArray *arrayResult = [NSArray array];
    if(![[Client sharedClient].user.friends isEqual:[NSNull null]]){
        arrayResult= [UtilityGraphic filterWithKey:@"status" withValue:@"0" inArray:[Client sharedClient].user.friends withType:1];
        for(unsigned i=0;i<arrayResult.count;i++){
            NSDictionary *dic = [arrayResult objectAtIndex:i];
            if([[dic objectForKey:@"username"] isEqualToString:email]){
                return YES;
            }
        }
    }
    return NO;
}



@end
