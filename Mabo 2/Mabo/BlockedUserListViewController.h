//
//  BlockedUserListViewController.h
//  Mabo
//
//  Created by vishnuprasathg on 11/22/17.
//  Copyright © 2017 DCI. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface BlockedUserListViewController : UIViewController

@property (weak, nonatomic) IBOutlet UITableView *tableViewOutlet;
- (IBAction)backBtnAction:(id)sender;
- (IBAction)unBlockBtnAction:(id)sender;
@property (weak, nonatomic) IBOutlet UILabel *noBlockedUserLblOutlet;

-(void)unBlockUser;
@end
