//
//  FbEmailPopUpView.m
//  Mabo
//
//  Created by vishnuprasathg on 11/6/17.
//  Copyright © 2017 DCI. All rights reserved.
//

#import "FbEmailPopUpView.h"
#import "Utilities.h"
#import "Client.h"
#import "SharedVariables.h"

@implementation FbEmailPopUpView

/*
// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect {
    // Drawing code
}
*/

-(BOOL) textFieldShouldReturn: (UITextField *) textField
{
    [textField resignFirstResponder];
    
    return YES;
}

- (IBAction)submitBtnAction:(id)sender {
    
    [self.emailtxtfieldOutlet resignFirstResponder];
    
    
    BOOL isvalid = [Utilities isValidMail:self.emailtxtfieldOutlet.text];
    
    
    if(self.emailtxtfieldOutlet.text.length>0){
        
        
        if (isvalid == YES) {
            
            [SharedVariables sharedInstance].fbLogInUserInputEmail = self.emailtxtfieldOutlet.text;
            [[Client sharedClient] FBLogin];
            
            [[Utilities sharedInstance] hideCustomAlertView:self.superview];
                
        }
        else
        {
            // [Utilities alertWithMessage:@"Enter valid email address."];
           // [[Utilities sharedInstance]showCustomAlertView:self.superview title:@"Warning" message:@"Enter valid email address."];
            
            [[Utilities sharedInstance] showCustomAlertView:self.superview title:@"Warning" message:@"Enter valid email address." controller:self.superview reason:@"fbPopup"];
            
            
        }
        
    }
    else
    {
        //[[Utilities sharedInstance]showCustomAlertView:self.superview title:@"Warning" message:@"Please enter email Id"];
        
        [[Utilities sharedInstance] showCustomAlertView:self.superview title:@"Warning" message:@"Please enter email Id" controller:self.superview reason:@"fbPopup"];
        //[Utilities alertWithMessage:@"Please enter email Id"];
        
    }

    
}
@end
