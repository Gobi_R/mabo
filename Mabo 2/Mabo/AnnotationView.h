//
//  HKAnnotationView.h
//  honk
//
//  Created by Steewe MacBook Pro on 26/01/15.
//  Copyright (c) 2015 Steewe. All rights reserved.
//

#import <MapKit/MapKit.h>
#import "Annotation.h"

@interface AnnotationView : MKAnnotationView

@property (nonatomic, strong) NSString *emailUser;
@property (nonatomic, strong) NSString *nameUser;
@property (nonatomic, strong) NSString *idMessage;

-(void)refreshWithAnnotation:(Annotation *)annotation;

@end
