//
//  SettingsViewController.m
//  Mobo
//
//  Created by vishnuprasathg on 10/23/17.
//  Copyright © 2017 DCI. All rights reserved.
//

#import "SettingsViewController.h"
#import "Client.h"
#import "SinchClient.h"
#import "SignInViewController.h"
#import "Utilities.h"
#import "ChangePasswordViewController.h"
#import <FBSDKCoreKit/FBSDKCoreKit.h>
#import <FBSDKLoginKit/FBSDKLoginKit.h>
#import <FBSDKShareKit/FBSDKShareKit.h>
#import "SharedVariables.h"
#import "BlockedUserListViewController.h"
#import "LoadingView.h"
#import "HelpsViewController.h"

@interface SettingsViewController ()

@property (nonatomic, assign) BOOL isNotification;
@property (nonatomic, strong) LoadingView *loading;

@end

@implementation SettingsViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    _isNotification = YES;
    
    self.logOutBtnOutlet.layer.cornerRadius = 10;
    self.logOutBtnOutlet.layer.masksToBounds = true;
    
    self.deleteAccountBtnOutlet.layer.cornerRadius = 10;
    self.deleteAccountBtnOutlet.layer.masksToBounds = true;
    
    self.shareManoBtnoutlet.layer.cornerRadius = 10;
    self.shareManoBtnoutlet.layer.masksToBounds = true;
    
    self.changePasswordBtnOutlet.layer.cornerRadius = 10;
    self.changePasswordBtnOutlet.layer.masksToBounds = true;
    
    self.blockedUserBtnOutlet.layer.cornerRadius = 10;
    self.blockedUserBtnOutlet.layer.masksToBounds = true;
    
    self.blockedUserBtn1Outlet.layer.cornerRadius = 10;
    self.blockedUserBtn1Outlet.layer.masksToBounds = true;
    
    if ([[NSUserDefaults standardUserDefaults]boolForKey:@"isFaceBook"] == YES) {
        
        self.changePasswordBtnOutlet.hidden = YES;
        self.blockedUserBtnOutlet.hidden = YES;
        self.blockedUserBtn1Outlet.hidden = NO;
        
    }
    else
    {
        self.changePasswordBtnOutlet.hidden = NO;
        self.blockedUserBtnOutlet.hidden = NO;
        self.blockedUserBtn1Outlet.hidden = YES;
    }
    
    // Do any additional setup after loading the view.
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

- (IBAction)backBtnAction:(id)sender {
    
    [self.navigationController popViewControllerAnimated:YES];
}

-(void)actionSheet:(UIActionSheet *)actionSheet clickedButtonAtIndex:(NSInteger)buttonIndex{
    switch (actionSheet.tag) {
        case 100:
            switch(buttonIndex){
                case 0:
                    [[Client sharedClient] deleteAccountWithCompletition:^(BOOL success) {
                        if(success){
                            [[Client sharedClient] logOut];
                            [[SinchClient sharedClient].client unregisterPushNotificationData];
                            [[SinchClient sharedClient].client stop];
                            
                            SignInViewController * signVC = [self.storyboard instantiateViewControllerWithIdentifier:@"SignInViewController"];
                            [self.navigationController pushViewController:signVC animated:NO];
                            
                        }else{
                            UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"OOPS!" message:NSLocalizedString(@"Error to delete account", nil) delegate:self cancelButtonTitle:@"OK" otherButtonTitles:nil];
                            [alert show];
                        }
                    }];
                    break;
            }
            
            break;
        case 101:
            switch(buttonIndex){
                case 0:
                    [[Client sharedClient] logOut];
                    [[SinchClient sharedClient].client unregisterPushNotificationData];
                    [[SinchClient sharedClient].client stop];
                    
                    SignInViewController * signVC = [self.storyboard instantiateViewControllerWithIdentifier:@"SignInViewController"];
                    [self.navigationController pushViewController:signVC animated:NO];
                    
                    break;
            }
            break;
        case 102:
            switch(buttonIndex){
                case 0:{
                    
                    NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
                    NSString *documentsDirectoryPrimary = [paths objectAtIndex:0];
                    NSString *documentsDirectory = [documentsDirectoryPrimary stringByAppendingPathComponent:@"Cache"];
                    //NSURL *urlDirectoryCache = [NSURL fileURLWithPath:documentsDirectory];
                    
                    NSDirectoryEnumerator* en = [[NSFileManager defaultManager] enumeratorAtPath:documentsDirectory];
                    NSError* err = nil;
                    BOOL res;
                    
                    NSString* file;
                    while (file = [en nextObject]) {
                        res = [[NSFileManager defaultManager] removeItemAtPath:[documentsDirectory stringByAppendingPathComponent:file] error:&err];
                        if (!res && err) {
                            NSLog(@"oops: %@", err);
                        }
                    }
                    
                    [self revealSizeCache];
                    
                    break;}
            }
            break;
    }
}

-(void)revealSizeCache{
    NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
    NSString *documentsDirectoryPrimary = [paths objectAtIndex:0];
    NSString *documentsDirectory = [documentsDirectoryPrimary stringByAppendingPathComponent:@"Cache"];
    NSURL *urlDirectoryCache = [NSURL fileURLWithPath:documentsDirectory];
    
    if ([[NSFileManager defaultManager] fileExistsAtPath:documentsDirectory]){
        double resultSize = 0;
        
        resultSize = [[[[NSFileManager defaultManager] attributesOfItemAtPath:[urlDirectoryCache path] error:nil] objectForKey:NSFileSize] unsignedIntegerValue];
        
        int multiplyFactor = 0;
        
        NSArray *tokens = @[@"bytes",@"KB",@"MB",@"GB",@"TB"];
        
        while (resultSize > 1024) {
            resultSize /= 1024;
            multiplyFactor++;
        }
        
        //[self.deleteCache setTitle:[NSString stringWithFormat:@"Delete Cache Images (%.2f %@)",resultSize, tokens[multiplyFactor]] forState:UIControlStateNormal];
    }
    
}

- (IBAction)pushNotificationBtnAction:(id)sender {
    
    if (_isNotification == NO) {
        
        [self.pushNotificationBtnOutlet setImage:[UIImage imageNamed:@"toggle_on.png"] forState:UIControlStateNormal];
        
         _isNotification = YES;
        [[NSUserDefaults standardUserDefaults] setBool:YES forKey:@"isNotification"];
        [[NSUserDefaults standardUserDefaults] synchronize];
        
        }
    else
    {
        
        [self.pushNotificationBtnOutlet setImage:[UIImage imageNamed:@"toggle_off.png"] forState:UIControlStateNormal];
        
        _isNotification = NO;
        [[NSUserDefaults standardUserDefaults] setBool:NO forKey:@"isNotification"];
        [[NSUserDefaults standardUserDefaults] synchronize];

    }

}

- (IBAction)logOutBtnAction:(id)sender {
    
    [[Utilities sharedInstance] showCustomAlertViewWithMultiple:self title:@"Mabo" message:@"Are you sure you want to logout?" actionResponse:@"logOut"];
    
//    UIActionSheet *actionSheet = [[UIActionSheet alloc]
//                                  initWithTitle:NSLocalizedString(@"Are you sure you want to Logout?", nil)
//                                  delegate:self
//                                  cancelButtonTitle:NSLocalizedString(@"Cancel",nil)
//                                  destructiveButtonTitle:nil
//                                  otherButtonTitles:@"Logout", nil];
//    [actionSheet setTag:101];
//    [actionSheet showInView:self.view];
}

- (IBAction)deleteBtnAction:(id)sender {
    
    [[Utilities sharedInstance] showCustomAlertViewWithMultiple:self title:@"Mabo" message:@"Are you sure you want to delete your account?" actionResponse:@"deleteAccount"];
    
//    UIActionSheet *actionSheet = [[UIActionSheet alloc]
//                                  initWithTitle:NSLocalizedString(@"Are you sure you want to Delete your Account?", nil)
//                                  delegate:self
//                                  cancelButtonTitle:NSLocalizedString(@"Cancel",nil)
//                                  destructiveButtonTitle:nil
//                                  otherButtonTitles:@"Delete Account", nil];
//    [actionSheet setTag:100];
//    [actionSheet showInView:self.view];

}

- (IBAction)shareMaboBtnAction:(id)sender {
    
    //[[Utilities sharedInstance]showCustomAlertView:self.view title:@"Warning" message:@"Password and repeat password does not match."];
    
    [[Utilities sharedInstance] showCustomAlertViewWithMultiple:self title:@"Mabo" message:@"This will let you share the app to your social friends." actionResponse:@"socialShare"];
}

- (IBAction)changePasswordBtnAction:(id)sender {
    
    ChangePasswordViewController * changePwdVC = [self.storyboard instantiateViewControllerWithIdentifier:@"ChangePasswordViewController"];
    [self.navigationController pushViewController:changePwdVC animated:YES];
}

- (IBAction)blockedUserBtnAction:(id)sender {
    
    BlockedUserListViewController * blockeduserVC = [self.storyboard instantiateViewControllerWithIdentifier:@"BlockedUserListViewController"];
    [self.navigationController pushViewController:blockeduserVC animated:YES];
}
- (void)logOut
{
    
    [[Client sharedClient] lastSeen:[Client sharedClient].user.email completition:^(NSDictionary *responceDict, NSError *error) {
        if(responceDict){
            
            NSLog(@"dict:%@",responceDict);
            
            FBSDKLoginManager *loginmanager= [[FBSDKLoginManager alloc]init];
            [loginmanager logOut];
            
            [[NSUserDefaults standardUserDefaults]removeObjectForKey:@"isFaceBook"];
            
            [Client sharedClient].authenticate=NO;
            
            [[Client sharedClient] logOut];
            [[SinchClient sharedClient].client unregisterPushNotificationData];
            [[SinchClient sharedClient].client stop];
            [[SinchClient sharedClient].client terminateGracefully]; // or invoke -[SINClient terminate]
            [SinchClient sharedClient].client = nil;
            
            [[SinchClient sharedClient] deleteSinchSingleton];
            
//            [[SinchClient sharedClient].client stopListeningOnActiveConnection];
//            [[SinchClient sharedClient].client terminate];
//            [SinchClient sharedClient].client = nil;
            
            SignInViewController * signVC = [self.storyboard instantiateViewControllerWithIdentifier:@"SignInViewController"];
            [self.navigationController pushViewController:signVC animated:NO];
            
        }else{
            NSLog(@"error:%@",error);
        }
    }];
    
}

-(void)deleteAccount
{
    self.loading = [[LoadingView alloc] initWithFrame:[UIScreen mainScreen].bounds];
    [self.view addSubview:self.loading];
    [self.loading animate:YES];
    
    [[Client sharedClient] deleteAccount:[Client sharedClient].user.email completition:^(NSDictionary *responceDict, NSError *error) {
        
        [self.loading animate:NO];
        if(responceDict){
            NSLog(@"dict:%@",responceDict);
            NSString* response = [responceDict objectForKey:@"response"];
            
            if ([response isEqualToString:@"Deleted successfully"]) {
                
                FBSDKLoginManager *loginmanager= [[FBSDKLoginManager alloc]init];
                [loginmanager logOut];
                
                [[NSUserDefaults standardUserDefaults]removeObjectForKey:@"isFaceBook"];
                
                [[Client sharedClient] logOut];
                [[SinchClient sharedClient].client unregisterPushNotificationData];
                [[SinchClient sharedClient].client stop];
                
                SignInViewController * signVC = [self.storyboard instantiateViewControllerWithIdentifier:@"SignInViewController"];
                [self.navigationController pushViewController:signVC animated:NO];
                
                //[self logOut];
            }
            
        }else{
            NSLog(@"error:%@",error);
            
        }
    }];
}
- (IBAction)termsOfUseBtnAction:(id)sender {
    
    HelpsViewController * helpVC = [self.storyboard instantiateViewControllerWithIdentifier:@"HelpsViewController"];
    helpVC.viewIdendify = @"termsOfUse";
    [self.navigationController pushViewController:helpVC animated:YES];
}

- (IBAction)helpBtnAction:(id)sender {
    
    HelpsViewController * helpVC = [self.storyboard instantiateViewControllerWithIdentifier:@"HelpsViewController"];
    helpVC.viewIdendify = @"help";
    [self.navigationController pushViewController:helpVC animated:YES];
}

- (IBAction)aboutBtnAction:(id)sender {
    
    HelpsViewController * helpVC = [self.storyboard instantiateViewControllerWithIdentifier:@"HelpsViewController"];
    helpVC.viewIdendify = @"about";
    [self.navigationController pushViewController:helpVC animated:YES];
}

- (IBAction)faqBtnAction:(id)sender {
    
    HelpsViewController * helpVC = [self.storyboard instantiateViewControllerWithIdentifier:@"HelpsViewController"];
    helpVC.viewIdendify = @"faq";
    [self.navigationController pushViewController:helpVC animated:YES];
}

- (IBAction)contactBtnAction:(id)sender {
    
    HelpsViewController * helpVC = [self.storyboard instantiateViewControllerWithIdentifier:@"HelpsViewController"];
    helpVC.viewIdendify = @"contact";
    [self.navigationController pushViewController:helpVC animated:YES];
}

- (IBAction)rateTheAppBtnAction:(id)sender {
    
    HelpsViewController * helpVC = [self.storyboard instantiateViewControllerWithIdentifier:@"HelpsViewController"];
    helpVC.viewIdendify = @"rateTheApp";
    [self.navigationController pushViewController:helpVC animated:YES];
}

- (IBAction)feedBackBtnAction:(id)sender {
    
    HelpsViewController * helpVC = [self.storyboard instantiateViewControllerWithIdentifier:@"HelpsViewController"];
    helpVC.viewIdendify = @"feedBack";
    [self.navigationController pushViewController:helpVC animated:YES];
}

- (IBAction)privacyBtnAction:(id)sender {
    
    HelpsViewController * helpVC = [self.storyboard instantiateViewControllerWithIdentifier:@"HelpsViewController"];
    helpVC.viewIdendify = @"privacy";
    [self.navigationController pushViewController:helpVC animated:YES];
}

- (void)socialShare
{
    
    NSArray *activityItems = @[@"Mabo! is a simple fun way to discover the stories next door! Get connected with new people around you. Express yourself, share your amazing moments and get inspired! http://www.mabo-app.com/"];
    UIActivityViewController *activityViewControntroller = [[UIActivityViewController alloc] initWithActivityItems:activityItems applicationActivities:nil];
    activityViewControntroller.excludedActivityTypes = @[];
    if (UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPad) {
        activityViewControntroller.popoverPresentationController.sourceView = self.view;
        activityViewControntroller.popoverPresentationController.sourceRect = CGRectMake(self.view.bounds.size.width/2, self.view.bounds.size.height/4, 0, 0);
    }
    [self presentViewController:activityViewControntroller animated:true completion:nil];
}

@end
