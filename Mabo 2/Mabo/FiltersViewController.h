//
//  FiltersViewController.h
//  Mobo
//
//  Created by vishnuprasathg on 10/26/17.
//  Copyright © 2017 DCI. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "TTRangeSlider.h"


@interface FiltersViewController : UIViewController<UITextFieldDelegate,UITableViewDelegate,UITableViewDataSource,TTRangeSliderDelegate,UIScrollViewDelegate>
@property (weak, nonatomic) IBOutlet UITextField *searchTextFieldOutlet;
@property (weak, nonatomic) IBOutlet UIButton *searchBtnOutlet;
@property (weak, nonatomic) IBOutlet UIButton *resetBtnOutlet;
@property (weak, nonatomic) IBOutlet UIButton *maleBtnOutlet;
@property (weak, nonatomic) IBOutlet UIButton *femailBtnOutlet;
@property (weak, nonatomic) IBOutlet UIButton *otherBtnOutlet;
@property (weak, nonatomic) IBOutlet UIButton *friendsBtnOutlet;
@property (weak, nonatomic) IBOutlet UIButton *niceDateBtnOutlet;
@property (weak, nonatomic) IBOutlet UIButton *crazyNightOutBtnoutlet;
@property (weak, nonatomic) IBOutlet UIButton *businessContactBtnOutlet;
@property (weak, nonatomic) IBOutlet UIButton *funEventsBtnOutlet;
@property (weak, nonatomic) IBOutlet UIButton *whateverHappensBtnOutlet;
@property (weak, nonatomic) IBOutlet UILabel *startAgeLblOutlet;
@property (weak, nonatomic) IBOutlet UILabel *endAgeLblOutlet;
@property (weak, nonatomic) IBOutlet UISlider *ageSliderOutlet;
- (IBAction)ageSliderAction:(id)sender;
@property (weak, nonatomic) IBOutlet UITableView *resetTableView;
//@property (weak, nonatomic) IBOutlet TTRangeSlider *rangeSlider;
@property (weak, nonatomic) IBOutlet TTRangeSlider *sliderRange;
@property (weak, nonatomic) IBOutlet UIScrollView *baseScrollView;

- (IBAction)searchBtnAction:(id)sender;
- (IBAction)resetBtnAction:(id)sender;
- (IBAction)maleBtnAction:(id)sender;
- (IBAction)femaleBtnAction:(id)sender;
- (IBAction)otherBtnAction:(id)sender;
- (IBAction)newFriendsBtnAction:(id)sender;
- (IBAction)niceDateBtnAction:(id)sender;
- (IBAction)crazyNightOutBtnAction:(id)sender;
- (IBAction)businessContactBtnAction:(id)sender;
- (IBAction)funEventsBtnAction:(id)sender;
- (IBAction)whatEverHappensBtnAction:(id)sender;



@end
