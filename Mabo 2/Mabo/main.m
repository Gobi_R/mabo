//
//  main.m
//  Mabo
//
//  Created by vishnuprasathg on 11/23/17.
//  Copyright © 2017 DCI. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "AppDelegate.h"

int main(int argc, char * argv[]) {
    @autoreleasepool {
        return UIApplicationMain(argc, argv, nil, NSStringFromClass([AppDelegate class]));
    }
}
