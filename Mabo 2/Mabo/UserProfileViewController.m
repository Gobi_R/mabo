//
//  UserProfileViewController.m
//  Mabo
//
//  Created by vishnuprasathg on 11/14/17.
//  Copyright © 2017 DCI. All rights reserved.
//

#import "UserProfileViewController.h"
#import "Client.h"
#import "UIImageView+WebCache.h"
#import "LoadingView.h"
#import "UtilityGraphic.h"
#import "MapViewController.h"
#import "ChatViewController.h"
#import "SharedVariables.h"
#import "TabBarViewController.h"
#import "Utilities.h"

@interface UserProfileViewController ()

@property (nonatomic, strong) LoadingView *loading;

@end

@implementation UserProfileViewController

{
    NSArray * genderArray ;
    NSArray * intrestedInArray ;
    NSDictionary * dictionaryUser;
    NSString * usernameProfile;
    NSString * userLat;
    NSString * userLng;
    
    NSMutableArray * hobbiesArray ;
    NSMutableArray * occupationsArray ;
    NSMutableArray * schoolsArray ;
    
    BOOL isVisibility;
    BOOL isFavorites;
    BOOL isScrumble;
    
    BOOL isUserScrumble;
    BOOL isUserFavourite;
    BOOL isBlocked;

}

- (void)viewDidLoad {
    [super viewDidLoad];
    
    hobbiesArray = [[NSMutableArray alloc] init];
    occupationsArray = [[NSMutableArray alloc] init];
    schoolsArray = [[NSMutableArray alloc] init];
    
    self.hobbiesTableView.delegate = self;
    self.hobbiesTableView.dataSource = self;
    
    self.occupationsTableView.delegate = self;
    self.occupationsTableView.dataSource = self;
    
    self.schoolsTableView.delegate = self;
    self.schoolsTableView.dataSource = self;
    
    genderArray = [[NSArray alloc] initWithObjects:@"Male",@"Female",@"Other",@"None", nil];
    intrestedInArray = [[NSArray alloc] initWithObjects:@"New friend",@"Nice date",@"Crazy night out",@"Business contact",@"Fun events",@"Whatever happens",@"None", nil];
    

    NSLog(@"userDict:%@",_userDict);
    
    self.blockThisUserBtnOutlet.layer.cornerRadius = self.blockThisUserBtnOutlet.frame.size.height / 2;
    self.blockThisUserBtnOutlet.layer.masksToBounds = YES;
    
    if ([self.viewIdendify isEqualToString:@"notification"])
    {
       usernameProfile = self.userName;
    }
    else
    {
    
    if ([_userDict objectForKey:@"user"]) {
        
        usernameProfile = [[_userDict objectForKey:@"user"] objectForKey:@"username"];
    }
    else
    {
        usernameProfile = [_userDict objectForKey:@"username"];
    }
    }
    
    NSLog(@"username:%@",usernameProfile);
    
    if ([usernameProfile isEqualToString:[Client sharedClient].user.email]) {
        
        self.blockThisUserBtnOutlet.hidden = YES;
        self.chatBtnOutlet.hidden = YES;
        self.favoriteBtnOutlet.hidden = YES;
        self.locationBtnOutlet.hidden = YES;
        
    }
    
    self.loading = [[LoadingView alloc] initWithFrame:[UIScreen mainScreen].bounds];
    [self.view addSubview:self.loading];
    [self.loading animate:YES];
    
    [[Client sharedClient] getUserProfileDetail:usernameProfile completition:^(NSDictionary *profileDict, NSError *error) {
        
        [self.loading animate:NO];
        if(profileDict){

            NSLog(@"dict:%@",profileDict);
            dictionaryUser = [profileDict objectForKey:@"user"];
            
            [self loadData];
            
        }else{
            NSLog(@"error:%@",error);
        }
    }];
    
    // Do any additional setup after loading the view.
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

-(void)loadData
{
    
    [self refreshBlockUserButton];
    [self refreshAddFavoriteButton];
    
    self.userNameLblOutlet.text = [dictionaryUser objectForKey:@"extended_name"];
    
    self.titleLblOutlet.text = [dictionaryUser objectForKey:@"extended_name"];
    
    NSString * location_visibility = [dictionaryUser valueForKey:@"location_visibility"];
    
    
    if ([[NSString stringWithFormat:@"%@", location_visibility]  isEqual: @"1"]) {
        
        self.visibleLblOutlet.text = @"public";
        
    } else if ([[NSString stringWithFormat:@"%@", location_visibility]  isEqual: @"2"]) {
        
        self.visibleLblOutlet.text = @"only favorites";
        
    } else if ([[NSString stringWithFormat:@"%@", location_visibility]  isEqual: @"0"]) {
        
        self.visibleLblOutlet.text = @"no one";
        
    }
    
    if ([[NSString stringWithFormat:@"%@", location_visibility]  isEqual: @"1"]) {
        
        isVisibility = YES;
        isFavorites = NO;
        isScrumble = NO;
    }
    else if ([[NSString stringWithFormat:@"%@", location_visibility]  isEqual: @"2"]) {
        
        
        isVisibility = YES;
        isFavorites = YES;
        
    } else if ([[NSString stringWithFormat:@"%@", location_visibility]  isEqual: @"0"]) {
        
        isVisibility = NO;
        isFavorites = NO;
        
    }
    
    NSString * is_scrumble = [dictionaryUser valueForKey:@"is_scrumble"];
    
    if ([[NSString stringWithFormat:@"%@", is_scrumble]  isEqual: @"1"]) {
        
        
        isScrumble = YES;
    }
    else
    {
        
        isScrumble = NO;
    }
    
    NSString *resultLocationStatusText=(isFavorites == YES)?NSLocalizedString(@"Status: only favorites", nil):NSLocalizedString(@"Status: public", nil);
    
    
    if(!isVisibility == YES){
        //self.isHiddenOnMap=YES;
        resultLocationStatusText=NSLocalizedString(@"Hidden", nil);
    }
    
    BOOL resultScrumbleStatusText = (isScrumble == YES)?YES:NO;
    
    [self.visibleLblOutlet setText:[NSString stringWithFormat:@"%@",resultLocationStatusText]];
    if(resultScrumbleStatusText && isVisibility == YES && isScrumble == YES){
        [self.visibleLblOutlet setText:[NSString stringWithFormat:@"%@ %@",resultLocationStatusText,NSLocalizedString(@"(scrumble)", nil)]];
    }
    
    
    userLat = [NSString stringWithFormat:@"%@",[dictionaryUser objectForKey:@"lat"]];
    userLng = [NSString stringWithFormat:@"%@",[dictionaryUser objectForKey:@"lon"]];
    
    if ([self.visibleLblOutlet.text containsString:@"scrumble"]) {
        
        NSLog(@"scrumble");
        isUserScrumble = YES;
    }
    else
    {
      NSLog(@"NOTscrumble");
      isUserScrumble = NO;
    }

    NSArray * interests = [dictionaryUser valueForKey:@"interests"];
    hobbiesArray = interests.mutableCopy;
    
    NSArray * occupations = [dictionaryUser valueForKey:@"occupations"];
    occupationsArray = occupations.mutableCopy;
    
    NSArray * schools = [dictionaryUser valueForKey:@"schools"];
    schoolsArray = schools.mutableCopy;
    
    
    NSLog(@"hobbies:%@",hobbiesArray);
    NSLog(@"occupations:%@",occupationsArray);
    NSLog(@"schools:%@",schoolsArray);
    
    if (!(hobbiesArray.count >0)) {
        
        [hobbiesArray addObject:@"None"];
    }
    if (!(occupationsArray.count >0)) {
        
        [occupationsArray addObject:@"None"];
    }
    if (!(schoolsArray.count >0)) {
        
        [schoolsArray addObject:@"None"];
    }
    
    NSString * sex = [dictionaryUser valueForKey:@"sex"];
    
    if ([[NSString stringWithFormat:@"%@", sex]  isEqual: @"-1"]) {
        
        self.genderLblOutlet.text = @"Undefiend";
    }
    else
    {
        
        NSInteger sex_Int = [sex integerValue];
        
        self.genderLblOutlet.text = [genderArray objectAtIndex:sex_Int];
        
    }
    
    NSString * income = [dictionaryUser valueForKey:@"income"];
    
    
    NSString * interested_in = [dictionaryUser valueForKey:@"interested_in"];
    
    if ([[NSString stringWithFormat:@"%@", interested_in]  isEqual: @"-1"]) {
        
        self.interestedInLblOutlet.text = @"Undefiend";
    }
    else
    {
        
        NSInteger interested_in_Int = [interested_in integerValue];
        
        self.interestedInLblOutlet.text = [intrestedInArray objectAtIndex:interested_in_Int];
        
        
    }
    
    
    [self.hobbiesTableView reloadData];
    
    if (hobbiesArray.count >0) {
        
        self.hobbiesTableViewHeightConstraint.constant = self.hobbiesTableView.contentSize.height;
        self.hobbiesBaseViewHeightConstraint.constant = 20 + self.hobbiesTableView.contentSize.height;
        
    }
    
    
    [self.occupationsTableView reloadData];
    
    if (occupationsArray.count >0) {
        
        self.occupationsTableViewHeightConstraint.constant = self.occupationsTableView.contentSize.height;
        self.occupationsBaseViewHeightConstraint.constant = 20 + self.occupationsTableView.contentSize.height;

    }
    
    
    [self.schoolsTableView reloadData];
    
    if (schoolsArray.count >0) {
        
        self.schoolsTableViewHeightConstraint.constant = self.schoolsTableView.contentSize.height;
        self.schoolsBaseViewHeightConstraint.constant = 20 + self.schoolsTableView.contentSize.height;
    }
    
    
    NSArray * profilePic = [dictionaryUser valueForKey:@"pictures"];
    
    if (profilePic.count >0) {
        
        
        dispatch_queue_t queue = dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0ul);
        dispatch_async(queue, ^{
            
            NSDictionary * picFirstDict = profilePic.firstObject;
            
            NSString * profilePicPath = [picFirstDict valueForKey:@"web_path"];
            
            NSString * urlstring = [NSString stringWithFormat:@"http://34.230.105.103:8002/%@", profilePicPath];
            NSURL *imageURL = [NSURL URLWithString:urlstring];
            NSData *imageData = [NSData dataWithContentsOfURL:imageURL];
            UIImage *image = [UIImage imageWithData:imageData];
            
            dispatch_sync(dispatch_get_main_queue(), ^{
                
                [self.profileImageViewOutlet sd_setImageWithURL:imageURL placeholderImage:[UIImage imageNamed:@"avatar2.png"]];
                
                
            });
        });
        
    }
    else
    {
        self.profileImageViewOutlet.image = [UIImage imageNamed:@"avatar2.png"];
    }
    
    self.profileImageViewOutlet.userInteractionEnabled = YES;
    
    self.profileImageViewOutlet.layer.cornerRadius = self.profileImageViewOutlet.frame.size.height / 2;
    self.profileImageViewOutlet.layer.masksToBounds = YES;
    
    
    NSString * age = [NSString stringWithFormat:@"%@",[dictionaryUser valueForKey:@"age"]];
    
    if ([age isEqualToString:@"-1"]) {
        self.dateOfBirthLblOutlet.text = @"-";
    }
    else
    {
    NSString * timeStampString = [dictionaryUser valueForKey:@"age"];
    NSTimeInterval _interval=[timeStampString doubleValue];
    NSDate *date = [NSDate dateWithTimeIntervalSince1970:_interval];
    NSLog(@"%@", date);
    
        NSDateFormatter *dateformatter=[[NSDateFormatter alloc]init];
        [dateformatter setDateFormat:@"dd MMMM yyyy"];
        NSString *dateString=[dateformatter stringFromDate:date];
    
    self.dateOfBirthLblOutlet.text = dateString;
    }
    
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

-(void)refreshAddFavoriteButton{
    NSPredicate *predicate = [NSPredicate predicateWithFormat:@"(username==%@)",usernameProfile];
    NSArray *arrayResult = [NSArray array];
    
    if(![[Client sharedClient].user.friends isEqual:[NSNull null]]){
        
        arrayResult= [UtilityGraphic filterWithKey:@"status" withValue:@"0" inArray:[Client sharedClient].user.friends withType:1];
        arrayResult=[arrayResult filteredArrayUsingPredicate:predicate];
    }
    
    
    if(arrayResult.count>0){
        isUserFavourite=YES;
        [self.favoriteBtnOutlet setSelected:YES];
        [self.favoriteBtnOutlet setImage:[UIImage imageNamed:@"ic_fav_on.png"] forState:UIControlStateNormal];
        
    }else{
        isUserFavourite=NO;
        [self.favoriteBtnOutlet setSelected:NO];
        [self.favoriteBtnOutlet setImage:[UIImage imageNamed:@"ic_fav_off.png"] forState:UIControlStateNormal];
    }
}

-(void)refreshBlockUserButton{
    NSPredicate *predicate = [NSPredicate predicateWithFormat:@"(username==%@)",usernameProfile];
    NSArray *arrayResult = [NSArray array];
    if(![[Client sharedClient].user.friends isEqual:[NSNull null]]){
        arrayResult = [UtilityGraphic filterWithKey:@"status" withValue:@"1" inArray:[Client sharedClient].user.friends withType:1];
        arrayResult = [arrayResult filteredArrayUsingPredicate:predicate];
    }
    if(arrayResult.count>0){
        isBlocked=YES;
        [self.blockThisUserBtnOutlet setTitle:NSLocalizedString(@"UNBLOCK THIS USER", nil) forState:UIControlStateNormal];
//        [self.blockThisUserBtnOutlet setTitleColor:COLOR_MB_GREEN forState:UIControlStateNormal];
//        [self.blockThisUserBtnOutlet.layer setBorderColor:COLOR_MB_GREEN.CGColor];
    }else{
        isBlocked=NO;
        [self.blockThisUserBtnOutlet setTitle:NSLocalizedString(@"BLOCK THIS USER", nil) forState:UIControlStateNormal];
//        [self.blockThisUserBtnOutlet setTitleColor:UIColorFromRGB(0xcc5555, 1.0f) forState:UIControlStateNormal];
//        [self.blockThisUserBtnOutlet.layer setBorderColor:UIColorFromRGB(0xcc5555, 1.0f).CGColor];
    }
    
    
}

- (IBAction)favoriteBtnAction:(id)sender {
    
    self.loading = [[LoadingView alloc] initWithFrame:[UIScreen mainScreen].bounds];
    [self.view addSubview:self.loading];
    [self.loading animate:YES];
    
    NSString *valueFriends = (isUserFavourite)?@"-1":@"0";
    [[Client sharedClient] updateFriend:usernameProfile withStatus:valueFriends completition:^(BOOL success, NSError *error) {
        [self.loading animate:NO];
        if(success){
            [[Client sharedClient] updateUserCahceWithcompletition:^(BOOL success, NSError *error) {
                [self refreshAddFavoriteButton];
            }];
            
        }else{
            NSLog(@"ERROR ADD FAVORITE: %@",error);
        }
    }];
}

- (IBAction)locationBtnAction:(id)sender {
    
    
    UIViewController *top = self.window.rootViewController;
    while (top.presentedViewController) {
        top = top.presentedViewController;
    }
    
    id WindowRootVC = [[[[UIApplication sharedApplication] delegate] window] rootViewController];
    
    id currentViewController = [self findTopViewController:WindowRootVC];
    
    if (isUserScrumble == YES) {
        
        MapViewController * mapVC = [self.storyboard instantiateViewControllerWithIdentifier:@"MapViewController"];
        mapVC.viewIdendity = @"userProfile_Scrumble";
        mapVC.latti = [userLat doubleValue];
        mapVC.longi = [userLng doubleValue];
        //[self.navigationController pushViewController:mapVC animated:YES];
        [currentViewController presentViewController:mapVC animated:YES completion:nil];
        
    }
    else
    {
        MapViewController * mapVC = [self.storyboard instantiateViewControllerWithIdentifier:@"MapViewController"];
        mapVC.viewIdendity = @"userProfile";
        mapVC.latti = [userLat doubleValue];
        mapVC.longi = [userLng doubleValue];
        //[self.navigationController pushViewController:mapVC animated:YES];
        [currentViewController presentViewController:mapVC animated:YES completion:nil];
    }
    
}

-(id)findTopViewController:(id)inController
{
    /* if ur using any Customs classes, do like this.
     * Here SlideNavigationController is a subclass of UINavigationController.
     * And ensure you check the custom classes before native controllers , if u have any in your hierarchy.
     if ([inController isKindOfClass:[SlideNavigationController class]])
     {
     return [self findTopViewController:[inController visibleViewController]];
     }
     else */
    
    if ([inController isKindOfClass:[UITabBarController class]])
    {
        return [self findTopViewController:[inController selectedViewController]];
    }
    else if ([inController isKindOfClass:[UINavigationController class]])
    {
        return [self findTopViewController:[inController visibleViewController]];
    }
    else if ([inController isKindOfClass:[UIViewController class]])
    {
        return inController;
    }
    else
    {
        NSLog(@"Unhandled ViewController class : %@",inController);
        return nil;
    }
}

- (IBAction)chatBtnAction:(id)sender {

//    NSDictionary*dictionaryPostSelected = self.chatList[indexPath.row];
//
//    NSLog(@"dict:%@",dictionaryPostSelected);
    
    NSLog(@"dict:%@",_userDict);
    
//    ChatViewController * chatVC = [self.storyboard instantiateViewControllerWithIdentifier:@"ChatViewController"];
//    [self.navigationController pushViewController:chatVC animated:NO];
    
//    ChatViewController * chatVC = [self.storyboard instantiateViewControllerWithIdentifier:@"ChatViewController"];
//    chatVC.viewIdendify = @"UserProfileView";
//    // [self.navigationController pushViewController:userProfileVC animated:YES];
//    [self.navigationController presentViewController:chatVC animated:NO completion:nil];
    
    ChatViewController * chatVC = [self.storyboard instantiateViewControllerWithIdentifier:@"ChatViewController"];
    chatVC.viewIdendify = @"UserProfileView";
    if (_userDict[@"user"] == nil) {
        
        chatVC.userDictionaryToChat = _userDict;
        
    }
    else
    {
        chatVC.userDictionaryToChat = _userDict[@"user"];
    }
    UINavigationController *nav = [[UINavigationController alloc] initWithRootViewController:chatVC];
    [self presentViewController:nav animated:NO completion:nil];
    
//    ChatViewController * chatVC = [self.storyboard instantiateViewControllerWithIdentifier:@"ChatViewController"];
//    chatVC.userDictionaryToChat = dictionaryUser;
//    [self.navigationController pushViewController:chatVC animated:YES];
    
//    ChatViewController *chatVC =[[ChatViewController alloc]initWithNibName:@"ChatViewController" bundle:nil];
//
//    viewIdendify
//    //UINavigationController *nav = [[UINavigationController alloc] initWithRootViewController:chatVC];
//    UINavigationController *nav = [SharedVariables sharedInstance].tabBarController.parentViewController;
//    [nav pushViewController:chatVC animated:YES];
    
    
    //[self presentViewController:nav animated:YES completion:nil];
//    ChatViewController * chatVC = [self.storyboard instantiateViewControllerWithIdentifier:@"ChatViewController"];
//    chatVC.userDictionaryToChat = _userDict;
//    [self.navigationController pushViewController:chatVC animated:YES];
}

- (IBAction)blockThisUserBtnAction:(id)sender {
    
    self.loading = [[LoadingView alloc] initWithFrame:[UIScreen mainScreen].bounds];
    [self.view addSubview:self.loading];
    [self.loading animate:YES];
    
    NSString *valueBlock = (isBlocked)?@"-1":@"1";
    [[Client sharedClient] updateFriend:usernameProfile withStatus:valueBlock completition:^(BOOL success, NSError *error) {
        
        [self.loading animate:NO];
        if(success){
            [[Client sharedClient] updateUserCahceWithcompletition:^(BOOL success, NSError *error) {
//                [self refreshBlockUserButton];
//                [self.navigationController popViewControllerAnimated:YES];
                
                [[Utilities sharedInstance]showCustomAlertView:self.view title:@"Mabo" message:@"User blocked successfully" controller:self reason:@"blockedUser"];
                
                
                NSLog(@"ok block");
                
                
            }];
            
        }else{
            NSLog(@"ERROR BLOCK USER: %@",error);
        }
    }];
}

-(void)blockedSuccessfully
{
    
    [self dismissViewControllerAnimated:YES completion:nil];
    
//    if ([self.viewIdendify isEqualToString:@"favoritesPost"])
//    {
//        UIViewController *top = self.window.rootViewController;
//        while (top.presentedViewController) {
//            top = top.presentedViewController;
//        }
//
//        id WindowRootVC = [[[[UIApplication sharedApplication] delegate] window] rootViewController];
//
//        // id currentViewController = [self findTopViewController:WindowRootVC];
//
//
//        UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"Main"
//                                                             bundle: nil];
//        //_client.callClient.delegate = self;
//
//        //CallViewController * callViewController = [storyboard instantiateViewControllerWithIdentifier:@"CallViewController"];
//
//        TabBarViewController * tabVC = [SharedVariables sharedInstance].tabBarController;
//        NSLog(@"%@",self.parentViewController);
//
//        [tabVC setSelectedIndex:2];
//    }
    
}
- (IBAction)backBtnAction:(id)sender {
    
   // [self.navigationController popViewControllerAnimated:YES];
    
    [self dismissViewControllerAnimated:YES completion:nil];
    
    if ([self.viewIdendify isEqualToString:@"favoritesPost"])
    {
        UIViewController *top = self.window.rootViewController;
        while (top.presentedViewController) {
            top = top.presentedViewController;
        }
        
        id WindowRootVC = [[[[UIApplication sharedApplication] delegate] window] rootViewController];
        
       // id currentViewController = [self findTopViewController:WindowRootVC];
        
        
        UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"Main"
                                                             bundle: nil];
        //_client.callClient.delegate = self;
        
        //CallViewController * callViewController = [storyboard instantiateViewControllerWithIdentifier:@"CallViewController"];
        
        TabBarViewController * tabVC = [SharedVariables sharedInstance].tabBarController;
        NSLog(@"%@",self.parentViewController);
        
        [tabVC setSelectedIndex:2];
    }
    else
    {
        
    }
}


- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 1;    //count of section
}


- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    

    if (tableView == self.hobbiesTableView)
    {
        return hobbiesArray.count;
    }
    else if (tableView == self.occupationsTableView)
    {
        return occupationsArray.count;
    }
    else if (tableView == self.schoolsTableView)
    {
        return schoolsArray.count;
    }
    else
    {
        return 1;
    }
    
    //count number of row from counting array hear cataGorry is An Array
}



- (UITableViewCell *)tableView:(UITableView *)tableView
         cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    UITableViewCell *cell = [[UITableViewCell alloc] init];

     if (tableView == self.hobbiesTableView)
    {
        cell = (UITableViewCell *) [tableView dequeueReusableCellWithIdentifier:@"CellHobbies"];
        UILabel *lbldescription = (UILabel *)[cell viewWithTag:1];
        
        if (hobbiesArray.count >0) {
            
            lbldescription.text = [hobbiesArray objectAtIndex:indexPath.row];
        }
        
    }
    else if (tableView == self.occupationsTableView)
    {
        cell = (UITableViewCell *) [tableView dequeueReusableCellWithIdentifier:@"CellOccupations"];
        UILabel *lbldescription = (UILabel *)[cell viewWithTag:1];
        
        if (occupationsArray.count >0) {
            
            lbldescription.text = [occupationsArray objectAtIndex:indexPath.row];
        }
        
    }
    else if (tableView == self.schoolsTableView)
    {
        cell = (UITableViewCell *) [tableView dequeueReusableCellWithIdentifier:@"CellSchools"];
        UILabel *lbldescription = (UILabel *)[cell viewWithTag:1];
        
        if (schoolsArray.count >0) {
            
            lbldescription.text = [schoolsArray objectAtIndex:indexPath.row];
        }
        
    }
    
    return cell;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    
    
}


@end
