//
//  ProfileViewController.h
//  Mobo
//
//  Created by vishnuprasathg on 10/16/17.
//  Copyright © 2017 DCI. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface ProfileViewController : UIViewController<UITableViewDelegate,UITableViewDataSource,UITextFieldDelegate,UIPickerViewDelegate,UIPickerViewDataSource,UIGestureRecognizerDelegate,UIScrollViewDelegate>
@property (weak, nonatomic) IBOutlet UITextField *userNameTxtFieldOutlet;
@property (weak, nonatomic) IBOutlet UIScrollView *baseScrollView;
//@property (weak, nonatomic) IBOutlet UILabel *userNameLblOutlet;
@property (weak, nonatomic) IBOutlet UIImageView *userImageLblOutlet;
@property (weak, nonatomic) IBOutlet UILabel *userVisibleLblOutlet;
@property (weak, nonatomic) IBOutlet UIButton *visibilityBtnOutlet;
@property (weak, nonatomic) IBOutlet UIButton *onlyWithFavouritesBtnOutlet;
@property (weak, nonatomic) IBOutlet UIButton *scrumbleLocationBtnOutlet;
@property (weak, nonatomic) IBOutlet UIButton *genderBtnOutlet;
@property (weak, nonatomic) IBOutlet UIButton *dateOfBirthBtnOutlet;
@property (weak, nonatomic) IBOutlet UIButton *interestedInBtnOutlet;
@property (weak, nonatomic) IBOutlet UIButton *interestsPlusBtnOutlet;
@property (weak, nonatomic) IBOutlet UIButton *occuptionsPlusBtnOutlet;
@property (weak, nonatomic) IBOutlet UIButton *schoolsPlusBtnOutlet;

@property (weak, nonatomic) IBOutlet UILabel *genderSelectedLblOutlet;
@property (weak, nonatomic) IBOutlet UILabel *dobLblOutlet;
@property (weak, nonatomic) IBOutlet UILabel *intrestedInLblOutlet;

@property (weak, nonatomic) IBOutlet UISegmentedControl *myProfileSegmentControlOutlet;
@property (weak, nonatomic) IBOutlet UIButton *editBtnOutlet;

- (IBAction)editBtnAction:(id)sender;
- (IBAction)myProfileSegmentControlAction:(id)sender;
- (IBAction)scrumbleInfoBtnAction:(id)sender;


- (IBAction)visibilityOnOffBtnAction:(id)sender;
- (IBAction)onlyWithFavoritesOnOffBtnAction:(id)sender;
- (IBAction)scrumbleOnOffBtnAction:(id)sender;

- (IBAction)genderDropdownBtnAction:(id)sender;
- (IBAction)dateOfbirthDropdownBtnAction:(id)sender;
- (IBAction)interestedInDropdownBtnAction:(id)sender;

- (IBAction)interestsPlusBtnAction:(id)sender;
- (IBAction)occupationsPlusBtnAction:(id)sender;
- (IBAction)schoolsPlusBtnAction:(id)sender;

- (IBAction)interestsCloseBtnAction:(id)sender;
- (IBAction)occupationsCloseBtnAction:(id)sender;
- (IBAction)schoolsCloseBtnAction:(id)sender;


@property (weak, nonatomic) IBOutlet NSLayoutConstraint *dobTopConstraint;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *interestedInTopConstraint;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *thirdViewheightConstraint;

@property (weak, nonatomic) IBOutlet NSLayoutConstraint *incomeTopConstraint;
@property (weak, nonatomic) IBOutlet UIView *schoolsBaseView;

@property (weak, nonatomic) IBOutlet NSLayoutConstraint *genderTableViewHeightConstraint;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *datePickerHeightConstraint;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *interestedInTableViewHeightConstraint;
@property (weak, nonatomic) IBOutlet UITableView *genderTableView;
@property (weak, nonatomic) IBOutlet UIDatePicker *customeDatePicker;
@property (weak, nonatomic) IBOutlet UITableView *interstedInTableView;

@property (weak, nonatomic) IBOutlet NSLayoutConstraint *interstsBaseViewHeightConstraint;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *interestsTableViewHeightConstraint;
@property (weak, nonatomic) IBOutlet UITableView *interestsTableView;
@property (weak, nonatomic) IBOutlet UITextField *interetTxtFieldOtlet;


@property (weak, nonatomic) IBOutlet NSLayoutConstraint *occupationsBaseViewHeightConstraint;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *occupationsTableViewHeightConstraint;
@property (weak, nonatomic) IBOutlet UITableView *occupationsTableView;
@property (weak, nonatomic) IBOutlet UITextField *occupationsTxtFieldOutlet;

@property (weak, nonatomic) IBOutlet NSLayoutConstraint *schoolsBaseViewHeightConstraint;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *schoolsTableViewHeightConstraint;
@property (weak, nonatomic) IBOutlet UITableView *schoolsTableView;
@property (weak, nonatomic) IBOutlet UITextField *schoolsTxtFieldOutlet;

- (IBAction)settingBtnAction:(id)sender;
- (IBAction)notificationBtnAction:(id)sender;

-(void)openCamera;

-(void)openGallery;

-(void)later;

-(void)afterEditButtonPressed;


@end
