//
//  LikesViewController.h
//  Mabo
//
//  Created by vishnuprasathg on 11/13/17.
//  Copyright © 2017 DCI. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface LikesViewController : UIViewController<UITableViewDelegate, UITableViewDataSource>
@property (weak, nonatomic) IBOutlet UITableView *tableViewOutlet;

@property (nonatomic, retain) NSString * wallIdString;
- (IBAction)profilePicAction:(id)sender;

- (IBAction)backBtnAction:(id)sender;
@end
