//
//  profilePopUpView.m
//  Mobo
//
//  Created by vishnuprasathg on 10/26/17.
//  Copyright © 2017 DCI. All rights reserved.
//

#import "profilePopUpView.h"
#import "Utilities.h"
#import "ProfileViewController.h"

@implementation profilePopUpView

/*
// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect {
    // Drawing code
}
*/


-(void)alertWithMultiple:(NSString *)title message:(NSString *)message
{
    
    self.messageLblOutlet.text = message;
    self.titleLblOutlet.text = title;
    
}

- (IBAction)laterBtnAction:(id)sender {
    
    
    if ([_alertParentViewController isKindOfClass: ProfileViewController.class]) {
        
        [[Utilities sharedInstance] hideCustomAlertView:self.superview];
        
        ProfileViewController * profileVC = _alertParentViewController;
        
        [profileVC later];
        
        
    }
    
}

- (IBAction)gotItBtnAction:(id)sender {
    
    [[Utilities sharedInstance] hideCustomAlertView:self.superview];
}


@end
