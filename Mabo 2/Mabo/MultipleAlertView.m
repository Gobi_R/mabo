//
//  MultipleAlertView.m
//  Mobo
//
//  Created by vishnuprasathg on 10/24/17.
//  Copyright © 2017 DCI. All rights reserved.
//

#import "MultipleAlertView.h"
#import "Utilities.h"
#import "SettingsViewController.h"
#import "CreatPostViewController.h"
#import "MyPostViewController.h"
#import "FevouritesViewController.h"
#import "PostsViewController.h"
#import "BlockedUserListViewController.h"

@implementation MultipleAlertView

/*
// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect {
    // Drawing code
}
*/

-(void)alertWithMultiple:(NSString *)title message:(NSString *)message
{
    
    self.alertmessageLblOut.text = message;
    self.alertTitleLblOutlet.text = title;
    
}


- (IBAction)noBtnAction:(id)sender {
    
    [[Utilities sharedInstance] hideCustomAlertView:self.superview];
}

- (IBAction)yesBtnAction:(id)sender {
    
    if ([_alertParentViewController isKindOfClass:SettingsViewController.class]) {
        
        if ([_yesActionResponse isEqualToString:@"socialShare"]) {
            
            [[Utilities sharedInstance] hideCustomAlertView:self.superview];
            
            SettingsViewController * settingVC = _alertParentViewController;
            
            [settingVC socialShare];
        }
        
        else if ([_yesActionResponse isEqualToString:@"logOut"]) {
            
            [[Utilities sharedInstance] hideCustomAlertView:self.superview];
            
            SettingsViewController * settingVC = _alertParentViewController;
            
            [settingVC logOut];
        }
        else if ([_yesActionResponse isEqualToString:@"deleteAccount"]) {
            
            [[Utilities sharedInstance] hideCustomAlertView:self.superview];
            
            SettingsViewController * settingVC = _alertParentViewController;
            
            [settingVC deleteAccount];
        }
        
        
        
    }
    else if ([_alertParentViewController isKindOfClass:CreatPostViewController.class])
    {
    
        if ([_yesActionResponse isEqualToString:@"addPhotos"]) {
            
            [[Utilities sharedInstance] hideCustomAlertView:self.superview];
            
            CreatPostViewController * creatPostVC = _alertParentViewController;
            
            [creatPostVC addPhotosAlert];
        }
        
        else if ([_yesActionResponse isEqualToString:@"addVideos"]) {
            
            [[Utilities sharedInstance] hideCustomAlertView:self.superview];
            
            CreatPostViewController * creatPostVC = _alertParentViewController;
            
            [creatPostVC addVideosAlert];
        }

        
    }
    
    else if ([_alertParentViewController isKindOfClass:MyPostViewController.class])
    {
        
        if ([_yesActionResponse isEqualToString:@"deletePost"]) {
            
            [[Utilities sharedInstance] hideCustomAlertView:self.superview];
            
            MyPostViewController * myPostVC = _alertParentViewController;
            
            [myPostVC postDelete];
        }
        
        
    }
    else if ([_alertParentViewController isKindOfClass:FevouritesViewController.class])
    {
        
        if ([_yesActionResponse isEqualToString:@"deletePost"]) {
            
            [[Utilities sharedInstance] hideCustomAlertView:self.superview];
            
            FevouritesViewController * favoritesVC = _alertParentViewController;
            
            [favoritesVC postDelete];
        }
        
        
    }
    else if ([_alertParentViewController isKindOfClass:PostsViewController.class])
    {
        
        if ([_yesActionResponse isEqualToString:@"deletePost"]) {
            
            [[Utilities sharedInstance] hideCustomAlertView:self.superview];
            
            PostsViewController * PostsVC = _alertParentViewController;
            
            [PostsVC postDelete];
        }
        
    }
    else if ([_alertParentViewController isKindOfClass:BlockedUserListViewController.class])
    {
        
        if ([_yesActionResponse isEqualToString:@"unblock"]) {
            
            [[Utilities sharedInstance] hideCustomAlertView:self.superview];
            
            BlockedUserListViewController * blockedUserVC = _alertParentViewController;
            
            [blockedUserVC unBlockUser];
        }
        
    }
}

@end
