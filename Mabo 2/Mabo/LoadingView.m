//
//  LoadingView.m
//  honk
//
//  Created by Steewe MacBook Pro on 06/03/15.
//  Copyright (c) 2015 Steewe. All rights reserved.
//

#import "LoadingView.h"
//#import "HKActivityView.h"

@interface LoadingView()

//@property (nonatomic, strong) HKActivityView *activity;

@end

@implementation LoadingView


-(instancetype)initWithFrame:(CGRect)frame{
    self=[super initWithFrame:frame];
    if(self){
        [self setBackgroundColor:[[UIColor blackColor] colorWithAlphaComponent:.6]];
        
        UIActivityIndicatorView *activity = [[UIActivityIndicatorView alloc] initWithActivityIndicatorStyle:UIActivityIndicatorViewStyleWhiteLarge];
        [activity setCenter:CGPointMake(CGRectGetMidX(self.frame),CGRectGetMidY(self.frame))];
        [self addSubview:activity];
        [activity startAnimating];

        /*
        self.activity = [[HKActivityView alloc] initWithFrame:CGRectMake(0, 0, 50, 50)];
        [self.activity setColorBase:HK_GREEN];
        [self.activity setTintBar:[UIColor whiteColor]];
        [self.activity setCenterActivity:CGPointMake(CGRectGetMidX(self.frame), CGRectGetMidY(self.frame))];
        [self.activity setLineWidthCircle:4];
        [self addSubview:self.activity];
        [self.activity show];
        */
        self.alpha=0;
    }
    return self;
}

/*
-(void)restartActivity{
    [self.activity startAnimation];
}
*/

-(void)animate:(BOOL)value{
    if(value){
        [UIView animateWithDuration:.3 animations:^{
            self.alpha=1;
        }];
    }else{
        [UIView animateWithDuration:.3 animations:^{
            self.alpha=0;
        } completion:^(BOOL finished) {
            [self removeFromSuperview];
        }];
    }
}

@end
