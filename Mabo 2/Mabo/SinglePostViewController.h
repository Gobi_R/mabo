//
//  SinglePostViewController.h
//  Mabo
//
//  Created by Aravind Kumar on 02/01/18.
//  Copyright © 2018 DCI. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface SinglePostViewController : UIViewController<UITableViewDelegate,UITableViewDataSource,UIGestureRecognizerDelegate,UITextFieldDelegate>
@property (weak, nonatomic) IBOutlet UISegmentedControl *myPostSegmentControlOutlet;
- (IBAction)MyPostSegmentControlAction:(id)sender;
- (IBAction)newBtnAction:(id)sender;
@property (strong, nonatomic) IBOutlet UIView *textPostViewOutlet;
@property (strong, nonatomic) IBOutlet UIView *eventPostViewOutlet;
@property (weak, nonatomic) IBOutlet UITableView *myPostTableViewOutlet;
@property (weak, nonatomic) IBOutlet UIButton *backBtnOutlet;
@property (weak, nonatomic) IBOutlet UILabel *emptyPostLblOutlet;
- (IBAction)backbtnAction:(id)sender;
- (IBAction)delectPostAction:(id)sender;
- (IBAction)locationBtnAction:(id)sender;
- (IBAction)likeBtnAction:(id)sender;
- (IBAction)commentsBtnAction:(id)sender;

- (IBAction)editPostBtnAction:(id)sender;
- (IBAction)joiningBtnAction:(id)sender;
- (IBAction)mayBeBtnAction:(id)sender;
- (IBAction)willBeThereAction:(id)sender;
- (IBAction)shareBtnAction:(id)sender;
- (IBAction)myPostProfilePicAction:(id)sender;


- (IBAction)PostLikedThisBtnaction:(id)sender;
- (IBAction)PostCommentsBtnAction:(id)sender;

@property (nonatomic, retain) NSString * postComment;
@property (nonatomic, retain) NSString * idWall;


-(void)commentOk;
-(void)postDelete;

@end
