//
//  TabBarViewController.m
//  Mobo
//
//  Created by vishnuprasathg on 10/17/17.
//  Copyright © 2017 DCI. All rights reserved.
//

#import "TabBarViewController.h"
#import "SharedVariables.h"
#import "Client.h"
#import "ChatRecentsViewController.h"
#import "SinchClient.h"

@interface TabBarViewController ()

@property (nonatomic, strong) NSMutableArray *chatList;

@property int lastCountBadge;

@end

@implementation TabBarViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    //[SharedVariables sharedInstance].tabBarController = self.tabBarController;
   // [self.tabBarItem setTitleTextAttributes:[NSDictionary dictionaryWithObjectsAndKeys:
//                                             [UIFont fontWithName:@"AmericanTypewriter" size:20.0f], UITextAttributeFont,
//                                             [UIColor blackColor], UITextAttributeTextColor,
//                                             [UIColor grayColor], UITextAttributeTextShadowColor,
//                                             [NSValue valueWithUIOffset:UIOffsetMake(0.0f, 1.0f)], UITextAttributeTextShadowOffset,
//                                             nil]];
    
    [Client sharedClient].authenticate=YES;
    NSLog(@"autenticato");
    [[SinchClient sharedClient] activeClient];
    
//    [[NSNotificationCenter defaultCenter] postNotificationName:@"UserDidLoginNotification"
//                                                        object:nil
//                                                      userInfo:@{
//                                                                 @"userId" : [Client sharedClient].user.email
//                                                                 }];
    
    for(UIViewController *tab in  self.viewControllers)
        
    {
        [tab.tabBarItem setTitleTextAttributes:[NSDictionary dictionaryWithObjectsAndKeys:[UIFont fontWithName:@"ProximaNova-Regular_0" size:50.0f], NSFontAttributeName, nil] forState:UIControlStateNormal];
        
    }
    
    [self.navigationController setNavigationBarHidden:YES animated:NO];
    self.selectedIndex = 4;
    
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(updateBadgeItem) name:@"cacheUpload" object:nil];
}
-(void)viewWillAppear:(BOOL)animated
{
    NSLog(@"%@",self.tabBarController);
    
   // [[NSNotificationCenter defaultCenter] removeObserver:self name:@"cacheUpload" object:nil];
    //[[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(receiveCacheUpload:) name:@"cacheUpload" object:nil];
}

- (void)viewWillLayoutSubviews {
    
    
    if([[UIDevice currentDevice]userInterfaceIdiom]==UIUserInterfaceIdiomPhone) {
        
        switch ((int)[[UIScreen mainScreen] nativeBounds].size.height) {
                
            case 1136:
                printf("iPhone 5 or 5S or 5C");
                
                CGRect tabFrame = self.tabBar.frame; //self.TabBar is IBOutlet of your TabBar
                tabFrame.size.height = 60;
                tabFrame.origin.y = self.view.frame.size.height - 60;
                self.tabBar.frame = tabFrame;
                
                break;
            case 1334:
                printf("iPhone 6/6S/7/8");
                
                CGRect tabFrame1 = self.tabBar.frame; //self.TabBar is IBOutlet of your TabBar
                tabFrame1.size.height = 60;
                tabFrame1.origin.y = self.view.frame.size.height - 60;
                self.tabBar.frame = tabFrame1;
                
                break;
            case 1920:
                printf("iPhone 6+/6S+/7+/8+");
                
                CGRect tabFrame2 = self.tabBar.frame; //self.TabBar is IBOutlet of your TabBar
                tabFrame2.size.height = 60;
                tabFrame2.origin.y = self.view.frame.size.height - 60;
                self.tabBar.frame = tabFrame2;
                
                break;
            case 2208:
                printf("iPhone 6+/6S+/7+/8+");
                
                CGRect tabFrame3 = self.tabBar.frame; //self.TabBar is IBOutlet of your TabBar
                tabFrame3.size.height = 60;
                tabFrame3.origin.y = self.view.frame.size.height - 60;
                self.tabBar.frame = tabFrame3;
                
                break;
            case 2436:
                printf("iPhone X");
                
                CGRect tabFrame4 = self.tabBar.frame; //self.TabBar is IBOutlet of your TabBar
                tabFrame4.size.height = 80;
                tabFrame4.origin.y = self.view.frame.size.height - 80;
                self.tabBar.frame = tabFrame4;
                
                break;
            default:
                printf("unknown");
        }
    }
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

-(void)updateBadgeItemInternal{
    NSLog(@"updateInternal: %i",self.lastCountBadge);
    UIButton *buttonItem = (UIButton *)[self.tabBar viewWithTag:102];
    UIView *badge = [buttonItem viewWithTag:666];
    if(self.lastCountBadge!=0){
        [badge setHidden:![buttonItem isEnabled]];
    }
}

-(void)updateBadgeItem{
    UIButton *buttonItem = (UIButton *)[self.tabBar viewWithTag:102];
    UIView *badge = [buttonItem viewWithTag:666];
    self.lastCountBadge = [[Client sharedClient] getMessageNotReadedCount];
    NSLog(@"update Mcount: %i",self.lastCountBadge);
    [badge setHidden:(self.lastCountBadge==0)];
    if(![badge isHidden]){
        [badge setHidden:![buttonItem isEnabled]];
    }
}

-(void)changeTitleController{
    int counter = 0;
    for(unsigned i=0;i<self.chatList.count;i++){
        counter += [self findNotReaded:[self.chatList objectAtIndex:i]];
    }
    if(counter==0){
        //[self setTitle:NSLocalizedString(@"Chat", nil)];
        [[self.tabBarController.tabBar.items objectAtIndex:3] setBadgeValue:nil];
    }else{
        // [self setTitle:[NSString stringWithFormat:@"%@ %i",NSLocalizedString(@"Chat", nil),counter]];
        
        [[self.tabBarController.tabBar.items objectAtIndex:3] setBadgeValue:[NSString stringWithFormat:@"%d",counter]];
    }
    
    [[UIApplication sharedApplication] setApplicationIconBadgeNumber:counter];
    //self.title=@"";
    //[self.delegate updateTitle];
}

-(NSMutableArray *)sortArray:(NSMutableArray *)array{
    NSDateFormatter *formatter = [[NSDateFormatter alloc] init];
    [formatter setDateFormat:@"yyyy-MM-dd HH:mm:ss Z"];
    
    NSArray *arrayResult = [array sortedArrayUsingComparator:^NSComparisonResult(NSDictionary *obj1, NSDictionary *obj2) {
        
        NSString *data1=[NSString stringWithFormat:@"%@",[obj1 objectForKey:@"lastUpdate"]];
        NSString *data2=[NSString stringWithFormat:@"%@",[obj2 objectForKey:@"lastUpdate"]];
        
        NSDate *d1 = [formatter dateFromString:data1];
        NSDate *d2 = [formatter dateFromString:data2];
        return [d2 compare:d1];
    }];
    
    return  [arrayResult mutableCopy];
}

-(int)findNotReaded:(NSDictionary *)dictionary{
    int result=0;
    
    if([dictionary objectForKey:@"list"]){
        NSPredicate *predicate = [NSPredicate predicateWithFormat:@"(readed==%i)",0];
        NSArray *arrayResult = [[dictionary objectForKey:@"list"] filteredArrayUsingPredicate:predicate];
        result = (int)arrayResult.count;
    }else{
        NSString *username = [dictionary objectForKey:@"username"];
        NSPredicate *predicate = [NSPredicate predicateWithFormat:@"(username==%@)",username];
        NSArray *arrayUserResult = [[[Client sharedClient] getMessagesList] filteredArrayUsingPredicate:predicate];
        NSArray *arrayListMessageUser = [[arrayUserResult lastObject] objectForKey:@"list"];
        
        NSPredicate *predicateReaded = [NSPredicate predicateWithFormat:@"(readed==%i)",0];
        NSArray *arrayResultReaded = [arrayListMessageUser filteredArrayUsingPredicate:predicateReaded];
        
        result = (int)arrayResultReaded.count;
        
    }
    return result;
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
