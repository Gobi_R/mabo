//
//  Utilities.h
//  Horizontal Messenger
//
//  Created by vishnuprasathg on 9/1/15.
//  Copyright (c) 2015 DCI. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <UIKit/UIKit.h>
#import "CustomAlertView.h"
#import "MultipleAlertView.h"
#import "CustomCameraAlertView.h"
#import "profilePopUpView.h"
#import "CustomVideoAlertView.h"
#import "FbEmailPopUpView.h"
#import "ForgotPopUpView.h"
#import "AddCommentsView.h"
#import "ChatMediaView.h"

//http://34.230.105.103:8002/%@

@class AppDelegate;

@interface Utilities : NSObject

@property(nonatomic,retain)NSMutableDictionary *currentUserDictionary, *currentChatDictionary;
@property(nonatomic)BOOL isChatEntryViewPresented;

@property(nonatomic,retain) CustomAlertView *customAlertView;
@property(nonatomic,retain) MultipleAlertView *multipleAlertView;
@property(nonatomic,retain) CustomCameraAlertView *customCameraAlertView;
@property(nonatomic,retain) profilePopUpView *profilePopView;
@property(nonatomic,retain) CustomVideoAlertView *customVideoAlertView;
@property(nonatomic,retain) FbEmailPopUpView *fbEmailPopUpView;
@property(nonatomic,retain) ForgotPopUpView *forgotPopUpView;
@property(nonatomic,retain) AddCommentsView *addCommentsView;
@property(nonatomic,retain) ChatMediaView *chatMediaView;

+(void)alertWithMessage:(NSString*)message;

+(id)sharedInstance;
+(void)clearSubViewsForView:(UIView*)baseView;
+(AppDelegate*)appDelegate;

+(void)addNotificationCenterForSelecetor:(SEL)aSelector forKey:(NSString*)key withClass:(id)className;
+(void)postNotificationforKey:(NSString*)key;


+ (BOOL)isValidMail:(NSString*)email;

+(void)showActivityIndicatiorForView:(UIView*)baseView withTitle:(NSString*)title;
+(void)hideActivityIndicatorForView:(UIView*)baseView;

+(NSString*)getDeviceModelName;

+(void)setKeyboardFrame:(CGRect)frame;
+(CGRect)getKeyboardFrame;
-(void)shake:(UIView *)View1 view2:(UIView *)View2 shakeCount:(int)count leftshake:(int)leftshake rightshake:(int)rightshake;
-(void)showCustomAlertView:(UIView *)View1 title:(NSString *)title message:(NSString *)message controller:(UIViewController *)controller reason:(NSString *)reason;
-(void)hideCustomAlertView:(UIView *)View1;
-(void)removeAlert:(UIView *)View1;

-(void)showCustomAlertViewWithMultiple:(UIViewController *)controller title:(NSString *)title message:(NSString *)message actionResponse:(NSString *)actionResponse;

-(void)showCustomCameraAlertView:(UIViewController *)controller;
-(void)showCustomVideoAlertView:(UIViewController *)controller;
-(NSMutableAttributedString*)loadTimeDifferent:(NSDate *)msg_creat_Date;

-(void)showCustomPopUpForProfileScreen:(UIViewController *)controller title:(NSString *)title message:(NSString *)message actionResponse:(NSString *)actionResponse;

-(void)showFbloginEmailAlertView:(UIView *)View1 title:(NSString *)title message:(NSString *)message controller:(UIViewController *)controller;

-(void)showCustomForgotPwdView:(UIViewController *)controller title:(NSString *)title message:(NSString *)message actionResponse:(NSString *)actionResponse;

-(void)showCustomAddCommentView:(UIViewController *)controller;
-(void)showMediaAlertView:(UIViewController *)controller;

-(NSMutableAttributedString*)loadTimeLocalDifferent:(NSDate *)msg_creat_Date;

@end

