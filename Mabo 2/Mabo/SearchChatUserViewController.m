//
//  SearchChatUserViewController.m
//  Mabo
//
//  Created by vishnuprasathg on 11/24/17.
//  Copyright © 2017 DCI. All rights reserved.
//

#import "SearchChatUserViewController.h"
//#import "UIImageView+AFNetworking.h"
#import "Utilities.h"
#import "Client.h"
#import "LoadingView.h"
#import "SharedVariables.h"
#import "UserProfileViewController.h"
#import "UIImageView+WebCache.h"

@interface SearchChatUserViewController ()<UITableViewDelegate,UITableViewDataSource,UITextFieldDelegate>

@property (nonatomic, strong) NSArray *usersList;
//@property (nonatomic, strong) NSArray *visibleUserList;
@property (nonatomic, strong) LoadingView *loading;

@end

@implementation SearchChatUserViewController

{
    NSMutableArray *visibleUserList;
}

- (void)viewDidLoad {
    [super viewDidLoad];
    self.tableViewOUtlet.delegate = self;
    self.tableViewOUtlet.dataSource = self;
    self.searchTxtField.delegate = self;
    
    self.searchBaseViewOUtlet.layer.cornerRadius = self.searchBaseViewOUtlet.frame.size.height / 2;
    self.searchBaseViewOUtlet.layer.masksToBounds = YES;
    
    [self.searchBaseViewOUtlet.layer setBorderColor:[UIColor colorWithRed:122/255.0f green:222/255.0f blue:225/255.0f alpha:1.0f].CGColor];
    [self.searchBaseViewOUtlet.layer setBorderWidth:1.0];
    
    
    self.tableViewOUtlet.hidden = YES;
    self.emptyArrayLblOutlet.hidden = NO;
    
    // Do any additional setup after loading the view.
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - TABLEVIEW DELEGATE METHODS


-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    
        NSLog(@"visibleUserList:%@",visibleUserList);
        return visibleUserList.count;
    
}

-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    
    UITableViewCell *cellReal = [tableView dequeueReusableCellWithIdentifier:@"Cell"];
    
    if (visibleUserList.count > 0) {
        
        NSDictionary* listMembers = [visibleUserList objectAtIndex:indexPath.row];
        
        UIImageView* profileImageView = [cellReal viewWithTag:1];
        UILabel * nameLbl = [cellReal viewWithTag:2];
        UILabel* feetLbl = [cellReal viewWithTag:3];
        UILabel* lastSeen = [cellReal viewWithTag:4];
        
        
        
        NSString* name = [listMembers objectForKey:@"extended_name"];
        NSString* picture = [listMembers objectForKey:@"picture_path"];
        NSString* lastseen = [listMembers objectForKey:@"last_seen"];
//        NSString* dist = [NSString stringWithFormat:@"%@",[listMembers objectForKey:@"distance"]];
//        NSArray * arr = [dist componentsSeparatedByString:@"."];
//        NSString* distance = arr.firstObject;
//                NSString* lat = [[listMembers objectForKey:@"user"] objectForKey:@"lat"];
//                NSString* lon = [[listMembers objectForKey:@"user"] objectForKey:@"lon"];
        
        if (name != nil)
        {
            nameLbl.text = [NSString stringWithFormat:@"%@",name];;
        }
        if (picture != nil)
        {
            
            NSString * urlstring = [NSString stringWithFormat:@"http://34.230.105.103:8002/%@", picture];
            NSURL *imageURL = [NSURL URLWithString:urlstring];
            //            NSData *imageData = [NSData dataWithContentsOfURL:imageURL];
            //            UIImage *image = [UIImage imageWithData:imageData];
            //            profileImageView.image = image;
            [profileImageView setImageWithURL:imageURL placeholderImage:[UIImage imageNamed:@"avatar3.png"]];
            
            profileImageView.layer.cornerRadius = profileImageView.frame.size.height / 2;
            profileImageView.layer.masksToBounds = YES;
            
            
        }
        else
        {
            
            profileImageView.image = [UIImage imageNamed:@"avatar3.png"];
        }
        
        NSString* lat = [listMembers objectForKey:@"lat"];
        NSString* lng = [listMembers objectForKey:@"lon"];
        
        CLLocationCoordinate2D  ctrpoint;
        ctrpoint.latitude = [lat doubleValue];
        ctrpoint.longitude = [lng doubleValue];
        
        NSString* currentLat = [NSString stringWithFormat:@"%f",[SharedVariables sharedInstance].myClLocation.coordinate.latitude];
        NSString* currentLng = [NSString stringWithFormat:@"%f",[SharedVariables sharedInstance].myClLocation.coordinate.longitude];
        
        CLLocation *loc1 = [[CLLocation alloc]initWithLatitude:[currentLat doubleValue] longitude:[currentLng doubleValue]];
        
        CLLocation *loc2 = [[CLLocation alloc]initWithLatitude:ctrpoint.latitude longitude:ctrpoint.longitude];
        
        CLLocationDistance distance = [loc1 distanceFromLocation:loc2];
        
        NSArray * arr = [[NSString stringWithFormat:@"%f",distance] componentsSeparatedByString:@"."];
        NSString* dist = [NSString stringWithFormat:@"%@",arr.firstObject];
        
        
        feetLbl.text = [NSString stringWithFormat:@"%@ feet",dist];
        
//        if (distance != nil)
//        {
//            feetLbl.text = [NSString stringWithFormat:@"%@ feet",distance];
//        }
        if (lastseen != nil)
        {
            NSString * timeStampString = [NSString stringWithFormat:@"%@",lastseen];
            
            if (![timeStampString isEqualToString:@"<null>"]) {
                
                NSTimeInterval _interval=[timeStampString doubleValue];
                NSDate *date = [NSDate dateWithTimeIntervalSince1970:_interval];
                NSLog(@"%@", date);
                
                
                NSDateFormatter *formatter = [[NSDateFormatter alloc] init];
                [formatter setDateFormat:@"dd MMMM yyyy"];
                NSString *result = [formatter stringFromDate:date];
                
                NSMutableAttributedString*tmeString = [[Utilities sharedInstance]loadTimeDifferent:date];
                
                lastSeen.attributedText = tmeString;
            }
            else
            {
                lastSeen.text = [NSString stringWithFormat:@"Not Available"];
            }
            
            
        }
        else
        {
            lastSeen.text = [NSString stringWithFormat:@"Last seen: "];
        }
        
    }
    
    return cellReal;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    
    UITableViewCell *cellReal = [tableView dequeueReusableCellWithIdentifier:@"Cell"];
    
    return cellReal.frame.size.height;
}


-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    
    NSDictionary*dictionaryPostSelected = visibleUserList[indexPath.row];
    
    NSLog(@"dict:%@",dictionaryPostSelected);
    
    UserProfileViewController * userProfileVC = [self.storyboard instantiateViewControllerWithIdentifier:@"UserProfileViewController"];
    userProfileVC.userDict = dictionaryPostSelected;
    //[self.navigationController pushViewController:userProfileVC animated:YES];
    [self.navigationController presentViewController:userProfileVC animated:YES completion:nil];
    
}

- (void)textFieldDidBeginEditing:(UITextField *)textField {
    
    
}
- (void)textFieldDidEndEditing:(UITextField *)textField {
    
}

-(BOOL) textFieldShouldReturn: (UITextField *) textField
{
    [textField resignFirstResponder];
    [self search:textField.text];
    return YES;
}

- (BOOL)textField:(UITextField *)textField shouldChangeCharactersInRange:(NSRange)range replacementString:(NSString *)string{
    NSLog(@"textField:shouldChangeCharactersInRange:replacementString:");
    [self search:[NSString stringWithFormat:@"%@%@",textField.text,string]];
    return YES;
}
/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

- (IBAction)backBtnAction:(id)sender {
    
    [self.navigationController popViewControllerAnimated:YES];
}
- (IBAction)searchBtnAction:(id)sender {
    
    [self search:self.searchTxtField.text];
}

-(void)search:(NSString *)searchText
{
//    self.loading = [[LoadingView alloc] initWithFrame:[UIScreen mainScreen].bounds];
//    [self.view addSubview:self.loading];
//    [self.loading animate:YES];
    
    [[Client sharedClient] getChatSearchUser:[Client sharedClient].user.email searchText:searchText completition:^(BOOL success, NSError *error) {
        
       // [self.loading animate:NO];
        if (success) {
            NSLog(@"%@",[SharedVariables sharedInstance].chatSearchDict);
            
            NSDictionary*dictionaryUsers = [[SharedVariables sharedInstance].chatSearchDict objectForKey:@"data"];
            
           // [self.visibleUserList.]
            [visibleUserList removeAllObjects];
            
            visibleUserList=[[dictionaryUsers objectForKey:@"users"] mutableCopy];
            NSLog(@"WallMessages:%@",visibleUserList);
            
            if (visibleUserList.count > 0) {
                
               [self.tableViewOUtlet reloadData];
                self.tableViewOUtlet.hidden = NO;
                self.emptyArrayLblOutlet.hidden = YES;
                
            }
            
        }
        else
        {
           // [self.loading animate:NO];
        }
        
    }];
}

@end
