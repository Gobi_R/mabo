//
//  UtilityGraphic.h
//  Mabo
//
//  Created by Steewe MacBook Pro on 26/03/15.
//  Copyright (c) 2015 Steewe. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <Photos/Photos.h>

enum {
    Upper  = 0,
    Downer = 1 << 0,
   };typedef NSUInteger PositionLine;

@interface UtilityGraphic : NSObject

//DRAW
+(void)drawLineOn:(UIView *)view withColor:(UIColor *)color withWidth:(float)width withPosition:(PositionLine)position;
+(void)drawLineOn:(UIView *)view withColor:(UIColor *)color withWidth:(float)width atPosition:(CGPoint)point;
+(void)drawShadowOn:(UIView *)view withPosition:(PositionLine)position;
+(void)drawInsideShadowOn:(UIView *)view withPosition:(PositionLine)position;
+(void)drawShadowRegular:(UIView *)view;

//POSITION
+(CGPoint)getDownerLeftOf:(UIView *)view;
+(void)slideYView:(UIView *)view inY:(float)newY;

//CONVERT
+(UIImage *)imageFromLayer:(CALayer *)layer;
+(UIImage *)rotateUIImage:(UIImage*)sourceImage clockwise:(BOOL)clockwise;
+(UIImage *)imageWithImage:(UIImage *)image scaledToSize:(CGSize)newSize;

//FILTER
+(NSArray *)filterWithKey:(NSString *)key withValue:(id)value inArray:(NSArray *)array withType:(int)classType;

+(NSString *)URLEncodedString:(NSString *)string;
@end
