//
//  HKAnnotation.h
//  honk
//
//  Created by Steewe MacBook Pro on 26/01/15.
//  Copyright (c) 2015 Steewe. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <MapKit/MapKit.h>

@interface Annotation : NSObject <MKAnnotation> {
    
@private
    CLLocationCoordinate2D _coordinate;
    NSString *_title;
    NSString *_subtitle;
    NSString *_picture;
    NSString *_idMessage;
    NSString *_isEvent;
    NSString *_email;
}

@property (nonatomic, readonly) CLLocationCoordinate2D coordinate;
@property (nonatomic, readonly, copy) NSString *title;
@property (nonatomic, readonly, copy) NSString *subtitle;
@property (nonatomic, readonly, copy) NSString *picture;
@property (nonatomic, readonly, copy) NSString *idMessage;
@property (nonatomic, readonly, copy) NSString *isEvent;
@property (nonatomic, readonly, copy) NSString *email;
@property int index;
@property BOOL isScrumble;
@property (nonatomic, readonly, copy) NSDictionary *userDictionary;

- (id)initWithDictionary:(NSDictionary *)dictionary andCoordinate:(CLLocationCoordinate2D)coordinate andUserDic:(NSDictionary *)userDictionary;

@end
