//
//  CallLogsViewController.h
//  Mabo
//
//  Created by Aravind Kumar on 05/01/18.
//  Copyright © 2018 DCI. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface CallLogsViewController : UIViewController
@property (weak, nonatomic) IBOutlet UITableView *tableViewOutlet;

- (IBAction)backBtnAction:(id)sender;
@end
