//
//  cloudinaryHelper.h
//  Graffiter
//
//  Created by Gowthamraj K on 02/11/16.
//  Copyright © 2016 Mja. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "Cloudinary.h"
#import <UIKit/UIKit.h>


typedef void(^cloudinaryBlock)(NSDictionary *ImageString);
typedef void(^cloudinaryBlockVideo)(NSDictionary *VideoString);
@interface cloudinaryHelper : NSObject <CLUploaderDelegate>
{
    CLCloudinary *cloudinary;
    NSString * cloudImageNameString;
    NSString * cloudImagePNGString;
    NSString * cloudImageString;
}
+(id)sharedInstance;
@property (nonatomic,copy)cloudinaryBlock imageBlock ;
@property (nonatomic,copy)cloudinaryBlockVideo VideoBlock ;

-(void)cloudinary:(cloudinaryBlock)cloud fromImage:(UIImage*)parentImage fromName:(NSString*)parentName;
-(void)cloudinaryVideo:(cloudinaryBlockVideo)cloud fromVideo:(NSString*)parentVideo fromName:(NSString*)parentName;
-(NSString*)cloudinaryURL: (NSString *)Cloudniaryurl;

-(NSString*)cloudinaryURLHomefeed: (NSString *)Cloudniaryurl ;

-(NSString*)cloudinaryArtWork:(NSString *)Cloudniaryurl fromOverlay:(NSString*)Overlay fromEffect:(NSString*)Effect;

-(NSString*)cloudinaryVideoURL: (NSString *)Cloudniaryvideourl;
-(NSString*)cloudinaryVideoThumpImage: (NSString *)CloudniaryvideoThump;



@end
