//
//  cloudinaryHelper.m
//  Graffiter
//
//  Created by Gowthamraj K on 02/11/16.
//  Copyright © 2016 Mja. All rights reserved.
//

#import "cloudinaryHelper.h"
//#import "SVProgressHUD.h
#import "ChatViewController.h"
#import "SharedVariables.h"

@implementation cloudinaryHelper

+(id)sharedInstance
{
    static cloudinaryHelper *cloudinaryclass ;
    static dispatch_once_t dispatchOnce ;
    
    dispatch_once(&dispatchOnce, ^{
        cloudinaryclass = [[cloudinaryHelper alloc]init];
    });
    return cloudinaryclass;
    
}

-(void)instanceForCloud
{

//    cloudinary = [[CLCloudinary alloc] initWithUrl: @"CLOUDINARY_URL=cloudinary://416525585885277:NXpi5_IxBhVK0oEuJcVZrhFBG3A@divjnpbqd"];
//
//    [cloudinary.config setValue:@"divjnpbqd" forKey:@"cloud_name"];
//    [cloudinary.config setValue:@"416525585885277" forKey:@"api_key"];
//    [cloudinary.config setValue:@"NXpi5_IxBhVK0oEuJcVZrhFBG3A" forKey:@"api_secret"];
    
    
    cloudinary = [[CLCloudinary alloc] initWithUrl: @"CLOUDINARY_URL=cloudinary://936184482786724:AKZD8ZHAmI1Bic76JWru-wwMJ_4@mabo-app"];
    
    [cloudinary.config setValue:@"mabo-app" forKey:@"cloud_name"];
    [cloudinary.config setValue:@"936184482786724" forKey:@"api_key"];
    [cloudinary.config setValue:@"AKZD8ZHAmI1Bic76JWru-wwMJ_4" forKey:@"api_secret"];

    }

-(void)cloudinary:(cloudinaryBlock)cloud fromImage:(UIImage*)parentImage fromName:(NSString*)parentName
{

    self.imageBlock= cloud;
 
    [self instanceForCloud];
   
    CLUploader* uploader = [[CLUploader alloc] init:cloudinary delegate:self];
    
    //  NSString *imageFilePath = [[NSBundle mainBundle] pathForResource:@"logo" ofType:@"jpg"];
    
    NSData *imagedata = UIImageJPEGRepresentation(parentImage, 0.5);
    
    [uploader upload:imagedata options:@{@"public_id": parentName}];
    
//    CLUploader* uploader = [[CLUploader alloc] init:cloudinary delegate:self];
    
//    [uploader upload:[self file] options:@{@"resource_type": @"raw"}];

}


-(void)cloudinaryVideo:(cloudinaryBlockVideo)cloud fromVideo:(NSString*)parentVideo fromName:(NSString*)parentName

{
    self.VideoBlock= cloud;
    
   [self instanceForCloud];
    
    
    NSData *data=[NSData dataWithContentsOfURL:[NSURL URLWithString:parentVideo]];
    
        CLUploader* uploader = [[CLUploader alloc] init:cloudinary delegate:self];
    [uploader upload:data options:@{@"resource_type": @"video",@"public_id": parentName}];
    
}

-(NSString*)cloudinaryURLHomefeed: (NSString *)Cloudniaryurl
{
    cloudinary = [[CLCloudinary alloc] initWithUrl: @"cloudinary://717536197283992:baKhJbjyOLdtDt49v-VU7h2Fu1c@graffitier"];
    
    [cloudinary.config setValue:@"graffitier" forKey:@"cloud_name"];
    [cloudinary.config setValue:@"717536197283992" forKey:@"api_key"];
    [cloudinary.config setValue:@"baKhJbjyOLdtDt49v-VU7h2Fu1c" forKey:@"api_secret"];
    
    //    CGRect screenRect = [[UIScreen mainScreen] bounds];
    //    CGFloat screenWidth = screenRect.size.width;
    //    CGFloat screenHeight = screenRect.size.height;
    
    CLTransformation *transformation = [CLTransformation transformation];
    //    [transformation setWidthWithInt: 600];
    [transformation setHeightWithInt: 300];
    ////    [transformation setFetchFormat:@"JPG"];
    //    [transformation setCrop: @"fit"];
    
    NSString *url = [cloudinary url:Cloudniaryurl options:@{@"transformation": transformation}];
    
    return url;
}


-(NSString*)cloudinaryURL: (NSString *)Cloudniaryurl 
{
    [self instanceForCloud];
    
    CLTransformation *transformation = [CLTransformation transformation];
   [transformation setWidthWithInt: [[UIScreen mainScreen] bounds].size.width];
    [transformation setHeightWithInt:[[UIScreen mainScreen] bounds].size.height - 100];
   
////    [transformation setFetchFormat:@"JPG"];
////    [transformation setCrop: @"fit"];

    //  http://res.cloudinary.com/graffitier/image/upload/s--SFrv-kiQ--/e_style_transfer,l_sailing_angel/zpdx7e1ebwrfiameb0gz.jpg

    NSString *url = [cloudinary url:Cloudniaryurl options:@{}];

    return url;
}



-(NSString*)cloudinaryArtWork:(NSString *)Cloudniaryurl fromOverlay:(NSString*)Overlay fromEffect:(NSString*)Effect
{
   // [SVProgressHUD showWithStatus:@"Genearting your artwork"];

    [self instanceForCloud];
    
    CLTransformation *transformation = [CLTransformation transformation];
    [transformation setEffect:Effect];
    [transformation setOverlay:Overlay];
    [transformation generate];
    NSString *url =  [cloudinary url:Cloudniaryurl options:@{@"transformation": transformation}];
    return url;
}






-(NSString*)cloudinaryVideoURL: (NSString *)Cloudniaryvideourl
{
    [self instanceForCloud];
    
    //    CGRect screenRect = [[UIScreen mainScreen] bounds];
    //    CGFloat screenWidth = screenRect.size.width;
    
    CLTransformation *transformation = [CLTransformation transformation];
    
    [transformation setWidthWithInt: 312];
    [transformation setHeightWithInt: 276];

    //    [transformation setFetchFormat:@"JPG"];
    // [transformation setCrop: @"fill"];
    NSString *videoString = [Cloudniaryvideourl componentsSeparatedByString:@"."].firstObject;
    // mov
    
    NSString *url = [cloudinary url:videoString options:@{@"resource_type": @"video",@"format":@"mp4"}];
    
    //    NSString *url = [cloudinary url:Cloudniaryvideourl options:@{@"resource_type": @"video",@"crop":@"fit",@"transformation": transformation}];
    
    return url;
}



-(NSString*)cloudinaryVideoThumpImage: (NSString *)CloudniaryvideoThump
{
     [self instanceForCloud];
    
//    CGRect screenRect = [[UIScreen mainScreen] bounds];
//    CGFloat screenWidth = screenRect.size.width;
    
    CLTransformation *transformation = [CLTransformation transformation];
    [transformation setWidthWithInt: 500];
    [transformation setHeightWithInt: 500];
    //    [transformation setFetchFormat:@"JPG"];
    [transformation setCrop: @"fill"];
    
    NSString *videoString = [CloudniaryvideoThump componentsSeparatedByString:@"."].firstObject;
    
    NSString *url = [cloudinary url:videoString options:@{@"resource_type":@"video",@"format":@"jpg",@"transformation": transformation,@"crop":@"imagga_scale"}];
    
    return url;
}

#pragma mark cloudinary delegate

- (void) uploaderSuccess:(NSDictionary*)result context:(id)context {
    NSString* publicId = [result valueForKey:@"public_id"];
    NSLog(@"Upload success. Public ID=%@, Full result=%@", publicId, result);
    
    cloudImageNameString = result[@"public_id"];
    cloudImagePNGString = result[@"format"];
    cloudImageString = [NSString stringWithFormat:@"%@.%@",cloudImageNameString,cloudImagePNGString];
    
    if(_imageBlock)
    {
    self.imageBlock(result);
    }else
    {
        self.VideoBlock(result);
    }
    
}

- (void) uploaderError:(NSString*)result code:(int) code context:(id)context {
    NSLog(@"Upload error: %@, %d", result, code);
    
    if([[SharedVariables sharedInstance].currentController isKindOfClass:[ChatViewController class]]){
        [(ChatViewController *)[SharedVariables sharedInstance].currentController cloudUploadFailuer:[NSString stringWithFormat:@"%@",result]];
        
    }
}

- (void) uploaderProgress:(NSInteger)bytesWritten totalBytesWritten:(NSInteger)totalBytesWritten totalBytesExpectedToWrite:(NSInteger)totalBytesExpectedToWrite context:(id)context {
    NSLog(@"Upload progress: %ld/%ld (+%ld)", (long)totalBytesWritten, (long)totalBytesExpectedToWrite, (long)bytesWritten);
}


@end
