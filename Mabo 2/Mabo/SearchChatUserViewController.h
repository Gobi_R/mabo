//
//  SearchChatUserViewController.h
//  Mabo
//
//  Created by vishnuprasathg on 11/24/17.
//  Copyright © 2017 DCI. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface SearchChatUserViewController : UIViewController
- (IBAction)backBtnAction:(id)sender;
@property (weak, nonatomic) IBOutlet UIButton *seachBtnOutlet;
@property (weak, nonatomic) IBOutlet UITextField *searchTxtField;
@property (weak, nonatomic) IBOutlet UIView *searchBaseViewOUtlet;
@property (weak, nonatomic) IBOutlet UITableView *tableViewOUtlet;
@property (weak, nonatomic) IBOutlet UILabel *emptyArrayLblOutlet;
- (IBAction)searchBtnAction:(id)sender;

@end
