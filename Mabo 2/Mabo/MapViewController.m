//
//  MapViewController.m
//  Mabo
//
//  Created by vishnuprasathg on 10/30/17.
//  Copyright © 2017 DCI. All rights reserved.
//

#import "MapViewController.h"
#import <MapKit/MapKit.h>
#import "Annotation.h"
#import "AnnotationView.h"
#import "AnnotationClusterView.h"
#import <CoreLocation/CoreLocation.h>
#import "SharedVariables.h"
#import "LoadingView.h"

@interface MapViewController ()<CLLocationManagerDelegate,MKMapViewDelegate>

@property (nonatomic, strong) MKMapView *map;
@property (nonatomic, strong) LoadingView *loading;

@property BOOL isMapView;
@property CLLocationCoordinate2D cordinateViewMap;
@property (nonatomic, strong) CLLocationManager *locationManager;


@end

@implementation MapViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    
    self.map = [[MKMapView alloc] initWithFrame:CGRectMake(0, 0, [UIScreen mainScreen].bounds.size.width, self.view.frame.size.height)];
    [self.map setDelegate:self];
    [self.map setMapType:MKMapTypeStandard];
    [self.map setShowsUserLocation:YES];
    
    self.locationManager = [[CLLocationManager alloc] init];
    
    [[self locationManager] setDelegate:self];
    [[self locationManager] setDesiredAccuracy:kCLLocationAccuracyBest];
    [[self locationManager] startUpdatingLocation];
    
    if ([self.viewIdendity isEqualToString:@"myPostLocation"]) {
        
        [self viewMark];
    }
    else if ([self.viewIdendity isEqualToString:@"userProfile"]) {
        
        [self viewMark];
    }
    else if ([self.viewIdendity isEqualToString:@"creatPost"]) {
        
        [self viewMark];
        
        UITapGestureRecognizer *tapGesture = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(handleTap:)];
        tapGesture.delegate = self;
        tapGesture.numberOfTapsRequired = 1;
        [self.map addGestureRecognizer:tapGesture];
    }
    else if ([self.viewIdendity isEqualToString:@"userProfile_Scrumble"]) {
        
        
        [self viewMarkScrumble];
    }
    else
    {
    UITapGestureRecognizer *tapGesture = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(handleTap:)];
    tapGesture.delegate = self;
    tapGesture.numberOfTapsRequired = 1;
    [self.map addGestureRecognizer:tapGesture];
    }
    
    [self.mapBaseViewOutlet addSubview:self.map];
    
    
}

-(void)viewWillAppear:(BOOL)animated
{
//    CLLocationCoordinate2D myCoordinate;
//    myCoordinate.latitude = *([SharedVariables sharedInstance].lat);
//    myCoordinate.longitude = *([SharedVariables sharedInstance].lon);
//    
//    MKCoordinateRegion viewRegion = MKCoordinateRegionMakeWithDistance
//    (myCoordinate, 300, 300);
//    [self.map setRegion:viewRegion animated:YES];
//    
//    [self.map setCenterCoordinate:myCoordinate animated:YES];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (MKOverlayRenderer *)mapView:(MKMapView *)map viewForOverlay:(id <MKOverlay>)overlay
{
    MKCircleRenderer *circleView = [[MKCircleRenderer alloc] initWithOverlay:overlay];
    circleView.strokeColor = [UIColor colorWithRed:122/255.0f green:222/255.0f blue:225/255.0f alpha:0.2f];
    circleView.fillColor = [[UIColor colorWithRed:122/255.0f green:222/255.0f blue:225/255.0f alpha:0.2f] colorWithAlphaComponent:0.2];
    return circleView;
}

-(void)handleTap: (UITapGestureRecognizer *) recognizer {
    
    self.loading = [[LoadingView alloc] initWithFrame:[UIScreen mainScreen].bounds];
    [self.view addSubview:self.loading];
    [self.loading animate:YES];
    
    CGPoint point = [recognizer locationInView:self.map];
    CLLocationCoordinate2D locCoord = [self.map convertPoint:point toCoordinateFromView:self.map];
    MKPointAnnotation *point1 = [[MKPointAnnotation alloc] init];
    point1.coordinate = locCoord;
    point1.title = @"Sample title";
    
    [self.map removeAnnotations:self.map.annotations];
    [self.map addAnnotation:point1];
    //[dropPin release];
    
    [self getAddressFromLatLon:locCoord.latitude withLongitude:locCoord.longitude];
    
    

}
-(void)viewMark
{
    CLLocationCoordinate2D  ctrpoint;
        ctrpoint.latitude = self.latti;
        ctrpoint.longitude = self.longi;
    
    MKCoordinateRegion region = MKCoordinateRegionMakeWithDistance(ctrpoint, 300, 300);
    [self.map setRegion:[self.map regionThatFits:region] animated:YES];
    
    MKPointAnnotation *annotation = [[MKPointAnnotation alloc] init];
    [annotation setCoordinate:ctrpoint];
    [annotation setTitle:@"Title"]; //You can set the subtitle too
    [self.map removeAnnotations:self.map.annotations];
    [self.map addAnnotation:annotation];
    
}
-(void)viewMarkScrumble
{
    
    CLLocationCoordinate2D myCoordinate;
    myCoordinate.latitude=self.latti;
    myCoordinate.longitude=self.longi;
    
    MKCoordinateRegion region = MKCoordinateRegionMakeWithDistance(myCoordinate, 300, 300);
    [self.map setRegion:[self.map regionThatFits:region] animated:YES];
    
    MKCircle *circle = [MKCircle circleWithCenterCoordinate:myCoordinate radius:500];
    [self.map addOverlay:circle];
    
}

-(void) locationManager:(CLLocationManager *)manager didUpdateLocations:(NSArray *)locations {
    CLLocation *location = locations.lastObject;
    
    //[self getAddressFromLatLon:location.coordinate.latitude withLongitude:location.coordinate.longitude];
}

- (void)mapView:(MKMapView *)mapView didUpdateUserLocation:(MKUserLocation *)userLocation
{
    
}
- (MKAnnotationView *)mapView:(MKMapView *)mapView viewForAnnotation:(id <MKAnnotation>)annotation
{
    if ([annotation isKindOfClass:[MKUserLocation class]])
        return nil;
    
    MKAnnotationView *annotationView = [[MKPinAnnotationView alloc] initWithAnnotation:annotation reuseIdentifier:@"loc"];
    annotationView.canShowCallout = YES;
    annotationView.rightCalloutAccessoryView = [UIButton buttonWithType:UIButtonTypeDetailDisclosure];
    
    return annotationView;
}

- (void)mapView:(MKMapView *)mapView annotationView:(MKAnnotationView *)view calloutAccessoryControlTapped:(UIControl *)control
{
    [self performSegueWithIdentifier:@"DetailsIphone" sender:view];
}

- (void)mapView:(MKMapView *)mapView didSelectAnnotationView:(MKAnnotationView *)view
{
    [mapView deselectAnnotation:view.annotation animated:YES];
    
}

-(void)getAddressFromLatLon:(double)pdblLatitude withLongitude:(double)pdblLongitude
{
    CLGeocoder *ceo = [[CLGeocoder alloc]init];
    CLLocation *loc = [[CLLocation alloc]initWithLatitude:pdblLatitude longitude:pdblLongitude]; //insert your coordinates
    
    [ceo reverseGeocodeLocation:loc
              completionHandler:^(NSArray *placemarks, NSError *error) {
                  CLPlacemark *placemark = [placemarks objectAtIndex:0];
                  if (placemark) {
                      
                      
                      NSLog(@"placemark %@",placemark);
                      //String to hold address
                      NSString *locatedAt = [[placemark.addressDictionary valueForKey:@"FormattedAddressLines"] componentsJoinedByString:@", "];
                      NSLog(@"addressDictionary %@", placemark.addressDictionary);
                      
                      NSLog(@"placemark %@",placemark.region);
                      NSLog(@"placemark %@",placemark.country);  // Give Country Name
                      NSLog(@"placemark %@",placemark.locality); // Extract the city name
                      NSLog(@"location %@",placemark.name);
                      NSLog(@"location %@",placemark.ocean);
                      NSLog(@"location %@",placemark.postalCode);
                      NSLog(@"location %@",placemark.subLocality);
                      
                      NSLog(@"location %@",placemark.location);
                      //Print the location to console
                      NSLog(@"I am currently at %@",locatedAt);
                      
                      [SharedVariables sharedInstance].selectedLat = [NSString stringWithFormat:@"%f",pdblLatitude];
                      [SharedVariables sharedInstance].selectedLon = [NSString stringWithFormat:@"%f",pdblLongitude];
                      
                      [self.loading animate:NO];
                      [self afterGetAddress:locatedAt];
                  }
                  else {
                      NSLog(@"Could not locate");
                  }
              }
     ];
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

-(void)afterGetAddress:(NSString *)address
{
    
    [SharedVariables sharedInstance].creatPostSeletedAddress = address;
    NSLog(@"selectedLat:%@",[SharedVariables sharedInstance].selectedLat);
    NSLog(@"selectedLng:%@",[SharedVariables sharedInstance].selectedLon);
    NSLog(@"selectedAddress:%@",[SharedVariables sharedInstance].creatPostSeletedAddress);
}

- (IBAction)backBtnAction:(id)sender {
    
    if ([self.viewIdendity isEqualToString:@"userProfile"]) {
        
        [self dismissViewControllerAnimated:YES completion:nil];
    }
    else if ([self.viewIdendity isEqualToString:@"userProfile_Scrumble"]) {
        
        
        [self dismissViewControllerAnimated:YES completion:nil];
    }
    else
    {
    [self.navigationController popViewControllerAnimated:YES];
    }
}
@end
