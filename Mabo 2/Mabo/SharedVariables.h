//
//  SharedVariables.h
//  Mobo
//
//  Created by vishnuprasathg on 10/13/17.
//  Copyright © 2017 DCI. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <Photos/Photos.h>

@interface SharedVariables : NSObject

@property (nonatomic, retain) NSString * signInEmailString;
@property (nonatomic, retain) NSString * signInPasswordString;
@property (nonatomic, retain) NSString * signInSaltString;
@property (nonatomic, retain) NSDictionary * signInResponseDict;
@property (nonatomic, retain) NSDictionary * signUpResponseDict;
@property (nonatomic, retain) NSDictionary * myPostResponseDict;
@property (nonatomic, retain) CLLocation * myClLocation;
@property (nonatomic, retain) NSString * creatPostSeletedAddress;
@property (nonatomic, retain) NSString * myCurrentAddress;
@property (nonatomic, retain) NSDictionary * likesDict;
@property (nonatomic, retain) NSDictionary * CommentsDict;
@property (nonatomic, retain) NSDictionary * joinsDict;
@property (nonatomic, retain) NSDictionary * chatSearchDict;

@property (nonatomic, retain) NSString * selectedLat;
@property (nonatomic, retain) NSString * selectedLon;
@property (nonatomic, retain) NSString * fcmToken;

@property (nonatomic, retain) NSString * editPostId;

@property (nonatomic, retain) NSString * callDurationTime;

@property (nonatomic, retain) NSDictionary * filterPreviousDict;
@property (nonatomic, retain) NSDictionary * filterCurrentDict;

@property (nonatomic, retain) NSDictionary * selectedUserDictToChat;

@property(nonatomic,assign) int annotationCount;
@property(nonatomic,assign) int callListPage;
@property(nonatomic,assign) UIViewController *currentController;
@property(nonatomic,assign) UITabBarController *tabBarController;
@property(nonatomic, retain) UIViewController *fbLogInViewController;
@property (nonatomic, retain) NSString * fbLogInUserInputEmail;
@property(nonatomic,assign) BOOL isFbGetEmail;
@property(nonatomic,assign) BOOL isFaceBookLogin;
@property(nonatomic,assign) BOOL isCreated;

@property (nonatomic, retain) NSString * s3BaseUrl;
@property (nonatomic, strong) NSString *userSharedIncoming;

@property (nonatomic) double * lat;
@property (nonatomic) double * lon;

@property (nonatomic, retain) NSDictionary * callLogListResponseDict;
@property (nonatomic, retain) NSDictionary * morePageCallLogListResponseDict;
//@property (nonatomic) int * annotationCount;

+ (instancetype)sharedInstance;

@end
