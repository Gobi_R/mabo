//
//  ChatViewController.m
//  Mobo
//
//  Created by vishnuprasathg on 10/17/17.
//  Copyright © 2017 DCI. All rights reserved.
//

#import "ChatViewController.h"
#import "Client.h"
#import "SinchClient.h"
#import "UtilityGraphic.h"
#import "ChatTableViewCell.h"
#import "VideoCallViewController.h"
#import "SharedVariables.h"
#import "UIImageView+WebCache.h"
#import <MapKit/MapKit.h>
#import "Utilities.h"
#import <MobileCoreServices/MobileCoreServices.h>

#import <AWSCore/AWSCore.h>
#import <AWSCognito/AWSCognito.h>
#import <AWSS3/AWSS3.h>
#import <AWSDynamoDB/AWSDynamoDB.h>
#import <AWSSQS/AWSSQS.h>
#import <AWSSNS/AWSSNS.h>

#import <Sinch/Sinch.h>
#import "SinchClient.h"
#import "AppDelegate.h"
#import "CallViewController.h"
#import "ShowPhotoVideoViewController.h"
#import "MapViewController.h"
#import "ActivityView.h"

#import <AVKit/AVKit.h>
#import <AVFoundation/AVFoundation.h>
#import "Cloudinary.h"
#import "cloudinaryHelper.h"
#import "LoadingView.h"

#define KMS_IN_MLS(value) (value*0.621371192)
#define MTS_IN_FTS(value) (value*3.2808399)

@interface ChatViewController ()<UITableViewDelegate,UITableViewDataSource,UITextViewDelegate,CLLocationManagerDelegate,MKMapViewDelegate,UIPickerViewDelegate,UIPickerViewDataSource,SINCallClientDelegate,UINavigationControllerDelegate>

@property (nonatomic, strong) NSMutableArray *arrayMessage;
@property (nonatomic, strong) UILabel *labelOldMessage;
@property int actualPageNumber;
@property (nonatomic, strong) NSString *userIncoming;
@property (nonatomic, strong) MKMapItem *mapItem;
@property (strong, nonatomic) id<SINClient> client;
@property (nonatomic, strong) ActivityView *activity;
@property (nonatomic, strong) UIImage *imageSavedForShare;
@property (nonatomic, strong) NSDictionary *dictionaryMessageForResend;
@property (nonatomic, strong) NSIndexPath *indexPathForResend;
@property (nonatomic, strong) NSString *savedTempImage;
@property int statePosition;
@property (nonatomic, strong) LoadingView *loading;

@property (copy, nonatomic) AWSS3TransferUtilityUploadCompletionHandlerBlock completionHandler;
@property (copy, nonatomic) AWSS3TransferUtilityProgressBlock progressBlock;
@end

@implementation ChatViewController
{
    CGRect keyboardFrameBeginRect;
    CGSize keyboardSize;
    
    int textviewHeight;
    NSString* selectIdentity;
    NSURL * sendVideoPathUrl;
    NSURL * videoUrl;
    UIImage* orginalImage;
    NSArray * timeArray;
}
#define MARGINCELL 6
#define ROUND_CORNER 10
#define ROUND_CORNER_INTERNAL 8
#define PICTURE_SIZE 40

- (id<SINClient>)client {
    return [[SinchClient sharedClient] client];
    //[(AppDelegate *)[[UIApplication sharedApplication] delegate] client];
}

#pragma mark - SINCallClientDelegate

- (void)client:(id<SINCallClient>)client didReceiveIncomingCall:(id<SINCall>)call {
    
    [self performSegueWithIdentifier:@"callView" sender:call];
}

- (SINLocalNotification *)client:(id<SINClient>)client localNotificationForIncomingCall:(id<SINCall>)call {
    SINLocalNotification *notification = [[SINLocalNotification alloc] init];
    notification.alertAction = @"Answer";
    notification.alertBody = [NSString stringWithFormat:@"Incoming call from %@", [call remoteUserId]];
    return notification;
}

- (void)viewDidLoad {
    [super viewDidLoad];
    
    //[[SinchClient sharedClient] activeClient];
    //self.client = [SinchClient sharedClient].client;
    //self.client.callClient.delegate = self;
    
    [SharedVariables sharedInstance].currentController = self;
    self.statusLblOutlet.hidden = YES;
    
    [self.navigationController setNavigationBarHidden:YES animated:NO];
    // Do any additional setup after loading the view.y
//    self.tableViewBaseViewOutlet.hidden = YES;
//    self.tableViewOutlet.hidden = YES;
    
//    AWSCognitoCredentialsProvider *credentialsProvider = [[AWSCognitoCredentialsProvider alloc] initWithRegionType:AWSRegionUSEast1 identityPoolId:@"us-east-1:2e250934-9456-4acd-a2cd-95f1bca4295d"];
//
//    AWSServiceConfiguration *configuration = [[AWSServiceConfiguration alloc] initWithRegion:AWSRegionUSEast1 credentialsProvider:credentialsProvider];
//
//    [AWSServiceManager defaultServiceManager].defaultServiceConfiguration = configuration;
    
    //id<SINCall> call = [self.client.callClient callUserVideoWithId:self.userIncoming];
    
    self.actualPageNumber = 30;
    self.tableViewOutlet.delegate = self;
    self.tableViewOutlet.dataSource = self;
    
    self.msgTextViewOutlet.delegate = self;
    
    self.msgTextViewOutlet.layer.cornerRadius = self.msgTextViewOutlet.frame.size.height / 2;
    self.msgTextViewOutlet.layer.masksToBounds = YES;
    
    [self.msgTextViewOutlet.layer setBorderColor:[UIColor colorWithRed:122/255.0f green:222/255.0f blue:225/255.0f alpha:1.0f].CGColor];
    [self.msgTextViewOutlet.layer setBorderWidth:1.0];
    
    [SharedVariables sharedInstance].selectedUserDictToChat = self.userDictionaryToChat;
    
    if (![self.userDictionaryToChat objectForKey:@"email"]) {
        
        if (![self.userDictionaryToChat objectForKey:@"username"]) {
            
            self.userIncoming = [[self.userDictionaryToChat objectForKey:@"user"] objectForKey:@"username"];
            
            [[Client sharedClient] updateCacheChatMessageReadedWithMessageId:@{@"sender":[[self.userDictionaryToChat objectForKey:@"user"] objectForKey:@"username"]}];
            [Client sharedClient].incomingUser=nil;
            
            self.arrayMessage = [[Client sharedClient] getMessagesCacheFromUser:[[self.userDictionaryToChat objectForKey:@"user"] objectForKey:@"username"] numberMessage:self.actualPageNumber];
        }
        else
        {
        [[Client sharedClient] updateCacheChatMessageReadedWithMessageId:@{@"sender":[self.userDictionaryToChat objectForKey:@"username"]}];
        [Client sharedClient].incomingUser=nil;
        
        self.arrayMessage = [[Client sharedClient] getMessagesCacheFromUser:[self.userDictionaryToChat objectForKey:@"username"] numberMessage:self.actualPageNumber];
            
            self.userIncoming = [self.userDictionaryToChat objectForKey:@"username"];
        }
    }
    else
    {
    [[Client sharedClient] updateCacheChatMessageReadedWithMessageId:@{@"sender":[self.userDictionaryToChat objectForKey:@"email"]}];
    [Client sharedClient].incomingUser=nil;
        
        self.arrayMessage = [[Client sharedClient] getMessagesCacheFromUser:[self.userDictionaryToChat objectForKey:@"email"] numberMessage:self.actualPageNumber];
        
        self.userIncoming = [self.userDictionaryToChat objectForKey:@"email"];
    }
    
    [SharedVariables sharedInstance].userSharedIncoming = self.userIncoming;
    
    self.titleLblOutlet.text = [NSString stringWithFormat:@"%@",[self.userDictionaryToChat objectForKey:@"extended_name"]];
    
    //RAF TUTTI I MESSAGGI//self.arrayMessage = [[HKClient sharedClient] getMessagesCacheFromUser:[self.userDictionaryToChat objectForKey:@"email"]];

    if(!self.arrayMessage){
        self.arrayMessage = [NSMutableArray arrayWithCapacity:0];
    }
    
    [self controlOldMessage];
    
    for(unsigned i=0;i<[Client sharedClient].sendingPhoto.count;i++){
        NSDictionary *dic =[[Client sharedClient].sendingPhoto objectAtIndex:i];
        if([[dic objectForKey:@"recipient"] isEqualToString:[self.userDictionaryToChat objectForKey:@"email"]]){
            self.actualPageNumber+=1;
            [self.arrayMessage addObject:[[Client sharedClient].sendingPhoto objectAtIndex:i]];
        }
    }
}
-(void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    [[NSNotificationCenter defaultCenter] removeObserver:self name:@"sentMessageStatus" object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(receiveStatusUpload:) name:@"sentMessageStatus" object:nil];
    
    [[NSNotificationCenter defaultCenter] removeObserver:self name:@"cacheUpload" object:nil];
    //[[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(updateBadgeBackButton) name:@"cacheUpload" object:nil];
    
//    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(keyBoardEvent:) name:UIKeyboardWillShowNotification object:nil];
//    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(keyBoardEvent:) name:UIKeyboardWillHideNotification object:nil];
//    [self setNavigationControllerApparence];
//    [self.tableMessage setFrame:CGRectMake(0, 0, [UIScreen mainScreen].bounds.size.width, self.view.frame.size.height-HEIGHT_MESSAGE_BAR)];
//    [self.baseMessageView setFrame:CGRectMake(0, self.view.frame.size.height-HEIGHT_MESSAGE_BAR, [UIScreen mainScreen].bounds.size.width, HEIGHT_MESSAGE_BAR)];
    
//    if ([self.userDictionaryToChat objectForKey:@"email"]) {
//        [[Client sharedClient] updateCacheChatMessageReadedWithMessageId:@{@"sender":[self.userDictionaryToChat objectForKey:@"email"]}];
//    } else {
//        [[Client sharedClient] updateCacheChatMessageReadedWithMessageId:@{@"sender":[self.userDictionaryToChat objectForKey:@"username"]}];
//    }
    
    //[[Client sharedClient] updateCacheChatMessageReadedWithMessageId:@{@"sender":[self.userDictionaryToChat objectForKey:@"email"]}];
    [Client sharedClient].incomingUser=nil;
    
    //RAF TUTTI I MESSAGGI//self.arrayMessage = [[HKClient sharedClient] getMessagesCacheFromUser:[self.userDictionaryToChat objectForKey:@"email"]];
    
   // self.arrayMessage = [[Client sharedClient] getMessagesCacheFromUser:[self.userDictionaryToChat objectForKey:@"email"] numberMessage:self.actualPageNumber];
    
    if(!self.arrayMessage){
        self.arrayMessage = [NSMutableArray arrayWithCapacity:0];
    }
    
    [self controlOldMessage];
    
    for(unsigned i=0;i<[Client sharedClient].sendingPhoto.count;i++){
        NSDictionary *dic =[[Client sharedClient].sendingPhoto objectAtIndex:i];
        if([[dic objectForKey:@"recipient"] isEqualToString:[self.userDictionaryToChat objectForKey:@"email"]]){
            self.actualPageNumber+=1;
            [self.arrayMessage addObject:[[Client sharedClient].sendingPhoto objectAtIndex:i]];
        }
    }
    
    //[self.tableMessage reloadData];
    if([Client sharedClient].incomingOffSet!=0){
        
    }else{
        [self scrollTableToBottomAnimated:NO];
    }
    
//    TabBarController *tabBar=(TabBarController *)[self.navigationController.childViewControllers objectAtIndex:0];
//    [tabBar updateBadgeItem];
//    UIViewController *prevController = [tabBar.childViewControllers lastObject];
//    if([prevController isKindOfClass:[ChatListViewController class]]){
//        [(ChatListViewController *)prevController reloadContent];
//    }
//
//    self.view.keyboardTriggerOffset = self.baseMessageView.bounds.size.height;
//
//    UIView *tempBaseMessageView = self.baseMessageView;
//    UITableView *tempTableMessage = self.tableMessage;
//
//    [self.view addKeyboardPanningWithFrameBasedActionHandler:^(CGRect keyboardFrameInView, BOOL opening, BOOL closing) {
//        CGRect toolBarFrame = tempBaseMessageView.frame;
//        toolBarFrame.origin.y = keyboardFrameInView.origin.y - toolBarFrame.size.height;
//        tempBaseMessageView.frame = toolBarFrame;
//
//        CGRect tableViewFrame = tempTableMessage.frame;
//        tableViewFrame.size.height = toolBarFrame.origin.y;
//        tempTableMessage.frame = tableViewFrame;
//
//    } constraintBasedActionHandler:nil];
    
 }

-(void)receiveConnectionChange:(NSNotificationCenter *)notification{
    //[self.buttonSendOther setEnabled:[[[Client sharedClient] reachAbility] isReachable]];
    
    NSString *stringReal = [self.msgTextViewOutlet.text stringByReplacingOccurrencesOfString:@" " withString:@""];
    int enableMoltiplicator = 1;
    if(stringReal.length==0)enableMoltiplicator*=0;
    if(self.msgTextViewOutlet.text.length==0)enableMoltiplicator*=0;
    
    BOOL enable = [[NSNumber numberWithInt:enableMoltiplicator] boolValue];
    
    if(![[[Client sharedClient] reachAbility] isReachable]){
        enable=NO;
    }
    
//    [self.sendMessageButton setEnabled:enable];
//    [self.sendMessageButton setAlpha:(enable)?1:.5];
}

- (void) viewDidAppear:(BOOL)animated
{
    [super viewDidAppear:animated];
    
//    if (self.tableViewOutlet.contentSize.height > self.tableViewOutlet.frame.size.height)
//    {
//        CGPoint offset = CGPointMake(0, self.tableViewOutlet.contentSize.height - self.tableViewOutlet.frame.size.height / 2 + 37);
//        [self.tableViewOutlet setContentOffset:offset animated:YES];
//    }
}

-(void)viewWillDisappear:(BOOL)animated{
    [super viewWillDisappear:animated];
    //AudioServicesDisposeSystemSoundID(soundClick);
    //[self.view removeKeyboardControl];
    [[NSNotificationCenter defaultCenter] removeObserver:self name:@"cacheUpload" object:nil];
    [[NSNotificationCenter defaultCenter] removeObserver:self name:@"sentMessageStatus" object:nil];
    [[NSNotificationCenter defaultCenter] removeObserver:self name:UIKeyboardWillShowNotification object:nil];
    [[NSNotificationCenter defaultCenter] removeObserver:self name:UIKeyboardWillHideNotification object:nil];

    for(unsigned i=0;i<self.arrayMessage.count;i++){
        NSDictionary *dic =[self.arrayMessage objectAtIndex:i];
        if([[dic objectForKey:@"sent"] intValue]==2){
            [[Client sharedClient] updateMessageStatusFromMessageId:[dic objectForKey:@"messageId"] withRecipient:[dic objectForKey:@"recipient"] withTimeStamp:[dic objectForKey:@"timestamp"] withStatus:0];
        }
    }

    [[NSNotificationCenter defaultCenter] removeObserver:self name:@"connectionChange" object:nil];
}

-(void)controlOldMessage{
    
    int totalCount;
    
    if (![self.userDictionaryToChat objectForKey:@"email"]) {
        
        if (![self.userDictionaryToChat objectForKey:@"username"]) {
            
            totalCount = [[Client sharedClient] getMessageCahceFromUserCount:[[self.userDictionaryToChat objectForKey:@"user"] objectForKey:@"username"]];
        }
        else
        {
            totalCount = [[Client sharedClient] getMessageCahceFromUserCount:[self.userDictionaryToChat objectForKey:@"username"]];
        }

    }
    else
    {
         totalCount = [[Client sharedClient] getMessageCahceFromUserCount:[self.userDictionaryToChat objectForKey:@"email"]];
    }
    
    // totalCount = [[Client sharedClient] getMessageCahceFromUserCount:[self.userDictionaryToChat objectForKey:@"email"]];
    int visibleCount = (int)self.arrayMessage.count;
    if(totalCount>visibleCount){
        
        [self.tableViewOutlet reloadData];
        
//        if(!self.labelOldMessage){
//            self.labelOldMessage = [[UILabel alloc] initWithFrame:CGRectMake(20, -60, [UIScreen mainScreen].bounds.size.width-40, 20)];
//            [self.labelOldMessage setText:NSLocalizedString(@"Load old message",nil)];
//            //[self.labelOldMessage setTextColor:COLOR_MB_GRAY];
//            [self.labelOldMessage setTextAlignment:NSTextAlignmentCenter];
//            //[self.labelOldMessage setFont:[UIFont fontWithName:FONT_MB_REGULAR size:12]];
//            //[self.labelOldMessage setBackgroundColor:COLOR_MB_LIGHTGRAY];
//            [self.labelOldMessage.layer setCornerRadius:10];
//            [self.labelOldMessage setClipsToBounds:YES];
//            //[self.tableMessage addSubview:self.labelOldMessage];
//        }
    }else{
        if(self.labelOldMessage){
            [self.labelOldMessage removeFromSuperview];
            self.labelOldMessage=nil;
        }
    }
    
    
    NSInteger numberOfRows = [self.tableViewOutlet numberOfRowsInSection:0];
    if (numberOfRows) {
        [self.tableViewOutlet scrollToRowAtIndexPath:[NSIndexPath indexPathForRow:numberOfRows-1 inSection:0] atScrollPosition:UITableViewScrollPositionBottom animated:NO];
    }
    
   // [self.tableViewOutlet setContentOffset:CGPointMake(0, CGFLOAT_MAX)];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

//- (void)updateTableContentInset {
//    NSInteger numRows = [self tableViewOutlet:self.tableViewOutlet numberOfRowsInSection:0];
//    CGFloat contentInsetTop = self.tableViewOutlet.bounds.size.height;
//    for (NSInteger i = 0; i < numRows; i++) {
//        contentInsetTop -= [self tableView:self.tableViewOutlet heightForRowAtIndexPath:[NSIndexPath indexPathForItem:i inSection:0]];
//        if (contentInsetTop <= 0) {
//            contentInsetTop = 0;
//            break;
//        }
//    }
//    self.tableViewOutlet.contentInset = UIEdgeInsetsMake(contentInsetTop, 0, 0, 0);
//}


#pragma mark - TABLEVIEW DELEGATE METHODS


-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    
    return self.arrayMessage.count;
    
}

-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    
    ChatTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"Cell"];

    NSDictionary * dictionary = [self.arrayMessage objectAtIndex:indexPath.row];

        if([dictionary objectForKey:@"tempImage"]){
          //  self.savedTempImage=[dictionary objectForKey:@"tempImage"];
           // [self populateLikeImage:dictionary];
            [self populateLikeImage:dictionary cell:cell];
            self.type=@"tempImage";
        }else{
            NSString *input = [dictionary objectForKey:@"text"];
            
            BOOL isImage = NO;
            NSString *pattern = @"\\[!<img>(.*?)\\</img>!]";
            NSRegularExpression *regex = [NSRegularExpression regularExpressionWithPattern:pattern options:NSRegularExpressionCaseInsensitive error:NULL];
            
            
            NSArray *myArray = [regex matchesInString:input options:0 range:NSMakeRange(0, [input length])] ;
            
            NSMutableArray *matches = [NSMutableArray arrayWithCapacity:[myArray count]];
            for (NSTextCheckingResult *match in myArray) {
                NSRange matchRange = [match rangeAtIndex:1];
                [matches addObject:[input substringWithRange:matchRange]];
                isImage=(![[matches lastObject] isEqual:[NSNull null]]);
                if(isImage)break;
            }
            
            BOOL isVideo = NO;
            NSString *patternVideo = @"\\[!<video>(.*?)\\</video>!]";
            NSRegularExpression *regexVid = [NSRegularExpression regularExpressionWithPattern:patternVideo options:NSRegularExpressionCaseInsensitive error:NULL];
            
            
            NSArray *myArrayVid = [regexVid matchesInString:input options:0 range:NSMakeRange(0, [input length])] ;
            
            NSMutableArray *matchesVid = [NSMutableArray arrayWithCapacity:[myArrayVid count]];
            for (NSTextCheckingResult *match in myArrayVid) {
                NSRange matchRange = [match rangeAtIndex:1];
                [matchesVid addObject:[input substringWithRange:matchRange]];
                isVideo=(![[matches lastObject] isEqual:[NSNull null]]);
                if(isVideo)break;
            }
            
            BOOL isLocation = NO;
            NSString *patternLoc = @"\\[!<loc>(.*?)\\</loc>!]";
            NSRegularExpression *regexLoc = [NSRegularExpression regularExpressionWithPattern:patternLoc options:NSRegularExpressionCaseInsensitive error:NULL];
            
            NSArray *myArrayLoc = [regexLoc matchesInString:input options:0 range:NSMakeRange(0, [input length])];
            
            NSMutableArray *matchesLoc = [NSMutableArray arrayWithCapacity:[myArrayLoc count]];
            for (NSTextCheckingResult *match in myArrayLoc) {
                NSRange matchRange = [match rangeAtIndex:1];
                [matchesLoc addObject:[input substringWithRange:matchRange]];
                isLocation=(![[matchesLoc lastObject] isEqual:[NSNull null]]);
                if(isLocation)break;
            }
            
            cell.leftImageViewOutlet.image = [UIImage imageNamed:@"placeHolderImage.png"];
            cell.rightImageViewOutlet.image = [UIImage imageNamed:@"placeHolderImage.png"];
            
            if(isImage){
                
                cell.leftPlayImageViewOutlet.hidden = YES;
                cell.rightPlayImageViewOutlet.hidden = YES;
                
                NSLog(@"image:%@",[matches lastObject]);

                [self populateLikeImageFromServer:dictionary withPath:[matches lastObject] cell:cell];
                self.type=@"image";
            }else if(isVideo){
                
                cell.leftPlayImageViewOutlet.hidden = NO;
                cell.rightPlayImageViewOutlet.hidden = NO;
                
                NSLog(@"video:%@",[matchesVid lastObject]);
                
                [self populateLikeVideoFromServer:dictionary withPath:[matchesVid lastObject] cell:cell];
                self.type=@"video";
            }else if(isLocation){
                
                cell.leftPlayImageViewOutlet.hidden = YES;
                cell.rightPlayImageViewOutlet.hidden = YES;
                
                NSLog(@"location:%@",[matchesLoc lastObject]);
                [self populateLikeLocation:dictionary withLocation:[matchesLoc lastObject] cell:cell];
                self.type=@"location";
            }else{
                
                cell.leftPlayImageViewOutlet.hidden = YES;
                cell.rightPlayImageViewOutlet.hidden = YES;
                
                [self populateLikeMessage:dictionary cell:cell];
                self.type=@"message";
            }
        }
        
    return cell;
}
- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    
    NSDictionary * dictionary = [self.arrayMessage objectAtIndex:indexPath.row];
    
    NSLog(@"dict:%@",dictionary);

        NSString *input = [dictionary objectForKey:@"text"];
        
        BOOL isImage = NO;
        NSString *pattern = @"\\[!<img>(.*?)\\</img>!]";
        NSRegularExpression *regex = [NSRegularExpression regularExpressionWithPattern:pattern options:NSRegularExpressionCaseInsensitive error:NULL];
        
        
        NSArray *myArray = [regex matchesInString:input options:0 range:NSMakeRange(0, [input length])] ;
        
        NSMutableArray *matches = [NSMutableArray arrayWithCapacity:[myArray count]];
        for (NSTextCheckingResult *match in myArray) {
            NSRange matchRange = [match rangeAtIndex:1];
            [matches addObject:[input substringWithRange:matchRange]];
            isImage=(![[matches lastObject] isEqual:[NSNull null]]);
            if(isImage)break;
        }
        
        BOOL isVideo = NO;
        NSString *patternVideo = @"\\[!<video>(.*?)\\</video>!]";
        NSRegularExpression *regexVid = [NSRegularExpression regularExpressionWithPattern:patternVideo options:NSRegularExpressionCaseInsensitive error:NULL];
        
        
        NSArray *myArrayVid = [regexVid matchesInString:input options:0 range:NSMakeRange(0, [input length])] ;
        
        NSMutableArray *matchesVid = [NSMutableArray arrayWithCapacity:[myArrayVid count]];
        for (NSTextCheckingResult *match in myArrayVid) {
            NSRange matchRange = [match rangeAtIndex:1];
            [matchesVid addObject:[input substringWithRange:matchRange]];
            isVideo=(![[matches lastObject] isEqual:[NSNull null]]);
            if(isVideo)break;
        }
        
        BOOL isLocation = NO;
        NSString *patternLoc = @"\\[!<loc>(.*?)\\</loc>!]";
        NSRegularExpression *regexLoc = [NSRegularExpression regularExpressionWithPattern:patternLoc options:NSRegularExpressionCaseInsensitive error:NULL];
        
        NSArray *myArrayLoc = [regexLoc matchesInString:input options:0 range:NSMakeRange(0, [input length])];
        
        NSMutableArray *matchesLoc = [NSMutableArray arrayWithCapacity:[myArrayLoc count]];
        for (NSTextCheckingResult *match in myArrayLoc) {
            NSRange matchRange = [match rangeAtIndex:1];
            [matchesLoc addObject:[input substringWithRange:matchRange]];
            isLocation=(![[matchesLoc lastObject] isEqual:[NSNull null]]);
            if(isLocation)break;
        }
        
        if(isImage){
            return 180;
        }else if(isVideo){
            return 180;
        }else if(isLocation){
            return 180;
        }else{
            return UITableViewAutomaticDimension;
        }
    
//    if (![self.type isEqualToString:@"message"]) {
//        return 180;
//    }
//    else
//    {
   // }
}

-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    
    ChatTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"Cell"];
    
    NSDictionary * dictionary = [self.arrayMessage objectAtIndex:indexPath.row];
    
    if([dictionary objectForKey:@"tempImage"]){
        //  self.savedTempImage=[dictionary objectForKey:@"tempImage"];
        // [self populateLikeImage:dictionary];
        [self populateLikeImage:dictionary cell:cell];
        self.type=@"tempImage";
    }else{
        NSString *input = [dictionary objectForKey:@"text"];
        
        BOOL isImage = NO;
        NSString *pattern = @"\\[!<img>(.*?)\\</img>!]";
        NSRegularExpression *regex = [NSRegularExpression regularExpressionWithPattern:pattern options:NSRegularExpressionCaseInsensitive error:NULL];
        
        
        NSArray *myArray = [regex matchesInString:input options:0 range:NSMakeRange(0, [input length])] ;
        
        NSMutableArray *matches = [NSMutableArray arrayWithCapacity:[myArray count]];
        for (NSTextCheckingResult *match in myArray) {
            NSRange matchRange = [match rangeAtIndex:1];
            [matches addObject:[input substringWithRange:matchRange]];
            isImage=(![[matches lastObject] isEqual:[NSNull null]]);
            if(isImage)break;
        }
        
        BOOL isVideo = NO;
        NSString *patternVideo = @"\\[!<video>(.*?)\\</video>!]";
        NSRegularExpression *regexVid = [NSRegularExpression regularExpressionWithPattern:patternVideo options:NSRegularExpressionCaseInsensitive error:NULL];
        
        
        NSArray *myArrayVid = [regexVid matchesInString:input options:0 range:NSMakeRange(0, [input length])] ;
        
        NSMutableArray *matchesVid = [NSMutableArray arrayWithCapacity:[myArrayVid count]];
        for (NSTextCheckingResult *match in myArrayVid) {
            NSRange matchRange = [match rangeAtIndex:1];
            [matchesVid addObject:[input substringWithRange:matchRange]];
            isVideo=(![[matches lastObject] isEqual:[NSNull null]]);
            if(isVideo)break;
        }
        
        BOOL isLocation = NO;
        NSString *patternLoc = @"\\[!<loc>(.*?)\\</loc>!]";
        NSRegularExpression *regexLoc = [NSRegularExpression regularExpressionWithPattern:patternLoc options:NSRegularExpressionCaseInsensitive error:NULL];
        
        NSArray *myArrayLoc = [regexLoc matchesInString:input options:0 range:NSMakeRange(0, [input length])];
        
        NSMutableArray *matchesLoc = [NSMutableArray arrayWithCapacity:[myArrayLoc count]];
        for (NSTextCheckingResult *match in myArrayLoc) {
            NSRange matchRange = [match rangeAtIndex:1];
            [matchesLoc addObject:[input substringWithRange:matchRange]];
            isLocation=(![[matchesLoc lastObject] isEqual:[NSNull null]]);
            if(isLocation)break;
        }
        
        if(isImage){
            
            NSString * urlstring = [NSString stringWithFormat:@"https://s3.amazonaws.com/staticapi.maboapp.com/%@", [matches lastObject]];
            
                
                ShowPhotoVideoViewController * showPhotoVideoVC = [self.storyboard instantiateViewControllerWithIdentifier:@"ShowPhotoVideoViewController"];
            
                    showPhotoVideoVC.titleString = @"Photo";
                    showPhotoVideoVC.selectedImageString = urlstring;
                    [self.navigationController pushViewController:showPhotoVideoVC animated:YES];

    
        }else if(isVideo){
            
            ShowPhotoVideoViewController * showPhotoVideoVC = [self.storyboard instantiateViewControllerWithIdentifier:@"ShowPhotoVideoViewController"];
                
                showPhotoVideoVC.titleString = @"videoMsg";
                showPhotoVideoVC.selectedVideoString = [NSString stringWithFormat:@"%@", [matchesVid lastObject]];
                [self.navigationController pushViewController:showPhotoVideoVC animated:YES];
        }

        else if(isLocation){
            
            
            NSArray *arrayLocation = [[matchesLoc lastObject] componentsSeparatedByString:@";"];
            float lat = [[arrayLocation objectAtIndex:0] floatValue];
            float lon = [[arrayLocation objectAtIndex:1] floatValue];
            
            MapViewController * mapVC = [self.storyboard instantiateViewControllerWithIdentifier:@"MapViewController"];
            mapVC.viewIdendity = @"myPostLocation";
            mapVC.latti = lat;
            mapVC.longi = lon;
            [self.navigationController pushViewController:mapVC animated:YES];
            
            
        }else{
            
           
        }
    }
//    ChatViewController * chatVC = [self.storyboard instantiateViewControllerWithIdentifier:@"ChatViewController"];
//    [self.navigationController pushViewController:chatVC animated:YES];
    
}

-(void)populateLikeLocation:(NSDictionary *)dictionary withLocation:(NSString *)locationString cell:(ChatTableViewCell *)cell{
    
    cell.textMsgBaseView.hidden = YES;
    cell.mediaMsgBaseView.hidden = NO;
    
    cell.leftImageViewOutlet.hidden = YES;
    cell.rightImageViewOutlet.hidden = YES;
    
    cell.leftMapViewBaseView.hidden = NO;
    cell.rightMapViewBaseView.hidden = NO;

    BOOL isFailed=NO;
    float alpha = 1;
    if([dictionary objectForKey:@"sent"]){
        isFailed = [[dictionary objectForKey:@"sent"] intValue]==0;
        if([[dictionary objectForKey:@"sent"] intValue]==2 || [[dictionary objectForKey:@"sent"] intValue]==0){
            alpha=.5;
        }
    }
    
    NSDate *date = [dictionary objectForKey:@"timestamp"];
    NSCalendar *cal = [NSCalendar currentCalendar];
    NSDateComponents *components = [cal components:(NSEraCalendarUnit|NSYearCalendarUnit|NSMonthCalendarUnit|NSDayCalendarUnit) fromDate:[NSDate date]];
    NSDate *today = [cal dateFromComponents:components];
    components = [cal components:(NSEraCalendarUnit|NSYearCalendarUnit|NSMonthCalendarUnit|NSDayCalendarUnit) fromDate:date];
    NSDate *otherDate = [cal dateFromComponents:components];
    
    NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
    NSString *timeString=@"";
    [dateFormatter setLocale:[NSLocale currentLocale]];
    if([today isEqualToDate:otherDate]){
        [dateFormatter setDateFormat:@"hh:mm a"];
        timeString = [dateFormatter stringFromDate:date];
    }else{
        [dateFormatter setDateFormat:@"dd MMM hh:mm a"];
        timeString = [dateFormatter stringFromDate:date];
    }
    
    UIColor *color;
    NSArray *arrayLocation = [locationString componentsSeparatedByString:@";"];
    float lat = [[arrayLocation objectAtIndex:0] floatValue];
    float lon = [[arrayLocation objectAtIndex:1] floatValue];
    
    // Create an MKMapItem to pass to the Maps app
    CLLocationCoordinate2D coordinate = CLLocationCoordinate2DMake(lat, lon);
    MKPlacemark *placemark = [[MKPlacemark alloc] initWithCoordinate:coordinate
                                                   addressDictionary:nil];
    self.mapItem = [[MKMapItem alloc] initWithPlacemark:placemark];

    
    if([[dictionary objectForKey:@"sender"] isEqualToString:[Client sharedClient].user.email]){
        
        cell.leftMapViewBaseView.hidden = YES;
        cell.leftMediaTimeLblOutlet.hidden = YES;
        
        cell.rightMapViewBaseView.hidden = NO;
        cell.rightMediaTimeLblOutlet.hidden = NO;
        
        cell.rightMediaTimeLblOutlet.text = timeString;
        
        cell.rightMapViewBaseView.layer.cornerRadius = 10;
        cell.rightMapViewBaseView.layer.masksToBounds = YES;
        
        [cell.rightMapViewBaseView.layer setBorderColor:[UIColor colorWithRed:122/255.0f green:222/255.0f blue:225/255.0f alpha:1.0f].CGColor];
        [cell.rightMapViewBaseView.layer setBorderWidth:1.0];
        
        MKMapView *mapView = [[MKMapView alloc] initWithFrame:cell.rightMapViewBaseView.bounds];
        [mapView setTag:666];
        [cell.rightMapViewBaseView addSubview:mapView];
        [mapView setScrollEnabled:NO];
        [mapView setMapType:MKMapTypeStandard];
        [mapView setShowsUserLocation:NO];
        [mapView setCenterCoordinate:CLLocationCoordinate2DMake(lat, lon)];
        MKCoordinateSpan span = MKCoordinateSpanMake(2, 2);
        [mapView setRegion:MKCoordinateRegionMake(mapView.region.center, span) animated:NO];
        
        CLLocationCoordinate2D  ctrpoint;
        ctrpoint.latitude = lat;
        ctrpoint.longitude = lon;
        
        MKCoordinateRegion region = MKCoordinateRegionMakeWithDistance(ctrpoint, 800, 800);
        [mapView setRegion:[mapView regionThatFits:region] animated:YES];
        
        MKPointAnnotation *annotation = [[MKPointAnnotation alloc] init];
        [annotation setCoordinate:ctrpoint];
        [annotation setTitle:@"Title"]; //You can set the subtitle too
        [mapView addAnnotation:annotation];
        
        mapView.userInteractionEnabled = NO;

    }else{
        
        cell.leftMapViewBaseView.hidden = NO;
        cell.leftMediaTimeLblOutlet.hidden = NO;
        
        cell.rightMapViewBaseView.hidden = YES;
        cell.rightMediaTimeLblOutlet.hidden = YES;
        
        cell.leftMediaTimeLblOutlet.text = timeString;
        
        cell.leftMapViewBaseView.layer.cornerRadius = 10;
        cell.leftMapViewBaseView.layer.masksToBounds = YES;
        
        [cell.leftMapViewBaseView.layer setBorderColor:[UIColor colorWithRed:122/255.0f green:222/255.0f blue:225/255.0f alpha:1.0f].CGColor];
        [cell.leftMapViewBaseView.layer setBorderWidth:1.0];

        MKMapView *mapView = [[MKMapView alloc] initWithFrame:cell.leftMapViewBaseView.bounds];
        [mapView setTag:666];
        [cell.leftMapViewBaseView addSubview:mapView];
        [mapView setScrollEnabled:NO];
        [mapView setMapType:MKMapTypeStandard];
        [mapView setShowsUserLocation:NO];
        [mapView setCenterCoordinate:CLLocationCoordinate2DMake(lat, lon)];
        MKCoordinateSpan span = MKCoordinateSpanMake(2, 2);
        [mapView setRegion:MKCoordinateRegionMake(mapView.region.center, span) animated:NO];
        
        CLLocationCoordinate2D  ctrpoint;
        ctrpoint.latitude = lat;
        ctrpoint.longitude = lon;
        
        MKCoordinateRegion region = MKCoordinateRegionMakeWithDistance(ctrpoint, 800, 800);
        [mapView setRegion:[mapView regionThatFits:region] animated:YES];
        
        MKPointAnnotation *annotation = [[MKPointAnnotation alloc] init];
        [annotation setCoordinate:ctrpoint];
        [annotation setTitle:@"Title"]; //You can set the subtitle too
        [mapView addAnnotation:annotation];
        mapView.userInteractionEnabled = NO;
        
    }

}


-(void)populateLikeImageFromServer:(NSDictionary *)dictionary withPath:(NSString *)path cell:(ChatTableViewCell *)cell{
   
    cell.textMsgBaseView.hidden = YES;
    cell.mediaMsgBaseView.hidden = NO;
    
    cell.leftImageViewOutlet.hidden = NO;
    cell.rightImageViewOutlet.hidden = NO;
    
    cell.leftMapViewBaseView.hidden = YES;
    cell.rightMapViewBaseView.hidden = YES;
    
    BOOL isFailed=NO;
    float alpha = 1;
    if([dictionary objectForKey:@"sent"]){
        isFailed = [[dictionary objectForKey:@"sent"] intValue]==0;
        if([[dictionary objectForKey:@"sent"] intValue]==2 || [[dictionary objectForKey:@"sent"] intValue]==0){
            alpha=.5;
        }
    }
    
    NSDate *date = [dictionary objectForKey:@"timestamp"];
    NSCalendar *cal = [NSCalendar currentCalendar];
    NSDateComponents *components = [cal components:(NSEraCalendarUnit|NSYearCalendarUnit|NSMonthCalendarUnit|NSDayCalendarUnit) fromDate:[NSDate date]];
    NSDate *today = [cal dateFromComponents:components];
    components = [cal components:(NSEraCalendarUnit|NSYearCalendarUnit|NSMonthCalendarUnit|NSDayCalendarUnit) fromDate:date];
    NSDate *otherDate = [cal dateFromComponents:components];
    
    NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
    [dateFormatter setLocale:[NSLocale currentLocale]];
    NSString *timeString=@"";
    if([today isEqualToDate:otherDate]){
        [dateFormatter setDateFormat:@"hh:mm a"];
        timeString = [dateFormatter stringFromDate:date];
    }else{
        [dateFormatter setDateFormat:@"dd MMM hh:mm a"];
        timeString = [dateFormatter stringFromDate:date];
    }

    if([[dictionary objectForKey:@"sender"] isEqualToString:[Client sharedClient].user.email]){
        
        cell.leftImageViewOutlet.hidden = YES;
        cell.leftMediaTimeLblOutlet.hidden = YES;
        
        cell.rightImageViewOutlet.hidden = NO;
        cell.rightMediaTimeLblOutlet.hidden = NO;
        
        cell.rightMediaTimeLblOutlet.text = timeString;
        
        cell.rightImageViewOutlet.layer.cornerRadius = 10;
        cell.rightImageViewOutlet.layer.masksToBounds = YES;
        
        [cell.rightImageViewOutlet.layer setBorderColor:[UIColor colorWithRed:122/255.0f green:222/255.0f blue:225/255.0f alpha:1.0f].CGColor];
        [cell.rightImageViewOutlet.layer setBorderWidth:1.0];
        
        
//        dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_BACKGROUND, 0), ^{
//            // retrive image on global queue
//            if (path.length > 0) {
//
//                NSString * urlstring = [NSString stringWithFormat:@"http://34.230.105.103:8002/%@", path];
//                NSURL *imageURL = [NSURL URLWithString:urlstring];
//                UIImage * img = [UIImage imageWithData:[NSData dataWithContentsOfURL:[NSURL URLWithString:urlstring]]];
//
//                dispatch_async(dispatch_get_main_queue(), ^{
//
//                    cell.rightImageViewOutlet.layer.cornerRadius = 10;
//                    cell.rightImageViewOutlet.layer.masksToBounds = YES;
//
//                    [cell.rightImageViewOutlet.layer setBorderColor:[UIColor colorWithRed:122/255.0f green:222/255.0f blue:225/255.0f alpha:1.0f].CGColor];
//                    [cell.rightImageViewOutlet.layer setBorderWidth:1.0];
//
//                    // assign cell image on main thread
//                    //[profileImageView sd_setImageWithURL:imageURL placeholderImage:[UIImage imageNamed:@"avatar2.png"]];
//                    [cell.rightImageViewOutlet setImageWithURL:imageURL placeholderImage:[UIImage imageNamed:@"avatar3.png"]];
//                    //[cell.textPostProfileImageView setImageWithURL:imageURL placeholderImage:[UIImage imageNamed:@"avatar2.png"]];
//                });
//            }
//
//        });
        
        cell.mediaMsgBaseViewHeightConstraint.constant = 180;
        
        CGRect frame = cell.frame;
        frame.size.height = 180;
        cell.frame = frame;
        
        [[Client sharedClient] getPicture:path from:path withCompletition:^(UIImage *image, BOOL isInCache, id sender) {
            //[self.labelText setHidden:YES];
            if(sender==path){
                [self.activity setHidden:YES];
                if(image){
                    self.imageSavedForShare=image;
                    CGSize sizeImage = CGSizeMake((([UIScreen mainScreen].bounds.size.width/4*3)-10-MARGINCELL*2), image.size.height*(([UIScreen mainScreen].bounds.size.width/4*3)-10-MARGINCELL*2)/image.size.width);
                    
                    //                    if([[dictionary objectForKey:@"sender"] isEqualToString:[Client sharedClient].user.email]){
                    //                        [self.pictureProfile setFrame:CGRectMake([UIScreen mainScreen].bounds.size.width-PICTURE_SIZE-MARGINCELL, 2, PICTURE_SIZE, PICTURE_SIZE)];
                    //                        [self.imageViewChat setFrame:CGRectMake([UIScreen mainScreen].bounds.size.width-self.pictureProfile.frame.size.width-MARGINCELL*2.5-sizeImage.width, MARGINCELL, sizeImage.width, sizeImage.height)];
                    //                        [self.labelTime setFrame:CGRectMake(self.imageViewChat.frame.origin.x+self.imageViewChat.frame.size.width-self.labelTime.frame.size.width, self.imageViewChat.frame.size.height+MARGINCELL*2, self.labelTime.frame.size.width, self.labelTime.frame.size.height)];
                    //                    }else{
                    //                       // [self.pictureProfile setFrame:CGRectMake(MARGINCELL, 2, PICTURE_SIZE, PICTURE_SIZE)];
                    //                        //[self.imageViewChat setFrame:CGRectMake(10+PICTURE_SIZE+MARGINCELL/2, MARGINCELL, sizeImage.width, sizeImage.height)];
                    //                       // [self.labelTime setFrame:CGRectMake(self.imageViewChat.frame.origin.x, self.imageViewChat.frame.size.height+MARGINCELL*2, self.labelTime.frame.size.width, self.labelTime.frame.size.height)];
                    //                    }
                    
                    [cell.rightImageViewOutlet setImage:image];
                    
                    //[self setFrame:CGRectMake(self.frame.origin.x, self.frame.origin.y, [UIScreen mainScreen].bounds.size.width, self.imageViewChat.frame.size.height+(MARGINCELL*3)+self.labelTime.frame.size.height)];
                    
                    //                    CGRect rectBaseNew = CGRectMake(cell.leftImageViewOutlet.frame.origin.x-MARGINCELL/2, self.imageViewChat.frame.origin.y-MARGINCELL/2, self.imageViewChat.frame.size.width+MARGINCELL, self.imageViewChat.frame.size.height+MARGINCELL);
                    //                    UIBezierPath *bezierPathNew = [UIBezierPath bezierPathWithRoundedRect:rectBaseNew cornerRadius:ROUND_CORNER];
                    //
                    //                    [self.layerBase setPath:nil];
                    //                    [self.layerBase setPath:bezierPathNew.CGPath];
                    //                    [self.layerBase setFillColor:COLOR_MB_GRAY.CGColor];
                    //                    [self.layerBase setStrokeColor:color.CGColor];
                    //                    [self.layerBase setStrokeEnd:1];
                    
                    //                    [self.buttonResend setFrame:CGRectMake(rectBase.origin.x-self.buttonResend.frame.size.width-2, rectBase.origin.y+(rectBase.size.height-self.buttonResend.frame.size.height)/2, self.buttonResend.frame.size.height, self.buttonResend.frame.size.height)];
                    //                    [self.buttonResend setHidden:!isFailed];
                    //                    [self.labelTime setHidden:isFailed];
                    //                    [self.button setFrame:rectBaseNew];
                    //
                    //                    [self.button setHidden:NO];
                    //                    [self.button setEnabled:YES];
                    
                    if(!isInCache){
                        [self.delegate reloadCell:self];
                        if(self.savedTempImage){
                            [self.delegate destroy:self.savedTempImage];
                            self.savedTempImage=nil;
                        }
                    }
                    
                    if(isFailed && !isInCache){
                        [self.delegate reloadCell:self];
                    }
                }
                
            }
            
        }];
    
    }else{
        
        cell.leftImageViewOutlet.hidden = NO;
        cell.leftMediaTimeLblOutlet.hidden = NO;
        
        cell.rightImageViewOutlet.hidden = YES;
        cell.rightMediaTimeLblOutlet.hidden = YES;
        
        cell.leftMediaTimeLblOutlet.text = timeString;
        
        
        cell.leftImageViewOutlet.layer.cornerRadius = 10;
        cell.leftImageViewOutlet.layer.masksToBounds = YES;
        
        [cell.leftImageViewOutlet.layer setBorderColor:[UIColor colorWithRed:122/255.0f green:222/255.0f blue:225/255.0f alpha:1.0f].CGColor];
        [cell.leftImageViewOutlet.layer setBorderWidth:1.0];
        
//        dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_BACKGROUND, 0), ^{
//            // retrive image on global queue
//            if (path.length > 0) {
//
//                NSString * urlstring = [NSString stringWithFormat:@"http://34.230.105.103:8002/%@", path];
//                NSURL *imageURL = [NSURL URLWithString:urlstring];
//                UIImage * img = [UIImage imageWithData:[NSData dataWithContentsOfURL:[NSURL URLWithString:urlstring]]];
//
//                dispatch_async(dispatch_get_main_queue(), ^{
//
//                    cell.leftImageViewOutlet.layer.cornerRadius = 10;
//                    cell.leftImageViewOutlet.layer.masksToBounds = YES;
//
//                    [cell.leftImageViewOutlet.layer setBorderColor:[UIColor colorWithRed:122/255.0f green:222/255.0f blue:225/255.0f alpha:1.0f].CGColor];
//                    [cell.leftImageViewOutlet.layer setBorderWidth:1.0];
//
//                    // assign cell image on main thread
//                    //[profileImageView sd_setImageWithURL:imageURL placeholderImage:[UIImage imageNamed:@"avatar2.png"]];
//                    [cell.leftImageViewOutlet setImageWithURL:imageURL placeholderImage:[UIImage imageNamed:@"avatar3.png"]];
//                    //[cell.textPostProfileImageView setImageWithURL:imageURL placeholderImage:[UIImage imageNamed:@"avatar2.png"]];
//                });
//            }
//
//        });
        
        
        cell.mediaMsgBaseViewHeightConstraint.constant = 180;
        
        CGRect frame = cell.frame;
        frame.size.height = 180;
        cell.frame = frame;
        
        [[Client sharedClient] getPicture:path from:path withCompletition:^(UIImage *image, BOOL isInCache, id sender) {
            //[self.labelText setHidden:YES];
            if(sender==path){
                [self.activity setHidden:YES];
                if(image){
                    self.imageSavedForShare=image;
                    CGSize sizeImage = CGSizeMake((([UIScreen mainScreen].bounds.size.width/4*3)-10-MARGINCELL*2), image.size.height*(([UIScreen mainScreen].bounds.size.width/4*3)-10-MARGINCELL*2)/image.size.width);
                    
//                    if([[dictionary objectForKey:@"sender"] isEqualToString:[Client sharedClient].user.email]){
//                        [self.pictureProfile setFrame:CGRectMake([UIScreen mainScreen].bounds.size.width-PICTURE_SIZE-MARGINCELL, 2, PICTURE_SIZE, PICTURE_SIZE)];
//                        [self.imageViewChat setFrame:CGRectMake([UIScreen mainScreen].bounds.size.width-self.pictureProfile.frame.size.width-MARGINCELL*2.5-sizeImage.width, MARGINCELL, sizeImage.width, sizeImage.height)];
//                        [self.labelTime setFrame:CGRectMake(self.imageViewChat.frame.origin.x+self.imageViewChat.frame.size.width-self.labelTime.frame.size.width, self.imageViewChat.frame.size.height+MARGINCELL*2, self.labelTime.frame.size.width, self.labelTime.frame.size.height)];
//                    }else{
//                       // [self.pictureProfile setFrame:CGRectMake(MARGINCELL, 2, PICTURE_SIZE, PICTURE_SIZE)];
//                        //[self.imageViewChat setFrame:CGRectMake(10+PICTURE_SIZE+MARGINCELL/2, MARGINCELL, sizeImage.width, sizeImage.height)];
//                       // [self.labelTime setFrame:CGRectMake(self.imageViewChat.frame.origin.x, self.imageViewChat.frame.size.height+MARGINCELL*2, self.labelTime.frame.size.width, self.labelTime.frame.size.height)];
//                    }
                    
                    [cell.leftImageViewOutlet setImage:image];
                    
                    //[self setFrame:CGRectMake(self.frame.origin.x, self.frame.origin.y, [UIScreen mainScreen].bounds.size.width, self.imageViewChat.frame.size.height+(MARGINCELL*3)+self.labelTime.frame.size.height)];
                    
//                    CGRect rectBaseNew = CGRectMake(cell.leftImageViewOutlet.frame.origin.x-MARGINCELL/2, self.imageViewChat.frame.origin.y-MARGINCELL/2, self.imageViewChat.frame.size.width+MARGINCELL, self.imageViewChat.frame.size.height+MARGINCELL);
//                    UIBezierPath *bezierPathNew = [UIBezierPath bezierPathWithRoundedRect:rectBaseNew cornerRadius:ROUND_CORNER];
//
//                    [self.layerBase setPath:nil];
//                    [self.layerBase setPath:bezierPathNew.CGPath];
//                    [self.layerBase setFillColor:COLOR_MB_GRAY.CGColor];
//                    [self.layerBase setStrokeColor:color.CGColor];
//                    [self.layerBase setStrokeEnd:1];
                    
//                    [self.buttonResend setFrame:CGRectMake(rectBase.origin.x-self.buttonResend.frame.size.width-2, rectBase.origin.y+(rectBase.size.height-self.buttonResend.frame.size.height)/2, self.buttonResend.frame.size.height, self.buttonResend.frame.size.height)];
//                    [self.buttonResend setHidden:!isFailed];
//                    [self.labelTime setHidden:isFailed];
//                    [self.button setFrame:rectBaseNew];
//                    
//                    [self.button setHidden:NO];
//                    [self.button setEnabled:YES];
                    
                    if(!isInCache){
                        [self.delegate reloadCell:self];
                        if(self.savedTempImage){
                            [self.delegate destroy:self.savedTempImage];
                            self.savedTempImage=nil;
                        }
                    }
                    
                    if(isFailed && !isInCache){
                        [self.delegate reloadCell:self];
                    }
                }
                
            }
            
        }];

    }

}

-(void)populateLikeVideoFromServer:(NSDictionary *)dictionary withPath:(NSString *)path cell:(ChatTableViewCell *)cell{
    
    cell.textMsgBaseView.hidden = YES;
    cell.mediaMsgBaseView.hidden = NO;
    
    cell.leftImageViewOutlet.hidden = NO;
    cell.rightImageViewOutlet.hidden = NO;
    
    cell.leftMapViewBaseView.hidden = YES;
    cell.rightMapViewBaseView.hidden = YES;
    
    BOOL isFailed=NO;
    float alpha = 1;
    if([dictionary objectForKey:@"sent"]){
        isFailed = [[dictionary objectForKey:@"sent"] intValue]==0;
        if([[dictionary objectForKey:@"sent"] intValue]==2 || [[dictionary objectForKey:@"sent"] intValue]==0){
            alpha=.5;
        }
    }
    
    NSDate *date = [dictionary objectForKey:@"timestamp"];
    NSCalendar *cal = [NSCalendar currentCalendar];
    NSDateComponents *components = [cal components:(NSEraCalendarUnit|NSYearCalendarUnit|NSMonthCalendarUnit|NSDayCalendarUnit) fromDate:[NSDate date]];
    NSDate *today = [cal dateFromComponents:components];
    components = [cal components:(NSEraCalendarUnit|NSYearCalendarUnit|NSMonthCalendarUnit|NSDayCalendarUnit) fromDate:date];
    NSDate *otherDate = [cal dateFromComponents:components];
    
    NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
    [dateFormatter setLocale:[NSLocale currentLocale]];
    NSString *timeString=@"";
    if([today isEqualToDate:otherDate]){
        [dateFormatter setDateFormat:@"hh:mm a"];
        timeString = [dateFormatter stringFromDate:date];
    }else{
        [dateFormatter setDateFormat:@"dd MMM hh:mm a"];
        timeString = [dateFormatter stringFromDate:date];
    }
    
    if([[dictionary objectForKey:@"sender"] isEqualToString:[Client sharedClient].user.email]){
        
        cell.rightPlayImageViewOutlet.hidden = NO;
        cell.leftPlayImageViewOutlet.hidden = YES;
        
        cell.leftImageViewOutlet.hidden = YES;
        cell.leftMediaTimeLblOutlet.hidden = YES;
        
        cell.rightImageViewOutlet.hidden = NO;
        cell.rightMediaTimeLblOutlet.hidden = NO;
        
        cell.rightMediaTimeLblOutlet.text = timeString;
        
//        cell.rightImageViewOutlet.layer.cornerRadius = 10;
//        cell.rightImageViewOutlet.layer.masksToBounds = YES;
            // retrive image on global queue
            if (path.length > 0) {
                
                dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_BACKGROUND, 0), ^{
                    // retrive image on global queue
                    
                    
                    NSString* pathString = [path substringToIndex:[path length] - 3];
                    
                    pathString = [NSString stringWithFormat:@"%@jpg",pathString];
                    
                    
                    cell.rightImageViewOutlet.layer.cornerRadius = 10;
                    cell.rightImageViewOutlet.layer.masksToBounds = YES;
                    
                    NSURL *imageURL = [NSURL URLWithString:pathString];
                    
                    dispatch_async(dispatch_get_main_queue(), ^{
                        
                        // assign cell image on main thread
                        [cell.rightImageViewOutlet sd_setImageWithURL:imageURL placeholderImage:[UIImage imageNamed:@"placeHolderImage.png"]];
                        //[cell.textPostProfileImageView setImageWithURL:imageURL placeholderImage:[UIImage imageNamed:@"avatar2.png"]];
                    });
                });
            }

        cell.mediaMsgBaseViewHeightConstraint.constant = 180;
        
        CGRect frame = cell.frame;
        frame.size.height = 180;
        cell.frame = frame;
        
    }else{
        
        cell.rightPlayImageViewOutlet.hidden = YES;
        cell.leftPlayImageViewOutlet.hidden = NO;
        
        cell.leftImageViewOutlet.hidden = NO;
        cell.leftMediaTimeLblOutlet.hidden = NO;
        
        cell.rightImageViewOutlet.hidden = YES;
        cell.rightMediaTimeLblOutlet.hidden = YES;
        
        cell.leftMediaTimeLblOutlet.text = timeString;
        
        if (path.length > 0) {
            
            dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_BACKGROUND, 0), ^{
                // retrive image on global queue
            
                NSString* pathString = [path substringToIndex:[path length] - 3];
                
                pathString = [NSString stringWithFormat:@"%@jpg",pathString];
                
                
                cell.leftImageViewOutlet.layer.cornerRadius = 10;
                cell.leftImageViewOutlet.layer.masksToBounds = YES;
                
                NSURL *imageURL = [NSURL URLWithString:pathString];
                
                dispatch_async(dispatch_get_main_queue(), ^{
                    
                    // assign cell image on main thread
                    [cell.leftImageViewOutlet sd_setImageWithURL:imageURL placeholderImage:[UIImage imageNamed:@"placeHolderImage.png"]];
                    //[cell.textPostProfileImageView setImageWithURL:imageURL placeholderImage:[UIImage imageNamed:@"avatar2.png"]];
                });
            });
        }
        
        cell.mediaMsgBaseViewHeightConstraint.constant = 180;
        
        CGRect frame = cell.frame;
        frame.size.height = 180;
        cell.frame = frame;
        
    }
    
}

-(void)populateLikeImage:(NSDictionary *)dictionary cell:(ChatTableViewCell *)cell{
   
    //[self.activity show];
    BOOL isFailed=NO;
    float alpha = 1;
    if([dictionary objectForKey:@"sent"]){
        isFailed = [[dictionary objectForKey:@"sent"] intValue]==0;
        if([[dictionary objectForKey:@"sent"] intValue]==2 || [[dictionary objectForKey:@"sent"] intValue]==0){
            alpha=.5;
        }
    }
    
    NSDate *date = [dictionary objectForKey:@"timestamp"];
    NSCalendar *cal = [NSCalendar currentCalendar];
    NSDateComponents *components = [cal components:(NSEraCalendarUnit|NSYearCalendarUnit|NSMonthCalendarUnit|NSDayCalendarUnit) fromDate:[NSDate date]];
    NSDate *today = [cal dateFromComponents:components];
    components = [cal components:(NSEraCalendarUnit|NSYearCalendarUnit|NSMonthCalendarUnit|NSDayCalendarUnit) fromDate:date];
    NSDate *otherDate = [cal dateFromComponents:components];
    
    NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
    [dateFormatter setLocale:[NSLocale currentLocale]];
    NSString *timeString=@"";
    if([today isEqualToDate:otherDate]){
        [dateFormatter setDateFormat:@"hh:mm a"];
        timeString = [dateFormatter stringFromDate:date];
    }else{
        [dateFormatter setDateFormat:@"dd MMM hh:mm a"];
        timeString = [dateFormatter stringFromDate:date];
    }
    
    UIImage *image = [UIImage imageWithContentsOfFile:[dictionary objectForKey:@"tempImage"]];

    
    if([[dictionary objectForKey:@"sender"] isEqualToString:[Client sharedClient].user.email]){
        
        cell.leftImageViewOutlet.hidden = YES;
        cell.leftMediaTimeLblOutlet.hidden = YES;
        
        cell.rightImageViewOutlet.hidden = NO;
        cell.rightMediaTimeLblOutlet.hidden = NO;
        
        cell.rightMediaTimeLblOutlet.text = timeString;
        
        cell.leftImageViewOutlet.image = image;
        
        cell.mediaMsgBaseViewHeightConstraint.constant = 180;
        
        CGRect frame = cell.frame;
        frame.size.height = 180;
        cell.frame = frame;
        
    }else{
        
        cell.leftImageViewOutlet.hidden = NO;
        cell.leftMediaTimeLblOutlet.hidden = NO;
        
        cell.rightImageViewOutlet.hidden = YES;
        cell.rightMediaTimeLblOutlet.hidden = YES;
        
        cell.leftMediaTimeLblOutlet.text = timeString;
        
        cell.leftImageViewOutlet.image = image;
        
//        dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_BACKGROUND, 0), ^{
//            // retrive image on global queue
//
//
//                NSString * urlstring = [NSString stringWithFormat:@"http://34.230.105.103:8002/%@", path];
//                NSURL *imageURL = [NSURL URLWithString:urlstring];
//                UIImage * img = [UIImage imageWithData:[NSData dataWithContentsOfURL:[NSURL URLWithString:urlstring]]];
//
//                dispatch_async(dispatch_get_main_queue(), ^{
//
//                    cell.leftImageViewOutlet.layer.cornerRadius = 10;
//                    cell.leftImageViewOutlet.layer.masksToBounds = YES;
//
//                    // assign cell image on main thread
//                    //[profileImageView sd_setImageWithURL:imageURL placeholderImage:[UIImage imageNamed:@"avatar2.png"]];
//                    [cell.leftImageViewOutlet setImageWithURL:imageURL placeholderImage:[UIImage imageNamed:@"avatar3.png"]];
//                    //[cell.textPostProfileImageView setImageWithURL:imageURL placeholderImage:[UIImage imageNamed:@"avatar2.png"]];
//                });
//            }
//
//        });
        
        
        cell.mediaMsgBaseViewHeightConstraint.constant = 180;
        
        CGRect frame = cell.frame;
        frame.size.height = 180;
        cell.frame = frame;
        
    }
}


-(void)populateLikeMessage:(NSDictionary *)dictionary cell:(ChatTableViewCell *)cell {
    
    cell.textMsgBaseView.hidden = NO;
    cell.mediaMsgBaseView.hidden = YES;
    
    BOOL isFailed=NO;
    float alpha = 1;
    if([dictionary objectForKey:@"sent"]){
        isFailed = [[dictionary objectForKey:@"sent"] intValue]==0;
        if([[dictionary objectForKey:@"sent"] intValue]==2 || [[dictionary objectForKey:@"sent"] intValue]==0){
            alpha=.5;
        }
    }
    NSDate *date = [dictionary objectForKey:@"timestamp"];
    NSCalendar *cal = [NSCalendar currentCalendar];
    NSDateComponents *components = [cal components:(NSEraCalendarUnit|NSYearCalendarUnit|NSMonthCalendarUnit|NSDayCalendarUnit) fromDate:[NSDate date]];
    NSDate *today = [cal dateFromComponents:components];
    components = [cal components:(NSEraCalendarUnit|NSYearCalendarUnit|NSMonthCalendarUnit|NSDayCalendarUnit) fromDate:date];
    NSDate *otherDate = [cal dateFromComponents:components];
    
    NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
    [dateFormatter setLocale:[NSLocale currentLocale]];
    NSString *timeString=@"";
    if([today isEqualToDate:otherDate]){
        [dateFormatter setDateFormat:@"hh:mm a"];
        timeString = [dateFormatter stringFromDate:date];
    }else{
        [dateFormatter setDateFormat:@"dd MMM hh:mm a"];
        timeString = [dateFormatter stringFromDate:date];
    }
    
    
    cell.textMsgTextViewOutlet.layer.cornerRadius = 10;
    cell.textMsgTextViewOutlet.layer.masksToBounds = YES;
    
    [cell.textMsgTextViewOutlet.layer setBorderColor:[UIColor colorWithRed:122/255.0f green:222/255.0f blue:225/255.0f alpha:1.0f].CGColor];
    [cell.textMsgTextViewOutlet.layer setBorderWidth:1.0];
    
    cell.lextTxtMsgTextViewOutlet.layer.cornerRadius = 10;
    cell.lextTxtMsgTextViewOutlet.layer.masksToBounds = YES;
    
    [cell.lextTxtMsgTextViewOutlet.layer setBorderColor:[UIColor colorWithRed:122/255.0f green:222/255.0f blue:225/255.0f alpha:1.0f].CGColor];
    [cell.lextTxtMsgTextViewOutlet.layer setBorderWidth:1.0];
    
    if([[dictionary objectForKey:@"sender"] isEqualToString:[Client sharedClient].user.email]){
        
        cell.lextTxtMsgTextViewOutlet.hidden = YES;
        cell.textMsgTextViewOutlet.hidden = NO;
        
        cell.rightTimeLblOutlet.hidden = NO;
        cell.leftTimeLblOutlet.hidden = YES;
        
        cell.textMsgTextViewOutlet.text = [dictionary objectForKey:@"text"];
        cell.rightTimeLblOutlet.text = timeString;
        
        [cell.textMsgTextViewOutlet setTextAlignment:NSTextAlignmentRight];
        
        
        CGSize sz = [cell.textMsgTextViewOutlet.text sizeWithFont:cell.textMsgTextViewOutlet.font];
        
        
//        [cell.textMsgTextViewOutlet setFrame:CGRectMake([UIScreen mainScreen].bounds.size.width-self.pictureProfile.frame.size.width-6*4, 6, ([UIScreen mainScreen].bounds.size.width/4*3)-10, 0)];
//        [cell.textMsgTextViewOutlet sizeToFit];
//        [cell.textMsgTextViewOutlet setFrame:CGRectMake([UIScreen mainScreen].bounds.size.width-cell.textMsgTextViewOutlet.frame.size.width-10-40-6/2, 6, cell.textMsgTextViewOutlet.frame.size.width, cell.textMsgTextViewOutlet.frame.size.height)];
        
        
        cell.textViewHeightConstraint.constant = cell.textMsgTextViewOutlet.contentSize.height;
        
        cell.textMsgBaseViewHeightConstraint.constant = cell.textMsgTextViewOutlet.contentSize.height + 10;
        
        textviewHeight = cell.textMsgBaseViewHeightConstraint.constant;
        
        CGRect frame = cell.frame;
        frame.size.height = cell.textMsgBaseViewHeightConstraint.constant;
        cell.frame = frame;
        
        //int numLines = cell.lextTxtMsgTextViewOutlet.contentSize.height / cell.lextTxtMsgTextViewOutlet.font.lineHeight;
        
        float rows = (cell.lextTxtMsgTextViewOutlet.contentSize.height - cell.lextTxtMsgTextViewOutlet.textContainerInset.top - cell.lextTxtMsgTextViewOutlet.textContainerInset.bottom) / cell.lextTxtMsgTextViewOutlet.font.lineHeight;
        
//        if (rows == 1) {
//
//            cell.textViewWidthConstraint.constant = sz.width + 20;
//        }
//        else
//        {
//            cell.textViewWidthConstraint.constant = 300;
//        }
        
        
//        if (sz.width > self.tableViewOutlet.frame.size.width - 50) {
//
//           cell.textViewWidthConstraint.constant = self.tableViewOutlet.frame.size.width - 50;
//
//           // cell.textViewWidthConstraint.constant = 200;
//
//        }
//        else
//        {
//            cell.textViewWidthConstraint.constant = sz.width + 20;
//            cell.textViewHeightConstraint.constant = cell.textMsgTextViewOutlet.contentSize.height;
//        }
    
    
    }else{
        
        cell.lextTxtMsgTextViewOutlet.hidden = NO;
        cell.textMsgTextViewOutlet.hidden = YES;
        
        cell.rightTimeLblOutlet.hidden = YES;
        cell.leftTimeLblOutlet.hidden = NO;
        
        cell.lextTxtMsgTextViewOutlet.text = [dictionary objectForKey:@"text"];
        cell.leftTimeLblOutlet.text = timeString;
        [cell.lextTxtMsgTextViewOutlet setTextAlignment:NSTextAlignmentLeft];
        
        CGSize sz = [cell.lextTxtMsgTextViewOutlet.text sizeWithFont:cell.lextTxtMsgTextViewOutlet.font];
        
        
        
        cell.leftTextViewHeightConstraint.constant = cell.lextTxtMsgTextViewOutlet.contentSize.height;
        
        cell.textMsgBaseViewHeightConstraint.constant = cell.lextTxtMsgTextViewOutlet.contentSize.height + 10;
        
        textviewHeight = cell.textMsgBaseViewHeightConstraint.constant;
        
        CGRect frame = cell.frame;
        frame.size.height = cell.textMsgBaseViewHeightConstraint.constant;
        cell.frame = frame;
        
        //int numLines = cell.lextTxtMsgTextViewOutlet.contentSize.height / cell.lextTxtMsgTextViewOutlet.font.lineHeight;
        
        float rows = (cell.lextTxtMsgTextViewOutlet.contentSize.height - cell.lextTxtMsgTextViewOutlet.textContainerInset.top - cell.lextTxtMsgTextViewOutlet.textContainerInset.bottom) / cell.lextTxtMsgTextViewOutlet.font.lineHeight;
        
//        if (rows == 1) {
//
//            cell.leftTextViewWidthConstraint.constant = sz.width + 20;
//        }
//        else
//        {
//           cell.leftTextViewWidthConstraint.constant = 300;
//        }
        
//        [cell.textMsgTextViewOutlet setFrame:CGRectMake([UIScreen mainScreen].bounds.size.width-self.pictureProfile.frame.size.width-6*4, 6, ([UIScreen mainScreen].bounds.size.width/4*3)-10, 0)];
//        [cell.textMsgTextViewOutlet sizeToFit];
//        [cell.textMsgTextViewOutlet setFrame:CGRectMake([UIScreen mainScreen].bounds.size.width-cell.textMsgTextViewOutlet.frame.size.width-10-40-6/2, 6, cell.textMsgTextViewOutlet.frame.size.width, cell.textMsgTextViewOutlet.frame.size.height)];
        
        
//        if (sz.width > self.tableViewOutlet.frame.size.width - 50) {
//
//            cell.leftTextViewWidthConstraint.constant = self.tableViewOutlet.frame.size.width - 50;
//           // cell.leftTextViewWidthConstraint.constant = 200;
//
//        }
//        else
//        {
//        cell.leftTextViewWidthConstraint.constant = sz.width + 20;
//            cell.leftTextViewHeightConstraint.constant = cell.lextTxtMsgTextViewOutlet.contentSize.height;
//        }
        
    }
        
}


- (BOOL) textViewShouldBeginEditing:(UITextView *)textView
{
    
    if (textView == self.msgTextViewOutlet) {
        
        [self animateTextField:self.sentMsgBaseViewOutlet up:YES];
        
    }
    
    return YES;
}

-(void) textViewDidChange:(UITextView *)textView
{
//    CGFloat fixedWidth = textView.frame.size.width;
//    CGSize newSize = [textView sizeThatFits:CGSizeMake(fixedWidth, MAXFLOAT)];
//    CGRect newFrame = textView.frame;
//    newFrame.size = CGSizeMake(fmaxf(newSize.width, fixedWidth), newSize.height);
//    textView.frame = newFrame;
}

- (BOOL)textView:(UITextView *)textView shouldChangeTextInRange:(NSRange)range replacementText:(NSString *)text {
    
    if([text isEqualToString:@"\n"]) {
        [self animateTextField:self.sentMsgBaseViewOutlet up:NO];
        
        [textView resignFirstResponder];
        return NO;
    }
    return YES;
}
-(void)textViewDidEndEditing:(UITextView *)textView
{
    
    /*YOUR CODE HERE*/
}

-(void)animateTextField:(UIView*)textField up:(BOOL)up
{
   // NSNotification * notification = [[NSNotification alloc] init];
    
    const int movementDistance = -258; // tweak as needed
    const float movementDuration = 0.3f; // tweak as needed
    
    int movement = (up ? movementDistance : -movementDistance);
    
    [UIView beginAnimations: @"animateTextField" context: nil];
    [UIView setAnimationBeginsFromCurrentState: YES];
    [UIView setAnimationDuration: movementDuration];
    self.tableViewBaseViewOutlet.frame = CGRectOffset(self.tableViewBaseViewOutlet.frame, 0, movement);
    [UIView commitAnimations];
    
    CGRect frame = self.tableViewBaseViewOutlet.frame;
    
    NSLog(@"tableviewFrame:%fl",frame.origin.y);
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

- (IBAction)backBtnAction:(id)sender {
    
    [self animateTextField:self.sentMsgBaseViewOutlet up:NO];
    
    [self.msgTextViewOutlet resignFirstResponder];
    
    if ([self.viewIdendify isEqualToString:@"UserProfileView"]) {
        
        [self dismissViewControllerAnimated:NO completion:nil];
        
    }
    else
    {
       [self.navigationController popViewControllerAnimated:NO];
    }
    
}

- (IBAction)videoCallBtnAction:(id)sender {
    
    if ([self.client isStarted]) {
        NSLog(@"%@",self.userIncoming);
        id<SINCall> call = [self.client.callClient callUserVideoWithId:self.userIncoming];
        [self performSegueWithIdentifier:@"callView" sender:call];
    }
    
}

- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    
//    CallViewController *callViewController = [segue destinationViewController];
//    id<SINCall> call = [self.client.callClient callUserVideoWithId:@"ioschatmotog@mailinator.com"];
//    callViewController.call = call;
    
    CallViewController *callViewController = [segue destinationViewController];
    callViewController.call = sender;
    
}

- (IBAction)cameraBtnAction:(id)sender {
    
    if (self.tableViewBaseViewOutlet.frame.origin.y < 0) {
        
        [self animateTextField:self.sentMsgBaseViewOutlet up:NO];
    }
    [[Utilities sharedInstance] showMediaAlertView:self];
    [self.msgTextViewOutlet resignFirstResponder];
    
}

- (IBAction)locationBtnaction:(id)sender {
    self.actualPageNumber+=1;
    double lat = *([SharedVariables sharedInstance].lat);
    double lon = *([SharedVariables sharedInstance].lon);
    NSString *locationText = [NSString stringWithFormat:@"[!<loc>%f;%f</loc>!]",lat,lon];
    [[SinchClient sharedClient] sendMessageText:locationText toUser:self.userIncoming];
    self.arrayMessage = [[Client sharedClient] getMessagesCacheFromUser:self.userIncoming numberMessage:self.actualPageNumber];
    
    if((int)self.arrayMessage.count==0){
        [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(receiveCacheUpload:) name:@"cacheUpload" object:nil];
        self.arrayMessage = [NSMutableArray arrayWithCapacity:0];
        NSDictionary *messageDictionary = @{@"usernameToChat":self.userIncoming,
                                            @"text":locationText,
                                            @"recipient":self.userIncoming,
                                            @"sender":[Client sharedClient].user.email,
                                            @"sent":[NSNumber numberWithInt:2],
                                            @"timestamp":[NSDate date]
                                            };
        [self.arrayMessage addObject:messageDictionary];
        [self.tableViewOutlet insertRowsAtIndexPaths:@[[NSIndexPath indexPathForRow:0 inSection:0]] withRowAnimation:UITableViewRowAnimationBottom];
    }else{
        if([self.tableViewOutlet numberOfRowsInSection:0]==self.arrayMessage.count-1){
            [[NSNotificationCenter defaultCenter] removeObserver:self name:@"cacheUpload" object:nil];
            [self.tableViewOutlet insertRowsAtIndexPaths:@[[NSIndexPath indexPathForRow:self.arrayMessage.count-1 inSection:0]] withRowAnimation:UITableViewRowAnimationBottom];
        }else{
            [self.tableViewOutlet reloadData];
        }
    }
    [self.msgTextViewOutlet setText:@""];
//    [self.sendMessageButton setEnabled:NO];
//    [self.sendMessageButton setAlpha:.5];
//    //[self.writeMessage resignFirstResponder];
//    [self scrollTableToBottomAnimated:YES];
    //[self lunchSound:@"chat_send"];
    
    [self controlOldMessage];
    
}

- (IBAction)sentBtnAction:(id)sender {
    
//    [self animateTextField:self.sentMsgBaseViewOutlet up:NO];
//
//    [self.msgTextViewOutlet resignFirstResponder];
    
    if (self.msgTextViewOutlet.text.length > 0) {

        self.actualPageNumber+=1;
        
        if (![self.userDictionaryToChat objectForKey:@"email"]) {
            
            
            if (![self.userDictionaryToChat objectForKey:@"username"]) {
                
                self.statusLblOutlet.hidden = NO;
                self.statusLblOutlet.text = @"Sending...";
                
                [[SinchClient sharedClient] sendMessageText:self.msgTextViewOutlet.text toUser:[[self.userDictionaryToChat objectForKey:@"user"] objectForKey:@"username"]];
                self.arrayMessage = [[Client sharedClient] getMessagesCacheFromUser:[[self.userDictionaryToChat objectForKey:@"user"] objectForKey:@"username"] numberMessage:self.actualPageNumber];
                
            }
            else
            {
                self.statusLblOutlet.hidden = NO;
                self.statusLblOutlet.text = @"Sending...";
                
                [[SinchClient sharedClient] sendMessageText:self.msgTextViewOutlet.text toUser:[self.userDictionaryToChat objectForKey:@"username"]];
                
                self.arrayMessage = [[Client sharedClient] getMessagesCacheFromUser:[self.userDictionaryToChat objectForKey:@"username"] numberMessage:self.actualPageNumber];
            }
            
            
        }
        else
        {
            self.statusLblOutlet.hidden = NO;
            self.statusLblOutlet.text = @"Sending...";
            
        [[SinchClient sharedClient] sendMessageText:self.msgTextViewOutlet.text toUser:[self.userDictionaryToChat objectForKey:@"email"]];
            
            self.arrayMessage = [[Client sharedClient] getMessagesCacheFromUser:[self.userDictionaryToChat objectForKey:@"email"] numberMessage:self.actualPageNumber];
        }
        
        if((int)self.arrayMessage.count==0){
            [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(receiveCacheUpload:) name:@"cacheUpload" object:nil];
            self.arrayMessage = [NSMutableArray arrayWithCapacity:0];
            
            NSDictionary *messageDictionary = [[NSDictionary alloc]init];
            if (![self.userDictionaryToChat objectForKey:@"email"]) {
                
                if (![self.userDictionaryToChat objectForKey:@"username"]) {
                    
                    
                    messageDictionary = @{@"usernameToChat":[[self.userDictionaryToChat objectForKey:@"user"] objectForKey:@"username"],
                                          @"text":self.msgTextViewOutlet.text,
                                          @"recipient":[[self.userDictionaryToChat objectForKey:@"user"] objectForKey:@"username"],
                                          @"sender":[Client sharedClient].user.email,
                                          @"sent":[NSNumber numberWithInt:2],
                                          @"timestamp":[NSDate date]
                                          };
                    
                }
                else
                {
                    messageDictionary = @{@"usernameToChat":[self.userDictionaryToChat objectForKey:@"username"],
                                          @"text":self.msgTextViewOutlet.text,
                                          @"recipient":[self.userDictionaryToChat objectForKey:@"username"],
                                          @"sender":[Client sharedClient].user.email,
                                          @"sent":[NSNumber numberWithInt:2],
                                          @"timestamp":[NSDate date]
                                          };
                }
                
                
            }
            else
            {
                messageDictionary = @{@"usernameToChat":[self.userDictionaryToChat objectForKey:@"email"],
                                                    @"text":self.msgTextViewOutlet.text,
                                                    @"recipient":[self.userDictionaryToChat objectForKey:@"email"],
                                                    @"sender":[Client sharedClient].user.email,
                                                    @"sent":[NSNumber numberWithInt:2],
                                                    @"timestamp":[NSDate date]
                                                    };
            }
            
            
            [self.arrayMessage addObject:messageDictionary];
            [self.tableViewOutlet insertRowsAtIndexPaths:@[[NSIndexPath indexPathForRow:0 inSection:0]] withRowAnimation:UITableViewRowAnimationBottom];
        }else{
            [[NSNotificationCenter defaultCenter] removeObserver:self name:@"cacheUpload" object:nil];
            if([self.tableViewOutlet numberOfRowsInSection:0]==self.arrayMessage.count-1){
                [self.tableViewOutlet insertRowsAtIndexPaths:@[[NSIndexPath indexPathForRow:self.arrayMessage.count-1 inSection:0]] withRowAnimation:UITableViewRowAnimationBottom];
            }else{
                [self.tableViewOutlet reloadData];
            }
        }
        
//        [self.msgTextViewOutlet setText:@""];
//        [self.sendMessageButton setEnabled:NO];
//        [self.sendMessageButton setAlpha:.5];
        //[self.writeMessage resignFirstResponder];
        //[self scrollTableToBottomAnimated:YES];
        //[self lunchSound:@"chat_send"];
    self.msgTextViewOutlet.text = @"";
        [self controlOldMessage];
    }
}

-(void)sendImage:(NSString *)stringImage{
    [[SinchClient sharedClient] sendMessageText:stringImage toUser:self.userIncoming];
    //[self scrollTableToBottomAnimated:YES];
}

-(void)sendVideo:(NSString *)stringVideo{
    [[SinchClient sharedClient] sendMessageText:stringVideo toUser:self.userIncoming];
    //[self scrollTableToBottomAnimated:YES];
}

-(NSString *)getUserIncoming{
    
    return self.userIncoming;
}
-(void)cloudUploadFailuer:(NSString *)error
{
    [[Utilities sharedInstance] showCustomAlertView:self.view title:@"Warning" message:@"File size too large" controller:self reason:@"chatView"];
    self.statusLblOutlet.hidden = YES;
    
}

-(void)updateWithMessage:(NSDictionary *)dictionaryMessage{
    
   // [self.loading animate:NO];
    
    self.statusLblOutlet.text = @"Sent";
    
    double delayInSeconds = 1.0;
    dispatch_time_t popTime = dispatch_time(DISPATCH_TIME_NOW, (int64_t)(delayInSeconds * NSEC_PER_SEC));
    dispatch_after(popTime, dispatch_get_main_queue(), ^(void){
        self.statusLblOutlet.hidden = YES;
    });
    
    if(dictionaryMessage){
        if([[dictionaryMessage objectForKey:@"sender"] isEqualToString:[Client sharedClient].user.email]){
            [self.arrayMessage replaceObjectAtIndex:(int)self.arrayMessage.count-1 withObject:dictionaryMessage];
            [self.tableViewOutlet reloadData];
        }else{
            self.actualPageNumber+=1;
            [self.arrayMessage addObject:dictionaryMessage];
            [self.tableViewOutlet insertRowsAtIndexPaths:@[[NSIndexPath indexPathForRow:self.arrayMessage.count-1 inSection:0]] withRowAnimation:  UITableViewRowAnimationBottom];
            //[self lunchSound:@"chat_receive"];
        }
        
        [[Client sharedClient] updateCacheChatMessageReadedWithMessageId:dictionaryMessage];
        
//        TabBarController *tabBar=(TabBarController *)[self.navigationController.childViewControllers objectAtIndex:0];
//        [tabBar updateBadgeItem];
//        UIViewController *prevController = [tabBar.childViewControllers lastObject];
//        if([prevController isKindOfClass:[ChatListViewController class]]){
//            [(ChatListViewController *)prevController reloadContent];
//        }
        
        [self scrollTableToBottomAnimated:YES];
    }else{
        self.arrayMessage = [[Client sharedClient] getMessagesCacheFromUser:[self.userDictionaryToChat objectForKey:@"email"] numberMessage:self.actualPageNumber];
        [self.tableViewOutlet reloadData];
    }
    
    NSInteger numberOfRows = [self.tableViewOutlet numberOfRowsInSection:0];
    if (numberOfRows) {
        [self.tableViewOutlet scrollToRowAtIndexPath:[NSIndexPath indexPathForRow:numberOfRows-1 inSection:0] atScrollPosition:UITableViewScrollPositionBottom animated:NO];
    }
}
-(UIImage *)loadThumbNail:(NSURL *)urlVideo
{
    AVURLAsset *asset = [[AVURLAsset alloc] initWithURL:urlVideo options:nil];
    AVAssetImageGenerator *generate = [[AVAssetImageGenerator alloc] initWithAsset:asset];
    generate.appliesPreferredTrackTransform=TRUE;
    NSError *err = NULL;
    CMTime time = CMTimeMake(1, 60);
    CGImageRef imgRef = [generate copyCGImageAtTime:time actualTime:NULL error:&err];
    NSLog(@"err==%@, imageRef==%@", err, imgRef);
    return [[UIImage alloc] initWithCGImage:imgRef];
}

-(UIImage *)generateThumbImage : (NSString *)sourceURLString{
    NSURL *sourceMovieURL = [NSURL URLWithString:sourceURLString];
    AVURLAsset *sourceAsset = [AVURLAsset URLAssetWithURL:sourceMovieURL options:nil];
    AVAssetImageGenerator *generateImg = [[AVAssetImageGenerator alloc] initWithAsset:sourceAsset];
    NSError *error = NULL;
    CMTime time = CMTimeMake(1, 65);
    CGImageRef refImg = [generateImg copyCGImageAtTime:time actualTime:NULL error:&error];
    NSLog(@"error==%@, Refimage==%@", error, refImg);
    
    return [[UIImage alloc] initWithCGImage:refImg];
}

-(void)openCamera
{
    selectIdentity = @"images";
    UIImagePickerController *picker = [[UIImagePickerController alloc] init];
    picker.delegate = self;
    picker.allowsEditing = YES;
    picker.sourceType = UIImagePickerControllerSourceTypeCamera;
    
    [self presentViewController:picker animated:YES completion:NULL];
}
-(void)openGallery
{
    selectIdentity = @"images";
    UIImagePickerController *pickerView = [[UIImagePickerController alloc] init];
    pickerView.allowsEditing = YES;
    pickerView.delegate = self;
    [pickerView setSourceType:UIImagePickerControllerSourceTypeSavedPhotosAlbum];
    [self presentViewController:pickerView animated:YES completion:nil];
    
}

-(void)openVideoRoll
{
    selectIdentity = @"videos";
    UIImagePickerController *videoPicker = [[UIImagePickerController alloc] init];
    videoPicker.delegate = self; // ensure you set the delegate so when a video is chosen the right method can be called
    
    videoPicker.modalPresentationStyle = UIModalPresentationCurrentContext;
    // This code ensures only videos are shown to the end user
    videoPicker.mediaTypes = @[(NSString*)kUTTypeMovie, (NSString*)kUTTypeAVIMovie, (NSString*)kUTTypeVideo, (NSString*)kUTTypeMPEG4];
    
    videoPicker.videoQuality = UIImagePickerControllerQualityTypeHigh;
    [self presentViewController:videoPicker animated:YES completion:nil];
    
}

-(void)openVideoCamera
{
    selectIdentity = @"videos";
    UIImagePickerController *picker = [[UIImagePickerController alloc]init];
    picker.sourceType = UIImagePickerControllerSourceTypeCamera;
    picker.delegate = self;
    picker.allowsEditing = NO;
    
    NSArray *mediaTypes = [[NSArray alloc]initWithObjects:(NSString *)kUTTypeMovie, nil];
    
    picker.mediaTypes = mediaTypes;
    
    [self presentViewController:picker animated:YES completion:nil];
    
}

- (void)imagePickerController:(UIImagePickerController *)picker didFinishPickingMediaWithInfo:(NSDictionary *)info{
    
    if ([selectIdentity isEqualToString:@"images"]) {
        
        self.statusLblOutlet.hidden = NO;
        self.statusLblOutlet.text = @"Sending...";
        
        UIImage *chosenImage = info[UIImagePickerControllerOriginalImage];
        CGSize newSize = CGSizeMake(640, chosenImage.size.height*640/chosenImage.size.width);
        UIImage *scaledImage = [UtilityGraphic imageWithImage:chosenImage scaledToSize:newSize];
        
        NSURL *imagePath = [info objectForKey:@"UIImagePickerControllerReferenceURL"];
        NSString *imageName = [imagePath lastPathComponent];
        if(!imageName){
            imageName = [NSString stringWithFormat:@"takePhoto_%i",(int)[[NSDate date] timeIntervalSince1970]];
        }else{
            imageName = [NSString stringWithFormat:@"%@_%i",imageName,(int)[[NSDate date] timeIntervalSince1970]];
        }
        
        NSString *pathSaveTempImage = [self saveTempImage:scaledImage withName:imageName];
        
        NSString *fileName = [[[NSProcessInfo processInfo] globallyUniqueString] stringByAppendingString:@".png"];
        NSString *filePath = [[NSTemporaryDirectory() stringByAppendingPathComponent:@"upload"] stringByAppendingPathComponent:fileName];
        NSData * imageData = UIImagePNGRepresentation(chosenImage);
        
        [imageData writeToFile:filePath atomically:YES];
        
        AWSS3TransferManagerUploadRequest *uploadRequest = [AWSS3TransferManagerUploadRequest new];
        uploadRequest.body = [NSURL fileURLWithPath:filePath];
        uploadRequest.key = fileName;
        uploadRequest.bucket = @"staticapi.maboapp.com";
        
        //[self uploadData:imageData];
        
       // [self.collection insertObject:uploadRequest atIndex:0];
        
        //[self upload:uploadRequest];
        
        [picker dismissViewControllerAnimated:YES completion:^{
            
            self.actualPageNumber+=1;
            NSDictionary *tempDicMessage = @{@"sender":[Client sharedClient].user.email,
                                             @"text":@"sendImage",
                                             @"tempImage":pathSaveTempImage,
                                             @"sent":[NSNumber numberWithInt:2],
                                             @"timestamp":[NSDate date],
                                             @"recipient":self.userIncoming
                                             };
            [self.arrayMessage addObject:tempDicMessage];
            
            [[Client sharedClient].sendingPhoto addObject:tempDicMessage];
            /*
             self.actualPageNumber+=1;
             NSDictionary *messageDictionary = @{@"usernameToChat":[self.userDictionaryToChat objectForKey:@"email"],
             @"messageId":[NSString stringWithFormat:@"tempImage_%@",imageName],
             @"recipient":[self.userDictionaryToChat objectForKey:@"email"],
             @"sender":[HKClient sharedClient].user.email,
             @"tempImage":pathSaveTempImage,
             @"sent":[NSNumber numberWithInt:2]
             };
             //[[HKClient sharedClient] updateCacheChatWithMessage:messageDictionary];
             [[HKClient sharedClient] addObjectToArr:messageDictionary];
             */
            //[self.tableViewOutlet reloadData];
            //[self.tableViewOutlet insertRowsAtIndexPaths:@[[NSIndexPath indexPathForRow:self.arrayMessage.count-1 inSection:0]] withRowAnimation:UITableViewRowAnimationBottom];
            //[self scrollTableToBottomAnimated:YES];
            
//            self.loading = [[LoadingView alloc] initWithFrame:[UIScreen mainScreen].bounds];
//            [self.view addSubview:self.loading];
//            [self.loading animate:YES];
            
            [[Client sharedClient] updatePicture:scaledImage withIndex:-2 completition:^(NSDictionary *dictionaryPicture, NSError *error) {
                if(dictionaryPicture){
                    BOOL success = [[dictionaryPicture objectForKey:@"success"] boolValue];
                    if(success){
                        NSString *stringImage = [NSString stringWithFormat:@"[!<img>%@</img>!]",[[dictionaryPicture objectForKey:@"data"] objectForKey:@"path"]];
                        [[Client sharedClient].sendingPhoto removeObject:tempDicMessage];
                        [self sendImage:stringImage];
                    }else{
                        self.actualPageNumber-=1;
                        [self.arrayMessage removeObject:tempDicMessage];
                        [[Client sharedClient].sendingPhoto removeAllObjects];
                        [self.tableViewOutlet deleteRowsAtIndexPaths:@[[NSIndexPath indexPathForRow:self.arrayMessage.count inSection:0]] withRowAnimation:UITableViewRowAnimationNone];
                        [self destroy:pathSaveTempImage];
                        
//                        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"OOPS!" message:NSLocalizedString(@"Error to send image", nil) delegate:self cancelButtonTitle:@"OK" otherButtonTitles:nil];
//                        [alert show];
                    }
                }else{
                    [self.arrayMessage removeObject:tempDicMessage];
                    [[Client sharedClient].sendingPhoto removeAllObjects];
                    self.actualPageNumber-=1;
                    [self.tableViewOutlet deleteRowsAtIndexPaths:@[[NSIndexPath indexPathForRow:self.arrayMessage.count inSection:0]] withRowAnimation:UITableViewRowAnimationNone];
                    [self destroy:pathSaveTempImage];
//                    UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"OOPS!" message:NSLocalizedString(@"Error to send image", nil) delegate:self cancelButtonTitle:@"OK" otherButtonTitles:nil];
//                    [alert show];
                }
                
            }];
            
        }];
        
    }
    else if ([selectIdentity isEqualToString:@"videos"])
    {
        
        self.statusLblOutlet.hidden = NO;
        self.statusLblOutlet.text = @"Sending...";
        
        NSURL *videoURL;
        NSString *videoPath1;
        
        sendVideoPathUrl = info[UIImagePickerControllerMediaURL];
        
        NSData *urlData = [NSData dataWithContentsOfURL:sendVideoPathUrl];
        
        int count = (int)(urlData.length);
        //NSLog(@"count:%d",count);
        
        //if (![self isFileSizeUpTo10Mebibytes:count]) {
        
        [picker dismissViewControllerAnimated:YES completion:NULL];
        NSLog(@"File is more than 10 MB.");
        // [[Utilities sharedInstance] showCustomAlertView:self.view title:@"Mabo" message:@"Your post video is more than 10 MB." controller:self reason:@"postVideo"];
        //[self uploadToS3];
        
        NSString*sourceURLString =[NSString stringWithFormat:@"%@", [info                                                         objectForKey:UIImagePickerControllerMediaURL]];
        
        //   dispatch_async(dispatch_get_main_queue(), ^{
        UIImage* orginalImage = [self generateThumbImage:sourceURLString];
        
//        self.loading = [[LoadingView alloc] initWithFrame:[UIScreen mainScreen].bounds];
//        [self.view addSubview:self.loading];
//        [self.loading animate:YES];
        
        NSString *fileName = [[[NSProcessInfo processInfo] globallyUniqueString] stringByAppendingString:@".png"];
        NSString *filePath = [[NSTemporaryDirectory() stringByAppendingPathComponent:@"upload"] stringByAppendingPathComponent:fileName];
        NSData * imageData = UIImagePNGRepresentation(orginalImage);
        
        [imageData writeToFile:filePath atomically:YES];
        
       AVURLAsset* asset = [AVURLAsset URLAssetWithURL:sendVideoPathUrl options:nil];
        AVAssetExportSession *exportSession = nil;
        exportSession = [[AVAssetExportSession alloc] initWithAsset:asset presetName:AVAssetExportPresetHighestQuality];
        exportSession.shouldOptimizeForNetworkUse = YES;
        exportSession.outputFileType = AVFileTypeMPEG4;
        
        NSString *fileName1 = [NSString stringWithFormat:@"%@_%@", [[NSProcessInfo processInfo] globallyUniqueString], @"video.mov"];
        NSString * filePath1 = [NSTemporaryDirectory() stringByAppendingPathComponent:fileName1];
        NSURL *fileURL = [NSURL fileURLWithPath:filePath1];
        exportSession.outputURL = fileURL;
        
        
        [exportSession exportAsynchronouslyWithCompletionHandler:^(void){
            
            NSString *status = @"";
            
            if( exportSession.status == AVAssetExportSessionStatusUnknown ) status = @"AVAssetExportSessionStatusUnknown";
            else if( exportSession.status == AVAssetExportSessionStatusWaiting ) status = @"AVAssetExportSessionStatusWaiting";
            else if( exportSession.status == AVAssetExportSessionStatusExporting ) status = @"AVAssetExportSessionStatusExporting";
            else if( exportSession.status == AVAssetExportSessionStatusCompleted ) status = @"AVAssetExportSessionStatusCompleted";
            else if( exportSession.status == AVAssetExportSessionStatusFailed ) status = @"AVAssetExportSessionStatusFailed";
            else if( exportSession.status == AVAssetExportSessionStatusCancelled ) status = @"AVAssetExportSessionStatusCancelled";
            
            NSLog(@"done exporting to %@ status %ld = %@ (%@)",exportSession.outputURL,(long)exportSession.status, status,[[exportSession error] localizedDescription]);
            
            NSURL *compressPostVideoURL = exportSession.outputURL;
            
            NSString * timestamp = [NSString stringWithFormat:@"%f",[[NSDate date] timeIntervalSince1970] * 1000];
            timeArray = [timestamp componentsSeparatedByString:@"."];
            
            NSString *cloudFileName = [NSString stringWithFormat:@"%@_%@",[Client sharedClient].user.email,timeArray.firstObject];
            
            [[cloudinaryHelper sharedInstance]cloudinaryVideo:^(NSDictionary *resultDict)
             {
                 
                 dispatch_async(dispatch_get_main_queue(), ^{
                     
                     NSLog(@"VideoUrl:%@",resultDict);
                     
                     NSString * cloudUrlString = [resultDict objectForKey:@"secure_url"];
                     
                     NSDictionary *tempDicMessage = @{@"sender":[Client sharedClient].user.email,
                                                      @"text":@"sendVideo",
                                                      @"tempImage":cloudUrlString,
                                                      @"sent":[NSNumber numberWithInt:2],
                                                      @"timestamp":[NSDate date],
                                                      @"recipient":self.userIncoming
                                                      };
                     [self.arrayMessage addObject:tempDicMessage];
                     
                     NSString *stringVideo = [NSString stringWithFormat:@"[!<video>%@</video>!]",cloudUrlString];
                     
                     [self sendVideo:stringVideo];
                     
                     [self sendTumbImageCloud:orginalImage];
                     
                 });
             }fromVideo:[NSString stringWithFormat:@"%@",compressPostVideoURL] fromName: cloudFileName];
        }];
        
       // [self uploadImageToS3:orginalImage];
    }
    
    //[self dismissViewControllerAnimated:YES completion:nil];
    
}

-(void)sendTumbImageCloud: (UIImage *)image ;
{
    NSString *cloudFileName = [NSString stringWithFormat:@"%@_%@",[Client sharedClient].user.email,timeArray.firstObject];
    
    [[cloudinaryHelper sharedInstance]cloudinary:^(NSDictionary *resultDict)
     {
         // NSString *cloudURL = [[cloudinaryHelper sharedInstance]cloudinaryURL:imagestring];
         NSLog(@"%@",resultDict);
         
     }
    fromImage:image fromName:cloudFileName];
}

-(NSString *)saveTempImage:(UIImage *)image withName:(NSString *)name{
    NSString *result=@"";
    NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
    NSString *documentsDirectory = [paths objectAtIndex:0];
    NSString *nameImage = [[name componentsSeparatedByString:@"/"] lastObject];
    NSString *fullPath = [documentsDirectory stringByAppendingPathComponent:nameImage];
    NSFileManager *fileManager = [NSFileManager defaultManager];
    NSData *dataImage = UIImageJPEGRepresentation(image,1.0);
    [fileManager createFileAtPath:fullPath contents:dataImage attributes:nil];
    result=fullPath;
    return result;
}

-(void)destroy:(NSString *)path{
    NSFileManager *fileManager = [NSFileManager defaultManager];
    [fileManager removeItemAtPath:path error:nil];
}

- (void)uploadImageToS3: (UIImage *)image {
    
    NSData *imageData = UIImageJPEGRepresentation(image, 0.7);
    NSString * timestamp = [NSString stringWithFormat:@"%f",[[NSDate date] timeIntervalSince1970] * 1000];
    timeArray = [timestamp componentsSeparatedByString:@"."];
    
    NSString *key = [NSString stringWithFormat:@"video/%@_%@.mp4_thumb.jpg",[Client sharedClient].user.email,timeArray.firstObject];
    
    //https://s3.amazonaws.com/staticapi.maboapp.com/video/chatmotog1%40mailinator.com_1512968755742.mp4_thumb.jpg
    
    
    AWSCognitoCredentialsProvider *credentialsProvider = [[AWSCognitoCredentialsProvider alloc] initWithRegionType:AWSRegionUSEast1 identityPoolId:@"us-east-1:2e250934-9456-4acd-a2cd-95f1bca4295d"];
    
   // AWSStaticCredentialsProvider *credentialsProvider = [[AWSStaticCredentialsProvider alloc] initWithAccessKey:@"AKIAJX3CEK2EVCTODUGQ" secretKey:@"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"];
    
    AWSServiceConfiguration *configuration = [[AWSServiceConfiguration alloc] initWithRegion:AWSRegionUSEast1 credentialsProvider:credentialsProvider];
    
    [AWSServiceManager defaultServiceManager].defaultServiceConfiguration = configuration;
    
    AWSS3GetPreSignedURLRequest *getPreSignedURLRequest = [AWSS3GetPreSignedURLRequest new];
    getPreSignedURLRequest.bucket = @"staticapi.maboapp.com";
    getPreSignedURLRequest.key = key;
    getPreSignedURLRequest.HTTPMethod = AWSHTTPMethodPUT;
    getPreSignedURLRequest.expires = [NSDate dateWithTimeIntervalSinceNow:3600];
    
    NSString *fileContentTypeString = @"image/jpeg";
    getPreSignedURLRequest.contentType = fileContentTypeString;
    
    [[[AWSS3PreSignedURLBuilder defaultS3PreSignedURLBuilder] getPreSignedURL:getPreSignedURLRequest] continueWithBlock:^id(AWSTask *task) {
        
        if (task.error) {
            NSLog(@"Error: %@", task.error);
        } else {
            
            NSURL *presignedURL = task.result;
            NSLog(@"upload presignedURL is \n%@", presignedURL);
            
            NSMutableURLRequest *request = [NSMutableURLRequest requestWithURL:presignedURL];
            request.cachePolicy = NSURLRequestReloadIgnoringLocalCacheData;
            [request setHTTPMethod:@"PUT"];
            [request setValue:fileContentTypeString forHTTPHeaderField:@"Content-Type"];
            
            NSURLSessionUploadTask *uploadTask = [[NSURLSession sharedSession] uploadTaskWithRequest:request fromData:imageData completionHandler:^(NSData *data, NSURLResponse *response, NSError *error) {
                
                if (error) {
                    
                    NSLog(@"Upload errer: %@", error);
                }
                else
                {
                   // NSString* newStr = [NSString stringWithUTF8String:[data bytes]];
                    //NSLog(@"dataSTR:%@",newStr);
                    NSLog(@"reponse:%@",response);
                    NSLog(@"responseResult:%@",response.URL);
                    NSLog(@"videoUrl:%@",sendVideoPathUrl);
                    
                    NSString * s3UrlString = [NSString stringWithFormat:@"video/%@_%@.mp4",[Client sharedClient].user.email,timeArray.firstObject];
                    
                    NSDictionary *tempDicMessage = @{@"sender":[Client sharedClient].user.email,
                                                     @"text":@"sendVideo",
                                                     @"tempImage":s3UrlString,
                                                     @"sent":[NSNumber numberWithInt:2],
                                                     @"timestamp":[NSDate date],
                                                     @"recipient":self.userIncoming
                                                     };
                    [self.arrayMessage addObject:tempDicMessage];

                   // [[Client sharedClient].sendingPhoto addObject:tempDicMessage];

                   // [self.tableViewOutlet insertRowsAtIndexPaths:@[[NSIndexPath indexPathForRow:self.arrayMessage.count-1 inSection:0]] withRowAnimation:UITableViewRowAnimationBottom];
                    
                    
                    NSString *stringVideo = [NSString stringWithFormat:@"[!<video>%@</video>!]",s3UrlString];
                    
                    [self sendVideo:stringVideo];
                    
                    
                    [self amazonS3Upload:sendVideoPathUrl];
                }
                
                NSLog(@"Done");
            }];
            
            [uploadTask resume];
        }
        
        return nil;
        
    }];
}

- (void)amazonS3Upload:(NSURL *) uploadUrl
{
    // amazon web service s3 api
    AWSS3TransferManager *transferManager = [AWSS3TransferManager defaultS3TransferManager];
    
   // NSString * timestamp = [NSString stringWithFormat:@"%f",[[NSDate date] timeIntervalSince1970] * 1000];
    
  //  NSArray * timeArray = [timestamp componentsSeparatedByString:@"."];
    
    NSString *key = [NSString stringWithFormat:@"video/%@_%@.mp4",[Client sharedClient].user.email,timeArray.firstObject];
    
//https://s3.amazonaws.com/staticapi.maboapp.com/video/chatmotog1%40mailinator.com_1512968755742.mp4
    
    AWSS3TransferManagerUploadRequest *uploadRequest = [AWSS3TransferManagerUploadRequest new];
    uploadRequest.bucket = @"staticapi.maboapp.com"; // Your Bucket Name
    uploadRequest.key = key; // Your File Name in Bucket
    uploadRequest.body = uploadUrl;
    uploadRequest.uploadProgress =  ^(int64_t bytesSent, int64_t totalBytesSent, int64_t totalBytesExpectedToSend){
        dispatch_async(dispatch_get_main_queue(), ^{
            //Update progress.
            NSLog(@"UPLOAD PROGRESS: %lld : %lld : %lld", bytesSent,totalBytesSent,totalBytesExpectedToSend);
        });
    };
    
    [[transferManager upload:uploadRequest] continueWithExecutor:[AWSExecutor mainThreadExecutor]
                                                       withBlock:^id(AWSTask *task) {
                                                           if (task.error) {
                                                               if ([task.error.domain isEqualToString:AWSS3TransferManagerErrorDomain]) {
                                                                   switch (task.error.code) {
                                                                       case AWSS3TransferManagerErrorCancelled:
                                                                       case AWSS3TransferManagerErrorPaused:
                                                                           break;
                                                                           
                                                                       default:
                                                                           NSLog(@"Error: %@", task.error);
                                                                           break;
                                                                   }
                                                               } else {
                                                                   // Unknown error.
                                                                   NSLog(@"Error: %@", task.error);
                                                               }
                                                           }
                                                           if (task.result) {
                                                               AWSS3TransferManagerUploadOutput *uploadOutput = task.result;
                                                               NSLog(@"upload response: %@", uploadOutput);
                                                               // The file uploaded successfully.
                                                           }
                                                           return nil;
                                                       }];
    
}

-(void)receiveStatusUpload:(NSNotification *)notification{
    self.arrayMessage = [[Client sharedClient] getMessagesCacheFromUser:self.userIncoming numberMessage:self.actualPageNumber];
    [self controlOldMessage];
    [self.tableViewOutlet reloadData];
}

#pragma mark - DELEGATE

-(void)reloadCell:(id)cell{
    //ChatTableViewCell *realCell = (ChatTableViewCell *)cell;
    //NSIndexPath *indexPath = [self.tableMessage indexPathForCell:realCell];
    //NSArray *arrayVisible = [self.tableMessage indexPathsForVisibleRows];
    //if([arrayVisible containsObject:indexPath]){
    //[self.tableMessage reloadData];
    [self.tableViewOutlet beginUpdates];
    [self.tableViewOutlet reloadData];
    [self.tableViewOutlet endUpdates];
    [self scrollTableToBottomAnimated:YES];
    //}
}

-(void)resendOrDeleteMessage:(NSDictionary *)dictionary withCell:(id)cell{
    
    self.dictionaryMessageForResend=dictionary;
    ChatTableViewCell *realCell = (ChatTableViewCell *)cell;
    self.indexPathForResend=[self.tableViewOutlet indexPathForCell:realCell];
    
    UIActionSheet *actionSheet = [[UIActionSheet alloc]
                                  initWithTitle:nil
                                  delegate:self
                                  cancelButtonTitle:NSLocalizedString(@"Cancel",nil)
                                  destructiveButtonTitle:nil
                                  otherButtonTitles:NSLocalizedString(@"Resend message", nil),NSLocalizedString(@"Delete message", nil), nil];
    [actionSheet setTag:101];
    [actionSheet showInView:self.view];
}

-(NSString *)whoIs{
    return [self.userDictionaryToChat objectForKey:@"extended_name"];
}

-(void)shareImage:(UIImage *)image fromCell:(id)cell{
    if(image){
        [Client sharedClient].incomingOffSet=self.tableViewOutlet.contentOffset.y;
        ChatTableViewCell *cellToShare = cell;
        [self.tableViewOutlet bringSubviewToFront:cell];
        CGRect rect = CGRectMake(0, -cellToShare.frame.origin.y+self.tableViewOutlet.contentOffset.y, [UIScreen mainScreen].bounds.size.width, self.view.frame.size.height);
        [UIView animateWithDuration:.3 animations:^{
            [cellToShare.leftImageViewOutlet setBackgroundColor:[UIColor grayColor]];
            [cellToShare.leftImageViewOutlet setFrame:rect];
        } completion:^(BOOL finished) {
//            ImageViewController *imageController = [[ImageViewController alloc] initWithImage:image andUsername:[self.userDictionaryToChat objectForKey:@"extended_name"] modeChat:YES isEdited:NO];
//            [imageController setModalTransitionStyle:UIModalTransitionStyleCrossDissolve];
//            [self.navigationController presentViewController:imageController animated:YES completion:nil];
        }];
    }
}

#pragma mark - SCROLLVIEW DELEGATE

- (void)scrollViewDidScroll:(UIScrollView *)scrollView{
    if(self.labelOldMessage){
        float valueAlpha=(-scrollView.contentOffset.y+scrollView.contentInset.top)/100;
        float newWidth=valueAlpha*([UIScreen mainScreen].bounds.size.width-40);
        if(newWidth>([UIScreen mainScreen].bounds.size.width-40)){newWidth=([UIScreen mainScreen].bounds.size.width-40);}
        [self.labelOldMessage setFrame:CGRectMake(([UIScreen mainScreen].bounds.size.width-newWidth)/2, self.labelOldMessage.frame.origin.y, newWidth, self.labelOldMessage.frame.size.height)];
        [self.labelOldMessage setAlpha:valueAlpha];
        [self.labelOldMessage setBackgroundColor:(scrollView.contentOffset.y<=-80)?[UIColor greenColor]:[UIColor lightGrayColor]];
        [self.labelOldMessage setTextColor:(scrollView.contentOffset.y<=-80)?[UIColor yellowColor]:[UIColor grayColor]];
        [self.labelOldMessage setTransform:CGAffineTransformMakeTranslation(0, scrollView.contentOffset.y/6)];
    }
}


-(void)scrollViewWillEndDragging:(UIScrollView *)scrollView withVelocity:(CGPoint)velocity targetContentOffset:(inout CGPoint *)targetContentOffset{
    
    if(scrollView.contentOffset.y<-80){
        int oldCount =  (int)self.arrayMessage.count;
        self.actualPageNumber+=30;
        self.arrayMessage = [[Client sharedClient] getMessagesCacheFromUser:[self.userDictionaryToChat objectForKey:@"email"] numberMessage:self.actualPageNumber];
        int newCount = (int)self.arrayMessage.count;
        [self.tableViewOutlet reloadData];
        
        self.statePosition = newCount-oldCount;
        [self controlOldMessage];
    }
    
}

-(void)scrollViewDidEndDragging:(UIScrollView *)scrollView willDecelerate:(BOOL)decelerate{
    
    if(self.statePosition>=0){
        NSIndexPath *indexPath = [NSIndexPath indexPathForRow:self.statePosition inSection:0];
        [self.tableViewOutlet scrollToRowAtIndexPath:indexPath atScrollPosition:UITableViewScrollPositionTop animated:NO];
        self.statePosition=-1;
    }
    
}

-(void)scrollTableToBottomAnimated:(BOOL)animated{
    if(self.arrayMessage.count>0){
        NSIndexPath* indexPath = [NSIndexPath indexPathForRow:self.arrayMessage.count-1 inSection:0];
        [self.tableViewOutlet scrollToRowAtIndexPath:indexPath atScrollPosition:UITableViewScrollPositionTop animated:animated];
        [[UIApplication sharedApplication] setApplicationIconBadgeNumber:0];
    }
}

@end
