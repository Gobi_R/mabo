  //
//  AppDelegate.m
//  Mobo
//
//  Created by vishnuprasathg on 10/12/17.
//  Copyright © 2017 DCI. All rights reserved.
//

#import "AppDelegate.h"
#import "Client.h"
#import "SinchClient.h"
#import "TabBarViewController.h"
#import "SignInViewController.h"
#import <FBSDKCoreKit/FBSDKCoreKit.h>
#import <FBSDKLoginKit/FBSDKLoginKit.h>
#import <FBSDKShareKit/FBSDKShareKit.h>
#import <UserNotifications/UserNotifications.h>
#import <UserNotificationsUI/UserNotificationsUI.h>
#import "SharedVariables.h"

#import <AWSCore/AWSCore.h>
#import <AWSCognito/AWSCognito.h>
#import <AWSS3/AWSS3.h>
#import <AWSDynamoDB/AWSDynamoDB.h>
#import <AWSSQS/AWSSQS.h>
#import <AWSSNS/AWSSNS.h>
#import "UIImageView+WebCache.h"
#import "CallViewController.h"
#import <PushKit/PushKit.h>

#define SYSTEM_VERSION_GREATER_THAN_OR_EQUAL_TO(v)  ([[[UIDevice currentDevice] systemVersion] compare:v options:NSNumericSearch] != NSOrderedAscending)
#define SYSTEM_VERSION_LESS_THAN(v) ([[[UIDevice currentDevice] systemVersion] compare:v options:NSNumericSearch] == NSOrderedAscending)

@import Firebase;

@interface AppDelegate () <FIRMessagingDelegate,UNUserNotificationCenterDelegate,SINClientDelegate, SINCallClientDelegate, SINManagedPushDelegate, SINCallDelegate, UIApplicationDelegate,PKPushRegistryDelegate>

@property (nonatomic, readwrite, strong) id<SINManagedPush> push;

@end

@implementation AppDelegate

{
    UIView * customNotification;
    UIImageView * notificationImageView;
    UILabel * notificationTitleLbl;
    UILabel * notificationSubTitleLbl;
}


- (BOOL)application:(UIApplication *)application didFinishLaunchingWithOptions:(NSDictionary *)launchOptions {
    /*
     [FIRApp configure];
    [[FBSDKApplicationDelegate sharedInstance] application:application
                             didFinishLaunchingWithOptions:launchOptions];
    [SharedVariables sharedInstance].s3BaseUrl = @"https://s3.amazonaws.com/staticapi.maboapp.com/";
    [[Client sharedClient] createOrLoadCacheUser];
    [[Client sharedClient] createOrLoadCacheChat];
    [[Client sharedClient] startReachAbility];
    self.push = [Sinch managedPushWithAPSEnvironment:SINAPSEnvironmentAutomatic];
    self.push.delegate = self;
    [self.push setDesiredPushTypeAutomatically];
    AWSCognitoCredentialsProvider *credentialsProvider = [[AWSCognitoCredentialsProvider alloc] initWithRegionType:AWSRegionUSEast1 identityPoolId:@"us-east-1:2e250934-9456-4acd-a2cd-95f1bca4295d"];
    AWSServiceConfiguration *configuration = [[AWSServiceConfiguration alloc] initWithRegion:AWSRegionUSEast1 credentialsProvider:credentialsProvider];
    AWSServiceManager.defaultServiceManager.defaultServiceConfiguration = configuration;
    
    
    [self handleLocalNotification:[launchOptions objectForKey:UIApplicationLaunchOptionsLocalNotificationKey]];
     [FIRMessaging messaging].delegate = self;
    void (^onUserDidLogin)(NSString *) = ^(NSString *userId) {
        NSLog(@"onUserDidLogin");
        [self.push registerUserNotificationSettings];
        [self initSinchClientWithUserId:userId];
    };
    
    [[NSNotificationCenter defaultCenter]
     addObserverForName:@"UserDidLoginNotification"
     object:nil
     queue:nil
     usingBlock:^(NSNotification *note) {
         NSString *userId = note.userInfo[@"userId"];
         [[NSUserDefaults standardUserDefaults] setObject:userId forKey:@"userId"];
         [[NSUserDefaults standardUserDefaults] synchronize];
         onUserDidLogin(userId);
     }];
    */
    
    // Override point for customization after application launch.
  
   
    [FIRApp configure];
   
    [[FBSDKApplicationDelegate sharedInstance] application:application
                             didFinishLaunchingWithOptions:launchOptions];
    
    [[Client sharedClient] createOrLoadCacheUser];
    [[Client sharedClient] createOrLoadCacheChat];
    [[Client sharedClient] startReachAbility];
    
    [SharedVariables sharedInstance].s3BaseUrl = @"https://s3.amazonaws.com/staticapi.maboapp.com/";
    
    if( SYSTEM_VERSION_LESS_THAN( @"10.0" ) ) {
        [[UIApplication sharedApplication] registerUserNotificationSettings:[UIUserNotificationSettings settingsForTypes:(UIUserNotificationTypeSound |    UIUserNotificationTypeAlert | UIUserNotificationTypeBadge) categories:nil]];
        [[UIApplication sharedApplication] registerForRemoteNotifications];
        
        
    } else {
        UNUserNotificationCenter *center = [UNUserNotificationCenter currentNotificationCenter];
        center.delegate = self;
        [center requestAuthorizationWithOptions:(UNAuthorizationOptionSound | UNAuthorizationOptionAlert | UNAuthorizationOptionBadge) completionHandler:^(BOOL granted, NSError * _Nullable error) {
            if( !error ) {
                // required to get the app to do anything at all about push notifications
                dispatch_async(dispatch_get_main_queue(), ^{
                    [[UIApplication sharedApplication] registerForRemoteNotifications];
                    self.push = [Sinch managedPushWithAPSEnvironment:SINAPSEnvironmentAutomatic];
                    self.push.delegate = self;
                    [self.push setDesiredPushTypeAutomatically];
                    
                    void (^onUserDidLogin)(NSString *) = ^(NSString *userId) {
                        [self.push registerUserNotificationSettings];
                        [self initSinchClientWithUserId:userId];
                        [[SinchClient sharedClient] activeClient];
                    };
                    
                    [[NSNotificationCenter defaultCenter]
                     addObserverForName:@"UserDidLoginNotification"
                     object:nil
                     queue:nil
                     usingBlock:^(NSNotification *note) {
                         NSString *userId = note.userInfo[@"userId"];
                         [[NSUserDefaults standardUserDefaults] setObject:userId forKey:@"userId"];
                         [[NSUserDefaults standardUserDefaults] synchronize];
                         onUserDidLogin(userId);
                     }];
                });
                NSLog( @"Push registration success." );
            } else {
                NSLog( @"Push registration FAILED" );
                NSLog( @"ERROR: %@ - %@", error.localizedFailureReason, error.localizedDescription );
                NSLog( @"SUGGESTIONS: %@ - %@", error.localizedRecoveryOptions, error.localizedRecoverySuggestion );
            }
        }];
    }
    
    
    
    [application registerForRemoteNotifications];
    
    [FIRMessaging messaging].delegate = self;
    
  
    
    AWSCognitoCredentialsProvider *credentialsProvider = [[AWSCognitoCredentialsProvider alloc] initWithRegionType:AWSRegionUSEast1 identityPoolId:@"us-east-1:2e250934-9456-4acd-a2cd-95f1bca4295d"];
    
    AWSServiceConfiguration *configuration = [[AWSServiceConfiguration alloc] initWithRegion:AWSRegionUSEast1 credentialsProvider:credentialsProvider];
    
    AWSServiceManager.defaultServiceManager.defaultServiceConfiguration = configuration;
     [[SinchClient sharedClient] activeClient];
    
    [self handleLocalNotification:[launchOptions objectForKey:UIApplicationLaunchOptionsLocalNotificationKey]];
  // [self requestUserNotificationPermission];
    
 //   NSString *userId = [NSString stringWithFormat:@"%@",[Client sharedClient].user.email];
   // [self initSinchClientWithUserId:userId];
    
//    [[NSNotificationCenter defaultCenter] addObserverForName:@"UserDidLoginNotification"
//                                                      object:nil
//                                                       queue:nil
//                                                  usingBlock:^(NSNotification *note) {
//                                                      [self initSinchClientWithUserId:userId];
//                                                      //[[SinchClient sharedClient] activeClient];
//                                                  }];
    //[[SinchClient sharedClient] activeClient];
    
    return YES;
}

- (BOOL)application:(UIApplication *)application
            openURL:(NSURL *)url
            options:(NSDictionary<UIApplicationOpenURLOptionsKey,id> *)options {
    
    BOOL handled = [[FBSDKApplicationDelegate sharedInstance] application:application
                                                                  openURL:url
                                                        sourceApplication:options[UIApplicationOpenURLOptionsSourceApplicationKey]
                                                               annotation:options[UIApplicationOpenURLOptionsAnnotationKey]
                    ];
    // Add any custom logic here.
    return handled;
}

- (void)application:(UIApplication *)application didReceiveLocalNotification:(UILocalNotification *)notification
{
    [self handleLocalNotification:notification];
}
/*
- (void)application:(UIApplication *)application didReceiveRemoteNotification:(NSDictionary *)userInfo {
    // If you are receiving a notification message while your app is in the background,
    // this callback will not be fired till the user taps on the notification launching the application.
    // TODO: Handle data of notification
    
    // With swizzling disabled you must let Messaging know about the message, for Analytics
    // [[FIRMessaging messaging] appDidReceiveMessage:userInfo];
    
    // Print message ID.
//    if (userInfo[kGCMMessageIDKey]) {
//        NSLog(@"Message ID: %@", userInfo[kGCMMessageIDKey]);
//    }
    // Print full message.
    NSLog(@"%@", userInfo);
//    id WindowRootVC = [[[[UIApplication sharedApplication] delegate] window] rootViewController];
//     id currentViewController = [self findTopViewController:WindowRootVC];
    
//    UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"Main"
//                                                         bundle: nil];
//    CallViewController * callViewController = [storyboard instantiateViewControllerWithIdentifier:@"CallViewController"];
//    callViewController.call = call;
//    [currentViewController presentViewController:callViewController animated:YES completion:nil];
  //  [self performSegueWithIdentifier:@"callView" sender:call];
                       self.client.callClient.delegate = self;
   // [self.push application:application didReceiveRemoteNotification:userInfo];
     [self.push application:application didReceiveRemoteNotification:userInfo];
    
} */


//- (void)application:(UIApplication *)application handleEventsForBackgroundURLSession:(NSString *)identifier
//  completionHandler:(void (^)())completionHandler {
//    /* Store the completion handler.*/
//    [AWSS3TransferUtility interceptApplication:application handleEventsForBackgroundURLSession:identifier completionHandler:completionHandler];
//}

- (void)application:(UIApplication *)application handleEventsForBackgroundURLSession:(NSString *)identifier completionHandler:(void (^)(void))completionHandler{
    
    [AWSS3TransferUtility interceptApplication:application handleEventsForBackgroundURLSession:identifier completionHandler:completionHandler];
}

//- (void)application:(UIApplication *)application didReceiveRemoteNotification:(NSDictionary *)userInfo
//fetchCompletionHandler:(void (^)(UIBackgroundFetchResult))completionHandler {
//
//    [[FIRMessaging messaging] appDidReceiveMessage:userInfo];
//    // If you are receiving a notification message while your app is in the background,
//    // this callback will not be fired till the user taps on the notification launching the application.
//    // TODO: Handle data of notification
//
//    // With swizzling disabled you must let Messaging know about the message, for Analytics
//    // [[FIRMessaging messaging] appDidReceiveMessage:userInfo];
//
//    // Print message ID.
////    if (userInfo[kGCMMessageIDKey]) {
////    /Users/vishnuprasathg/Desktop/Mabo/Mabo/ProfileViewController.m  NSLog(@"Message ID: %@", userInfo[kGCMMessageIDKey]);
////    }
//
//    // Print full message.
//    NSLog(@"%@", userInfo);
//
//    completionHandler(UIBackgroundFetchResultNewData);
//}



- (void)messaging:(FIRMessaging *)messaging didReceiveRegistrationToken:(NSString *)fcmToken {
    NSLog(@"FCM registration token: %@", fcmToken);
    
    NSString *FCMToken = [FIRMessaging messaging].FCMToken;
    NSLog(@"FCM registration token: %@", FCMToken);
    [SharedVariables sharedInstance].fcmToken = FCMToken;
  //  UIView* view = [[UIView alloc] init];
    
    
   // [self sendUpdate:@{@"push_device_token":[SharedVariables sharedInstance].fcmToken} inView:view];
    
    // TODO: If necessary send token to application server.
    // Note: This callback is fired at each app startup and whenever a new token is generated.
}

- (void)applicationWillResignActive:(UIApplication *)application {
    
     [Firebase goOnline];
    // Sent when the application is about to move from active to inactive state. This can occur for certain types of temporary interruptions (such as an incoming phone call or SMS message) or when the user quits the application and it begins the transition to the background state.
    // Use this method to pause ongoing tasks, disable timers, and invalidate graphics rendering callbacks. Games should use this method to pause the game.
}


- (void)applicationDidEnterBackground:(UIApplication *)application {
    
    // Use this method to release shared resources, save user data, invalidate timers, and store enough application state information to restore your application to its current state in case it is terminated later.
    // If your application supports background execution, this method is called instead of applicationWillTerminate: when the user quits.
//    [[SinchClient sharedClient].client stopListeningOnActiveConnection];
//    [[SinchClient sharedClient].client terminate];
//    [SinchClient sharedClient].client = nil;
    
    //if (![SinchClient sharedClient].client) {
        [[SinchClient sharedClient]activeClient];
    //}
    
    [Firebase goOffline];
    [[Client sharedClient] lastSeen:[Client sharedClient].user.email completition:^(NSDictionary *responceDict, NSError *error) {
        
        if(responceDict){
        
        }else{
            NSLog(@"error:%@",error);
        }
    }];
}

-(void)sendUpdate:(NSDictionary *)dictionary inView:(UIView *)updateView{
    NSLog(@"update");
        [[Client sharedClient].user initWithDictionary:dictionary];
        //        CGPoint center = CGPointMake(-20, updateView.frame.size.height/2);
        //        ActivityView *activity = [[ActivityView alloc] initWithFrame:CGRectMake(0, 0, 20, 20)];
        //        [activity setColorBase:COLOR_MB_GREEN];
        //        [activity setColorCircles:COLOR_MB_LIGHTGRAY];
        //        [activity setLineWidthCircles:1];
        //        [updateView addSubview:activity];
        //        [activity setCenterActivity:center];
        //        [activity show];
        NSLog(@"userDict:%@",[Client sharedClient].user.getUserInDictionary);
        
        [[Client sharedClient] updateUserInfoWithCompletition:^(BOOL success, NSError *error) {
            // [activity hide];
            if(success){
                NSLog(@"update ok");
                [[Client sharedClient] getUserInfo:[Client sharedClient].user.email completition:^(NSDictionary *dictionary, NSError *error) {
                    if(dictionary){
                        [[Client sharedClient].user initWithDictionary:dictionary];
                        [[Client sharedClient].user getUserInDictionary];
                        // self.tempImageProfile=nil;
                        // [self refreshImages];
                    }
                    
                }];
            }else if(error){
                NSLog(@"error update: %@",error.description);
                UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"OOPS!" message:NSLocalizedString(@"Error to send update", nil) delegate:self cancelButtonTitle:@"OK" otherButtonTitles:nil];
                [alert show];
                // [self populateContent];   
            }
        }];
    }

- (void)applicationWillEnterForeground:(UIApplication *)application {
    // Called as part of the transition from the background to the active state; here you can undo many of the changes made on entering the background.
    [FBSDKAppEvents activateApp];
    [Firebase goOnline];
}

- (void)applicationDidBecomeActive:(UIApplication *)application {
    [FBSDKAppEvents activateApp];
    [Firebase goOnline];
}


- (void)applicationWillTerminate:(UIApplication *)application {
    
//    [[SinchClient sharedClient].client unregisterPushNotificationData];
//    [[SinchClient sharedClient].client stop];
    
//    [[SinchClient sharedClient].client stopListeningOnActiveConnection];
//    [[SinchClient sharedClient].client terminate];
//    [SinchClient sharedClient].client = nil;
    [Firebase goOffline];
    // Called when the application is about to terminate. Save data if appropriate. See also applicationDidEnterBackground:.
}

#pragma mark -

- (void)initSinchClientWithUserId:(NSString *)userId {
    
     NSLog(@"SinchUSERid:%@",userId);
    
    userId = [NSString stringWithFormat:@"%@",[Client sharedClient].user.email];
    
    if (!_client) {
   
            self.client = [Sinch clientWithApplicationKey:@"fbcb743b-55d6-49fa-8adb-a51a7b8d7b7b"
                                        applicationSecret:@"Xs/gjiD8BUSitNPuE9ag+w=="
                                          environmentHost:@"sandbox.sinch.com"
                                                   userId:userId];
       
         
        _client = [SinchClient sharedClient].client;
        _client.callClient.delegate = self;
        [_client setSupportCalling:YES];
        [_client setSupportMessaging:YES];
        [_client setSupportPushNotifications:YES]; //krishna
        _client.delegate = self;
        [_client start];
        [_client enableManagedPushNotifications];
        [_client startListeningOnActiveConnection];
       //  _callKitProvider = [[SINCallKitProvider alloc] initWithClient:_client];
       // [_client setSupportActiveConnectionInBackground:YES];
    }
}

- (void)handleRemoteNotification:(NSDictionary *)userInfo {
    if (!_client) {
        NSString *userId = [[NSUserDefaults standardUserDefaults] objectForKey:@"userId"];
        if (userId) {
            [self initSinchClientWithUserId:userId];
        }
    }

    id<SINNotificationResult> result = [self.client relayRemotePushNotification:userInfo];

    if ([result isCall] && result.callResult) {
        NSLog(@"HINT: isVideoOffered: %@", @(result.callResult.isVideoOffered));
    }
}

- (void)application:(UIApplication*)application didRegisterForRemoteNotificationsWithDeviceToken:(NSData *)deviceToken
{
    [FIRMessaging messaging].APNSToken = deviceToken;
    [self.push application:application didRegisterForRemoteNotificationsWithDeviceToken:deviceToken];
}

-(void)userNotificationCenter:(UNUserNotificationCenter *)center willPresentNotification:(UNNotification *)notification withCompletionHandler:(void (^)(UNNotificationPresentationOptions options))completionHandler{
    NSLog(@"User Info : %@",notification.request.content.userInfo);
    completionHandler(UNAuthorizationOptionSound | UNAuthorizationOptionAlert | UNAuthorizationOptionBadge);
    
    [self.push application:[UIApplication sharedApplication] didReceiveRemoteNotification:notification.request.content.userInfo];
}

- (void)application:(UIApplication *)application didFailToRegisterForRemoteNotificationsWithError:(NSError *)error {
    NSLog(@"%@:%@", NSStringFromSelector(_cmd), error);
}


- (void)userNotificationCenter:(UNUserNotificationCenter *)center didReceiveNotificationResponse:(UNNotificationResponse *)response withCompletionHandler:(void(^)(void))completionHandler{
    NSLog(@"User Info : %@",response.notification.request.content.userInfo);
    completionHandler();
    [self.push application:[UIApplication sharedApplication] didReceiveRemoteNotification:response.notification.request.content.userInfo];
}


- (void)application:(UIApplication *)application didReceiveRemoteNotification:(NSDictionary *)userInfo {
    [self.push application:application didReceiveRemoteNotification:userInfo];
}

#pragma mark - SINCallClientDelegate

- (void)client:(id<SINCallClient>)client didReceiveIncomingCall:(id<SINCall>)call {
    // Find MainViewController and present CallViewController from it.
    NSLog(@"Incoming call");
    UIViewController *top = self.window.rootViewController;
    while (top.presentedViewController) {
        top = top.presentedViewController;
    }
    [top performSegueWithIdentifier:@"callView" sender:call];
    
    if (call.details.applicationStateWhenReceived == UIApplicationStateActive) {
        // Show an answer button or similar in the UI
    } else {
        // Application was in not in the foreground when the call was initially received,
        // and the user has opened the application (e.g. via a Local Notification),
        // which we then interpret as that the user want to answer the call.
        [call answer];
    }
}

- (SINLocalNotification *)client:(id<SINClient>)client localNotificationForIncomingCall:(id<SINCall>)call {
    NSLog(@"Local notification");
    SINLocalNotification *notification = [[SINLocalNotification alloc] init];
    notification.alertAction = @"Answer";
    notification.alertBody = [NSString stringWithFormat:@"Incoming call from %@", [call remoteUserId]];
    return notification;
}

#pragma mark - SINClientDelegate

- (void)clientDidStart:(id<SINClient>)client {
    NSLog(@"Sinch client started successfully (version: %@)", [Sinch version]);
    [self voipRegistration];
}

- (void)clientDidFail:(id<SINClient>)client error:(NSError *)error {
    NSLog(@"Sinch client error: %@", [error localizedDescription]);
}

#pragma mark - SINManagedPushDelegate

- (void)managedPush:(id<SINManagedPush>)unused
didReceiveIncomingPushWithPayload:(NSDictionary *)payload
            forType:(NSString *)pushType {
    [self handleRemoteNotification:payload];
    [self customLocalNotification:payload];
    
   // id<SINClient> client; // get previously created client
//    [client relayRemotePushNotification:userInfo];
}

-(void)customLocaChatNotification:(NSDictionary *)notificationDict
{
    [customNotification removeFromSuperview];
    
    dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_BACKGROUND, 0), ^{
        // retrive image on global queue
        
        dispatch_async(dispatch_get_main_queue(), ^{
            
            
            customNotification = [[UIView alloc] initWithFrame:CGRectMake(10, 0, self.window.frame.size.width-20, 100)];
            
            customNotification.layer.cornerRadius = 10;
            customNotification.layer.masksToBounds = YES;
            
            customNotification.backgroundColor = [UIColor whiteColor];
            
            //customNotification.backgroundColor = [UIColor colorWithRed:58/255.0 green:63/255.0 blue:116/255.0 alpha:1.0];
            
            [self.window addSubview:customNotification];
            [self.window bringSubviewToFront:customNotification];
            
            notificationImageView = [[UIImageView alloc] initWithFrame:CGRectMake(10, customNotification.frame.size.height/2 - 20, 60, 60)];
            
            UIImage *logoImg = [UIImage imageNamed:@"App-icon-512.png"];
            
            notificationImageView.image = logoImg;
            //notificationImageView.backgroundColor = [UIColor blueColor];
            
            
            //    dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_BACKGROUND, 0), ^{
            //        // retrive image on global queue
            //
            //        NSString* profilePicPath = [NSString stringWithFormat:@"%@",[notificationDict objectForKey:@"picture"]];
            //
            //        NSString * urlstring = [NSString stringWithFormat:@"http://34.230.105.103:8002/%@", profilePicPath];
            //        NSURL *imageURL = [NSURL URLWithString:urlstring];
            //
            //        dispatch_async(dispatch_get_main_queue(), ^{
            //
            //            notificationImageView.layer.cornerRadius = notificationImageView.frame.size.height / 2;
            //            notificationImageView.layer.masksToBounds = YES;
            //            // assign cell image on main thread
            //            [notificationImageView sd_setImageWithURL:imageURL placeholderImage:[UIImage imageNamed:@"avatar2.png"]];
            //            //[cell.textPostProfileImageView setImageWithURL:imageURL placeholderImage:[UIImage imageNamed:@"avatar2.png"]];
            //        });
            //    });
            
            [customNotification addSubview:notificationImageView];
            
            notificationTitleLbl = [[UILabel alloc] initWithFrame:CGRectMake(notificationImageView.frame.origin.x + notificationImageView.frame.size.width + 10, notificationImageView.frame.origin.y + 10, self.window.frame.size.width - 20 - notificationImageView.frame.size.width - 30, 20)];
            
            //notificationTitleLbl.backgroundColor = [UIColor yellowColor];
            
            NSDictionary* header = [notificationDict objectForKey:@"headers"];
            
            NSString* title = [NSString stringWithFormat:@"%@",[header objectForKey:@"extended_name"]];
            
            notificationTitleLbl.text = title;
            
            [notificationTitleLbl setFont:[UIFont systemFontOfSize:16]];
            
            [customNotification addSubview:notificationTitleLbl];
            
            notificationSubTitleLbl = [[UILabel alloc] initWithFrame:CGRectMake(notificationTitleLbl.frame.origin.x , notificationTitleLbl.frame.origin.y + notificationTitleLbl.frame.size.height + 5, notificationTitleLbl.frame.size.width, 20)];
            
            // notificationSubTitleLbl.backgroundColor = [UIColor greenColor];
            
            NSString* subTitle = [NSString stringWithFormat:@"%@",[notificationDict objectForKey:@"text"]];
            
            notificationSubTitleLbl.text = subTitle;
            
            [notificationSubTitleLbl setFont:[UIFont systemFontOfSize:14]];
            
            [customNotification addSubview:notificationSubTitleLbl];
            
            UIButton* notiBtn = [[UIButton alloc] init];
            
            notiBtn.backgroundColor = [UIColor clearColor];
            
            [notiBtn addTarget:self
                        action:@selector(closeNotification)
              forControlEvents:UIControlEventTouchUpInside];
            
            notiBtn.frame = customNotification.frame;
            
            [customNotification addSubview:notiBtn];
            
            // [customView addSubview:sunTitleLbl];
            
            //[self.window addSubview:customView];
            
            CGRect sFrame = customNotification.frame;
            sFrame.origin.x = 10;
            sFrame.origin.y = -200;
            customNotification.frame = sFrame;
            
            [UIView animateWithDuration:1.0f delay:0.0
                                options:UIKeyboardAnimationDurationUserInfoKey.floatValue+1.25f
                             animations:^{
                                 
                                 CGRect frame = customNotification.frame;
                                 frame.origin.x = 10;
                                 frame.origin.y = 0;
                                 customNotification.frame = frame;
                                 
                                 
                             } completion:^(BOOL finished) {
                                 
                                 [UIView animateWithDuration:1.0f delay:1.0
                                                     options:UIKeyboardAnimationDurationUserInfoKey.floatValue+1.25f
                                                  animations:^{
                                                      
                                                      CGRect frame = customNotification.frame;
                                                      frame.origin.x = 10;
                                                      frame.origin.y = -200;
                                                      customNotification.frame = frame;
                                                      
                                                      
                                                  } completion:^(BOOL finished) {
                                                      
                                                    [customNotification removeFromSuperview];
                                                  }];
                                 
                             }];
            
           
        });
    });
    
}

-(void)customLocalNotification:(NSDictionary *)notificationDict
{
    
    [customNotification removeFromSuperview];
    
    dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_BACKGROUND, 0), ^{
        // retrive image on global queue
        
        dispatch_async(dispatch_get_main_queue(), ^{
            
            customNotification = [[UIView alloc] initWithFrame:CGRectMake(10, 0, self.window.frame.size.width-20, 100)];
            
            customNotification.layer.cornerRadius = 10;
            customNotification.layer.masksToBounds = YES;
            
            customNotification.backgroundColor = [UIColor whiteColor];
            
            //customNotification.backgroundColor = [UIColor colorWithRed:58/255.0 green:63/255.0 blue:116/255.0 alpha:1.0];
            
            [self.window addSubview:customNotification];
            [self.window bringSubviewToFront:customNotification];
            
            notificationImageView = [[UIImageView alloc] initWithFrame:CGRectMake(10, customNotification.frame.size.height/2 - 20, 60, 60)];
            
            UIImage *logoImg = [UIImage imageNamed:@"App-icon-512.png"];
            
            notificationImageView.image = logoImg;
            //notificationImageView.backgroundColor = [UIColor blueColor];
            
            
            //    dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_BACKGROUND, 0), ^{
            //        // retrive image on global queue
            //
            //        NSString* profilePicPath = [NSString stringWithFormat:@"%@",[notificationDict objectForKey:@"picture"]];
            //
            //        NSString * urlstring = [NSString stringWithFormat:@"http://34.230.105.103:8002/%@", profilePicPath];
            //        NSURL *imageURL = [NSURL URLWithString:urlstring];
            //
            //        dispatch_async(dispatch_get_main_queue(), ^{
            //
            //            notificationImageView.layer.cornerRadius = notificationImageView.frame.size.height / 2;
            //            notificationImageView.layer.masksToBounds = YES;
            //            // assign cell image on main thread
            //            [notificationImageView sd_setImageWithURL:imageURL placeholderImage:[UIImage imageNamed:@"avatar2.png"]];
            //            //[cell.textPostProfileImageView setImageWithURL:imageURL placeholderImage:[UIImage imageNamed:@"avatar2.png"]];
            //        });
            //    });
            
            [customNotification addSubview:notificationImageView];
            
            notificationTitleLbl = [[UILabel alloc] initWithFrame:CGRectMake(notificationImageView.frame.origin.x + notificationImageView.frame.size.width + 10, notificationImageView.frame.origin.y + 10, self.window.frame.size.width - 20 - notificationImageView.frame.size.width - 30, 20)];
            
            //notificationTitleLbl.backgroundColor = [UIColor yellowColor];
            
            NSString* title = [NSString stringWithFormat:@"%@",[notificationDict objectForKey:@"title"]];
            
            notificationTitleLbl.text = title;
            
            [notificationTitleLbl setFont:[UIFont systemFontOfSize:16]];
            
            [customNotification addSubview:notificationTitleLbl];
            
            notificationSubTitleLbl = [[UILabel alloc] initWithFrame:CGRectMake(notificationTitleLbl.frame.origin.x , notificationTitleLbl.frame.origin.y + notificationTitleLbl.frame.size.height + 5, notificationTitleLbl.frame.size.width, 20)];
            
            // notificationSubTitleLbl.backgroundColor = [UIColor greenColor];
            
            NSString* subTitle = [NSString stringWithFormat:@"%@",[notificationDict objectForKey:@"sub_title"]];
            
            notificationSubTitleLbl.text = subTitle;
            
            [notificationSubTitleLbl setFont:[UIFont systemFontOfSize:14]];
            
            [customNotification addSubview:notificationSubTitleLbl];
            
            
            UIButton* notiBtn = [[UIButton alloc] init];
            
            notiBtn.backgroundColor = [UIColor clearColor];
            
            [notiBtn addTarget:self
                        action:@selector(closeNotification)
             forControlEvents:UIControlEventTouchUpInside];
            
            notiBtn.frame = customNotification.frame;
            
            [customNotification addSubview:notiBtn];
            
            // [customView addSubview:sunTitleLbl];
            
            //[self.window addSubview:customView];
            
            CGRect sFrame = customNotification.frame;
            sFrame.origin.x = 10;
            sFrame.origin.y = -200;
            customNotification.frame = sFrame;
            
            [UIView animateWithDuration:1.0f delay:0.0
                                options:UIKeyboardAnimationDurationUserInfoKey.floatValue+1.25f
                             animations:^{
                                 
                                 CGRect frame = customNotification.frame;
                                 frame.origin.x = 10;
                                 frame.origin.y = 0;
                                 customNotification.frame = frame;
                                 
                                 
                             } completion:^(BOOL finished) {
                                 
                                 [UIView animateWithDuration:1.0f delay:1.0
                                                     options:UIKeyboardAnimationDurationUserInfoKey.floatValue+1.25f
                                                  animations:^{

                                                      CGRect frame = customNotification.frame;
                                                      frame.origin.x = 10;
                                                      frame.origin.y = -200;
                                                      customNotification.frame = frame;


                                                  } completion:^(BOOL finished) {

                                                      [customNotification removeFromSuperview];
                                                  }];
                                 
                             }];
            
            
        });
    });
    
}

- (void)closeNotification
{
    [UIView animateWithDuration:1.0f delay:1.0
                        options:UIKeyboardAnimationDurationUserInfoKey.floatValue+1.25f
                     animations:^{
                         
                         CGRect frame = customNotification.frame;
                         frame.origin.x = 10;
                         frame.origin.y = -200;
                         customNotification.frame = frame;
                         
                         
                     } completion:^(BOOL finished) {
                         
                         for(unsigned i=0;i<[self.window subviews].count;i++){

                             if ([[self.window subviews][i] isKindOfClass:[UIView class]]) {

                                 if (i > 0) {
                                    [[self.window subviews][i] removeFromSuperview];
                                 }

                             }

                         }
                         
                     }];
}

#pragma mark - SINCallClientDelegate

- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    CallViewController *callViewController = [segue destinationViewController];
    callViewController.call = sender;
}
/*
- (void)client:(id<SINCallClient>)client didReceiveIncomingCall:(id<SINCall>)call {
    // Find MainViewController and present CallViewController from it.

    UIViewController *top = self.window.rootViewController;
    while (top.presentedViewController) {
        top = top.presentedViewController;
    }
    
    id WindowRootVC = [[[[UIApplication sharedApplication] delegate] window] rootViewController];
    
    id currentViewController = [self findTopViewController:WindowRootVC];
    

    
    UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"Main"
                                                         bundle: nil];
    //_client.callClient.delegate = self;
    
    call.delegate = self;

    CallViewController * callViewController = [storyboard instantiateViewControllerWithIdentifier:@"CallViewController"];
    callViewController.call = call;
    //[self.navigationController pushViewController:userProfileVC animated:YES];
    [currentViewController presentViewController:callViewController animated:YES completion:nil];
    

    if (call.details.applicationStateWhenReceived == UIApplicationStateActive) {
        // Show an answer button or similar in the UI
    } else {
        // Application was in not in the foreground when the call was initially received,
        // and the user has opened the application (e.g. via a Local Notification),
        // which we then interpret as that the user want to answer the call.
        [call answer];
    }
} */

-(id)findTopViewController:(id)inController
{
    /* if ur using any Customs classes, do like this.
     * Here SlideNavigationController is a subclass of UINavigationController.
     * And ensure you check the custom classes before native controllers , if u have any in your hierarchy.
     if ([inController isKindOfClass:[SlideNavigationController class]])
     {
     return [self findTopViewController:[inController visibleViewController]];
     }
     else */
    
    if ([inController isKindOfClass:[UITabBarController class]])
    {
        return [self findTopViewController:[inController selectedViewController]];
    }
    else if ([inController isKindOfClass:[UINavigationController class]])
    {
        return [self findTopViewController:[inController visibleViewController]];
    }
    else if ([inController isKindOfClass:[UIViewController class]])
    {
        return inController;
    }
    else
    {
        NSLog(@"Unhandled ViewController class : %@",inController);
        return nil;
    }
}





- (void)client:(id<SINClient>)client
    logMessage:(NSString *)message
          area:(NSString *)area
      severity:(SINLogSeverity)severity
     timestamp:(NSDate *)timestamp {
    NSLog(@"MESSAGE:%@", message);
}

- (void)requestUserNotificationPermission {
    
    if ([[UIApplication sharedApplication] respondsToSelector:@selector(registerUserNotificationSettings:)]) {
        UIUserNotificationType types = UIUserNotificationTypeAlert | UIUserNotificationTypeSound;
        UIUserNotificationSettings *settings = [UIUserNotificationSettings settingsForTypes:types categories:nil];
        [[UIApplication sharedApplication] registerUserNotificationSettings:settings];
    }
}

- (void)handleLocalNotification:(UILocalNotification *)notification {
    if (notification) {

        id<SINNotificationResult> result = [self.client relayLocalNotification:notification];

        if ([result isCall] && [[result callResult] isTimedOut]) {
            UIAlertView *alert = [[UIAlertView alloc]
                                  initWithTitle:@"Missed call"
                                  message:[NSString stringWithFormat:@"Missed call from %@", [[result callResult] remoteUserId]]
                                  delegate:nil
                                  cancelButtonTitle:nil
                                  otherButtonTitles:@"OK", nil];
            [alert show];
        }
    }
}

// Register for VoIP notifications
 

-(void)voipRegistration
{
    PKPushRegistry* voipRegistry = [[PKPushRegistry alloc] initWithQueue:dispatch_get_main_queue()];
    voipRegistry.delegate = self;
    voipRegistry.desiredPushTypes = [NSSet setWithObject:PKPushTypeVoIP];
}

-(void)pushRegistry:(PKPushRegistry *)registry didUpdatePushCredentials:(PKPushCredentials *)credentials forType:(NSString *)type
{
    
    [_client registerPushNotificationData:credentials.token];
}
-(void)pushRegistry:(PKPushRegistry *)registry   didInvalidatePushTokenForType:(PKPushType)type{
    
    NSLog(@"invalidated");
    
}


@end
