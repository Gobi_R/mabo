//
//  FbEmailPopUpView.h
//  Mabo
//
//  Created by vishnuprasathg on 11/6/17.
//  Copyright © 2017 DCI. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface FbEmailPopUpView : UIView<UITextFieldDelegate>
@property (weak, nonatomic) IBOutlet UITextField *emailtxtfieldOutlet;
@property (weak, nonatomic) IBOutlet UIButton *submitBtnOutlet;
- (IBAction)submitBtnAction:(id)sender;

@end
