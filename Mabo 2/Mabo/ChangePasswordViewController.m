 //
//  ChangePasswordViewController.m
//  Mobo
//
//  Created by vishnuprasathg on 10/24/17.
//  Copyright © 2017 DCI. All rights reserved.
//

#import "ChangePasswordViewController.h"
#import "Utilities.h"
#import "Client.h"
#import "LoadingView.h"
#import "User.h"

@interface ChangePasswordViewController ()

@property (nonatomic, strong) LoadingView *loading;
@property (nonatomic, strong) User *user;

@end

@implementation ChangePasswordViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    
    self.submitBtnOutlet.layer.cornerRadius = 10;
    self.submitBtnOutlet.layer.masksToBounds = true;
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void)textFieldDidBeginEditing:(UITextField *)textField {
    
}
- (void)textFieldDidEndEditing:(UITextField *)textField {
        
}

-(BOOL) textFieldShouldReturn: (UITextField *) textField
{
    [textField resignFirstResponder];
    
    return YES;
}


/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

- (IBAction)backBtnAction:(id)sender {
    
    [self.navigationController popViewControllerAnimated:YES];
}

- (IBAction)submitBtnAction:(id)sender {
    
    //{"current_password":"pavithra","plainPassword":{"first":"12345678","second":"12345678"}}
    
    [_oldPasswordTxtFieldOutlet resignFirstResponder];
    [_currentPasswordTxtFieldOutlet resignFirstResponder];
    [_confirmPasswordTxtFieldOutlet resignFirstResponder];
    
    if ([_currentPasswordTxtFieldOutlet.text isEqualToString:_confirmPasswordTxtFieldOutlet.text]) {
        
        self.loading = [[LoadingView alloc] initWithFrame:[UIScreen mainScreen].bounds];
        [self.view addSubview:self.loading];
        [self.loading animate:YES];
        
        NSMutableDictionary * plainPwddict = [[NSMutableDictionary alloc] init];
        
        [plainPwddict setObject:_currentPasswordTxtFieldOutlet.text  forKey:@"first"];
        [plainPwddict setObject:_confirmPasswordTxtFieldOutlet.text  forKey:@"second"];
        
        
        NSMutableDictionary*changePwddict = [[NSMutableDictionary alloc] init];
        
        [changePwddict setObject:_oldPasswordTxtFieldOutlet.text  forKey:@"current_password"];
        [changePwddict setObject:plainPwddict  forKey:@"plainPassword"];
        
        NSLog(@"ChangePwdDict:%@",changePwddict);
        
        [[Client sharedClient] changePassword:changePwddict toWallMessageWithId:@"" completition:^(BOOL success, NSError *error) {
            
                if(success){
                    
                    //self.loading = [[LoadingView alloc] initWithFrame:[UIScreen mainScreen].bounds];
                    [self.loading animate:NO];
                NSLog(@"password changed success");
                  //  NSString *password = [[Client sharedClient] getCryptWithPlainPassword:_currentPasswordTxtFieldOutlet.text  andSalt:salt];
                    NSLog(@"salt:%@",[Client sharedClient].user.salt);
                    NSLog(@"pwd:%@",_currentPasswordTxtFieldOutlet.text);
                    
                    NSString *password = [[Client sharedClient] getCryptWithPlainPassword:_currentPasswordTxtFieldOutlet.text andSalt:[Client sharedClient].user.salt];
                    
                    
                    NSDictionary *dictionary = @{
                                                 @"password":password,
                                                 };
                    [[Client sharedClient].user initWithDictionary:dictionary];

                    [[Utilities sharedInstance] showCustomAlertView:self.view title:@"Mabo" message:@"Password has been changed successfully" controller:self reason:@"changePassword"];
                
                }else{
                    [self.loading animate:NO];
                }
            }];

    }
    else
    {
        [[Utilities sharedInstance] showCustomAlertView:self.view title:@"Mabo" message:@"Password and Confirm password does not match." controller:self reason:@"changePasswordFail"];
    }
}
-(void)changePwdAfterSuccess
{
    [self.navigationController popViewControllerAnimated:YES];
}
@end
