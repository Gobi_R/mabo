//
//  CommentsViewController.h
//  Mabo
//
//  Created by vishnuprasathg on 11/13/17.
//  Copyright © 2017 DCI. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface CommentsViewController : UIViewController<UITableViewDelegate, UITableViewDataSource>
@property (weak, nonatomic) IBOutlet UITableView *tableViewOutlet;
- (IBAction)backBtnAction:(id)sender;
- (IBAction)profilePicAction:(id)sender;

@property (nonatomic, retain) NSString * wallIdString;

@end
