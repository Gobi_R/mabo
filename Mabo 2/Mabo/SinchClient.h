//
//  SinchClient.h
//  honk
//
//  Created by Steewe MacBook Pro on 08/01/15.
//  Copyright (c) 2015 Steewe. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <Sinch/Sinch.h>

@interface SinchClient : NSObject <SINClientDelegate, SINMessageClientDelegate, SINCallClientDelegate>

@property (nonatomic, strong) id<SINClient> client;
@property (nonatomic, strong) id<SINMessageClient> messageClient;
@property (strong, nonatomic) UIWindow *window;

+(SinchClient*)sharedClient;

-(void)activeClient;
- (void)deleteSinchSingleton;
-(void)sendMessageText:(NSString *)text toUser:(NSString *)user;

@end
