//
//  MyPostTableViewCell.h
//  Mabo
//
//  Created by vishnuprasathg on 11/8/17.
//  Copyright © 2017 DCI. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface FevouritesTableViewCell : UITableViewCell
@property (weak, nonatomic) IBOutlet UIView *textPostBaseView;
@property (weak, nonatomic) IBOutlet UIImageView *textPostProfileImageView;
@property (weak, nonatomic) IBOutlet UILabel *textPostTitleNamelblOutlet;
@property (weak, nonatomic) IBOutlet UILabel *textPostSubTitleLblOutlet;
@property (weak, nonatomic) IBOutlet UILabel *textPostMomentLblOutlet;
@property (weak, nonatomic) IBOutlet UITextView *textPostTextViewOutlet;
@property (weak, nonatomic) IBOutlet UILabel *textPostLikedThisLblOutlet;
@property (weak, nonatomic) IBOutlet UILabel *textPostWillBeThereLblOutlet;
@property (weak, nonatomic) IBOutlet UILabel *textPostCommentsLblOutlet;
@property (weak, nonatomic) IBOutlet UIButton *textPostProfileBtnOutlet;
@property (weak, nonatomic) IBOutlet UIButton *textPostLocationBtnOutlet;
@property (weak, nonatomic) IBOutlet UIButton *textPostFevoriteBtnOutlet;
@property (weak, nonatomic) IBOutlet UIButton *textPostCommentBtnOutlet;
@property (weak, nonatomic) IBOutlet UIButton *textPostDeleteBtnOutlet;
@property (weak, nonatomic) IBOutlet UIImageView *textPostImageViewOutlet;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *textPostImageViewHeightConstraint;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *textPostBaseViewHeightConstraint;
@property (weak, nonatomic) IBOutlet UIImageView *textPostPlayImageView;
@property (weak, nonatomic) IBOutlet UIImageView *textPostLikeImageViewOutlet;
@property (weak, nonatomic) IBOutlet UILabel *textPostDistanceLblOutlet;
@property (weak, nonatomic) IBOutlet UILabel *testPostImagesCountLblOutlet;

- (IBAction)textPostProfilePicBtnAction:(id)sender;
- (IBAction)textPostDeleteBtnAction:(id)sender;
- (IBAction)textPostLocationBtnAction:(id)sender;
- (IBAction)textPostFevoriteBtnAction:(id)sender;
- (IBAction)textPostCommentsBtnAction:(id)sender;


@property (weak, nonatomic) IBOutlet UILabel *eventPostImagesCountLblOutlet;
@property (weak, nonatomic) IBOutlet UIView *eventPostBaseView;
@property (weak, nonatomic) IBOutlet UIImageView *eventPostProfileImageView;
@property (weak, nonatomic) IBOutlet UILabel *eventPostTitleNameLblOutlet;
@property (weak, nonatomic) IBOutlet UILabel *eventPostSubtitleLblOutlet;
@property (weak, nonatomic) IBOutlet UILabel *eventPostMomentsLblOutlet;
@property (weak, nonatomic) IBOutlet UILabel *eventPostQuickNotLblOutlet;
@property (weak, nonatomic) IBOutlet UILabel *eventPostDateLblOutlet;
@property (weak, nonatomic) IBOutlet UITextView *eventPostTextViewOutlet;
@property (weak, nonatomic) IBOutlet UILabel *eventPostWillBeThereLblOutlet;
@property (weak, nonatomic) IBOutlet UILabel *eventPostLikedThisLblOutlet;
@property (weak, nonatomic) IBOutlet UILabel *eventPostCommentsLblOutlet;
@property (weak, nonatomic) IBOutlet UIButton *eventPostProfilePicBtnOutlet;
@property (weak, nonatomic) IBOutlet UIButton *eventPostEditBtnOutlet;
@property (weak, nonatomic) IBOutlet UIButton *eventPostDeleteBtnOutlet;
@property (weak, nonatomic) IBOutlet UIButton *eventPostJoiningBtnOutlet;
@property (weak, nonatomic) IBOutlet UIButton *eventPostMaybeBtnOutlet;
@property (weak, nonatomic) IBOutlet UIButton *eventPostPriceBtnOutlet;
@property (weak, nonatomic) IBOutlet UIButton *eventPostLocationBtnOutlet;
@property (weak, nonatomic) IBOutlet UIButton *eventPostFevoriteBtnOutlet;
@property (weak, nonatomic) IBOutlet UIButton *eventPostCommentsBtnOutlet;
@property (weak, nonatomic) IBOutlet UIImageView *eventPostImageViewOutlet;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *eventPostImageViewHeightConstraint;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *eventPostBaseViewHeightConstraint;
@property (weak, nonatomic) IBOutlet UIImageView *eventPostPlayImageView;
@property (weak, nonatomic) IBOutlet UIImageView *eventPostLikeImageViewOutlet;
@property (weak, nonatomic) IBOutlet UILabel *eventPostDistanceLblOutlet;
@property (weak, nonatomic) IBOutlet UIView *eventPostJoiningBaseView;
@property (weak, nonatomic) IBOutlet UIView *eventPostMaybeBaseView;

- (IBAction)eventPostProfilePicBtnAction:(id)sender;
- (IBAction)eventPostEditBtnAction:(id)sender;
- (IBAction)eventPostDeleteBtnAction:(id)sender;
- (IBAction)eventPostJoiningBtnAction:(id)sender;
- (IBAction)eventPostMaybeBtnAction:(id)sender;
- (IBAction)eventPostPriceBtnAction:(id)sender;
- (IBAction)eventPostLocationBtnAction:(id)sender;
- (IBAction)eventPostFevoritesBtnAction:(id)sender;
- (IBAction)eventPostCommentsBtnAction:(id)sender;



@end
