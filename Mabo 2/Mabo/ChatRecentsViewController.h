//
//  ChatRecentsViewController.h
//  Mabo
//
//  Created by vishnuprasathg on 11/23/17.
//  Copyright © 2017 DCI. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface ChatRecentsViewController : UIViewController
- (IBAction)segmentValueChanged:(id)sender;
@property (weak, nonatomic) IBOutlet UISegmentedControl *segmentOutlet;
- (IBAction)plusBtnAction:(id)sender;
@property (weak, nonatomic) IBOutlet UITableView *tableViewOutlet;
- (IBAction)profileBtnAction:(id)sender;
@property (weak, nonatomic) IBOutlet UIButton *logBtnOutlet;

-(void)populateCell:(NSDictionary *)dictionary tableCell:(UITableViewCell *)cell;
-(void)updateBadge:(int)number tableCell:(UITableViewCell *)cell;
-(void)lunchObserv :(UITableViewCell *)cell;

-(void)reloadContent;
-(void)receiveCacheUpload:(NSNotification *)notification;
- (IBAction)logBtnAction:(id)sender;

@end
