//
//  HKAnnotationClusterView.h
//  honk
//
//  Created by Steewe MacBook Pro on 25/02/15.
//  Copyright (c) 2015 Steewe. All rights reserved.
//

#import <MapKit/MapKit.h>
#import "Annotation.h"

@interface AnnotationClusterView : MKAnnotationView

-(void)refreshWithAnnotation:(Annotation *)annotation;

@end
