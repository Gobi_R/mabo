//
//  TutorialViewController.m
//  Mobo
//
//  Created by vishnuprasathg on 10/12/17.
//  Copyright © 2017 DCI. All rights reserved.
//

#import "TutorialViewController.h"
#import "SignInViewController.h"

@interface TutorialViewController ()

@end

#define screenSize [[UIScreen mainScreen] bounds]
@implementation TutorialViewController
{
    UIPinchGestureRecognizer *recognizerr;
    UISwipeGestureRecognizer *swipeUp;
    NSString* page;
    CGRect firstImageFrame;
    float Fheight;
    float Fwidth;
}

- (void)viewDidLoad {
    [super viewDidLoad];
    
    page = @"1";
    
    self.firstLbl.backgroundColor = [UIColor colorWithRed:127/255.0 green:230/255.0 blue:233/255.0 alpha:1.0];
    self.secondLbl.backgroundColor = [UIColor colorWithRed:90/255.0 green:98/255.0 blue:152/255.0 alpha:1.0];
    self.thirdLbl.backgroundColor = [UIColor colorWithRed:90/255.0 green:98/255.0 blue:152/255.0 alpha:1.0];
    self.forthLbl.backgroundColor = [UIColor colorWithRed:90/255.0 green:98/255.0 blue:152/255.0 alpha:1.0];
    
    self.firstLbl.layer.masksToBounds = YES;
    self.firstLbl.layer.cornerRadius = self.firstLbl.frame.size.height/2;
    self.secondLbl.layer.masksToBounds = YES;
    self.secondLbl.layer.cornerRadius = self.secondLbl.frame.size.height/2;
    self.thirdLbl.layer.masksToBounds = YES;
    self.thirdLbl.layer.cornerRadius = self.thirdLbl.frame.size.height/2;
    self.forthLbl.layer.masksToBounds = YES;
    self.forthLbl.layer.cornerRadius = self.thirdLbl.frame.size.height/2;
    
    self.firstLbl.layer.borderWidth = 0.5;
    self.firstLbl.layer.borderColor = [UIColor blackColor].CGColor;
    
    self.secondLbl.layer.borderWidth = 0.5;
    self.secondLbl.layer.borderColor = [UIColor blackColor].CGColor;
    
    self.thirdLbl.layer.borderWidth = 0.5;
    self.thirdLbl.layer.borderColor = [UIColor blackColor].CGColor;
    
    self.forthLbl.layer.borderWidth = 0.5;
    self.forthLbl.layer.borderColor = [UIColor blackColor].CGColor;
    
    [self.navigationController setNavigationBarHidden:YES animated:NO];
    
    NSLog(@"firstImageView:%@",self.firstImageView);
    
    UITapGestureRecognizer *tapGesture = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(handleTap:)];
    tapGesture.delegate = self;
    tapGesture.numberOfTapsRequired = 1;
    self.firstImageView.userInteractionEnabled = YES;
    [self.firstImageView addGestureRecognizer:tapGesture];
    
    UITapGestureRecognizer *tapGesture1 = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(handleTap:)];
    tapGesture1.delegate = self;
    tapGesture1.numberOfTapsRequired = 1;
    self.secondImageView.userInteractionEnabled = YES;
    [self.secondImageView addGestureRecognizer:tapGesture1];
    
    UITapGestureRecognizer *tapGesture2 = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(handleTap:)];
    tapGesture2.delegate = self;
    tapGesture2.numberOfTapsRequired = 1;
    self.thirdImageView.userInteractionEnabled = YES;
    [self.thirdImageView addGestureRecognizer:tapGesture2];
    
    UITapGestureRecognizer *tapGesture3 = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(handleTap:)];
    tapGesture3.delegate = self;
    tapGesture3.numberOfTapsRequired = 1;
    self.forthImageView.userInteractionEnabled = YES;
    [self.forthImageView addGestureRecognizer:tapGesture3];
    
    
    UISwipeGestureRecognizer *swipeleft = [[UISwipeGestureRecognizer alloc] initWithTarget:self action:@selector(handleSwipe:)];
    swipeleft.delegate = self;
    [swipeleft setDirection:UISwipeGestureRecognizerDirectionLeft];
    [self.firstImageView addGestureRecognizer:swipeleft];
    
    
    UISwipeGestureRecognizer *swipe1left = [[UISwipeGestureRecognizer alloc] initWithTarget:self action:@selector(handleSwipe:)];
    swipe1left.delegate = self;
    [swipe1left setDirection:UISwipeGestureRecognizerDirectionLeft];
    [self.secondImageView addGestureRecognizer:swipe1left];
    
    
    UISwipeGestureRecognizer *swipe2left = [[UISwipeGestureRecognizer alloc] initWithTarget:self action:@selector(handleSwipe:)];
    swipe2left.delegate = self;
    [swipe2left setDirection:UISwipeGestureRecognizerDirectionLeft];
    [self.thirdImageView addGestureRecognizer:swipe2left];
    
    UISwipeGestureRecognizer *swipe3left = [[UISwipeGestureRecognizer alloc] initWithTarget:self action:@selector(handleSwipe:)];
    swipe3left.delegate = self;
    [swipe3left setDirection:UISwipeGestureRecognizerDirectionLeft];
    [self.forthImageView addGestureRecognizer:swipe3left];
    
    
    UISwipeGestureRecognizer *swipeRight = [[UISwipeGestureRecognizer alloc] initWithTarget:self action:@selector(handleSwipe:)];
    swipeRight.delegate = self;
    [swipeRight setDirection:UISwipeGestureRecognizerDirectionRight];
    [self.firstImageView addGestureRecognizer:swipeRight];
    
    
    UISwipeGestureRecognizer *swipe1Right = [[UISwipeGestureRecognizer alloc] initWithTarget:self action:@selector(handleSwipe:)];
    swipe1Right.delegate = self;
    [swipe1Right setDirection:UISwipeGestureRecognizerDirectionRight];
    [self.secondImageView addGestureRecognizer:swipe1Right];
    
    
    UISwipeGestureRecognizer *swipe2Right = [[UISwipeGestureRecognizer alloc] initWithTarget:self action:@selector(handleSwipe:)];
    swipe2Right.delegate = self;
    [swipe2Right setDirection:UISwipeGestureRecognizerDirectionRight];
    [self.thirdImageView addGestureRecognizer:swipe2Right];
    
    UISwipeGestureRecognizer *swipe3Right = [[UISwipeGestureRecognizer alloc] initWithTarget:self action:@selector(handleSwipe:)];
    swipe3Right.delegate = self;
    [swipe3Right setDirection:UISwipeGestureRecognizerDirectionRight];
    [self.forthImageView addGestureRecognizer:swipe3Right];

    
    CGRect sFrame = self.secondImageView.frame;
    sFrame.origin.x = screenSize.size.width + 200;
    sFrame.origin.y = self.firstImageView.frame.origin.y;
    sFrame.size.width = self.firstImageView.frame.size.width;
    sFrame.size.height = self.firstImageView.frame.size.height;
    self.secondImageView.frame = sFrame;
    
    CGRect tFrame = self.thirdImageView.frame;
    tFrame.origin.x = screenSize.size.width + 200;
    tFrame.origin.y = self.firstImageView.frame.origin.y;
    tFrame.size.width = self.firstImageView.frame.size.width;
    tFrame.size.height = self.firstImageView.frame.size.height;
    self.thirdImageView.frame = tFrame;
    
    
    CGRect fFrame = self.forthImageView.frame;
    fFrame.origin.x = screenSize.size.width + 200;
    fFrame.origin.y = self.firstImageView.frame.origin.y;
    fFrame.size.width = self.firstImageView.frame.size.width;
    fFrame.size.height = self.firstImageView.frame.size.height;
    self.forthImageView.frame = fFrame;
    
    [self.imagesBaseView addSubview:_secondImageView];
    [self.imagesBaseView addSubview:_thirdImageView];
    [self.imagesBaseView addSubview:_forthImageView];
    
    Fheight = self.firstImageView.frame.size.height;
    Fwidth = self.firstImageView.frame.size.width;
    
    NSLog(@"%f",Fheight);
    NSLog(@"%f",Fwidth);
    
    [self.previousBtnOutlet bringSubviewToFront:self.imagesBaseView];
    [self.nextBtnOutlet bringSubviewToFront:self.imagesBaseView];
    
    //self.imagesBaseView.backgroundColor = [UIColor redColor];
    
    //[self.view addSubview: self.firstImageView];
    
}
-(void)handleTap: (UITapGestureRecognizer *) recognizer{

    NSLog(@"tappppppp");
    NSLog(@"TAPPage:%@",page);
    
    if ([page  isEqual: @"1"]) {
        
        CGRect sFrame = self.secondImageView.frame;
        sFrame.origin.x = screenSize.size.width + 500;
        self.secondImageView.frame = sFrame;
        
        [UIView animateWithDuration:0.5f delay:0.0
                            options:UIKeyboardAnimationDurationUserInfoKey.floatValue+0.25f
                         animations:^{
                             
                             CGRect frame = self.firstImageView.frame;
                             frame.origin.x = -500;
                             self.firstImageView.frame = frame;
                             
                             CGRect sFrame = self.secondImageView.frame;
                             sFrame.origin.x = 16;
                             
                             sFrame.size.width = _imagesBaseView.frame.size.width - 32;
                             sFrame.size.height = _imagesBaseView.frame.size.height;
                             self.secondImageView.frame = sFrame;
                             
                         } completion:^(BOOL finished) {
                             
                             page = @"2";
                             
                             self.firstLbl.backgroundColor = [UIColor colorWithRed:90/255.0 green:98/255.0 blue:152/255.0 alpha:1.0];
                             self.secondLbl.backgroundColor = [UIColor colorWithRed:127/255.0 green:230/255.0 blue:233/255.0 alpha:1.0];
                             self.thirdLbl.backgroundColor = [UIColor colorWithRed:90/255.0 green:98/255.0 blue:152/255.0 alpha:1.0];
                             
                             [self.previousBtnOutlet bringSubviewToFront:self.imagesBaseView];
                             [self.nextBtnOutlet bringSubviewToFront:self.imagesBaseView];

                             
                         }];
        
    }
    else if ([page  isEqual: @"2"]) {
        
        CGRect sFrame = self.thirdImageView.frame;
        sFrame.origin.x = screenSize.size.width + 500;
        self.thirdImageView.frame = sFrame;
        
        [UIView animateWithDuration:0.5f delay:0.0
                            options:UIKeyboardAnimationDurationUserInfoKey.floatValue+0.25f
                         animations:^{
                             
                             NSLog(@"%@",screenSize);
                             
                             CGRect frame = self.secondImageView.frame;
                             frame.origin.x = -500;
                             self.secondImageView.frame = frame;
                             
                             CGRect sFrame = self.thirdImageView.frame;
                             sFrame.origin.x = 16;
                             
                             sFrame.size.width = _imagesBaseView.frame.size.width - 32;
                             sFrame.size.height = _imagesBaseView.frame.size.height;
                             self.thirdImageView.frame = sFrame;
                             
                         } completion:^(BOOL finished) {
                             
                             page = @"3";
                             
                             self.firstLbl.backgroundColor = [UIColor colorWithRed:90/255.0 green:98/255.0 blue:152/255.0 alpha:1.0];
                             self.secondLbl.backgroundColor = [UIColor colorWithRed:90/255.0 green:98/255.0 blue:152/255.0 alpha:1.0];
                             self.thirdLbl.backgroundColor = [UIColor colorWithRed:127/255.0 green:230/255.0 blue:233/255.0 alpha:1.0];
                             
                             [self.previousBtnOutlet bringSubviewToFront:self.imagesBaseView];
                             [self.nextBtnOutlet bringSubviewToFront:self.imagesBaseView];
                             
                         }];
        
    }
    
    else if ([page  isEqual: @"3"]) {
        
        CGRect sFrame = self.forthImageView.frame;
        sFrame.origin.x = screenSize.size.width + 500;
        self.forthImageView.frame = sFrame;
        
        [UIView animateWithDuration:0.5f delay:0.0
                            options:UIKeyboardAnimationDurationUserInfoKey.floatValue+0.25f
                         animations:^{
                             
                             NSLog(@"%@",screenSize);
                             
                             CGRect frame = self.thirdImageView.frame;
                             frame.origin.x = -500;
                             self.thirdImageView.frame = frame;
                             
                             CGRect sFrame = self.forthImageView.frame;
                             sFrame.origin.x = 16;
                             
                             sFrame.size.width = _imagesBaseView.frame.size.width - 32;
                             sFrame.size.height = _imagesBaseView.frame.size.height;
                             self.forthImageView.frame = sFrame;
                             
                         } completion:^(BOOL finished) {
                             
                             page = @"4";
                             
                             self.firstLbl.backgroundColor = [UIColor colorWithRed:90/255.0 green:98/255.0 blue:152/255.0 alpha:1.0];
                             self.secondLbl.backgroundColor = [UIColor colorWithRed:90/255.0 green:98/255.0 blue:152/255.0 alpha:1.0];
                             self.thirdLbl.backgroundColor = [UIColor colorWithRed:90/255.0 green:98/255.0 blue:152/255.0 alpha:1.0];
                             self.forthLbl.backgroundColor = [UIColor colorWithRed:127/255.0 green:230/255.0 blue:233/255.0 alpha:1.0];
                             
                             [self.previousBtnOutlet bringSubviewToFront:self.imagesBaseView];
                             [self.nextBtnOutlet bringSubviewToFront:self.imagesBaseView];
                             
                         }];
        
    }
    else if ([page  isEqual: @"4"]) {
        
        SignInViewController * signInVC = [self.storyboard instantiateViewControllerWithIdentifier:@"SignInViewController"];
        [self.navigationController pushViewController:signInVC animated:YES];
        
    }
}

- (void)handleSwipe:(UISwipeGestureRecognizer *)swipe {
    
    NSLog(@"SWIPEPage:%@",page);
    
    if (swipe.direction == UISwipeGestureRecognizerDirectionRight) {
        NSLog(@"ri");
        
        if ([page  isEqual: @"1"]) {
            
            
        }
        if ([page  isEqual: @"2"]) {
            
                        CGRect sFrame = self.firstImageView.frame;
                        sFrame.origin.x = -500;
                        self.firstImageView.frame = sFrame;
            
                        [UIView animateWithDuration:0.5f delay:0.0
                                            options:UIKeyboardAnimationDurationUserInfoKey.floatValue+0.25f
                                         animations:^{
            
                                             CGRect frame = self.secondImageView.frame;
                                             frame.origin.x = screenSize.size.width + 500;
                                             self.secondImageView.frame = frame;
            
                                             CGRect sFrame = self.firstImageView.frame;
                                             sFrame.origin.x = 16;
                                             sFrame.size.width = _imagesBaseView.frame.size.width - 32;
                                             sFrame.size.height = _imagesBaseView.frame.size.height;
                                             self.firstImageView.frame = sFrame;
            
                                         } completion:^(BOOL finished) {
                                             
                                             page = @"1";
                                             
                                             self.firstLbl.backgroundColor = [UIColor colorWithRed:127/255.0 green:230/255.0 blue:233/255.0 alpha:1.0];
                                             self.secondLbl.backgroundColor = [UIColor colorWithRed:90/255.0 green:98/255.0 blue:152/255.0 alpha:1.0];
                                             self.thirdLbl.backgroundColor = [UIColor colorWithRed:90/255.0 green:98/255.0 blue:152/255.0 alpha:1.0];
                                             self.forthLbl.backgroundColor = [UIColor colorWithRed:90/255.0 green:98/255.0 blue:152/255.0 alpha:1.0];
                                             
                                             [self.previousBtnOutlet bringSubviewToFront:self.imagesBaseView];
                                             [self.nextBtnOutlet bringSubviewToFront:self.imagesBaseView];
                                             
                                         }];
            
            
        }
        
        if ([page  isEqual: @"3"])
        {
            CGRect sFrame = self.secondImageView.frame;
            sFrame.origin.x = -500;
            self.secondImageView.frame = sFrame;
            
            [UIView animateWithDuration:0.5f delay:0.0
                                options:UIKeyboardAnimationDurationUserInfoKey.floatValue+0.25f
                             animations:^{
                                 
                                 CGRect frame = self.thirdImageView.frame;
                                 frame.origin.x = screenSize.size.width + 500;
                                 self.thirdImageView.frame = frame;
                                 
                                 CGRect sFrame = self.secondImageView.frame;
                                 sFrame.origin.x = 16;
                                 sFrame.size.width = _imagesBaseView.frame.size.width - 32;
                                 sFrame.size.height = _imagesBaseView.frame.size.height;
                                 self.secondImageView.frame = sFrame;
                                 
                             } completion:^(BOOL finished) {
                                 
                                 page = @"2";
                                 
                                 self.firstLbl.backgroundColor = [UIColor colorWithRed:90/255.0 green:98/255.0 blue:152/255.0 alpha:1.0];
                                 self.secondLbl.backgroundColor = [UIColor colorWithRed:127/255.0 green:230/255.0 blue:233/255.0 alpha:1.0];
                                 self.thirdLbl.backgroundColor = [UIColor colorWithRed:90/255.0 green:98/255.0 blue:152/255.0 alpha:1.0];
                                 self.forthLbl.backgroundColor = [UIColor colorWithRed:90/255.0 green:98/255.0 blue:152/255.0 alpha:1.0];
                                 
                                 [self.previousBtnOutlet bringSubviewToFront:self.imagesBaseView];
                                 [self.nextBtnOutlet bringSubviewToFront:self.imagesBaseView];
                                 
                             }];
            
        }
        if ([page  isEqual: @"4"])
        {
            
            
            CGRect sFrame = self.thirdImageView.frame;
            sFrame.origin.x = -500;
            self.thirdImageView.frame = sFrame;
            
            [UIView animateWithDuration:0.5f delay:0.0
                                options:UIKeyboardAnimationDurationUserInfoKey.floatValue+0.25f
                             animations:^{
                                 
                                 CGRect frame = self.forthImageView.frame;
                                 frame.origin.x = screenSize.size.width + 500;
                                 self.forthImageView.frame = frame;
                                 
                                 CGRect sFrame = self.thirdImageView.frame;
                                 sFrame.origin.x = 16;
                                 sFrame.size.width = _imagesBaseView.frame.size.width - 32;
                                 sFrame.size.height = _imagesBaseView.frame.size.height;
                                 self.thirdImageView.frame = sFrame;
                                 
                             } completion:^(BOOL finished) {
                                 
                                 page = @"3";
                                 
                                 self.firstLbl.backgroundColor = [UIColor colorWithRed:90/255.0 green:98/255.0 blue:152/255.0 alpha:1.0];
                                 self.secondLbl.backgroundColor = [UIColor colorWithRed:90/255.0 green:98/255.0 blue:152/255.0 alpha:1.0];
                                 self.thirdLbl.backgroundColor = [UIColor colorWithRed:127/255.0 green:230/255.0 blue:233/255.0 alpha:1.0];
                                 self.forthLbl.backgroundColor = [UIColor colorWithRed:90/255.0 green:98/255.0 blue:152/255.0 alpha:1.0];
                                 
                                 [self.previousBtnOutlet bringSubviewToFront:self.imagesBaseView];
                                 [self.nextBtnOutlet bringSubviewToFront:self.imagesBaseView];
                                 
                             }];
            
        }
        
    }
    
    if (swipe.direction == UISwipeGestureRecognizerDirectionLeft) {
        NSLog(@"ri");
        
        if ([page  isEqual: @"1"]) {
            
            CGRect sFrame = self.secondImageView.frame;
            sFrame.origin.x = screenSize.size.width + 500;
            self.secondImageView.frame = sFrame;
            
            [UIView animateWithDuration:0.5f delay:0.0
                                options:UIKeyboardAnimationDurationUserInfoKey.floatValue+0.25f
                             animations:^{
                                 
                                 CGRect frame = self.firstImageView.frame;
                                 frame.origin.x = -500;
                                 self.firstImageView.frame = frame;
                                 
                                 CGRect sFrame = self.secondImageView.frame;
                                 sFrame.origin.x = 16;
                                 sFrame.size.width = _imagesBaseView.frame.size.width - 32;
                                 sFrame.size.height = _imagesBaseView.frame.size.height;
                                 self.secondImageView.frame = sFrame;
                                 
                             } completion:^(BOOL finished) {
                                 
                                 page = @"2";
                                 
                                 self.firstLbl.backgroundColor = [UIColor colorWithRed:90/255.0 green:98/255.0 blue:152/255.0 alpha:1.0];
                                 self.secondLbl.backgroundColor = [UIColor colorWithRed:127/255.0 green:230/255.0 blue:233/255.0 alpha:1.0];
                                 self.thirdLbl.backgroundColor = [UIColor colorWithRed:90/255.0 green:98/255.0 blue:152/255.0 alpha:1.0];
                                 self.forthLbl.backgroundColor = [UIColor colorWithRed:90/255.0 green:98/255.0 blue:152/255.0 alpha:1.0];
                                 
                                 [self.previousBtnOutlet bringSubviewToFront:self.imagesBaseView];
                                 [self.nextBtnOutlet bringSubviewToFront:self.imagesBaseView];
                                 
                             }];
            
        }
        if ([page  isEqual: @"2"]) {
            
            
            CGRect sFrame = self.thirdImageView.frame;
            sFrame.origin.x = screenSize.size.width + 500;
            self.thirdImageView.frame = sFrame;
            
            [UIView animateWithDuration:0.5f delay:0.0
                                options:UIKeyboardAnimationDurationUserInfoKey.floatValue+0.25f
                             animations:^{
                                 
                                 CGRect frame = self.secondImageView.frame;
                                 frame.origin.x = -500;
                                 self.secondImageView.frame = frame;
                                 
                                 CGRect sFrame = self.thirdImageView.frame;
                                 sFrame.origin.x = 16;
                                 sFrame.size.width = _imagesBaseView.frame.size.width - 32;
                                 sFrame.size.height = _imagesBaseView.frame.size.height;
                                 self.thirdImageView.frame = sFrame;
                                 
                             } completion:^(BOOL finished) {
                                 
                                 page = @"3";
                                 
                                 self.firstLbl.backgroundColor = [UIColor colorWithRed:90/255.0 green:98/255.0 blue:152/255.0 alpha:1.0];
                                 self.secondLbl.backgroundColor = [UIColor colorWithRed:90/255.0 green:98/255.0 blue:152/255.0 alpha:1.0];
                                 self.thirdLbl.backgroundColor = [UIColor colorWithRed:127/255.0 green:230/255.0 blue:233/255.0 alpha:1.0];
                                 self.forthLbl.backgroundColor = [UIColor colorWithRed:90/255.0 green:98/255.0 blue:152/255.0 alpha:1.0];
                                 
                                 [self.previousBtnOutlet bringSubviewToFront:self.imagesBaseView];
                                 [self.nextBtnOutlet bringSubviewToFront:self.imagesBaseView];
                                 
                             }];

            
        }
        
        if ([page  isEqual: @"3"])
        {
            
            CGRect sFrame = self.forthImageView.frame;
            sFrame.origin.x = screenSize.size.width + 500;
            self.forthImageView.frame = sFrame;
            
            [UIView animateWithDuration:0.5f delay:0.0
                                options:UIKeyboardAnimationDurationUserInfoKey.floatValue+0.25f
                             animations:^{
                                 
                                 CGRect frame = self.thirdImageView.frame;
                                 frame.origin.x = -500;
                                 self.thirdImageView.frame = frame;
                                 
                                 CGRect sFrame = self.forthImageView.frame;
                                 sFrame.origin.x = 16;
                                 sFrame.size.width = _imagesBaseView.frame.size.width - 32;
                                 sFrame.size.height = _imagesBaseView.frame.size.height;
                                 self.forthImageView.frame = sFrame;
                                 
                             } completion:^(BOOL finished) {
                                 
                                 page = @"4";
                                 
                                 self.firstLbl.backgroundColor = [UIColor colorWithRed:90/255.0 green:98/255.0 blue:152/255.0 alpha:1.0];
                                 self.secondLbl.backgroundColor = [UIColor colorWithRed:90/255.0 green:98/255.0 blue:152/255.0 alpha:1.0];
                                 self.thirdLbl.backgroundColor = [UIColor colorWithRed:90/255.0 green:98/255.0 blue:152/255.0 alpha:1.0];
                                 
                                 self.forthLbl.backgroundColor = [UIColor colorWithRed:127/255.0 green:230/255.0 blue:233/255.0 alpha:1.0];
                                 
                                 [self.previousBtnOutlet bringSubviewToFront:self.imagesBaseView];
                                 [self.nextBtnOutlet bringSubviewToFront:self.imagesBaseView];
                                 
                             }];
        }
        
    }
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

- (IBAction)previousBtnAction:(id)sender {
}

- (IBAction)nextBtnAction:(id)sender {
}

- (IBAction)skipBtnAction:(id)sender {
    
    SignInViewController * signInVC = [self.storyboard instantiateViewControllerWithIdentifier:@"SignInViewController"];
    [self.navigationController pushViewController:signInVC animated:YES];
    
}
@end
