//
//  TutorialViewController.h
//  Mobo
//
//  Created by vishnuprasathg on 10/12/17.
//  Copyright © 2017 DCI. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface TutorialViewController : UIViewController<UIGestureRecognizerDelegate>
@property (weak, nonatomic) IBOutlet UIImageView *firstImageView;
@property (strong, nonatomic) IBOutlet UIImageView *secondImageView;
@property (strong, nonatomic) IBOutlet UIImageView *thirdImageView;
@property (weak, nonatomic) IBOutlet UIView *imagesBaseView;
@property (strong, nonatomic) IBOutlet UIImageView *forthImageView;
@property (weak, nonatomic) IBOutlet UILabel *forthLbl;

@property (weak, nonatomic) IBOutlet UILabel *firstLbl;
@property (weak, nonatomic) IBOutlet UILabel *secondLbl;
@property (weak, nonatomic) IBOutlet UILabel *thirdLbl;
@property (weak, nonatomic) IBOutlet UIButton *previousBtnOutlet;
@property (weak, nonatomic) IBOutlet UIButton *nextBtnOutlet;
- (IBAction)previousBtnAction:(id)sender;
- (IBAction)nextBtnAction:(id)sender;
- (IBAction)skipBtnAction:(id)sender;
@end
