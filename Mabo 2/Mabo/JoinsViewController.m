//
//  JoinsViewController.m
//  Mabo
//
//  Created by vishnuprasathg on 11/13/17.
//  Copyright © 2017 DCI. All rights reserved.
//

#import "JoinsViewController.h"
#import "Client.h"
#import "SharedVariables.h"
#import "LoadingView.h"
#import "UIImageView+WebCache.h"
#import "UserProfileViewController.h"

@interface JoinsViewController ()

@property (nonatomic, strong) NSMutableDictionary *dictionaryJoins;
@property (nonatomic, strong) NSMutableArray *joinsArray;
@property (nonatomic, strong) LoadingView *loading;

@end

@implementation JoinsViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    
    self.tableViewOutlet.delegate = self;
    self.tableViewOutlet.dataSource = self;
    

}

-(void)viewWillAppear:(BOOL)animated
{
    self.loading = [[LoadingView alloc] initWithFrame:[UIScreen mainScreen].bounds];
    [self.view addSubview:self.loading];
    [self.loading animate:YES];
    
    [[Client sharedClient] getMyJoins:self.wallIdString completition:^(BOOL success, NSError *error) {
        
        if (success) {
            [self.loading animate:NO];
            NSLog(@"%@",[SharedVariables sharedInstance].joinsDict);
            
            self.dictionaryJoins = [[SharedVariables sharedInstance].joinsDict objectForKey:@"data"];
            
            self.joinsArray=[[self.dictionaryJoins objectForKey:@"joinUsers"] mutableCopy];
            NSLog(@"WallMessages:%@",self.joinsArray);
            [self.tableViewOutlet reloadData];
        }
        else
        {
            [self.loading animate:NO];
        }
        
    }];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}


#pragma mark - TABLEVIEW DELEGATE METHODS


-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    
    return _joinsArray.count;
    
}

-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    
    UITableViewCell *cellReal = [tableView dequeueReusableCellWithIdentifier:@"Cell"];
    
    if (_joinsArray.count > 0) {
        
        NSDictionary* joinsDict = [_joinsArray objectAtIndex:indexPath.row];
        
        UIImageView* profileImageView = [cellReal viewWithTag:1];
        UILabel * nameLbl = [cellReal viewWithTag:2];
        UILabel * statusLbl = [cellReal viewWithTag:3];
        
        NSString* name = [joinsDict objectForKey:@"extended_name"];
        NSString* picture = [joinsDict objectForKey:@"picture_path"];
        NSString* status = [NSString stringWithFormat:@"%@",[joinsDict objectForKey:@"status"]];
        
        
        nameLbl.text = name;
        
        if ([status isEqualToString:@"1"]) {
            
            statusLbl.text = @"Joining";
        }
        else if ([status isEqualToString:@"2"])
        {
           statusLbl.text = @"Maybe";
        }
        
        dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_BACKGROUND, 0), ^{
            
            
            NSString * urlstring = [NSString stringWithFormat:@"http://34.230.105.103:8002/%@", picture];
            NSURL *imageURL = [NSURL URLWithString:urlstring];
            UIImage * img = [UIImage imageWithData:[NSData dataWithContentsOfURL:[NSURL URLWithString:urlstring]]];
            
            dispatch_async(dispatch_get_main_queue(), ^{
                
                profileImageView.layer.cornerRadius = profileImageView.frame.size.height / 2;
                profileImageView.layer.masksToBounds = YES;
                
                // assign cell image on main thread
                [profileImageView sd_setImageWithURL:imageURL placeholderImage:[UIImage imageNamed:@"avatar2.png"]];
                //[cell.textPostProfileImageView setImageWithURL:imageURL placeholderImage:[UIImage imageNamed:@"avatar2.png"]];
            });
        });
        
    }
    
    return cellReal;
}


-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    
    
    NSDictionary * selectedDict = _joinsArray[indexPath.row];
    
    NSLog(@"dict:%@",selectedDict);
    
    UserProfileViewController * userProfileVC = [self.storyboard instantiateViewControllerWithIdentifier:@"UserProfileViewController"];
    userProfileVC.userDict = selectedDict;
    //[self.navigationController pushViewController:userProfileVC animated:YES];
    [self.navigationController presentViewController:userProfileVC animated:YES completion:nil];
    
}

-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    
    
    UITableViewCell *cellReal = [tableView dequeueReusableCellWithIdentifier:@"Cell"];
    
    NSLog(@"%f",cellReal.frame.size.height);
    
    return cellReal.frame.size.height;
}


/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

- (IBAction)backBtnAction:(id)sender {
    
    [self.navigationController popViewControllerAnimated:YES];
}

- (IBAction)profilePicAction:(id)sender {
    
    UIView* baseView = [sender superview];
    UIView* cell = [baseView superview];
    
    NSIndexPath *indexPath = [self.tableViewOutlet indexPathForCell:cell];
    
    NSDictionary * selectedDict = _joinsArray[indexPath.row];
    
    NSLog(@"dict:%@",selectedDict);
    
    UserProfileViewController * userProfileVC = [self.storyboard instantiateViewControllerWithIdentifier:@"UserProfileViewController"];
    userProfileVC.userDict = selectedDict;
   // [self.navigationController pushViewController:userProfileVC animated:YES];
    [self.navigationController presentViewController:userProfileVC animated:YES completion:nil];
}
@end
