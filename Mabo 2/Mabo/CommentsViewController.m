//
//  CommentsViewController.m
//  Mabo
//
//  Created by vishnuprasathg on 11/13/17.
//  Copyright © 2017 DCI. All rights reserved.
//

#import "CommentsViewController.h"
#import "Client.h"
#import "SharedVariables.h"
#import "LoadingView.h"
#import "UIImageView+WebCache.h"
#import "Utilities.h"
#import "UserProfileViewController.h"

@interface CommentsViewController ()

@property (nonatomic, strong) NSMutableDictionary *dictionaryComments;
@property (nonatomic, strong) NSMutableArray *commentsArray;
@property (nonatomic, strong) LoadingView *loading;

@end

@implementation CommentsViewController


- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    
    
    self.tableViewOutlet.delegate = self;
    self.tableViewOutlet.dataSource = self;
    

}

-(void)viewWillAppear:(BOOL)animated
{
    
    self.loading = [[LoadingView alloc] initWithFrame:[UIScreen mainScreen].bounds];
    [self.view addSubview:self.loading];
    [self.loading animate:YES];
    
    [[Client sharedClient] getMyComments:self.wallIdString completition:^(BOOL success, NSError *error) {
        
        if (success) {
            [self.loading animate:NO];
            NSLog(@"%@",[SharedVariables sharedInstance].CommentsDict);
            
            self.dictionaryComments = [[SharedVariables sharedInstance].CommentsDict objectForKey:@"data"];
            
            self.commentsArray=[[self.dictionaryComments objectForKey:@"commentedUsers"] mutableCopy];
            NSLog(@"commentedUsers:%@",self.commentsArray);
            
            [self.tableViewOutlet reloadData];
            // [self populateContent];
        }
        else
        {
            [self.loading animate:NO];
        }
        
    }];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}


#pragma mark - TABLEVIEW DELEGATE METHODS


-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    
    return _commentsArray.count;
    
}

-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    
    UITableViewCell *cellReal = [tableView dequeueReusableCellWithIdentifier:@"Cell"];
    
    if (_commentsArray.count > 0) {
        
        NSDictionary* commentsDict = [_commentsArray objectAtIndex:indexPath.row];
        
        UIImageView* profileImageView = [cellReal viewWithTag:1];
        UILabel * nameLbl = [cellReal viewWithTag:2];
        UILabel* timeLbl = [cellReal viewWithTag:3];
        UITextView* commentLbl = [cellReal viewWithTag:4];
        
        NSString* name = [commentsDict objectForKey:@"extended_name"];
        NSString* picture = [commentsDict objectForKey:@"picture_path"];
        NSString* cmment = [commentsDict objectForKey:@"content"];
        
        
        nameLbl.text = name;
        commentLbl.text = cmment;
        
        
        NSString * timeStampString = [NSString stringWithFormat:@"%@",[commentsDict objectForKey:@"timestamp"]];
        
        if (![timeStampString isEqualToString:@"<null>"]) {
            
            NSTimeInterval _interval=[timeStampString doubleValue];
            NSDate *date = [NSDate dateWithTimeIntervalSince1970:_interval];
            NSLog(@"%@", date);
            
            
            NSDateFormatter *formatter = [[NSDateFormatter alloc] init];
            [formatter setDateFormat:@"dd MMMM yyyy"];
            NSString *result = [formatter stringFromDate:date];
            
            NSMutableAttributedString*tmeString = [[Utilities sharedInstance]loadTimeDifferent:date];
            
            timeLbl.attributedText = tmeString;
        }
        else
        {
            timeLbl.text = [NSString stringWithFormat:@"Not Available"];
        }
        
        dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_BACKGROUND, 0), ^{
            
            
            NSString * urlstring = [NSString stringWithFormat:@"http://34.230.105.103:8002/%@", picture];
            NSURL *imageURL = [NSURL URLWithString:urlstring];
            UIImage * img = [UIImage imageWithData:[NSData dataWithContentsOfURL:[NSURL URLWithString:urlstring]]];
            
            dispatch_async(dispatch_get_main_queue(), ^{
                
                profileImageView.layer.cornerRadius = profileImageView.frame.size.height / 2;
                profileImageView.layer.masksToBounds = YES;
                
                // assign cell image on main thread
                [profileImageView sd_setImageWithURL:imageURL placeholderImage:[UIImage imageNamed:@"avatar2.png"]];
                //[cell.textPostProfileImageView setImageWithURL:imageURL placeholderImage:[UIImage imageNamed:@"avatar2.png"]];
            });
        });

        
    }
    
    return cellReal;
}


-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    
    
}

-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    
    
    UITableViewCell *cellReal = [tableView dequeueReusableCellWithIdentifier:@"Cell"];
    
    int height;
    
    NSDictionary* commentsDict = [_commentsArray objectAtIndex:indexPath.row];
    
    NSString* cmment = [commentsDict objectForKey:@"content"];
    
    UITextView* commentLbl = [cellReal viewWithTag:4];
    UIImageView* profileImageView = [cellReal viewWithTag:1];
    
    commentLbl.text = cmment;
    
    int value = commentLbl.contentSize.height;
    int value2 = profileImageView.frame.size.height;
    

    return value + value2 + 10;
}



/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

- (IBAction)backBtnAction:(id)sender {
    
    [self.navigationController popViewControllerAnimated:YES];
}

- (IBAction)profilePicAction:(id)sender {
    
    UIView* baseView = [sender superview];
    UIView* cell = [baseView superview];
    
    NSIndexPath *indexPath = [self.tableViewOutlet indexPathForCell:cell];
    
    NSDictionary * selectedDict = _commentsArray[indexPath.row];
    
    NSLog(@"dict:%@",selectedDict);
    
    UserProfileViewController * userProfileVC = [self.storyboard instantiateViewControllerWithIdentifier:@"UserProfileViewController"];
    userProfileVC.userDict = selectedDict;
   // [self.navigationController pushViewController:userProfileVC animated:YES];
    [self.navigationController presentViewController:userProfileVC animated:YES completion:nil];
}
@end
