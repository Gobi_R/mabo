//
//  CreatPostViewController.m
//  Mobo
//
//  Created by vishnuprasathg on 10/24/17.
//  Copyright © 2017 DCI. All rights reserved.
//

#import "CreatPostViewController.h"
#import <MobileCoreServices/MobileCoreServices.h>

#import <AVKit/AVKit.h>
#import <AVFoundation/AVFoundation.h>
#import "SharedVariables.h"
#import "Client.h"
#import "MapViewController.h"
#import "Utilities.h"
#import "LoadingView.h"
#import "cloudinaryHelper.h"
#import "Cloudinary.h"
//@import AssetsLibrary;
//@import AVKit;
#define screenSize [[UIScreen mainScreen] bounds]

@interface CreatPostViewController ()

@property (nonatomic, strong) LoadingView *loading;
@property int sendPictureIndex;
@property int sendVideoIndex;

@end

@implementation CreatPostViewController


{
    
    BOOL isEvent;
    BOOL isParicipate;
    BOOL isAddPhotos;
    BOOL isAddVideos;
    BOOL isparticipant;

    NSMutableArray * photosArray ;
    NSMutableArray * videosArray ;
    NSMutableArray * videosUrlArray ;
    UIView* backGroundView ;
    UITextField* activeField ;
    NSMutableDictionary * postDict ;
    NSString * selectedDate ;
    
    NSMutableArray * photosBase64Array ;
    NSMutableArray * videosBase64Array ;
    
    NSURL *movieURL;
    
     double latti;
     double longi;
    
}


- (void)viewDidLoad {
    [super viewDidLoad];
    
    UITapGestureRecognizer *tap = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(dismissKeyboard)];
    
    self.postContentTextViewOutlet.autocorrectionType = UITextAutocorrectionTypeNo;
    self.titleTextViewOutlet.autocorrectionType = UITextAutocorrectionTypeNo;
    self.eventTextViewoutlet.autocorrectionType = UITextAutocorrectionTypeNo;
    
    [self.view addGestureRecognizer:tap];
    
    isEvent = NO;
    isParicipate = NO;
    
    isAddPhotos = NO;
    isAddVideos = NO;
    
    isparticipant = NO;
    self.priceTxtFieldOutlet.hidden = YES;
    self.priceTitleLblOutlet.hidden = YES;
    
    self.baseScrollViewOutlet.delegate = self;
    
    photosArray = [[NSMutableArray alloc] init];
    videosArray = [[NSMutableArray alloc] init];
    videosUrlArray = [[NSMutableArray alloc] init];
    photosBase64Array = [[NSMutableArray alloc] init];
    videosBase64Array = [[NSMutableArray alloc] init];

    self.titleTextViewOutlet.text = @"Title...";
    self.titleTextViewOutlet.textColor = [UIColor lightGrayColor];
    self.titleTextViewOutlet.delegate = self;
    
    self.postContentTextViewOutlet.text = @"Post Content...";
    self.postContentTextViewOutlet.textColor = [UIColor lightGrayColor];
    self.postContentTextViewOutlet.delegate = self;
    
    self.eventTextViewoutlet.text = @"Quick notes";
    self.eventTextViewoutlet.textColor = [UIColor lightGrayColor];
    self.eventTextViewoutlet.delegate = self;
    
    self.addphotosBaseViewHeightConstraint.constant = 80;
    self.addVideosBaseViewHeightConstraint.constant = 80;
    
    self.eventBaseViewHeightConstraint.constant = 0;
    self.particapationBaseViewHeightConstraint.constant = 0;
    
    self.eventBaseViewoutlet.hidden = YES;
    self.participationBaseViewOutlet.hidden = YES;
    
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(keyboardWasShown:) name:UIKeyboardDidShowNotification object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(keyboardWillBeHidden:) name:UIKeyboardWillHideNotification object:nil];
    
    if ([self.viewIdendity isEqualToString:@"editPost"]) {
        
        [self loadData];
        
        self.titleTextViewOutlet.userInteractionEnabled = NO;
        self.addphotosBtnOutlet.userInteractionEnabled = NO;
        self.addVideosBtnOutlet.userInteractionEnabled = NO;
        
        [self.createBtnOutlet setTitle:@"Update" forState:UIControlStateNormal];
        self.titileLblOutlet.text = @"Edit Event";
        
        
        self.addPhotosTitleLblOutlet.text =@"Photos";
        self.addVideosTitleLblOutlet.text =@"Videos";
        
//        self.addPhotosBtnHeightConstraint.constant = 0;
//        self.addVideosBtnHeightConstraint.constant = 0;
//        
//        self.addphotosBaseViewHeightConstraint.constant = self.addphotosBaseViewHeightConstraint.constant -64;
//        self.addVideosBaseViewHeightConstraint.constant = self.addVideosBaseViewHeightConstraint.constant -64;
        
    }
    else
    {
        [self.createBtnOutlet setTitle:@"Create" forState:UIControlStateNormal];
        
//        NSDateFormatter *dateFormatter=[[NSDateFormatter alloc] init];
//        [dateFormatter setDateFormat:@"dd MMMM yyyy"] ;
//        // or @"yyyy-MM-dd hh:mm:ss a" if you prefer the time with AM/PM
//        NSLog(@"%@",[dateFormatter stringFromDate:[NSDate date]]);
        
        NSDateFormatter * dateFormatter = [[NSDateFormatter alloc] init] ;
        [dateFormatter setDateFormat:@"dd MMMM yyyy"] ;
        NSDate *date = [NSDate date]                                                                                                                                                                                                                 ;
        NSLog(@"date=%@",date) ;
        NSTimeInterval interval  = [date timeIntervalSince1970] ;
        NSLog(@"interval=%f",interval) ;
        NSDate *methodStart = [NSDate dateWithTimeIntervalSince1970:interval] ;
        [dateFormatter setDateFormat:@"dd MMMM yyyy"] ;
        NSLog(@"result: %@", [dateFormatter stringFromDate:methodStart]) ;
        
        
        
        selectedDate = [NSString stringWithFormat:@"%f",interval];
        
        //selectedDate = [NSString stringWithFormat:@"%@",[dateFormatter stringFromDate:[NSDate date]]];
        
        self.titileLblOutlet.text = @"Create Post";
        
        self.addPhotosTitleLblOutlet.text =@"Add photos";
        self.addVideosTitleLblOutlet.text =@"Add videos";
        
        latti = *([SharedVariables sharedInstance].lat);
        longi = *([SharedVariables sharedInstance].lon);
        
        
//        self.addPhotosBtnHeightConstraint.constant = 64;
//        self.addVideosBtnHeightConstraint.constant = 64;
    }
    
    
    
}

-(void)loadData
{
    
    self.eventBtnOutlet.userInteractionEnabled = NO;
    self.participatebtnoutlet.userInteractionEnabled = NO;
    
    NSDictionary *dict = self.editDateDict;
    
    NSLog(@"editDict:%@",dict);
    
    [SharedVariables sharedInstance].editPostId = [NSString stringWithFormat:@"%@",[dict objectForKey:@"id"]];
    
    
    self.titleTextViewOutlet.text = [dict objectForKey:@"title"];
    self.titleTextViewOutlet.textColor = [UIColor blackColor];
    
        int length = (int)self.titleTextViewOutlet.text.length + 1;
        
        _titleLimitLblOutlet.text = [NSString stringWithFormat:@"%d / 2-30",length];
        
        if(self.titleTextViewOutlet.text.length >= 10){
            
            _titleLimitLblOutlet.textColor = [UIColor darkGrayColor];
        }
        else
        {
            _titleLimitLblOutlet.textColor = [UIColor redColor];
        }

    
    self.postContentTextViewOutlet.text = [dict objectForKey:@"message"];
    self.postContentTextViewOutlet.textColor = [UIColor blackColor];
    
    int length1 = (int)self.postContentTextViewOutlet.text.length + 1;
    self.postContentLimitLblOutlet.text = [NSString stringWithFormat:@"%d / 2-160",length1];
    
    if(self.postContentTextViewOutlet.text.length >= 10){
        
        _postContentLimitLblOutlet.textColor = [UIColor darkGrayColor];
    }
    else
    {
        _postContentLimitLblOutlet.textColor = [UIColor redColor];
    }
    
    NSArray * picArray = [dict objectForKey:@"pictures"];
    NSArray * videoArray = [dict objectForKey:@"videos"];
    
    if (picArray.count > 0 || videoArray.count>0) {
        
        if(videoArray.count > 0) {
            
            for (NSDictionary*videoDict in videoArray) {
                
                    // retrive image on global queue
                    
                    NSString*sourceURLString =[NSString stringWithFormat:@"http://34.230.105.103:8002/%@",[videoDict objectForKey:@"vid_path"]];
                
                    [videosUrlArray addObject: sourceURLString];
                
                for (NSDictionary*imageDict in picArray) {
                    
                    //NSDictionary * imageDict = picArray.firstObject;
                    
                    dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), ^{
                        // retrive image on global queue
                        
                        NSString* profilePicPath = [NSString stringWithFormat:@"%@",[imageDict objectForKey:@"pic_path"]];
                        
                        NSString * urlstring = [NSString stringWithFormat:@"http://34.230.105.103:8002/%@", profilePicPath];
                        // NSURL *imageURL = [NSURL URLWithString:urlstring];
                        UIImage * img = [UIImage imageWithData:[NSData dataWithContentsOfURL:[NSURL URLWithString:urlstring]]];
                        
                        [videosArray addObject:img];
                        
                        
                        dispatch_async(dispatch_get_main_queue(), ^{
                            
                            [self.addVideosCollectionViewOutlet reloadData];
                            self.addVideosBaseViewHeightConstraint.constant = 194;
                        });
                    });
                    
                }
                    
            }
            
        }
        else if (picArray.count > 0) {
            
            for (NSDictionary*imageDict in picArray) {
                
            //NSDictionary * imageDict = picArray.firstObject;
            
            dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), ^{
                // retrive image on global queue
                
                NSString* profilePicPath = [NSString stringWithFormat:@"%@",[imageDict objectForKey:@"pic_path"]];
                
                NSString * urlstring = [NSString stringWithFormat:@"http://34.230.105.103:8002/%@", profilePicPath];
               // NSURL *imageURL = [NSURL URLWithString:urlstring];
                UIImage * img = [UIImage imageWithData:[NSData dataWithContentsOfURL:[NSURL URLWithString:urlstring]]];
                
                [photosArray addObject:img];
                
            
                dispatch_async(dispatch_get_main_queue(), ^{
                
                [self.addPhotosCollectionViewOutlet reloadData];
                self.addphotosBaseViewHeightConstraint.constant = 194;
                });
            });
                
            }
        }
    }
    
    latti = *([SharedVariables sharedInstance].lat);
    longi = *([SharedVariables sharedInstance].lon);
    
    [SharedVariables sharedInstance].creatPostSeletedAddress = [dict objectForKey:@"poi"];
    self.selectedAddressLblOutlet.text = [dict objectForKey:@"poi"];
    
    NSString * event = [NSString stringWithFormat:@"%@",[dict objectForKey:@"is_event"]];
    
    if ([event isEqualToString:@"1"]) {
        
        [self.eventBtnOutlet setImage:[UIImage imageNamed:@"toggle_on.png"] forState:UIControlStateNormal];
        
        isEvent = YES;
        
        self.eventBaseViewHeightConstraint.constant = 108;
        self.eventBaseViewoutlet.hidden = NO;
        
        NSString * timeStampString = [NSString stringWithFormat:@"%@",[dict objectForKey:@"event_date"]];
        
        if (![timeStampString isEqualToString:@"<null>"]) {
            
            NSTimeInterval _interval=[timeStampString doubleValue];
            NSDate *date = [NSDate dateWithTimeIntervalSince1970:_interval];
            NSLog(@"%@", date);
            
            
            NSDateFormatter *formatter = [[NSDateFormatter alloc] init];
            [formatter setDateFormat:@"dd MMMM yyyy"];
            NSString *result = [formatter stringFromDate:date];
            
            NSMutableAttributedString*tmeString = [[Utilities sharedInstance]loadTimeDifferent:date];
            
            // cell.eventPostDateLblOutlet.attributedText = tmeString;
            
            NSString * str = [NSString stringWithFormat:@"%@",result];
            // NSString *truncatedString = [str substringToIndex:[str length]-3];
            
            _dateLblOutlet.text = str;
            
            selectedDate = timeStampString;
        }
        
        self.eventTextViewoutlet.text = [NSString stringWithFormat:@"%@",[dict objectForKey:@"event_location_info"]];
        self.eventTextViewoutlet.textColor = [UIColor blackColor];

        
    }
    else
    {
        [self.eventBtnOutlet setImage:[UIImage imageNamed:@"toggle_off.png"] forState:UIControlStateNormal];
        
        isEvent = NO;
        
        self.eventBaseViewHeightConstraint.constant = 0;
        self.eventBaseViewoutlet.hidden = YES;
    }
    
    
    NSString * canParticipate = [NSString stringWithFormat:@"%@",[dict objectForKey:@"event_can_join"]];
    
    
    if ([canParticipate isEqualToString:@"1"]) {
        
        [self.participatebtnoutlet setImage:[UIImage imageNamed:@"toggle_on.png"] forState:UIControlStateNormal];
        
        isParicipate = YES;
        
        self.participationBaseViewOutlet.hidden = NO;
        self.particapationBaseViewHeightConstraint.constant = 139;
        
        
        NSString * price = [NSString stringWithFormat:@"%@",[dict objectForKey:@"event_price"]];
        
        if (![price isEqualToString:@"0"]) {
            
            
            self.priceTxtFieldOutlet.text = price;
            
            isparticipant = YES;
            
            [self.yesBtnOutlet setImage:[UIImage imageNamed:@"radio-sel.png"] forState:UIControlStateNormal];
            [self.noBtnOutlet setImage:[UIImage imageNamed:@"radio-unsel.png"] forState:UIControlStateNormal];
            
            self.priceTxtFieldOutlet.hidden = NO;
            self.priceTitleLblOutlet.hidden = NO;
        }
        
        else
        {
            isparticipant = NO;
            
            [self.yesBtnOutlet setImage:[UIImage imageNamed:@"radio-unsel.png"] forState:UIControlStateNormal];
            [self.noBtnOutlet setImage:[UIImage imageNamed:@"radio-sel.png"] forState:UIControlStateNormal];
            
            self.priceTxtFieldOutlet.hidden = YES;
            self.priceTitleLblOutlet.hidden = YES;
        }
        
        
    }
    else
    {
        [self.participatebtnoutlet setImage:[UIImage imageNamed:@"toggle_off.png"] forState:UIControlStateNormal];
        
        isParicipate = NO;
        
        self.participationBaseViewOutlet.hidden = YES;
        self.particapationBaseViewHeightConstraint.constant = 0;
    }
    
}

-(void)dismissKeyboard
{
    [self.priceTxtFieldOutlet resignFirstResponder];
    [self.titleTextViewOutlet resignFirstResponder];
    [self.postContentTextViewOutlet resignFirstResponder];
    [self.eventTextViewoutlet resignFirstResponder];
   
}

-(void)viewWillAppear:(BOOL)animated
{
    
    if (![self.viewIdendity isEqualToString:@"editPost"]) {
       
        if ([SharedVariables sharedInstance].creatPostSeletedAddress.length > 0) {
            
            double latdouble = [[SharedVariables sharedInstance].selectedLat doubleValue];
            double londouble = [[SharedVariables sharedInstance].selectedLon doubleValue];
            
            latti = latdouble;
            longi = londouble;
            
            //NSLog(@"lat: %f and lng : %f",self.latti,self.longi);
            
            self.selectedAddressLblOutlet.text = [SharedVariables sharedInstance].creatPostSeletedAddress;
        }
        else
        {
            self.selectedAddressLblOutlet.text = [SharedVariables sharedInstance].myCurrentAddress ;
        }
    }
    else
    {

        self.selectedAddressLblOutlet.text = [SharedVariables sharedInstance].creatPostSeletedAddress;
    }
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void)keyboardWasShown:(NSNotification *)aNotification

{// scroll to the text view
    NSDictionary* info = [aNotification userInfo];
    CGSize kbSize = [[info objectForKey:UIKeyboardFrameBeginUserInfoKey] CGRectValue].size;
    
    UIEdgeInsets contentInsets = UIEdgeInsetsMake(0.0, 0.0, kbSize.height, 0.0);
    self.baseScrollViewOutlet.contentInset = contentInsets;
    self.baseScrollViewOutlet.scrollIndicatorInsets = contentInsets;
    
    // If active text field is hidden by keyboard, scroll it so it's visible.
    // Your app might not need or want this behavior.
    CGRect aRect = self.view.frame;
    aRect.size.height -= kbSize.height;
    self.baseScrollViewOutlet.scrollEnabled = YES;
    if (!CGRectContainsPoint(aRect, activeField.frame.origin) ) {
        [self.baseScrollViewOutlet scrollRectToVisible:activeField.frame animated:YES];
    }
}

- (void)keyboardWillBeHidden:(NSNotification *)aNotification
{
    // scroll back..
    UIEdgeInsets contentInsets = UIEdgeInsetsZero;
    self.baseScrollViewOutlet.contentInset = contentInsets;
    self.baseScrollViewOutlet.scrollIndicatorInsets = contentInsets;
    //self.baseScrollViewOutlet.scrollEnabled = NO;
}

- (void)textFieldDidBeginEditing:(UITextField *)textField
{
    if (textField == self.priceTxtFieldOutlet) {
        
//        self.baseScrollViewOutlet.frame = CGRectMake(self.baseScrollViewOutlet.frame.origin.x,
//                                                     self.baseScrollViewOutlet.frame.origin.y,
//                                                     self.baseScrollViewOutlet.frame.size.width,
//                                                     self.baseScrollViewOutlet.frame.size.height - 215 + 50);
        
        self.baseScrollViewOutlet.contentSize = CGSizeMake(self.baseScrollViewOutlet.frame.size.width, self.baseScrollViewOutlet.contentSize.height + 250);
        
    }
    else
    {
       activeField = textField;
    }
}

- (void)textFieldDidEndEditing:(UITextField *)textField
{
    
    
    if (textField == self.priceTxtFieldOutlet) {
        
//        self.baseScrollViewOutlet.frame = CGRectMake(self.baseScrollViewOutlet.frame.origin.x,
//                                                     self.baseScrollViewOutlet.frame.origin.y,
//                                                     self.baseScrollViewOutlet.frame.size.width,
//                                                     self.baseScrollViewOutlet.frame.size.height + 215 - 50);
        
        self.baseScrollViewOutlet.contentSize = CGSizeMake(self.baseScrollViewOutlet.frame.size.width, self.baseScrollViewOutlet.contentSize.height - 250);
    }
    else
    {
       activeField = nil;
    }
}

-(BOOL) textFieldShouldReturn: (UITextField *) textField
{
    [textField resignFirstResponder];
    
    return YES;
}

- (BOOL)textField:(UITextField *)textField shouldChangeCharactersInRange:(NSRange)range replacementString:(NSString *)string {
    // Prevent crashing undo bug – see note below.
    if(range.length + range.location > textField.text.length)
    {
        return NO;
    }
    
    NSUInteger newLength = [textField.text length] + [string length] - range.length;
    return newLength <= 5;
}



- (BOOL) textViewShouldBeginEditing:(UITextView *)textView
{
    
    if (textView == self.titleTextViewOutlet) {
        
        if (self.titleTextViewOutlet.text.length > 0 && [self.titleTextViewOutlet.text  isEqual: @"Title..."]) {
            
            self.titleTextViewOutlet.text = @"";
            self.titleTextViewOutlet.textColor = [UIColor blackColor];
        }
        
    }
    else if (textView == self.postContentTextViewOutlet)
    {
        
        if (self.postContentTextViewOutlet.text.length > 0 && [self.postContentTextViewOutlet.text  isEqual: @"Post Content..."]) {
            
            self.postContentTextViewOutlet.text = @"";
            self.postContentTextViewOutlet.textColor = [UIColor blackColor];
        }
    }
    else if (textView == self.eventTextViewoutlet)
    {
        
        if (self.eventTextViewoutlet.text.length > 0 && [self.eventTextViewoutlet.text  isEqual: @"Quick notes"]) {
            
            self.eventTextViewoutlet.text = @"";
            self.eventTextViewoutlet.textColor = [UIColor blackColor];
            
           // activeField.frame = textView.frame;
        }
        else
        {
            
            
        }
        
    }
    
    return YES;
}

-(void) textViewDidChange:(UITextView *)textView
{
    if (textView == self.titleTextViewOutlet) {
        
        if(self.titleTextViewOutlet.text.length == 0){
            self.titleTextViewOutlet.textColor = [UIColor lightGrayColor];
            self.titleTextViewOutlet.text = @"Title...";
            [self.titleTextViewOutlet resignFirstResponder];
            
            _titleLimitLblOutlet.text = @"0 / 2-30";
            self.titleLimitLblOutlet.textColor = [UIColor darkGrayColor];
        }
        
    }
    else if (textView == self.postContentTextViewOutlet)
    {
        
        if(self.postContentTextViewOutlet.text.length == 0){
            self.postContentTextViewOutlet.textColor = [UIColor lightGrayColor];
            self.postContentTextViewOutlet.text = @"Post Content...";
            [self.postContentTextViewOutlet resignFirstResponder];
            
            self.postContentLimitLblOutlet.text = @"0 / 2-160";
            self.postContentLimitLblOutlet.textColor = [UIColor darkGrayColor];
        }
        
    }
    
    else if (textView == self.eventTextViewoutlet)
    {
        
        
        if(self.eventTextViewoutlet.text.length == 0){
            self.eventTextViewoutlet.textColor = [UIColor lightGrayColor];
            self.eventTextViewoutlet.text = @"Quick notes";
            [self.eventTextViewoutlet resignFirstResponder];
        }
    }
}

- (BOOL)textView:(UITextView *)textView shouldChangeTextInRange:(NSRange)range replacementText:(NSString *)text {
    
    if([text isEqualToString:@"\n"]) {
        
        if(self.titleTextViewOutlet.text.length == 0){
            self.titleTextViewOutlet.textColor = [UIColor lightGrayColor];
            self.titleTextViewOutlet.text = @"Title...";
            [self.titleTextViewOutlet resignFirstResponder];
        }
        
        else if(self.postContentTextViewOutlet.text.length == 0){
            self.postContentTextViewOutlet.textColor = [UIColor lightGrayColor];
            self.postContentTextViewOutlet.text = @"Post Content...";
            [self.postContentTextViewOutlet resignFirstResponder];
        }
        
        else if(self.eventTextViewoutlet.text.length == 0){
            self.eventTextViewoutlet.textColor = [UIColor lightGrayColor];
            self.eventTextViewoutlet.text = @"Quick notes";
            [self.eventTextViewoutlet resignFirstResponder];
        }
        [textView resignFirstResponder];
        return NO;
    }
     
    
    else if (textView == self.titleTextViewOutlet) {
        
        if(self.titleTextViewOutlet.text.length >= 30){
            
           
            return [self isAcceptableTextLength:self.titleTextViewOutlet.text.length + text.length - range.length shouldChangeTextInRange:textView];
        }
        else
        {
            
            int length = 0;
            
            const char * _char = [text cStringUsingEncoding:NSUTF8StringEncoding];
            int isBackSpace = strcmp(_char, "\b");
            
            if (isBackSpace == -8) {
                NSLog(@"Backspace was pressed");
                length = (int)self.titleTextViewOutlet.text.length - 1;
            }
            else
            {
              length = (int)self.titleTextViewOutlet.text.length + 1;
            }
            
            _titleLimitLblOutlet.text = [NSString stringWithFormat:@"%d / 2-30",length];
            
            if(length >= 2){
                
                _titleLimitLblOutlet.textColor = [UIColor darkGrayColor];
            }
            else
            {
               _titleLimitLblOutlet.textColor = [UIColor redColor];
            }
        }
    }
    else if (textView == self.postContentTextViewOutlet)
    {
        
        if(self.postContentTextViewOutlet.text.length >= 160){
            
            
            return [self isAcceptableTextLength:self.postContentTextViewOutlet.text.length + text.length - range.length shouldChangeTextInRange:textView];
        }
        
        else
        {
            
            int length = 0;
            
            const char * _char = [text cStringUsingEncoding:NSUTF8StringEncoding];
            int isBackSpace = strcmp(_char, "\b");
            
            if (isBackSpace == -8) {
                NSLog(@"Backspace was pressed");
                length = (int)self.postContentTextViewOutlet.text.length - 1;
            }
            else
            {
                length = (int)self.postContentTextViewOutlet.text.length + 1;
            }
            
            
            
            self.postContentLimitLblOutlet.text = [NSString stringWithFormat:@"%d / 2-160",length];
            
            if(length >= 2){
                
                _postContentLimitLblOutlet.textColor = [UIColor darkGrayColor];
            }
            else
            {
                _postContentLimitLblOutlet.textColor = [UIColor redColor];
            }
        }
        
    }
    
    else if (textView == self.eventTextViewoutlet)
    {
        
        if(self.eventTextViewoutlet.text.length >= 30){
            
            return [self isAcceptableTextLength:self.eventTextViewoutlet.text.length + text.length - range.length shouldChangeTextInRange:textView];
        }
        
    }
 
       return YES;
}

- (void)textViewDidBeginEditing:(UITextView *)textView
{
    //[self animateTextView: YES];
    
//    self.baseScrollViewOutlet.frame = CGRectMake(self.baseScrollViewOutlet.frame.origin.x,
//                                  self.baseScrollViewOutlet.frame.origin.y,
//                                  self.baseScrollViewOutlet.frame.size.width,
//                                  self.baseScrollViewOutlet.frame.size.height - 215 + 50);   //resize
    
    self.baseScrollViewOutlet.contentSize = CGSizeMake(self.baseScrollViewOutlet.frame.size.width, self.baseScrollViewOutlet.contentSize.height + 250);
    
    
}

- (void)textViewDidEndEditing:(UITextView *)textView
{
   // [self animateTextView:NO];
    //CGFloat keyboardHeight = [KeyboardService keyboardHeight];
    //CGSize * size = [[Utilities sharedInstance] getKeyboardFrame];
    
    self.baseScrollViewOutlet.contentSize = CGSizeMake(self.baseScrollViewOutlet.frame.size.width, self.baseScrollViewOutlet.contentSize.height - 250);
    
//    self.baseScrollViewOutlet.frame = CGRectMake(self.baseScrollViewOutlet.frame.origin.x,
//                                  self.baseScrollViewOutlet.frame.origin.y,
//                                  self.baseScrollViewOutlet.frame.size.width,
//                                  self.baseScrollViewOutlet.frame.size.height + 215 - 50); //resize
    
//    textField.autocorrectionType = UITextAutocorrectionTypeNo;
}
- (BOOL)isAcceptableTextLength:(NSUInteger)length shouldChangeTextInRange:(UITextView*)textView{
    
    if (textView == self.titleTextViewOutlet) {
       return length <= 30;
        
    }
    else if (textView == self.postContentTextViewOutlet) {
        
        return length <= 160;
    }
    else if (textView == self.eventTextViewoutlet) {
        
        return length <= 30;
        
    }
    else
    {
        return 30;
    }
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

- (IBAction)backBtnAction:(id)sender {
    
    [self.navigationController popViewControllerAnimated:YES];
    
}
-(void)creatSuccessAction
{
    [UIApplication sharedApplication].networkActivityIndicatorVisible = NO;
   [self.navigationController popViewControllerAnimated:YES];
}
- (IBAction)createBtnAction:(id)sender {
    
    [UIApplication sharedApplication].networkActivityIndicatorVisible = YES;
    
    [self.titleTextViewOutlet resignFirstResponder];
    [self.postContentTextViewOutlet resignFirstResponder];
    [self.eventTextViewoutlet resignFirstResponder];
    [self.priceTxtFieldOutlet resignFirstResponder];
    
    postDict = [[NSMutableDictionary alloc] init];
    
    [postDict setObject:@""  forKey:@"title"];
    [postDict setObject:@""  forKey:@"message"];
    [postDict setObject:@""  forKey:@"photos"];
    [postDict setObject:@""  forKey:@"videos"];
    [postDict setObject:@""  forKey:@"poi"];
    [postDict setObject:@""  forKey:@"is_event"];
    [postDict setObject:@""  forKey:@"event_date"];
    [postDict setObject:@""  forKey:@"event_location_info"];
    [postDict setObject:@""  forKey:@"event_can_join"];
    [postDict setObject:@""  forKey:@"event_price"];
    
    [postDict setObject:[SharedVariables sharedInstance].selectedLat  forKey:@"lat"];
    [postDict setObject:[SharedVariables sharedInstance].selectedLon  forKey:@"lon"];
    
    self.loading = [[LoadingView alloc] initWithFrame:[UIScreen mainScreen].bounds];
    [self.view addSubview:self.loading];
    [self.loading animate:YES];
    
    if (movieURL) {
        
        AVURLAsset* asset = [AVURLAsset URLAssetWithURL:movieURL options:nil];
        AVAssetExportSession *exportSession = nil;
        exportSession = [[AVAssetExportSession alloc] initWithAsset:asset presetName:AVAssetExportPresetHighestQuality];
        exportSession.shouldOptimizeForNetworkUse = YES;
        exportSession.outputFileType = AVFileTypeMPEG4;
        
        NSString *fileName1 = [NSString stringWithFormat:@"%@_%@", [[NSProcessInfo processInfo] globallyUniqueString], @"video.mov"];
        NSString * filePath1 = [NSTemporaryDirectory() stringByAppendingPathComponent:fileName1];
        NSURL *fileURL = [NSURL fileURLWithPath:filePath1];
        exportSession.outputURL = fileURL;
        
        [exportSession exportAsynchronouslyWithCompletionHandler:^(void){
            
            NSString *status = @"";
            
            if( exportSession.status == AVAssetExportSessionStatusUnknown ) status = @"AVAssetExportSessionStatusUnknown";
            else if( exportSession.status == AVAssetExportSessionStatusWaiting ) status = @"AVAssetExportSessionStatusWaiting";
            else if( exportSession.status == AVAssetExportSessionStatusExporting ) status = @"AVAssetExportSessionStatusExporting";
            else if( exportSession.status == AVAssetExportSessionStatusCompleted ) status = @"AVAssetExportSessionStatusCompleted";
            else if( exportSession.status == AVAssetExportSessionStatusFailed ) status = @"AVAssetExportSessionStatusFailed";
            else if( exportSession.status == AVAssetExportSessionStatusCancelled ) status = @"AVAssetExportSessionStatusCancelled";
            
            NSLog(@"done exporting to %@ status %ld = %@ (%@)",exportSession.outputURL,(long)exportSession.status, status,[[exportSession error] localizedDescription]);
            
            NSURL *compressPostVideoURL = exportSession.outputURL;
            
            NSString * timestamp = [NSString stringWithFormat:@"%f",[[NSDate date] timeIntervalSince1970] * 1000];
            NSArray* timeArray = [timestamp componentsSeparatedByString:@"."];
            
            NSString *cloudFileName = [NSString stringWithFormat:@"%@_%@",[Client sharedClient].user.email,timeArray.firstObject];
            
            [[cloudinaryHelper sharedInstance]cloudinaryVideo:^(NSDictionary *resultDict)
             {
                 
                 dispatch_async(dispatch_get_main_queue(), ^{
                     
                     NSLog(@"VideoUrl:%@",resultDict);
                     
                     NSString * cloudNameString = resultDict[@"public_id"];
                     NSString * cloudString = resultDict[@"format"];
                     NSString * cloudVideoString = [NSString stringWithFormat:@"%@.%@",cloudNameString,cloudString];
                     
                     
                     [videosUrlArray addObject:cloudVideoString];
                     
                     [self creat];
                     
                 });
             }fromVideo:[NSString stringWithFormat:@"%@",compressPostVideoURL] fromName: cloudFileName];
        }];
        
    }
    else
    {
        
        [self creat];
    }

}


-(void)creat
{
    if (self.titleTextViewOutlet.text.length >= 2 && ![self.titleTextViewOutlet.text isEqualToString:@"Title..."]) {
        
        NSString *string = self.titleTextViewOutlet.text;
        NSString *filteredString = [string stringByReplacingOccurrencesOfString:@"'"
                                                                     withString:@""
                                                                        options:NSRegularExpressionSearch range:NSMakeRange(0, string.length)];
        NSLog(@"%@", filteredString);
        
        [postDict setObject:filteredString forKey:@"title"];
    }
    else
    {
        [self.loading animate:NO];
        [[Utilities sharedInstance] showCustomAlertView:self.view title:@"Mabo" message:@"Title should be between 2 - 30 characters." controller:self reason:@"creatPostFail"];
        // [[Utilities sharedInstance] showCustomAlertView:self.view title:@"Mabo" message:@"Invalid title"];
        return;
    }
    
    if (self.postContentTextViewOutlet.text.length >= 2 && ![self.postContentTextViewOutlet.text isEqualToString:@"Post Content..."]) {
        
        NSString *string = self.postContentTextViewOutlet.text;
        NSString *filteredString = [string stringByReplacingOccurrencesOfString:@"'"
                                                                     withString:@""
                                                                        options:NSRegularExpressionSearch range:NSMakeRange(0, string.length)];
        NSLog(@"%@", filteredString);
        
        [postDict setObject:filteredString forKey:@"message"];
    }
    else
    {
        // [[Utilities sharedInstance] showCustomAlertView:self.view title:@"Mabo" message:@"Invalid message"];
        [self.loading animate:NO];
        [[Utilities sharedInstance] showCustomAlertView:self.view title:@"Mabo" message:@"Post content should be between 2 - 160 characters." controller:self reason:@"creatPostFail"];
        return;
    }
    
    if (videosUrlArray.count >0) {
        
        //        for (NSString*videoPathUrl in videosUrlArray) {
        //
        //            NSData *zipFileData = [NSData dataWithContentsOfFile:videoPathUrl];
        //
        //            NSString *base64String = [zipFileData base64EncodedStringWithOptions:0];
        //
        //            base64String = [base64String stringByReplacingOccurrencesOfString:@"/"
        //                                                                   withString:@"_"];
        //
        //            base64String = [base64String stringByReplacingOccurrencesOfString:@"+"
        //                                                                   withString:@"-"];
        //
        //            [videosBase64Array addObject:base64String];
        //        }
        NSLog(@"videos:%@",videosUrlArray);
        
        [postDict setObject:videosUrlArray  forKey:@"videos"];
        
        NSLog(@"photos:%@",videosArray);
        
        [postDict setObject:videosArray  forKey:@"photos"];
    }
    
    else if (photosArray.count >0) {
        
        //        for (UIImage* image in photosArray) {
        //
        //            NSString*imageBase64String = [UIImagePNGRepresentation(image) base64EncodedStringWithOptions:NSDataBase64Encoding64CharacterLineLength];
        //
        //            [photosBase64Array addObject:imageBase64String];
        //        }
        NSLog(@"photos:%@",photosArray);
        
        [postDict setObject:photosArray  forKey:@"photos"];
    }
    else
    {
        
    }
    
    
    if (self.selectedAddressLblOutlet.text.length > 0) {
        
        [postDict setObject:self.selectedAddressLblOutlet.text  forKey:@"poi"];
    }
    else
    {
        //[[Utilities sharedInstance] showCustomAlertView:self.view title:@"Mabo" message:@"Invalid address"];
        [self.loading animate:NO];
        [[Utilities sharedInstance] showCustomAlertView:self.view title:@"Mabo" message:@"Invalid address" controller:self reason:@"creatPostFail"];
        return;
    }
    
    if (isEvent == YES) {
        
        [postDict setObject:@"1"  forKey:@"is_event"];
        
        if (self.dateLblOutlet.text.length > 0) {
            
            [postDict setObject:selectedDate  forKey:@"event_date"];
        }
        else
        {
            [postDict setObject:@"-1"  forKey:@"event_date"];
        }
        
        if (self.eventTextViewoutlet.text.length > 0) {
            
            NSString *string = self.eventTextViewoutlet.text;
            NSString *filteredString = [string stringByReplacingOccurrencesOfString:@"'"
                                                                         withString:@""
                                                                            options:NSRegularExpressionSearch range:NSMakeRange(0, string.length)];
            NSLog(@"%@", filteredString);
            
            [postDict setObject:filteredString  forKey:@"event_location_info"];
        }
        else
        {
            [postDict setObject:@""  forKey:@"event_location_info"];
        }
    }
    else
    {
        [postDict setObject:@"0"  forKey:@"is_event"];
    }
    
    
    if (isParicipate == YES) {
        
        [postDict setObject:@"1"  forKey:@"event_can_join"];
        
        if (isparticipant == YES) {
            
            if (self.priceTxtFieldOutlet.text.length > 0) {
                
                int price = [self.priceTxtFieldOutlet.text intValue];
                
                if (price > 20000) {
                    
                    [self.loading animate:NO];
                    
                    [[Utilities sharedInstance] showCustomAlertView:self.view title:@"Mabo" message:@"Participation fee should be less than or equal to $20,000" controller:self reason:@"creatPostFail"];
                    return;
                }
                else
                {
                    [postDict setObject:self.priceTxtFieldOutlet.text  forKey:@"event_price"];
                }
            }
            else
            {
                [postDict setObject:@"0"  forKey:@"event_price"];
            }
        }
        else
        {
            [postDict setObject:@"0"  forKey:@"event_price"];
        }
        
    }
    else
    {
        [postDict setObject:@"0"  forKey:@"event_can_join"];
    }
    
    NSLog(@"PostDict:%@",postDict);
    
    if ([self.viewIdendity isEqualToString:@"editPost"]) {
        
        NSLog(@"--> %@",self.editDateDict);
        [[Client sharedClient] updateWallMessageFromId:[SharedVariables sharedInstance].editPostId withDictionary:postDict completition:^(BOOL success, NSError *error) {
            
            [self.loading animate:NO];
            if(success){
                NSLog(@"update user from POST");
                [[Client sharedClient] getUserInfo:[Client sharedClient].user.email completition:^(NSDictionary *dictionary, NSError *error) {
                    
                    if ([self.viewIdendity isEqualToString:@"editPost"]) {
                        
                        [[Utilities sharedInstance] showCustomAlertView:self.view title:@"Mabo" message:@"Your event successfully updated" controller:self reason:@"creatPost"];
                    }
                    else
                    {
                        [[Utilities sharedInstance] showCustomAlertView:self.view title:@"Mabo" message:@"Your post successfully created" controller:self reason:@"creatPost"];
                    }
                    
                    // [self.navigationController popViewControllerAnimated:YES];
                    if(dictionary){[[Client sharedClient].user initWithDictionary:dictionary];}
                    
                }];
            }else{
                UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"OOPS!" message:@"Error to send update" delegate:self cancelButtonTitle:@"OK" otherButtonTitles:nil];
                [alert show];
                
            }
        }];
        
    }
    else
    {
        [[Client sharedClient] postWallMessageFromDictionary:postDict completition:^(NSDictionary *dictionary, NSError *error) {
            if(dictionary){
                BOOL success = [[dictionary objectForKey:@"success"] boolValue];
                if(success){
                    NSString *idMessage = [[[dictionary objectForKey:@"data"] objectForKey:@"wall_message_id"] stringValue];
                    
                    if ([[postDict objectForKey:@"photos"]  isEqual: @""]) {
                        
                        if ([[postDict objectForKey:@"videos"] isEqual:@""]) {
                            
                            [self.loading animate:NO];
                            if ([self.viewIdendity isEqualToString:@"editPost"]) {
                                
                                [[Utilities sharedInstance] showCustomAlertView:self.view title:@"Mabo" message:@"Your event successfully updated" controller:self reason:@"creatPost"];
                            }
                            else
                            {
                                [[Utilities sharedInstance] showCustomAlertView:self.view title:@"Mabo" message:@"Your post successfully created" controller:self reason:@"creatPost"];
                            }
                            // [self.navigationController popViewControllerAnimated:YES];
                            
                        }
                        else
                        {
                            if([[postDict objectForKey:@"videos"] count]>0){
                                //                        [((NavigationController *)self.navigationController) updateLoadingViewMessage:NSLocalizedString(@"Sending Photos", nil) withDictionary:@{@"type":@"sendPhotos",@"photos":[self.dictionaryPost objectForKey:@"photos"]}];
                                
                                self.sendVideoIndex = 0;
                                [self lunchPostVideosWithIdMessage:idMessage];
                            }
                            else
                            {
                                [[Client sharedClient] getUserInfo:[Client sharedClient].user.email completition:^(NSDictionary *dictionary, NSError *error) {
                                    [self.loading animate:NO];
                                    if(dictionary){[[Client sharedClient].user initWithDictionary:dictionary];}
                                    
                                    self.sendVideoIndex = 0;
                                    if ([self.viewIdendity isEqualToString:@"editPost"]) {
                                        
                                        [[Utilities sharedInstance] showCustomAlertView:self.view title:@"Mabo" message:@"Your event successfully updated" controller:self reason:@"creatPost"];
                                    }
                                    else
                                    {
                                        [[Utilities sharedInstance] showCustomAlertView:self.view title:@"Mabo" message:@"Your post successfully created" controller:self reason:@"creatPost"];
                                    }
                                    // [self.navigationController popViewControllerAnimated:YES];
                                    //[self lunchPostVideosWithIdMessage:idMessage];
                                    
                                }];
                            }
                        }
                    }
                    else
                    {
                        if([[postDict objectForKey:@"photos"] count]>0){
                            
                            //                        [((NavigationController *)self.navigationController) updateLoadingViewMessage:NSLocalizedString(@"Sending Photos", nil) withDictionary:@{@"type":@"sendPhotos",@"photos":[self.dictionaryPost objectForKey:@"photos"]}];
                            
                            self.sendPictureIndex = 0;
                            [self lunchPostPictureWithIdMessage:idMessage];
                        }else{
                            NSLog(@"update user from POST");
                            
                            [[Client sharedClient] getUserInfo:[Client sharedClient].user.email completition:^(NSDictionary *dictionary, NSError *error) {
                                if(dictionary){[[Client sharedClient].user initWithDictionary:dictionary];}
                                //                            [((NavigationController *)self.navigationController) updateLoadingViewMessage:NSLocalizedString(@"Post created successfully", nil) withDictionary:@{@"type":@"successfully"}];
                                //                            [((NavigationController *)self.navigationController) dismissLoadingViewWithDelay:3 completition:^(BOOL success) {
                                //                                [self backNavigation];
                                //                            }];
                            }];
                        }
                    }
                    
                    
                }else{
                    [self.loading animate:NO];
                    NSString *messageError=@"";
                    NSDictionary *dicErrors = [[dictionary objectForKey:@"errors"] objectForKey:@"children"];
                    for(NSString *key in [dicErrors allKeys]){
                        id obj=[dicErrors objectForKey:key];
                        if ([obj isKindOfClass:[NSDictionary class]]) {
                            NSArray *arrayErrors = [[dicErrors objectForKey:key] objectForKey:@"errors"];
                            for (unsigned i=0; i<arrayErrors.count; i++) {
                                messageError = [messageError stringByAppendingString:[arrayErrors objectAtIndex:i]];
                                messageError = [messageError stringByAppendingString:@"\n"];
                            }
                        }
                    }
                    
                    if([messageError isEqualToString:@""])messageError=[dictionary objectForKey:@"message"];
                    if([messageError isEqualToString:@""])messageError=[dictionary objectForKey:@"errorString"];
                    
                    // [[Utilities sharedInstance] showCustomAlertView:self.view title:@"Mabo" message:messageError];
                    
                    [[Utilities sharedInstance] showCustomAlertView:self.view title:@"Mabo" message:messageError controller:self reason:@"creatPostFail"];
                    
                    //UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"OOPS!" message:messageError delegate:self cancelButtonTitle:@"OK" otherButtonTitles:nil];
                    //[alert show];
                    
                }
            }
            if(error){
                // UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"OOPS!" message:error.description delegate:self cancelButtonTitle:@"OK" otherButtonTitles:nil];
                //[alert show];
                [self.loading animate:NO];
                //[[Utilities sharedInstance] showCustomAlertView:self.view title:@"Mabo" message:error.description];
                [[Utilities sharedInstance] showCustomAlertView:self.view title:@"Mabo" message:error.description controller:self reason:@"creatPostFail"];
                
            }
        }];
    }
}

- (IBAction)addPhotosBtnAction:(id)sender {
    
    [self.priceTxtFieldOutlet resignFirstResponder];
    [self.titleTextViewOutlet resignFirstResponder];
    [self.postContentTextViewOutlet resignFirstResponder];
    [self.eventTextViewoutlet resignFirstResponder];
    
    if (videosUrlArray.count > 0) {
        
        [[Utilities sharedInstance] showCustomAlertViewWithMultiple:self title:@"Mabo" message:@"You have already added a video to this post, attaching a photo will remove it" actionResponse:@"addPhotos"];
    }
    else
    {
        isAddPhotos = YES;
        isAddVideos = NO;
        [[Utilities sharedInstance] showCustomCameraAlertView:self];
    }
}

- (IBAction)addVideosBtnAction:(id)sender {
    
    [self.priceTxtFieldOutlet resignFirstResponder];
    [self.titleTextViewOutlet resignFirstResponder];
    [self.postContentTextViewOutlet resignFirstResponder];
    [self.eventTextViewoutlet resignFirstResponder];
    
    if (photosArray.count > 0) {
        
        [[Utilities sharedInstance] showCustomAlertViewWithMultiple:self title:@"Mabo" message:@"You have already added one or more photos to this post, attaching a video will remove it" actionResponse:@"addVideos"];
    }
    else
    {
        
        if (videosUrlArray.count >= 1) {
            
            [[Utilities sharedInstance] showCustomAlertView:self.view title:@"Mabo" message:@"You have already added one video to this post" controller:self reason:@"addVideo"];
        }
        else
        {
        
    isAddPhotos = NO;
    isAddVideos = YES;
    [[Utilities sharedInstance] showCustomVideoAlertView:self];
        }
        
    }
}

-(void)addPhotosAlert
{
    [videosUrlArray removeAllObjects];
    [videosArray removeAllObjects];
    
    NSLog(@"videosArray:%@",videosArray);
    NSLog(@"videoUrlArray:%@",videosUrlArray);
    
    [self.addVideosCollectionViewOutlet reloadData];
    
    if (videosArray.count == 0) {
        
        self.addVideosBaseViewHeightConstraint.constant = 80;
    }
    
    isAddPhotos = YES;
    isAddVideos = NO;
    [[Utilities sharedInstance] showCustomCameraAlertView:self];
    
    
}
-(void)addVideosAlert
{
    [photosArray removeAllObjects];
    
    NSLog(@"photosArray:%@",photosArray);
    
    [self.addPhotosCollectionViewOutlet reloadData];
    
    if (photosArray.count == 0) {
        
        self.addphotosBaseViewHeightConstraint.constant = 80;
    }
    
    isAddPhotos = NO;
    isAddVideos = YES;
    [[Utilities sharedInstance] showCustomVideoAlertView:self];
    
}


- (IBAction)postLoactionBtnAction:(id)sender {
    
    
    [self.priceTxtFieldOutlet resignFirstResponder];
    [self.titleTextViewOutlet resignFirstResponder];
    [self.postContentTextViewOutlet resignFirstResponder];
    [self.eventTextViewoutlet resignFirstResponder];
    
    MapViewController * mapVC = [self.storyboard instantiateViewControllerWithIdentifier:@"MapViewController"];
    mapVC.viewIdendity = @"creatPost";
    mapVC.latti = latti;
    mapVC.longi = longi;
    [self.navigationController pushViewController:mapVC animated:YES];
}

- (IBAction)eventBtnAction:(id)sender {
    
    [self.priceTxtFieldOutlet resignFirstResponder];
    [self.titleTextViewOutlet resignFirstResponder];
    [self.postContentTextViewOutlet resignFirstResponder];
    [self.eventTextViewoutlet resignFirstResponder];
    
    if (isEvent == NO) {
        
        [self.eventBtnOutlet setImage:[UIImage imageNamed:@"toggle_on.png"] forState:UIControlStateNormal];
        
        isEvent = YES;
        
        self.eventBaseViewHeightConstraint.constant = 108;
        self.eventBaseViewoutlet.hidden = NO;
        
        
        
    }
    else
    {
        
        [self.eventBtnOutlet setImage:[UIImage imageNamed:@"toggle_off.png"] forState:UIControlStateNormal];
        
        isEvent = NO;
        
        self.eventBaseViewHeightConstraint.constant = 0;
        self.eventBaseViewoutlet.hidden = YES;
        
        [self.participatebtnoutlet setImage:[UIImage imageNamed:@"toggle_off.png"] forState:UIControlStateNormal];
        
        isParicipate = NO;
        
        self.participationBaseViewOutlet.hidden = YES;
        self.particapationBaseViewHeightConstraint.constant = 0;
        
       
    }
    
}

- (IBAction)particiapteBtnAction:(id)sender {
    
    
    if (isEvent == YES) {
    
    [self.priceTxtFieldOutlet resignFirstResponder];
    [self.titleTextViewOutlet resignFirstResponder];
    [self.postContentTextViewOutlet resignFirstResponder];
    [self.eventTextViewoutlet resignFirstResponder];
    
    if (isParicipate == NO) {
        
        [self.participatebtnoutlet setImage:[UIImage imageNamed:@"toggle_on.png"] forState:UIControlStateNormal];
        
        isParicipate = YES;
        
        self.participationBaseViewOutlet.hidden = NO;
        self.particapationBaseViewHeightConstraint.constant = 139;
        
        isparticipant = NO;
        
        [self.yesBtnOutlet setImage:[UIImage imageNamed:@"radio-unsel.png"] forState:UIControlStateNormal];
        [self.noBtnOutlet setImage:[UIImage imageNamed:@"radio-sel.png"] forState:UIControlStateNormal];
        
    }
    else
    {
        
        [self.participatebtnoutlet setImage:[UIImage imageNamed:@"toggle_off.png"] forState:UIControlStateNormal];
        
        isParicipate = NO;
        
        self.participationBaseViewOutlet.hidden = YES;
        self.particapationBaseViewHeightConstraint.constant = 0;
        
        
    }
    }
}

- (IBAction)photosCloseBtnAction:(UIButton *)sender {
    
    if (![self.viewIdendity isEqualToString:@"editPost"]) {
    
    UICollectionViewCell * cell = sender.superview.superview;
    
    NSIndexPath *indexPath = [self.addPhotosCollectionViewOutlet indexPathForCell:cell];
    
    [photosArray removeObjectAtIndex:indexPath.row];
    
    NSLog(@"photosArray:%@",photosArray);
    
     [self.addPhotosCollectionViewOutlet reloadData];
    
    if (photosArray.count == 0) {
        
        self.addphotosBaseViewHeightConstraint.constant = 80;
    }
        
    }
}

- (IBAction)videosCloseBtnAction:(UIButton *)sender {
    
    if (![self.viewIdendity isEqualToString:@"editPost"]) {
    
    UICollectionViewCell * cell = sender.superview.superview;
    
    NSIndexPath *indexPath = [self.addVideosCollectionViewOutlet indexPathForCell:cell];
    
    [videosArray removeObjectAtIndex:indexPath.row];
    [videosUrlArray removeObjectAtIndex:indexPath.row];
    
    NSLog(@"videosArray:%@",videosArray);
    NSLog(@"videoUrlArray:%@",videosUrlArray);
    
    [self.addVideosCollectionViewOutlet reloadData];
    
    if (videosArray.count == 0) {
        
        self.addVideosBaseViewHeightConstraint.constant = 80;
    }
    }
}

- (IBAction)yesBtnAction:(id)sender {
    
    isparticipant = YES;
    
    [self.yesBtnOutlet setImage:[UIImage imageNamed:@"radio-sel.png"] forState:UIControlStateNormal];
    [self.noBtnOutlet setImage:[UIImage imageNamed:@"radio-unsel.png"] forState:UIControlStateNormal];
    
    self.priceTxtFieldOutlet.hidden = NO;
    self.priceTitleLblOutlet.hidden = NO;
}

- (IBAction)noBtnAction:(id)sender {
    
    [self.priceTxtFieldOutlet resignFirstResponder];
    [self.titleTextViewOutlet resignFirstResponder];
    [self.postContentTextViewOutlet resignFirstResponder];
    [self.eventTextViewoutlet resignFirstResponder];
    
    isparticipant = NO;
    
    [self.yesBtnOutlet setImage:[UIImage imageNamed:@"radio-unsel.png"] forState:UIControlStateNormal];
    [self.noBtnOutlet setImage:[UIImage imageNamed:@"radio-sel.png"] forState:UIControlStateNormal];
    
    self.priceTxtFieldOutlet.hidden = YES;
    self.priceTitleLblOutlet.hidden = YES;
}

- (IBAction)dateBtnAction:(id)sender {
    
    [self.priceTxtFieldOutlet resignFirstResponder];
    [self.titleTextViewOutlet resignFirstResponder];
    [self.postContentTextViewOutlet resignFirstResponder];
    [self.eventTextViewoutlet resignFirstResponder];
    
    backGroundView =  [[UIView alloc] initWithFrame:self.view.frame];
    
    backGroundView.backgroundColor = [UIColor colorWithRed:0 green:0 blue:0 alpha:0.5];
    
    
    UITapGestureRecognizer *tapGesture = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(handleTap:)];
    tapGesture.delegate = self;
    tapGesture.numberOfTapsRequired = 1;
    [backGroundView addGestureRecognizer:tapGesture];
    
    CGRect frame = self.datePickerBaseViewOutlet.frame;
    frame.origin.x = 0;
    frame.origin.y = screenSize.size.height/2 - self.datePickerBaseViewOutlet.frame.size.height/2 -50;
    frame.size.width = self.view.frame.size.width;
    frame.size.height = self.datePickerBaseViewOutlet.frame.size.height + 100;
    self.datePickerBaseViewOutlet.frame = frame;
    
    [self.view addSubview:backGroundView];
    [self.view bringSubviewToFront:backGroundView];
    
    [backGroundView addSubview:self.datePickerBaseViewOutlet];
    [backGroundView bringSubviewToFront:self.datePickerBaseViewOutlet];
    
    
}

-(void)handleTap: (UITapGestureRecognizer *) recognizer {
    
    
    NSDateFormatter *formatter=[[NSDateFormatter alloc]init];
    [formatter setDateFormat:@"dd MMMM yyyy"];
    self.dateLblOutlet.text=[NSString stringWithFormat:@"%@",[formatter stringFromDate:self.datePickerViewOutlet.date]];
    
    NSLog(@"datePickerDate:%@",self.datePickerViewOutlet.date);
    
    NSTimeInterval t = [self.datePickerViewOutlet.date timeIntervalSince1970];
    
    int value = (int)t / 1000;
    
    NSDateFormatter * dateFormatter = [[NSDateFormatter alloc] init] ;
    [dateFormatter setDateFormat:@"dd MMMM yyyy"] ;
    NSDate *date = [dateFormatter dateFromString:self.dateLblOutlet.text] ;
    NSLog(@"date=%@",date) ;
    NSTimeInterval interval  = [date timeIntervalSince1970] ;
    NSLog(@"interval=%f",interval) ;
    NSDate *methodStart = [NSDate dateWithTimeIntervalSince1970:interval] ;
    [dateFormatter setDateFormat:@"dd MMMM yyyy"] ;
    NSLog(@"result: %@", [dateFormatter stringFromDate:methodStart]) ;
    
    
    
    selectedDate = [NSString stringWithFormat:@"%f",interval];
    NSLog(@"selectedDate:%@",selectedDate);
    [backGroundView removeFromSuperview];
}

- (void) animateTextView:(BOOL) up
{
    if (up == YES)
    {
        self.baseScrollViewOutlet.contentSize = CGSizeMake(self.baseScrollViewOutlet.frame.size.width, self.baseScrollViewOutlet.frame.size.height + 200);
    }
    else
    {
        self.baseScrollViewOutlet.contentSize = CGSizeMake(self.baseScrollViewOutlet.frame.size.width, self.baseScrollViewOutlet.frame.size.height - 200);
    }
        
//    const int movementDistance = 200; // tweak as needed
//    const float movementDuration = 0.3f; // tweak as needed
//    int movement= movement = (up ? -movementDistance : movementDistance);
//    NSLog(@"%d",movement);
//
//    [UIView beginAnimations: @"anim" context: nil];
//    [UIView setAnimationBeginsFromCurrentState: YES];
//    [UIView setAnimationDuration: movementDuration];
//    self.view.frame = CGRectOffset(self.inputView.frame, 0, movement);
//    [UIView commitAnimations];
}



- (void)scrollViewDidScroll:(UIScrollView *)scrollView
{
    
    [self.titleTextViewOutlet resignFirstResponder];
    [self.postContentTextViewOutlet resignFirstResponder];
    //[self.eventTextViewoutlet resignFirstResponder];

}


- (void)imagePickerController:(UIImagePickerController *)picker didFinishPickingMediaWithInfo:(NSDictionary *)info{
    
    if (isAddPhotos == YES) {
        
        UIImage* orginalImage = [info objectForKey:UIImagePickerControllerOriginalImage];
    
        [photosArray addObject:orginalImage];
        
        dispatch_async(dispatch_get_main_queue(), ^{
            [self.addPhotosCollectionViewOutlet reloadData];
        });
        
        self.addphotosBaseViewHeightConstraint.constant = 194;
        
        [picker dismissViewControllerAnimated:YES completion:NULL];
        
    }
    else if (isAddVideos == YES)
    {
        
        NSURL *videoURL;
        NSString *videoPath1;
        
        NSLog(@"file info = %@", info);
        NSString *pickedType = [info objectForKey:@"UIImagePickerControllerMediaType"];
        NSData *videoData;
        if ([pickedType isEqualToString:@"public.movie"]){
            
            videoURL=(NSURL*)[info objectForKey:UIImagePickerControllerMediaURL];
            videoData = [NSData dataWithContentsOfURL:videoURL];
            
        }

        movieURL = [info objectForKey:
                           UIImagePickerControllerMediaURL];
        
//        NSString* pathString = [NSString stringWithFormat:@"%@",[movieURL pathComponents].lastObject];
//
//        NSURL *uploadURL = [NSURL fileURLWithPath:[[NSTemporaryDirectory() stringByAppendingPathComponent:@"Aravind"] stringByAppendingString:@".mp4"]];
//
//        [self convertVideoToLowQuailtyWithInputURL:movieURL outputURL:uploadURL];
        
//        NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
//        NSString *documentsDirectory = [paths objectAtIndex:0];
//        NSString *filePath = [documentsDirectory stringByAppendingPathComponent:@"MyVideo.mp4"];
//        NSURL *outputURL = [NSURL fileURLWithPath:filePath];
//
//        [self convertVideoToLowQuailtyWithInputURL:movieURL outputURL:outputURL handler:^(AVAssetExportSession *exportSession)
//        {
//            if (exportSession.status == AVAssetExportSessionStatusCompleted) {
//                // Video conversation completed
//                NSLog(@"urllll:%@",exportSession.outputURL);
//            }
//        }];
        
        NSString *mediaType = [info objectForKey: UIImagePickerControllerMediaType];
//        if (CFStringCompare ((__bridge_retained CFStringRef) mediaType, kUTTypeMovie, 0) == kCFCompareEqualTo)
//        {
//            NSString *moviePath = [[info objectForKey:UIImagePickerControllerMediaURL] path];
//
//            if (UIVideoAtPathIsCompatibleWithSavedPhotosAlbum(moviePath))
//            {
//                NSString *docDir = [NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES) objectAtIndex:0];
//                NSString *moviePath = [[info objectForKey:UIImagePickerControllerMediaURL] path];
//                videoPath1 =[NSString stringWithFormat:@"%@/xyz.mov",docDir];
//                NSURL *videoURL = [info objectForKey:UIImagePickerControllerMediaURL];
//                NSData *videoData = [NSData dataWithContentsOfURL:videoURL];
//                [videoData writeToFile:videoPath1 atomically:NO];
//                //  UISaveVideoAtPathToSavedPhotosAlbum(moviePath, self, nil, nil);
//            }
//        }
//
//        AVURLAsset *avAsset = [AVURLAsset URLAssetWithURL:[NSURL fileURLWithPath:videoPath1] options:nil];
//        NSArray *compatiblePresets = [AVAssetExportSession exportPresetsCompatibleWithAsset:avAsset];
//
//        if ([compatiblePresets containsObject:AVAssetExportPresetLowQuality])
//        {
//            AVAssetExportSession *exportSession = [[AVAssetExportSession alloc]initWithAsset:avAsset presetName:AVAssetExportPresetLowQuality];
//            NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
//            NSString*videoPath = [NSString stringWithFormat:@"%@/xyz.mp4", [paths objectAtIndex:0]];
//            exportSession.outputURL = [NSURL fileURLWithPath:videoPath];
//            NSLog(@"videopath of your mp4 file = %@",videoPath);  // PATH OF YOUR .mp4 FILE
//            exportSession.outputFileType = AVFileTypeMPEG4;
//
//            videoURL = exportSession.outputURL;
//            NSLog(@"VideoURL = %@", videoURL);
//
//        }
        
        NSData *urlData = [NSData dataWithContentsOfURL:videoURL];

        int count = (int)(urlData.length);

        if (![self isFileSizeUpTo10Mebibytes:count]) {

            [picker dismissViewControllerAnimated:YES completion:NULL];
            NSLog(@"File is more than 10 MB.");
            [[Utilities sharedInstance] showCustomAlertView:self.view title:@"Mabo" message:@"Your post video is more than 10 MB." controller:self reason:@"postVideo"];
        }
        else
        {
            
            [self dismissViewControllerAnimated:YES completion:^{
                
                //
               // [self writeFileData:videoData];
            }];
            
            _addVideosCollectionViewOutlet.hidden = NO;

            NSString*sourceURLString =[NSString stringWithFormat:@"%@", [info                                                         objectForKey:UIImagePickerControllerMediaURL]];

            //   dispatch_async(dispatch_get_main_queue(), ^{
            UIImage* orginalImage = [self generateThumbImage:sourceURLString];

            [videosArray addObject:orginalImage];
            
            NSLog(@"%@",_addVideosCollectionViewOutlet);

            dispatch_async(dispatch_get_main_queue(), ^{
                [self.addVideosCollectionViewOutlet reloadData];
            });

            //[self.addVideosCollectionViewOutlet reloadItemsAtIndexPaths:[self.addVideosCollectionViewOutlet indexPathsForVisibleItems]];
            //        _addVideosCollectionViewOutlet.delegate = self;
            //        _addVideosCollectionViewOutlet.dataSource = self;
            self.addVideosBaseViewHeightConstraint.constant = 194;

            [picker dismissViewControllerAnimated:YES completion:NULL];
        }
    }
    //[self dismissViewControllerAnimated:YES completion:nil];
    
}

- (NSString *)applicationDocumentsDirectory
{
    //    return [[[NSFileManager defaultManager] URLsForDirectory:NSDocumentDirectory inDomains:NSUserDomainMask] lastObject];
    NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
    NSString *documentsDirectory = [paths objectAtIndex:0];
    return documentsDirectory;
}

- (void) writeFileData:(NSData *)fileData{

    float size = fileData.length / (1024 * 1024);
    
    NSString *fileName = nil;
    NSString *strPath =  nil;
    
    NSString *documentsDirectory = [self applicationDocumentsDirectory];
    
    double CurrentTime = CACurrentMediaTime();
    
    
    fileName = [NSString stringWithFormat:@"%d.mp4",(int)CurrentTime];
    
    
    strPath =  [documentsDirectory stringByAppendingPathComponent:fileName];
    NSFileManager *filemanager=[[NSFileManager alloc] init];
    NSError *er;
    
    if (![filemanager fileExistsAtPath:documentsDirectory]) {
        [filemanager createDirectoryAtPath:documentsDirectory withIntermediateDirectories:YES attributes:nil error:&er];
        NSLog(@"error in folder creation = %@", er);
        
    }
    
    NSLog(@"size of data = %lu", (unsigned long)[fileData length]);
    BOOL saved = [fileData writeToFile:strPath atomically:YES];
    if (saved) {
        NSURL *videoURLMp4 = [NSURL URLWithString:strPath];
        NSLog(@"VIDEOURL:%@",videoURLMp4);
        [videosUrlArray addObject:videoURLMp4];
        // now u can handle mp4 video from videoURL
    }
    else
        return;
    
    
}

- (void)convertVideoToLowQuailtyWithInputURL:(NSURL*)inputURL outputURL:(NSURL*)outputURL handler:(void (^)(AVAssetExportSession*))handler {
    [[NSFileManager defaultManager] removeItemAtURL:outputURL error:nil];
    AVURLAsset *asset = [AVURLAsset URLAssetWithURL:inputURL options:nil];
    AVAssetExportSession *exportSession = [[AVAssetExportSession alloc] initWithAsset:asset presetName:AVAssetExportPresetPassthrough];
    exportSession.outputURL = outputURL;
    exportSession.outputFileType = AVFileTypeMPEG4;
    [exportSession exportAsynchronouslyWithCompletionHandler:^(void) {
        handler(exportSession);
    }];
}

-(BOOL) isFileSizeUpTo10Mebibytes:(int)fileSize
{
    return fileSize <= 10485760;
}

- (void)imagePickerControllerDidCancel:(UIImagePickerController *)picker {
    
    [picker dismissViewControllerAnimated:YES completion:NULL];
    
}

-(UIImage *)generateThumbImage : (NSString *)sourceURLString{
    NSURL *sourceMovieURL = [NSURL URLWithString:sourceURLString];
    AVURLAsset *sourceAsset = [AVURLAsset URLAssetWithURL:sourceMovieURL options:nil];
    AVAssetImageGenerator *generateImg = [[AVAssetImageGenerator alloc] initWithAsset:sourceAsset];
    NSError *error = NULL;
    CMTime time = CMTimeMake(1, 65);
    CGImageRef refImg = [generateImg copyCGImageAtTime:time actualTime:NULL error:&error];
    NSLog(@"error==%@, Refimage==%@", error, refImg);
    
    return [[UIImage alloc] initWithCGImage:refImg];
}

- (NSInteger)numberOfSectionsInCollectionView:(UICollectionView *)collectionView
{
    return 1;
}

- (NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section
{
    if (collectionView == self.addPhotosCollectionViewOutlet) {
        
      return photosArray.count;
    }
    else if (collectionView == self.addVideosCollectionViewOutlet) {
        
       return videosArray.count;
    }
    else
    {
      return 0;
    }
    
}

- (UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath
{
    UICollectionViewCell *cell = [[UICollectionViewCell alloc] init];
    
    
    if (collectionView == self.addPhotosCollectionViewOutlet) {
        
        if (photosArray.count > 0) {

        cell = [collectionView dequeueReusableCellWithReuseIdentifier:@"Cell" forIndexPath:indexPath];
        
        UIImageView *photosImageView = (UIImageView *)[cell viewWithTag:1];
        
        photosImageView.image = [photosArray objectAtIndex:indexPath.row];
            
            UIButton *closeBtn = (UIButton *)[cell viewWithTag:10];
            
            if ([self.viewIdendity isEqualToString:@"editPost"]) {
                
                closeBtn.hidden = YES;
                
            }
            else
            {
                closeBtn.hidden = NO;
            }

        }
    }
    else if (collectionView == self.addVideosCollectionViewOutlet) {
        
        if (videosArray.count > 0) {
        
        cell = [collectionView dequeueReusableCellWithReuseIdentifier:@"Cell1" forIndexPath:indexPath];
        
        UIImageView *videosImageView = (UIImageView *)[cell viewWithTag:1];
        
        videosImageView.image = [videosArray objectAtIndex:indexPath.row];
            
            UIButton *closeBtn = (UIButton *)[cell viewWithTag:10];
            
            if ([self.viewIdendity isEqualToString:@"editPost"]) {
                
                closeBtn.hidden = YES;
                
            }
            else
            {
                closeBtn.hidden = NO;
            }
            
        }
    }
    
    return cell;
}

-(void)lunchPostPictureWithIdMessage:(NSString *)idMessage{
    
    
    [[Client sharedClient] postImage:[[postDict objectForKey:@"photos"] objectAtIndex:self.sendPictureIndex] toWallMessageWithId:idMessage completition:^(BOOL success, NSError *error) {
        
      //  [((NavigationController *)self.navigationController) updateLoadingViewPhotosWithIndex:self.sendPictureIndex andSuccess:success];
        
        self.sendPictureIndex++;
        NSLog(@"postWithImages:%d",self.sendPictureIndex);
        if(self.sendPictureIndex<[[postDict objectForKey:@"photos"] count]){
            [self lunchPostPictureWithIdMessage:idMessage];
        }else{
            NSLog(@"update user from POST with Images");
            
            if ([[postDict objectForKey:@"videos"] isEqual:@""]) {
                
                [[Client sharedClient] getUserInfo:[Client sharedClient].user.email completition:^(NSDictionary *dictionary, NSError *error) {
                    [self.loading animate:NO];
                    if(dictionary){[[Client sharedClient].user initWithDictionary:dictionary];}
                    
                    self.sendVideoIndex = 0;
                    if ([self.viewIdendity isEqualToString:@"editPost"]) {
                        
                        [[Utilities sharedInstance] showCustomAlertView:self.view title:@"Mabo" message:@"Your event successfully updated" controller:self reason:@"creatPost"];
                    }
                    else
                    {
                        [[Utilities sharedInstance] showCustomAlertView:self.view title:@"Mabo" message:@"Your post successfully created" controller:self reason:@"creatPost"];
                    }
                   // [self.navigationController popViewControllerAnimated:YES];
                    //[self lunchPostVideosWithIdMessage:idMessage];
                    
                }];

            }
            else
            {
            if([[postDict objectForKey:@"videos"] count]>0){
                //                        [((NavigationController *)self.navigationController) updateLoadingViewMessage:NSLocalizedString(@"Sending Photos", nil) withDictionary:@{@"type":@"sendPhotos",@"photos":[self.dictionaryPost objectForKey:@"photos"]}];
                
                self.sendVideoIndex = 0;
                [self lunchPostVideosWithIdMessage:idMessage];
            }
            else
            {
            [[Client sharedClient] getUserInfo:[Client sharedClient].user.email completition:^(NSDictionary *dictionary, NSError *error) {
                [self.loading animate:NO];
                if(dictionary){[[Client sharedClient].user initWithDictionary:dictionary];}
                
                self.sendVideoIndex = 0;
                if ([self.viewIdendity isEqualToString:@"editPost"]) {
                    
                    [[Utilities sharedInstance] showCustomAlertView:self.view title:@"Mabo" message:@"Your event successfully updated" controller:self reason:@"creatPost"];
                }
                else
                {
                    [[Utilities sharedInstance] showCustomAlertView:self.view title:@"Mabo" message:@"Your post successfully created" controller:self reason:@"creatPost"];
                }
                //[self.navigationController popViewControllerAnimated:YES];
                //[self lunchPostVideosWithIdMessage:idMessage];
               
            }];
            }
            }
        }
    }];

}

-(void)lunchPostVideosWithIdMessage:(NSString *)idMessage{
    
    
    [[Client sharedClient] postVideo:[[postDict objectForKey:@"videos"] objectAtIndex:self.sendVideoIndex] toWallMessageWithId:idMessage completition:^(BOOL success, NSError *error) {
        
        //  [((NavigationController *)self.navigationController) updateLoadingViewPhotosWithIndex:self.sendPictureIndex andSuccess:success];
        
        self.sendVideoIndex++;
        if(self.sendVideoIndex<[[postDict objectForKey:@"videos"] count]){
            [self lunchPostVideosWithIdMessage:idMessage];
        }else{
            NSLog(@"update user from POST with Videos");
            [[Client sharedClient] getUserInfo:[Client sharedClient].user.email completition:^(NSDictionary *dictionary, NSError *error) {
                [self.loading animate:NO];
                if ([self.viewIdendity isEqualToString:@"editPost"]) {
                    
                    [[Utilities sharedInstance] showCustomAlertView:self.view title:@"Mabo" message:@"Your event successfully updated" controller:self reason:@"creatPost"];
                }
                else
                {
                    [[Utilities sharedInstance] showCustomAlertView:self.view title:@"Mabo" message:@"Your post successfully created" controller:self reason:@"creatPost"];
                }
               // [self.navigationController popViewControllerAnimated:YES];
                if(dictionary){[[Client sharedClient].user initWithDictionary:dictionary];}
                
            }];
        }
    }];
}
-(void)openCamera
{
    UIImagePickerController *picker = [[UIImagePickerController alloc] init];
    picker.delegate = self;
    picker.allowsEditing = YES;
    picker.sourceType = UIImagePickerControllerSourceTypeCamera;
    
    [self presentViewController:picker animated:YES completion:NULL];
}
-(void)openGallery
{
    UIImagePickerController *pickerView = [[UIImagePickerController alloc] init];
    pickerView.allowsEditing = YES;
    pickerView.delegate = self;
    [pickerView setSourceType:UIImagePickerControllerSourceTypeSavedPhotosAlbum];
    [self presentViewController:pickerView animated:YES completion:nil];
}

-(void)openVideoRoll
{
    UIImagePickerController *videoPicker = [[UIImagePickerController alloc] init];
    videoPicker.delegate = self; // ensure you set the delegate so when a video is chosen the right method can be called
    
    videoPicker.modalPresentationStyle = UIModalPresentationCurrentContext;
    // This code ensures only videos are shown to the end user
    videoPicker.mediaTypes = @[(NSString*)kUTTypeMovie, (NSString*)kUTTypeAVIMovie, (NSString*)kUTTypeVideo, (NSString*)kUTTypeMPEG4];
    
    videoPicker.videoQuality = UIImagePickerControllerQualityTypeHigh;
    [self presentViewController:videoPicker animated:YES completion:nil];

}

-(void)openVideoCamera
{
    UIImagePickerController *picker = [[UIImagePickerController alloc]init];
    picker.sourceType = UIImagePickerControllerSourceTypeCamera;
    picker.delegate = self;
    picker.allowsEditing = NO;
    
    NSArray *mediaTypes = [[NSArray alloc]initWithObjects:(NSString *)kUTTypeMovie, nil];
    
    picker.mediaTypes = mediaTypes;
    
    [self presentViewController:picker animated:YES completion:nil];
    
}





@end
