//
//  CustomCameraAlertView.h
//  Mobo
//
//  Created by vishnuprasathg on 10/24/17.
//  Copyright © 2017 DCI. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface CustomCameraAlertView : UIView
@property (weak, nonatomic) IBOutlet UIButton *cameraBtnOutlet;
@property (weak, nonatomic) IBOutlet UIButton *galleryBtnOutlet;
@property (weak, nonatomic) IBOutlet UIButton *cancelBtnOutlet;

- (IBAction)cameraBtnAction:(id)sender;
- (IBAction)galleryBtnAction:(id)sender;
- (IBAction)cancelBtnAction:(id)sender;

@property (strong, nonatomic) UIViewController * alertParentViewController;





@end
