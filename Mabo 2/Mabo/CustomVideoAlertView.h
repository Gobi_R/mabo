//
//  CustomVideoAlertView.h
//  Mabo
//
//  Created by vishnuprasathg on 10/31/17.
//  Copyright © 2017 DCI. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface CustomVideoAlertView : UIView
@property (weak, nonatomic) IBOutlet UIButton *openCameraBtnoutlet;
@property (weak, nonatomic) IBOutlet UIButton *videoRollBtnOutlet;
@property (weak, nonatomic) IBOutlet UIButton *cancelBtnOutlet;
- (IBAction)openCameraBtnAction:(id)sender;
- (IBAction)videoRollBtnAction:(id)sender;
- (IBAction)cancelBtnAction:(id)sender;

@property (strong, nonatomic) UIViewController * alertParentViewController;

@end
