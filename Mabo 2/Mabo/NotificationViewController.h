//
//  NotificationViewController.h
//  Mabo
//
//  Created by Aravind Kumar on 27/12/17.
//  Copyright © 2017 DCI. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface NotificationViewController : UIViewController<UITableViewDelegate,UITableViewDataSource>

@property (weak, nonatomic) IBOutlet UITableView *tableViewOutlet;
- (IBAction)backBtnAction:(id)sender;
@end
