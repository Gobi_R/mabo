//
//  SplashViewController.m
//  Mobo
//
//  Created by vishnuprasathg on 10/12/17.
//  Copyright © 2017 DCI. All rights reserved.
//

#import "SplashViewController.h"
#import "TutorialViewController.h"
#import "Client.h"
#import "SinchClient.h"
#import "TabBarViewController.h"
#import "SignInViewController.h"

@interface SplashViewController ()

@end

@implementation SplashViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    [self.navigationController setNavigationBarHidden:YES animated:NO];

    if([[[Client sharedClient] reachAbility] isReachable]){
        [[Client sharedClient] checkAuthenticateWithCompletition:^(BOOL success) {
            if(success){
                
                TabBarViewController * tabVC = [self.storyboard instantiateViewControllerWithIdentifier:@"TabBarViewController"];
                [self.navigationController pushViewController:tabVC animated:YES];
                
                
                
            }else{
                NSLog(@"non autenticato");
                [Client sharedClient].authenticate=NO;
                
                TutorialViewController * tutorialVC = [self.storyboard instantiateViewControllerWithIdentifier:@"TutorialViewController"];
                [self.navigationController pushViewController:tutorialVC animated:YES];
                
                
                //                LoginViewController *login = [[LoginViewController alloc] init];
                //                [self.navController setViewControllers:@[login]];
            }
            //[self.window setRootViewController:self.navController];
            
        }];
    }else{
        if([[Client sharedClient].user.email isEqualToString:@""]){
            //[loadingPage setAsNoUserWithOutConnection];
        }else{
            NSLog(@"nessuna connessione ma con utente");
            TabBarViewController * tabVC = [self.storyboard instantiateViewControllerWithIdentifier:@"TabBarViewController"];
            [self.navigationController pushViewController:tabVC animated:YES];
            [[NSNotificationCenter defaultCenter] postNotificationName:@"connectionChange" object:self];
        }
    
    }
    
    
//    TutorialViewController * tutorialVC = [self.storyboard instantiateViewControllerWithIdentifier:@"TutorialViewController"];
//    [self.navigationController pushViewController:tutorialVC animated:YES];
    
    
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
