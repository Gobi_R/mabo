//
//  SignInViewController.h
//  Mobo
//
//  Created by vishnuprasathg on 10/13/17.
//  Copyright © 2017 DCI. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "CustomAlertView.h"

@interface SignInViewController : UIViewController<UITextFieldDelegate>
@property (weak, nonatomic) IBOutlet UIView *creatNewAccountBseView;
@property (weak, nonatomic) IBOutlet UIView *signInBaseView;
@property (weak, nonatomic) IBOutlet UIButton *creatNewAccountBtnOutlet;
@property (weak, nonatomic) IBOutlet UIScrollView *baseScrollView;
@property (weak, nonatomic) IBOutlet UIButton *signInBtnOutlet;

@property (weak, nonatomic) IBOutlet UIButton *creatAccountBtnOutlet;
@property (weak, nonatomic) IBOutlet UIButton *forgotbtnOutlet;

- (IBAction)forgotBtnAction:(id)sender;

- (IBAction)creatNewAccountBtnAction:(id)sender;
- (IBAction)creatAccountBtnAction:(id)sender;
- (IBAction)signInBtnAction:(id)sender;
- (IBAction)checkBoxBtnAction:(id)sender;
- (IBAction)backToSignAction:(id)sender;
- (IBAction)faceBookLoginAction:(id)sender;

@property (weak, nonatomic) IBOutlet UITextField *emailTxtFieldOulet;
@property (weak, nonatomic) IBOutlet UITextField *passwordTxtFieldOutlet;
@property (weak, nonatomic) IBOutlet UIButton *checkBoxBtnOutlet;

@property (weak, nonatomic) IBOutlet UITextField *creatEmailTxtFldOutlet;
@property (weak, nonatomic) IBOutlet UITextField *creatUserNameTxtFldOutlet;
@property (weak, nonatomic) IBOutlet UITextField *creatPasswordTxtFldOutlet;
@property (weak, nonatomic) IBOutlet UITextField *creatRepeatPasswordTxtFldOutlet;

@property(nonatomic,retain) CustomAlertView *customAlertView;

-(void)afterFbLoginSuccess;
-(void)afterFbLoginCancel;

-(void)forgotOkAction;
-(void)forgotYesAction;
-(void)forgotfailiour;
-(void)afterRegisterSuccessfully;

@end
