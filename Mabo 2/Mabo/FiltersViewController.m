//
//  FiltersViewController.m
//  Mobo
//
//  Created by vishnuprasathg on 10/26/17.
//  Copyright © 2017 DCI. All rights reserved.
//

#import "FiltersViewController.h"
#import "Client.h"
#import "SharedVariables.h"

@interface FiltersViewController ()

@end

@implementation FiltersViewController

{
    
    BOOL isMale;
    BOOL isFemale;
    BOOL isOther;
    BOOL isNewFriend;
    BOOL isNiceDate;
    BOOL isCrazyNightOut;
    BOOL isBusinessContact;
    BOOL isFunEvents;
    BOOL isWhatEverHappens;
    
    BOOL isResetClicked;
    BOOL isCurrent;
    BOOL isSearch;
    
//    BOOL isPreMale;
//    BOOL isPreFemale;
//    BOOL isPreOther;
//    BOOL isPreNewFriend;
//    BOOL isPreNiceDate;
//    BOOL isPreCrazyNightOut;
//    BOOL isPreBusinessContact;
//    BOOL isPreFunEvents;
//    BOOL isPreWhatEverHappens;
//    
//    BOOL isPreviousFilter;
    
    NSString* stateIdendifier;
    
    int sliderValue;
    NSString* searchString;
    
    int sliderPreValue;
    NSString* searchPreString;
    
    int startPreAge;
    int endPreAge;
    
    
    NSMutableArray * genderArray ;
    NSMutableArray * ageArray ;
    NSMutableArray * intrestedInArray ;
    NSMutableDictionary * filterDict ;
    NSArray * resetArray;
    
    NSDictionary * filterDic;
    NSDictionary * previosDict;
    NSDictionary * newDict;
    NSDictionary * chagedPreviousDict;
}

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    
    _searchTextFieldOutlet.delegate = self;
    
    _ageSliderOutlet.minimumValue = 18;
    _ageSliderOutlet.maximumValue = 65;
    
    self.resetTableView.delegate = self;
    self.resetTableView.dataSource =self;
    
    self.resetTableView.hidden = YES;
    
    self.baseScrollView.delegate = self;
    
    isMale = NO;
    isFemale = NO;
    isOther = NO;
    isNewFriend = NO;
    isNiceDate = NO;
    isCrazyNightOut = NO;
    isBusinessContact = NO;
    isFunEvents = NO;
    isWhatEverHappens = NO;
    isResetClicked = NO;
    stateIdendifier =@"previous";
    
    
//     isPreMale= NO;
//     isPreFemale= NO;
//     isPreOther= NO;
//     isPreNewFriend= NO;
//     isPreNiceDate= NO;
//     isPreCrazyNightOut= NO;
//     isPreBusinessContact= NO;
//     isPreFunEvents= NO;
//     isPreWhatEverHappens= NO;

    
    [self.maleBtnOutlet setImage:[UIImage imageNamed:@"toggle_off.png"] forState:UIControlStateNormal];
    [self.femailBtnOutlet setImage:[UIImage imageNamed:@"toggle_off.png"] forState:UIControlStateNormal];
    [self.otherBtnOutlet setImage:[UIImage imageNamed:@"toggle_off.png"] forState:UIControlStateNormal];
    [self.friendsBtnOutlet setImage:[UIImage imageNamed:@"toggle_off.png"] forState:UIControlStateNormal];
    [self.niceDateBtnOutlet setImage:[UIImage imageNamed:@"toggle_off.png"] forState:UIControlStateNormal];
    [self.crazyNightOutBtnoutlet setImage:[UIImage imageNamed:@"toggle_off.png"] forState:UIControlStateNormal];
    [self.businessContactBtnOutlet setImage:[UIImage imageNamed:@"toggle_off.png"] forState:UIControlStateNormal];
    [self.funEventsBtnOutlet setImage:[UIImage imageNamed:@"toggle_off.png"] forState:UIControlStateNormal];
    [self.whateverHappensBtnOutlet setImage:[UIImage imageNamed:@"toggle_off.png"] forState:UIControlStateNormal];
    
    filterDict = [[NSMutableDictionary alloc]init];
    
    genderArray = [[NSMutableArray alloc] init];
    ageArray = [[NSMutableArray alloc] init];
    intrestedInArray = [[NSMutableArray alloc] init];
    resetArray = [[NSArray alloc] init];
    chagedPreviousDict = [[NSDictionary alloc] init];
    newDict =[[NSDictionary alloc] init];
    
    resetArray= [[NSArray alloc] initWithObjects:@"Clear All",@"Go Previous", nil];
    
    [filterDict setObject:@""  forKey:@"age"];
    [filterDict setObject:@""  forKey:@"interested_in"];
    [filterDict setObject:@""  forKey:@"sex"];
    [filterDict setObject:@""  forKey:@"text"];
    
      [SharedVariables sharedInstance].filterCurrentDict = [Client sharedClient].user.filterMap;
    chagedPreviousDict = [Client sharedClient].user.filterMap;
        
        filterDic =
        [Client sharedClient].user.filterMap;
   
        
        //filterDic =
        //[SharedVariables sharedInstance].filterPreviousDict;
   

    NSString* age = [filterDic objectForKey:@"age"];
    NSString* interested = [filterDic objectForKey:@"interested_in"];
    NSString* sex = [filterDic objectForKey:@"sex"];
    NSString* seachTest = [filterDic objectForKey:@"text"];
    
    if (seachTest.length > 0) {
        
        self.searchTextFieldOutlet.text = seachTest;
        searchString = seachTest;
    }
    else
    {
        self.searchTextFieldOutlet.text = @"";
        searchString = @"";
    }
    
    NSArray*ageArr = [age componentsSeparatedByString:@";"];
    NSArray*interestedArr = [interested componentsSeparatedByString:@";"];
    NSArray*sexArr = [sex componentsSeparatedByString:@";"];
    
    if (ageArr.count == 2) {
        
        for (NSString*age in ageArr) {
            
            [ageArray addObject:age];
        }
    }
    else
    {
       [ageArray addObject:@"18"];
        [ageArray addObject:@""];
    }
    
    if (ageArr.count > 1) {
        
        //NSInteger ageValue = [(ageArr[1]) integerValue];
        self.startAgeLblOutlet.text = (ageArr[0]);
        self.endAgeLblOutlet.text = (ageArr[1]);
        
        //self.ageSliderOutlet.value = ageValue;
    }
    else
    {
        self.endAgeLblOutlet.text = @"";
        
        //self.ageSliderOutlet.value = 0;
    }
    
    
    
    
    for (NSString*sexvalue in sexArr) {
        
        if ([sexvalue isEqualToString:@"0"]) {
            
            [self.maleBtnOutlet setImage:[UIImage imageNamed:@"toggle_on.png"] forState:UIControlStateNormal];
            
            [genderArray addObject:@"0"];
            
            isMale = YES;
            
        }
        else if ([sexvalue isEqualToString:@"1"]) {
            
            [self.femailBtnOutlet setImage:[UIImage imageNamed:@"toggle_on.png"] forState:UIControlStateNormal];
            
            [genderArray addObject:@"1"];
            isFemale = YES;
        }
        else if ([sexvalue isEqualToString:@"2"]) {
            
            [self.otherBtnOutlet setImage:[UIImage imageNamed:@"toggle_on.png"] forState:UIControlStateNormal];
            
            [genderArray addObject:@"2"];
            isOther = YES;
        }
    }
    
    for (NSString*interestedvalue in interestedArr) {
        
        if ([interestedvalue isEqualToString:@"0"]) {
            
            [self.friendsBtnOutlet setImage:[UIImage imageNamed:@"toggle_on.png"] forState:UIControlStateNormal];
            [intrestedInArray addObject:@"0"];
            
            isNewFriend = YES;
            
            
        }
        else if ([interestedvalue isEqualToString:@"1"]) {
            
            [self.niceDateBtnOutlet setImage:[UIImage imageNamed:@"toggle_on.png"] forState:UIControlStateNormal];
            
            [intrestedInArray addObject:@"1"];
            
            isNiceDate = YES;
            
            
        }
        else if ([interestedvalue isEqualToString:@"2"]) {
            
            [self.crazyNightOutBtnoutlet setImage:[UIImage imageNamed:@"toggle_on.png"] forState:UIControlStateNormal];
            [intrestedInArray addObject:@"2"];
            isCrazyNightOut = YES;
            
        }
        else if ([interestedvalue isEqualToString:@"3"]) {
            
            [self.businessContactBtnOutlet setImage:[UIImage imageNamed:@"toggle_on.png"] forState:UIControlStateNormal];
            [intrestedInArray addObject:@"3"];
            isBusinessContact = YES;
            
        }
        else if ([interestedvalue isEqualToString:@"4"]) {
            
            [self.funEventsBtnOutlet setImage:[UIImage imageNamed:@"toggle_on.png"] forState:UIControlStateNormal];
            [intrestedInArray addObject:@"4"];
            isFunEvents = YES;
            
        }
        else if ([interestedvalue isEqualToString:@"5"]) {
            
            [self.whateverHappensBtnOutlet setImage:[UIImage imageNamed:@"toggle_on.png"] forState:UIControlStateNormal];
            [intrestedInArray addObject:@"5"];
            isWhatEverHappens = YES;
        }
    }

    
    
    NSLog(@"FilterDict:%@",filterDict);
    
    self.searchTextFieldOutlet.layer.cornerRadius = self.searchTextFieldOutlet.frame.size.height / 2;
    self.searchTextFieldOutlet.layer.masksToBounds = YES;
    
    self.startAgeLblOutlet.layer.cornerRadius = self.startAgeLblOutlet.frame.size.height / 2;
    self.startAgeLblOutlet.layer.masksToBounds = YES;
    
    
    self.endAgeLblOutlet.layer.cornerRadius = self.endAgeLblOutlet.frame.size.height / 2;
    self.endAgeLblOutlet.layer.masksToBounds = YES;
    
    [self.searchTextFieldOutlet.layer setBorderColor:[UIColor colorWithRed:122/255.0f green:222/255.0f blue:225/255.0f alpha:1.0f].CGColor];
    [self.searchTextFieldOutlet.layer setBorderWidth:1.0];
    
    
    self.sliderRange.minValue = 18;
    self.sliderRange.maxValue = 65;
    self.sliderRange.delegate = self;
    
    if (ageArr.count ==2) {
        
        NSInteger ageValue = [(ageArr[0]) integerValue];
        NSInteger ageValue1 = [(ageArr[1]) integerValue];
        
        self.sliderRange.selectedMinimum = ageValue;
        self.sliderRange.selectedMaximum = ageValue1;
    }
    else
    {
        self.sliderRange.selectedMinimum = 18;
        self.sliderRange.selectedMaximum = 65;
        
        self.startAgeLblOutlet.text = @"18";
        self.endAgeLblOutlet.text = @"65";
    }
    
    
    
    [self.sliderRange addTarget:self action:@selector(logControlEvent:) forControlEvents:UIControlEventValueChanged];
}

- (void)logControlEvent:(TTRangeSlider *)sender {
    NSLog(@"Standard slider updated. Min Value: %.0f Max Value: %.0f", sender.selectedMinimum, sender.selectedMaximum);
}

#pragma mark TTRangeSliderViewDelegate
-(void)rangeSlider:(TTRangeSlider *)sender didChangeSelectedMinimumValue:(float)selectedMinimum andMaximumValue:(float)selectedMaximum{
    if (sender == self.sliderRange) {
        NSLog(@"Currency slider updated. Min Value: %.0f Max Value: %.0f", selectedMinimum, selectedMaximum);
        
//        NSInteger ageMinimum = selectedMinimum ;
//        NSInteger ageMaximum = selectedMaximum;
        
        [self.searchTextFieldOutlet resignFirstResponder];
        _resetTableView.hidden = YES;
        if (isResetClicked == YES) {
            isResetClicked = NO;
        }
        
        self.startAgeLblOutlet.text = [NSString stringWithFormat:@"%.0f",selectedMinimum];
        self.endAgeLblOutlet.text = [NSString stringWithFormat:@"%.0f",selectedMaximum];
        
        isCurrent = YES;
        stateIdendifier =@"new";
        
        
        ageArray[0] = [NSString stringWithFormat:@"%.0f",selectedMinimum];
        ageArray[1] = [NSString stringWithFormat:@"%.0f",selectedMaximum];
    }
}

-(void)viewWillAppear:(BOOL)animated
{
    
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}
-(void)resumeNewFilter{
    
    NSLog(@"Gender:%@",genderArray);
    
    if (genderArray.count > 0) {
        
        if (genderArray.count > 1) {
            
            
            NSString * genderString = [genderArray componentsJoinedByString:@";"];
            [filterDict setObject:genderString forKey:@"sex"];
            
        }
        else
        {
            [filterDict setObject:genderArray.firstObject forKey:@"sex"];
        }
        
    }
    
    NSLog(@"Intester:%@",intrestedInArray);
    
    if (intrestedInArray.count > 0) {
        
        if (intrestedInArray.count > 1) {
            
            NSString * interestedString = [intrestedInArray componentsJoinedByString:@";"];
            [filterDict setObject:interestedString forKey:@"interested_in"];
        }
        else
        {
            [filterDict setObject:intrestedInArray.firstObject forKey:@"interested_in"];
        }
        
    }
    
    NSLog(@"Age:%@",ageArray);
    
    if (ageArray.count > 0) {
        
        if (ageArray.count == 2) {
            
            NSString * ageString = [ageArray componentsJoinedByString:@";"];
            [filterDict setObject:ageString forKey:@"age"];
        }
        else
        {
            [filterDict setObject:@"" forKey:@"age"];
        }
    }
    else
    {
        [filterDict setObject:@"" forKey:@"age"];
    }
    
    
    if ([searchString length] == 0) {
        
        [filterDict setObject:@"" forKey:@"text"];
    }
    else{
        [filterDict setObject:searchString forKey:@"text"];
    }
    
    NSLog(@"FilterDict:%@",filterDict);
    
    // if (isSearch == YES) {
    
    newDict = filterDict;
    
    
//    [Client sharedClient].newFilterForMap=YES;
//    [[Client sharedClient].user initWithDictionary:@{@"filterMap":filterDict}];
//    
//    [SharedVariables sharedInstance].filterPreviousDict = [SharedVariables sharedInstance].filterCurrentDict;
//    [SharedVariables sharedInstance].filterCurrentDict = filterDict;
//    //    }
    
    NSLog(@"new:%@",newDict);
    
}

-(void)resumeFilter{
    
    
    
    NSLog(@"Gender:%@",genderArray);
    
    if (genderArray.count > 0) {
        
        if (genderArray.count > 1) {
        
            
            NSString * genderString = [genderArray componentsJoinedByString:@";"];
            [filterDict setObject:genderString forKey:@"sex"];
            
        }
        else
        {
            [filterDict setObject:genderArray.firstObject forKey:@"sex"];
        }
  
    }
    
    NSLog(@"Intester:%@",intrestedInArray);
    
    if (intrestedInArray.count > 0) {
        
        if (intrestedInArray.count > 1) {
            
            NSString * interestedString = [intrestedInArray componentsJoinedByString:@";"];
            [filterDict setObject:interestedString forKey:@"interested_in"];
        }
        else
        {
            [filterDict setObject:intrestedInArray.firstObject forKey:@"interested_in"];
        }

    }
    
    NSLog(@"Age:%@",ageArray);
    
    if (ageArray.count > 0) {
        
        if (ageArray.count > 1) {
            
            if ([ageArray[1] isEqualToString:@""]) {
                
               [filterDict setObject:@"" forKey:@"age"];
            }
            else
            {
            NSString * ageString = [ageArray componentsJoinedByString:@";"];
            [filterDict setObject:ageString forKey:@"age"];
            }
        }
        else
        {
            [filterDict setObject:@"" forKey:@"age"];
        }
    }
    else
    {
        [filterDict setObject:@"" forKey:@"age"];
    }
    
    
    if ([searchString length] == 0) {
        
        [filterDict setObject:@"" forKey:@"text"];
    }
    else{
        [filterDict setObject:searchString forKey:@"text"];
    }
    
    NSLog(@"FilterDict:%@",filterDict);
    
   // if (isSearch == YES) {
        
    
        [Client sharedClient].newFilterForMap=YES;
        [[Client sharedClient].user initWithDictionary:@{@"filterMap":filterDict}];

           [SharedVariables sharedInstance].filterPreviousDict = [SharedVariables sharedInstance].filterCurrentDict;
           [SharedVariables sharedInstance].filterCurrentDict = filterDict;
//    }
    
    NSLog(@"current:%@",[SharedVariables sharedInstance].filterCurrentDict);
    NSLog(@"previous:%@",[SharedVariables sharedInstance].filterPreviousDict);

}


/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

- (IBAction)ageSliderAction:(UISlider *)sender {
    
    isCurrent = YES;
    
    float number = [sender value];
    sliderValue = (int)(number);
    
    self.endAgeLblOutlet.text = [NSString stringWithFormat:@"%d", sliderValue];
    
    ageArray[1] = [NSString stringWithFormat:@"%d",sliderValue];
}

- (IBAction)searchBtnAction:(id)sender {
    
    [self.searchTextFieldOutlet resignFirstResponder];
    _resetTableView.hidden = YES;
    isSearch = YES;
    [self resumeFilter];
    [self.navigationController popViewControllerAnimated:YES];
}

- (IBAction)resetBtnAction:(id)sender {
    
    
    
//    if ([stateIdendifier  isEqual: @"new"]) {
//        
//        if (isCurrent == YES) {
//            
//            [self resumeFilter];
//            previosDict = [SharedVariables sharedInstance].filterPreviousDict;
//            isCurrent = NO;
//        }
//        
//        
//    }
//    
//    if (!([SharedVariables sharedInstance].filterPreviousDict == nil)) {
//        
//        resetArray= [[NSArray alloc] initWithObjects:@"Clear All",@"Go Previous", nil];
//    }
//    else
//    {
//    
//    if (isPreviousFilter == YES) {
//        
//        resetArray= [[NSArray alloc] initWithObjects:@"Clear All",@"Go Previous", nil];
//        
//    }
//    else
//    {
//    if (isCurrent == NO) {
//        
//        resetArray= [[NSArray alloc] initWithObjects:@"Clear All", nil];
//    }
//    else
//    {
//       resetArray= [[NSArray alloc] initWithObjects:@"Clear All",@"Go Previous", nil];
//        
//    }
//    }
//    }
//
    
    
        if ([SharedVariables sharedInstance].filterPreviousDict == nil) {
    
            resetArray= [[NSArray alloc] initWithObjects:@"Clear All", nil];
        }
        else
        {
    
            resetArray= [[NSArray alloc] initWithObjects:@"Clear All",@"Go Previous", nil];
            
        }

    dispatch_async(dispatch_get_main_queue(), ^{
        //This code will run in the main thread:
        CGRect frame = _resetTableView.frame;
        frame.size.height = _resetTableView.contentSize.height;
        _resetTableView.frame = frame;
    });
    
    [_resetTableView reloadData];
    
    
    if ([stateIdendifier isEqualToString:@"new"]) {
        
        [self resumeNewFilter];
    }
    
    if (isResetClicked == NO) {
        
        self.resetTableView.hidden = NO;
        isResetClicked = YES;
    }
    else
    {
        self.resetTableView.hidden = YES;
        isResetClicked = NO;
    }
    
}

- (IBAction)maleBtnAction:(id)sender {
    
    isCurrent = YES;
    stateIdendifier =@"new";
    [self.searchTextFieldOutlet resignFirstResponder];
    _resetTableView.hidden = YES;
    
    if (isResetClicked == YES) {
        isResetClicked = NO;
    }
    
    if (isMale == NO) {
        
        [self.maleBtnOutlet setImage:[UIImage imageNamed:@"toggle_on.png"] forState:UIControlStateNormal];
        
        isMale = YES;
        
        [genderArray addObject:@"0"];
        
    }
    else
    {
        
        
        [self.maleBtnOutlet setImage:[UIImage imageNamed:@"toggle_off.png"] forState:UIControlStateNormal];
        
        isMale = NO;
        
        NSUInteger index = 0;
        for(NSString *string in genderArray)
        {
            if ([string isEqualToString:@"0"]) {
                
                [genderArray removeObjectAtIndex:index];
                break;
            }
            
            index++;
        }
    }
    
    NSLog(@"M_genderArry:%@",genderArray);
}

- (IBAction)femaleBtnAction:(id)sender {
    
    isCurrent = YES;
    stateIdendifier =@"new";
    [self.searchTextFieldOutlet resignFirstResponder];
    _resetTableView.hidden = YES;
    if (isResetClicked == YES) {
        isResetClicked = NO;
    }
    
    if (isFemale == NO) {
        
        [self.femailBtnOutlet setImage:[UIImage imageNamed:@"toggle_on.png"] forState:UIControlStateNormal];
        
        isFemale = YES;
        
        [genderArray addObject:@"1"];
        
    }
    else
    {
        
        
        [self.femailBtnOutlet setImage:[UIImage imageNamed:@"toggle_off.png"] forState:UIControlStateNormal];
        
        isFemale = NO;
        
        NSUInteger index = 0;
        for(NSString *string in genderArray)
        {
            if ([string isEqualToString:@"1"]) {
                
                [genderArray removeObjectAtIndex:index];
                break;
            }
            
            index++;
        }
    }
    
    NSLog(@"F_genderArry:%@",genderArray);

}

- (IBAction)otherBtnAction:(id)sender {
    
    isCurrent = YES;
    stateIdendifier =@"new";
    [self.searchTextFieldOutlet resignFirstResponder];
    _resetTableView.hidden = YES;
    if (isResetClicked == YES) {
        isResetClicked = NO;
    }
    
    if (isOther == NO) {
        
        [self.otherBtnOutlet setImage:[UIImage imageNamed:@"toggle_on.png"] forState:UIControlStateNormal];
        
        isOther = YES;
        
        [genderArray addObject:@"2"];
    }
    else
    {
        
        
        [self.otherBtnOutlet setImage:[UIImage imageNamed:@"toggle_off.png"] forState:UIControlStateNormal];
        
        isOther = NO;
        
        NSUInteger index = 0;
        for(NSString *string in genderArray)
        {
            if ([string isEqualToString:@"2"]) {
                
                [genderArray removeObjectAtIndex:index];
                break;
            }
            
            index++;
        }
    }
    
    NSLog(@"O_genderArry:%@",genderArray);

}

- (IBAction)newFriendsBtnAction:(id)sender {
    
    isCurrent = YES;
    stateIdendifier =@"new";
    [self.searchTextFieldOutlet resignFirstResponder];
    _resetTableView.hidden = YES;
    if (isResetClicked == YES) {
        isResetClicked = NO;
    }
    
    if (isNewFriend == NO) {
        
        [self.friendsBtnOutlet setImage:[UIImage imageNamed:@"toggle_on.png"] forState:UIControlStateNormal];
        
        isNewFriend = YES;
        
        [intrestedInArray addObject:@"0"];
        
    }
    else
    {
        
        
        [self.friendsBtnOutlet setImage:[UIImage imageNamed:@"toggle_off.png"] forState:UIControlStateNormal];
        
        isNewFriend = NO;
        
        NSUInteger index = 0;
        for(NSString *string in intrestedInArray)
        {
            if ([string isEqualToString:@"0"]) {
                
                [intrestedInArray removeObjectAtIndex:index];
                break;
            }
            
            index++;
        }

    }
    
    NSLog(@"interested:%@",intrestedInArray);

}

- (IBAction)niceDateBtnAction:(id)sender {
    
    isCurrent = YES;
    stateIdendifier =@"new";
    [self.searchTextFieldOutlet resignFirstResponder];
    _resetTableView.hidden = YES;
    if (isResetClicked == YES) {
        isResetClicked = NO;
    }
    
    if (isNiceDate == NO) {
        
        [self.niceDateBtnOutlet setImage:[UIImage imageNamed:@"toggle_on.png"] forState:UIControlStateNormal];
        
        isNiceDate = YES;
        [intrestedInArray addObject:@"1"];
        
    }
    else
    {
        
        
        [self.niceDateBtnOutlet setImage:[UIImage imageNamed:@"toggle_off.png"] forState:UIControlStateNormal];
        
        isNiceDate = NO;
        
        NSUInteger index = 0;
        for(NSString *string in intrestedInArray)
        {
            if ([string isEqualToString:@"1"]) {
                
                [intrestedInArray removeObjectAtIndex:index];
                break;
            }
            
            index++;
        }
    }
    
    NSLog(@"interested:%@",intrestedInArray);

}

- (IBAction)crazyNightOutBtnAction:(id)sender {
    
    isCurrent = YES;
    stateIdendifier =@"new";
    [self.searchTextFieldOutlet resignFirstResponder];
    _resetTableView.hidden = YES;
    if (isResetClicked == YES) {
        isResetClicked = NO;
    }
    
    if (isCrazyNightOut == NO) {
        
        [self.crazyNightOutBtnoutlet setImage:[UIImage imageNamed:@"toggle_on.png"] forState:UIControlStateNormal];
        
        isCrazyNightOut = YES;
        [intrestedInArray addObject:@"2"];
        
    }
    else
    {
        
        
        [self.crazyNightOutBtnoutlet setImage:[UIImage imageNamed:@"toggle_off.png"] forState:UIControlStateNormal];
        
        isCrazyNightOut = NO;
        
        NSUInteger index = 0;
        for(NSString *string in intrestedInArray)
        {
            if ([string isEqualToString:@"2"]) {
                
                [intrestedInArray removeObjectAtIndex:index];
                break;
            }
            
            index++;
        }
    }
    
    NSLog(@"interested:%@",intrestedInArray);

}

- (IBAction)businessContactBtnAction:(id)sender {
    
    isCurrent = YES;
    stateIdendifier =@"new";
    [self.searchTextFieldOutlet resignFirstResponder];
    _resetTableView.hidden = YES;
    if (isResetClicked == YES) {
        isResetClicked = NO;
    }
    
    if (isBusinessContact == NO) {
        
        [self.businessContactBtnOutlet setImage:[UIImage imageNamed:@"toggle_on.png"] forState:UIControlStateNormal];
        
        isBusinessContact = YES;
        [intrestedInArray addObject:@"3"];
        
    }
    else
    {
        
        
        [self.businessContactBtnOutlet setImage:[UIImage imageNamed:@"toggle_off.png"] forState:UIControlStateNormal];
        
        isBusinessContact = NO;
        
        NSUInteger index = 0;
        for(NSString *string in intrestedInArray)
        {
            if ([string isEqualToString:@"3"]) {
                
                [intrestedInArray removeObjectAtIndex:index];
                break;
            }
            
            index++;
        }
    }
    
    NSLog(@"interested:%@",intrestedInArray);

}

- (IBAction)funEventsBtnAction:(id)sender {
    
    isCurrent = YES;
    stateIdendifier =@"new";
    [self.searchTextFieldOutlet resignFirstResponder];
    _resetTableView.hidden = YES;
    if (isResetClicked == YES) {
        isResetClicked = NO;
    }
    
    if (isFunEvents == NO) {
        
        [self.funEventsBtnOutlet setImage:[UIImage imageNamed:@"toggle_on.png"] forState:UIControlStateNormal];
        
        isFunEvents = YES;
        [intrestedInArray addObject:@"4"];
        
    }
    else
    {
        
        
        [self.funEventsBtnOutlet setImage:[UIImage imageNamed:@"toggle_off.png"] forState:UIControlStateNormal];
        
        isFunEvents = NO;
        
        NSUInteger index = 0;
        for(NSString *string in intrestedInArray)
        {
            if ([string isEqualToString:@"4"]) {
                
                [intrestedInArray removeObjectAtIndex:index];
                break;
            }
            
            index++;
        }
    }
    
    NSLog(@"interested:%@",intrestedInArray);

}

- (IBAction)whatEverHappensBtnAction:(id)sender {
    
    isCurrent = YES;
    stateIdendifier =@"new";
    [self.searchTextFieldOutlet resignFirstResponder];
    _resetTableView.hidden = YES;
    if (isResetClicked == YES) {
        isResetClicked = NO;
    }
    
    if (isWhatEverHappens == NO) {
        
        [self.whateverHappensBtnOutlet setImage:[UIImage imageNamed:@"toggle_on.png"] forState:UIControlStateNormal];
        
        isWhatEverHappens = YES;
        [intrestedInArray addObject:@"5"];
        
    }
    else
    {
        
        
        [self.whateverHappensBtnOutlet setImage:[UIImage imageNamed:@"toggle_off.png"] forState:UIControlStateNormal];
        
        isWhatEverHappens = NO;
        
        NSUInteger index = 0;
        for(NSString *string in intrestedInArray)
        {
            if ([string isEqualToString:@"5"]) {
                
                [intrestedInArray removeObjectAtIndex:index];
            }
            
            index++;
        }
    }
    
    NSLog(@"interested:%@",intrestedInArray);

}

- (void)textFieldDidBeginEditing:(UITextField *)textField {
    
    _resetTableView.hidden = YES;
    if (isResetClicked == YES) {
        isResetClicked = NO;
    }
    
}
- (void)textFieldDidEndEditing:(UITextField *)textField {

    
}

- (BOOL) textField: (UITextField *)theTextField shouldChangeCharactersInRange: (NSRange)range replacementString: (NSString *)string {
    
    isCurrent = YES;
    stateIdendifier =@"new";
    
    
    
    return YES;
}

-(BOOL) textFieldShouldReturn: (UITextField *) textField
{
    [textField resignFirstResponder];
    
    searchString = textField.text;
    
    return YES;
}

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 1;    //count of section
}


- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {

   return resetArray.count;
    //count number of row from counting array hear cataGorry is An Array
}



- (UITableViewCell *)tableView:(UITableView *)tableView
         cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    UITableViewCell *cell = [[UITableViewCell alloc] init];
    cell = (UITableViewCell *) [tableView dequeueReusableCellWithIdentifier:@"Cell"];
    
    UILabel *lbldescription = (UILabel *)[cell viewWithTag:1];
    lbldescription.text = [resetArray objectAtIndex:indexPath.row];
    
    cell.selectionStyle = UITableViewCellSelectionStyleNone;

    return cell;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    
//    if (isCurrent == NO) {
//        
//        if (isPreviousFilter == NO) {
//            
//            isCurrent = NO;
//            //isPreviousFilter = NO;
//        }
//        else
//        {
//        
//        isCurrent = YES;
//        }
//    }
    
    
    NSString* indexString = [resetArray objectAtIndex:indexPath.row];
    
   // @"Clear All",@"Go Previous"
    
    if ([indexString  isEqual: @"Clear All"]) {
        
        [self clearAll];
        
    }
    
    else if ([indexString  isEqual: @"Go Previous"])
    {
        
        if ([stateIdendifier  isEqual:@"new"]) {
            
           previosDict = chagedPreviousDict;
            stateIdendifier =@"";
        }
        
        else if ([stateIdendifier  isEqual:@"previous"]) {
            
            previosDict = [SharedVariables sharedInstance].filterPreviousDict;
            
            chagedPreviousDict = [SharedVariables sharedInstance].filterPreviousDict;
            
            if ([SharedVariables sharedInstance].filterPreviousDict == nil) {
                
               stateIdendifier =@"previous";
            }
            else
            {
            stateIdendifier =@"current";
            }
        }
        else if ([stateIdendifier  isEqual:@"current"]) {
            
           previosDict = [SharedVariables sharedInstance].filterCurrentDict;
            
           chagedPreviousDict = [SharedVariables sharedInstance].filterCurrentDict;
            
            stateIdendifier =@"previous";
        }
        else
        {
            previosDict = newDict;
            stateIdendifier =@"new";
        }
        
        
        NSLog(@"preDict:%@",previosDict);
        [self previousFilter];
    }
    
    self.resetTableView.hidden = YES;
    isResetClicked = NO;
    
}

- (void)scrollViewDidScroll:(UIScrollView *)scrollView
{
    
    [self.searchTextFieldOutlet resignFirstResponder];
    _resetTableView.hidden = YES;
    
    if (isResetClicked == YES) {
        isResetClicked = NO;
    }
    
}

-(void)clearAll
{
   // if (ageArray.count > 1) {
        
        [ageArray removeAllObjects];
   // }
        [genderArray removeAllObjects];
        [intrestedInArray removeAllObjects];
    
        searchString = @"";
        self.searchTextFieldOutlet.text = @"";
        self.endAgeLblOutlet.text  = @"";
    
        [filterDict setObject:@""  forKey:@"age"];
        [filterDict setObject:@""  forKey:@"interested_in"];
        [filterDict setObject:@""  forKey:@"sex"];
        [filterDict setObject:@""  forKey:@"text"];
    
        isMale = NO;
        isFemale = NO;
        isOther = NO;
        isNewFriend = NO;
        isNiceDate = NO;
        isCrazyNightOut = NO;
        isBusinessContact = NO;
        isFunEvents = NO;
        isWhatEverHappens = NO;
        isResetClicked = NO;
    
        [self.maleBtnOutlet setImage:[UIImage imageNamed:@"toggle_off.png"] forState:UIControlStateNormal];
        [self.femailBtnOutlet setImage:[UIImage imageNamed:@"toggle_off.png"] forState:UIControlStateNormal];
        [self.otherBtnOutlet setImage:[UIImage imageNamed:@"toggle_off.png"] forState:UIControlStateNormal];
        [self.friendsBtnOutlet setImage:[UIImage imageNamed:@"toggle_off.png"] forState:UIControlStateNormal];
        [self.niceDateBtnOutlet setImage:[UIImage imageNamed:@"toggle_off.png"] forState:UIControlStateNormal];
        [self.crazyNightOutBtnoutlet setImage:[UIImage imageNamed:@"toggle_off.png"] forState:UIControlStateNormal];
        [self.businessContactBtnOutlet setImage:[UIImage imageNamed:@"toggle_off.png"] forState:UIControlStateNormal];
        [self.funEventsBtnOutlet setImage:[UIImage imageNamed:@"toggle_off.png"] forState:UIControlStateNormal];
        [self.whateverHappensBtnOutlet setImage:[UIImage imageNamed:@"toggle_off.png"] forState:UIControlStateNormal];
        
    
    self.sliderRange.selectedMinimum = 18;
    self.sliderRange.selectedMaximum = 65;
    
    self.startAgeLblOutlet.text = @"18";
    self.endAgeLblOutlet.text = @"65";
    
    //isPreviousFilter = NO;
    isCurrent = NO;
    
    [SharedVariables sharedInstance].filterPreviousDict = nil;
    [SharedVariables sharedInstance].filterCurrentDict = nil;
        
    //[self resumeFilter];
}
-(void)previousFilter
{
   // if (isCurrent == NO) {
        
        [self goPrevious];
       // isCurrent = YES;
//    }
//    else
//    {
//        float number = [self.ageSliderOutlet value];
//        sliderPreValue = (int)(number);
        
//        isPreviousFilter = YES;
//        
//        startPreAge = self.sliderRange.selectedMinimum;
//        endPreAge = self.sliderRange.selectedMaximum;
//        
//        searchPreString = self.searchTextFieldOutlet.text;
//      
//        
//        if (isMale == YES) {
//            isPreMale = YES;
//        }
//        else
//        {
//            isPreMale = NO;
//        }
//        
//        if (isFemale == YES) {
//            isPreFemale = YES;
//        }
//        else
//        {
//            isPreFemale = NO;
//        }
//        
//        if (isOther == YES) {
//            isPreOther = YES;
//        }
//        else
//        {
//            isPreOther = NO;
//        }
//        
//        if (isNewFriend == YES) {
//            isPreNewFriend = YES;
//        }
//        else
//        {
//            isPreNewFriend = NO;
//        }
//        
//        if (isNiceDate == YES) {
//            isPreNiceDate = YES;
//        }
//        else
//        {
//            isPreNiceDate = NO;
//        }
//        
//        if (isCrazyNightOut == YES) {
//            isPreCrazyNightOut = YES;
//        }
//        else
//        {
//            isPreCrazyNightOut = NO;
//        }
//        if (isBusinessContact == YES) {
//            isPreBusinessContact = YES;
//        }
//        else
//        {
//            isPreBusinessContact = NO;
//        }
//        if (isFunEvents == YES) {
//            isPreFunEvents = YES;
//        }
//        else
//        {
//            isPreFunEvents = NO;
//        }
//        if (isWhatEverHappens == YES) {
//            isPreWhatEverHappens = YES;
//        }
//        else
//        {
//            isPreWhatEverHappens = NO;
//        }
        
//        [self viewDidLoad];
//        isCurrent = NO;
//    }
}

-(void)goPrevious
{
    
    isMale = NO;
    isFemale = NO;
    isOther = NO;
    isNewFriend = NO;
    isNiceDate = NO;
    isCrazyNightOut = NO;
    isBusinessContact = NO;
    isFunEvents = NO;
    isWhatEverHappens = NO;
    //isResetClicked = NO;
    
    
    //     isPreMale= NO;
    //     isPreFemale= NO;
    //     isPreOther= NO;
    //     isPreNewFriend= NO;
    //     isPreNiceDate= NO;
    //     isPreCrazyNightOut= NO;
    //     isPreBusinessContact= NO;
    //     isPreFunEvents= NO;
    //     isPreWhatEverHappens= NO;
    
    
    [self.maleBtnOutlet setImage:[UIImage imageNamed:@"toggle_off.png"] forState:UIControlStateNormal];
    [self.femailBtnOutlet setImage:[UIImage imageNamed:@"toggle_off.png"] forState:UIControlStateNormal];
    [self.otherBtnOutlet setImage:[UIImage imageNamed:@"toggle_off.png"] forState:UIControlStateNormal];
    [self.friendsBtnOutlet setImage:[UIImage imageNamed:@"toggle_off.png"] forState:UIControlStateNormal];
    [self.niceDateBtnOutlet setImage:[UIImage imageNamed:@"toggle_off.png"] forState:UIControlStateNormal];
    [self.crazyNightOutBtnoutlet setImage:[UIImage imageNamed:@"toggle_off.png"] forState:UIControlStateNormal];
    [self.businessContactBtnOutlet setImage:[UIImage imageNamed:@"toggle_off.png"] forState:UIControlStateNormal];
    [self.funEventsBtnOutlet setImage:[UIImage imageNamed:@"toggle_off.png"] forState:UIControlStateNormal];
    [self.whateverHappensBtnOutlet setImage:[UIImage imageNamed:@"toggle_off.png"] forState:UIControlStateNormal];
    
    filterDict = [[NSMutableDictionary alloc]init];
    
    genderArray = [[NSMutableArray alloc] init];
    ageArray = [[NSMutableArray alloc] init];
    intrestedInArray = [[NSMutableArray alloc] init];
    resetArray = [[NSArray alloc] init];
    
    resetArray= [[NSArray alloc] initWithObjects:@"Clear All",@"Go Previous", nil];
    
    [filterDict setObject:@""  forKey:@"age"];
    [filterDict setObject:@""  forKey:@"interested_in"];
    [filterDict setObject:@""  forKey:@"sex"];
    [filterDict setObject:@""  forKey:@"text"];
    
    
    
   // filterDic =
    //[Client sharedClient].user.filterMap;
    
    
    //filterDic =
    //[SharedVariables sharedInstance].filterPreviousDict;
    
    
    NSString* age = [previosDict objectForKey:@"age"];
    NSString* interested = [previosDict objectForKey:@"interested_in"];
    NSString* sex = [previosDict objectForKey:@"sex"];
    NSString* seachTest = [previosDict objectForKey:@"text"];
    
    if (seachTest.length > 0) {
        
        self.searchTextFieldOutlet.text = seachTest;
        searchString = seachTest;
    }
    else
    {
        self.searchTextFieldOutlet.text = @"";
        searchString = @"";
    }
    
    NSArray*ageArr = [age componentsSeparatedByString:@";"];
    NSArray*interestedArr = [interested componentsSeparatedByString:@";"];
    NSArray*sexArr = [sex componentsSeparatedByString:@";"];
    
    
    self.sliderRange.minValue = 18;
    self.sliderRange.maxValue = 65;
    self.sliderRange.delegate = self;
    
    if (ageArr.count ==2) {
        
        NSInteger ageValue = [(ageArr[0]) integerValue];
        NSInteger ageValue1 = [(ageArr[1]) integerValue];
        
        self.sliderRange.selectedMinimum = ageValue;
        self.sliderRange.selectedMaximum = ageValue1;
        
        self.startAgeLblOutlet.text = [NSString stringWithFormat:@"%ld",(long)ageValue];
        self.endAgeLblOutlet.text = [NSString stringWithFormat:@"%ld",(long)ageValue1];
    }
    else
    {
        self.sliderRange.selectedMinimum = 18;
        self.sliderRange.selectedMaximum = 65;
        
        self.startAgeLblOutlet.text = @"18";
        self.endAgeLblOutlet.text = @"65";
    }
    
    if (ageArr.count == 2) {
        
        for (NSString*age in ageArr) {
            
            [ageArray addObject:age];
        }
    }
    else
    {
        [ageArray addObject:@"18"];
        [ageArray addObject:@"65"];
    }

//    if (ageArr.count > 1) {
//        
//        NSInteger ageValue = [(ageArr[1]) integerValue];
//        self.endAgeLblOutlet.text = (ageArr[1]);
//        
//        //self.ageSliderOutlet.value = ageValue;
//    }
//    else
//    {
//        self.endAgeLblOutlet.text = @"65";
//        
//        //self.ageSliderOutlet.value = 0;
//    }
    
    
    
    
    for (NSString*sexvalue in sexArr) {
        
        if ([sexvalue isEqualToString:@"0"]) {
            
            [self.maleBtnOutlet setImage:[UIImage imageNamed:@"toggle_on.png"] forState:UIControlStateNormal];
            
            [genderArray addObject:@"0"];
            
            isMale = YES;
            
        }
        else if ([sexvalue isEqualToString:@"1"]) {
            
            [self.femailBtnOutlet setImage:[UIImage imageNamed:@"toggle_on.png"] forState:UIControlStateNormal];
            
            [genderArray addObject:@"1"];
            isFemale = YES;
        }
        else if ([sexvalue isEqualToString:@"2"]) {
            
            [self.otherBtnOutlet setImage:[UIImage imageNamed:@"toggle_on.png"] forState:UIControlStateNormal];
            
            [genderArray addObject:@"2"];
            isOther = YES;
        }
    }
    
    for (NSString*interestedvalue in interestedArr) {
        
        if ([interestedvalue isEqualToString:@"0"]) {
            
            [self.friendsBtnOutlet setImage:[UIImage imageNamed:@"toggle_on.png"] forState:UIControlStateNormal];
            [intrestedInArray addObject:@"0"];
            
            isNewFriend = YES;
            
            
        }
        else if ([interestedvalue isEqualToString:@"1"]) {
            
            [self.niceDateBtnOutlet setImage:[UIImage imageNamed:@"toggle_on.png"] forState:UIControlStateNormal];
            
            [intrestedInArray addObject:@"1"];
            
            isNiceDate = YES;
            
            
        }
        else if ([interestedvalue isEqualToString:@"2"]) {
            
            [self.crazyNightOutBtnoutlet setImage:[UIImage imageNamed:@"toggle_on.png"] forState:UIControlStateNormal];
            [intrestedInArray addObject:@"2"];
            isCrazyNightOut = YES;
            
        }
        else if ([interestedvalue isEqualToString:@"3"]) {
            
            [self.businessContactBtnOutlet setImage:[UIImage imageNamed:@"toggle_on.png"] forState:UIControlStateNormal];
            [intrestedInArray addObject:@"3"];
            isBusinessContact = YES;
            
        }
        else if ([interestedvalue isEqualToString:@"4"]) {
            
            [self.funEventsBtnOutlet setImage:[UIImage imageNamed:@"toggle_on.png"] forState:UIControlStateNormal];
            [intrestedInArray addObject:@"4"];
            isFunEvents = YES;
            
        }
        else if ([interestedvalue isEqualToString:@"5"]) {
            
            [self.whateverHappensBtnOutlet setImage:[UIImage imageNamed:@"toggle_on.png"] forState:UIControlStateNormal];
            [intrestedInArray addObject:@"5"];
            isWhatEverHappens = YES;
        }
    }

    
    
 
}


@end
