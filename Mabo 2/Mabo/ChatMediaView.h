//
//  ChatMediaView.h
//  Mabo
//
//  Created by vishnuprasathg on 11/29/17.
//  Copyright © 2017 DCI. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface ChatMediaView : UIView
- (IBAction)takePhotoBtnAction:(id)sender;
- (IBAction)selectPhotoBtnAction:(id)sender;
- (IBAction)takeVideoBtnAction:(id)sender;
- (IBAction)selectVideoBtnAction:(id)sender;

@property (strong, nonatomic) UIViewController * alertParentViewController;

@end
