//
//  ChatMediaView.m
//  Mabo
//
//  Created by vishnuprasathg on 11/29/17.
//  Copyright © 2017 DCI. All rights reserved.
//

#import "ChatMediaView.h"
#import "ChatViewController.h"
#import "Utilities.h"

@implementation ChatMediaView

/*
// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect {
    // Drawing code
}
*/

- (IBAction)takePhotoBtnAction:(id)sender {
    
    if ([_alertParentViewController isKindOfClass: ChatViewController.class]) {
        
        [[Utilities sharedInstance] hideCustomAlertView:self.superview];
        
        ChatViewController * chatVC = _alertParentViewController;
        
        [chatVC openCamera];
        
        
    }
}

- (IBAction)selectPhotoBtnAction:(id)sender {
    
    if ([_alertParentViewController isKindOfClass: ChatViewController.class]) {
        
        [[Utilities sharedInstance] hideCustomAlertView:self.superview];
        
        ChatViewController * chatVC = _alertParentViewController;
        
        [chatVC openGallery];
        
        
    }
}

- (IBAction)takeVideoBtnAction:(id)sender {
    
    if ([_alertParentViewController isKindOfClass: ChatViewController.class]) {
        
        [[Utilities sharedInstance] hideCustomAlertView:self.superview];
        
        ChatViewController * chatVC = _alertParentViewController;
        
        [chatVC openVideoCamera];

    }
}

- (IBAction)selectVideoBtnAction:(id)sender {
    
    if ([_alertParentViewController isKindOfClass: ChatViewController.class]) {
        
        [[Utilities sharedInstance] hideCustomAlertView:self.superview];
        
        ChatViewController * chatVC = _alertParentViewController;
        
        [chatVC openVideoRoll];
        
        
    }
}
@end
