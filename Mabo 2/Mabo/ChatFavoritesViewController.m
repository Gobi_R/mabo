//
//  ChatFavoritesViewController.m
//  Mabo
//
//  Created by vishnuprasathg on 11/23/17.
//  Copyright © 2017 DCI. All rights reserved.
//

#import "ChatFavoritesViewController.h"
#import "Client.h"
#import "UIImageView+WebCache.h"
#import "UtilityGraphic.h"
#import <Firebase/Firebase.h>
#import "NSDate+TimeAgo.h"
#import "Utilities.h"
//#import "UIImageView+AFNetworking.h"
#import "SearchChatUserViewController.h"
#import "UIImageView+WebCache.h"


#import "UserProfileViewController.h"
#import "ChatViewController.h"

@interface ChatFavoritesViewController ()<UITableViewDelegate,UITableViewDataSource>

@property (nonatomic, strong) NSMutableArray *chatList;

@property (nonatomic, strong) Firebase *connectRef;
@property (nonatomic, strong) Firebase *lastOnlineRef;
@property (nonatomic, strong) Firebase *connectRefObserv;

@end

@implementation ChatFavoritesViewController

{
    NSMutableArray * favoritesArray;
    
}

- (void)viewDidLoad {
    [super viewDidLoad];
    
    self.segmentOutlet.selectedSegmentIndex = 1;
    favoritesArray = [[NSMutableArray alloc] init];
    
    self.tableViewOutlet.delegate = self;
    self.tableViewOutlet.dataSource = self;
    
    
    NSArray *friendsArray = [Client sharedClient].user.friends;
    
    favoritesArray = [friendsArray mutableCopy];
    
    self.chatList = [NSMutableArray arrayWithArray:[UtilityGraphic filterWithKey:@"status" withValue:@"0" inArray:[Client sharedClient].user.friends withType:1]];
    
    NSLog(@"chatList:%@",self.chatList);
    
//}else{
//    self.chatList = [self sortArray:[[[Client sharedClient] getMessagesList] mutableCopy]];
    
    // Do any additional setup after loading the view.
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}


/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/


#pragma mark - TABLEVIEW DELEGATE METHODS


-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    
    return self.chatList.count;
    
}

-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    
    UITableViewCell *cellReal = [tableView dequeueReusableCellWithIdentifier:@"Cell"];
    
    if (self.chatList.count > 0) {
        
        NSDictionary* recentDict = [self.chatList objectAtIndex:indexPath.row];
        
        UIImageView* profileImageView = [cellReal viewWithTag:1];
        UILabel * nameLbl = [cellReal viewWithTag:2];
        UILabel* lastSeen = [cellReal viewWithTag:3];
        
        [lastSeen setText:@"Offline"];
        lastSeen.textColor = [UIColor redColor];
        
        NSString *picture = [recentDict objectForKey:@"picture_path"];
        
        profileImageView.layer.cornerRadius = profileImageView.frame.size.height / 2;
        profileImageView.layer.masksToBounds = YES;
        
        NSLog(@"%@",[recentDict objectForKey:@"extended_name"]);
        [nameLbl setText:[recentDict objectForKey:@"extended_name"]];
    
        dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_BACKGROUND, 0), ^{
            // retrive image on global queue
            if (picture.length > 0) {
                
                NSString * urlstring = [NSString stringWithFormat:@"http://34.230.105.103:8002/%@", picture];
                NSURL *imageURL = [NSURL URLWithString:urlstring];
                UIImage * img = [UIImage imageWithData:[NSData dataWithContentsOfURL:[NSURL URLWithString:urlstring]]];
                
                dispatch_async(dispatch_get_main_queue(), ^{
                    
                    profileImageView.layer.cornerRadius = profileImageView.frame.size.height / 2;
                    profileImageView.layer.masksToBounds = YES;
                    
                    // assign cell image on main thread
                    //[profileImageView sd_setImageWithURL:imageURL placeholderImage:[UIImage imageNamed:@"avatar2.png"]];
                    [profileImageView setImageWithURL:imageURL placeholderImage:[UIImage imageNamed:@"avatar3.png"]];
                    //[cell.textPostProfileImageView setImageWithURL:imageURL placeholderImage:[UIImage imageNamed:@"avatar2.png"]];
                });
            }
            
        });
        
            [self.connectRef removeAllObservers];
            [self.connectRefObserv removeAllObservers];
            [self.lastOnlineRef removeAllObservers];
            self.connectRef=nil;
            self.connectRefObserv=nil;
            self.lastOnlineRef=nil;
        
            self.connectRef = [[Firebase alloc] initWithUrl:@"https://mabo-7.firebaseio.com"];
        
            self.connectRef = [self.connectRef childByAppendingPath:[NSString stringWithFormat:@"users/%@/connections",[UtilityGraphic URLEncodedString:[recentDict objectForKey:@"username"]]]];
        
        NSLog(@"%@",[NSString stringWithFormat:@"users/%@/connections",[UtilityGraphic URLEncodedString:[recentDict objectForKey:@"username"]]]);
        
            [self.connectRef observeEventType:FEventTypeValue withBlock:^(FDataSnapshot *snapshot) {
                
                if(![snapshot.value isEqual:[NSNull null]]){
                    
                    [lastSeen setText:@"Online"];
                    lastSeen.textColor = [UIColor greenColor];
                     // [lastSeen setTextColor:COLOR_MB_GREEN];
                }else{
                    [lastSeen setText:@"Offline"];
                   // [self.labelConnect setTextColor:COLOR_MB_LIGHTGRAY];
                    self.lastOnlineRef = [[Firebase alloc] initWithUrl:@"https://mabo-7.firebaseio.com"];
                    self.lastOnlineRef = [self.lastOnlineRef  childByAppendingPath:[NSString stringWithFormat:@"users/%@/lastOnLine",[UtilityGraphic URLEncodedString:[recentDict objectForKey:@"username"]]]];
                    
                    [self.lastOnlineRef observeEventType:FEventTypeValue withBlock:^(FDataSnapshot *snapshot) {
                        if(![snapshot.value isEqual:[NSNull null]]){
                            long long value = [snapshot.value longLongValue];
                            NSDate *date = [[NSDate alloc] initWithTimeIntervalSince1970:value/1000];
                            
                            NSDateFormatter *fmt = [[NSDateFormatter alloc] init];
                            [fmt setDateFormat:@"dd MMMM yyyy"] ;
                            fmt.timeZone = [NSTimeZone systemTimeZone];
                            NSString *local = [fmt stringFromDate:date];
                            NSLog(@"%@", local);
                            
                            NSMutableAttributedString * value1 = [[Utilities sharedInstance]loadTimeDifferent:date];
                            NSString * validateString = [NSString stringWithFormat:@"last seen %@",value1];
                            
                            validateString = [validateString substringToIndex:[validateString length] - 3];
                            [lastSeen setText:validateString];
                           // [lastSeen setText:[NSString stringWithFormat:@"%@: %@",NSLocalizedString(@"Last seen",nil),ago]];
                        }
                        else
                        {
                            [lastSeen setText:@"Offline"];
                            lastSeen.textColor = [UIColor redColor];
                            NSLog(@"Offline");
                            
                            
                        }
                    }];
                }
            }];
            
            self.connectRefObserv = [[Firebase alloc] initWithUrl:@"https://mabo-7.firebaseio.com"];
            self.connectRefObserv = [self.connectRefObserv childByAppendingPath:[NSString stringWithFormat:@"users/%@/connections",[UtilityGraphic URLEncodedString:[recentDict objectForKey:@"username"]]]];
        
        NSLog(@"%@",[NSString stringWithFormat:@"users/%@/connections",[UtilityGraphic URLEncodedString:[recentDict objectForKey:@"username"]]]);
            [self.connectRef observeEventType:FEventTypeChildChanged withBlock:^(FDataSnapshot *snapshot) {
                if(![snapshot.value isEqual:[NSNull null]]){
                    [lastSeen setText:@"Online"];
                    lastSeen.textColor = [UIColor greenColor];
                   // [lastSeen setTextColor:COLOR_MB_GREEN];
                }else{
                    [lastSeen setText:@"Offline"];
                   // [self.labelConnect setTextColor:COLOR_MB_LIGHTGRAY];
                    self.lastOnlineRef = [[Firebase alloc] initWithUrl:@"https://mabo-7.firebaseio.com"];
                    self.lastOnlineRef = [self.lastOnlineRef  childByAppendingPath:[NSString stringWithFormat:@"users/%@/lastOnLine",[UtilityGraphic URLEncodedString:[recentDict objectForKey:@"username"]]]];
                    [self.lastOnlineRef observeEventType:FEventTypeValue withBlock:^(FDataSnapshot *snapshot) {
                        if(![snapshot.value isEqual:[NSNull null]]){
                            long long value = [snapshot.value longLongValue];
                            NSDate *date = [[NSDate alloc] initWithTimeIntervalSince1970:value/1000];
                           // NSString *ago = [date timeAgo];
                            NSDateFormatter *fmt = [[NSDateFormatter alloc] init];
                            [fmt setDateFormat:@"dd MMMM yyyy"] ;
                            fmt.timeZone = [NSTimeZone systemTimeZone];
                            NSString *local = [fmt stringFromDate:date];
                            NSLog(@"%@", local);
                            NSMutableAttributedString * value1 = [[Utilities sharedInstance]loadTimeDifferent:date];                           // NSString *ago = [date timeAgo];
                            NSString * validateString = [NSString stringWithFormat:@"last seen %@",value1];
                            validateString = [validateString substringToIndex:[validateString length] - 3];
                            [lastSeen setText:validateString];
                          //  [lastSeen setText:[NSString stringWithFormat:@"%@: %@",NSLocalizedString(@"Last seen",nil),ago]];
                        }
                        else
                        {
                            [lastSeen setText:@"Offline"];
                            lastSeen.textColor = [UIColor redColor];
                            NSLog(@"Offline");
                        }
                    }];
                    
                }
                
            }];


        }
    
    return cellReal;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    UITableViewCell *cellReal = [tableView dequeueReusableCellWithIdentifier:@"Cell"];
    
    return cellReal.frame.size.height;
}

-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    
    NSDictionary*dictionaryPostSelected = self.chatList[indexPath.row];
    
    NSLog(@"dict:%@",dictionaryPostSelected);
    
    ChatViewController * chatVC = [self.storyboard instantiateViewControllerWithIdentifier:@"ChatViewController"];
    chatVC.userDictionaryToChat = dictionaryPostSelected;
    [self.navigationController pushViewController:chatVC animated:YES];
    
}


- (IBAction)segmentValueChanged:(id)sender {
    
    
    if(self.segmentOutlet.selectedSegmentIndex==0){
        
        [self.navigationController popViewControllerAnimated:NO];
        
    }
    else if(self.segmentOutlet.selectedSegmentIndex==1){
        
        
    }

}
- (IBAction)plusBtnAction:(id)sender {
    
    SearchChatUserViewController * chatSearchVC = [self.storyboard instantiateViewControllerWithIdentifier:@"SearchChatUserViewController"];
    [self.navigationController pushViewController:chatSearchVC animated:YES];
}

- (IBAction)backBtnAction:(id)sender {
    
    [self.navigationController popViewControllerAnimated:NO];
}
- (IBAction)profileBtnAction:(id)sender {
    
    UIView* baseView = [sender superview];
    UIView* cell = [baseView superview];
    
    NSIndexPath *indexPath = [self.tableViewOutlet indexPathForCell:cell];
    
    NSDictionary*dictionaryPostSelected = self.chatList[indexPath.row];
    
    NSLog(@"dict:%@",dictionaryPostSelected);
    
    UserProfileViewController * userProfileVC = [self.storyboard instantiateViewControllerWithIdentifier:@"UserProfileViewController"];
    userProfileVC.userDict = dictionaryPostSelected;
   // [self.navigationController pushViewController:userProfileVC animated:YES];
    [self.navigationController presentViewController:userProfileVC animated:YES completion:nil];
}
@end
