//
//  ActivityView.h
//  Mabo
//
//  Created by Steewe MacBook Pro on 28/04/15.
//  Copyright (c) 2015 Steewe. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface ActivityView : UIView

@property (nonatomic, strong) UIColor *colorBase;
@property (nonatomic, strong) UIColor *colorCircles;
@property (nonatomic, strong) CAShapeLayer *circles;
@property float lineWidthCircles;
@property CGPoint centerPoint;

-(void)show;
-(void)startAnimation;
-(void)hide;
-(void)setCenterActivity:(CGPoint)centerPoint;

@end
