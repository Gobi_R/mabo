//
//  LikesViewController.m
//  Mabo
//
//  Created by vishnuprasathg on 11/13/17.
//  Copyright © 2017 DCI. All rights reserved.
//

#import "LikesViewController.h"
#import "Client.h"
#import "SharedVariables.h"
#import "LoadingView.h"
//#import "UIImageView+WebCache.h"
#import <SDWebImage/UIImageView+WebCache.h>
#import "UserProfileViewController.h"

@interface LikesViewController ()

@property (nonatomic, strong) NSMutableDictionary *dictionaryLikes;
@property (nonatomic, strong) NSMutableArray *likesArray;
@property (nonatomic, strong) LoadingView *loading;

@end

@implementation LikesViewController


- (void)viewDidLoad {
    [super viewDidLoad];
    
    _likesArray = [[NSMutableArray alloc] init];
    
    self.tableViewOutlet.delegate = self;
    self.tableViewOutlet.dataSource = self;
    
    
    
    // Do any additional setup after loading the view.
}

-(void)viewWillAppear:(BOOL)animated
{
    self.loading = [[LoadingView alloc] initWithFrame:[UIScreen mainScreen].bounds];
    [self.view addSubview:self.loading];
    [self.loading animate:YES];
    
    [[Client sharedClient] getMyLikes:self.wallIdString completition:^(BOOL success, NSError *error) {
        
        if (success) {
            [self.loading animate:NO];
            NSLog(@"%@",[SharedVariables sharedInstance].likesDict);
            
            self.dictionaryLikes = [[SharedVariables sharedInstance].likesDict objectForKey:@"data"];
            
            self.likesArray = [[self.dictionaryLikes objectForKey:@"likedUsers"] mutableCopy];
            NSLog(@"WallMessages:%@",self.likesArray);
            
            [self.tableViewOutlet reloadData];
            // [self populateContent];
        }
        else
        {
            [self.loading animate:NO];
        }
        
    }];

}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}


#pragma mark - TABLEVIEW DELEGATE METHODS


-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    
    return _likesArray.count;
    
}

-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    
    UITableViewCell *cellReal = [tableView dequeueReusableCellWithIdentifier:@"Cell"];
    
    if (_likesArray.count > 0) {
        
        NSDictionary* likesDict = [_likesArray objectAtIndex:indexPath.row];
        
        UIImageView* profileImageView = [cellReal viewWithTag:1];
        UILabel * nameLbl = [cellReal viewWithTag:2];
        
        NSString* name = [likesDict objectForKey:@"extended_name"];
        NSString* picture = [likesDict objectForKey:@"picture_path"];
        
        
        nameLbl.text = name;
        
        dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_BACKGROUND, 0), ^{
           
            
            NSString * urlstring = [NSString stringWithFormat:@"http://34.230.105.103:8002/%@", picture];
            NSURL *imageURL = [NSURL URLWithString:urlstring];
            UIImage * img = [UIImage imageWithData:[NSData dataWithContentsOfURL:[NSURL URLWithString:urlstring]]];
            
            dispatch_async(dispatch_get_main_queue(), ^{
                
                profileImageView.layer.cornerRadius = profileImageView.frame.size.height / 2;
                profileImageView.layer.masksToBounds = YES;
                
                // assign cell image on main thread
                [profileImageView sd_setImageWithURL:imageURL placeholderImage:[UIImage imageNamed:@"avatar2.png"]];
                //[cell.textPostProfileImageView setImageWithURL:imageURL placeholderImage:[UIImage imageNamed:@"avatar2.png"]];
            });
        });

           }
    
    return cellReal;
}


-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    
    NSDictionary * selectedDict = _likesArray[indexPath.row];
    
    NSLog(@"dict:%@",selectedDict);
    
    UserProfileViewController * userProfileVC = [self.storyboard instantiateViewControllerWithIdentifier:@"UserProfileViewController"];
    userProfileVC.userDict = selectedDict;
   // [self.navigationController pushViewController:userProfileVC animated:YES];
    [self.navigationController presentViewController:userProfileVC animated:YES completion:nil];
}


/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

- (IBAction)profilePicAction:(id)sender {
    
    
    UIView* baseView = [sender superview];
    UIView* cell = [baseView superview];
    
    NSIndexPath *indexPath = [self.tableViewOutlet indexPathForCell:cell];
    
    NSDictionary * selectedDict = _likesArray[indexPath.row];
    
    NSLog(@"dict:%@",selectedDict);
    
    UserProfileViewController * userProfileVC = [self.storyboard instantiateViewControllerWithIdentifier:@"UserProfileViewController"];
    userProfileVC.userDict = selectedDict;
    //[self.navigationController pushViewController:userProfileVC animated:YES];
    [self.navigationController presentViewController:userProfileVC animated:YES completion:nil];

}

- (IBAction)backBtnAction:(id)sender {
    
    [self.navigationController popViewControllerAnimated:YES];
}
@end
