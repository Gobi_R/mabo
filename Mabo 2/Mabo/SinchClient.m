//
//  SinchClient.m
//  honk
//
//  Created by Steewe MacBook Pro on 08/01/15.
//  Copyright (c) 2015 Steewe. All rights reserved.
//

#import "SinchClient.h"
#import "Client.h"
#import "ChatViewController.h"
#import "AppDelegate.h"
#import "UtilityGraphic.h"
#import "SharedVariables.h"
//#import "NavigationController.h"
//#import "TabBarController.h"
//#import "ChatListViewController.h"
#import "ChatRecentsViewController.h"
#import "TabBarViewController.h"
#import "LoadingView.h"
#import "CallViewController.h"

@interface SinchClient()

@property (nonatomic, strong) LoadingView *loading;

@end

@implementation SinchClient

static SinchClient* _sharedClient = nil;

+ (SinchClient*)sharedClient{
    @synchronized([SinchClient class]){
        if (!_sharedClient)
            _sharedClient = [[self alloc] init];
        return _sharedClient;
    }
    return nil;
}

+ (id)alloc{
    @synchronized([SinchClient class]){
        NSAssert(_sharedClient == nil, @"Attempted to allocate a second instance of a singleton.");
        _sharedClient = [super alloc];
        return _sharedClient;
    }
    return nil;
}

-(void)activeClient{
    
    NSString *userId = [NSString stringWithFormat:@"%@",[Client sharedClient].user.email];
//    self.client = [Sinch clientWithApplicationKey:[[[NSBundle mainBundle] infoDictionary] objectForKey:@"SinchApplicationKey"]
//                                              applicationSecret:[[[NSBundle mainBundle] infoDictionary] objectForKey:@"SinchApplicationSecret"]
//                                                environmentHost:@"sandbox.sinch.com"
//                                                         userId:userId];
    if (userId.length > 0) {
        
        self.client = [Sinch clientWithApplicationKey:@"fbcb743b-55d6-49fa-8adb-a51a7b8d7b7b"
                                    applicationSecret:@"Xs/gjiD8BUSitNPuE9ag+w=="
                                      environmentHost:@"sandbox.sinch.com"
                                               userId:userId];
        
        //    self.client = [Sinch clientWithApplicationKey:@"c53580d5-0f31-4ba5-810d-50ed53416aeb"
        //                            applicationSecret:@"t0IP0pOhv02xfJTfxy8NmQ=="
        //                              environmentHost:@"sandbox.sinch.com"
        //                                       userId:userId];
        
        //self.client.callClient.delegate = self;
        [self.client setSupportCalling:YES];
        [self.client setSupportMessaging:YES];
        [self.client setSupportPushNotifications:YES];
        self.client.delegate = self;
        self.client.callClient.delegate = self;
        [self.client start];
        [self.client startListeningOnActiveConnection];
    }


}

- (void)deleteSinchSingleton{
    @synchronized([SinchClient class]) {
        if (_sharedClient != nil) {
           // _sharedClient = nil;
        }
    }
}

-(void)sendMessageText:(NSString *)text toUser:(NSString *)user{
    
    NSLog(@"send message:");
    SINOutgoingMessage *message = [SINOutgoingMessage messageWithRecipient:user text:text];
    [message addHeaderWithValue:[Client sharedClient].user.extended_name key:@"extended_name"];
    NSString *path_picture=@"";
    int indexProfileFoto= [[Client sharedClient].user.picture_index_default intValue];
    NSArray *arrayPictures = [Client sharedClient].user.pictures;
    for(unsigned i=0;i<arrayPictures.count;i++){
        NSDictionary *dic = [arrayPictures objectAtIndex:i];
        if([[dic objectForKey:@"pic_index"] intValue]==indexProfileFoto){
            path_picture = [dic objectForKey:@"web_path"];
            break;
        }
    }
    [message addHeaderWithValue:path_picture key:@"path_picture"];
    
    NSDictionary *messageDictionary = @{@"usernameToChat":[message.recipientIds lastObject],
                                        @"messageId":message.messageId,
                                        @"recipient":[message.recipientIds lastObject],
                                        @"sender":[Client sharedClient].user.email,
                                        @"text":message.text,
                                        @"headers":message.headers,
                                        @"timestamp":[NSDate date],
                                        @"sent":[NSNumber numberWithInt:2]
                                        };    
    [[Client sharedClient] updateCacheChatWithMessage:messageDictionary];
    [self.messageClient sendMessage:message];
}

#pragma mark SINCLIENT DELEGATE METHODS

-(void)clientDidStart:(id<SINClient>)client{
//    AppDelegate *ad = [UIApplication sharedApplication].delegate;
//    [ad enablePushNotification];
    self.messageClient = [self.client messageClient];
    self.messageClient.delegate = self;
    [self.client startListeningOnActiveConnection];
}

-(void)clientDidStop:(id<SINClient>)client{
     NSLog(@"client stop: %@",client);
}

-(void)clientDidFail:(id<SINClient>)client error:(NSError *)error{
     NSLog(@"clientFail: %@ error: %@",client,error);
}

#pragma mark - SINCallClientDelegate

- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    CallViewController *callViewController = [segue destinationViewController];
    callViewController.call = sender;
}

- (void)client:(id<SINCallClient>)client didReceiveIncomingCall:(id<SINCall>)call {
    // Find MainViewController and present CallViewController from it.
    
    NSLog(@"%@",call.remoteUserId);
    NSLog(@"%@",call.userInfo);
    [[Client sharedClient] getUserInfo:call.remoteUserId completition:^(NSDictionary *dictionary, NSError *error) {
        if(dictionary){
            
            NSString* incommingUserName = [dictionary objectForKey:@"extended_name"];
            UIViewController *top = self.window.rootViewController;
            while (top.presentedViewController) {
                top = top.presentedViewController;
            }
            id WindowRootVC = [[[[UIApplication sharedApplication] delegate] window] rootViewController];
            
            id currentViewController = [self findTopViewController:WindowRootVC];
            UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"Main"
                                                                 bundle: nil];
            call.delegate = self;
            CallViewController * callViewController = [storyboard instantiateViewControllerWithIdentifier:@"CallViewController"];
            callViewController.call = call;
            callViewController.incomUserName = incommingUserName;
            [currentViewController presentViewController:callViewController animated:YES completion:nil];
            
            
            if (call.details.applicationStateWhenReceived == UIApplicationStateActive) {
                // Show an answer button or similar in the UI
            } else {
                // Application was in not in the foreground when the call was initially received,
                // and the user has opened the application (e.g. via a Local Notification),
                // which we then interpret as that the user want to answer the call.
                [call answer];
            }
            
        }
        
    }];
}

-(id)findTopViewController:(id)inController
{
    /* if ur using any Customs classes, do like this.
     * Here SlideNavigationController is a subclass of UINavigationController.
     * And ensure you check the custom classes before native controllers , if u have any in your hierarchy.
     if ([inController isKindOfClass:[SlideNavigationController class]])
     {
     return [self findTopViewController:[inController visibleViewController]];
     }
     else */
    
    if ([inController isKindOfClass:[UITabBarController class]])
    {
        return [self findTopViewController:[inController selectedViewController]];
    }
    else if ([inController isKindOfClass:[UINavigationController class]])
    {
        return [self findTopViewController:[inController visibleViewController]];
    }
    else if ([inController isKindOfClass:[UIViewController class]])
    {
        return inController;
    }
    else
    {
        NSLog(@"Unhandled ViewController class : %@",inController);
        return nil;
    }
}

- (SINLocalNotification *)client:(id<SINClient>)client localNotificationForIncomingCall:(id<SINCall>)call {
    
    __block NSString *userName;
    
    SINLocalNotification *notification = [[SINLocalNotification alloc] init];
    notification.alertAction = @"Answer";
   // notification.alertBody = [NSString stringWithFormat:@"Incoming call from %@",userName];
    notification.alertBody = [NSString stringWithFormat:@"Incoming call"];
    
//    - (void)get_data:(NSString *)size completionHandler:(void (^)(NSArray *array))completionHandler {
//        // ...
    [[Client sharedClient] getUserInfo:call.remoteUserId completition:^(NSDictionary *dictionary, NSError *error) {
        
        if(dictionary){
            
            NSString* incommingUserName = [dictionary objectForKey:@"extended_name"];
            userName = incommingUserName;
        }
    }];
    return notification;
}

#pragma mark SINMESSAGECLIENT DELEGATE METHODS

-(void)messageClient:(id<SINMessageClient>) messageClient didReceiveIncomingMessage:(id<SINMessage>)message {
    NSArray *arrayResult=[NSArray array];
    if(![[Client sharedClient].user.friends isEqual:[NSNull null]]){
        NSArray *arrayBlocked = [UtilityGraphic filterWithKey:@"status" withValue:@"1" inArray:[Client sharedClient].user.friends withType:1];
        //NSArray *arrayBlocked = [self filterWith:@"blocked"];
        NSPredicate *predicate = [NSPredicate predicateWithFormat:@"(username==%@)",message.senderId];
        arrayResult = [arrayBlocked filteredArrayUsingPredicate:predicate];
    }
    if(arrayResult.count==0){
        
        UIViewController *top = self.window.rootViewController;
        while (top.presentedViewController) {
            top = top.presentedViewController;
        }
        
        id WindowRootVC = [[[[UIApplication sharedApplication] delegate] window] rootViewController];
        
        id currentController = [self findTopViewController:WindowRootVC];
        
        BOOL setReaded=NO;
        NSDictionary *messageDictionary = @ {@"usernameToChat":message.senderId,
                                            @"messageId":message.messageId,
                                            @"recipient":[message.recipientIds lastObject],
                                            @"sender":message.senderId,
                                            @"text":message.text,
                                            @"headers":message.headers,
                                            @"timestamp":message.timestamp,
                                            @"sent":[NSNumber numberWithInt:1]
                                            };
        //[[HKClient sharedClient] updateCacheChatWithMessage:messageDictionary];
        
//        AppDelegate* appdelegate = (AppDelegate*)[[UIApplication sharedApplication]delegate];
//        NavigationController *nav = (NavigationController *)ad.window.rootViewController;
//        UIViewController *currentController = nav.visibleViewController;
        
        if([currentController isKindOfClass:[ChatViewController class]]){
            //NSLog(@"call update Chat");
            NSString *userIncoming = [(ChatViewController *)currentController getUserIncoming];
            [Client sharedClient].incomingUser=nil;
            if([userIncoming isEqualToString:message.senderId]){
                [(ChatViewController*)currentController updateWithMessage:messageDictionary];
                setReaded=YES;
            }else{
//                TabBarController *tabBarController = (TabBarController *)[nav.childViewControllers objectAtIndex:0];
//                UIViewController *currentController = [tabBarController.childViewControllers lastObject];
                if([currentController isKindOfClass:[ChatRecentsViewController class]]){
                    [(ChatRecentsViewController*)currentController reloadContent];
                }else{
                    AppDelegate* appdelegate = (AppDelegate*)[[UIApplication sharedApplication]delegate];
                    [appdelegate customLocaChatNotification:messageDictionary];
                    //[nav lunchAlertMessage:messageDictionary likeNotification:NO];
                }
            }
        }
        else if ([currentController isKindOfClass:[ChatRecentsViewController class]]){
            
            //UIViewController *currentTab = [currentController.childViewControllers lastObject];
            if([currentController isKindOfClass:[ChatRecentsViewController class]]){
                [Client sharedClient].incomingUser=nil;
                [(ChatRecentsViewController*)currentController reloadContent];
            }else{
                //NSLog(@"open Alert 1");
                //[nav lunchAlertMessage:messageDictionary likeNotification:NO];
                AppDelegate* appdelegate = (AppDelegate*)[[UIApplication sharedApplication]delegate];
                [appdelegate customLocaChatNotification:messageDictionary];
            }
        }
        else{
            //NSLog(@"open Alert 2");
            //[nav lunchAlertMessage:messageDictionary likeNotification:NO];
            
            AppDelegate* appdelegate = (AppDelegate*)[[UIApplication sharedApplication]delegate];
            [appdelegate customLocaChatNotification:messageDictionary];
            
//            TabBarController *tabBarController = (TabBarController *)[nav.childViewControllers objectAtIndex:0];
//            UIViewController *presumeChatList = (ChatListViewController *)[tabBarController.childViewControllers lastObject];
            if([currentController isKindOfClass:[ChatRecentsViewController class]]){
                //NSLog(@"c'è chatList");
                [(ChatRecentsViewController*)currentController reloadContent];
            }
        }
        [[Client sharedClient] updateCacheChatWithMessage:messageDictionary];
        if(setReaded){
            [[Client sharedClient]updateCacheChatMessageReadedWithMessageId:messageDictionary];
           // TabBarController *tabBarController = (TabBarController *)[nav.childViewControllers objectAtIndex:0];
            TabBarViewController *tabBarController = [SharedVariables sharedInstance].tabBarController;
            [tabBarController updateBadgeItem];
            UIViewController *currentController = [tabBarController.childViewControllers lastObject];
            if([currentController isKindOfClass:[ChatRecentsViewController class]]){
                [(ChatRecentsViewController*)currentController reloadContent];
            }
        }
    }
}

//-(void)messageClient:(id<SINMessageClient>) messageClient didReceiveIncomingMessage:(id<SINMessage>)message {
//    NSArray *arrayResult=[NSArray array];
//    if(![[Client sharedClient].user.friends isEqual:[NSNull null]]){
//        NSArray *arrayBlocked = [UtilityGraphic filterWithKey:@"status" withValue:@"1" inArray:[Client sharedClient].user.friends withType:1];
//        //NSArray *arrayBlocked = [self filterWith:@"blocked"];
//        NSPredicate *predicate = [NSPredicate predicateWithFormat:@"(username==%@)",message.senderId];
//        arrayResult = [arrayBlocked filteredArrayUsingPredicate:predicate];
//    }
//    if(arrayResult.count==0){
//
//        BOOL setReaded=NO;
//        NSDictionary *messageDictionary = @{@"usernameToChat":message.senderId,
//                                            @"messageId":message.messageId,
//                                            @"recipient":[message.recipientIds lastObject],
//                                            @"sender":message.senderId,
//                                            @"text":message.text,
//                                            @"headers":message.headers,
//                                            @"timestamp":message.timestamp,
//                                            @"sent":[NSNumber numberWithInt:1]
//                                            };
//
//        //[[HKClient sharedClient] updateCacheChatWithMessage:messageDictionary];
//
//        //AppDelegate *ad = [UIApplication sharedApplication].delegate;
//        UINavigationController *nav = [SharedVariables sharedInstance].tabBarController.parentViewController;
//        UIViewController *currentController = nav.visibleViewController;
//
//        NSArray *viewControllerArray = [SharedVariables sharedInstance].tabBarController.viewControllers;
//
//
//        for (UIViewController *vc in viewControllerArray) {
//
//            if ([vc isKindOfClass:ChatRecentsViewController.class]) {
//
//                ChatRecentsViewController * chatVC = vc;
//
//                [chatVC reloadContent];
//            }
//
//        }
//
//        if([currentController isKindOfClass:[ChatViewController class]]){
//            //NSLog(@"call update Chat");
//            NSString *userIncoming = [(ChatViewController *)currentController getUserIncoming];
//            [Client sharedClient].incomingUser=nil;
//            if([userIncoming isEqualToString:message.senderId]){
//                [(ChatViewController*)currentController updateWithMessage:messageDictionary];
//                setReaded=YES;
//            }else{
//               // TabBarController *tabBarController = (TabBarController *)[nav.childViewControllers objectAtIndex:0];
//                UIViewController *currentController = [[SharedVariables sharedInstance].tabBarController.childViewControllers lastObject];
//                if([currentController isKindOfClass:[ChatRecentsViewController class]]){
//                    [(ChatRecentsViewController*)currentController reloadContent];
//                }else{
//                    AppDelegate* appdelegate = (AppDelegate*)[[UIApplication sharedApplication]delegate];
//                    [appdelegate customLocalNotification:messageDictionary];
//                }
//            }
//        }else if ([currentController isKindOfClass:[ChatRecentsViewController class]]){
//            UIViewController *currentTab = [currentController.childViewControllers lastObject];
//            if([currentTab isKindOfClass:[ChatRecentsViewController class]]){
//                [Client sharedClient].incomingUser=nil;
//                [(ChatRecentsViewController*)currentTab reloadContent];
//            }else{
//                //NSLog(@"open Alert 1");
//                AppDelegate* appdelegate = (AppDelegate*)[[UIApplication sharedApplication]delegate];
//                [appdelegate customLocalNotification:messageDictionary];
//            }
//        }else{
//            //NSLog(@"open Alert 2");
//           // [nav lunchAlertMessage:messageDictionary likeNotification:NO];
//            //TabBarController *tabBarController = (TabBarController *)[nav.childViewControllers objectAtIndex:0];
//            UITabBarController *tabBarController = [SharedVariables sharedInstance].tabBarController;
//            UIViewController *presumeChatList = (ChatRecentsViewController *)[tabBarController.childViewControllers lastObject];
//            if([currentController isKindOfClass:[ChatRecentsViewController class]]){
//                //NSLog(@"c'è chatList");
//                [(ChatRecentsViewController*)presumeChatList reloadContent];
//            }
//        }
//        [[Client sharedClient] updateCacheChatWithMessage:messageDictionary];
//        if(setReaded){
//            [[Client sharedClient]updateCacheChatMessageReadedWithMessageId:messageDictionary];
//           // TabBarController *tabBarController = (TabBarController *)[nav.childViewControllers objectAtIndex:0];
//            TabBarViewController *tabBarController = [SharedVariables sharedInstance].tabBarController;
//            [tabBarController updateBadgeItem];
//            UIViewController *currentController = [tabBarController.childViewControllers lastObject];
//            if([currentController isKindOfClass:[ChatRecentsViewController class]]){
//                [(ChatRecentsViewController*)currentController reloadContent];
//            }
//        }
//    }
//}

-(void)messageSent:(id<SINMessage>)message recipientId:(NSString *)recipientId{
    NSLog(@"messageSent:");
    int sentStatus = 1;
    NSDictionary *messageDictionary = @{@"usernameToChat":[message.recipientIds lastObject],
                                        @"messageId":message.messageId,
                                        @"recipient":[message.recipientIds lastObject],
                                        @"sender":message.senderId,
                                        @"text":message.text,
                                        @"headers":message.headers,
                                        @"timestamp":message.timestamp,
                                        @"sent":[NSNumber numberWithInt:sentStatus]
                                        };
    
    [[Client sharedClient] updateMessageStatusFromMessageId:message.messageId withRecipient:[message.recipientIds lastObject] withTimeStamp:message.timestamp withStatus:sentStatus];
    
//    AppDelegate *ad = [UIApplication sharedApplication].delegate;
//    NavigationController *nav = (NavigationController *)ad.window.rootViewController;
//    UIViewController *currentController = nav.visibleViewController;
//
    if([[SharedVariables sharedInstance].currentController isKindOfClass:[ChatViewController class]]){
        [(ChatViewController *)[SharedVariables sharedInstance].currentController updateWithMessage:messageDictionary];
        
    }
}

-(void) messageDelivered:(id<SINMessageDeliveryInfo>)info {
    //NSLog(@"messageDelivered:");
}

-(void)messageFailed:(id<SINMessage>)message info:(id<SINMessageFailureInfo>)messageFailureInfo{
    NSLog(@"messageFailed: %@",messageFailureInfo.error);
    [[Client sharedClient] updateMessageStatusFromMessageId:message.messageId withRecipient:[message.recipientIds lastObject] withTimeStamp:message.timestamp withStatus:0];
}

-(void)message:(id<SINMessage>)message shouldSendPushNotifications:(NSArray *)pushPairs{
    //NSLog(@"shouldSendPushNotifications");
    //int count=1;
    
    for(id<SINPushPair> push in pushPairs){
        //NSLog(@"tokenfromString: %@",[NSString stringWithUTF8String:[push.pushData bytes]]);
        //NSString *tokenString = [NSString stringWithUTF8String:[push.pushData bytes]];
        
        const unsigned *tokenBytes = [push.pushData bytes];
        NSString *hexToken = [NSString stringWithFormat:@"%08x%08x%08x%08x%08x%08x%08x%08x",
                              ntohl(tokenBytes[0]), ntohl(tokenBytes[1]), ntohl(tokenBytes[2]),
                              ntohl(tokenBytes[3]), ntohl(tokenBytes[4]), ntohl(tokenBytes[5]),
                              ntohl(tokenBytes[6]), ntohl(tokenBytes[7])];

        /*
        if([NSString stringWithUTF8String:[push.pushData bytes]]){
            tokenString = [NSString stringWithUTF8String:[push.pushData bytes]];
            NSLog(@"tokenString: %@",tokenString);
            NSArray* items = [tokenString componentsSeparatedByString:@"@"];
            if(items.count>0){
                tokenString = [items objectAtIndex:0];
            }
        }
         */
        int length=(int)[push.pushData length];
        if(length!=32){
            hexToken = [NSString stringWithUTF8String:[push.pushData bytes]];
        }
        NSLog(@"length: %lu",(unsigned long)[push.pushData length]);
        NSLog(@"hexToken: %@",hexToken);
        //count++;
        
        NSMutableDictionary *dicPush = [NSMutableDictionary dictionaryWithCapacity:0];
        [dicPush setObject:hexToken forKey:@"token"];
        [dicPush setObject:push.pushPayload forKey:@"payLoad"];
        [dicPush setObject:message.messageId forKey:@"messageId"];
        [dicPush setObject:[message.recipientIds lastObject] forKey:@"recipient"];
        [dicPush setObject:message.senderId forKey:@"sender"];
        [dicPush setObject:message.text forKey:@"text"];

        if(![hexToken isEqualToString:@""]){

            [[Client sharedClient] sendWakeUpWithDictionary:dicPush completition:^(BOOL success, NSError *error) {
                if(success){
                    NSLog(@"send push ok");
                }
                if(error){
                    NSLog(@"error send push: %@",error);
                }
            }];

        }
    }
}


@end
