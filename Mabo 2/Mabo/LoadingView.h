//
//  LoadingView.h
//  honk
//
//  Created by Steewe MacBook Pro on 06/03/15.
//  Copyright (c) 2015 Steewe. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface LoadingView : UIView

-(void)animate:(BOOL)value;
//-(void)restartActivity;

@end
