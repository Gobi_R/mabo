//
//  CreatPostViewController.h
//  Mobo
//
//  Created by vishnuprasathg on 10/24/17.
//  Copyright © 2017 DCI. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface CreatPostViewController : UIViewController<UITextViewDelegate,UICollectionViewDelegate,UICollectionViewDataSource,UITextFieldDelegate,UIPickerViewDelegate,UIPickerViewDataSource,UIGestureRecognizerDelegate,UIScrollViewDelegate>
- (IBAction)backBtnAction:(id)sender;
@property (weak, nonatomic) IBOutlet UIButton *createBtnOutlet;
- (IBAction)createBtnAction:(id)sender;

@property (weak, nonatomic) IBOutlet UIScrollView *baseScrollViewOutlet;
@property (weak, nonatomic) IBOutlet UITextView *titleTextViewOutlet;
@property (weak, nonatomic) IBOutlet UITextView *postContentTextViewOutlet;
@property (weak, nonatomic) IBOutlet UIButton *addphotosBtnOutlet;
@property (weak, nonatomic) IBOutlet UICollectionView *addPhotosCollectionViewOutlet;
@property (weak, nonatomic) IBOutlet UIButton *addVideosBtnOutlet;
@property (weak, nonatomic) IBOutlet UICollectionView *addVideosCollectionViewOutlet;
@property (weak, nonatomic) IBOutlet UIButton *postLoactionBtnoutlet;
@property (weak, nonatomic) IBOutlet UIButton *eventBtnOutlet;
@property (weak, nonatomic) IBOutlet UIButton *participatebtnoutlet;
@property (weak, nonatomic) IBOutlet UIView *eventBaseViewoutlet;
@property (weak, nonatomic) IBOutlet UIView *participationBaseViewOutlet;
@property (weak, nonatomic) IBOutlet UITextView *eventTextViewoutlet;
@property (weak, nonatomic) IBOutlet UIButton *noBtnOutlet;
@property (weak, nonatomic) IBOutlet UIButton *yesBtnOutlet;
@property (weak, nonatomic) IBOutlet UITextField *priceTxtFieldOutlet;
@property (weak, nonatomic) IBOutlet UILabel *titleLimitLblOutlet;
@property (weak, nonatomic) IBOutlet UILabel *postContentLimitLblOutlet;
@property (weak, nonatomic) IBOutlet UILabel *dateLblOutlet;
@property (weak, nonatomic) IBOutlet UIButton *dateBtnOutlet;
@property (weak, nonatomic) IBOutlet UILabel *selectedAddressLblOutlet;
@property (weak, nonatomic) IBOutlet UILabel *priceTitleLblOutlet;
@property (weak, nonatomic) IBOutlet UILabel *titileLblOutlet;


@property (strong, nonatomic) IBOutlet UIView *datePickerBaseViewOutlet;
@property (weak, nonatomic) IBOutlet UIDatePicker *datePickerViewOutlet;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *addPhotosBtnHeightConstraint;
@property (weak, nonatomic) IBOutlet UILabel *addPhotosTitleLblOutlet;

@property (weak, nonatomic) IBOutlet UILabel *addVideosTitleLblOutlet;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *addVideosBtnHeightConstraint;


- (IBAction)addPhotosBtnAction:(id)sender;
- (IBAction)addVideosBtnAction:(id)sender;
- (IBAction)postLoactionBtnAction:(id)sender;
- (IBAction)eventBtnAction:(id)sender;
- (IBAction)particiapteBtnAction:(id)sender;

- (IBAction)photosCloseBtnAction:(id)sender;
- (IBAction)videosCloseBtnAction:(id)sender;

- (IBAction)yesBtnAction:(id)sender;
- (IBAction)noBtnAction:(id)sender;
- (IBAction)dateBtnAction:(id)sender;

@property (weak, nonatomic) IBOutlet NSLayoutConstraint *addphotosBaseViewHeightConstraint;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *addVideosBaseViewHeightConstraint;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *eventBaseViewHeightConstraint;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *particapationBaseViewHeightConstraint;

-(void)openCamera;
-(void)openGallery;
-(void)openVideoRoll;
-(void)openVideoCamera;

-(void)addPhotosAlert;
-(void)addVideosAlert;

-(void)creatSuccessAction;


@property (nonatomic, retain) NSDictionary * editDateDict;

@property (nonatomic, retain) NSString * viewIdendity;


@end
