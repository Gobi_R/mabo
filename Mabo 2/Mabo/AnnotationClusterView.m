//
//  HKAnnotationClusterView.m
//  honk
//
//  Created by Steewe MacBook Pro on 25/02/15.
//  Copyright (c) 2015 Steewe. All rights reserved.
//

#import "AnnotationClusterView.h"
#import "SharedVariables.h"

@interface AnnotationClusterView()

@property (nonatomic, strong) UILabel *label;

@end

@implementation AnnotationClusterView

-(instancetype)initWithAnnotation:(id<MKAnnotation>)annotation reuseIdentifier:(NSString *)reuseIdentifier{
    self=[super initWithAnnotation: annotation reuseIdentifier: reuseIdentifier];
    if(self){
        UIView *box = [[UIView alloc] initWithFrame:CGRectMake(-15, -15, 30, 30)];
        [box setBackgroundColor:[UIColor grayColor]];
        box.layer.cornerRadius=box.frame.size.height/2;
        [box.layer setShadowColor:[UIColor blackColor].CGColor];
        [box.layer setShadowOffset:CGSizeMake(0, 3)];
        [box.layer setShadowOpacity:.5];
        [box.layer setShadowRadius:3];
        [box.layer setBorderColor:[UIColor colorWithRed:122/255.0f green:222/255.0f blue:225/255.0f alpha:1.0f].CGColor];
        [box.layer setBorderWidth:2];
        
        Annotation *hkannotation = (Annotation *)annotation;
        
        self.label = [[UILabel alloc] initWithFrame:CGRectMake(0, 0, box.frame.size.width, box.frame.size.height)];
        [self.label setFont:[UIFont fontWithName:@"ProximaNova-Regular_0" size:5]];
        [self.label setTextColor:[UIColor whiteColor]];
        [self.label setText:hkannotation.title];
        [self.label setTextAlignment:NSTextAlignmentCenter];
        [box addSubview:self.label];
        
        [self addSubview:box];
    }
    
    return self;
}

-(void)refreshWithAnnotation:(Annotation *)annotation{
    
    
    [self.label setText:annotation.title];
}

@end
