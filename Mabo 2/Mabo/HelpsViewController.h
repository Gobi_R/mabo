//
//  HelpsViewController.h
//  Mabo
//
//  Created by vishnuprasathg on 11/22/17.
//  Copyright © 2017 DCI. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface HelpsViewController : UIViewController

@property (weak, nonatomic) IBOutlet UIWebView *webViewOutlet;
- (IBAction)backBtnAction:(id)sender;
@property (weak, nonatomic) IBOutlet UILabel *titleLblOutlet;
@property (weak, nonatomic) IBOutlet UIView *feedBackBaseView;
@property (weak, nonatomic) IBOutlet UITextView *textViewOutlet;
@property (weak, nonatomic) IBOutlet UIButton *submitBtnOutlet;

@property (nonatomic, retain) NSString * viewIdendify;
- (IBAction)submitBtnAction:(id)sender;
@end
