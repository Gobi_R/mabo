//
//  CustomVideoAlertView.m
//  Mabo
//
//  Created by vishnuprasathg on 10/31/17.
//  Copyright © 2017 DCI. All rights reserved.
//

#import "CustomVideoAlertView.h"
#import "Utilities.h"
#import "ProfileViewController.h"
#import "CreatPostViewController.h"

@implementation CustomVideoAlertView

/*
// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect {
    // Drawing code
}
*/

- (IBAction)openCameraBtnAction:(id)sender {
    

    if ([_alertParentViewController isKindOfClass: CreatPostViewController.class]) {
        
        [[Utilities sharedInstance] hideCustomAlertView:self.superview];
        
        CreatPostViewController * creatPostVC = _alertParentViewController;
        
        [creatPostVC openVideoCamera];
        
        
    }

}

- (IBAction)videoRollBtnAction:(id)sender {
    
   if ([_alertParentViewController isKindOfClass: CreatPostViewController.class]) {
        
        [[Utilities sharedInstance] hideCustomAlertView:self.superview];
        
        CreatPostViewController * creatPostVC = _alertParentViewController;
        
        [creatPostVC openVideoRoll];
        
        
    }

}

- (IBAction)cancelBtnAction:(id)sender {
     [[Utilities sharedInstance] hideCustomAlertView:self.superview];
}
@end
