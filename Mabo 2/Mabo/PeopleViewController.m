//
//  PeopleViewController.m
//  Mobo
//
//  Created by vishnuprasathg on 10/17/17.
//  Copyright © 2017 DCI. All rights reserved.
//

#import "PeopleViewController.h"
#import "Client.h"
#import "ProfileViewController.h"
#import <MapKit/MapKit.h>
#import "Annotation.h"
#import "AnnotationView.h"
#import "AnnotationClusterView.h"

#import "KPAnnotation.h"
#import "KPGridClusteringAlgorithm.h"
#import "KPClusteringController.h"
#import "NSDate+TimeAgo.h"
#import "Utilities.h"
//#import "UIImageView+AFNetworking.h"
#import "FiltersViewController.h"
#import "SharedVariables.h"
#import "PrefixHeader.pch"
#import "NSDate+TimeAgo.h"
#import <Firebase/Firebase.h>
#import "UserProfileViewController.h"
#import "UIImageView+WebCache.h"

@interface PeopleViewController ()<UITableViewDelegate, UITableViewDataSource, MKMapViewDelegate, KPClusteringControllerDelegate, KPClusteringControllerDelegate>

@property float radius;
@property (nonatomic, strong) NSArray *usersList;
@property (nonatomic, strong) NSArray *visibleUserList;
@property (nonatomic, strong) MKMapView *map;

@property BOOL isMapView;
@property CLLocationCoordinate2D cordinateViewMap;
@property BOOL fisrtOpen;
@property BOOL fisrtChange;


@property (strong, nonatomic) KPClusteringController *clusteringController;

@end

@implementation PeopleViewController

{
    NSMutableArray * annotationsArray;
}

static NSString *CellIdentifier = @"Cell";
static NSString *PinIdentifier = @"Pin";
static NSString *ClusterIdentifier = @"Cluster";

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
   // self.mapBtnOutlet.hidden = YES;
    
    self.radius=100;
    
    self.tableViewOutlet.delegate = self;
    self.tableViewOutlet.dataSource = self;
    
    self.map = [[MKMapView alloc] initWithFrame:CGRectMake(0, 0, [UIScreen mainScreen].bounds.size.width, self.view.frame.size.height)];
    [self.map setDelegate:self];
    [self.map setMapType:MKMapTypeStandard];
    [self.map setShowsUserLocation:YES];
    
    [self.mapBaseView addSubview:self.map];
    
    
    
    //CLUSTERING
    KPGridClusteringAlgorithm *algorithm = [KPGridClusteringAlgorithm new];
    algorithm.annotationSize = CGSizeMake(50, 50);
    algorithm.clusteringStrategy = KPGridClusteringAlgorithmStrategyTwoPhase;
    self.clusteringController = [[KPClusteringController alloc] initWithMapView:self.map
                                                            clusteringAlgorithm:algorithm];
    self.clusteringController.delegate = self;
    self.clusteringController.animationOptions = UIViewAnimatingPositionStart;
    
    self.tableViewOutlet.tableFooterView = [UIView new];
}

-(void)viewWillAppear:(BOOL)animated
{
    //[self.map removeAnnotations:self.map.annotations];
    [self.peopleBtnOutlet setTitle:@"Loading..." forState:UIControlStateNormal];
    self.peopleBtnOutlet.userInteractionEnabled = NO;
    
    self.tableViewOutlet.hidden = YES;
    self.mapBaseView.hidden = NO;
    
    [SharedVariables sharedInstance].annotationCount = 1;
    [self.map setFrame:CGRectMake(0, 0, [UIScreen mainScreen].bounds.size.width, self.view.frame.size.height)];
    
    if(self.tableViewOutlet.indexPathForSelectedRow){
        [self.tableViewOutlet deselectRowAtIndexPath:self.tableViewOutlet.indexPathForSelectedRow animated:NO];
    }
    
    BOOL valueFilterControl=NO;
    
    NSLog(@"FilterMap:%@",[Client sharedClient].user.filterMap);
    if([[Client sharedClient].user.filterMap isKindOfClass:[NSMutableDictionary class]] && (int)[[Client sharedClient].user.filterMap allKeys].count>0){
        for(NSString *key in [[Client sharedClient].user.filterMap allKeys]){
            NSString *value = [[Client sharedClient].user.filterMap objectForKey:key];
            if(value.length>0){
                valueFilterControl=YES;
                break;
            }
        }
    }
    
    //[self.map setCenterCoordinate:[Client sharedClient].locationManager.location.coordinate animated:YES];
    
    CLLocationCoordinate2D myCoordinate;
    myCoordinate.latitude = *([SharedVariables sharedInstance].lat);
    myCoordinate.longitude = *([SharedVariables sharedInstance].lon);
    
    MKCoordinateRegion viewRegion = MKCoordinateRegionMakeWithDistance
    (myCoordinate, 300, 300);
    [self.map setRegion:viewRegion animated:YES];
    
    [self.map setCenterCoordinate:myCoordinate animated:YES];
    
    [self refreshSearch];
    
    //[self receiveConnectionChange:nil];
    //[[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(receiveConnectionChange:) name:@"connectionChange" object:nil];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}
//self.radius/1000
-(void)refreshSearch{
    [[Client sharedClient] getUsersAroundWithRadius:(float)self.radius/1000 andCoordinate:self.cordinateViewMap completition:^(NSArray *usersList, NSError *error) {
        if(self.usersList && ![Client sharedClient].newFilterForMap){
            
            [self mergeUserList:usersList];
        }else{
            self.usersList=usersList;
            [self.clusteringController setAnnotations:[self createAnnotationsFromArray:self.usersList]];
            [self getAnotationsInVisibleMapRectangle];
            [self.tableViewOutlet setContentOffset:CGPointZero];
            [self.tableViewOutlet reloadData];
            /*
             [self.clusteringController setAnnotations:[self createAnnotations]];
             [self.table reloadData];
             [self.table setHidden:(self.usersList.count==0)];
             */
        }
        
        [Client sharedClient].newFilterForMap=NO;
        
        [self.peopleBtnOutlet setTitle:@"People" forState:UIControlStateNormal];
        self.peopleBtnOutlet.userInteractionEnabled = YES;
    }];
    
    MKAnnotationView *userLocationView = [self.map viewForAnnotation:self.map.userLocation];
    userLocationView.canShowCallout = NO;
}
-(void)mergeUserList:(NSArray *)array{
    NSMutableArray *arrayMerged = [NSMutableArray arrayWithCapacity:0];
    for(NSDictionary *dic in array){
        NSPredicate *predicate = [NSPredicate predicateWithFormat:@"(username == %@)",[dic objectForKey:@"username"]];
        NSArray *arrayResult = [self.usersList filteredArrayUsingPredicate:predicate];
        if((int)arrayResult.count==0){
            [arrayMerged addObject:dic];
        }
    }
    
    self.usersList = [self.usersList arrayByAddingObjectsFromArray:arrayMerged];
    if((int)arrayMerged.count==0){
        [self.clusteringController refresh:YES];
    }else{
        [self.clusteringController setAnnotations:[self createAnnotationsFromArray:self.usersList]];
    }
    
    
    [self getAnotationsInVisibleMapRectangle];
    [self.tableViewOutlet reloadData];
    /*
     BOOL allInOne = YES;
     for(unsigned i=0;i<array.count;i++){
     if(![self.usersList containsObject:[array objectAtIndex:i]]){
     allInOne=NO;
     break;
     }
     }
     
     if(allInOne){
     [self.clusteringController refresh:YES];
     self.usersList=array;
     }else{
     self.usersList=array;
     [self.clusteringController setAnnotations:[self createAnnotations]];
     }
     
     [self.table reloadData];
     */
}

#pragma mark - TABLEVIEW DELEGATE METHODS


-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    if(self.isMapView){
        return 0;
    }else{
        NSLog(@"visibleUserList:%@",self.visibleUserList);
        return (self.visibleUserList.count==0)?1:self.visibleUserList.count;
    }
}

-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    
    UITableViewCell *cellReal = [tableView dequeueReusableCellWithIdentifier:@"Cell"];
    
    if (self.visibleUserList.count > 0) {
        
        NSDictionary* listMembers = [self.visibleUserList objectAtIndex:indexPath.row];
        
        UIImageView* profileImageView = [cellReal viewWithTag:1];
        UILabel * nameLbl = [cellReal viewWithTag:2];
        UILabel* feetLbl = [cellReal viewWithTag:3];
        UILabel* lastSeen = [cellReal viewWithTag:4];
        
        NSString* name = [[listMembers objectForKey:@"user"] objectForKey:@"extended_name"];
        NSString* picture = [[listMembers objectForKey:@"user"] objectForKey:@"picture_path"];
        NSString* lastseen = [[listMembers objectForKey:@"user"] objectForKey:@"last_seen"];
        NSString* dist = [NSString stringWithFormat:@"%@",[listMembers objectForKey:@"distance"]];
        NSArray * arr = [dist componentsSeparatedByString:@"."];
        NSString* distance = arr.firstObject;
//        NSString* lat = [[listMembers objectForKey:@"user"] objectForKey:@"lat"];
//        NSString* lon = [[listMembers objectForKey:@"user"] objectForKey:@"lon"];
        
        if (name != nil)
        {
            nameLbl.text = [NSString stringWithFormat:@"%@",name];;
        }
        if (picture != nil)
        {
            
            NSString * urlstring = [NSString stringWithFormat:@"http://34.230.105.103:8002/%@", picture];
            NSURL *imageURL = [NSURL URLWithString:urlstring];
//            NSData *imageData = [NSData dataWithContentsOfURL:imageURL];
//            UIImage *image = [UIImage imageWithData:imageData];
//            profileImageView.image = image;
            [profileImageView setImageWithURL:imageURL placeholderImage:[UIImage imageNamed:@"avatar3.png"]];
        
            profileImageView.layer.cornerRadius = profileImageView.frame.size.height / 2;
            profileImageView.layer.masksToBounds = YES;
            
            
        }
        else
        {
            
            profileImageView.image = [UIImage imageNamed:@"avatar3.png"];
        }
        if (distance != nil)
        {
            feetLbl.text = [NSString stringWithFormat:@"%@ feet",distance];
        }
        if (lastseen != nil)
        {
            NSString * timeStampString = [NSString stringWithFormat:@"%@",lastseen];
            
            if (![timeStampString isEqualToString:@"<null>"]) {
                
                NSTimeInterval _interval=[timeStampString doubleValue];
                NSDate *date = [NSDate dateWithTimeIntervalSince1970:_interval];
                NSLog(@"%@", date);
                
                
                NSDateFormatter *formatter = [[NSDateFormatter alloc] init];
                [formatter setDateFormat:@"dd MMMM yyyy"];
                NSString *result = [formatter stringFromDate:date];
                
                NSMutableAttributedString*tmeString = [[Utilities sharedInstance]loadTimeDifferent:date];
                
                lastSeen.attributedText = tmeString;
            }
            else
            {
               lastSeen.text = [NSString stringWithFormat:@"Not Available"];
            }
            
            
        }
        else
        {
            lastSeen.text = [NSString stringWithFormat:@"Last seen: "];
        }

    }
    
    return cellReal;
}

- (BOOL)tableView:(UITableView *)tableView shouldHighlightRowAtIndexPath:(NSIndexPath *)indexPath{
    return (self.visibleUserList.count>0);
}


-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    if([[Client sharedClient].reachAbility isReachable]){
//        ProfileViewController *profile = [[ProfileViewController alloc] initWithUser:[[self.visibleUserList objectAtIndex:indexPath.row] objectForKey:@"username"] andName:[[self.visibleUserList objectAtIndex:indexPath.row] objectForKey:@"extended_name"]];
//        [self.navigationController pushViewController:profile animated:YES];
    }
}

#pragma mark - MAPVIEW DELEGATE METHODS

-(void)mapView:(MKMapView *)mapView regionWillChangeAnimated:(BOOL)animated{
    //NSLog(@"WillChange");
    [SharedVariables sharedInstance].annotationCount = 1;
}

-(void)mapView:(MKMapView *)mapView regionDidChangeAnimated:(BOOL)animated{
    //NSLog(@"DidChange");
    self.cordinateViewMap = mapView.centerCoordinate;
    MKMapRect mRect = mapView.visibleMapRect;
    MKMapPoint eastMapPoint = MKMapPointMake(MKMapRectGetMinX(mRect), MKMapRectGetMidY(mRect));
    MKMapPoint westMapPoint = MKMapPointMake(MKMapRectGetMaxX(mRect), MKMapRectGetMidY(mRect));
    self.radius = (int)MKMetersBetweenMapPoints(eastMapPoint, westMapPoint)/2;
    
    if(!self.fisrtChange){
        [self refreshSearch];
    }else{
        self.fisrtChange=NO;
    }
}

-(void)mapView:(MKMapView *)mapView didUpdateUserLocation:(MKUserLocation *)userLocation{
    if(self.fisrtOpen){
        self.fisrtOpen=NO;
        [self.map setCenterCoordinate:[Client sharedClient].locationManager.location.coordinate animated:YES];
        float latdelta = MAX(MIN(mapView.region.span.latitudeDelta, 80), 0.0001);
        float londelta = MAX(MIN(mapView.region.span.longitudeDelta, 80), 0.0001);
        MKCoordinateSpan span = MKCoordinateSpanMake(latdelta, londelta);
        [self.map setRegion:MKCoordinateRegionMake(mapView.region.center, span) animated:YES];
    }
}

#pragma mark - MAP METHODS

-(void)getAnotationsInVisibleMapRectangle{
    NSSet *annotationSet = [self.map annotationsInMapRect:self.map.visibleMapRect];
    NSArray *annotationArray = [annotationSet allObjects];
    NSMutableArray *arrayResultVisible = [NSMutableArray arrayWithCapacity:0];
    for(KPAnnotation *cluster in annotationArray){
        if([cluster isKindOfClass:[KPAnnotation class]]){
            for(Annotation *annotation in cluster.annotations){
                [arrayResultVisible addObject:annotation.userDictionary];
            }
        }
    }
    
    
    NSMutableArray *arrayDictionary = [NSMutableArray arrayWithCapacity:0];
    NSMutableArray *arrayFriends = [NSMutableArray arrayWithCapacity:0];
    
    for(NSDictionary *dictionary in arrayResultVisible){
        
        CLLocation *locationUser = [[CLLocation alloc] initWithLatitude:[[dictionary  objectForKey:@"lat"] doubleValue] longitude:[[dictionary  objectForKey:@"lon"] doubleValue]];
        CLLocationDistance distance = [[SharedVariables sharedInstance].myClLocation distanceFromLocation:locationUser];
        NSDictionary *dictionaryResult = @{@"user":dictionary,@"distance":[NSNumber numberWithDouble:distance]};
        [arrayDictionary addObject:dictionaryResult];
        
    }
    
    NSSortDescriptor *descriptor = [[NSSortDescriptor alloc] initWithKey:@"distance" ascending:YES];
    NSArray *arrayOrderer =[arrayDictionary sortedArrayUsingDescriptors:[NSArray arrayWithObjects:descriptor,nil]];
    
    for(unsigned i=0;i<arrayOrderer.count;i++){
        [arrayFriends addObject:[[arrayOrderer objectAtIndex:i] objectForKey:@"user"]];
    }
    
    NSSet *setList = [NSSet setWithArray:arrayOrderer];
    
    
    self.visibleUserList= [NSArray arrayWithArray:[setList allObjects]];
}

- (NSMutableArray *)createAnnotationsFromArray:(NSArray *)array {
    annotationsArray = [[NSMutableArray alloc] init];
    
    for(unsigned i=0;i<array.count;i++){
        NSDictionary *user = [array objectAtIndex:i];
        NSNumber *latitude = [NSNumber numberWithDouble:[[user objectForKey:@"lat"] doubleValue]];
        NSNumber *longitude = [NSNumber numberWithDouble:[[user objectForKey:@"lon"] doubleValue]];
        NSString *title = [user objectForKey:@"extended_name"];
        NSString *picture = [user objectForKey:@"picture_path"];
        NSString *username = [user objectForKey:@"username"];
        BOOL isScrumble = [[user objectForKey:@"is_scrumble"] boolValue];
        
        CLLocationCoordinate2D coord;
        coord.latitude = latitude.doubleValue;
        coord.longitude = longitude.doubleValue;
        
        NSDictionary *dictionary=@{
                                   @"title":title,
                                   @"subtitle":username,
                                   @"picture_path":picture,
                                   @"index":[NSNumber numberWithInt:i],
                                   @"scrumble":[NSNumber numberWithBool:isScrumble]
                                   };
        
        Annotation *annotation = [[Annotation alloc] initWithDictionary:dictionary andCoordinate:coord andUserDic:user];
        [annotationsArray addObject:annotation];
    }
    
    return annotationsArray;
}


- (MKAnnotationView *)mapView:(MKMapView *)mapView viewForAnnotation:(id <MKAnnotation>)annotation{
    
    id annotationView;
    
    NSLog(@"Annotation:%@",annotation);
    
    if ([annotation isKindOfClass:[KPAnnotation class]]) {
        KPAnnotation *a = (KPAnnotation *)annotation;
        if ([annotation isKindOfClass:[MKUserLocation class]]){
            return nil;
        }
        
        if (a.isCluster) {
            annotationView = (AnnotationClusterView *)[mapView dequeueReusableAnnotationViewWithIdentifier:ClusterIdentifier];
            
            if (annotationView == nil) {
                annotationView = [[AnnotationClusterView alloc] initWithAnnotation:a reuseIdentifier:ClusterIdentifier];
            }
            
            [((AnnotationClusterView *)annotationView) refreshWithAnnotation:annotation];
        }
        
        else {
            annotationView = (AnnotationView *)[mapView dequeueReusableAnnotationViewWithIdentifier:PinIdentifier];
            
            if (annotationView == nil) {
                annotationView = [[AnnotationView alloc] initWithAnnotation:[a.annotations anyObject]
                                                            reuseIdentifier:PinIdentifier];
                
            }
            
            [((AnnotationView *)annotationView) refreshWithAnnotation:[a.annotations anyObject]];
            ((AnnotationView *)annotationView).canShowCallout=YES;
        }
        
    }
    
    [SharedVariables sharedInstance].annotationCount = [SharedVariables sharedInstance].annotationCount + 1;
    
    return annotationView;
    
}

-(void)mapView:(MKMapView *)mapView didSelectAnnotationView:(MKAnnotationView *)view{
    
    if([view isKindOfClass:[AnnotationView class]]){
        //AnnotationView *hkView = (AnnotationView *)view;
        
        //ProfileViewController *profile = [[ProfileViewController alloc] initWithUser:hkView.emailUser andName:hkView.nameUser];
        //[self.navigationController pushViewController:profile animated:YES];
    }
    if([view isKindOfClass:[AnnotationClusterView class]]){
        KPAnnotation *cluster = (KPAnnotation *)view.annotation;
        if (cluster.annotations.count > 1){
            [self.map setRegion:MKCoordinateRegionMakeWithDistance(cluster.coordinate,cluster.radius * 2.5f,cluster.radius * 2.5f) animated:YES];
        }
        
    }
}

-(void)mapView:(MKMapView *)mapView annotationView:(MKAnnotationView *)view calloutAccessoryControlTapped:(UIControl *)control{
    AnnotationView *hkView = (AnnotationView *)view;

    NSLog(@"email:%@",hkView.emailUser);
    NSLog(@"email:%@",hkView.nameUser);
    

    NSMutableDictionary* dictionaryPostSelected = [[NSMutableDictionary alloc] init];
    
    NSMutableDictionary* dictionaryPost = [[NSMutableDictionary alloc] init];
    
    [dictionaryPost setObject:hkView.emailUser forKey:@"username"];
    [dictionaryPost setObject:hkView.nameUser forKey:@"extended_name"];
    
    [dictionaryPostSelected setObject:dictionaryPost forKey:@"user"];
    

        NSLog(@"selectedDict:%@",dictionaryPostSelected);
    
                
                UserProfileViewController * userProfileVC = [self.storyboard instantiateViewControllerWithIdentifier:@"UserProfileViewController"];
                userProfileVC.userDict = dictionaryPostSelected;
                //[self.navigationController pushViewController:userProfileVC animated:YES];
    [self.navigationController presentViewController:userProfileVC animated:YES completion:nil];


    
    
    
  //  ProfileViewController *profile = [[ProfileViewController alloc] initWithUser:hkView.emailUser andName:hkView.nameUser];
    //[self.navigationController pushViewController:profile animated:YES];
    
    /*
     [[[GAI sharedInstance] defaultTracker] set:[GAIFields customDimensionForIndex:1] value:annotation.shop.name];
     [[[GAI sharedInstance] defaultTracker] send:[[GAIDictionaryBuilder createEventWithCategory:EVENT_CATEGORY_UI_ACTION action:EVENT_ACTION_CLICK label:EVENT_LABEL_SHOP_DETAIL_INFO_WINDOW value:nil] build]];
     
     DetailShopViewController *dsc = [[DetailShopViewController alloc] initWithShop:annotation.shop];
     [self.navigationController pushViewController:dsc animated:YES];
     */
}

#pragma mark - KPCLUSERTINGCONTROL DELEGATE

- (void)clusteringController:(KPClusteringController *)clusteringController configureAnnotationForDisplay:(KPAnnotation *)annotation {
    if(annotation.annotations.count>1){
        annotation.title = [NSString stringWithFormat:@"%lu", (unsigned long)annotation.annotations.count];
    }else{
        annotation.title = ((Annotation *)[annotation.annotations anyObject]).title;
        annotation.subtitle = @"";
        [self lunchObserv:annotation withUser:((Annotation *)[annotation.annotations anyObject]).subtitle];
    }
}

-(void)lunchObserv:(KPAnnotation *)annotation withUser:(NSString *)email{
    Firebase *connectRef = [[Firebase alloc] initWithUrl:FIREBASE_URL];
    connectRef = [connectRef childByAppendingPath:[NSString stringWithFormat:@"users/%@/connections",[UtilityGraphic URLEncodedString:email]]];
    [connectRef observeEventType:FEventTypeValue withBlock:^(FDataSnapshot *snapshot) {
        if(![snapshot.value isEqual:[NSNull null]]){
            annotation.subtitle = @"Online";
            
        }else{
            Firebase *lastOnlineRef = [[Firebase alloc] initWithUrl:FIREBASE_URL];
            lastOnlineRef = [lastOnlineRef  childByAppendingPath:[NSString stringWithFormat:@"users/%@/lastOnLine",[UtilityGraphic URLEncodedString:email]]];
            [lastOnlineRef observeEventType:FEventTypeValue withBlock:^(FDataSnapshot *snapshot) {
                if(![snapshot.value isEqual:[NSNull null]]){
                    long long value = [snapshot.value longLongValue];
                    NSDate *date = [[NSDate alloc] initWithTimeIntervalSince1970:value/1000];
                   // NSString *ago = [date time];
                    annotation.subtitle=[NSString stringWithFormat:@"%@: %@",NSLocalizedString(@"Last seen",nil),@"5"];
                    
                }
            }];
            
        }
    }];
}
- (BOOL)clusteringControllerShouldClusterAnnotations:(KPClusteringController *)clusteringController {
    return YES;
}

- (void)clusteringControllerWillUpdateVisibleAnnotations:(KPClusteringController *)clusteringController {
    //NSLog(@"Clustering controller %@ will update visible annotations", clusteringController);
}

- (void)clusteringControllerDidUpdateVisibleMapAnnotations:(KPClusteringController *)clusteringController {
    //NSLog(@"Clustering controller %@ did update visible annotations", clusteringController);
}

- (void)clusteringController:(KPClusteringController *)clusteringController performAnimations:(void (^)())animations withCompletionHandler:(void (^)(BOOL))completion {
    [UIView animateWithDuration:0.5
                          delay:0
         usingSpringWithDamping:0.8
          initialSpringVelocity:0.6
                        options:UIViewAnimationOptionBeginFromCurrentState
                     animations:animations
                     completion:completion];
}




/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

- (IBAction)plusBtnAction:(id)sender {
}

- (IBAction)filterBtnAction:(id)sender {
    
    FiltersViewController * filterVC = [self.storyboard instantiateViewControllerWithIdentifier:@"FiltersViewController"];
    [self.navigationController pushViewController:filterVC animated:YES];
    
}

- (IBAction)mapBtnAction:(id)sender {
    
    self.tableViewOutlet.hidden = YES;
    self.mapBaseView.hidden = NO;
    //self.mapBtnOutlet.hidden = YES;
}

- (IBAction)peopleBtnAction:(id)sender {
    
        self.tableViewOutlet.hidden = NO;
        self.mapBaseView.hidden = YES;
        //self.mapBtnOutlet.hidden = NO;
        
}

- (IBAction)profilePicBtnAction:(id)sender {
    
    UIView* baseView = [sender superview];
    UIView* cell = [baseView superview];
    
    NSIndexPath *indexPath = [self.tableViewOutlet indexPathForCell:cell];
    
    NSDictionary*dictionaryPostSelected = self.visibleUserList[indexPath.row];
    
    NSLog(@"dict:%@",dictionaryPostSelected);
    
    UserProfileViewController * userProfileVC = [self.storyboard instantiateViewControllerWithIdentifier:@"UserProfileViewController"];
    userProfileVC.userDict = dictionaryPostSelected;
    //[self.navigationController pushViewController:userProfileVC animated:YES];
    [self.navigationController presentViewController:userProfileVC animated:YES completion:nil];
}
@end
