//
//  UtilityGraphic.m
//  Mabo
//
//  Created by Steewe MacBook Pro on 26/03/15.
//  Copyright (c) 2015 Steewe. All rights reserved.
//

#import "UtilityGraphic.h"
#import <Photos/Photos.h>

@implementation UtilityGraphic

#pragma mark - DRAW

+(void)drawLineOn:(UIView *)view withColor:(UIColor *)color withWidth:(float)width withPosition:(PositionLine)position{
    CAShapeLayer *line = [CAShapeLayer layer];
    NSString *newName = (position==Downer)?@"downer":@"upper";
    line.name = newName;
    
    for (CALayer *layer in view.layer.sublayers) {
        if ([layer.name isEqualToString:newName]) {
            [layer removeFromSuperlayer];
            break;
        }
    }
    
    UIBezierPath *path = [UIBezierPath bezierPath];
    
    float x1 =(position==Upper)?0:0;
    float x2 =(position==Upper)?view.frame.size.width:view.frame.size.width;
    float y1 =(position==Upper)?0:view.frame.size.height;
    float y2 =(position==Upper)?0:view.frame.size.height;
    
    [path moveToPoint:CGPointMake(x1, y1)];
    [path addLineToPoint:CGPointMake(x2, y2)];
    [line setPath:path.CGPath];
    [line setStrokeColor:color.CGColor];
    [line setLineWidth:width];
    [view.layer addSublayer:line];
}

+(void)drawLineOn:(UIView *)view withColor:(UIColor *)color withWidth:(float)width atPosition:(CGPoint)point{
    CAShapeLayer *line = [CAShapeLayer layer];
    /*
    NSString *newName = (position==Downer)?@"downer":@"upper";
    line.name = newName;
    
    for (CALayer *layer in view.layer.sublayers) {
        if ([layer.name isEqualToString:newName]) {
            [layer removeFromSuperlayer];
            break;
        }
    }
    */
    UIBezierPath *path = [UIBezierPath bezierPath];
    
    float x1 =0;
    float x2 =view.frame.size.width;
    float y1 =point.y;
    float y2 =point.y;
    
    [path moveToPoint:CGPointMake(x1, y1)];
    [path addLineToPoint:CGPointMake(x2, y2)];
    [line setPath:path.CGPath];
    [line setStrokeColor:color.CGColor];
    [line setLineWidth:width];
    [view.layer addSublayer:line];
}

+(void)drawShadowOn:(UIView *)view withPosition:(PositionLine)position{
    CGSize sizePosition = CGSizeMake(0, (position==Upper)?-3:3);
    [view.layer setShadowColor:[UIColor blackColor].CGColor];
    [view.layer setShadowOffset:sizePosition];
    [view.layer setShadowOpacity:.5];
    [view.layer setShadowRadius:3];
}

+(void)drawShadowRegular:(UIView *)view {
    CGSize sizePosition = CGSizeMake(0,0);
    [view.layer setShadowColor:[UIColor blackColor].CGColor];
    [view.layer setShadowOffset:sizePosition];
    [view.layer setShadowOpacity:.5];
    [view.layer setShadowRadius:5];
}


+(void)drawInsideShadowOn:(UIView *)view withPosition:(PositionLine)position{
    CAGradientLayer *gradientShadow = [CAGradientLayer layer];
    float heightShadow = 10;
    float x=(position==Upper)?0:0;
    float y=(position==Upper)?0:view.frame.size.height-heightShadow;
    
    float alpha1 = (position==Upper)?.5:0;
    float alpha2 = (position==Upper)?0:.5;
    
    gradientShadow.frame = CGRectMake(x, y, view.bounds.size.width, heightShadow);
    gradientShadow.colors = @[(id)[[UIColor blackColor] colorWithAlphaComponent:alpha1].CGColor,(id)[[UIColor blackColor] colorWithAlphaComponent:alpha2].CGColor];
    [view.layer addSublayer:gradientShadow];

}

#pragma mark - POSITION

+(CGPoint)getDownerLeftOf:(UIView *)view{
    return CGPointMake(view.frame.origin.x, view.frame.origin.y+view.frame.size.height);
}

+(void)slideYView:(UIView *)view inY:(float)newY{
    [view setFrame:CGRectMake(view.frame.origin.x, newY, view.frame.size.width, view.frame.size.height)];
}

#pragma mark - CONVERT

+ (UIImage *)imageFromLayer:(CALayer *)layer{
    UIGraphicsBeginImageContext([layer frame].size);
    
    [layer renderInContext:UIGraphicsGetCurrentContext()];
    UIImage *outputImage = UIGraphicsGetImageFromCurrentImageContext();
    
    UIGraphicsEndImageContext();
    
    return outputImage;
}

+ (UIImage*)rotateUIImage:(UIImage*)sourceImage clockwise:(BOOL)clockwise{
    CGSize size = sourceImage.size;
    UIGraphicsBeginImageContext(CGSizeMake(size.height, size.width));
    [[UIImage imageWithCGImage:[sourceImage CGImage] scale:1.0 orientation:clockwise ? UIImageOrientationRight : UIImageOrientationLeft] drawInRect:CGRectMake(0,0,size.height ,size.width)];
    UIImage* newImage = UIGraphicsGetImageFromCurrentImageContext();
    UIGraphicsEndImageContext();
    
    return newImage;
}

+ (UIImage *)imageWithImage:(UIImage *)image scaledToSize:(CGSize)newSize {
    
    
    float ratio = MAX(newSize.width/image.size.width, newSize.height/image.size.height);
    newSize = CGSizeMake(image.size.width*ratio, image.size.height*ratio);
    UIGraphicsBeginImageContextWithOptions(newSize, NO, 0.0);
    [image drawInRect:CGRectMake(0, 0, newSize.width, newSize.height)];
    CGContextSetInterpolationQuality( UIGraphicsGetCurrentContext() , kCGInterpolationHigh );
    UIImage *newImage = UIGraphicsGetImageFromCurrentImageContext();
    UIGraphicsEndImageContext();
    return newImage;
}

+(NSArray *)filterWithKey:(NSString *)key withValue:(id)value inArray:(NSArray *)array withType:(int)classTipe{
    NSMutableArray *arrayPartial = [NSMutableArray arrayWithCapacity:0];
    for(unsigned i=0;i<array.count;i++){
        NSDictionary *dic = [array objectAtIndex:i];
        if(classTipe==0){
            if([[dic objectForKey:key] isEqualToString:[value stringValue]]){
                [arrayPartial addObject:dic];
            }
        }else{
            if([[dic objectForKey:key] intValue] == [value intValue]){
                [arrayPartial addObject:dic];
            }
        }
        
    }
    /*
    NSPredicate *predicate;
    if(classTipe==0){
        NSString *valueString = [value stringValue];
        predicate = [NSPredicate predicateWithFormat:@"(%@==%@)",key,valueString];
    }else{
        int valueInt = [value intValue];
        NSLog(@"value int: %i",valueInt);
        predicate = [NSPredicate predicateWithFormat:@"(%@==%d)",key,valueInt];
    }
   
    NSArray *arrayResult = [array filteredArrayUsingPredicate:predicate];
     */
    
    return [NSArray arrayWithArray:arrayPartial];
}


+(NSString *)URLEncodedString:(NSString *)string {
    NSMutableString * output = [NSMutableString string];
    const unsigned char * source = (const unsigned char *)[string UTF8String];
    int sourceLen = (int)strlen((const char *)source);
    for (int i = 0; i < sourceLen; ++i) {
        const unsigned char thisChar = source[i];
        if (thisChar == ' '){
            [output appendString:@"+"];
        } else if (thisChar == '-' || thisChar == '_' || thisChar == '~' ||
                   (thisChar >= 'a' && thisChar <= 'z') ||
                   (thisChar >= 'A' && thisChar <= 'Z') ||
                   (thisChar >= '0' && thisChar <= '9')) {
            [output appendFormat:@"%c", thisChar];
        } else {
            [output appendFormat:@"%%%02X", thisChar];
        }
    }
    return output;
}

@end
