//
//  MultipleAlertView.h
//  Mobo
//
//  Created by vishnuprasathg on 10/24/17.
//  Copyright © 2017 DCI. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface MultipleAlertView : UIView

@property (weak, nonatomic) IBOutlet UILabel *alertTitleLblOutlet;
@property (weak, nonatomic) IBOutlet UILabel *alertmessageLblOut;
@property (weak, nonatomic) IBOutlet UIButton *noBtnOutlet;
@property (weak, nonatomic) IBOutlet UIButton *yesBtnOutlet;


@property (strong, nonatomic) NSString * yesActionResponse;
@property (strong, nonatomic) UIViewController * alertParentViewController;

- (IBAction)noBtnAction:(id)sender;
- (IBAction)yesBtnAction:(id)sender;


-(void)alertWithMultiple:(NSString *)title message:(NSString *)message;

@end
