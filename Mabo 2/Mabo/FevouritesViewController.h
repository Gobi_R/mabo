//
//  MyPostViewController.h
//  Mobo
//
//  Created by vishnuprasathg on 10/23/17.
//  Copyright © 2017 DCI. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface FevouritesViewController : UIViewController<UITableViewDelegate,UITableViewDataSource,UIGestureRecognizerDelegate,UITextFieldDelegate>
@property (weak, nonatomic) IBOutlet UISegmentedControl *myPostSegmentControlOutlet;
- (IBAction)MyPostSegmentControlAction:(id)sender;
- (IBAction)newBtnAction:(id)sender;
@property (strong, nonatomic) IBOutlet UIView *textPostViewOutlet;
@property (strong, nonatomic) IBOutlet UIView *eventPostViewOutlet;
@property (weak, nonatomic) IBOutlet UITableView *myPostTableViewOutlet;
@property (weak, nonatomic) IBOutlet UIButton *backBtnOutlet;
- (IBAction)backbtnAction:(id)sender;
- (IBAction)delectPostAction:(id)sender;
- (IBAction)locationBtnAction:(id)sender;
- (IBAction)likeBtnAction:(id)sender;
- (IBAction)commentsBtnAction:(id)sender;
@property (weak, nonatomic) IBOutlet UILabel *emptyPostLblOutlet;

- (IBAction)editPostBtnAction:(id)sender;
- (IBAction)joiningBtnAction:(id)sender;
- (IBAction)mayBeBtnAction:(id)sender;
- (IBAction)willBeThereAction:(id)sender;
- (IBAction)shareBtnAction:(id)sender;
- (IBAction)myPostProfilePicAction:(id)sender;

@property (weak, nonatomic) IBOutlet UIButton *leftBarBtnOutlet;
- (IBAction)leftBarBtnAction:(id)sender;

- (IBAction)PostLikedThisBtnaction:(id)sender;
- (IBAction)PostCommentsBtnAction:(id)sender;

@property (nonatomic, retain) NSString * postComment;

@property (weak, nonatomic) IBOutlet UIView *mapViewBaseView;

-(void)commentOk;
-(void)postDelete;

@end
