//
//  CustomAlertView.m
//  Mobo
//
//  Created by vishnuprasathg on 10/16/17.
//  Copyright © 2017 DCI. All rights reserved.
//

#import "CustomAlertView.h"
#import "Utilities.h"
#import "SettingsViewController.h"
#import "ChangePasswordViewController.h"
#import "CreatPostViewController.h"
#import "SignInViewController.h"
#import "ProfileViewController.h"
#import "ChatViewController.h"
#import "UserProfileViewController.h"

@implementation CustomAlertView

/*
// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect {
    // Drawing code
}
*/

- (IBAction)okBtnAction:(id)sender {
    
    
    [[Utilities sharedInstance] hideCustomAlertView:self.superview];
    
    if ([self.yesActionResponse isEqualToString:@"changePassword"]) {
        
        if ([_alertParentViewController isKindOfClass:ChangePasswordViewController.class]) {
            

                [[Utilities sharedInstance] hideCustomAlertView:self.superview];
                
                ChangePasswordViewController * changePwdVC = _alertParentViewController;
                
                [changePwdVC changePwdAfterSuccess];
            }

    }
    
    else if ([self.yesActionResponse isEqualToString:@"creatPost"])
    {
        if ([_alertParentViewController isKindOfClass:CreatPostViewController.class]) {
            
            
            [[Utilities sharedInstance] hideCustomAlertView:self.superview];
            
            CreatPostViewController * creatPostVC = _alertParentViewController;
            
            [creatPostVC creatSuccessAction];
        }
    }
    else if ([self.yesActionResponse isEqualToString:@"creatPostFail"])
    {
        if ([_alertParentViewController isKindOfClass:CreatPostViewController.class]) {
            
            
            [[Utilities sharedInstance] hideCustomAlertView:self.superview];
            
//            CreatPostViewController * creatPostVC = _alertParentViewController;
//
//            [creatPostVC creatSuccessAction];
        }
    }
    else if ([self.yesActionResponse isEqualToString:@"chatView"])
    {
        if ([ChatViewController isKindOfClass:CreatPostViewController.class]) {
            
            
            [[Utilities sharedInstance] hideCustomAlertView:self.superview];
            
            //            CreatPostViewController * creatPostVC = _alertParentViewController;
            //
            //            [creatPostVC creatSuccessAction];
        }
    }
    else if ([self.yesActionResponse isEqualToString:@"forgot"])
    {
        if ([SignInViewController isKindOfClass:SignInViewController.class]) {
            
            
            [[Utilities sharedInstance] hideCustomAlertView:self.superview];
            
            //            CreatPostViewController * creatPostVC = _alertParentViewController;
            //
            //            [creatPostVC creatSuccessAction];
        }
    }
    
    else if ([self.yesActionResponse isEqualToString:@"registraion"])
    {
        if ([_alertParentViewController isKindOfClass:SignInViewController.class]) {
            
            
            [[Utilities sharedInstance] hideCustomAlertView:self.superview];
            
            SignInViewController * signVC = _alertParentViewController;
            
            [signVC afterRegisterSuccessfully];
        }
    }
    else if ([self.yesActionResponse isEqualToString:@"profileViewController"])
    {
        if ([_alertParentViewController isKindOfClass:ProfileViewController.class]) {
            
            
            [[Utilities sharedInstance] hideCustomAlertView:self.superview];
            
            ProfileViewController * profileVC = _alertParentViewController;
            
            [profileVC afterEditButtonPressed];
        }
    }
    else if ([self.yesActionResponse isEqualToString:@"blockedUser"])
    {
        if ([_alertParentViewController isKindOfClass:UserProfileViewController.class]) {
            
            
            [[Utilities sharedInstance] hideCustomAlertView:self.superview];
            
            UserProfileViewController * profileVC = _alertParentViewController;
            
            [profileVC blockedSuccessfully];
        }
    }
    
    
    
}


@end
