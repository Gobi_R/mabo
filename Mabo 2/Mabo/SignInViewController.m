//
//  SignInViewController.m
//  Mobo
//
//  Created by vishnuprasathg on 10/13/17.
//  Copyright © 2017 DCI. All rights reserved.


#import "SignInViewController.h"
#import "LoadingView.h"
#import "Client.h"
#import "Utilities.h"
#import "User.h"
#import "SharedVariables.h"
#import "TabBarViewController.h"
#import "SinchClient.h"

@interface SignInViewController ()

@property (nonatomic, strong) LoadingView *loading;

@end

@implementation SignInViewController

{
    User * user;
    BOOL isCheck;
    BOOL isCreated;
}


- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    
    
    
    UITapGestureRecognizer *tap = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(dismissKeyboard)];
    
    [self.view addGestureRecognizer:tap];
    
    isCheck = NO;
    
    [self.navigationController setNavigationBarHidden:YES animated:NO];
    self.automaticallyAdjustsScrollViewInsets = NO;
}



- (void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
    
}

- (void)viewWillDisappear:(BOOL)animated {
    [super viewWillDisappear:animated];
    
}


- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

-(void)dismissKeyboard
{
    [self.emailTxtFieldOulet resignFirstResponder];
    [self.passwordTxtFieldOutlet resignFirstResponder];
    [self.creatEmailTxtFldOutlet resignFirstResponder];
    [self.creatUserNameTxtFldOutlet resignFirstResponder];
    [self.creatPasswordTxtFldOutlet resignFirstResponder];
    [self.creatRepeatPasswordTxtFldOutlet resignFirstResponder];
}


- (void)textFieldDidBeginEditing:(UITextField *)textField {
    if (textField == self.emailTxtFieldOulet) {
        [UIView beginAnimations:nil context:NULL];
        [UIView setAnimationDelegate:self];
        [UIView setAnimationDuration:0.5];
        [UIView setAnimationBeginsFromCurrentState:YES];
        self.signInBaseView.frame = CGRectMake(self.signInBaseView.frame.origin.x, (self.signInBaseView.frame.origin.y - 100.0), self.signInBaseView.frame.size.width, self.signInBaseView.frame.size.height);
        [UIView commitAnimations];
    } else if (textField == self.passwordTxtFieldOutlet) {
        [UIView beginAnimations:nil context:NULL];
        [UIView setAnimationDelegate:self];
        [UIView setAnimationDuration:0.5];
        [UIView setAnimationBeginsFromCurrentState:YES];
        self.signInBaseView.frame = CGRectMake(self.signInBaseView.frame.origin.x, (self.signInBaseView.frame.origin.y - 100.0), self.signInBaseView.frame.size.width, self.signInBaseView.frame.size.height);
        [UIView commitAnimations];
    }
    else if (textField == self.creatEmailTxtFldOutlet) {
        [UIView beginAnimations:nil context:NULL];
        [UIView setAnimationDelegate:self];
        [UIView setAnimationDuration:0.5];
        [UIView setAnimationBeginsFromCurrentState:YES];
        self.creatNewAccountBseView.frame = CGRectMake(self.creatNewAccountBseView.frame.origin.x, (self.creatNewAccountBseView.frame.origin.y - 100.0), self.creatNewAccountBseView.frame.size.width, self.creatNewAccountBseView.frame.size.height);
        [UIView commitAnimations];
    } else if (textField == self.creatUserNameTxtFldOutlet) {
        [UIView beginAnimations:nil context:NULL];
        [UIView setAnimationDelegate:self];
        [UIView setAnimationDuration:0.5];
        [UIView setAnimationBeginsFromCurrentState:YES];
        self.creatNewAccountBseView.frame = CGRectMake(self.creatNewAccountBseView.frame.origin.x, (self.creatNewAccountBseView.frame.origin.y - 100.0), self.creatNewAccountBseView.frame.size.width, self.creatNewAccountBseView.frame.size.height);
        [UIView commitAnimations];
    }
    else if (textField == self.creatPasswordTxtFldOutlet) {
        [UIView beginAnimations:nil context:NULL];
        [UIView setAnimationDelegate:self];
        [UIView setAnimationDuration:0.5];
        [UIView setAnimationBeginsFromCurrentState:YES];
        self.creatNewAccountBseView.frame = CGRectMake(self.creatNewAccountBseView.frame.origin.x, (self.creatNewAccountBseView.frame.origin.y - 100.0), self.creatNewAccountBseView.frame.size.width, self.creatNewAccountBseView.frame.size.height);
        [UIView commitAnimations];
    } else if (textField == self.creatRepeatPasswordTxtFldOutlet) {
        [UIView beginAnimations:nil context:NULL];
        [UIView setAnimationDelegate:self];
        [UIView setAnimationDuration:0.5];
        [UIView setAnimationBeginsFromCurrentState:YES];
        self.creatNewAccountBseView.frame = CGRectMake(self.creatNewAccountBseView.frame.origin.x, (self.creatNewAccountBseView.frame.origin.y - 150.0), self.creatNewAccountBseView.frame.size.width, self.creatNewAccountBseView.frame.size.height);
        [UIView commitAnimations];
    }
    
}
- (void)textFieldDidEndEditing:(UITextField *)textField {
    if (textField == self.emailTxtFieldOulet) {
        [UIView beginAnimations:nil context:NULL];
        [UIView setAnimationDelegate:self];
        [UIView setAnimationDuration:0.5];
        [UIView setAnimationBeginsFromCurrentState:YES];
        self.signInBaseView.frame = CGRectMake(self.signInBaseView.frame.origin.x, (self.signInBaseView.frame.origin.y + 100.0), self.signInBaseView.frame.size.width, self.signInBaseView.frame.size.height);
        [UIView commitAnimations];
    } else if (textField == self.passwordTxtFieldOutlet) {
        [UIView beginAnimations:nil context:NULL];
        [UIView setAnimationDelegate:self];
        [UIView setAnimationDuration:0.5];
        [UIView setAnimationBeginsFromCurrentState:YES];
        self.signInBaseView.frame = CGRectMake(self.signInBaseView.frame.origin.x, (self.signInBaseView.frame.origin.y + 100.0), self.signInBaseView.frame.size.width, self.signInBaseView.frame.size.height);
        [UIView commitAnimations];
    }
    else if (textField == self.creatEmailTxtFldOutlet) {
        [UIView beginAnimations:nil context:NULL];
        [UIView setAnimationDelegate:self];
        [UIView setAnimationDuration:0.5];
        [UIView setAnimationBeginsFromCurrentState:YES];
        self.creatNewAccountBseView.frame = CGRectMake(self.creatNewAccountBseView.frame.origin.x, (self.creatNewAccountBseView.frame.origin.y + 100.0), self.creatNewAccountBseView.frame.size.width, self.creatNewAccountBseView.frame.size.height);
        [UIView commitAnimations];
    } else if (textField == self.creatUserNameTxtFldOutlet) {
        [UIView beginAnimations:nil context:NULL];
        [UIView setAnimationDelegate:self];
        [UIView setAnimationDuration:0.5];
        [UIView setAnimationBeginsFromCurrentState:YES];
        self.creatNewAccountBseView.frame = CGRectMake(self.creatNewAccountBseView.frame.origin.x, (self.creatNewAccountBseView.frame.origin.y + 100.0), self.creatNewAccountBseView.frame.size.width, self.creatNewAccountBseView.frame.size.height);
        [UIView commitAnimations];
    }
    else if (textField == self.creatPasswordTxtFldOutlet) {
        [UIView beginAnimations:nil context:NULL];
        [UIView setAnimationDelegate:self];
        [UIView setAnimationDuration:0.5];
        [UIView setAnimationBeginsFromCurrentState:YES];
        self.creatNewAccountBseView.frame = CGRectMake(self.creatNewAccountBseView.frame.origin.x, (self.creatNewAccountBseView.frame.origin.y + 100.0), self.creatNewAccountBseView.frame.size.width, self.creatNewAccountBseView.frame.size.height);
        [UIView commitAnimations];
    } else if (textField == self.creatRepeatPasswordTxtFldOutlet) {
        [UIView beginAnimations:nil context:NULL];
        [UIView setAnimationDelegate:self];
        [UIView setAnimationDuration:0.5];
        [UIView setAnimationBeginsFromCurrentState:YES];
        self.creatNewAccountBseView.frame = CGRectMake(self.creatNewAccountBseView.frame.origin.x, (self.creatNewAccountBseView.frame.origin.y + 150.0), self.creatNewAccountBseView.frame.size.width, self.creatNewAccountBseView.frame.size.height);
        [UIView commitAnimations];
    }
    
}

-(BOOL) textFieldShouldReturn: (UITextField *) textField
{
    [textField resignFirstResponder];
    
    return YES;
}

-(void)setViewMovedUp:(BOOL)movedUp
{
    [UIView beginAnimations:nil context:NULL];
    
    [UIView setAnimationDuration:0.3]; // if you want to slide up the view
    
    CGRect rect = self.view.frame;
    
    if (movedUp)
    {
        // 1. move the view's origin up so that the text field that will be hidden come above the keyboard
        // 2. increase the size of the view so that the area behind the keyboard is covered up.
        rect.origin.y -= 253;
        rect.size.height += self.view.frame.size.height;
    }
    else
    {
        // revert back to the normal state.
        rect.origin.y += 253;
        rect.size.height -= self.view.frame.size.height;
    }
    self.view.frame = rect;
    
    [UIView commitAnimations];
    
    
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

- (IBAction)forgotBtnAction:(id)sender {
    
    [[Utilities sharedInstance] showCustomForgotPwdView:self title:@"Mabo" message:@"" actionResponse:@"forgotPassword"];
}

-(void)forgotOkAction
{
    self.loading = [[LoadingView alloc] initWithFrame:[UIScreen mainScreen].bounds];
    [self.view addSubview:self.loading];
    [self.loading animate:YES];
}
-(void)forgotfailiour
{
  [self.loading animate:NO];
    
}

-(void)forgotYesAction
{
    [self.loading animate:NO];
    [[Utilities sharedInstance] showCustomAlertView:self.view title:@"Mabo" message:@"Please check your mail" controller:self reason:@"forgotPwdSussecc"];
    
}

- (IBAction)creatNewAccountBtnAction:(id)sender {
    
    [self.emailTxtFieldOulet resignFirstResponder];
    [self.passwordTxtFieldOutlet resignFirstResponder];
    [self.creatEmailTxtFldOutlet resignFirstResponder];
    [self.creatUserNameTxtFldOutlet resignFirstResponder];
    [self.creatPasswordTxtFldOutlet resignFirstResponder];
    [self.creatRepeatPasswordTxtFldOutlet resignFirstResponder];
    
    self.signInBaseView.hidden = YES;
    self.creatNewAccountBseView.hidden = NO;

}

- (IBAction)creatAccountBtnAction:(id)sender {
    
    [self.creatEmailTxtFldOutlet resignFirstResponder];
    [self.creatUserNameTxtFldOutlet resignFirstResponder];
    [self.creatPasswordTxtFldOutlet resignFirstResponder];
    [self.creatRepeatPasswordTxtFldOutlet resignFirstResponder];
    
    BOOL isvalid = [Utilities isValidMail:self.creatEmailTxtFldOutlet.text];
    
    if(self.creatEmailTxtFldOutlet.text.length>0){
        
    if (isvalid == YES) {
      //  if(self.creatEmailTxtFldOutlet.text.length>0 && self.creatPasswordTxtFldOutlet.text.length>0 && [self.creatPasswordTxtFldOutlet.text isEqualToString:self.creatRepeatPasswordTxtFldOutlet.text] && self.creatUserNameTxtFldOutlet.text.length>0){
        if(self.creatUserNameTxtFldOutlet.text.length>0){
            
            BOOL containsNumber = NSNotFound != [self.creatUserNameTxtFldOutlet.text rangeOfCharacterFromSet:NSCharacterSet.decimalDigitCharacterSet].location;
            if (containsNumber == NO) {
                
        if(self.creatPasswordTxtFldOutlet.text.length>0){
            
            if (self.creatPasswordTxtFldOutlet.text.length>=8 && self.creatPasswordTxtFldOutlet.text.length<=15) {
                
                if(self.creatRepeatPasswordTxtFldOutlet.text.length>0){
            
            if([self.creatPasswordTxtFldOutlet.text isEqualToString:self.creatRepeatPasswordTxtFldOutlet.text]){
        
                self.loading = [[LoadingView alloc] initWithFrame:[UIScreen mainScreen].bounds];
                [self.view addSubview:self.loading];
                [self.loading animate:YES];
                
        NSLog(@"emailAccount: %@",self.creatEmailTxtFldOutlet.text);
        NSDictionary *dictionary = [NSDictionary dictionaryWithObjectsAndKeys:self.creatEmailTxtFldOutlet.text,@"email",
                                    self.creatEmailTxtFldOutlet.text,@"username",
                                    self.creatPasswordTxtFldOutlet.text,@"plainPassword",nil];
                
                NSDictionary * formDict = [NSDictionary dictionaryWithObjectsAndKeys:dictionary,@"signup_form",nil];
        
        [[Client sharedClient] createAccountWithDictionary:formDict completition:^(BOOL success, NSError *error) {
            if(success){
                NSLog(@"create acount ok");
                [[Client sharedClient] checkAuthenticateWithCompletition:^(BOOL success) {
                    [_loading animate:NO];
                    if(success){
                        [_loading animate:NO];
//                        self.signInBaseView.hidden = NO;
//                        self.creatNewAccountBseView.hidden = YES;
                        
                        NSDictionary *dictionaryRestInfo = @{@"extended_name":self.creatUserNameTxtFldOutlet.text,@"location_visibility":[NSNumber numberWithInt:1],@"is_scrumble":[NSNumber numberWithInt:0]};
                        
                        [[Client sharedClient].user initWithDictionary:dictionaryRestInfo];
                        [[Client sharedClient] updateUserInfoWithCompletition:^(BOOL success, NSError *error) {
                            
                            if (success == YES) {
                                
                                //[[SinchClient sharedClient] activeClient];
                                
                                [[Utilities sharedInstance]showCustomAlertView:self.view title:@"Mabo" message:@"Successfully registered" controller:self reason:@"registraion"];
                                
                            }
                        }];
                        
                        self.creatEmailTxtFldOutlet.text = @"";
                        self.creatUserNameTxtFldOutlet.text = @"";
                        self.creatPasswordTxtFldOutlet.text = @"";
                        self.creatRepeatPasswordTxtFldOutlet.text = @"";
                        
//                        AppDelegate *ad = [UIApplication sharedApplication].delegate;
//                        if([ad.navController.visibleViewController isKindOfClass:[self class]]){
//                            [self goToTutorial];
//                            //[ad switchRootController];
//                        }
                    }
                }];
                
            }else{
                NSLog(@"responceDict: %@",dictionary);
                [_loading animate:NO];
               // [[Utilities sharedInstance]showCustomAlertView:self.view title:@"Warning" message:[NSString stringWithFormat:@"%@",error]];
                
                [[Utilities sharedInstance] showCustomAlertView:self.view title:@"Warning" message:[NSString stringWithFormat:@"%@",error] controller:self reason:@"creatAccount"];
                //[Utilities alertWithMessage:error.description];
                NSLog(@"error create: %@",error);
            }
        }];
            }
            else
            {
                //[Utilities alertWithMessage:@"Password and repeat password does not match."];
                //[[Utilities sharedInstance]showCustomAlertView:self.view title:@"Warning" message:@"Password and repeat password does not match."];
                [[Utilities sharedInstance] showCustomAlertView:self.view title:@"Warning" message:@"Password and repeat password does not match." controller:self reason:@"creatAccount"];
            }
            }
            else
            {
                //[Utilities alertWithMessage:@"Password enter repeat password."];
                //[[Utilities sharedInstance]showCustomAlertView:self.view title:@"Warning" message:@"Password enter repeat password."];
                
                [[Utilities sharedInstance] showCustomAlertView:self.view title:@"Warning" message:@"Password enter repeat password." controller:self reason:@"creatAccount"];
                
            }
            }
            else
            {
                //[Utilities alertWithMessage:@"Password between 8 and 15 characters."];
                //[[Utilities sharedInstance]showCustomAlertView:self.view title:@"Warning" message:@"Password between 8 and 15 characters."];
                
                [[Utilities sharedInstance] showCustomAlertView:self.view title:@"Warning" message:@"Password between 8 and 15 characters." controller:self reason:@"creatAccount"];
            }
        }
        else
        {
           // [Utilities alertWithMessage:@"Please enter password."];
           // [[Utilities sharedInstance]showCustomAlertView:self.view title:@"Warning" message:@"Please enter password."];
            
            [[Utilities sharedInstance] showCustomAlertView:self.view title:@"Warning" message:@"Please enter password." controller:self reason:@"creatAccount"];
        }
        }
        else
        {
           // [Utilities alertWithMessage:@"Username cannot be numbers."];
           // [[Utilities sharedInstance]showCustomAlertView:self.view title:@"Warning" message:@"Username cannot be numbers."];
            
            [[Utilities sharedInstance] showCustomAlertView:self.view title:@"Warning" message:@"Username cannot be numbers." controller:self reason:@"creatAccount"];
        }
        }
        else
        {
            //[Utilities alertWithMessage:@"Please enter username."];
           // [[Utilities sharedInstance]showCustomAlertView:self.view title:@"Warning" message:@"Please enter username."];
            
            [[Utilities sharedInstance] showCustomAlertView:self.view title:@"Warning" message:@"Please enter username." controller:self reason:@"creatAccount"];
        }
        
    }
    else
    {
        //[Utilities alertWithMessage:@"Enter valid email address."];
       // [[Utilities sharedInstance]showCustomAlertView:self.view title:@"Warning" message:@"Enter valid email address."];
        
        [[Utilities sharedInstance] showCustomAlertView:self.view title:@"Warning" message:@"Enter valid email address." controller:self reason:@"creatAccount"];
    }
        
    }
        else
        {
            //[Utilities alertWithMessage:@"Please enter email Id."];
           // [[Utilities sharedInstance]showCustomAlertView:self.view title:@"Warning" message:@"Please enter email Id."];
            
            [[Utilities sharedInstance] showCustomAlertView:self.view title:@"Warning" message:@"Please enter email Id." controller:self reason:@"creatAccount"];
        }
    
    //self.signInBaseView.hidden = YES;
    //self.creatNewAccountBseView.hidden = YES;
}

-(void)afterRegisterSuccessfully
{
    if ([SharedVariables sharedInstance].isCreated == YES) {
        
        [SharedVariables sharedInstance].isCreated = NO;
        
//        [[NSNotificationCenter defaultCenter] postNotificationName:@"UserDidLoginNotification"
//                                                            object:nil
//                                                          userInfo:@{
//                                                                     @"userId" : [Client sharedClient].user.email
//                                                                     }];
        
        TabBarViewController * tabVC = [self.storyboard instantiateViewControllerWithIdentifier:@"TabBarViewController"];
        [self.navigationController pushViewController:tabVC animated:YES];
        
        [[NSUserDefaults standardUserDefaults]setBool:NO forKey:@"isFaceBook"];
    }
}

- (IBAction)signInBtnAction:(id)sender {
    
    
    [self.emailTxtFieldOulet resignFirstResponder];
    [self.passwordTxtFieldOutlet resignFirstResponder];
    
    
    BOOL isvalid = [Utilities isValidMail:self.emailTxtFieldOulet.text];
    
    
    if(self.emailTxtFieldOulet.text.length>0){
    
    
    if (isvalid == YES) {
        
        if (isCheck == YES) {
            
            if([self.emailTxtFieldOulet isTouchInside])[self.emailTxtFieldOulet resignFirstResponder];
            if([self.passwordTxtFieldOutlet isTouchInside])[self.passwordTxtFieldOutlet resignFirstResponder];
                
                if(self.passwordTxtFieldOutlet.text.length>0){
                    
                [SharedVariables sharedInstance].signInEmailString = self.emailTxtFieldOulet.text;
                [SharedVariables sharedInstance].signInPasswordString = self.passwordTxtFieldOutlet.text;
                
                self.loading = [[LoadingView alloc] initWithFrame:[UIScreen mainScreen].bounds];
                [self.view addSubview:self.loading];
                [self.loading animate:YES];
                
                [[Client sharedClient] loginWithEmail:self.emailTxtFieldOulet.text withPassword:self.passwordTxtFieldOutlet.text passwordPlain:YES completition:^(BOOL success, NSError *error) {
                    if(success){
                        NSLog(@"OK Login");
                        [self.loading animate:NO];
                        
                        [self.emailTxtFieldOulet resignFirstResponder];
                        [self.passwordTxtFieldOutlet resignFirstResponder];
                        
                        //[[SinchClient sharedClient] activeClient];
                        
//                        [[NSNotificationCenter defaultCenter] postNotificationName:@"UserDidLoginNotification"
//                                                                            object:nil
//                                                                          userInfo:@{
//                                                                                     @"userId" : [Client sharedClient].user.email
//                                                                                     }];
                        
                        TabBarViewController * tabVC = [self.storyboard instantiateViewControllerWithIdentifier:@"TabBarViewController"];
                        [self.navigationController pushViewController:tabVC animated:YES];
                        
                        [[NSUserDefaults standardUserDefaults]setBool:NO forKey:@"isFaceBook"];
                        
                    }else{
                        
                        [self.loading animate:NO];
                        
                        [self.checkBoxBtnOutlet setImage:[UIImage imageNamed:@"checkbox.png"] forState:UIControlStateNormal];
                        
                        isCheck = NO;
                        //[[Utilities sharedInstance]showCustomAlertView:self.view title:@"Warning" message:[NSString stringWithFormat:@"%@",error]];
                        NSString* erroeString = [NSString stringWithFormat:@"%@",error];
                        
                        if ([erroeString isEqualToString:@"(null)"]) {
                            
                            [[Utilities sharedInstance] showCustomAlertView:self.view title:@"Warning" message:@"Please Check email or password" controller:self reason:@"signIn"];
                            
                        }
                        else
                        {
                        [[Utilities sharedInstance] showCustomAlertView:self.view title:@"Warning" message:erroeString controller:self reason:@"signIn"];
                        }
                        //[Utilities alertWithMessage:error.description];
                        NSLog(@"error create: %@",error);
                    }
                    
                    //[self deleteLoading];
                    
                }];
                        
                }
                else
                {
                   // [Utilities alertWithMessage:@"Please enter password"];
                   // [[Utilities sharedInstance]showCustomAlertView:self.view title:@"Warning" message:@"Please enter password."];
                    
                    [[Utilities sharedInstance] showCustomAlertView:self.view title:@"Warning" message:@"Please enter password." controller:self reason:@"signIn"];
                }
        
            
        }
        else
        {
          // [Utilities alertWithMessage:@"Accept terms and conditions before sign-in."];
            //[[Utilities sharedInstance]showCustomAlertView:self.view title:@"Warning" message:@"Accept terms and conditions before sign-in."];
            
            [[Utilities sharedInstance] showCustomAlertView:self.view title:@"Warning" message:@"Accept terms of use before sign-in." controller:self reason:@"signIn"];
        }
    }
    else
    {
       // [Utilities alertWithMessage:@"Enter valid email address."];
       // [[Utilities sharedInstance]showCustomAlertView:self.view title:@"Warning" message:@"Enter valid email address."];
        [[Utilities sharedInstance] showCustomAlertView:self.view title:@"Warning" message:@"Enter valid email address." controller:self reason:@"signIn"];
    }
        
    }
    else
        {
           // [[Utilities sharedInstance]showCustomAlertView:self.view title:@"Warning" message:@"Please enter email Id"];
            
            [[Utilities sharedInstance] showCustomAlertView:self.view title:@"Warning" message:@"Please enter email Id" controller:self reason:@"signIn"];
           //[Utilities alertWithMessage:@"Please enter email Id"];
            
        }
    
    
}

- (IBAction)checkBoxBtnAction:(id)sender {
    
    [self.emailTxtFieldOulet resignFirstResponder];
    [self.passwordTxtFieldOutlet resignFirstResponder];
    
    if (isCheck == NO) {
        
        // [self.checkBoxBtnOutlet setBackgroundImage:[UIImage imageNamed:@"checkbox_on.png"] forState:UIControlStateNormal];
        
        [self.checkBoxBtnOutlet setImage:[UIImage imageNamed:@"checkbox_on.png"] forState:UIControlStateNormal];
        
        isCheck = YES;
    }
    else
    {
       // [self.checkBoxBtnOutlet setBackgroundImage:[UIImage imageNamed:@"checkbox.png"] forState:UIControlStateNormal];
        
        [self.checkBoxBtnOutlet setImage:[UIImage imageNamed:@"checkbox.png"] forState:UIControlStateNormal];
        
        isCheck = NO;
    }
    
   
}

- (IBAction)backToSignAction:(id)sender {
    
    
    [self.emailTxtFieldOulet resignFirstResponder];
    [self.passwordTxtFieldOutlet resignFirstResponder];
    [self.creatEmailTxtFldOutlet resignFirstResponder];
    [self.creatUserNameTxtFldOutlet resignFirstResponder];
    [self.creatPasswordTxtFldOutlet resignFirstResponder];
    [self.creatRepeatPasswordTxtFldOutlet resignFirstResponder];
    
    self.creatNewAccountBseView.hidden = YES;
    self.signInBaseView.hidden = NO;
}

- (IBAction)faceBookLoginAction:(id)sender {
    
    
    if (isCheck == YES) {
        
        _loading = [[LoadingView alloc] initWithFrame:[UIScreen mainScreen].bounds];
        [self.view addSubview:_loading];
        [_loading animate:YES];
        [self lunchAcceptController:@"FB"];
        
        
        [SharedVariables sharedInstance].fbLogInViewController = self;
        NSLog(@"%@",[SharedVariables sharedInstance].fbLogInViewController);
        
    }
    else
    {
        // [Utilities alertWithMessage:@"Accept terms and conditions before sign-in."];
       // [[Utilities sharedInstance]showCustomAlertView:self.view title:@"Warning" message:@"Accept terms and conditions before sign-in."];
        
        [[Utilities sharedInstance] showCustomAlertView:self.view title:@"Warning" message:@"Accept terms and conditions before sign-in." controller:self reason:@"creatAccount"];
    }
    
    
}

-(void)lunchAcceptController:(NSString *)mode{

    
    if([mode isEqualToString:@"FB"]){
        [[Client sharedClient] FBLogin];
    }
}
-(void)afterFbLoginSuccess
{
    [[NSUserDefaults standardUserDefaults]setBool:YES forKey:@"isFaceBook"];
    
    [SharedVariables sharedInstance].isFaceBookLogin = YES;
    
    [_loading animate:NO];
    
//    [[NSNotificationCenter defaultCenter] postNotificationName:@"UserDidLoginNotification"
//                                                        object:nil
//                                                      userInfo:@{
//                                                                 @"userId" : [Client sharedClient].user.email
//                                                                 }];
    
    TabBarViewController * tabVC = [self.storyboard instantiateViewControllerWithIdentifier:@"TabBarViewController"];
    [self.navigationController pushViewController:tabVC animated:YES];
}

-(void)afterFbLoginCancel
{

    [_loading animate:NO];
}


@end
