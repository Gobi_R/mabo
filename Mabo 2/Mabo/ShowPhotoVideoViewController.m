//
//  ShowPhotoVideoViewController.m
//  Mabo
//
//  Created by vishnuprasathg on 11/9/17.
//  Copyright © 2017 DCI. All rights reserved.
//

#import "ShowPhotoVideoViewController.h"
//#import "UIImageView+AFNetworking.h"
#import "ShowImageViewController.h"
//#import "UIImageView+WebCache.h"
#import <SDWebImage/UIImageView+WebCache.h>
#import <MediaPlayer/MediaPlayer.h>


@import AVKit;
@import AVFoundation;



@interface ShowPhotoVideoViewController ()

@property (nonatomic) AVPlayer *avPlayer;
@property (nonatomic) AVPlayerViewController* avPlayerView;


@end

@implementation ShowPhotoVideoViewController

{
    AVPlayerViewController *playerViewController;
    NSURL *url;
    AVPlayer *playerCommen;
}

- (void)viewDidLoad {
    [super viewDidLoad];
    
    playerViewController = [[AVPlayerViewController alloc] init];
    
    self.titleLblOutlet.text = self.titleString;

    
    // Do any additional setup after loading the view.
}

-(void)viewWillAppear:(BOOL)animated
{
    if ([self.titleString isEqualToString:@"Photos"]) {
        
        self.collectionViewOutlet.hidden = NO;
        self.videoBaseViewOutlet.hidden = YES;
        
        self.collectionViewOutlet.delegate = self;
        self.collectionViewOutlet.dataSource = self;
        
    }
    else if ([self.titleString isEqualToString:@"video"])
    {
        self.collectionViewOutlet.hidden = YES;
        self.videoBaseViewOutlet.hidden = NO;
        self.showImageView.hidden = YES;
        
        NSLog(@"videoArray:%@",self.imagesArray);
        NSDictionary * videoDict = self.imagesArray.firstObject;
        
        //https://res.cloudinary.com/mabo-app/video/upload/v1521438947/
        
        NSString*sourceURLString =[NSString stringWithFormat:@"https://res.cloudinary.com/mabo-app/video/upload/v1521438947/%@",[videoDict objectForKey:@"vid_path"]];
        
        // remote file from server:
        url = [[NSURL alloc] initWithString:sourceURLString];
        
        [self playVideo];
        //[self playVideoStreem];
        //[self playAgain];
        
    }
    else if ([self.titleString isEqualToString:@"Photo"]) {
        
        self.collectionViewOutlet.hidden = YES;
        self.showImageView.hidden = NO;
        self.videoBaseViewOutlet.hidden = YES;
        
        
        dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_BACKGROUND, 0), ^{
            // retrive image on global queue
            
            //NSString* profilePicPath = [NSString stringWithFormat:@"%@",[imageDict objectForKey:@"pic_path"]];
            
            //NSString * urlstring = [NSString stringWithFormat:@"http://34.230.105.103:8002/%@", profilePicPath];
            NSURL *imageURL = [NSURL URLWithString:self.selectedImageString];
            NSData *imageData = [NSData dataWithContentsOfURL:imageURL];
            UIImage *image = [UIImage imageWithData:imageData];
            dispatch_async(dispatch_get_main_queue(), ^{
                
                [self.showImageView sd_setImageWithURL:imageURL placeholderImage:[UIImage imageNamed:@"placeHolderImage.png"]];
                
            });
        });
        
    }
    else if ([self.titleString isEqualToString:@"videoMsg"])
    {
        self.collectionViewOutlet.hidden = YES;
        self.showImageView.hidden = YES;
        self.videoBaseViewOutlet.hidden = NO;
        
        NSLog(@"videoArray:%@",self.imagesArray);
        NSDictionary * videoDict = self.imagesArray.firstObject;
        
       // NSString*sourceURLString =[NSString stringWithFormat:@"http://34.230.105.103:8002/%@",[videoDict objectForKey:@"vid_path"]];
        
        NSString*sourceURLString = self.selectedVideoString;
        
        // remote file from server:
        url = [[NSURL alloc] initWithString:sourceURLString];
        
        [self playVideo];
        //[self playVideoStreem];
        
    }
}

-(void)playAgain
{
    // your filepath which is may be "http" type
   // NSString *filePath = [self.video_Array objectAtIndex:index];
    // http to NSURL using string filepath
    //NSURL *url= [[NSURL alloc] initWithString:url];
    AVPlayerViewController *playerViewController = [[AVPlayerViewController alloc] init];
    playerViewController.videoGravity = AVLayerVideoGravityResize;
    playerViewController.view.frame = self.videoBaseViewOutlet.bounds;
    playerViewController.showsPlaybackControls = YES;
    playerCommen=[AVPlayer playerWithURL:url];
    playerViewController.player = playerCommen;
    playerViewController.view.userInteractionEnabled = false;
    [playerCommen play];
    [self.videoBaseViewOutlet addSubview:playerViewController.view];
}

-(void)playVideoStreem
{
    
    NSLog(@"linked path: %@",[url absoluteString]);
    NSString* linkedPath;
    AVURLAsset* asset;
    NSFileManager *filemgr = [NSFileManager defaultManager];
    
    for (NSString* string in [AVURLAsset audiovisualMIMETypes]) {
        
        if ([string containsString:@"video/"]) {
            
            NSLog(@"Trying: %@",string);
           // linkedPath = [[url absoluteString] stringByAppendingPathExtension:[string stringByReplacingOccurrencesOfString:@"video/" withString:@""]];
            linkedPath = [url absoluteString];
            NSLog(@"linked path: %@",linkedPath);
            
            if (![filemgr fileExistsAtPath:linkedPath]) {
                NSError *error = nil;
                [filemgr createSymbolicLinkAtURL:[NSURL URLWithString:linkedPath] withDestinationURL:url error:&error];
                if (error) {
                    NSLog(@"error %@",error.localizedDescription);
                }
            }
            
            asset = [AVURLAsset assetWithURL:[NSURL URLWithString:linkedPath]];
            if ([asset isPlayable]) {
                NSLog(@"Playable");
                
                AVPlayerViewController *playerController = [[AVPlayerViewController alloc] init];
                
               // AVURLAsset *asset = [AVURLAsset assetWithURL: url];
                AVPlayerItem *item = [AVPlayerItem playerItemWithAsset: asset];
                
                AVPlayer *player = [[AVPlayer alloc] initWithPlayerItem:item];
                
                playerController.player = player;
                playerController.showsPlaybackControls = YES;
                [self addChildViewController:playerController];
                playerController.view.frame = self.videoBaseViewOutlet.frame;
                [self.videoBaseViewOutlet addSubview:playerController.view];
                [playerController.view setFrame:CGRectMake(0,0, self.videoBaseViewOutlet.frame.size.width, self.videoBaseViewOutlet.frame.size.height-70)];
                playerController.view.tag = 1000;
                
                break;
            }else{
                NSLog(@"Not Playable");
                asset = nil;
            }
        }
        
    }
    
    
    
}

- (void)playVideo {
    
   // https://res.cloudinary.com/mabo-app/video/upload/v1521438947/
    
    NSURL *url1 = [[NSURL alloc] initWithString:@"https://res.cloudinary.com/mabo-app/video/upload/v1521438947/chatmotog1%40mailinator.com_1516625091277.mp4"];
    
    //NSURL *url = [[NSURL alloc] initWithString:@"http://techslides.com/demos/sample-videos/small.mp4"];
    
    //NSURL *url2 = [[NSURL alloc] initWithString:@"file:///var/mobile/Containers/Data/Application/AF56D3C2-9FF6-4E94-BAED-E48584B5C615/Documents/MyVideo.mp4"];
    
    //file:///private/var/mobile/Containers/Data/Application/F4FB74AB-6EFE-4BE0-9B11-B839B8F020F5/tmp/53804904991__F17ECC32-1E28-4E83-95AC-B51A82B4193A.MOV
    
   // NSURL *videoURL = [[NSURL fileURLWithPath:NSTemporaryDirectory()] URLByAppendingPathComponent:@"AF56D3C2-9FF6-4E94-BAED-E48584B5C615.mp4"];
    
    AVURLAsset *asset = [AVURLAsset assetWithURL: url];
    AVPlayerItem *item = [AVPlayerItem playerItemWithAsset: asset];
    //AVPlayer * ply = [[AVPlayer alloc] initWithURL:url1];
    
    //NSURL * localURL = [NSURL URLWithString:@"file:///var/mobile/Containers/Data/Application/7CA0269F-C53D-48C0-905D-15FA1881FDA1/Documents/xyz.mp4"];
    //AVPlayer * player = [[AVPlayer alloc] initWithURL:url];
    //AVPlayer *player = [AVPlayer playerWithURL:url];

    AVPlayer * player = [[AVPlayer alloc] initWithPlayerItem: item];
    NSLog(@"URL:%@",url);
    playerViewController.player = player;
    [playerViewController.view setFrame:CGRectMake(0, 0, self.videoBaseViewOutlet.bounds.size.width, self.videoBaseViewOutlet.bounds.size.height)];

    playerViewController.showsPlaybackControls = YES;

    [self.videoBaseViewOutlet addSubview:playerViewController.view];

    [player play];
    
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (NSInteger)numberOfSectionsInCollectionView:(UICollectionView *)collectionView {
    return 1;
}

- (NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section {
    return [self.imagesArray count];
}

- (UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath
{
    UICollectionViewCell *cell = [collectionView dequeueReusableCellWithReuseIdentifier:@"Cell" forIndexPath:indexPath];
    NSDictionary *imageDict = [self.imagesArray objectAtIndex:[indexPath row]];
    
    
    dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_BACKGROUND, 0), ^{
        // retrive image on global queue
        
        NSString* profilePicPath = [NSString stringWithFormat:@"%@",[imageDict objectForKey:@"pic_path"]];
        
        NSString * urlstring = [NSString stringWithFormat:@"http://34.230.105.103:8002/%@", profilePicPath];
        NSURL *imageURL = [NSURL URLWithString:urlstring];
        NSData *imageData = [NSData dataWithContentsOfURL:imageURL];
        UIImage *image = [UIImage imageWithData:imageData];
        
        UIImageView *imageView = [cell viewWithTag:1];
        imageView.layer.cornerRadius = 5;
        imageView.layer.masksToBounds = YES;
        
        dispatch_async(dispatch_get_main_queue(), ^{
            
            [imageView sd_setImageWithURL:imageURL placeholderImage:[UIImage imageNamed:@"placeHolderImage.png"]];
            
        });
    });
    
    return cell;
}

- (void)collectionView:(UICollectionView *)collectionView didSelectItemAtIndexPath:(NSIndexPath *)indexPath
{
    ShowImageViewController * showImageVC = [self.storyboard instantiateViewControllerWithIdentifier:@"ShowImageViewController"];
    showImageVC.imageDict = [self.imagesArray objectAtIndex:[indexPath row]];
    [self.navigationController pushViewController:showImageVC animated:YES];
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

- (IBAction)backBtnAction:(id)sender {
    
    [self.navigationController popViewControllerAnimated:NO];
}
@end
