

//
//  Utilities.m
//  Horizontal Messenger
//
//  Created by vishnuprasathg on 9/1/15.
//  Copyright (c) 2015 DCI. All rights reserved.
//

#import "Utilities.h"
#import "AppDelegate.h"
//#import "MBProgressHUD.h"
#import <AVFoundation/AVFoundation.h>
#import <sys/utsname.h>
#import "SignInViewController.h"
#import "MyPostViewController.h"
#import "PostsViewController.h"
#import "FevouritesViewController.h"

#define k_LoggedInUserData @"k_LoggedInUserData"
#define screenSize [[UIScreen mainScreen] bounds]

static CGRect keyBoardFrame;

@interface Utilities()
@property (nonatomic,strong)AVAudioPlayer *k_ReceiveMessage_Tone_Player;
@property (nonatomic,strong)AVAudioPlayer *k_ResizingNotification_Tone_Player;
@property (nonatomic,strong)AVAudioPlayer *k_DeleteNotification_Tone_Player;
@property (nonatomic,strong)AVAudioPlayer *k_SendingNotification_Tone_Player;
@property (nonatomic,strong)AVAudioPlayer *k_CameraFlash_Tone_Player;
@property (nonatomic,strong)AVAudioPlayer *k_SelectMediaTone_Tone_Player;
@property (nonatomic,strong)AVAudioPlayer *k_VideoCompressing_Tone_Player;
@property (nonatomic,strong)AVAudioPlayer *k_Train_Tone_Player;
@property (nonatomic,strong)AVAudioPlayer *k_WarningNotification_Tone_Player;
@property (nonatomic,strong)AVAudioPlayer *k_ContactAdd_Tone_Player;
@property (nonatomic,strong)AVAudioPlayer *k_EmojiMessage_Tone_Player;
@property (nonatomic,strong)AVAudioPlayer *k_ReadNotification_Tone_Player;
@property (nonatomic,strong)AVAudioPlayer *k_WarningAnimTone_Tone_Player;
@property (nonatomic,strong)AVAudioPlayer *k_DelectReceiverNotification_Tone_Player;
@property (nonatomic,retain)NSArray *audioPlayers, *audioTones;

@property (nonatomic,strong)UIView *backView;

@end
@implementation Utilities
{
    int direction;
    int shakes;
}
#pragma mark
#pragma mark Shared Instance

+(id)sharedInstance
{
    static Utilities *utilities = nil;
    static dispatch_once_t dispatchOnce;
    
    dispatch_once(&dispatchOnce, ^{
        NSError *error;
        utilities = [[Utilities alloc]init];

    });
    return utilities;
}

+(void)alertWithMessage:(NSString*)message{
    [[[UIAlertView alloc]initWithTitle:@"MABO" message:message delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil]show];
}


//+(void)showActivityIndicatiorForView:(UIView*)baseView withTitle:(NSString*)title
//{
//    [MBProgressHUD showHUDAddedTo:baseView animated:YES];
//}
//
//+(void)hideActivityIndicatorForView:(UIView*)baseView
//{
//    [MBProgressHUD hideHUDForView:baseView animated:YES];
//}

+(void)clearSubViewsForView:(UIView*)baseView
{
    for (UIView *view in baseView.subviews) {
        [view removeFromSuperview];
        if ([view isKindOfClass:[UILabel class]] || [view isKindOfClass:[UIButton class]] || [view isKindOfClass:[UIImageView class]] || [view isKindOfClass:[UITableView class]] || [view isKindOfClass:[UIPickerView class]] || [view isKindOfClass:[UITextField class]] ||[view isKindOfClass:[UITextView class]]) {
            [view removeFromSuperview];
        }
    }
}

+(AppDelegate*)appDelegate
{
    AppDelegate *appdelegate = (AppDelegate*)[[UIApplication sharedApplication]delegate];
    return appdelegate;
}

+(void)addNotificationCenterForSelecetor:(SEL)aSelector forKey:(NSString*)key withClass:(id)className
{
    [[NSNotificationCenter defaultCenter]addObserver:className selector:aSelector name:key object:nil];
}

+(void)postNotificationforKey:(NSString*)key
{
    [[NSNotificationCenter defaultCenter]postNotificationName:key object:nil];
}

+(void)setUserDefaultValue:(id)value forKey:(NSString*)key
{
    [[NSUserDefaults standardUserDefaults]setObject:value forKey:key];
    [[NSUserDefaults standardUserDefaults]synchronize];
}

+(id)getUserDefaultValueForKey:(NSString*)key
{
    id userDefault_obj = [[NSUserDefaults standardUserDefaults]objectForKey:key];
    userDefault_obj = (!userDefault_obj || [userDefault_obj isKindOfClass:[NSNull class]]||[userDefault_obj isEqual:[NSNull null]])?[NSObject class]:userDefault_obj;
    return userDefault_obj;
}

//+ (BOOL)isValidMail:(NSString*)email
//{
//    NSRange searchRange = NSMakeRange(0, email.length);
//    NSRange foundRange = [email rangeOfString:@"^[A-Z0-9._%+-]+@[A-Z0-9.-]+.[A-Z]{2,6}$"
//                                      options:(NSCaseInsensitiveSearch|NSRegularExpressionSearch) range:searchRange];
//    if (foundRange.length)
//        return NO;
//    return YES;
//}

+(BOOL) isValidMail:(NSString *)checkString
{
//    BOOL stricterFilter = NO; // Discussion http://blog.logichigh.com/2010/09/02/validating-an-e-mail-address/
//    NSString *stricterFilterString = @"^[A-Z0-9a-z\\._%+-]+@([A-Za-z0-9-]+\\.)+[A-Za-z]{2,4}$";
//    NSString *laxString = @"^.+@([A-Za-z0-9-]+\\.)+[A-Za-z]{2}[A-Za-z]*$";
//    NSString *emailRegex = stricterFilter ? stricterFilterString : laxString;
//    NSPredicate *emailTest = [NSPredicate predicateWithFormat:@"SELF MATCHES %@", emailRegex];
//
//    BOOL isvalid = [emailTest evaluateWithObject:checkString];
//    return [emailTest evaluateWithObject:checkString];
    
    NSString *emailRegex = @"[A-Z0-9a-z._%+-]+@[A-Za-z0-9.-]+\\.[A-Za-z]{2,4}";
    NSPredicate *emailTest = [NSPredicate predicateWithFormat:@"SELF MATCHES %@", emailRegex];
    return [emailTest evaluateWithObject:checkString];
    
}


+(NSString*)getDeviceModelName
{
    struct utsname systemInfo;
    uname(&systemInfo);
    
    return [NSString stringWithCString:systemInfo.machine encoding:NSUTF8StringEncoding];
}
-(NSMutableAttributedString*)loadTimeLocalDifferent:(NSDate *)msg_creat_Date
{
    
    NSDateFormatter *formatter = [[NSDateFormatter alloc] init];
    //[formatter setTimeZone:[NSTimeZone timeZoneWithAbbreviation:@"UTC"]];
    [formatter setDateFormat:@"yyyy-MM-dd HH:mm:ss"];
    
    NSDate *startDate = msg_creat_Date;
    NSDate *endDate = [NSDate date];
    NSLog(@"msgCreatedTime = %@l",[formatter stringFromDate:startDate]);
    NSLog(@"currentTime = %@l",[formatter stringFromDate:endDate]);
    
    if ([startDate compare: endDate] == NSOrderedDescending) {
        NSLog(@"startDate is later than endDate");
        
    } else if ([startDate compare:endDate] == NSOrderedAscending) {
        NSLog(@"startDate is earlier than endDate");
        
    } else {
        NSLog(@"dates are the same");
        
    }
    NSTimeInterval timeDifference = [endDate timeIntervalSinceDate:startDate];
    
    double minutes = timeDifference / 60;
    double hours = minutes / 60;
    double seconds = timeDifference;
    double days = minutes / 1440;
    double weeks = days / 7;
    double months = weeks / 4;
    double years = months / 12;
    
    NSLog(@" years = %i, months = %i, weeks = %i, days = %i,hours = %i, minutes = %i,seconds = %i", (int)years, (int)months, (int)weeks, (int)days, (int)hours, (int)minutes, (int)seconds);
    
    NSMutableAttributedString * attributedText;
    if (years >= 1)
    {
        
        NSString * str = [NSString stringWithFormat:@"%f",years];
        NSArray * arr = [str componentsSeparatedByString:@"."];
        
        if ([arr.firstObject isEqualToString:@"1"]) {
            
            attributedText = [[NSMutableAttributedString alloc] initWithString:[NSString stringWithFormat:@"%@ year ago",arr.firstObject]];
        }
        else
        {
            attributedText = [[NSMutableAttributedString alloc] initWithString:[NSString stringWithFormat:@"%@ years ago",arr.firstObject]];
        }
        
        
        
    }
    else if (months >= 1)
    {
        NSString * str = [NSString stringWithFormat:@"%f",months];
        NSArray * arr = [str componentsSeparatedByString:@"."];
        
        if ([arr.firstObject isEqualToString:@"1"]) {
            
            attributedText = [[NSMutableAttributedString alloc] initWithString:[NSString stringWithFormat:@"%@ month ago",arr.firstObject]];
        }
        else
        {
            attributedText = [[NSMutableAttributedString alloc] initWithString:[NSString stringWithFormat:@"%@ months ago",arr.firstObject]];
        }
        
        
        
    }
    else if (weeks >= 1)
    {
        NSString * str = [NSString stringWithFormat:@"%f",weeks];
        NSArray * arr = [str componentsSeparatedByString:@"."];
        
        if ([arr.firstObject isEqualToString:@"1"]) {
            
            attributedText = [[NSMutableAttributedString alloc] initWithString:[NSString stringWithFormat:@"%@ week ago",arr.firstObject]];
        }
        else
        {
            attributedText = [[NSMutableAttributedString alloc] initWithString:[NSString stringWithFormat:@"%@ weeks ago",arr.firstObject]];
        }
        
        
        
    }
    else if (days >= 1)
    {
        NSString * str = [NSString stringWithFormat:@"%f",days];
        NSArray * arr = [str componentsSeparatedByString:@"."];
        if ([arr.firstObject isEqualToString:@"1"]) {
            
            attributedText = [[NSMutableAttributedString alloc] initWithString:[NSString stringWithFormat:@"%@ day ago",arr.firstObject]];
        }
        else
        {
            attributedText = [[NSMutableAttributedString alloc] initWithString:[NSString stringWithFormat:@"%@ days ago",arr.firstObject]];
        }
        
        
        
    }
    else if (hours >= 1)
    {
        NSString * str = [NSString stringWithFormat:@"%f",hours];
        NSArray * arr = [str componentsSeparatedByString:@"."];
        
        if ([arr.firstObject isEqualToString:@"1"]) {
            
            attributedText = [[NSMutableAttributedString alloc] initWithString:[NSString stringWithFormat:@"%@ hour ago",arr.firstObject]];
        }
        else
        {
            attributedText = [[NSMutableAttributedString alloc] initWithString:[NSString stringWithFormat:@"%@ hours ago",arr.firstObject]];
        }
        
        
        
    }
    else if (minutes >= 1)
    {
        NSString * str = [NSString stringWithFormat:@"%f",minutes];
        NSArray * arr = [str componentsSeparatedByString:@"."];
        
        if ([arr.firstObject isEqualToString:@"1"]) {
            
            attributedText = [[NSMutableAttributedString alloc] initWithString:[NSString stringWithFormat:@"%@ min ago",arr.firstObject]];
        }
        else
        {
            attributedText = [[NSMutableAttributedString alloc] initWithString:[NSString stringWithFormat:@"%@ mins ago",arr.firstObject]];
        }
        
        
        
        
    }
    else if (seconds >= 1)
    {
        NSString * str = [NSString stringWithFormat:@"%f",seconds];
        NSArray * arr = [str componentsSeparatedByString:@"."];
        
        if ([arr.firstObject isEqualToString:@"1"]) {
            
            attributedText = [[NSMutableAttributedString alloc] initWithString:[NSString stringWithFormat:@"%@ sec ago",arr.firstObject]];
        }
        else
        {
            attributedText = [[NSMutableAttributedString alloc] initWithString:[NSString stringWithFormat:@"%@ secs ago",arr.firstObject]];
        }
        
        
        
    }
    
    return attributedText;
}

-(NSMutableAttributedString*)loadTimeDifferent:(NSDate *)msg_creat_Date
{
    
    NSDateFormatter *formatter = [[NSDateFormatter alloc] init];
    [formatter setTimeZone:[NSTimeZone timeZoneWithAbbreviation:@"UTC"]];
    [formatter setDateFormat:@"yyyy-MM-dd HH:mm:ss"];
    
    NSDate *startDate = msg_creat_Date;
    NSDate *endDate = [NSDate date];
    NSLog(@"msgCreatedTime = %@l",[formatter stringFromDate:startDate]);
    NSLog(@"currentTime = %@l",[formatter stringFromDate:endDate]);
    
    if ([startDate compare: endDate] == NSOrderedDescending) {
        NSLog(@"startDate is later than endDate");
        
    } else if ([startDate compare:endDate] == NSOrderedAscending) {
        NSLog(@"startDate is earlier than endDate");
        
    } else {
        NSLog(@"dates are the same");
        
    }
    NSTimeInterval timeDifference = [endDate timeIntervalSinceDate:startDate];
    
    double minutes = timeDifference / 60;
    double hours = minutes / 60;
    double seconds = timeDifference;
    double days = minutes / 1440;
    double weeks = days / 7;
    double months = weeks / 4;
    double years = months / 12;
    
    NSLog(@" years = %i, months = %i, weeks = %i, days = %i,hours = %i, minutes = %i,seconds = %i", (int)years, (int)months, (int)weeks, (int)days, (int)hours, (int)minutes, (int)seconds);
    
    NSMutableAttributedString * attributedText;
    if (years >= 1)
    {
        
        NSString * str = [NSString stringWithFormat:@"%f",years];
        NSArray * arr = [str componentsSeparatedByString:@"."];
        
        if ([arr.firstObject isEqualToString:@"1"]) {
            
            attributedText = [[NSMutableAttributedString alloc] initWithString:[NSString stringWithFormat:@"%@ year ago",arr.firstObject]];
        }
        else
        {
            attributedText = [[NSMutableAttributedString alloc] initWithString:[NSString stringWithFormat:@"%@ years ago",arr.firstObject]];
        }
        
        
        
    }
    else if (months >= 1)
    {
        NSString * str = [NSString stringWithFormat:@"%f",months];
        NSArray * arr = [str componentsSeparatedByString:@"."];
        
        if ([arr.firstObject isEqualToString:@"1"]) {
            
            attributedText = [[NSMutableAttributedString alloc] initWithString:[NSString stringWithFormat:@"%@ month ago",arr.firstObject]];
        }
        else
        {
            attributedText = [[NSMutableAttributedString alloc] initWithString:[NSString stringWithFormat:@"%@ months ago",arr.firstObject]];
        }

        
        
    }
    else if (weeks >= 1)
    {
        NSString * str = [NSString stringWithFormat:@"%f",weeks];
        NSArray * arr = [str componentsSeparatedByString:@"."];
        
        if ([arr.firstObject isEqualToString:@"1"]) {
            
            attributedText = [[NSMutableAttributedString alloc] initWithString:[NSString stringWithFormat:@"%@ week ago",arr.firstObject]];
        }
        else
        {
            attributedText = [[NSMutableAttributedString alloc] initWithString:[NSString stringWithFormat:@"%@ weeks ago",arr.firstObject]];
        }

        
        
    }
    else if (days >= 1)
    {
        NSString * str = [NSString stringWithFormat:@"%f",days];
        NSArray * arr = [str componentsSeparatedByString:@"."];
        if ([arr.firstObject isEqualToString:@"1"]) {
            
            attributedText = [[NSMutableAttributedString alloc] initWithString:[NSString stringWithFormat:@"%@ day ago",arr.firstObject]];
        }
        else
        {
            attributedText = [[NSMutableAttributedString alloc] initWithString:[NSString stringWithFormat:@"%@ days ago",arr.firstObject]];
        }

        
        
    }
    else if (hours >= 1)
    {
        NSString * str = [NSString stringWithFormat:@"%f",hours];
        NSArray * arr = [str componentsSeparatedByString:@"."];
        
        if ([arr.firstObject isEqualToString:@"1"]) {
            
           attributedText = [[NSMutableAttributedString alloc] initWithString:[NSString stringWithFormat:@"%@ hour ago",arr.firstObject]];
        }
        else
        {
          attributedText = [[NSMutableAttributedString alloc] initWithString:[NSString stringWithFormat:@"%@ hours ago",arr.firstObject]];
        }

        
        
    }
    else if (minutes >= 1)
    {
        NSString * str = [NSString stringWithFormat:@"%f",minutes];
        NSArray * arr = [str componentsSeparatedByString:@"."];
        
        if ([arr.firstObject isEqualToString:@"1"]) {
            
            attributedText = [[NSMutableAttributedString alloc] initWithString:[NSString stringWithFormat:@"%@ min ago",arr.firstObject]];
        }
        else
        {
           attributedText = [[NSMutableAttributedString alloc] initWithString:[NSString stringWithFormat:@"%@ mins ago",arr.firstObject]];
        }

        
        
        
    }
    else if (seconds >= 1)
    {
        NSString * str = [NSString stringWithFormat:@"%f",seconds];
        NSArray * arr = [str componentsSeparatedByString:@"."];
        
        if ([arr.firstObject isEqualToString:@"1"]) {
            
            attributedText = [[NSMutableAttributedString alloc] initWithString:[NSString stringWithFormat:@"%@ sec ago",arr.firstObject]];
        }
        else
        {
           attributedText = [[NSMutableAttributedString alloc] initWithString:[NSString stringWithFormat:@"%@ secs ago",arr.firstObject]];
        }

        
        
    }
    
    return attributedText;
}

+(void)setKeyboardFrame:(CGRect)frame
{
    keyBoardFrame = frame;
}
+(CGRect)getKeyboardFrame
{
   
    if (keyBoardFrame.size.height > 0)
    {
        return keyBoardFrame;
    }
    else if (screenSize.size.height == 480)
    {
        keyBoardFrame = CGRectMake(CGRectGetMinX(keyBoardFrame), CGRectGetMinY(keyBoardFrame), CGRectGetWidth(screenSize), 253);
    }
    else if (screenSize.size.height == 568)
    {
        keyBoardFrame = CGRectMake(CGRectGetMinX(keyBoardFrame), CGRectGetMinY(keyBoardFrame), CGRectGetWidth(screenSize), 253);
        
    }
    else if (screenSize.size.height == 667)
    {
        keyBoardFrame = CGRectMake(CGRectGetMinX(keyBoardFrame), CGRectGetMinY(keyBoardFrame), CGRectGetWidth(screenSize), 258);
        
    }
    else if (screenSize.size.height == 736)
    {
        keyBoardFrame = CGRectMake(CGRectGetMinX(keyBoardFrame), CGRectGetMinY(keyBoardFrame), CGRectGetWidth(screenSize), 271);
        
    }

    return keyBoardFrame;
}

-(void)shake:(UIView *)View1 view2:(UIView *)View2 shakeCount:(int)count leftshake:(int)leftshake rightshake:(int)rightshake
{
    
    CGAffineTransform leftShake = CGAffineTransformMakeTranslation(leftshake, 0);
    CGAffineTransform rightShake = CGAffineTransformMakeTranslation(rightshake, 0);
    
    View1.transform = leftShake;
    View2.transform = leftShake;// starting point
    
    [UIView beginAnimations:@"shake_button" context:(__bridge void * _Nullable)(View1)];
    [UIView beginAnimations:@"shake_button" context:(__bridge void * _Nullable)(View2)];
    [UIView setAnimationRepeatAutoreverses:YES]; // important
    [UIView setAnimationRepeatCount:count];
    [UIView setAnimationDuration:0.06];
    [UIView setAnimationDelegate:self];
    //[UIView setAnimationDidStopSelector:@selector(shakeEnded:finished:context:)];
    View1.transform = rightShake;
    View2.transform = rightShake;// end here & auto-reverse
    [UIView commitAnimations];
    [UIView commitAnimations];
}

-(void)showMediaAlertView:(UIViewController *)controller
{
    self.chatMediaView = [[[NSBundle mainBundle]loadNibNamed:@"ChatMediaView" owner:self options:nil]firstObject];
    
    UIView* backGroundView =  [[UIView alloc] initWithFrame:controller.view.frame];
    
    backGroundView.backgroundColor = [UIColor colorWithRed:0 green:0 blue:0 alpha:0.5];
    
    UITapGestureRecognizer *tap = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(closeTap)];
    
    self.backView = backGroundView;
        [backGroundView addGestureRecognizer:tap];
    
//        self.customAlertView.titleLblOutlet.text = title;
//        self.customAlertView.messageLblOutlet.text = message;
    
    self.chatMediaView.alertParentViewController = controller;
    
    CGRect frame = self.chatMediaView.frame;
    frame.origin.x = screenSize.size.width/2 - self.chatMediaView.frame.size.width/2;
    frame.origin.y = screenSize.size.height/2 - self.chatMediaView.frame.size.height/2 -20;
    frame.size.width = self.chatMediaView.frame.size.width;
    frame.size.height = self.chatMediaView.frame.size.height;
    self.chatMediaView.frame = frame;
    
    [controller.view addSubview:backGroundView];
    [controller.view bringSubviewToFront:backGroundView];
    
    [backGroundView addSubview:self.chatMediaView];
    [backGroundView bringSubviewToFront:self.chatMediaView];
}

-(void)closeTap
{
    [self hideCustomAlertView:self.backView];
}

-(void)showCustomVideoAlertView:(UIViewController *)controller
{
    self.customVideoAlertView = [[[NSBundle mainBundle]loadNibNamed:@"CustomVideoAlertView" owner:self options:nil]firstObject];
    
    UIView* backGroundView =  [[UIView alloc] initWithFrame:controller.view.frame];
    
    backGroundView.backgroundColor = [UIColor colorWithRed:0 green:0 blue:0 alpha:0.5];
    
    //    UITapGestureRecognizer *tap = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector([self rem])];
    //
    //    [backGroundView addGestureRecognizer:tap];
    
    //    self.customAlertView.titleLblOutlet.text = title;
    //    self.customAlertView.messageLblOutlet.text = message;
    
    self.customVideoAlertView.alertParentViewController = controller;
    
    CGRect frame = self.customCameraAlertView.frame;
    frame.origin.x = screenSize.size.width/2 - self.customVideoAlertView.frame.size.width/2;
    frame.origin.y = screenSize.size.height/2 - self.customVideoAlertView.frame.size.height/2;
    frame.size.width = self.customVideoAlertView.frame.size.width;
    frame.size.height = self.customVideoAlertView.frame.size.height;
    self.customVideoAlertView.frame = frame;
    
    [controller.view addSubview:backGroundView];
    [controller.view bringSubviewToFront:backGroundView];
    
    [backGroundView addSubview:self.customVideoAlertView];
    [backGroundView bringSubviewToFront:self.customVideoAlertView];
}

-(void)showCustomCameraAlertView:(UIViewController *)controller
{
    self.customCameraAlertView = [[[NSBundle mainBundle]loadNibNamed:@"CustomCameraAlertView" owner:self options:nil]firstObject];
    
    UIView* backGroundView =  [[UIView alloc] initWithFrame:controller.view.frame];
    
    backGroundView.backgroundColor = [UIColor colorWithRed:0 green:0 blue:0 alpha:0.5];
    
    //    UITapGestureRecognizer *tap = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector([self rem])];
    //
    //    [backGroundView addGestureRecognizer:tap];
    
//    self.customAlertView.titleLblOutlet.text = title;
//    self.customAlertView.messageLblOutlet.text = message;
    
    self.customCameraAlertView.alertParentViewController = controller;
    
    CGRect frame = self.customCameraAlertView.frame;
    frame.origin.x = screenSize.size.width/2 - self.customCameraAlertView.frame.size.width/2;
    frame.origin.y = screenSize.size.height/2 - self.customCameraAlertView.frame.size.height/2;
    frame.size.width = self.customCameraAlertView.frame.size.width;
    frame.size.height = self.customCameraAlertView.frame.size.height;
    self.customCameraAlertView.frame = frame;
    
    [controller.view addSubview:backGroundView];
    [controller.view bringSubviewToFront:backGroundView];
    
    [backGroundView addSubview:self.customCameraAlertView];
    [backGroundView bringSubviewToFront:self.customCameraAlertView];
    
}

-(void)showCustomAddCommentView:(UIViewController *)controller
{
    self.addCommentsView = [[[NSBundle mainBundle]loadNibNamed:@"AddCommentsView" owner:self options:nil]firstObject];
    
    UIView* backGroundView =  [[UIView alloc] initWithFrame:controller.view.frame];
    
    backGroundView.backgroundColor = [UIColor colorWithRed:0 green:0 blue:0 alpha:0.5];
    
    //    UITapGestureRecognizer *tap = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector([self rem])];
    //
    //    [backGroundView addGestureRecognizer:tap];
    
    //    self.customAlertView.titleLblOutlet.text = title;
    //    self.customAlertView.messageLblOutlet.text = message;
    
    if ([controller isKindOfClass:MyPostViewController.class]) {
        
        
        MyPostViewController * myPostVC = controller;
        self.addCommentsView.commentTxtFieldOutlet.delegate = myPostVC;
        
    }
    else if ([controller isKindOfClass:PostsViewController.class]) {
        
        
        PostsViewController * PostVC = controller;
        self.addCommentsView.commentTxtFieldOutlet.delegate = PostVC;
        
    }
    else if ([controller isKindOfClass:FevouritesViewController.class]) {
        
        
        FevouritesViewController * fevVC = controller;
        self.addCommentsView.commentTxtFieldOutlet.delegate = fevVC;
        
    }
    
    self.addCommentsView.alertParentViewController = controller;
    
    CGRect frame = self.addCommentsView.frame;
    frame.origin.x = screenSize.size.width/2 - self.addCommentsView.frame.size.width/2;
    frame.origin.y = screenSize.size.height/2 - self.addCommentsView.frame.size.height/2 -100;
    frame.size.width = self.addCommentsView.frame.size.width;
    frame.size.height = self.addCommentsView.frame.size.height;
    self.addCommentsView.frame = frame;
    
    [controller.view addSubview:backGroundView];
    [controller.view bringSubviewToFront:backGroundView];
    
    [backGroundView addSubview:self.addCommentsView];
    [backGroundView bringSubviewToFront:self.addCommentsView];
    
}


-(void)showCustomAlertView:(UIView *)View1 title:(NSString *)title message:(NSString *)message controller:(UIViewController *)controller reason:(NSString *)reason
{
     self.customAlertView = [[[NSBundle mainBundle]loadNibNamed:@"CustomAlertView" owner:self options:nil]firstObject];
    
    UIView* backGroundView =  [[UIView alloc] initWithFrame:View1.frame];
    
    backGroundView.backgroundColor = [UIColor colorWithRed:0 green:0 blue:0 alpha:0.5];
    
//    UITapGestureRecognizer *tap = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector([self rem])];
//    
//    [backGroundView addGestureRecognizer:tap];
    
    self.customAlertView.titleLblOutlet.text = title;
    self.customAlertView.messageLblOutlet.text = message;
    self.customAlertView.yesActionResponse = reason;
    self.customAlertView.alertParentViewController = controller;
    
    CGRect frame = self.customAlertView.frame;
    frame.origin.x = screenSize.size.width/2 - self.customAlertView.frame.size.width/2;
    frame.origin.y = screenSize.size.height/2 - self.customAlertView.frame.size.height/2;
    frame.size.width = self.customAlertView.frame.size.width;
    frame.size.height = self.customAlertView.frame.size.height;
    self.customAlertView.frame = frame;
    
    [View1 addSubview:backGroundView];
    [View1 bringSubviewToFront:backGroundView];
    
    [backGroundView addSubview:self.customAlertView];
    [backGroundView bringSubviewToFront:self.customAlertView];
    
}

-(void)showCustomAlertViewWithMultiple:(UIViewController *)controller title:(NSString *)title message:(NSString *)message actionResponse:(NSString *)actionResponse
{
    self.multipleAlertView = [[[NSBundle mainBundle]loadNibNamed:@"CustomAlertView" owner:self options:nil]lastObject];
    
    UIView* backGroundView =  [[UIView alloc] initWithFrame:controller.view.frame];
    
    backGroundView.backgroundColor = [UIColor colorWithRed:0 green:0 blue:0 alpha:0.5];
    
    //    UITapGestureRecognizer *tap = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector([self rem])];
    //
    //    [backGroundView addGestureRecognizer:tap];
    
//    self.customAlertView.alertTitleLblOutlet.text = title;
//    self.customAlertView.alertmessageLblOut.text = message;
    
    [self.multipleAlertView alertWithMultiple:title message:message];
    
    CGRect frame = self.multipleAlertView.frame;
    frame.origin.x = screenSize.size.width/2 - self.multipleAlertView.frame.size.width/2;
    frame.origin.y = screenSize.size.height/2 - self.multipleAlertView.frame.size.height/2;
    frame.size.width = self.multipleAlertView.frame.size.width;
    frame.size.height = self.multipleAlertView.frame.size.height;
    self.multipleAlertView.frame = frame;
    
    self.multipleAlertView.yesActionResponse = actionResponse;
    self.multipleAlertView.alertParentViewController = controller;
    
    [controller.view addSubview:backGroundView];
    [controller.view bringSubviewToFront:backGroundView];
    
    [backGroundView addSubview:self.multipleAlertView];
    [backGroundView bringSubviewToFront:self.multipleAlertView];
    
}

-(void)showCustomForgotPwdView:(UIViewController *)controller title:(NSString *)title message:(NSString *)message actionResponse:(NSString *)actionResponse
{
    self.forgotPopUpView = [[[NSBundle mainBundle]loadNibNamed:@"ForgotPopUpView" owner:self options:nil]firstObject];
    
    UIView* backGroundView =  [[UIView alloc] initWithFrame:controller.view.frame];
    
    backGroundView.backgroundColor = [UIColor colorWithRed:0 green:0 blue:0 alpha:0.5];
    
    //    UITapGestureRecognizer *tap = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector([self rem])];
    //
    //    [backGroundView addGestureRecognizer:tap];
    
    //    self.customAlertView.alertTitleLblOutlet.text = title;
    //    self.customAlertView.alertmessageLblOut.text = message;
    
    //[self.forgotPopUpView alertWithMultiple:title message:message];
    
    if ([controller isKindOfClass:SignInViewController.class]) {
        
            
            SignInViewController * signInVC = controller;
            self.forgotPopUpView.emailtextFieldOutlet.delegate = signInVC;
        
        }
    
    CGRect frame = self.forgotPopUpView.frame;
    frame.origin.x = screenSize.size.width/2 - self.forgotPopUpView.frame.size.width/2;
    frame.origin.y = screenSize.size.height/2 - self.forgotPopUpView.frame.size.height/2-100;
    frame.size.width = self.forgotPopUpView.frame.size.width;
    frame.size.height = self.forgotPopUpView.frame.size.height;
    self.forgotPopUpView.frame = frame;
    
    self.forgotPopUpView.yesActionResponse = actionResponse;
    self.forgotPopUpView.alertParentViewController = controller;
    
    [controller.view addSubview:backGroundView];
    [controller.view bringSubviewToFront:backGroundView];
    
    [backGroundView addSubview:self.forgotPopUpView];
    [backGroundView bringSubviewToFront:self.forgotPopUpView];
    
}


-(void)showCustomPopUpForProfileScreen:(UIViewController *)controller title:(NSString *)title message:(NSString *)message actionResponse:(NSString *)actionResponse
{
    self.profilePopView = [[[NSBundle mainBundle]loadNibNamed:@"CustomCameraAlertView" owner:self options:nil]lastObject];
    
    UIView* backGroundView =  [[UIView alloc] initWithFrame:controller.view.frame];
    
    backGroundView.backgroundColor = [UIColor colorWithRed:0 green:0 blue:0 alpha:0.5];
    
    //    UITapGestureRecognizer *tap = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector([self rem])];
    //
    //    [backGroundView addGestureRecognizer:tap];
    
//        self.customCameraAlertView.titleLblOutlet.text = title;
//        self.customCameraAlertView.messageLblOutlet.text = message;
    
    [self.profilePopView alertWithMultiple:title message:message];
    
    CGRect frame = self.profilePopView.frame;
    frame.origin.x = screenSize.size.width/2 - self.profilePopView.frame.size.width/2;
    frame.origin.y = screenSize.size.height/2 - self.profilePopView.frame.size.height/2;
    frame.size.width = self.profilePopView.frame.size.width;
    frame.size.height = self.profilePopView.frame.size.height;
    self.profilePopView.frame = frame;
    
    //self.customCameraAlertView.yesActionResponse = actionResponse;
    self.profilePopView.alertParentViewController = controller;
    
    [controller.view addSubview:backGroundView];
    [controller.view bringSubviewToFront:backGroundView];
    
    [backGroundView addSubview:self.profilePopView];
    [backGroundView bringSubviewToFront:self.profilePopView];
    
}

-(void)showFbloginEmailAlertView:(UIView *)View1 title:(NSString *)title message:(NSString *)message controller:(UIViewController *)controller
{
    self.fbEmailPopUpView = [[[NSBundle mainBundle]loadNibNamed:@"FbEmailPopUpView" owner:self options:nil]firstObject];
    
    UIView* backGroundView =  [[UIView alloc] initWithFrame:View1.frame];
    
    backGroundView.backgroundColor = [UIColor colorWithRed:0 green:0 blue:0 alpha:0.5];
    
    //    UITapGestureRecognizer *tap = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector([self rem])];
    //
    //    [backGroundView addGestureRecognizer:tap];
    
//    self.customAlertView.titleLblOutlet.text = title;
//    self.customAlertView.messageLblOutlet.text = message;
    
    if ([controller isKindOfClass:SignInViewController.class]) {
        
        
        SignInViewController * signInVC = controller;
        self.fbEmailPopUpView.emailtxtfieldOutlet.delegate = signInVC;
        
    }
    
    
    
    CGRect frame = self.fbEmailPopUpView.frame;
    frame.origin.x = screenSize.size.width/2 - self.fbEmailPopUpView.frame.size.width/2;
    frame.origin.y = screenSize.size.height/2 - self.fbEmailPopUpView.frame.size.height/2 -100;
    frame.size.width = self.fbEmailPopUpView.frame.size.width;
    frame.size.height = self.fbEmailPopUpView.frame.size.height;
    self.fbEmailPopUpView.frame = frame;
    
    [View1 addSubview:backGroundView];
    [View1 bringSubviewToFront:backGroundView];
    
    [backGroundView addSubview:self.fbEmailPopUpView];
    [backGroundView bringSubviewToFront:self.fbEmailPopUpView];
    
}

-(void)removeAlert:(UIView *)View1
{
   
}


-(void)hideCustomAlertView:(UIView *)View1
{
    [View1 removeFromSuperview];
    
}


//- (void) shakeEnded:(NSString *)animationID finished:(NSNumber *)finished context:(void *)context
//{
//    if ([finished boolValue]) {
//        UIView* item = (UIView *)CFBridgingRelease(context);
//        item.transform = CGAffineTransformIdentity;
//    }
//}

@end
