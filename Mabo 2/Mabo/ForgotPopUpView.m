//
//  ForgotPopUpView.m
//  Mabo
//
//  Created by vishnuprasathg on 11/7/17.
//  Copyright © 2017 DCI. All rights reserved.
//

#import "ForgotPopUpView.h"
#import "SignInViewController.h"
#import "Utilities.h"
#import "Client.h"

#import "LoadingView.h"


@implementation ForgotPopUpView
{
 LoadingView *loading;
}

/*
// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect {
    // Drawing code
}
*/

- (IBAction)okBtnAction:(id)sender {
    
    [[Utilities sharedInstance] hideCustomAlertView:self.superview];
    
    if ([_alertParentViewController isKindOfClass:SignInViewController.class]) {
        
        [[Utilities sharedInstance] hideCustomAlertView:self.superview];
        
        SignInViewController * signInVC = _alertParentViewController;
    
    BOOL isvalid = [Utilities isValidMail:self.emailtextFieldOutlet.text];
    
    
    if (isvalid == YES) {
        
        NSString* randomPwd = [self randomStringWithLength:8];
        
        NSLog(@"RandomPwd:%@",randomPwd);
        
        NSMutableDictionary * plainPwddict = [[NSMutableDictionary alloc] init];
        
        [plainPwddict setObject:randomPwd  forKey:@"first"];
        [plainPwddict setObject:randomPwd  forKey:@"second"];
        
        
        NSMutableDictionary*forgotPwddict = [[NSMutableDictionary alloc] init];
        
        [forgotPwddict setObject:plainPwddict  forKey:@"plainPassword"];
        
        NSLog(@"forGotPwdDict:%@",forgotPwddict);
        
        [signInVC forgotOkAction];
        
        [[Client sharedClient] forgotPassword:forgotPwddict toWallMessageWithId:self.emailtextFieldOutlet.text completition:^(BOOL success, NSError *error) {
            
            if(success){
                
                NSLog(@"password changed success");
                
                NSLog(@"salt:%@",[Client sharedClient].user.salt);
                
                //                    NSString *password = [[Client sharedClient] getCryptWithPlainPassword:_currentPasswordTxtFieldOutlet.text andSalt:[Client sharedClient].user.salt];
                //
                //
                //                    NSDictionary *dictionary = @{
                //                                                 @"password":password,
                //                                                 };
                //                    [[Client sharedClient].user initWithDictionary:dictionary];
                //
                //                    [[Utilities sharedInstance] showCustomAlertView:self.view title:@"Mabo" message:@"Password has been changed successfully" controller:self reason:@"changePassword"];
                
                
                if ([_alertParentViewController isKindOfClass:SignInViewController.class]) {
                    
                    [[Utilities sharedInstance] hideCustomAlertView:self.superview];
                    
                    SignInViewController * signInVC = _alertParentViewController;
                    
                    [signInVC forgotYesAction];
                }
                
            }else{
                
                if ([_alertParentViewController isKindOfClass:SignInViewController.class]) {
                    
                    [[Utilities sharedInstance] hideCustomAlertView:self.superview];
                    
                    SignInViewController * signInVC = _alertParentViewController;
                    
                    [signInVC forgotfailiour];
                }
                
                // [self.loading animate:NO];
            }
        }];
        
    }
    else
    {
        // [Utilities alertWithMessage:@"Enter valid email address."];
        // [[Utilities sharedInstance]showCustomAlertView:self.view title:@"Warning" message:@"Enter valid email address."];
        [[Utilities sharedInstance] showCustomAlertView:signInVC.view title:@"Warning" message:@"Enter valid email address." controller:signInVC reason:@"forgot"];
    }
        
    }
    // {"plainPassword":{"first":"iamtest","second":"iamtest"}
}

- (IBAction)cancelBtnAction:(id)sender {
    
    [[Utilities sharedInstance] hideCustomAlertView:self.superview];
}

NSString *letters = @"abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789";

-(NSString *) randomStringWithLength: (int) len {
    
    NSMutableString *randomString = [NSMutableString stringWithCapacity: len];
    
    for (int i=0; i<len; i++) {
        [randomString appendFormat: @"%C", [letters characterAtIndex: arc4random_uniform([letters length])]];
    }
    
    return randomString;
}

@end
