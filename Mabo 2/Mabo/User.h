//
//  User.h
//  Mabo
//
//  Created by Steewe MacBook Pro on 30/03/15.
//  Copyright (c) 2015 Steewe. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface User : NSObject

@property (nonatomic, strong) NSString *email;
@property (nonatomic, strong) NSString *password;
@property (nonatomic, strong) NSString *salt;
@property (nonatomic, strong) NSString *idUser;
@property (nonatomic, strong) NSString *username;
@property (nonatomic, strong) NSMutableArray *pictures;
@property (nonatomic, strong) NSMutableArray *friends;
@property (nonatomic, strong) NSNumber *last_update;
@property (nonatomic, strong) NSMutableDictionary *filterMap;

@property (nonatomic, strong) NSNumber *lat;
@property (nonatomic, strong) NSNumber *lon;
@property (nonatomic, strong) NSString *address;
@property (nonatomic, strong) NSString *description;
@property (nonatomic, strong) NSString *extended_name;
@property (nonatomic, strong) NSNumber *sex;
@property (nonatomic, strong) NSNumber *age;
@property (nonatomic, strong) NSNumber *interested_in;
@property (nonatomic, strong) NSNumber *picture_index_default;
@property (nonatomic, strong) NSNumber *income;
@property (nonatomic, strong) NSNumber *location_visibility;
@property (nonatomic, strong) NSNumber *platform;
@property (nonatomic, strong) NSNumber *is_scrumble;

@property (nonatomic, strong) NSMutableArray *interests;
@property (nonatomic, strong) NSMutableArray *occupations;
@property (nonatomic, strong) NSMutableArray *schools;
@property (nonatomic, strong) NSMutableArray *wall_messages;

@property (nonatomic, strong) NSString *push_device_token;
@property (nonatomic, strong) NSArray *notifications;


-(void)initWithDictionary:(NSDictionary *)dictionary;
-(void)createDictionaryUser;
-(NSDictionary *)getUserInDictionary;
-(void)resetUser;



@end
