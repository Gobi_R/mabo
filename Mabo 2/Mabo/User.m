//
//  User.m
//  Mabo
//
//  Created by Steewe MacBook Pro on 30/03/15.
//  Copyright (c) 2015 Steewe. All rights reserved.
//

#import "User.h"
#import "Client.h"
#import <objc/runtime.h>

@implementation User

@synthesize email,password,salt,idUser,username,pictures,friends,last_update,filterMap;
@synthesize lat,lon,address,description,extended_name,sex,age,interested_in,picture_index_default,income,location_visibility,platform,is_scrumble;
@synthesize interests,occupations,schools,wall_messages;
@synthesize push_device_token;
@synthesize notifications;

-(void)initWithDictionary:(NSDictionary *)dictionary{
    
    for(NSString *key in dictionary){
        NSString *firstLetter = [[key substringToIndex:1] uppercaseString];
        NSString *nameKey = [NSString stringWithFormat:@"set%@%@:",firstLetter,[key substringFromIndex:1]];
        SEL selector = NSSelectorFromString(nameKey);
        
        IMP imp = [self methodForSelector:selector];
        
        if([self respondsToSelector:selector]){
            if([[dictionary objectForKey:key] isKindOfClass:[NSArray class]]){
                void (*func)(id, SEL, NSArray *) = (void *)imp;
                func(self, selector, (NSArray *)[dictionary objectForKey:key]);
            }else if([[dictionary objectForKey:key] isKindOfClass:[NSNumber class]]){
                void (*func)(id, SEL, NSNumber *) = (void *)imp;
                func(self, selector, [dictionary objectForKey:key]);
            }else{
                void (*func)(id, SEL, NSString *) = (void *)imp;
                func(self, selector, [dictionary objectForKey:key]);
            }
        }
    }
    
    [self createDictionaryUser];
}

-(NSDictionary *)getUserInDictionary{
    unsigned int varCount;
    NSMutableDictionary *dictionary = [NSMutableDictionary dictionaryWithCapacity:0];
    Ivar *vars = class_copyIvarList([self class], &varCount);
    
    for (int i = 0; i < varCount; i++) {
        Ivar var = vars[i];
        
        const char* name = ivar_getName(var);
        
        NSString *key = [[NSString alloc] initWithUTF8String:name];
        if([self valueForKey:key]){
            [dictionary setObject:[self valueForKey:key] forKey:key];
        }
    }
    
    free(vars);
    
    return dictionary;
}

-(void)createDictionaryUser{
    unsigned int varCount;
    NSMutableDictionary *dictionary = [NSMutableDictionary dictionaryWithCapacity:0];
    Ivar *vars = class_copyIvarList([self class], &varCount);
    
    for (int i = 0; i < varCount; i++) {
        Ivar var = vars[i];
        
        const char* name = ivar_getName(var);
        const char* typeEncoding = ivar_getTypeEncoding(var);
        
        NSString *key = [[NSString alloc] initWithUTF8String:name];
        if([self valueForKey:key] && ![[self valueForKey:key] isEqual:[NSNull null]]){
            [dictionary setObject:[self valueForKey:key] forKey:key];
        }else{
            NSString* typeEncodingString = [NSString stringWithFormat:@"%s" , typeEncoding];
            
            if([typeEncodingString isEqualToString:@"@\"NSMutableArray\""]){
                [dictionary setObject:@[] forKey:key];
            }else if([typeEncodingString isEqualToString:@"@\"NSNumber\""]){
                if([key isEqualToString:@"picture_index_default"] || [key isEqualToString:@"last_update"]){
                    [dictionary setObject:[NSNumber numberWithInt:0] forKey:key];
                }else{
                    [dictionary setObject:[NSNull null] forKey:key];
                }
            }else{
                [dictionary setObject:@"" forKey:key];
            }

            NSString *firstLetter = [[key substringToIndex:1] uppercaseString];
            NSString *nameKey = [NSString stringWithFormat:@"set%@%@:",firstLetter,[key substringFromIndex:1]];
            SEL selector = NSSelectorFromString(nameKey);
            IMP imp = [self methodForSelector:selector];
            
            if([[dictionary objectForKey:key] isKindOfClass:[NSArray class]]){
                void (*func)(id, SEL, NSArray *) = (void *)imp;
                func(self, selector, @[]);
            }else if([[dictionary objectForKey:key] isKindOfClass:[NSNumber class]]){
                void (*func)(id, SEL, NSNumber *) = (void *)imp;
                func(self, selector, [NSNumber numberWithInt:-1]);
            }else{
                void (*func)(id, SEL, NSString *) = (void *)imp;
                func(self, selector, @"");
            }

            
        }
    }
    
    free(vars);
    
    [[Client sharedClient] saveUser:dictionary];
}

-(void)resetUser{
    unsigned int varCount;
    NSMutableDictionary *dictionary = [NSMutableDictionary dictionaryWithCapacity:0];
    Ivar *vars = class_copyIvarList([self class], &varCount);
    
    for (int i = 0; i < varCount; i++) {
        Ivar var = vars[i];
        
        const char* name = ivar_getName(var);
        const char* typeEncoding = ivar_getTypeEncoding(var);
        
        
        NSString* typeEncodingString = [NSString stringWithFormat:@"%s" , typeEncoding];
        
        NSString *key = [[NSString alloc] initWithUTF8String:name];
        
        if([typeEncodingString isEqualToString:@"@\"NSMutableArray\""]){
            [dictionary setObject:@[] forKey:key];
        }else if([typeEncodingString isEqualToString:@"@\"NSNumber\""]){
            [dictionary setObject:[NSNumber numberWithInt:-1] forKey:key];
        }else{
            [dictionary setObject:@"" forKey:key];
        }
        
        NSString *firstLetter = [[key substringToIndex:1] uppercaseString];
        NSString *nameKey = [NSString stringWithFormat:@"set%@%@:",firstLetter,[key substringFromIndex:1]];
        SEL selector = NSSelectorFromString(nameKey);
        IMP imp = [self methodForSelector:selector];
        
        if([[dictionary objectForKey:key] isKindOfClass:[NSArray class]]){
            void (*func)(id, SEL, NSArray *) = (void *)imp;
            func(self, selector, @[]);
        }else if([[dictionary objectForKey:key] isKindOfClass:[NSNumber class]]){
            void (*func)(id, SEL, NSNumber *) = (void *)imp;
            func(self, selector, [NSNumber numberWithInt:-1]);
        }else{
            void (*func)(id, SEL, NSString *) = (void *)imp;
            func(self, selector, @"");
        }
    }
    
    free(vars);
    
    NSMutableDictionary *dictionaryFilter = [NSMutableDictionary dictionaryWithDictionary:@{@"sex":@"",@"age":@"",@"interested_in":@"",@"income":@"",@"text":@""}];
    
    
    [self initWithDictionary:@{@"email":@"",@"password":@"",@"pictures":@[],@"friends":@[],@"last_update":[NSNumber numberWithInt:0],@"filterMap":dictionaryFilter,@"platform":[NSNumber numberWithInt:1],@"picture_index_default":[NSNumber numberWithInt:0]}];
    
    [[NSUserDefaults standardUserDefaults] setObject:[NSNumber numberWithInt:0] forKey:@"startItemMabo"];
    [[NSUserDefaults standardUserDefaults] removeObjectForKey:@"mabo_push_disable"];
    [[NSUserDefaults standardUserDefaults] removeObjectForKey:@"mabo_sound_disable"];
    
    
    [[Client sharedClient] saveUser:dictionary];
}


@end
