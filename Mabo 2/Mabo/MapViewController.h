//
//  MapViewController.h
//  Mabo
//
//  Created by vishnuprasathg on 10/30/17.
//  Copyright © 2017 DCI. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface MapViewController : UIViewController
@property (weak, nonatomic) IBOutlet UIButton *backbtnoutlet;
@property (weak, nonatomic) IBOutlet UIView *mapBaseViewOutlet;

@property (nonatomic, retain) NSString * viewIdendity;

@property(nonatomic,assign) double latti;
@property(nonatomic,assign) double longi;

- (IBAction)backBtnAction:(id)sender;

@end
