//
//  AppDelegate.h
//  Mobo
//
//  Created by vishnuprasathg on 10/12/17.
//  Copyright © 2017 DCI. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <Sinch/Sinch.h>
#import <CallKit/CallKit.h>

@interface AppDelegate : UIResponder <UIApplicationDelegate,SINClientDelegate, SINCallClientDelegate, SINCallDelegate>

@property (strong, nonatomic) UIWindow *window;
@property (strong, nonatomic) id<SINClient> client;
@property (strong, nonatomic) id<SINClientDelegate> client1;
//@property (nonatomic, readwrite, strong) SINCallKitProvider *callKitProvider;

//-(void)customLocalNotification:(NSDictionary *)notificationDict;
-(void)customLocaChatNotification:(NSDictionary *)notificationDict;



@end

