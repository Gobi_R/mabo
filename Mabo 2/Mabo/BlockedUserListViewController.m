//
//  BlockedUserListViewController.m
//  Mabo
//
//  Created by vishnuprasathg on 11/22/17.
//  Copyright © 2017 DCI. All rights reserved.
//

#import "BlockedUserListViewController.h"
#import "Client.h"
#import "LoadingView.h"
#import "Utilities.h"


@interface BlockedUserListViewController ()<UITableViewDelegate, UITableViewDataSource>

@property (nonatomic, strong) LoadingView *loading;

@end

@implementation BlockedUserListViewController

{
    NSMutableArray * blockedUserListArray;
    NSDictionary* selectedDict;
    NSInteger selectedIndex;
}

- (void)viewDidLoad {
    [super viewDidLoad];
    
    self.noBlockedUserLblOutlet.hidden = YES;
    
    blockedUserListArray = [[NSMutableArray alloc] init];
    selectedDict = [[NSDictionary alloc] init];
    
    self.tableViewOutlet.delegate = self;
    self.tableViewOutlet.dataSource = self;
    
    self.loading = [[LoadingView alloc] initWithFrame:[UIScreen mainScreen].bounds];
    [self.view addSubview:self.loading];
    [self.loading animate:YES];
    
    [[Client sharedClient] getUserBlockList:[Client sharedClient].user.email completition:^(NSDictionary *responceDict, NSError *error) {
        
        [self.loading animate:NO];
        if(responceDict){
            
            NSLog(@"dict:%@",responceDict);
            blockedUserListArray = [responceDict objectForKey:@"blockedUsers"];
            
            if (blockedUserListArray.count > 0) {
                
                self.tableViewOutlet.hidden = NO;
                self.noBlockedUserLblOutlet.hidden = YES;
                
                [self.tableViewOutlet reloadData];
            }
            else
            {
                self.tableViewOutlet.hidden = YES;
                self.noBlockedUserLblOutlet.hidden = NO;
            }
            
        }else{
            NSLog(@"error:%@",error);
        }
    }];
    
    // Do any additional setup after loading the view.
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - TABLEVIEW DELEGATE METHODS


-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    
    return blockedUserListArray.count;
    
}

-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    
    UITableViewCell *cellReal = [tableView dequeueReusableCellWithIdentifier:@"Cell"];
    
    if (blockedUserListArray.count > 0) {
        
        NSDictionary* blockedMembers = [blockedUserListArray objectAtIndex:indexPath.row];
        
        UILabel * nameLbl = [cellReal viewWithTag:1];

        NSString* name = [blockedMembers objectForKey:@"extended_name"];
        
        nameLbl.text = name;
        
    }
    
    return cellReal;
}

-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    if([[Client sharedClient].reachAbility isReachable]){
        //        ProfileViewController *profile = [[ProfileViewController alloc] initWithUser:[[self.visibleUserList objectAtIndex:indexPath.row] objectForKey:@"username"] andName:[[self.visibleUserList objectAtIndex:indexPath.row] objectForKey:@"extended_name"]];
        //        [self.navigationController pushViewController:profile animated:YES];
    }
}


/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

- (IBAction)backBtnAction:(id)sender {
    
    [self.navigationController popViewControllerAnimated:YES];
}

- (IBAction)unBlockBtnAction:(UIButton *)sender {
    
    
    UIView * contentView = sender.superview;
    UIView * cell = contentView.superview;
    
    NSIndexPath *indexPath = [self.tableViewOutlet indexPathForCell:cell];
    
    selectedIndex = (int)indexPath.row;
    
    selectedDict = blockedUserListArray[indexPath.row];
    
    [[Utilities sharedInstance] showCustomAlertViewWithMultiple:self title:@"Unblock" message:@"Are you sure to unblock this user?" actionResponse:@"unblock"];

    
}


-(void)unBlockUser
{
    NSLog(@"dict:%@",selectedDict);
    
    NSString* name = [selectedDict objectForKey:@"username"];
    
    self.loading = [[LoadingView alloc] initWithFrame:[UIScreen mainScreen].bounds];
    [self.view addSubview:self.loading];
    [self.loading animate:YES];
    
    NSString *valueBlock = @"-1";
    [[Client sharedClient] updateFriend:name withStatus:valueBlock completition:^(BOOL success, NSError *error) {
        
        [self.loading animate:NO];
        if(success){
            
            NSMutableSet* set = [NSMutableSet setWithArray:blockedUserListArray];
            [set removeObject:selectedDict];
            NSArray *result =[set allObjects];
            NSLog(@"%@",result);
            blockedUserListArray = [result mutableCopy];
            
            if (blockedUserListArray.count > 0) {
                
                self.tableViewOutlet.hidden = NO;
                self.noBlockedUserLblOutlet.hidden = YES;
                
                [self.tableViewOutlet reloadData];
            }
            else
            {
                self.tableViewOutlet.hidden = YES;
                self.noBlockedUserLblOutlet.hidden = NO;
            }
            
            [[Client sharedClient] updateUserCahceWithcompletition:^(BOOL success, NSError *error) {
                //[self refreshBlockUserButton];
                
                NSLog(@"ok block");
            }];
            
        }else{
            NSLog(@"ERROR BLOCK USER: %@",error);
        }
    }];
}
@end
