//
//  profilePopUpView.h
//  Mobo
//
//  Created by vishnuprasathg on 10/26/17.
//  Copyright © 2017 DCI. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface profilePopUpView : UIView

@property (weak, nonatomic) IBOutlet UILabel *titleLblOutlet;
@property (weak, nonatomic) IBOutlet UILabel *messageLblOutlet;

@property (weak, nonatomic) IBOutlet UIButton *laterBtnOutlet;

@property (strong, nonatomic) UIViewController * alertParentViewController;
@property (weak, nonatomic) IBOutlet UIButton *gotItBtnOutlet;

- (IBAction)laterBtnAction:(id)sender;
- (IBAction)gotItBtnAction:(id)sender;

-(void)alertWithMultiple:(NSString *)title message:(NSString *)message;

@end
