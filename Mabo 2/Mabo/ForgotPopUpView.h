//
//  ForgotPopUpView.h
//  Mabo
//
//  Created by vishnuprasathg on 11/7/17.
//  Copyright © 2017 DCI. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "SignInViewController.h"

@interface ForgotPopUpView : UIView
@property (weak, nonatomic) IBOutlet UITextField *emailtextFieldOutlet;

- (IBAction)okBtnAction:(id)sender;
- (IBAction)cancelBtnAction:(id)sender;

@property (strong, nonatomic) NSString * yesActionResponse;
@property (strong, nonatomic) UIViewController * alertParentViewController;

@end
