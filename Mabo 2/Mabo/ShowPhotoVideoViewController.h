//
//  ShowPhotoVideoViewController.h
//  Mabo
//
//  Created by vishnuprasathg on 11/9/17.
//  Copyright © 2017 DCI. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <AVKit/AVKit.h>
#import <AVFoundation/AVFoundation.h>

@interface ShowPhotoVideoViewController : UIViewController<UICollectionViewDelegate,UICollectionViewDataSource>
@property (weak, nonatomic) IBOutlet UILabel *titleLblOutlet;
@property (weak, nonatomic) IBOutlet UIButton *backBtnOutlet;
@property (weak, nonatomic) IBOutlet UICollectionView *collectionViewOutlet;
@property (weak, nonatomic) IBOutlet UIView *videoBaseViewOutlet;
@property (weak, nonatomic) IBOutlet UIImageView *showImageView;

@property (nonatomic, retain) NSArray * imagesArray;
@property (nonatomic, retain) NSString * titleString;
@property (nonatomic, retain) NSString * selectedImageString;
@property (nonatomic, retain) NSString * selectedVideoString;

- (IBAction)backBtnAction:(id)sender;

@end
