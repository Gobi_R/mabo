#import "CallViewController.h"
#import "CallViewController+UI.h"
#import <Sinch/SINUIView+Fullscreen.h>
#import "SinchClient.h"
#import "SharedVariables.h"
#import "Client.h"

// Usage of gesture recognizers to toggle between front- and back-camera and fullscreen.
//
// * Double tap the local preview view to switch between front- and back-camera.
// * Single tap the local video stream view to make it go full screen.
//     Tap again to go back to normal size.
// * Single tap the remote video stream view to make it go full screen.
//     Tap again to go back to normal size.

@interface CallViewController () <SINCallDelegate>
@property (weak, nonatomic) IBOutlet UIGestureRecognizer *remoteVideoFullscreenGestureRecognizer;
@property (weak, nonatomic) IBOutlet UIGestureRecognizer *localVideoFullscreenGestureRecognizer;
@property (weak, nonatomic) IBOutlet UIGestureRecognizer *switchCameraGestureRecognizer;
@end

@implementation CallViewController

{
    BOOL isIncomming;
    BOOL isOutGoing;
    BOOL isCallConnected;
    
    NSString *startTime;
    NSString *endTime;
    
}

- (id<SINAudioController>)audioController {
    return [[[SinchClient sharedClient] client] audioController];
    //[[(AppDelegate *)[[UIApplication sharedApplication] delegate] client] audioController];
}

- (id<SINVideoController>)videoController {
  return [[[SinchClient sharedClient] client] videoController];
    //[[(AppDelegate *)[[UIApplication sharedApplication] delegate] client] videoController];
}

- (void)setCall:(id<SINCall>)call {
  _call = call;
  _call.delegate = self;
}

#pragma mark - UIViewController Cycle

- (void)viewDidLoad {
  [super viewDidLoad];
    
     isIncomming = NO;
     isOutGoing = NO;
     isCallConnected = NO;

  if ([self.call direction] == SINCallDirectionIncoming) {
    [self setCallStatusText:@"Incomming Call"];
    [self showButtons:kButtonsAnswerDecline];
    [[self audioController] startPlayingSoundFile:[self pathForSound:@"incoming.wav"] loop:YES];
    self.remoteUsername.text = _incomUserName;
    isIncomming = YES;
      
      NSDateFormatter *dateFormatter=[[NSDateFormatter alloc] init];
      [dateFormatter setDateFormat:@"yyyy-MM-dd HH:mm:ss"];
      // or @"yyyy-MM-dd hh:mm:ss a" if you prefer the time with AM/PM
      
      startTime = [NSString stringWithFormat:@"%@",[dateFormatter stringFromDate:[NSDate date]]];
      NSLog(@"%@",startTime);
      
  } else {
    [self setCallStatusText:@"calling..."];
    [self showButtons:kButtonsHangup];
      self.remoteUsername.text = [NSString stringWithFormat:@"%@",[[SharedVariables sharedInstance].selectedUserDictToChat objectForKey:@"extended_name"]];
      isOutGoing = YES;
      
      NSDateFormatter *dateFormatter=[[NSDateFormatter alloc] init];
      [dateFormatter setDateFormat:@"yyyy-MM-dd HH:mm:ss"];
      // or @"yyyy-MM-dd hh:mm:ss a" if you prefer the time with AM/PM
      
      startTime = [NSString stringWithFormat:@"%@",[dateFormatter stringFromDate:[NSDate date]]];
      NSLog(@"%@",startTime);
      
  }

  if ([self.call.details isVideoOffered]) {
    [self.localVideoView addSubview:[[self videoController] localView]];
    [self.localVideoFullscreenGestureRecognizer requireGestureRecognizerToFail:self.switchCameraGestureRecognizer];
    [[[self videoController] localView] addGestureRecognizer:self.localVideoFullscreenGestureRecognizer];
    [[[self videoController] remoteView] addGestureRecognizer:self.remoteVideoFullscreenGestureRecognizer];
  }
}

- (void)viewWillAppear:(BOOL)animated {
  [super viewWillAppear:animated];
    
 // self.remoteUsername.text = [self.call remoteUserId];
  [[self audioController] enableSpeaker];
}

#pragma mark - Call Actions

- (IBAction)accept:(id)sender {
  [[self audioController] stopPlayingSoundFile];
  self.call.delegate = self;
  [self.call answer];
}

- (IBAction)decline:(id)sender {
  [self.call hangup];
  [self dismiss];
}

- (IBAction)hangup:(id)sender {
  [self.call hangup];
  [self dismiss];
}

- (IBAction)muteBtnAction:(id)sender {
}

- (IBAction)onSwitchCameraTapped:(id)sender {
  AVCaptureDevicePosition current = self.videoController.captureDevicePosition;
  self.videoController.captureDevicePosition = SINToggleCaptureDevicePosition(current);
}

- (IBAction)onFullScreenTapped:(id)sender {
  UIView *view = [sender view];
  if ([view sin_isFullscreen]) {
    view.contentMode = UIViewContentModeScaleAspectFit;
    [view sin_disableFullscreen:YES];
  } else {
    view.contentMode = UIViewContentModeScaleAspectFill;
    [view sin_enableFullscreen:YES];
  }
}

- (void)onDurationTimer:(NSTimer *)unused {
  NSInteger duration = [[NSDate date] timeIntervalSinceDate:[[self.call details] establishedTime]];
  [self setDuration:duration];
}

#pragma mark - SINCallDelegate

- (void)callDidProgress:(id<SINCall>)call {
  [self setCallStatusText:@"ringing..."];
  [[self audioController] startPlayingSoundFile:[self pathForSound:@"ringback.wav"] loop:YES];
}

- (void)callDidEstablish:(id<SINCall>)call {
  [self startCallDurationTimerWithSelector:@selector(onDurationTimer:)];
  [self showButtons:kButtonsHangup];
  [[self audioController] stopPlayingSoundFile];
    isCallConnected = YES;
}

- (void)callDidEnd:(id<SINCall>)call {
  [self dismiss];
  [[self audioController] stopPlayingSoundFile];
  [self stopCallDurationTimer];
  [[[self videoController] remoteView] removeFromSuperview];
  [[self audioController] disableSpeaker];
    
    NSDateFormatter *dateFormatter=[[NSDateFormatter alloc] init];
    [dateFormatter setDateFormat:@"yyyy-MM-dd HH:mm:ss"];
    // or @"yyyy-MM-dd hh:mm:ss a" if you prefer the time with AM/PM
    
    endTime = [NSString stringWithFormat:@"%@",[dateFormatter stringFromDate:[NSDate date]]];
    NSLog(@"%@",endTime);
    
    if (isIncomming == YES) {
        
        if (isCallConnected == YES) {
            
            [[Client sharedClient] saveCallLog:[Client sharedClient].user.email to_user:[self.call remoteUserId] type:@"incoming" duration:[SharedVariables sharedInstance].callDurationTime callstartdatetime:startTime callenddatetime:endTime from_user:[Client sharedClient].user.email completition:^(BOOL success, NSError *error) {
                
                if (success) {
                    
                }
                else
                {
                    
                }
            }];
            
        }
        else
        {
            
            [[Client sharedClient] saveCallLog:[Client sharedClient].user.email to_user:[self.call remoteUserId] type:@"missed" duration:@"00:00" callstartdatetime:startTime callenddatetime:endTime from_user:[Client sharedClient].user.email completition:^(BOOL success, NSError *error) {
                
                if (success) {
                    
                }
                else
                {
                    
                }
            }];
            
        }
    }
    else if (isOutGoing == YES)
    {
        if (isCallConnected == YES) {
            
            [[Client sharedClient] saveCallLog:[Client sharedClient].user.email to_user:[self.call remoteUserId] type:@"outgoing" duration:[SharedVariables sharedInstance].callDurationTime callstartdatetime:startTime callenddatetime:endTime from_user:[Client sharedClient].user.email completition:^(BOOL success, NSError *error) {
                
                if (success) {
                    
                }
                else
                {
                    
                }
            }];
        }
        else
        {
            [[Client sharedClient] saveCallLog:[Client sharedClient].user.email to_user:[self.call remoteUserId] type:@"outgoing" duration:@"00:00" callstartdatetime:startTime callenddatetime:endTime from_user:[Client sharedClient].user.email completition:^(BOOL success, NSError *error) {
                
                if (success) {
                    
                }
                else
                {
                    
                }
            }];
        }
    }
}

- (void)callDidAddVideoTrack:(id<SINCall>)call {
  [self.remoteVideoView addSubview:[[self videoController] remoteView]];
}

#pragma mark - Sounds

- (NSString *)pathForSound:(NSString *)soundName {
    
  return [[[NSBundle mainBundle] resourcePath] stringByAppendingPathComponent:soundName];
    
}

@end
