//
//  Client.h
//  Mabo
//
//  Created by Steewe MacBook Pro on 30/03/15.
//  Copyright (c) 2015 Steewe. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "User.h"
#import <CoreLocation/CoreLocation.h>
#import "Reachability.h"
#import <Firebase/Firebase.h>
#import <Photos/Photos.h>
#import <FBSDKLoginKit/FBSDKLoginKit.h>

@interface Client : NSObject <CLLocationManagerDelegate>{
    //NSMutableArray *arr;
}

//@property  NSMutableArray* arr;

@property (nonatomic, strong) User *user;
@property (nonatomic, strong) CLLocationManager *locationManager;
@property BOOL authenticate;
@property BOOL isInCacheSave;
@property (nonatomic, strong) NSString *incomingUser;
@property (nonatomic, strong) NSString *incomingName;
@property (nonatomic, strong) NSString *incomingWallId;
@property (strong, nonatomic) Reachability * reachAbility;
@property BOOL newFilterForMap;
@property (nonatomic, strong) NSMutableArray *sendingPhoto;
@property float incomingOffSet;
@property (nonatomic, strong) Firebase *firebaseConnectedRef;
@property (nonatomic, strong) FBSDKLoginManager *loginManager;

//@property (nonatomic, strong) NSMutableArray *pictureInDownload;;

+(Client*)sharedClient;

-(void)startReachAbility;

-(void)createOrLoadCacheChat;
-(void)createOrLoadCacheUser;
-(void)activeLocation;
-(void)checkAuthenticateWithCompletition:(void (^)(BOOL success))result;
-(void)loginWithEmail:(NSString *)email withPassword:(NSString *)password passwordPlain:(BOOL)isPlain completition:(void (^)(BOOL success, NSError *error))result;
-(void)logOut;
-(void)createAccountWithDictionary:(NSDictionary *)dictionary completition:(void (^)(BOOL success, NSError *error))result;
-(void)createAccountFbWithDictionary:(NSDictionary *)dictionary completition:(void (^)(BOOL success, NSError *error))result;
-(void)updateUserInfoWithCompletition:(void (^)(BOOL success,NSError *error))result;
-(void)saveUser:(NSDictionary *)dictionary;
-(void)FBLogin;
-(void)getUserInfo:(NSString *)email completition:(void (^)(NSDictionary *dictionary, NSError *error))result;
-(void)getPicture:(NSString *)path from:(id)sender withCompletition:(void (^)(UIImage *image, BOOL isInCache, id sender))result;
-(void)updatePicture:(UIImage *)image withIndex:(int)index completition:(void (^)(NSDictionary *dictionary, NSError *error))result;

-(void)updatePicture:(UIImage *)image withIndex:(int)index completition:(void (^)(NSDictionary *dictionary, NSError *error))result;

-(void)getUserBlockList:(NSString *)email completition:(void (^)(NSDictionary *responceDict, NSError *error))result;
-(void)deleteAccount:(NSString *)email completition:(void (^)(NSDictionary *responseDict, NSError *error))result;

-(void)lastSeen:(NSString *)email completition:(void (^)(NSDictionary *responceDict, NSError *error))result;

-(void)feedBackSubmit:(NSString *)value completition:(void (^)(NSDictionary *responseDict, NSError *error))result;
-(void)deletePictureAtIndex:(int)index completition:(void (^)(BOOL success, NSError *error))result;
-(void)getUsersAroundWithRadius:(float)radius andCoordinate:(CLLocationCoordinate2D)coordinate completition:(void (^)(NSArray *usersList, NSError *error))result;
-(void)updateFriend:(NSString *)user withStatus:(NSString *)status completition:(void (^)(BOOL success,NSError *error))result;
-(void)updateUserCahceWithcompletition:(void (^)(BOOL success, NSError *error))result;
-(void)getSearchWithDictionary:(NSDictionary *)dicSearch completition:(void (^)(NSDictionary *dictionary, NSError *error))result;
-(NSArray *)getMessagesList;
-(void)updateCacheChatWithMessage:(NSDictionary *)messageDictionary;
-(void)updateCacheInfoUserWithDictionary:(NSDictionary *)dictionary andUser:(NSString *)username;
-(void)updateCacheChatMessageReadedWithMessageId:(NSDictionary *)messageDictionary;
-(void)updateMessageStatusFromMessageId:(NSString *)messageId withRecipient:(NSString *)recipient withTimeStamp:(NSDate *)timeStamp withStatus:(int)status;
-(void)sendWakeUpWithDictionary:(NSDictionary *)dictionary completition:(void (^)(BOOL success,NSError *error))result;
-(NSMutableArray *)getMessagesCacheFromUser:(NSString *)user numberMessage:(int)number;
-(int)getMessageCahceFromUserCount:(NSString *)user;
-(void)deleteMessageInCahceWithMessageId:(NSString *)messageId withRecipient:(NSString *)recipient;
-(void)deleteUserInCache:(NSString *)username;
-(void)getPictureFromEmail:(NSString *)email completition:(void (^)(NSString *pathImage, NSError *error))result;
-(int)getMessageNotReadedCount;

-(void)updateMultiTextWithType:(NSString *)type withTitle:(NSString *)title completition:(void (^)(NSDictionary *dictionary, NSError *error))result;
-(void)deleteMultiTextWithType:(NSString *)type withTitle:(NSString *)title completition:(void (^)(NSDictionary *dictionary, NSError *error))result;

-(void)deleteAccountWithCompletition:(void (^)(BOOL success))result;

//WALL

-(void)postWallMessageFromDictionary:(NSDictionary *)dictionary completition:(void (^)(NSDictionary *dictionary, NSError *error))result;

-(void)postImage:(UIImage *)image toWallMessageWithId:(NSString *)idMessage completition:(void (^)(BOOL success,NSError *error))result;
-(void)postVideo:(NSString *)file toWallMessageWithId:(NSString *)idMessage completition:(void (^)(BOOL success,NSError *error))result;
-(void)getPostsAroundWithRadius:(float)radius andCoordinate:(CLLocationCoordinate2D)coordinate completition:(void (^)(NSArray *postList, NSError *error))result;
-(void)getPostsAroundWithOutRadius:(float)radius andCoordinate:(CLLocationCoordinate2D)coordinate completition:(void (^)(NSArray *postList, NSError *error))result;
-(void)deleteWallMessageFromId:(NSString *)idMessage completition:(void (^)(BOOL success, NSError *error))result;
-(void)updateWallMessageFromId:(NSString *)idMessage withDictionary:(NSDictionary *)dictionary completition:(void (^)(BOOL success, NSError *error))result;
-(void)updatePartecipateWallMessageFromId:(NSString *)idMessage withStatus:(int)status completition:(void (^)(BOOL success,NSError *error))result;
-(void)updateLikeWallMessageFromId:(NSString *)idMessage withStatus:(int)status completition:(void (^)(BOOL success,NSError *error))result;
-(void)sendCommentWallMessageFromId:(NSString *)idMessage withComment:(NSString *)comment completition:(void (^)(NSDictionary *dictionary,NSError *error))result;
-(void)getWallMessageFromId:(NSString *)idWall completition:(void (^)(NSArray *dictionary,NSError *error))result;

-(void)getUserProfileDetail:(NSString *)email completition:(void (^)(NSDictionary *profileDict, NSError *error))result;  

-(void)changePassword:(NSMutableDictionary *)file toWallMessageWithId:(NSString *)idMessage completition:(void (^)(BOOL success,NSError *error))result;
-(void)forgotPassword:(NSMutableDictionary *)file toWallMessageWithId:(NSString *)emailId completition:(void (^)(BOOL success,NSError *error))result;
-(NSString *)getCryptWithPlainPassword:(NSString *)plainPassword andSalt:(NSString*)salt;

-(void)getMyPost:(void (^)(BOOL success, NSError *error))result;
-(void)saveCallLog:(NSString *)email to_user:(NSString *)to_user type:(NSString *)type duration:(NSString *)duration callstartdatetime:(NSString *)callstartdatetime callenddatetime:(NSString *)callenddatetime from_user:(NSString *)from_user completition:(void (^)(BOOL success, NSError *error))result; 
-(void)getMyLikes:(NSString *)idWall completition:(void (^)(BOOL success, NSError *error))result;
-(void)getMyComments:(NSString *)idWall completition:(void (^)(BOOL success, NSError *error))result;
-(void)getMyJoins:(NSString *)idWall completition:(void (^)(BOOL success, NSError *error))result;
-(void)getChatSearchUser:(NSString *)email searchText:(NSString *)searchText completition:(void (^)(BOOL success, NSError *error))result;
-(void)getCallLogList:(NSString *)page completition:(void (^)(BOOL success, NSError *error))result;
/*
-(void)startReachAbility;
*/


@end
