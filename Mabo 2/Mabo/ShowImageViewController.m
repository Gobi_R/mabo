//
//  ShowImageViewController.m
//  Mabo
//
//  Created by vishnuprasathg on 11/9/17.
//  Copyright © 2017 DCI. All rights reserved.
//

#import "ShowImageViewController.h"
//#import "UIImageView+AFNetworking.h"
#import "UIImageView+WebCache.h"

@interface ShowImageViewController ()

@end

@implementation ShowImageViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    
    
    dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_BACKGROUND, 0), ^{
        // retrive image on global queue
        
        NSString* profilePicPath = [NSString stringWithFormat:@"%@",[self.imageDict objectForKey:@"pic_path"]];
        
        NSString * urlstring = [NSString stringWithFormat:@"http://34.230.105.103:8002/%@", profilePicPath];
        NSURL *imageURL = [NSURL URLWithString:urlstring];
        NSData *imageData = [NSData dataWithContentsOfURL:imageURL];
        UIImage *image = [UIImage imageWithData:imageData];
        
        dispatch_async(dispatch_get_main_queue(), ^{
            
            [self.imageViewOutlet sd_setImageWithURL:imageURL placeholderImage:[UIImage imageNamed:@"placeHolderImage.png"]];
            
            
        });
    });
    
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

- (IBAction)backBtnAction:(id)sender {
    
    [self.navigationController popViewControllerAnimated:NO];
}
@end
