//
//  SharedVariables.m
//  Mobo
//
//  Created by vishnuprasathg on 10/13/17.
//  Copyright © 2017 DCI. All rights reserved.
//

#import "SharedVariables.h"


@implementation SharedVariables

+ (instancetype)sharedInstance
{
    static dispatch_once_t once;
    static SharedVariables * sharedInstance;
    dispatch_once(&once, ^{
        sharedInstance = [[SharedVariables alloc] init];

        
    });
    return sharedInstance;
}


@end
