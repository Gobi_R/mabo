//
//  ChangePasswordViewController.h
//  Mobo
//
//  Created by vishnuprasathg on 10/24/17.
//  Copyright © 2017 DCI. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface ChangePasswordViewController : UIViewController<UITextFieldDelegate>

@property (weak, nonatomic) IBOutlet UITextField *currentPasswordTxtFieldOutlet;

@property (weak, nonatomic) IBOutlet UITextField *oldPasswordTxtFieldOutlet;
@property (weak, nonatomic) IBOutlet UITextField *confirmPasswordTxtFieldOutlet;
@property (weak, nonatomic) IBOutlet UIButton *submitBtnOutlet;
- (IBAction)backBtnAction:(id)sender;
- (IBAction)submitBtnAction:(id)sender;

-(void)changePwdAfterSuccess;

@end
