//
//  ShowImageViewController.h
//  Mabo
//
//  Created by vishnuprasathg on 11/9/17.
//  Copyright © 2017 DCI. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface ShowImageViewController : UIViewController

- (IBAction)backBtnAction:(id)sender;
@property (weak, nonatomic) IBOutlet UIImageView *imageViewOutlet;

@property (nonatomic, retain) NSDictionary * imageDict;
@end
